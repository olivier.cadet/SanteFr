package com.adyax.srisgp;

import android.app.Application;

import com.adyax.srisgp.data.tracker.AppsFlyerHandler;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.core.Twitter;

/**
 * Created by anton.kobylianskiy on 1/16/17.
 */

public class ApplicationUtil {

    private ApplicationUtil() {}

    public static void init(Application application) {
        // Twitter init
        // TwitterAuthConfig authConfig = new TwitterAuthConfig(application.getString(R.string.twitter_consumer_key), application.getString(R.string.twitter_consumer_secret));
        Twitter.initialize(application);

        // Facebook SDK Init
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();

        // AppFlyer Init
        new AppsFlyerHandler(application);
    }
}
