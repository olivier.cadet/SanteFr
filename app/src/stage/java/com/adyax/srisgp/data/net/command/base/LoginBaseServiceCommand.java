package com.adyax.srisgp.data.net.command.base;

import android.os.Parcel;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.UUIDUtils;
import com.adyax.srisgp.data.repository.IRepository;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 12/6/16.
 */

public abstract class LoginBaseServiceCommand extends ServiceCommand {

    public int firstLogin=0;
    public final String device_id;
    public final String push_token;

    public String latitude;
    public String longitude;

    public final int isAndroid=1;

//    @Inject
//    IRepository repository;

    protected LoginBaseServiceCommand(Parcel in) {
//        App.getApplicationComponent().inject(this);
        device_id = in.readString();
        push_token = in.readString();
//        latitude= in.readFloat();
//        longitude= in.readFloat();
//        updateLocation();
    }

    public void updateLocation(String geoLocation) {
//        final String geoLocation = repository.getGeoLocation();
        if(geoLocation!=null){
            String[] split=geoLocation.split(",");
            if(split.length==3){
                latitude=split[0];
                longitude=split[1];
            }
        }
    }

    public LoginBaseServiceCommand(String push_token) {
//        App.getApplicationComponent().inject(this);
        this.device_id = UUIDUtils.generateUniqueID(App.getAppContext());
        this.push_token = push_token;
//        updateLocation();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(device_id);
        dest.writeString(push_token);
//        if(latitude!=null&& longitude!=null) {
//            dest.writeFloat(latitude);
//            dest.writeFloat(longitude);
//        }
    }

    public boolean isFirstLogin() {
        return firstLogin==1;
    }

    public LoginBaseServiceCommand setFirstLogin() {
        this.firstLogin = 1;
        return this;
    }

    public String getPushToken() {
        return push_token;
    }

//    public LoginBaseServiceCommand setGeo(Float latitude, Float longitude) {
//        this.latitude = latitude;
//        this.longitude = longitude;
//        return this;
//    }
}
