package com.adyax.srisgp;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;
import android.util.Log;

import com.adyax.srisgp.data.tracker.AppsFlyerHandler;
import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterConfig;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 1/16/17.
 */

public class ApplicationUtil {

    private ApplicationUtil() {}

    public static void init(Application application) {
        // Twitter init
        TwitterConfig config = new TwitterConfig.Builder(application)
                .logger(new DefaultLogger(Log.DEBUG))
                .debug(true)
                .build();
        Twitter.initialize(config);

        // Facebook SDK Init
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();

        // AppFlyer Init
        new AppsFlyerHandler(application);

//        Timber.plant(new Timber.DebugTree());
//        Stetho.initializeWithDefaults(application);
//        FirebaseApp.initializeApp(application);
    }
}

