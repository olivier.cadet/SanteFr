package com.mapbox.mapboxsdk.maps;

import android.graphics.RectF;
import android.os.Handler;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraUpdate;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 3/22/17.
 */

public class MapboxHelper {

    public static List<Marker> getMarkersInRect(MapboxMap mapboxMap, RectF rect) {
        Class<? extends MapboxMap> clazz = mapboxMap.getClass();
        Field field = null;
        try {
            field = clazz.getDeclaredField("annotationManager");
            field.setAccessible(true);

            AnnotationManager annotationManager = null;
            annotationManager = (AnnotationManager) field.get(mapboxMap);
            return annotationManager.getMarkersInRect(rect);
        } catch (NoSuchFieldException e){
            e.printStackTrace();
            return Collections.emptyList();
        } catch (IllegalAccessException e) {
            return Collections.emptyList();
        }
    }

    public static void moveCamera(MapboxMap mapboxMap, CameraUpdate update, MapboxMap.CancelableCallback callback) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {

                Class<? extends MapboxMap> clazz = mapboxMap.getClass();
                Field field = null;
                try {
                    field = clazz.getDeclaredField("transform");
                    field.setAccessible(true);

                    Transform transform = (Transform) field.get(mapboxMap);

                    transform.moveCamera(mapboxMap, update, null);
                    // MapChange.REGION_DID_CHANGE_ANIMATED is not called for `jumpTo`
                    // invalidate camera position to provide OnCameraChange event.
                    mapboxMap.onPostMapReady();
                    callback.onFinish();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
