package com.adyax.srisgp.firebase;

import android.util.Log;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.command.SetPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.UserResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.repository.SharedPreferencesHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.NotificationHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class AsipMessagingService extends FirebaseMessagingService {
    private String TAG = this.getClass().getSimpleName();
    IRepository repository = new SharedPreferencesHelper(App.getAppContext());

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        notificationHelper.send(remoteMessage);
        Log.d(TAG, "HANDLING THE RECEIVED MESSAGE ! ");
        NotificationHelper.getInstance(this).send(remoteMessage);
    }

    public void setRepo(IRepository repo) {
        this.repository = repo;
    }


    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "NEW TOKEN: " + token);

        sendRegistrationToServer(token);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Log.d(TAG, "SENDING NEW TOKEN TO SERVER: " + token);

        String uid = "0";
        if (repository != null) {
            uid = App.getAppContext().getSharedPreferences("settings", 0).getString(UserResponse.UID, "0");
        }

        if (uid != null && !uid.equals("0")) {
            ExecutionService.sendCommand(getBaseContext(), null, new SetPushCredentialServiceCommand(token, uid), ExecutionService.SET_PUSH_CREDENTIAL);
        }
    }
}
