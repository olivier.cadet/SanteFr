package com.adyax.srisgp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.BmcItem;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends BaseOrmLiteSqliteOpenHelper {

    public static final Class<?> dataClass[] = {
            RelatedFilters.class,
            RelatedItem.class,
            Related.class,
            QuickCardsAnonymousItem.class,
            NotificationItem.class,
            SearchItemHuge.class,
            FavoritesItem.class,
            HistoryItem.class,
            AlertItem.class,
            RecentLocationItem.class,
            SearchItem.class,
            SearchMessage.class,
            BmcItem.class
    };

    private static int VERSION = 65;
    private static String DBNAME = "tables.db";
    private SQLiteDatabase sqLiteDatabase;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, VERSION);
        this.sqLiteDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        for (Class<?> data : dataClass) {
            try {
                TableUtils.createTable(connectionSource, data);
            } catch (Exception e) {
//			MyLog.getMyLog().e(e);
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        for (Class<?> data : dataClass) {
            try {
                TableUtils.dropTable(connectionSource, data, true);
            } catch (Exception e) {
                e.printStackTrace();
////			MyLog.getMyLog().e(e);
//                try {
//                    TableUtils.createTable(connectionSource, data);
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
            }
        }
        onCreate(database, connectionSource);
    }
}