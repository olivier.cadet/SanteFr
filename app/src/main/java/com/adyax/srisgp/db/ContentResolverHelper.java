package com.adyax.srisgp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.adyax.srisgp.mvp.history.HistoryKey;
import com.adyax.srisgp.mvp.history.HistoryType;
import com.adyax.srisgp.mvp.notification.StatusNotification;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by SUVOROV on 3/25/16.
 */
public class ContentResolverHelper implements IDataBaseHelper {

    public static final NodeType[] INFO_NODE_TYPES = {NodeType.APPLICATIONS, NodeType.FICHES_INFOS, NodeType.LIENS_EXTERNES, NodeType.NUMERO_TEL, NodeType.PAGE_SIMPLE, NodeType.RECHERCHE_ENREGISTREE, NodeType.VIDEO};
    public static final NodeType[] FIND_NODE_TYPES = {NodeType.PROFESSIONNEL_DE_SANTE, NodeType.SERVICE_DE_SANTE, NodeType.ESTABLISSEMENT_DE_SANTE, NodeType.OFFRES_DE_SOINS};
    public static final Set<NodeType> findType = new HashSet<>();

    static {
        findType.add(NodeType.PROFESSIONNEL_DE_SANTE);
        findType.add(NodeType.SERVICE_DE_SANTE);
        findType.add(NodeType.ESTABLISSEMENT_DE_SANTE);
        findType.add(NodeType.OFFRES_DE_SOINS);
    }

    private Context context;

    public ContentResolverHelper(Context context) {
        this.context = context;
        // We will show all urgent alerts after reload app
        setUnreadAllUrgentAlert();
    }

    @Override
    public boolean getFavorite(long cardId) {
        final Cursor cursor = context.getContentResolver().query(TablesContentProvider.CARDS_CONTENT_URI, new String[]{QuickCardsAnonymousItem.FAVORITE},
                String.format("%s==?", QuickCardsAnonymousItem.NID), new String[]{String.valueOf(cardId)}, null);
        try {
            if (cursor != null && cursor.getCount() == 1 && cursor.moveToFirst()) {
                return cursor.getInt(0) > 0;
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return false;
    }

    @Override
    public void setPushReadAlert(long id) {
        ContentValues values = new ContentValues();
        values.put(AlertItem.PUSH_READ, "true");
        context.getContentResolver().update(TablesContentProvider.ALERTS_CONTENT_URI, values,
                String.format("%s==?", AlertItem.ID_DB), new String[]{String.valueOf(id)});
    }

    @Override
    public boolean isPushReadAlert(long id) {
        final Cursor cursor = context.getContentResolver().query(TablesContentProvider.ALERTS_CONTENT_URI,
                new String[]{"COUNT(*)"}, String.format("%s==%d AND %s<>'false'", AlertItem.ID_DB, id, AlertItem.PUSH_READ), null, null);
        try {
            if (cursor != null) {
                return cursor.getCount() == 1 && cursor.moveToFirst() && cursor.getInt(0) > 0;
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return false;
    }

    @Override
    public void updateFavoriteAdvertCard(long cardId, long favorite) {
        ContentValues values = new ContentValues();
        values.put(SearchItemHuge.FAVORITE, favorite);
        context.getContentResolver().update(TablesContentProvider.ADVERT_CONTENT_URI, values,
                String.format("%s==?", SearchItemHuge.ID_DB), new String[]{String.valueOf(cardId)});
    }

    // TODO may be it wrong method
    @Override
    public void updateFavoriteQuickCard(long cardId, long favorite) {
        ContentValues values = new ContentValues();
        values.put(QuickCardsAnonymousItem.FAVORITE, favorite);
        context.getContentResolver().update(TablesContentProvider.CARDS_CONTENT_URI, values,
                String.format("%s==?", QuickCardsAnonymousItem.NID), new String[]{String.valueOf(cardId)});
    }

    @Override
    public Cursor getNotification(StatusNotification statusNotification) {
        return context.getContentResolver().query(TablesContentProvider.NOTIFICATION_CONTENT_URI, null,
                String.format("%s==?", NotificationItem.STATUS), new String[]{statusNotification.getValueStr()}, NotificationItem.CREATED);
    }

    @Override
    public void markNotificationAsRead(long id) {
        ContentValues values = new ContentValues();
        values.put(NotificationItem.IS_READ, true);
        context.getContentResolver().update(TablesContentProvider.NOTIFICATION_CONTENT_URI, values,
                String.format("%s==?", NotificationItem.NID), new String[]{String.valueOf(id)});
    }

    @Override
    public void markSearchMessageAdRead(String key) {
        ContentValues values = new ContentValues();
        values.put(SearchMessage.READ, true);
        context.getContentResolver().update(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, values,
                String.format("%s==?", SearchMessage.KEY), new String[]{key});
    }

    @Override
    public void removeNotification(long id) {
        context.getContentResolver().delete(TablesContentProvider.NOTIFICATION_CONTENT_URI,
                String.format("%s==?", NotificationItem.NID), new String[]{String.valueOf(id)});
    }

    @Override
    public Cursor getSearchMessages(SearchType searchType) {
        return context.getContentResolver().query(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, null,
                SearchMessage.READ + "=? and " + SearchMessage.SEARCH_TYPE + "=?", new String[]{String.valueOf("false"), searchType.name()}, null, null);
    }

    @Override
    public boolean containsSearchMessage(String key, SearchType searchType) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, null,
                    SearchMessage.KEY + "=? and " + SearchMessage.SEARCH_TYPE + "=?", new String[]{String.valueOf(key), searchType.name()}, null, null);

            if (cursor != null) {
                return cursor.getCount() > 0;
            }
            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public int deleteHistory(Uri uri, List<HistoryKey> ids) {
        int deleteCount = 0;
        for (HistoryKey key : ids) {
            deleteCount += context.getContentResolver().delete(uri, HistoryItem.SID + "=" + key.sid + " and " + HistoryItem.NID + "=" + key.nid, null);
        }
        return deleteCount;
    }

    @Override
    public Cursor getRelated(SearchType searchType) {
        return context.getContentResolver().query(TablesContentProvider.RELATED_CONTENT_URI, null,
                String.format("%s=='%s'", Related.SEARCH_TYPE, searchType.name()), null, null);
    }

    @Override
    public Cursor getRelatedItem(SearchType searchType) {
//        return context.getContentResolver().query(TablesContentProvider.RELATED_ITEM_CONTENT_URI, null,
//        String.format("%s=='%s'", RelatedItem.SEARCH_TYPE, searchType.name()), null, null);
        return context.getContentResolver().query(TablesContentProvider.RELATED_VIRTUAL_CONTENT_URI, null,
                null, new String[]{searchType.name()}, null);
    }

    @Override
    public void closeUrgentAlert(AlertItem alertItem) {
        ContentValues values = new ContentValues();
        values.put(AlertItem.URGENT_READ, "true");
        context.getContentResolver().update(TablesContentProvider.ALERTS_CONTENT_URI, values,
                String.format("%s==?", AlertItem.ID_DB), new String[]{String.valueOf(alertItem.getId())});
    }

    @Override
    public void setUnreadAllUrgentAlert() {
        ContentValues values = new ContentValues();
        values.put(AlertItem.URGENT_READ, "false");
        context.getContentResolver().update(TablesContentProvider.ALERTS_CONTENT_URI, values, null, null);
    }

    @Override
    public Cursor getAdvertSearch(SearchType searchType) {
        switch (searchType) {
            case info:
                return context.getContentResolver().query(TablesContentProvider.ADVERT_CONTENT_URI, null,
                        String.format("%s=='%s'", SearchItemHuge.REQUEST_TYPE, searchType.name()), null, null);
            case find:
            case around:
            default:
                final String MIN_DOUBLE_STR = String.valueOf(AroundMeArguments.MIN_DOUBLE).replace(",", ".");
                return context.getContentResolver().query(TablesContentProvider.ADVERT_CONTENT_URI, null,
                        String.format("abs(%s)>%s AND abs(%s)>%s AND %s=='%s'", SearchItemHuge.LAT, MIN_DOUBLE_STR,
                                SearchItemHuge.LON, MIN_DOUBLE_STR, SearchItemHuge.REQUEST_TYPE, searchType.name()),
                        null, SearchItemHuge.DISTANCE_M);
        }
    }

    @Override
    public Cursor getFavorites(StatusNotification statusNotification) {
        return context.getContentResolver().query(TablesContentProvider.FAVORITES_CONTENT_URI, null,
                String.format("%s==?", NotificationItem.STATUS), new String[]{statusNotification.getValueStr()}, NotificationItem.NID);
    }

    @Override
    public Cursor getAlerts(boolean bAll) {
        return context.getContentResolver().query(TablesContentProvider.ALERTS_CONTENT_URI, bAll ?
                        new String[]{AlertItem.ID_DB, AlertItem.PUSH_READ, AlertItem.URGENT_READ} : null,
                bAll ? String.format("DATE('now')>=DATE(%s) AND DATE('now')<=DATE(%s)", AlertItem.START_DATE, AlertItem.END_DATE) : null, null, getAlertOrder());
    }

    @Override
    public int getCountAlerts() {
        final Cursor cursor = context.getContentResolver().query(TablesContentProvider.ALERTS_CONTENT_URI,
                new String[]{"COUNT(*)"}, String.format("DATE('now')>=DATE(%s) AND DATE('now')<=DATE(%s)",
                        AlertItem.START_DATE, AlertItem.END_DATE), null, null);
        try {
            if (cursor != null && cursor.getCount() == 1 && cursor.moveToFirst()) {
                return cursor.getInt(0);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return 0;
    }

    @Override
    public Cursor getUrgentAlerts() {
        return context.getContentResolver().query(TablesContentProvider.ALERTS_CONTENT_URI, null,
                String.format("%s==%d AND DATE('now')>=DATE(%s) AND DATE('now')<=DATE(%s) AND %s!='true'",
                        AlertItem.LEVEL, AlertItem.URGENT_ALERTS, AlertItem.START_DATE, AlertItem.END_DATE, AlertItem.URGENT_READ),
                null, getAlertOrder() + " LIMIT 1");
    }

    private String getAlertOrder() {
        return String.format("%s DESC, %s DESC, %s DESC, %s DESC", AlertItem.LEVEL, AlertItem.START_DATE, AlertItem.INFO_UPDATED, AlertItem.ID_DB);
    }

    @Override
    public Cursor getHistory(HistoryType type) {
        return context.getContentResolver().query(TablesContentProvider.HISTORY_CONTENT_URI, null, getSelection(type),
                null, HistoryItem.LAST_VISIT + " DESC");
    }

    private String getSelection(HistoryType type) {
        switch (type) {
            case search:
                return HistoryItem.ANONYMOUS + "=" + String.valueOf(HistoryItem.TYPE_SEARCH);
        }
        return HistoryItem.ANONYMOUS + "=" + String.valueOf(HistoryItem.TYPE_ANONIMUS_HISTORY) + " AND " + HistoryItem.NODE_TYPE + " IN (" + getFilter(type) + ")";
    }

    private String getFilter(HistoryType type) {
        switch (type) {
            case info:
                Set<NodeType> a = new HashSet<>();
                return getNodeTypesStrings(INFO_NODE_TYPES);
            case find:
                return getNodeTypesStrings(FIND_NODE_TYPES);
        }
        throw new IllegalArgumentException();
    }

    private String getNodeTypesStrings(NodeType... nodeTypes) {
        StringBuilder s = new StringBuilder();
        for (NodeType n : nodeTypes) {
            if (s.length() > 0) {
                s.append(",");
            }
            s.append("'" + n.name() + "'");
        }
        return s.toString();
    }


    @Override
    public void deleteHistory(Uri uri, SearchType searchType) {
        int deleteCount = context.getContentResolver().delete(uri,
                String.format("%s=?", SearchItemHuge.REQUEST_TYPE), new String[]{searchType.name()});
    }

    @Override
    public void deleteHistory(Uri uri, String fieldName, SearchType searchType) {
        int deleteCount = context.getContentResolver().delete(uri,
                String.format("%s=?", fieldName), new String[]{searchType.name()});
    }

    @Override
    public void deleteHistory(Uri uri) {
        int deleteCount = context.getContentResolver().delete(uri, null, null);
//        if(uri==TablesContentProvider.NOTIFICATION_CONTENT_URI){
//            context.getContentResolver().notifyChange(uri, null);
//        }
    }

    @Override
    public void reset() {
        deleteHistory(TablesContentProvider.CARDS_CONTENT_URI);
        deleteHistory(TablesContentProvider.NOTIFICATION_CONTENT_URI);
        deleteHistory(TablesContentProvider.ADVERT_CONTENT_URI);
        deleteHistory(TablesContentProvider.FAVORITES_CONTENT_URI);
        if (BuildConfig.DEBUG) {
            deleteHistory(TablesContentProvider.HISTORY_CONTENT_URI);
        }
        deleteHistory(TablesContentProvider.ALERTS_CONTENT_URI);
        deleteHistory(TablesContentProvider.RECENT_LOCATION_CONTENT_URI);
        deleteHistory(TablesContentProvider.RELATED_CONTENT_URI);
        deleteHistory(TablesContentProvider.RELATED_ITEM_CONTENT_URI);
        deleteHistory(TablesContentProvider.RELATED_FILTER_ITEM_CONTENT_URI);
        deleteHistory(TablesContentProvider.RECENT_PHRASE_CONTENT_URI);
    }

    @Override
    public Uri addRecentLocation(RecentLocationItem recentLocationItem) {
        return context.getContentResolver().insert(TablesContentProvider.RECENT_LOCATION_CONTENT_URI,
                BaseOrmLiteSqliteOpenHelper.getContentValues(recentLocationItem));
    }

    @Override
    public Cursor getRecentLocations() {
        return context.getContentResolver().query(TablesContentProvider.RECENT_LOCATION_CONTENT_URI, null, null, null, RecentLocationItem.ID + " DESC" + " LIMIT 3");
    }

    @Override
    public void clearRecentLocations() {
        context.getContentResolver().delete(TablesContentProvider.RECENT_LOCATION_CONTENT_URI, null, null);
    }

    @Override
    public void removeRecentLocation(long id) {
        context.getContentResolver().delete(TablesContentProvider.RECENT_LOCATION_CONTENT_URI,
                String.format("%s==?", RecentLocationItem.ID), new String[]{String.valueOf(id)});
    }

    @Override
    public Uri addRecentWhatPhrase(SearchItem searchItem) {
        return context.getContentResolver().insert(TablesContentProvider.RECENT_PHRASE_CONTENT_URI,
                BaseOrmLiteSqliteOpenHelper.getContentValues(searchItem));
    }

    @Override
    public Cursor getRecentPhrases() {
        return context.getContentResolver().query(TablesContentProvider.RECENT_PHRASE_CONTENT_URI, null, null, null, SearchItem.ID + " DESC" + " LIMIT 3");
    }

    @Override
    public void removeRecentPhrase(long id) {
        context.getContentResolver().delete(TablesContentProvider.RECENT_PHRASE_CONTENT_URI,
                String.format("%s==?", SearchItem.ID), new String[]{String.valueOf(id)});
    }

    @Override
    public void clearRecentPhrases() {
        context.getContentResolver().delete(TablesContentProvider.RECENT_PHRASE_CONTENT_URI, null, null);
    }

    @Override
    public int bulkInsert(Uri uri, List<ContentValues> contentValuesForBulk) {
        return context.getContentResolver().bulkInsert(uri,
                contentValuesForBulk.toArray(new ContentValues[contentValuesForBulk.size()]));
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return context.getContentResolver().insert(uri, contentValues);
    }

    @Override
    public void insertOrUpdateHistory(HistoryItem item) {
//        Cursor cursor = context.getContentResolver().query(
//                TablesContentProvider.HISTORY_CONTENT_URI,
//                null,
//                HistoryItem.NID + "=? and " + HistoryItem.TITLE + "=?",
//                new String[]{String.valueOf(item.nid), item.title},
//                null
//        );
//        try {
//            if (cursor != null && cursor.getCount() > 0) {
//                ContentValues values = new ContentValues();
//                values.put(HistoryItem.LAST_VISIT, HistoryItem.getTime());
//                final int count = context.getContentResolver().update(
//                        TablesContentProvider.HISTORY_CONTENT_URI,
//                        values,
//                        HistoryItem.NID + "=? and " + HistoryItem.TITLE + "=?",
//                        new String[]{String.valueOf(item.nid), item.title}
//                );
//            } else {
//                insert(TablesContentProvider.HISTORY_CONTENT_URI, BaseOrmLiteSqliteOpenHelper.getContentValues(item));
//            }
//        }finally {
//            cursor.close();
//        }
        Cursor cursor = context.getContentResolver().query(
                TablesContentProvider.HISTORY_CONTENT_URI,
                null,
                HistoryItem.NID + "=? and " + HistoryItem.SID + "=?",
                new String[]{String.valueOf(item.nid), String.valueOf(item.hashCode())},
                null
        );
        try {
            if (cursor != null && cursor.getCount() > 0) {
                ContentValues values = new ContentValues();
                values.put(HistoryItem.LAST_VISIT, HistoryItem.getTime());
                final int count = context.getContentResolver().update(
                        TablesContentProvider.HISTORY_CONTENT_URI,
                        values,
                        HistoryItem.NID + "=? and " + HistoryItem.SID + "=?",
                        new String[]{String.valueOf(item.nid), String.valueOf(item.hashCode())}
                );
            } else {
                insert(TablesContentProvider.HISTORY_CONTENT_URI, BaseOrmLiteSqliteOpenHelper.getContentValues(item));
            }
        } finally {
            cursor.close();
        }
    }

    @Override
    public int deleteUnreadSearchMessages(SearchType searchType) {
        return context.getContentResolver().delete(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI,
                SearchMessage.READ + "=? and " + SearchMessage.SEARCH_TYPE + "=?", new String[]{String.valueOf("false"), searchType.name()});
    }

    @Override
    public void updateHistory(HistoryItem historyItem) {
        ContentValues values = new ContentValues();
        values.put(HistoryItem.LAST_VISIT, HistoryItem.getTime());
        final int count = context.getContentResolver().update(TablesContentProvider.HISTORY_CONTENT_URI, values,
                String.format("%s==? AND %s==?", HistoryItem.SID, HistoryItem.NID), new String[]{String.valueOf(historyItem.getSid()), String.valueOf(historyItem.getNid())});
    }
}
