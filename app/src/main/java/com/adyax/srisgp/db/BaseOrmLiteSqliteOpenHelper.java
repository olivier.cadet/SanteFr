package com.adyax.srisgp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.SearchItemType;
import com.adyax.srisgp.data.net.response.tags.FavoriteNodeType;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.lang.reflect.Field;

/**
 * Created by SUVOROV on 4/17/16.
 */
public abstract class BaseOrmLiteSqliteOpenHelper extends OrmLiteSqliteOpenHelper {

    public BaseOrmLiteSqliteOpenHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory, int databaseVersion) {
        super(context, databaseName, factory, databaseVersion);
    }

    public static ContentValues getContentValues(Object object) {
        if(object instanceof IExpandArray){
            ((IExpandArray)object).collapse();
        }
        final ContentValues contentValues = new ContentValues();
        final Field[] fields = object.getClass().getFields();
        for (Field field : fields) {
            try {
                final Object o = field.get(object);
                if (o != null) {
                    if(field.isAnnotationPresent(DatabaseField.class)) {
                        DatabaseField annotation = field.getAnnotation(DatabaseField.class);
                        if(annotation.foreign()){
                            contentValues.put(annotation.columnName(), getForeignId(o));
                        }else {
                            if(!annotation.generatedId()) {
                                contentValues.put(annotation.columnName(), o != null ? o.toString() : null);
                            }
                        }
                    }
                }
            } catch (IllegalAccessException e) {
//                MyLog.getMyLog().e(e);
            } catch (Exception e) {
//                MyLog.getMyLog().e(e);
            }
        }
        return contentValues;
    }

    private static String getForeignId(Object o) throws IllegalAccessException {
        for (Field field2 :  o.getClass().getFields()) {
            final Object o2 = field2.get(o);
            if(o2!=null){
                if(field2.isAnnotationPresent(DatabaseField.class)){
                    DatabaseField annotation2 = field2.getAnnotation(DatabaseField.class);
                    if(annotation2.generatedId()||annotation2.id() ){
                        return o2 != null ? o2.toString() : null;
                    }
                }
            }
        }
        return null;
    }

    public static void fromCursor(Cursor cursor, Object object) {
        final Field[] fields = object.getClass().getFields();
        for (Field field : fields) {
            try {
                final DatabaseField annotation = field.getAnnotation(DatabaseField.class);
                if (annotation != null) {
                    final String columnName = annotation.columnName();
                    final int columnIndex = cursor.getColumnIndex(columnName);
                    if (columnIndex != -1) {
                        final String string = cursor.getString(columnIndex);
                        if (string != null) {
                            if (field.getGenericType() == Long.TYPE) {
                                field.setLong(object, Long.parseLong(string));
                            } else if (field.getGenericType() == Float.TYPE) {
                                field.setFloat(object, Float.parseFloat(string));
                            } else if (field.getGenericType() == FavoriteNodeType.class) {
                                field.set(object, FavoriteNodeType.valueOf(string));
                            } else if (field.getGenericType() == Integer.TYPE) {
                                field.set(object, Integer.valueOf(string));
                            } else if (field.getGenericType() == Double.TYPE) {
                                field.setDouble(object, Double.parseDouble(string));
                            } else if (field.getGenericType() == IconType.class) {
                                field.set(object, IconType.valueOf(string));
                            } else if (field.getGenericType() == SearchItemType.class){
                                field.set(object, SearchItemType.valueOf(string));
                            } else if (field.getGenericType() == NodeType.class){
                                field.set(object, NodeType.valueOf(string));
                            } else if (field.getGenericType() == SearchType.class){
                                field.set(object, SearchType.valueOf(string));
                            } else {
                                field.set(object, string);
                            }
                        }
                    }
                }
            } catch (IllegalAccessException e) {
//                MyLog.getMyLog().e(e);
            } catch (Exception e) {
//                MyLog.getMyLog().e(e);
            }
        }
        if(object instanceof IExpandArray){
            ((IExpandArray)object).expand(cursor);
        }
    }
}
