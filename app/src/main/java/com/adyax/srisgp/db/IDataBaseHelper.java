package com.adyax.srisgp.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.mvp.history.HistoryKey;
import com.adyax.srisgp.mvp.history.HistoryType;
import com.adyax.srisgp.mvp.notification.StatusNotification;

import java.util.List;

/**
 * Created by SUVOROV on 3/25/16.
 */
public interface IDataBaseHelper {
    int bulkInsert(Uri uri, List<ContentValues> contentValuesForBulk);

    void updateHistory(HistoryItem time);

    void updateFavoriteAdvertCard(long cardId, long favorite);

    void updateFavoriteQuickCard(long cardId, long favorite);

    Cursor getNotification(StatusNotification statusNotification);

    Cursor getRelated(SearchType searchType);

    void setUnreadAllUrgentAlert();

    Cursor getAdvertSearch(SearchType searchType);
    Cursor getFavorites(StatusNotification statusNotification);

    Cursor getAlerts(boolean bAll);
    int getCountAlerts();
    Cursor getUrgentAlerts();

    Cursor getHistory(HistoryType type);

    void deleteHistory(Uri uri, SearchType searchType);

    void deleteHistory(Uri uri, String fieldName, SearchType searchType);

    void deleteHistory(Uri uri);
    void reset();

    void removeNotification(long id);
    void markNotificationAsRead(long id);

    Uri addRecentLocation(RecentLocationItem recentLocationItem);
    Cursor getRecentLocations();
    void removeRecentLocation(long id);
    void clearRecentLocations();

    Uri addRecentWhatPhrase(SearchItem searchItem);
    Cursor getRecentPhrases();
    void removeRecentPhrase(long id);
    void clearRecentPhrases();

    boolean getFavorite(long id);

    void setPushReadAlert(long id);

    boolean isPushReadAlert(long id);

    Uri insert(Uri uri, ContentValues contentValues);

    void insertOrUpdateHistory(HistoryItem item);

    Cursor getRelatedItem(SearchType type);

    void closeUrgentAlert(AlertItem alertItem);

    Cursor getSearchMessages(SearchType searchType);
    void markSearchMessageAdRead(String key);
    int deleteUnreadSearchMessages(SearchType searchType);
    boolean containsSearchMessage(String key, SearchType searchType);

    int deleteHistory(Uri uri, List<HistoryKey> condition);
}
