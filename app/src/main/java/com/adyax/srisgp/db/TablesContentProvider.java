package com.adyax.srisgp.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import androidx.annotation.Nullable;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.BmcItem;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.j256.ormlite.table.DatabaseTable;

import javax.inject.Inject;

import timber.log.Timber;

public class TablesContentProvider extends ContentProvider {
	
	public static final String AUTHORITY= String.format("%s.tables.provider", BuildConfig.APPLICATION_ID);
	public static final String CONTENT = "content://";

	private static final int CARDS = 1;
	private static final int NOTIFICATION = 2;
	private static final int ADVERT = 3;
	private static final int FAVORITES = 4;
	private static final int HISTORY = 5;
	private static final int ALERT = 6;
	private static final int RECENT_LOCATION = 7;
	private static final int RELATED = 8;
	private static final int RELATED_ITEM = 9;
	private static final int RELATED_FILTER_ITEM = 10;
	private static final int RELATED_VIRTUAL = 11;
	private static final int RECENT_PHRASE = 12;
	private static final int SEARCH_MESSAGE = 13;
	private static final int BMC_ITEM = 14;

	public static final String CARDS_TABLE_NAME = QuickCardsAnonymousItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String NOTIFICATION_TABLE_NAME = NotificationItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String ADVERT_TABLE_NAME = SearchItemHuge.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String FAVORITES_TABLE_NAME = FavoritesItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String HISTORY_TABLE_NAME = HistoryItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String ALERT_TABLE_NAME = AlertItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RECENT_LOCATION_TABLE_NAME = RecentLocationItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RECENT_PHRASE_TABLE_NAME = SearchItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RELATED_TABLE_NAME = Related.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RELATED_ITEM_TABLE_NAME = RelatedItem.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RELATED_FILTER_ITEM_TABLE_NAME = RelatedFilters.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String RELATED_VIRTUAL_TABLE_NAME = "RELATED_VIRTUAL";
	public static final String SEARCH_MESSAGE_TABLE_NAME = SearchMessage.class.getAnnotation(DatabaseTable.class).tableName();
	public static final String BMC_ITEM_TABLE_NAME = BmcItem.class.getAnnotation(DatabaseTable.class).tableName();

	private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);


	static {
		sUriMatcher.addURI(AUTHORITY, CARDS_TABLE_NAME, CARDS);
		sUriMatcher.addURI(AUTHORITY, NOTIFICATION_TABLE_NAME, NOTIFICATION);
		sUriMatcher.addURI(AUTHORITY, ADVERT_TABLE_NAME, ADVERT);
		sUriMatcher.addURI(AUTHORITY, FAVORITES_TABLE_NAME, FAVORITES);
		sUriMatcher.addURI(AUTHORITY, HISTORY_TABLE_NAME, HISTORY);
		sUriMatcher.addURI(AUTHORITY, ALERT_TABLE_NAME, ALERT);
		sUriMatcher.addURI(AUTHORITY, RECENT_LOCATION_TABLE_NAME, RECENT_LOCATION);
		sUriMatcher.addURI(AUTHORITY, RELATED_TABLE_NAME, RELATED);
		sUriMatcher.addURI(AUTHORITY, RELATED_ITEM_TABLE_NAME, RELATED_ITEM);
		sUriMatcher.addURI(AUTHORITY, RELATED_FILTER_ITEM_TABLE_NAME, RELATED_FILTER_ITEM);
		sUriMatcher.addURI(AUTHORITY, RELATED_VIRTUAL_TABLE_NAME, RELATED_VIRTUAL);
		sUriMatcher.addURI(AUTHORITY, RECENT_PHRASE_TABLE_NAME, RECENT_PHRASE);
		sUriMatcher.addURI(AUTHORITY, SEARCH_MESSAGE_TABLE_NAME, SEARCH_MESSAGE);
		sUriMatcher.addURI(AUTHORITY, BMC_ITEM_TABLE_NAME, BMC_ITEM);
	}

	public static Uri getContentUri(String name) {
		return Uri.parse(String.format("%s%s/%s", CONTENT, AUTHORITY, name));
	}

	public static final Uri CARDS_CONTENT_URI = getContentUri(CARDS_TABLE_NAME);
	public static final Uri NOTIFICATION_CONTENT_URI = getContentUri(NOTIFICATION_TABLE_NAME);
	public static final Uri ADVERT_CONTENT_URI = getContentUri(ADVERT_TABLE_NAME);
	public static final Uri FAVORITES_CONTENT_URI = getContentUri(FAVORITES_TABLE_NAME);
	public static final Uri HISTORY_CONTENT_URI = getContentUri(HISTORY_TABLE_NAME);
	public static final Uri ALERTS_CONTENT_URI = getContentUri(ALERT_TABLE_NAME);
	public static final Uri RECENT_LOCATION_CONTENT_URI = getContentUri(RECENT_LOCATION_TABLE_NAME);
	public static final Uri RECENT_PHRASE_CONTENT_URI = getContentUri(RECENT_PHRASE_TABLE_NAME);
	public static final Uri RELATED_CONTENT_URI = getContentUri(RELATED_TABLE_NAME);
	public static final Uri RELATED_ITEM_CONTENT_URI = getContentUri(RELATED_ITEM_TABLE_NAME);
	public static final Uri RELATED_FILTER_ITEM_CONTENT_URI = getContentUri(RELATED_FILTER_ITEM_TABLE_NAME);
	public static final Uri RELATED_VIRTUAL_CONTENT_URI = getContentUri(RELATED_VIRTUAL_TABLE_NAME);
	public static final Uri SEARCH_MESSAGE_CONTENT_URI = getContentUri(SEARCH_MESSAGE_TABLE_NAME);
	public static final Uri BMC_ITEM_CONTENT_URI = getContentUri(BMC_ITEM_TABLE_NAME);

	@Inject ITablesDataBaseProvider dataBaseMaster;

	@Override
	public boolean onCreate() {
		return true;
	}

	private ITablesDataBaseProvider getDataBaseMaster() {
		if(dataBaseMaster==null)
			App.getApplicationComponent().inject(this);
		return dataBaseMaster;
	}

	@Nullable
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		switch(sUriMatcher.match(uri)) {
			case RELATED_VIRTUAL:
				return getDataBaseMaster().getChatDatabaseHelper().getReadableDatabase().rawQuery(
//						"SELECT *  FROM related JOIN related_items ON related_items.parent_id==related.id JOIN related_filters ON related_filters.parent_id==related_items.id", null);
			"SELECT title_item, type_filters, value_filters FROM related_items JOIN related_filters ON related_filters.parent_id_filters==related_items.id_item WHERE related_items.type_item==?", selectionArgs);
			default:
				return getDataBaseMaster().query(uri.getLastPathSegment(), projection, selection, selectionArgs, sortOrder);
		}
	}

	public Cursor getBmcItem(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		return getDataBaseMaster().query(uri.getLastPathSegment(), projection, selection, selectionArgs, sortOrder);
	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Nullable
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		long id=-1;
		switch(sUriMatcher.match(uri)) {
			default:
				id=getDataBaseMaster().insert(values, uri.getLastPathSegment());
				break;
		}
		if(id!=-1)
			getContext().getContentResolver().notifyChange(uri, null);
		return uri.buildUpon().appendPath(String.valueOf(id)).build();
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		int updateCount=0;
		switch(sUriMatcher.match(uri)){
			case ALERT:
				// disable notifyChange for ALERT update
				updateCount = getDataBaseMaster().update(values, selection, selectionArgs, uri.getLastPathSegment());
				return updateCount;
			default:
				updateCount = getDataBaseMaster().update(values, selection, selectionArgs, uri.getLastPathSegment());
				break;
		}
		if(updateCount>0)
			getContext().getContentResolver().notifyChange(uri, null);
		return updateCount;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int deleteCount=0;
		int match = sUriMatcher.match(uri);
		switch(match) {
			default:
				deleteCount=getDataBaseMaster().delete(selection, selectionArgs, uri.getLastPathSegment());
				break;
		}
		switch(match) {
			case NOTIFICATION:
				break;
			default:
				if (deleteCount > 0)
					getContext().getContentResolver().notifyChange(uri, null);
		}
		return deleteCount;

	}

	@Override
	public int bulkInsert(Uri uri, ContentValues[] values) {
		int insertCount=0;
		switch(sUriMatcher.match(uri)) {
//			case MESSAGE:
//				insertCount=getDataBaseMaster().bulkInsert(values, uri.getLastPathSegment(), ChatMessageModel.MESSAGE_ID);
//				break;
			default:
//				return super.bulkInsert(uri, values);
				SQLiteDatabase db = getDataBaseMaster().getChatDatabaseHelper().getWritableDatabase();
				try{
					db.beginTransaction();
					for (ContentValues value : values) {
						try {
							long insertId = db.insertWithOnConflict(uri.getLastPathSegment(), null, value, SQLiteDatabase.CONFLICT_IGNORE);
							if (insertId == -1) {
								insertCount--;
//							final Object o = value.get(XmppDatabaseHelper.USER_ID);
//							if(o!=null) {
//								removeOddNickName(value);
//								final int updateCount=db.update(XmppDatabaseHelper.TABLE_FRIENDS, value,
//										String.format("%s=?", XmppDatabaseHelper.USER_ID), new String[]{o.toString()});
//								if(updateCount==1) {
//									resultCount++;
//								}
//							}
							}
							insertCount++;
						}catch (Exception e){
							Timber.e(e);
						}

					}
					db.setTransactionSuccessful();
				} finally {
					db.endTransaction();
				}
				break;
		}
		if(insertCount>0)
			getContext().getContentResolver().notifyChange(uri, null);
		return insertCount;
	}

}
