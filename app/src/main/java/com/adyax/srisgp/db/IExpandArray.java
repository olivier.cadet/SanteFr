package com.adyax.srisgp.db;

import android.database.Cursor;

/**
 * Created by SUVOROV on 10/26/16.
 */

// TODO it need to change in the future
public interface IExpandArray {
    void collapse();
    void expand(Cursor cursor);
}
