package com.adyax.srisgp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


/**
 * Created by Sergii Suvorov AKA CYBOPOB on 05.09.2015.
 */
public class DataBaseMaster implements ITablesDataBaseProvider {

    @Override
    public DatabaseHelper getChatDatabaseHelper() {
        return chatDatabaseHelper;
    }

    private DatabaseHelper chatDatabaseHelper;

    public DataBaseMaster(DatabaseHelper chatDatabaseHelper) {
        this.chatDatabaseHelper = chatDatabaseHelper;
    }

    @Override
    public Cursor query(String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return chatDatabaseHelper.getReadableDatabase().query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Override
    public long insert(ContentValues values, String tableName) {
        return chatDatabaseHelper.getWritableDatabase().insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public int delete(String selection, String[] selectionArgs, String tableName) {
        return chatDatabaseHelper.getWritableDatabase().delete(tableName, selection, selectionArgs);
    }

    @Override
    public int update(ContentValues values, String selection, String[] selectionArgs, String tableName) {
        return chatDatabaseHelper.getWritableDatabase().update(tableName, values, selection, selectionArgs);
    }

    @Override
    public long insertUpdateConflict(ContentValues values, String tableName, String nameKeyId) {
        final SQLiteDatabase writableDatabase = chatDatabaseHelper.getWritableDatabase();
        long id = writableDatabase.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1 && values.containsKey(nameKeyId))
            id = writableDatabase.update(tableName, values, String.format("%s==?", nameKeyId), new String[]{values.get(nameKeyId).toString()});
        return id;
    }

    @Override
    public int bulkInsert(ContentValues[] values, String tableName, String nameKeyId) {
        int resultCount = 0;
//        final SQLiteDatabase db = chatDatabaseHelper.getWritableDatabase();
//        try {
//            db.beginTransaction();
//            for (ContentValues value : values) {
//                long insertId = -1;
//                if (value.containsKey(nameKeyId)) {
//                    Boolean bSaveMessageRead = null;
//                    if (value.containsKey(ChatMessageModel.MESSAGE_READ)) {
//                        bSaveMessageRead = value.getAsBoolean(ChatMessageModel.MESSAGE_READ);
//                        if (!bSaveMessageRead) {
//                            value.remove(ChatMessageModel.MESSAGE_READ);
//                        } else
//                            bSaveMessageRead = null;
//                    }
//                    insertId = db.updateWithOnConflict(tableName, value,
//                            String.format("%s==?", nameKeyId), new String[]{value.get(nameKeyId).toString()}, SQLiteDatabase.CONFLICT_IGNORE);
//                    if (insertId == 0 && bSaveMessageRead != null)
//                        value.put(ChatMessageModel.MESSAGE_READ, bSaveMessageRead);
//                }
//                if (insertId == 0) {
//                    insertId = db.insertWithOnConflict(tableName, null, value, SQLiteDatabase.CONFLICT_IGNORE);
//                    if (insertId != -1)
//                        resultCount++;
//                }
//            }
//            db.setTransactionSuccessful();
//        } finally {
//            db.endTransaction();
//        }
        return resultCount;
    }
}
