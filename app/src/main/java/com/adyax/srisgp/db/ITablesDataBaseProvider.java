package com.adyax.srisgp.db;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by SUVOROV on 3/25/16.
 */
public interface ITablesDataBaseProvider {
    DatabaseHelper getChatDatabaseHelper();
    Cursor query(String tableName, String[] projection, String selection, String[] selectionArgs, String sortOrder);
    long insert(ContentValues values, String tableName);
    int delete(String selection, String[] selectionArgs, String tableName);
    int update(ContentValues values, String selection, String[] selectionArgs, String tableName);
    long insertUpdateConflict(ContentValues values, String lastPathSegment, String chatId);
    int bulkInsert(ContentValues[] values, String tableName, String nameKeyId);
}
