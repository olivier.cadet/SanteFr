package com.adyax.srisgp.mvp.searchresults;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.adyax.srisgp.mvp.searchresults.info.InfoSearchResultsFragment;

/**
 * Created by anton.kobylianskiy on 10/11/16.
 */

public class SearchResultsPagerAdapter extends FragmentStatePagerAdapter {
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private Context context;
    private SearchResultsFactory factory;

    public SearchResultsPagerAdapter(SearchResultsFactory factory, FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        this.factory = factory;
    }

    @Override
    public Fragment getItem(int position) {
        return factory.create(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public int getItemPosition(Object object) {
        if (!(object instanceof InfoSearchResultsFragment)) {
            return POSITION_NONE;
        }

        return super.getItemPosition(object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    public interface SearchResultsFactory {
        Fragment create(int position);
    }
}
