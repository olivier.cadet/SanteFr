package com.adyax.srisgp.mvp.emergency;

import android.os.Bundle;

import androidx.annotation.NonNull;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class EmergencyPresenter extends Presenter<EmergencyPresenter.EmergencyView> implements AppReceiver.AppReceiverCallback {

    protected IRepository repository;
    private AppReceiver appReceiver = new AppReceiver();

    private EmergencyArg emergencyArg;
    private TrackerHelper trackerHelper;

    public interface EmergencyView extends IView {
        void showProgress();

        void hideProgress();

        void closeFragment();

        void showMessage(String errorMessage);
    }

    @Inject
    public EmergencyPresenter(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public void attachView(@NonNull EmergencyPresenter.EmergencyView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void clickSend(EmergencyCommand emergencyCommand) {
        getView().showProgress();
        // Attention! Must be before repository.saveVote(emergencyCommand);
        final ClickType clickType = getVoteClick(repository.isVote(getEmergencyArg()),
                emergencyCommand.getValue().intValue());
        repository.getVote(getEmergencyArg());
        repository.saveVote(emergencyCommand);
        if (trackerHelper != null) {
            trackerHelper.clickSignaler(TrackerLevel.UNIT_CONTENTS_FIND, clickType);
        }
//        emergencyCommand.setDebug();
        ExecutionService.sendCommand(getContext(), appReceiver,
                emergencyCommand, ExecutionService.SEND_EMERGENCY);
    }

    private ClickType getVoteClick(boolean isVote, int valueIndex) {
        if (!isVote) {
            switch (valueIndex) {
                case 2:
                    return ClickType.clic_premier_attente_urgences_modere;
                case 3:
                    return ClickType.clic_premier_attente_urgences_fort;
            }

        } else {
            switch (valueIndex) {
                case 1:
                    return ClickType.clic_attente_urgence_faible;
                case 2:
                    return ClickType.clic_attente_urgence_modere;
                case 3:
                    return ClickType.clic_attente_urgence_fort;
            }
        }
        return ClickType.UNKNOWN;
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.SEND_EMERGENCY) {
            getView().closeFragment();
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == ExecutionService.SEND_EMERGENCY) {
            getView().hideProgress();//{"form_name":{"error_code":406044,"error_message":"Flood control"}}
            if (BuildConfig.DEBUG) {
                getView().closeFragment();
            } else {
                getView().showMessage(errorMessage.getErrorMessage());
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public IRepository getRepository() {
        return repository;
    }

    public EmergencyArg getEmergencyArg() {
        return emergencyArg;
    }

    public void initEmergencyArg(EmergencyArg emergencyArg) {
        this.emergencyArg = emergencyArg;
        trackerHelper = new TrackerHelper(emergencyArg.getAnchorResponse());
    }

    public EmergencyCommand getEmergencyCommand(String service, String value) {
        return new EmergencyCommand(getEmergencyArg().getNid(),
                getEmergencyArg().getServiceIndex(service),
                getEmergencyArg().getValueIndex(value));
    }

}
