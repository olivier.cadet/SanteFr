package com.adyax.srisgp.mvp.favorite;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import java.util.List;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public class YourFavoritesPresenter extends Presenter<YourFavoritesPresenter.YourFavoritesView>
        implements AppReceiver.AppReceiverCallback {

    private AppReceiver appReceiver = new AppReceiver();

    public YourFavoritesPresenter() {
    }

    @Override
    public void attachView(@NonNull YourFavoritesPresenter.YourFavoritesView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void onFavoriteRemove(List<Long> itemsToRemove, IGetUrl getUrl) {
        AddOrRemoveFavoriteCommand command =
                new AddOrRemoveFavoriteCommand(TrackerLevel.FAVORITES, itemsToRemove, AddOrRemoveFavoriteCommand.ACTION_REMOVE, getUrl);
        ExecutionService.sendCommand(getContext(), appReceiver, command, ExecutionService.FAVORITE_ACTION);
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == ExecutionService.FAVORITE_ACTION) {

        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.FAVORITE_ACTION) {
            AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
            if (getView() != null) {
                for (int i = 0; i < command.getNids().size(); i++) {
                    getView().onRemoved(command.getNids().get(i));
                }

                getView().onCloseEditMode();
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public interface YourFavoritesView extends IView {
        void onRemoved(long removedId);

        void onCloseEditMode();
    }
}
