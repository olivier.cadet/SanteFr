package com.adyax.srisgp.mvp.history;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.GetHistoryCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.GetHistoryResponse;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class HistoriesPresenter extends Presenter<HistoriesPresenter.HistoriesView>
        implements AppReceiver.AppReceiverCallback {

    private static final int COMMAND_HISTORIES = 0;
    private static final int COMMAND_REMOVE = 1;

    private AppReceiver appReceiver = new AppReceiver();

    private boolean hasMore = true;
    private HistoryType historyType;
    private FragmentFactory fragmentFactory;

    @Inject
    public HistoriesPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(@NonNull HistoriesPresenter.HistoriesView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void onItemClicked(HistoryItem item) {
        if (item == null)
            return;
        new TrackerHelper(item).clickForWriteToHistory();
        switch (item.getNodeType()) {
            case APPLICATIONS:
            case NUMERO_TEL:
            case LIENS_EXTERNES:
                getView().openHistoryScreen(item);
                break;
            default:
//                fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.HISTORICAL), -1);
                new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.HISTORICAL);
                break;
        }
    }


    public void setType(HistoryType historyType) {
        this.historyType = historyType;
    }

    public void clickItem(HistoryItem item) {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.HISTORICAL), -1);
    }

    /**
     * Requests
     */

    public void requestHistories(int offset) {
        if (hasMore || offset == 0) {
            GetHistoryCommand command = new GetHistoryCommand(offset, historyType);
            ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_HISTORIES);

            if (offset == 0) {
                if (getView() != null) {
                    getView().resetHistories();
                }
            }
        }
    }

//    public void removeHistoryItem(long item, HistoryType type) {
//        ArrayList<Long> ids = new ArrayList<>();
//        ids.add(item);
//        ClearHistoryCommand command = new ClearHistoryCommand(ids, type);
//        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_REMOVE);
//    }

    /**
     * AppReceiverCallback
     */

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_HISTORIES) {
            GetHistoryResponse response = data.getParcelable(CommandExecutor.BUNDLE_HISTORIES);

            if (response.items != null) {
                hasMore = response.hasMore();
            } else {
                hasMore = false;
            }

            if (getView() != null) {
                getView().showHistories(response);
            }
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public interface HistoriesView extends IView {
        void openHistoryScreen(HistoryItem item);

        void showHistories(GetHistoryResponse response);

        void showEmptyView();

        void resetHistories();
    }
}
