package com.adyax.srisgp.mvp.widget;


import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.UrlUtility;

import org.json.JSONObject;

import java.util.HashMap;


public class WidgetViewHelper {

    public static final int REQUEST_WIDGET = 9876;
    public static final String SHOW_AROUNDME_RESPONSE = "SHOW_AROUNDME_RESPONSE";

    // Location IDS.
    public static final int LOCATION_HOME = 2;
    public static final int LOCATION_INFO = 3;
    public static final int LOCATION_FIND = 4;

    // Widget view Contexts
    static Boolean widgetVisibilityDONE = false;
    static Boolean widgetVisibilityHome = false;
    static Boolean widgetVisibilityInfo = false;
    static Boolean widgetVisibilityFind = false;

    int widgetLocation = 0;

    static JSONObject widgetJSON = null;
    static HashMap<String, String> widgetData = new HashMap<>();

    public WidgetViewHelper(int widgetLocation) {
        this.widgetLocation = widgetLocation;

        if (widgetVisibilityDONE == false) {
            JSONObject json_widget = getWidgetJSON(false);
            try {
                if (json_widget.getJSONObject("field_widget_visibility_home").get("#value").toString().equals("1")) {
                    widgetVisibilityHome = true;
                }
                if (json_widget.getJSONObject("field_widget_visibility_info").get("#value").toString().equals("1")) {
                    widgetVisibilityInfo = true;
                }
                if (json_widget.getJSONObject("field_widget_visibility_find").get("#value").toString().equals("1")) {
                    widgetVisibilityFind = true;
                }
                widgetVisibilityDONE = true;
            } catch (Exception e) {
            }
        }

    }

    /**
     * Init data.
     */
    public static void init() {
        widgetJSON = null;
        widgetData = new HashMap<>();
    }

    // Get widget visibility of a location.
    public Boolean getVisibility() {
        if (this.widgetLocation == LOCATION_HOME) {
            return widgetVisibilityHome;
        } else if (this.widgetLocation == LOCATION_INFO) {
            return widgetVisibilityInfo;
        } else if (this.widgetLocation == LOCATION_FIND) {
            return widgetVisibilityFind;
        }
        return false;
    }


    public static JSONObject getWidgetJSON(Boolean update) {
        if (update || widgetJSON == null) {
            String params = "?";
            if (widgetData.get("age") != null) {
                params += "&age=" + widgetData.get("age");
            }
            if (widgetData.get("sexe") != null) {
                params += "&sexe=" + widgetData.get("sexe");
            }
            if (widgetData.get("sous_cas") != null) {
                params += "&sous_cas=" + widgetData.get("sous_cas");
            }

            // Cache to avoid multiple url calls.
            String cache_params = widgetData.get("cache_params");
            if (cache_params == null || !cache_params.equals(params) || widgetJSON == null) {
                UrlUtility urlutility = new UrlUtility(UrlExtras.getBaseURL() + UrlExtras.API_WIDGET_FORM + params);
                urlutility.setBasicAuth(BuildConfig.CREDENTIAL_SERVER);
                widgetJSON = urlutility.readUrlJson();
                widgetData.put("cache_params", params);
            }
        }
        return widgetJSON;
    }

}
