package com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.adyax.srisgp.data.repository.IRepository;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by SUVOROV on 10/20/16.
 */

public class GeoHelper implements IGeoHelper {

    public static final int REQUEST_LOCATION_PERMISSION = 3;
    public static final int REQUEST_CHECK_SETTINGS = 2;

    final private Context context;
    private IRepository repository;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private boolean bFinished = false;
    private boolean bGetLocation = false;

    private LocationListener onLocationListener;

    public GeoHelper(Context context, IRepository repository) {
        this.context = context;
        this.repository = repository;
    }

    private Context getContext() {
        return context.getApplicationContext();
    }

    public synchronized void onRequestCurrentLocation(Activity activity) {
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO Afficher une modal ou autre pour accepter les perms
                                return;
                            }
                            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                            repository.saveGeoLocation(location);//-
                            if (location == null) {
                                // Begin polling for new location updates.
                                startLocationUpdates(activity);
                            } else {
                                if (onLocationListener != null) {
                                    onLocationListener.onLocationChanged(location);
                                }
                            }
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .build();
            googleApiClient.connect();
        }
//        else if (aroundMe) {
//            Location location = getLastLocation();
//            setCurrentCityByLocation(location);
//        }
    }

    private void startLocationUpdates(Activity activity) {
        // Create the location request
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(1000);
        // Request location updates

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        requestLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        onPromptLocation(activity, status);
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (googleApiClient != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, locationRequest, locationListener);
        }
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            disconnect();
        }
    };

    @Override
    public synchronized void disconnect() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
            googleApiClient = null;
        }
    }

    @Override
    public boolean isFinished() {
        final boolean bResult = bFinished;
        bFinished = false;
        return bResult;
    }

    @Override
    public void setFinished() {
        bFinished = true;
    }

    @Override
    public boolean isShowGeoRequest() {
        if (!hasPermissionsForRequestingLocation()) {
            return false;
        }
        if (!hasLocationPermissions()) {
            return false;
        }
        return true;
    }

    private boolean hasPermissionsForRequestingLocation() {
        return ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasLocationPermissions() {
        LocationManager service = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.NETWORK_PROVIDER) /*&& hasPermissionsForRequestingLocation()*/;
    }

    @Override
    public boolean askPermission(Activity activity) {
        if (!hasPermissionsForRequestingLocation()) {
            requestLocationPermissions(activity);
        } else {
            onRequestCurrentLocation(activity);
            if (hasLocationPermissions()) {
                if (!bGetLocation) {
                    setFinished();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean askGeo(Activity activity) {
        if (hasPermissionsForRequestingLocation()) {
            onRequestCurrentLocation(activity);
            if (hasLocationPermissions()) {
                if (!bGetLocation) {
                    setFinished();
                    return true;
                }
            }
        }
        return false;
    }

    private void requestLocationPermissions(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }
    }

    private void onPromptLocation(Activity activity, Status status) {
        try {
            status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnLocationListener(LocationListener onLocationListener) {
        this.onLocationListener = onLocationListener;
        String geoLocation2 = null;
    }

    public static Location geoStringToLocation(String geoLocationStr) {
        try {
            if (geoLocationStr != null) {
                String[] split = geoLocationStr.split(",");
                if (split.length == 3) {
                    Location result = new Location("network");
                    result.setLatitude(Double.parseDouble(split[0]));
                    result.setLongitude(Double.parseDouble(split[1]));
                    return result;
                }
            }
        } catch (Exception e) {

        }
        return null;
    }

}
