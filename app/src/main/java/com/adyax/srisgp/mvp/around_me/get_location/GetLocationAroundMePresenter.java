package com.adyax.srisgp.mvp.around_me.get_location;

import android.location.Location;

import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.LocationPresenter;
import com.adyax.srisgp.mvp.LocationView;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/31/16.
 */

public class GetLocationAroundMePresenter extends LocationPresenter<GetLocationAroundMePresenter.GetLocationAroundMeView>{

    private FragmentFactory fragmentFactory;

    @Inject
    public GetLocationAroundMePresenter(FragmentFactory fragmentFactory, IRepository repository) {
        super(repository);
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    protected boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch) {
        getView().setCurrentLocation(location);
        return false;
    }

    public void startAroundMe(AroundMeArguments aroundMeArguments) {
        fragmentFactory.startAroundMeFragment(getContext(), aroundMeArguments);
    }

    public interface GetLocationAroundMeView extends LocationView {
        void setCurrentLocation(Location location);
    }
}
