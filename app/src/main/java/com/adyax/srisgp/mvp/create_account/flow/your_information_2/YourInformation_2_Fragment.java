package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.databinding.FragmentYoursInformation2Binding;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationActivity;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.DialogUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class YourInformation_2_Fragment extends ViewFragment implements YourInformation_2_Presenter.CreateRecipeView {
    private static final int REQUEST_LOCATION = 1343;
    public static final int THRESHOLD = 3;

    public static final String GENDER_KEY = "gender";
    public static final String YEAR_KEY = "year";

    @Inject
    YourInformation_2_Presenter presenter;

    private FragmentYoursInformation2Binding binding;
    private CitiesResponse citiesResponse;
    private CityResult cityResult = new CityResult(null, 0);

    public static Fragment newInstance() {
        YourInformation_2_Fragment fragment = new YourInformation_2_Fragment();
        return fragment;
    }

    public static Fragment newInstance(String gender, String year) {
        YourInformation_2_Fragment fragment = new YourInformation_2_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(GENDER_KEY, gender);
        bundle.putString(YEAR_KEY, year);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                cityResult = data.getParcelableExtra(ProvideLocationFragment.INTENT_CITY);
                if (!cityResult.isEmpty()) {
                    binding.postalCode.setText(cityResult.getTitle());
                }
                testValidateButton();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        testValidateButton();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_yours_information_2, container, false);
        binding.ageCreateAccount.setObjects(YearsHelper.getYearsArray());//GenderType
        binding.yourSexCreateAccount.setObjects(GenderType.getStringArray(getContext()).toArray(new String[GenderType.getSize()]));
        binding.yourSexCreateAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                testValidateButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.ageCreateAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                testValidateButton();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (getArguments() != null && getArguments().containsKey(GENDER_KEY)) {
            final int selection = GenderType.getOrdinal(getContext(), getArguments().getString(GENDER_KEY));
            if (selection >= 0 && selection < GenderType.getSize()) {
                binding.yourSexCreateAccount.setSelection(selection);
            }
        }

        if (getArguments() != null && getArguments().containsKey(YEAR_KEY)) {
            int selection = YearsHelper.getPosition(getArguments().getString(YEAR_KEY));
            if (selection >= 0 && selection < YearsHelper.getYearsArray().length)
                binding.ageCreateAccount.setSelection(selection);
        }

        binding.validateCreateAccount.setOnClickListener(view -> {
            IViewPager activity = (IViewPager) getActivity();
            GenderType gender = GenderType.valueOf(getContext(), binding.yourSexCreateAccount.getValue());

            if (gender != GenderType.empty) {
                String age = binding.ageCreateAccount.getValue();
                if (age != null) {
                    Long postalId = getPostCode();
                    if (postalId == null) {
//                binding.postalCode.setError(getString(errStr));
                        binding.postalCode.requestFocus();
                    } else {
                        if (activity.getCreateAccountHelper().validateSex(gender, age, postalId)) {
                            binding.progressLayout.setVisibility(View.VISIBLE);
                            presenter.sendFakeRegistration();
//                activity.moveToNext();
                        }
                    }
                } else {
                    //                binding.ageCreateAccount.setError(getString(errStr));
                    binding.ageCreateAccount.requestFocus();
                }
            } else {
                //                binding.yourSexCreateAccount.setError(getString(errStr));
                binding.yourSexCreateAccount.requestFocus();
            }
        });

//        binding.bottInf.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.bottInf, true);

        binding.postalCode.setOnClickListener(v -> {
            Intent intent = ProvideLocationActivity.newIntent(getContext());
            getActivity().startActivityForResult(intent, REQUEST_LOCATION);
            getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        });
        return binding.getRoot();
    }

    private void testValidateButton() {
        final boolean bValidate = (!binding.yourSexCreateAccount.isEmpty() &&
                !binding.ageCreateAccount.isEmpty() && getPostCode() != null && !binding.postalCode.getText().toString().isEmpty());
        binding.validateCreateAccount.setEnabled(bValidate);
    }

    private Long getPostCode() {
        if (citiesResponse != null)
            return citiesResponse.getId(binding.postalCode.getText().toString());
        return cityResult.getPostalCode();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.registration);
    }

    @NonNull
    @Override
    public YourInformation_2_Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void updateCities(CitiesResponse citiesResponse) {
        this.citiesResponse = citiesResponse;
        // TODO need to throw out citiesResponse.getCites()
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_dropdown_item_1line, citiesResponse.getCites());
        ((AutoCompleteTextView) binding.postalCode).setAdapter(adapter);
        ((AutoCompleteTextView) binding.postalCode).showDropDown();
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        binding.progressLayout.setVisibility(View.INVISIBLE);
        if (BuildConfig.DEBUG) {
            if (errorResponse.isAnyError()) {
                showMessage(errorResponse.getErrorMessage(null));
            }
        }else{
            if (errorResponse.isSSOError()){
                DialogUtils.showBadSSO(getContext(),
                        errorResponse.sso_client_id.getLocalizeErrorMessage(getContext()));
            }
        }
    }

    @Override
    public void onSuccess() {
//        IViewPager activity = (IViewPager) getActivity();
//        activity.moveToNext();
        presenter.moveToNext();
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }
}
