package com.adyax.srisgp.mvp.favorite;

import android.os.Bundle;

import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import javax.inject.Inject;

public class FavoritePresenter extends PresenterAppReceiver<FavoritePresenter.FavoriteView> {

    public interface FavoriteView extends IView {
        void updateFavoriteStatus(boolean favorite);
    }

    private FragmentFactory fragmentFactory;

    @Inject
    public FavoritePresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    public void downloadClicked(FavoritesItem item) {
        if (getView() != null) {
//            ViewUtils.showAppsDialog(getContext(), item.appStoreLink, item.playStoreLink);
            new TrackerHelper(item).showAppsDialog(getContext(), null);
        }
    }

    public void onFavoriteClicked(FavoritesItem item, boolean favorite) {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new AddOrRemoveFavoriteCommand(TrackerLevel.FAVORITES, item.nid, !favorite ? AddOrRemoveFavoriteCommand.ACTION_ADD :
                        AddOrRemoveFavoriteCommand.ACTION_REMOVE, item), ExecutionService.FAVORITE_ACTION);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.FAVORITE_ACTION) {
            if (getView() != null) {
                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                if (command != null && command.getNids() != null) {
                    if (getView() != null) {
                        getView().updateFavoriteStatus(command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD));
                    }
                }
            }
        }
    }

    public void readClicked(FavoritesItem item) {
//        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.FAVORITES), -1);
        new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.FAVORITES);
    }
}
