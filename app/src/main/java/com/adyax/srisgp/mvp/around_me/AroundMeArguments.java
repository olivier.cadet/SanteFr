package com.adyax.srisgp.mvp.around_me;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.tracker.InternalSearchTrackerHelper;

/**
 * Created by SUVOROV on 10/27/16.
 */

public class AroundMeArguments implements Parcelable {

    public static final String BUNDLE_NAME = "AroundMeArguments";
    public static final double MIN_DOUBLE = 0.000001;

    public static final float TOURS_LATITUDE = 47.39625761F;
    public static final float TOURS_LONGITUDE = 0.68492889F;

    public static final float PARIS_LATITUDE = 48.8567879F;
    public static final float PARIS_LONGITUDE = 2.3510768F;

    public static final float KHARKIV_LATITUDE = 50.0031093F;
    public static final float KHARKIV_LONGITUDE = 36.2409482F;

    public static final float CENTRE_OF_FRANCE_LATITUDE = TOURS_LATITUDE;
    public static final float CENTRE_OF_FRANCE_LONGITUDE = TOURS_LONGITUDE;

    private SearchType searchType;

    private GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation = GetAutoCompleteSearchLocationsResponse.StatusLocation.createEmpty();
    private String city;
    private double longitude2;
    private double latitude2;
    private boolean hasMoreItems;
    private boolean sortedByDistance;


    private SearchCommand searchCommand;

    public AroundMeArguments(SearchType searchType, double longitude, double latitude) {
        setLocation(latitude, longitude);
        this.searchType = searchType;
    }

    public AroundMeArguments(SearchType searchType, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        this.searchType = searchType;
        this.statusLocation = statusLocation;
        this.city = statusLocation.getLocation();
    }

    public AroundMeArguments(SearchType searchType, String city) {
        this.searchType = searchType;
        this.city = city;
    }

    public AroundMeArguments(SearchType searchType, String city, double longitude, double latitude) {
        this.searchType = searchType;
        this.city = city;
        setLocation(latitude, longitude);
    }

    public AroundMeArguments(SearchType searchType, Location location) {
        this(searchType, location.getLongitude(), location.getLatitude());
    }

    public AroundMeArguments(SearchType searchType) {
        this.searchType = searchType;
    }

    public AroundMeArguments(SearchCommand searchCommand, boolean sortedByDistance, boolean hasMoreItems) {
        this.searchCommand = searchCommand;
        this.sortedByDistance = sortedByDistance;
        this.hasMoreItems = hasMoreItems;
        if (searchCommand.getCityLocation() != null) {
            this.statusLocation = new GetAutoCompleteSearchLocationsResponse.StatusLocation(searchCommand.getCityLocation());
        }
    }

    public Double getLongitude() {
        if (searchCommand != null) {
            return searchCommand.getLongitude();
        }
        return longitude2;
    }

    public Double getLatitude() {
        if (searchCommand != null) {
            return searchCommand.getLatitude();
        }
        return latitude2;
    }

    public SearchType getType() {
        if (searchCommand != null) {
            return searchCommand.getType();
        }
        return searchType;
    }

    public String getCity() {
        if (searchCommand != null && searchCommand.getBbox() == null) {
            return searchCommand.getCityLocation();
        }
        return city;
    }

    public boolean isCity() {
//        return !TextUtils.isEmpty(city);
        return !TextUtils.isEmpty(getCity());
    }

    public boolean isSearchInPosition() {
        return searchCommand != null && searchCommand.isBbox();
    }

    public AroundMeArguments(Parcel in) {
        city = in.readString();
        longitude2 = in.readDouble();
        latitude2 = in.readDouble();
        searchType = SearchType.valueOf(in.readString());
        searchCommand = in.readParcelable(SearchCommand.class.getClassLoader());
        statusLocation = in.readParcelable(GetAutoCompleteSearchLocationsResponse.StatusLocation.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city);
        dest.writeDouble(longitude2);
        dest.writeDouble(latitude2);
        if (searchType != null)
            dest.writeString(searchType.name());
        else
            dest.writeString(SearchType.unknown.name());
        dest.writeParcelable(searchCommand, flags);
        dest.writeParcelable(statusLocation, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AroundMeArguments> CREATOR = new Creator<AroundMeArguments>() {
        @Override
        public AroundMeArguments createFromParcel(Parcel in) {
            return new AroundMeArguments(in);
        }

        @Override
        public AroundMeArguments[] newArray(int size) {
            return new AroundMeArguments[size];
        }
    };

    public boolean isAnyLocation() {
        return isCity() || isGeoLocation();
    }

    public boolean isGeoLocation() {
        if (searchCommand != null) {
            if (searchCommand.isGeoLocation()/*|| searchCommand.isAroundGeo()*/) {
                return true;
            }
//            return false;
        }
        return isLocationAlive();//AAA
    }

    public void setLocation(Location location) {
//        latitude2 = location.getLatitude();//AAA
//        longitude2 = location.getLongitude();
        setLocation(location.getLatitude(), location.getLongitude());
        if (searchCommand != null) {
            searchCommand.setGeo(location.getLatitude(), location.getLongitude());
        }
    }

    public void setLocation(double latitude, double longitude) {
        this.latitude2 = latitude;//AAA
        this.longitude2 = longitude;
//        if(statusLocation.isEmpty()&& isLocationAlive()){
//            statusLocation.setActive();
//        }
    }

    public Location getMyLocationNew() {
        if (searchCommand != null && searchCommand.isAroundGeo()) {//***2 ***5
            return searchCommand.getAroundLocation();
        }
        if (isLocationAlive()) {
            Location result = new Location("network");
            result.setLatitude(latitude2);
            result.setLongitude(longitude2);
            return result;
        }
        if (searchCommand != null && searchCommand.isGeoLocation()) {
            return searchCommand.getMyLocation();
        }// AAA
        return null;
    }

    public Location getMyLocation() {
        if (searchCommand != null && searchCommand.isGeoLocation()) {
            return searchCommand.getMyLocation();
        }
        if (isLocationAlive()) {
            Location result = new Location("network");
            result.setLatitude(latitude2);
            result.setLongitude(longitude2);
            return result;
        }
        return null;
    }

    public SearchCommand getSearchCommand() {
        return searchCommand;
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
    }

    public boolean hasMoreItems() {
        return hasMoreItems;
    }

    public boolean isSortedByDistance() {
        return sortedByDistance;
    }

    public void setSortedByDistance(boolean sortedByDistance) {
        this.sortedByDistance = sortedByDistance;
    }

    public void setSearchCommand(SearchCommand searchCommand) {
        this.searchCommand = searchCommand;
        if (this.searchCommand != null && isGeoLocation() && this.searchCommand.getCityLocation() == null) {
            this.searchCommand.setLocation(InternalSearchTrackerHelper.AUTOUR_DE_MOI);
        }
    }

    public String getGeoString(boolean bAroundMe) {
        if (isCity()) {
            return getCity();
        } else {
            if (isGeoLocation()) {
                if (bAroundMe) {
                    return App.getAppContext().getString(R.string.search_around_me);
                } else {
                    return String.format("%f %f", getLatitude(), getLongitude());
                }
            } else {
                return "";
            }
        }
    }

    public void update(SearchResponseHuge searchResponseHuge) {
        if (isCity() && searchResponseHuge.isAroundGeoLocation() && !isLocationAlive()) {
            setLocation(searchResponseHuge.around_lat, searchResponseHuge.around_lon);
            if (searchCommand != null) {
                searchCommand.setGeo(searchResponseHuge.around_lat, searchResponseHuge.around_lon);
            }
        }
    }

    /*
        Attention!!! Works correctly only after calls update
    */
    private boolean isCityPrecision() {
        return city != null && isLocationAlive();
    }

    public boolean isOnlyLocation() {
        return city == null && isLocationAlive();
    }

    private boolean isLocationAlive() {
        return Math.abs(longitude2) > MIN_DOUBLE && Math.abs(latitude2) > MIN_DOUBLE;
    }

    public GetAutoCompleteSearchLocationsResponse.StatusLocation getStatusLocation() {
        return statusLocation;
    }

    public void setStatusLocation(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        this.statusLocation = statusLocation;
    }

}
