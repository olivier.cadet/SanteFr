package com.adyax.srisgp.mvp.search;

import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;

/**
 * Created by anton.kobylianskiy on 9/27/16.
 */

public class RecentLocationViewModel {
    private long id;
    private String location;

    private boolean removed;
    private boolean progress;

    public RecentLocationViewModel(RecentLocationItem searchItem) {
        this.id = searchItem.id;
        this.location = searchItem.location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isProgress() {
        return progress;
    }

    public void setProgress(boolean progress) {
        this.progress = progress;
    }
}
