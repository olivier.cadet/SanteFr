package com.adyax.srisgp.mvp.searchhome;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.UrlUtility;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.SearchItemType;
import com.adyax.srisgp.data.net.response.tags.ImageConfigurationItem;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.QuestionnaireItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;
import com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes.AlertsSwipeFragment;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;
import com.adyax.srisgp.mvp.widget.WidgetViewHolder;
import com.adyax.srisgp.presentation.BaseAnimatorListener;
import com.adyax.srisgp.utils.ImageUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.mapbox.services.api.geocoding.v5.GeocodingCriteria;
import com.mapbox.services.api.geocoding.v5.MapboxGeocoding;
import com.mapbox.services.api.geocoding.v5.models.GeocodingResponse;
import com.mapbox.services.commons.models.Position;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import retrofit2.Call;
import retrofit2.Response;
import ru.rambler.libs.swipe_layout.SwipeLayout;
import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 9/21/16.
 */
public class SearchHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int MAX_CARD_NUMBER = 7;

    private static final int ITEM_HEADER = 0;
    private static final int ITEM_CARD = 1;
    private static final int ITEM_FOOTER = 2;
    private static final int ITEM_NO_CARDS = 3;
    private static final int ITEM_HEADER_RESULTS = 5;
    private static final int ITEM_HEADER_WIDGET = 7;
    private static final int ITEM_HEADER_CLINIC = 9;

    private boolean closedHowWorks = false;

    private OnItemClickedListener itemClickedListener;

    private List<QuickCardsAnonymousItem> items = new ArrayList<>();
    private boolean signedIn;

    private IAlerts alerts;
    private FragmentManager fragmentmanager;

    private IRepository repository;

    private QuickCardsAnonymousItem essaiClinic = null;

    private int offsetHeaderStatic() {
        return essaiClinic != null ? 1 : 0;
    }

    public SearchHomeAdapter(IRepository repository, IAlerts alerts, FragmentManager fragmentmanager, boolean signedIn, OnItemClickedListener onItemClickedListener) {
        this.repository = repository;
        this.alerts = alerts;
        this.fragmentmanager = fragmentmanager;
        this.signedIn = signedIn;
        this.itemClickedListener = onItemClickedListener;
    }

    public void notifyCardChanged(long id, boolean favorite) {
        for (int i = 0; i < items.size(); i++) {
            if (id == items.get(i).nid) {
                items.get(i).setFavorite(favorite);
                int firstCardIndex = getFirstCardIndexInDataset();
                if(firstCardIndex != -1) {
                    notifyItemChanged(firstCardIndex + i);
                }
                break;
            }
        }
    }

    public void notifyLoggedInStateChanged(boolean signedIn) {
        this.signedIn = signedIn;
        notifyItemChanged(getItemCount() - 1);
    }

    @Override
    public int getItemCount() {
        if (items.size() == 0) {
            return 4 + offsetHeaderStatic();
        }

        if (items.size() >= MAX_CARD_NUMBER) {
            return MAX_CARD_NUMBER + 3 + offsetHeaderStatic();
        }
        return items.size() + 3 + offsetHeaderStatic();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_HEADER;
        } else if (position == getItemCount() - 1) {
            return ITEM_FOOTER;
        }

        if (position == 1) {
            return ITEM_HEADER_RESULTS;
        }
        if (position == 2) {
            WidgetViewHelper widgetHelper = new WidgetViewHelper(WidgetViewHelper.LOCATION_HOME);
            if (widgetHelper.getVisibility()) {
                return ITEM_HEADER_WIDGET;
            }
        }
        if (position >= 3) {
            if (essaiClinic != null && position == 3) {
                return ITEM_HEADER_CLINIC;
            } else {
                if (!hasCards()) {
                    return ITEM_NO_CARDS;
                }
            }
        }
        return ITEM_CARD;
    }

    public boolean isCardView(int position) {
        return getItemViewType(position) == ITEM_CARD;
    }

    private boolean hasCards() {
        return !items.isEmpty();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM_HEADER:
                View headerView = inflater.inflate(R.layout.item_search_home_header, parent, false);
                viewHolder = new HeaderViewHolder(headerView, alerts);
                break;
            case ITEM_CARD:
                View cardView = inflater.inflate(R.layout.item_search_home_card, parent, false);
                viewHolder = new CardViewHolder(cardView);
                break;
            case ITEM_FOOTER:
                View footerView = inflater.inflate(R.layout.item_search_home_footer, parent, false);
                viewHolder = new FooterViewHolder(footerView);
                break;
            case ITEM_NO_CARDS:
                View noCardsView = inflater.inflate(R.layout.item_search_home_no_cards, parent, false);
                viewHolder = new NoCardsViewHolder(noCardsView);
                break;
            case ITEM_HEADER_RESULTS:
                View headerResults = inflater.inflate(R.layout.item_search_home_header_results, parent, false);
                viewHolder = new HeaderResultViewHolder(headerResults);
                break;
            case ITEM_HEADER_WIDGET:
                View headerWidget = inflater.inflate(R.layout.item_search_home_widget, parent, false);
                viewHolder = new WidgetViewHolder(headerWidget);
                break;
            case ITEM_HEADER_CLINIC:
                View clinicView = inflater.inflate(R.layout.item_search_home_essai_clinic, parent, false);
                viewHolder = new ClinicViewHolder(clinicView);
                break;
            default:
                throw new RuntimeException("No such view type!");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            switch (holder.getItemViewType()) {
                case ITEM_HEADER:
                    final HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                    headerViewHolder.bind(itemClickedListener);
                    headerViewHolder.updateSwipeContainer(alerts);

                    final ConfigurationResponse config = repository.getConfig();
                    if (config != null) {
                        final ImageConfigurationItem images = config.getImages();
                        if (images != null) {
                            if (images.getHeader_logo_subtitle() == null || images.getHeader_logo_subtitle().equals("")) {
                                headerViewHolder.tv_ministry_logo_subtitle.setVisibility(View.GONE);
                            } else {
                                headerViewHolder.tv_ministry_logo_subtitle.setText(images.getHeader_logo_subtitle());
                            }
                            Glide.with(headerViewHolder.iv_ministry_logo.getContext())
                                    .load(images.getHeader_logo_android())
                                    .into(headerViewHolder.iv_ministry_logo);
                        }
                    }

//                Glide.with(((HeaderViewHolder) holder).bgHome.getContext()).load(R.drawable.bg_home2)
//                        .asBitmap()
//                        .fitCenter()
//                        .override(((HeaderViewHolder) holder).bgHome.getWidth(), Target.SIZE_ORIGINAL)
//                        .into(((HeaderViewHolder) holder).bgHome);
////                        into(new SimpleTarget<Bitmap>() {
////                    @Override
////                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
////
////                        // creating the image that maintain aspect ratio with width of image is set to screenwidth.
////                        int width = ((HeaderViewHolder) holder).bgHome.getMeasuredWidth();
////                        int diw = resource.getWidth();
////                        if (diw > 0) {
////                            int height = 0;
////                            height = width * resource.getHeight() / diw;
////                            resource = Bitmap.createScaledBitmap(resource, width, height, false);
////                        }
////                        ((HeaderViewHolder) holder).bgHome.setImageBitmap(resource);
////                    }
////                });


                    break;
                case ITEM_CARD:
                    int firstCardIndex = getFirstCardIndexInDataset();
                    int cardIndex = position - firstCardIndex;
                    QuickCardsAnonymousItem card = items.get(cardIndex);
                    CardViewHolder viewHolder = (CardViewHolder) holder;
                    viewHolder.bind(card, itemClickedListener);
                    break;
                case ITEM_FOOTER:
                    ((FooterViewHolder) holder).bind(signedIn, itemClickedListener);
                    break;
                case ITEM_HEADER_WIDGET:
                    ((WidgetViewHolder) holder).bind();
                    break;
                case ITEM_HEADER_CLINIC:
                    ((ClinicViewHolder) holder).bind(essaiClinic, itemClickedListener);
                    break;
                case ITEM_NO_CARDS:
                case ITEM_HEADER_RESULTS:
                    break;
            }
        } catch (Exception e) {
        }
    }

    private int getFirstCardIndexInDataset() {
        for(int index = 0; index < getItemCount(); ++index) {
            int viewType = getItemViewType(index);
            if(index == ITEM_CARD) {
                return index;
            }
        }

        return -1;
    }

    void showData(List<QuickCardsAnonymousItem> items) {
        this.items = new ArrayList<>();
        for (QuickCardsAnonymousItem quickCard : items) {
            if (quickCard.getNodeType() == NodeType.ESSAI_CLINIC) {
                this.essaiClinic = quickCard;
            } else {
                this.items.add(quickCard);
            }
        }
        notifyDataSetChanged();
    }

    public QuickCardsAnonymousItem remove(int position) {
        QuickCardsAnonymousItem result = this.items.get(position - 2);
        this.items.remove(position - 2);
        notifyItemRemoved(position);
        notifyDataSetChanged();

        if (items.size() >= MAX_CARD_NUMBER) {
            notifyItemInserted(MAX_CARD_NUMBER);
        }

        if (!hasCards()) {
            notifyItemInserted(2);
        }
        return result;
    }

    private void removeCard(QuickCardsAnonymousItem card) {
        itemClickedListener.removeCard(card);
        items.remove(card);
        notifyDataSetChanged();
    }

    public void update() {
//        notifyDataSetChanged();
        notifyItemChanged(0);
    }

    private String bmcValue = null;

    public void setBmcValue(String value) {
        this.bmcValue = value;
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llSearchBar)
        LinearLayout llSearchBar;
        @BindView(R.id.etSearch)
        EditText etSearch;
        @BindView(R.id.fragment_container_swipe)
        FrameLayout fragmentContainer;
        @BindView(R.id.bgHome)
        ImageView bgHome;
        @BindView(R.id.iv_ministry_logo)
        ImageView iv_ministry_logo;
        @BindView(R.id.tv_ministry_logo_subtitle)
        TextView tv_ministry_logo_subtitle;
        @BindView(R.id.urgenceBtn)
        Button urgenceBtn;
        JSONObject json_emergency = null;
        Location currentLocation = null;
        String currentPostalCode = null;


        public HeaderViewHolder(View view, IAlerts alerts) {
            super(view);
            ButterKnife.bind(this, view);

            FragmentTransaction fragmentTransaction = fragmentmanager.beginTransaction();
            fragmentTransaction
                    .replace(R.id.fragment_container_swipe, AlertsSwipeFragment.newInstance())
                    .commitAllowingStateLoss();

            // fit image to width
            ImageUtils.fitImageToHeight(bgHome);
            //
            updateSwipeContainer(alerts);
        }

        private void updateSwipeContainer(IAlerts alerts) {
            fragmentContainer.setVisibility(alerts.isEmpty() ? View.GONE : View.VISIBLE);
        }

        private void bind(OnItemClickedListener listener) {
            llSearchBar.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onSearchBarClicked();
                }
            });

            etSearch.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onSearchBarClicked();
                }
            });


            try {
                // Get Localized url from web service
                if (currentPostalCode == null && currentLocation != null) {
                    // Loc ok but Url KO
                    Log.v("localisation", "Updating localized url");
                    Position position = Position.fromCoordinates(currentLocation.getLongitude(), currentLocation.getLatitude(), currentLocation.getAltitude());
                    MapboxGeocoding reverseGeocode = new MapboxGeocoding.Builder()
                            .setAccessToken(itemView.getContext().getString(R.string.access_token))
                            .setCoordinates(position)
                            .setGeocodingType(GeocodingCriteria.TYPE_POSTCODE)
                            .build();
                    Call<GeocodingResponse> call = reverseGeocode.getCall();
                    Response responce = call.execute();
                    currentPostalCode = ((GeocodingResponse) responce.body()).getFeatures().get(0).getText();

                    String urlParams = "?lat=" + currentLocation.getLatitude() + "&lon=" + currentLocation.getLongitude() + "&postal=" + currentPostalCode;
                    UrlUtility urlutility = new UrlUtility(UrlExtras.getBaseURL() + UrlExtras.PAGE_URGENCES + urlParams);
                    urlutility.setBasicAuth(BuildConfig.CREDENTIAL_SERVER);
                    json_emergency = urlutility.readUrlJson();
                }

                // If not localized or pending, get global url.
                if (json_emergency == null) {
                    // Call Url without localisatoin.
                    UrlUtility urlutility = new UrlUtility(UrlExtras.getBaseURL() + UrlExtras.PAGE_URGENCES);
                    urlutility.setBasicAuth(BuildConfig.CREDENTIAL_SERVER);
                    json_emergency = urlutility.readUrlJson();


                    try {
                        LocationManager locationManager = (LocationManager) itemView.getContext().getSystemService(itemView.getContext().LOCATION_SERVICE);
                        // Create a criteria object to retrieve provider
                        Criteria criteria = new Criteria();
                        String provider = locationManager.getBestProvider(criteria, true);

                        LocationListener locationListenerGPS = new LocationListener() {
                            @Override
                            public void onLocationChanged(Location location) {
                                currentLocation = location;
                                Timber.tag("localisation").v("Location Listener: is Lat:" + currentLocation.getLatitude() + " Lon:" + currentLocation.getLongitude());
                            }

                            @Override
                            public void onStatusChanged(String provider, int status, Bundle extras) {
                                Timber.tag("localisation").v("onStatusChanged " + provider);
                            }

                            @Override
                            public void onProviderEnabled(String provider) {
                                Timber.tag("localisation").v("onProviderEnabled " + provider);
                            }

                            @Override
                            public void onProviderDisabled(String provider) {
                                Timber.tag("localisation").v("onProviderDisabled " + provider);
                            }
                        };

                        // Get Current Location
                        if (ActivityCompat.checkSelfPermission(itemView.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(itemView.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            Timber.tag("localisation").e("Localisation context / permission error");
                        } else {
                            locationManager.requestSingleUpdate(provider, locationListenerGPS, null);
                        }
                    } catch (Exception e) {
                        Timber.tag("localisation").e(e);
                    }

                }
                if (json_emergency != null) {
                    urgenceBtn.setText(json_emergency.get("text").toString());
                    listener.setUrgenceButtonUrl(json_emergency.get("url").toString());
                    urgenceBtn.setOnClickListener(v -> {
                        if (listener != null) {
                            listener.urgenceButtonClicked();
                        }
                    });
                    urgenceBtn.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                Timber.tag("urgenceBtn").e(e);
            }
        }
    }

    class CardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;
        @BindView(R.id.tvPropose)
        TextView tvFooter;
        @BindView(R.id.ivFavorite)
        ImageView ivFavorite;
        @BindView(R.id.ivAction)
        ImageView ivAction;
        @BindView(R.id.tvAction)
        TextView tvAction;
        @BindView(R.id.llRead)
        LinearLayout llRead;
        @BindView(R.id.item_delete)
        View deleteView;
        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.rlContent)
        RelativeLayout rlContent;

        QuickCardsAnonymousItem card;

        public CardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bind(QuickCardsAnonymousItem card, OnItemClickedListener listener) {
            this.card = card;

            boolean showFavorite;
            if (card.nodeType == NodeType.RECHERCHE_ENREGISTREE || card.nodeType == NodeType.PAGE_SIMPLE) {
                ivFavorite.setVisibility(View.GONE);
                showFavorite = false;
            } else {
                ivFavorite.setSelected(card.isFavorite());
                ivFavorite.setOnClickListener(view -> {
                    if (listener != null) {
                        listener.onFavorite(card);
                    }
                });
                ivFavorite.setVisibility(View.VISIBLE);
                showFavorite = true;
            }

            if (card.ficheType != null && card.ficheType == SearchItemType.ACTUALITE) {
                String actuality = App.getAppContext().getString(R.string.search_results_actuality);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(actuality + " " + card.title);
                int greyColor = App.getAppContext().getResources().getColor(R.color.textGray);
                spannableProposed.setSpan(new ForegroundColorSpan(greyColor), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableProposed.setSpan(new StyleSpan(Typeface.BOLD), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvTitle.setText(spannableProposed);
                tvDate.setText(tvDate.getContext().getString(R.string.search_get_informed_published, card.updateDate));
                tvDate.setVisibility(View.VISIBLE);
            } else {
                tvTitle.setText(card.title);
                tvDate.setVisibility(View.GONE);
            }

            // TODO MRO video navigation
            if (card.nodeType == NodeType.VIDEO) {
                // TODO
            }

            if (card.nodeType == NodeType.FICHES_INFOS || card.nodeType == NodeType.PAGE_SIMPLE) {
                ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                tvAction.setText(R.string.search_home_read);
                llRead.setOnClickListener(v -> listener.onReadClicked(card));
                rlContent.setOnClickListener(v -> listener.onReadClicked(card));
            } else {
                ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                tvAction.setText(R.string.around_me_more);
                llRead.setOnClickListener(v -> listener.onReadClicked(card));
                rlContent.setOnClickListener(v -> listener.onReadClicked(card));
            }

            if (card.nodeType == NodeType.APPLICATIONS) {
                ivAction.setImageResource(R.drawable.telecharger);
                tvAction.setText(R.string.search_results_download);
                llRead.setOnClickListener(v -> listener.onDownloadClicked(card));
                rlContent.setOnClickListener(v -> listener.onDownloadClicked(card));
            } else if (card.nodeType == NodeType.LIENS_EXTERNES) {
                ivAction.setImageResource(R.drawable.ressource_distante);
                tvAction.setText(R.string.view);
                llRead.setOnClickListener(v -> listener.onReadClicked(card));
                rlContent.setOnClickListener(v -> listener.onReadClicked(card));
            }

            tvContent.setText(card.description);
            tvContent.setTextSize(15);

            if (card.nodeType == NodeType.PROFESSIONNEL_DE_SANTE || card.nodeType == NodeType.SERVICE_DE_SANTE || card.nodeType == NodeType.OFFRES_DE_SOINS ||
                    card.nodeType == NodeType.ESTABLISSEMENT_DE_SANTE || card.nodeType == NodeType.NUMERO_TEL
                    || card.nodeType == NodeType.DOSSIER_THEMATIC || card.nodeType == NodeType.ENTITE_GEOGRAPHIQUE) {
                if (!TextUtils.isEmpty(card.phoneNumber)) {
                    ivAction.setImageResource(R.drawable.appeler_copier);
                    tvAction.setText(tvTitle.getContext().getString(R.string.search_results_call) + " " + card.phoneNumber);
                    llRead.setOnClickListener(v -> {
                        listener.onCall(card);
                    });
                    rlContent.setOnClickListener(v -> listener.onCall(card));
                } else {
                    ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                    tvAction.setText(R.string.around_me_more);
                    llRead.setOnClickListener(v -> listener.onReadClicked(card));
                    rlContent.setOnClickListener(v -> listener.onReadClicked(card));
                }
            }

            if (card.propose != null) {
                tvFooter.setVisibility(View.VISIBLE);
                String proposedString = App.getAppContext().getString(R.string.search_home_proposed_by);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(proposedString + " "
                        + (!TextUtils.isEmpty(card.propose) ? card.propose.toUpperCase() : ""));
                spannableProposed.setSpan(
                        new StyleSpan(Typeface.BOLD), proposedString.length(), spannableProposed.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvFooter.setText(spannableProposed);
            } else if (card.uo_type != null) {
                tvFooter.setVisibility(View.VISIBLE);
                tvFooter.setTypeface(null, Typeface.BOLD);
                tvFooter.setText(card.uo_type);
            } else {
                tvFooter.setVisibility(View.GONE);
            }

            deleteView.setOnClickListener(v -> {
                        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "translationX", -itemView.getWidth());
                        animator.setDuration(500);
                        animator.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                removeCard(card);
                                itemView.animate().translationX(0).setDuration(0).start();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                        animator.start();
                    }

            );

            swipeLayout.reset();
        }
    }

    class FooterViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnPersonalize)
        Button btnPersonalize;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;
        @BindView(R.id.tvClose)
        ImageButton tvClose;
        @BindView(R.id.tvMoreInformation)
        TextView tvMoreInformation;
        @BindView(R.id.cvHowWorks)
        CardView cvHowWorks;

        FooterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(boolean signedIn, OnItemClickedListener listener) {
            if (signedIn || closedHowWorks) {
                cvHowWorks.setVisibility(View.GONE);
            } else {
                cvHowWorks.setVisibility(View.VISIBLE);
            }

            btnPersonalize.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onPersonalizeButtonClicked();
                }
            });

            tvMoreInformation.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onMoreInformationClicked();
                }
            });

            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cvHowWorks.animate()
                            .alpha(0)
                            .translationX(-cvHowWorks.getWidth())
                            .setDuration(500)
                            .setListener(new BaseAnimatorListener() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    closedHowWorks = true;
                                    cvHowWorks.setVisibility(View.GONE);
                                }
                            });
                }
            });
        }
    }

    class NoCardsViewHolder extends RecyclerView.ViewHolder {
        public NoCardsViewHolder(View itemView) {
            super(itemView);
        }
    }

    class HeaderResultViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvResultsTitle)
        TextView tvResultsTitle;

        HeaderResultViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class ClinicViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.tvLinkTitle)
        TextView tvLinkTitle;
        @BindView(R.id.ivClinicImage)
        ImageView ivClinicImage;
        @BindView(R.id.footerLayout)
        ConstraintLayout footerLayout;

        ClinicViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(QuickCardsAnonymousItem clinic, OnItemClickedListener listener) {
            tvTitle.setText(clinic.title);
            tvDescription.setText(clinic.description);
            tvLinkTitle.setText(clinic.getLinkTitle());

            int roundSize = ivClinicImage.getContext().getResources().getDimensionPixelSize(R.dimen.ecl_corner_round);

            Glide.with(ivClinicImage.getContext())
                    .load(clinic.getImageUrl())
                    .transform(new CenterCrop(), new RoundedCornersTransformation(roundSize, 0, RoundedCornersTransformation.CornerType.TOP_LEFT))
                    .error(R.drawable.adtracking48)
                    .into(ivClinicImage);

            footerLayout.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onEssaiClinicClicked(clinic.getContentUrl());
                }
            });
        }

    }

    public interface OnItemClickedListener {
        void onSearchBarClicked();

        void onNoMoreCards();

        void urgenceButtonClicked();

        void setUrgenceButtonUrl(String url);


        void onWidgetInfoUpdated(String key, String value);

        void onWidgetSubmitted();

        JSONObject getWidgetJSON(Boolean update);

        void onPersonalizeButtonClicked();

        void onMoreInformationClicked();

        //        void onFavoriteClicked();
        void onReadClicked(QuickCardsAnonymousItem card);

        void onFavorite(QuickCardsAnonymousItem card);

        void onCall(QuickCardsAnonymousItem card);

        void onDownloadClicked(QuickCardsAnonymousItem item);

        void removeCard(QuickCardsAnonymousItem card);

        // Essai Clinic
        void onEssaiClinicClicked(String url);
    }

}
