package com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes;

import android.content.Context;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentAlertSwipeBinding;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.CustomViewPager;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertsSwipeFragment extends ViewFragment implements AlertsSwipePresenter.CreateRecipeView {

    public static final int AUTO_SWIPE_DELAY = 10000;
    private static final int CHOICE_DATABASE_LOADER_ID = 12;
    @Inject
    AlertsSwipePresenter alertsSwipePresenter;
    private FragmentAlertSwipeBinding binding;
    private AlertsSwipeAdapter adapter;

    private Timer timer;
    private SwipeTimerTask swipeTimerTask;
    private Loader.ForceLoadContentObserver forceLoadContentObserver;

    private class SwipeTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(() -> {
                final int item = binding.createAccountViewPagerr.getCurrentItem() + 1;
                if (item >= binding.createAccountViewPagerr.getAdapter().getCount()) {
//                    finishActivity();
                    binding.createAccountViewPagerr.setCurrentItem(0);
                } else
                    binding.createAccountViewPagerr.setCurrentItem(item);
            });
        }
    }

    public void startTimer() {
        if (timer == null) {
            timer = new Timer();
            swipeTimerTask = new SwipeTimerTask();
            timer.schedule(swipeTimerTask, AUTO_SWIPE_DELAY, AUTO_SWIPE_DELAY);
        }
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public AlertsSwipeFragment() {
    }

    public static Fragment newInstance() {
        AlertsSwipeFragment fragment = new AlertsSwipeFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alert_swipe, container, false);
        adapter = new AlertsSwipeAdapter(getFragmentManager(), null){

            @Override
            public void update() {
                try {
                    // TODO it is crutch!!!
                    binding.createAccountViewPagerr.setAdapter(adapter);
                }catch(Exception e){

                }
            }
        };
        binding.createAccountViewPagerr.setAdapter(adapter);
        binding.createAccountViewPagerr.setOnSwipeOutListener(new CustomViewPager.OnSwipeOutListener() {

            @Override
            public void onSwipeOutAtStart() {
            }

            @Override
            public void onSwipeOutAtEnd() {
            }
        });

        binding.createAccountViewPagerr.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                stopTimer();
                startTimer();
                setBullet(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return binding.getRoot();
    }

    private void updateButtons() {
        binding.finishCycleContainer.removeAllViews();
        final int newCount = adapter.getCount();
        if (newCount > 1) {
            for (int i = 0; i < newCount; i++) {
                RadioButton radioButton = new RadioButton(getActivity());
                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                        (int) getResources().getDimension(R.dimen.alerts_progress_circle_size),
                        (int) getResources().getDimension(R.dimen.alerts_progress_circle_size));
                params.setMargins((int) getResources().getDimension(R.dimen.alerts_progress_half_padding),
                        0, (int) getResources().getDimension(R.dimen.alerts_progress_half_padding), 0);
                radioButton.setLayoutParams(params);
                radioButton.setBackground(getResources().getDrawable(R.drawable.selector_alert_bullet));
                radioButton.setButtonDrawable(getResources().getDrawable(android.R.color.transparent));
                binding.finishCycleContainer.addView(radioButton);
            }
            setBullet(binding.createAccountViewPagerr.getCurrentItem());
        }
    }

    private void setBullet(int index) {
        try {
            final View childAt = binding.finishCycleContainer.getChildAt(index);
            if (childAt != null) {
                ((RadioButton) childAt).setChecked(true);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLoaderManager().initLoader(CHOICE_DATABASE_LOADER_ID, new Bundle(), cursorLoaderCallbacks).startLoading();
        forceLoadContentObserver = getLoaderManager().getLoader(CHOICE_DATABASE_LOADER_ID).new ForceLoadContentObserver();
        getActivity().getContentResolver().registerContentObserver(TablesContentProvider.ALERTS_CONTENT_URI, true, forceLoadContentObserver);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.historical);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getContentResolver().unregisterContentObserver(forceLoadContentObserver);
    }

    private void finishActivity() {
        getActivity().finish();
        getActivity().onBackPressed();
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @NonNull
    @Override
    public AlertsSwipePresenter getPresenter() {
        return alertsSwipePresenter;
    }

    private LoaderManager.LoaderCallbacks<Cursor> cursorLoaderCallbacks = new LoaderManager.LoaderCallbacks<Cursor>() {
        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//            showProgress();
            return new AlertsSwipeCursorLoader(getActivity(), getPresenter().getAlerts());
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
            adapter.swapCursor(cursor);
//            hideProgress();
            getActivity().runOnUiThread(() -> updateButtons());
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            adapter.swapCursor(null);
        }
    };

}
