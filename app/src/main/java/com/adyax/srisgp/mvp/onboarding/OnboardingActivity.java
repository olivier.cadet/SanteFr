package com.adyax.srisgp.mvp.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.presentation.BaseActivity;
import com.adyax.srisgp.presentation.CustomViewPager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public class OnboardingActivity extends BaseActivity implements OnboardingFragmentListener {
    public static final int REQUEST_SETTINGS = 124;

    public static final String INTENT_OPEN_CREATE_ACCOUNT = "open_create_account";

    private static final long SPLASH_MILLIS = 3000;

    @BindView(R.id.pager)
    CustomViewPager pager;

    @Inject
    IRepository repository;

    private OnboardingPagerAdapter pagerAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, OnboardingActivity.class);
        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SETTINGS) {
            Fragment locationFragment = pagerAdapter.getRegisteredFragment(OnboardingPagerAdapter.GEOLOCATION_POSITION);
            if (locationFragment != null) {
                ((GeolocationOnboardingFragment) locationFragment).onLocationResultRetrieved(resultCode == Activity.RESULT_OK);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        App.getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        ButterKnife.bind(this);

        pagerAdapter = new OnboardingPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == pagerAdapter.getCount() - 1) {
                    repository.setNotFirstRun();
                    pager.setPagingEnabled(false);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = createIntent(false);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }, SPLASH_MILLIS);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private Intent createIntent(boolean openCreateAccount) {
        Intent intent = new Intent();
        intent.putExtra(INTENT_OPEN_CREATE_ACCOUNT, openCreateAccount);
        return intent;
    }

    @Override
    public void onNext() {
        pager.setCurrentItem(pager.getCurrentItem() + 1);
    }

    @Override
    public void onSkip() {
        repository.setNotFirstRun();
        finish();
    }

    @Override
    public void onCreateAccountClicked() {
        Intent intent = createIntent(true);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() > 0) {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        } else {
            // TODO: temporary solution
//            super.onBackPressed();
        }
    }
}
