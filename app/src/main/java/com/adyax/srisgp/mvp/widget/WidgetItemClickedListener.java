package com.adyax.srisgp.mvp.widget;


import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import org.json.JSONObject;

import java.util.Iterator;

import javax.inject.Inject;

import static java.lang.Integer.parseInt;


/**
 * Created by NW on 18/04/2019.
 */
public class WidgetItemClickedListener implements WidgetAdapter.OnItemClickedListener {


    //  static JSONObject widgetJSON = null;
    String widgetPageUrl = null;

    View view;

    @Inject
    FragmentFactory fragmentFactory;

    public WidgetItemClickedListener(View view) {
        this.view = view;
    }

    @Override
    public void onWidgetInfoUpdated(String key, String value) {
        if (key != null) {
            // Handle particular cases.
            if (key.equals("sous_cas") && value != null) {
                try {
                    JSONObject options = WidgetViewHelper.getWidgetJSON(false).getJSONObject("checkboxes_fieldset").getJSONObject("sous_cas").getJSONObject("#options");
                    Iterator<String> keys = options.keys();
                    value = keys.next();
                    value = value.substring(value.lastIndexOf("|") + 1);
                } catch (Exception e) {
                    value = null;
                }
            }

            if (value == null || value.equals("")) {
                WidgetViewHelper.widgetData.remove(key);
            } else {
                WidgetViewHelper.widgetData.put(key, value);
            }
            Log.e("HeaderWidgetViewHolder", "onWidgetInfoUpdated");
        }
        JSONObject widgetJSON = WidgetViewHelper.getWidgetJSON(true);

        // Update View
        // Set Sous_cas text and visibility.
        CheckBox widgetCheckBox = (CheckBox) view.findViewById(R.id.widgetCheckBox);
        try {
            // Set Page url
            widgetPageUrl = widgetJSON.getJSONObject("sgp_widget_page_prev").get("#value").toString();

            // Set Options (sous_cas)
            JSONObject options = widgetJSON.getJSONObject("checkboxes_fieldset").getJSONObject("sous_cas").getJSONObject("#options");
            value = options.keys().next();
            widgetCheckBox.setText(options.optString(value));
            widgetCheckBox.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            widgetCheckBox.setVisibility(View.GONE);
            Log.e("onWidgetSubmitted", e.getMessage());
        }

        // Error message Visibility.
        TextView widgetErrText = (TextView) view.findViewById(R.id.widgetErrText);
        if (widgetErrText.getVisibility() == View.VISIBLE) {
            if (WidgetViewHelper.widgetData.get("age") != null && WidgetViewHelper.widgetData.get("sexe") != null) {
                widgetErrText.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onWidgetSubmitted() {

        // Set error message.
        TextView widgetErrText = (TextView) view.findViewById(R.id.widgetErrText);
        String age = WidgetViewHelper.widgetData.get("age");
        String sexe = WidgetViewHelper.widgetData.get("sexe");
        if (age == null && sexe == null) {
            widgetErrText.setVisibility(View.VISIBLE);
            widgetErrText.setText(view.getContext().getString(R.string.pls_select_age_sexe));
            return;
        } else if (age == null) {
            widgetErrText.setVisibility(View.VISIBLE);
            widgetErrText.setText(view.getContext().getString(R.string.pls_select_age));
            return;
        } else if (sexe == null) {
            widgetErrText.setVisibility(View.VISIBLE);
            widgetErrText.setText(view.getContext().getString(R.string.pls_select_sexe));
            return;
        }
        if (parseInt(age) > 123) {
            widgetErrText.setVisibility(View.VISIBLE);
            widgetErrText.setText(view.getContext().getString(R.string.yrs_too_much));
            return;
        }

        try {
            Log.d("onWidgetSubmitted", "Open URL : " + widgetPageUrl);
            if (widgetPageUrl != null && !widgetPageUrl.equals("null")) {
                String url = widgetPageUrl;
                // Clear current data.
                WidgetViewHelper.init();
                widgetPageUrl = null;
                // Open web view.
                if (fragmentFactory == null) {
                    fragmentFactory = new FragmentFactory();
                }
                fragmentFactory.startWebViewActivity(view.getContext(), WebViewArg.webViewArgForWidget(url), WidgetViewHelper.REQUEST_WIDGET);
            }
        } catch (Exception e) {
            Log.e("onWidgetSubmitted", e.getMessage());
        }
    }

}
