package com.adyax.srisgp.mvp.notification;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 9/22/16.
 */

public enum StatusNotification {

    all(0),
    read(1),
    unread(2);

    private int value;
    private static Map<Integer, StatusNotification> map = new HashMap<>();

    StatusNotification(int value) {
        this.value = value;
    }

    static {
        for (StatusNotification pageType : StatusNotification.values()) {
            map.put(pageType.value, pageType);
        }
    }

    public static StatusNotification valueOf(int pageType) {
        return (StatusNotification) map.get(pageType);
    }

    public int getValue() {
        return value;
    }

    public String getValueStr() {
        return String.valueOf(value);
    }
}