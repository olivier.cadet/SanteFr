package com.adyax.srisgp.mvp.my_account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.UpdatePersonalInformationCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationActivity;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.CityResult;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.NetworkPresenter;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import java.util.Calendar;

import javax.inject.Inject;


public class PersonalInformationPresenter extends NetworkPresenter<PersonalInformationPresenter.PersonalInformationView> {

    public interface PersonalInformationView extends IView {
        void setGender(GenderType gender);

        void setYear(String year);

        void setCity(String city);

        void stateChanged(boolean isChanged);

        void startActivityForResult(Intent intent, int requestCode);
    }

    private static final int UPDATE_PERSONAL_INFORMATION_REQUEST = 1;

    private static final int CITY_ACTIVITY_REQUEST_CODE = 100;

    private IRepository repository;
    private FragmentFactory fragmentFactory;

    private User currentUser;

    private String gender;
    private String yearOfBirth;
    private String city;

    @Inject
    public PersonalInformationPresenter(IRepository repository, FragmentFactory fragmentFactory) {
        this.repository = repository;
        this.fragmentFactory = fragmentFactory;
    }

    public void onViewCreated() {
        setCurrentUser();
    }

    private void setCurrentUser() {
        currentUser = repository.getCredential().user;
        if (currentUser != null) {

            if (currentUser.sex != null) {
                getView().setGender(GenderType.valueOf(currentUser.sex.id));
                gender = currentUser.sex.id;
            }

            getView().setYear(currentUser.birth_year);
            yearOfBirth = currentUser.birth_year;

            if (currentUser.city != null) {
                getView().setCity(currentUser.city.value);
                city = currentUser.city.id;
            }
        }
    }

//    public void showGenderDialog() {
//        if (getView() == null)
//            return;
//
//        CharSequence[] genders = GenderType.getCharSequence(getContext());
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setItems(genders, (dialog, which) -> {
//            if (getView() != null) {
//                String genderText = genders[which].toString();
//                getView().setGender(genderText);
//                gender = GenderType.valueOf(getContext(), genderText).name();
//                getView().stateChanged(isChanged());
//            }
//        });
//        builder.show();
//    }
//
//    public void showYearDialog() {
//        if (getView() == null)
//            return;
//
//        YearAlertDialog.quickShow(getContext(), Integer.parseInt(yearOfBirth), year -> {
//            if (getView() != null) {
//                getView().setYear(Integer.toString(year));
//                yearOfBirth = Integer.toString(year);
//                getView().stateChanged(isChanged());
//            }
//        });
//    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    public void selectGender(String gender) {
        this.gender = GenderType.valueOf(getContext(), gender).name();
        getView().stateChanged(isChanged());
    }

    public void selectAge(String year) {
        yearOfBirth = year;
        getView().stateChanged(isChanged());
    }

    public void changeLocation() {
        if (getView() != null) {
            Intent intent = ProvideLocationActivity.newIntent(getContext());
            getView().startActivityForResult(intent, CITY_ACTIVITY_REQUEST_CODE);
        }
    }

    private boolean isChanged() {
        if (currentUser == null || gender == null || yearOfBirth == null || city == null)
            return false;

        if (!gender.equals(currentUser.sex.id))
            return true;

        if (!yearOfBirth.equals(currentUser.birth_year))
            return true;

        if (!city.equals(currentUser.city.id))
            return true;

        return false;
    }

    public void validate() {
        if (getView() != null) {
            showProgress();
            sendUpdatePersonalInformationRequest();
        }
    }

    private void showConformationDialog() {
        if (getView() == null)
            return;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.alert_information_updated_title);
        builder.setMessage(R.string.alert_information_updated_message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    /**
     * onActivityResult
     */

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CITY_ACTIVITY_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    if (data.hasExtra(ProvideLocationFragment.INTENT_CITY)) {
                        CityResult cityResult = data.getParcelableExtra(ProvideLocationFragment.INTENT_CITY);
                        getView().setCity(cityResult.getTitle());
                        city = Long.toString(cityResult.getPostalCode());
                        getView().stateChanged(isChanged());
                    }
                }
                break;
        }
    }

    /**
     * Request
     */

    private void sendUpdatePersonalInformationRequest() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(yearOfBirth));
        String timestampForYear = Long.toString(calendar.getTimeInMillis() / 1000);

        ServiceCommand command =
                new UpdatePersonalInformationCommand(gender, timestampForYear, city);
        sendCommand(command, UPDATE_PERSONAL_INFORMATION_REQUEST);
    }

    /**
     * AppReceiver
     */

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case UPDATE_PERSONAL_INFORMATION_REQUEST:
                setCurrentUser();
                if (getView() != null)
                    getView().stateChanged(isChanged());
                hideProgress();
                showConformationDialog();
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        switch (requestCode) {
            case UPDATE_PERSONAL_INFORMATION_REQUEST:
                hideProgress();
                break;
        }
    }
}
