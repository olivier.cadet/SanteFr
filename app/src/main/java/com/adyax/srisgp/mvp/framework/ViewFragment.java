package com.adyax.srisgp.mvp.framework;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;

import com.adyax.srisgp.presentation.BaseFragment;

public abstract class ViewFragment extends BaseFragment implements IView {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getPresenter().attachView(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().restoreState(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().registerListeners();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().unregisterListeners();
    }

    @Override
    public void onDetach() {
        getPresenter().detachView();
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getPresenter().saveState(outState);
    }

    @NonNull
    abstract public IPresenter getPresenter();

    protected void startIntroAnimation(View view) {
//        binding.list.setTranslationY(latestPostRecyclerview.getHeight());
        view.setAlpha(0f);
        view.animate()
                .translationY(0)
                .setDuration(1000)
                .alpha(1f)
//                .setInterpolator(new AccelerateDecelerateInterpolator())
                .start();
    }

    // don't delete! It for sample
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onBackPressed() {
        return false;
    }
}
