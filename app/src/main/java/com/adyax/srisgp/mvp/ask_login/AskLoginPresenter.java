package com.adyax.srisgp.mvp.ask_login;

import android.app.Activity;
import android.content.Intent;

import com.adyax.srisgp.R;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.authentication.login.LoginActivity;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class AskLoginPresenter extends Presenter<AskLoginPresenter.DummyView> {

    public static final int START_LOGIN_REQUEST_CODE = 1030;
    public static final int START_CREATE_ACCOUNT_REQUEST_CODE = 1031;
    private FragmentFactory fragmentFactory;

    @Inject
    public AskLoginPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    public void clickCreateAccount() {
        Intent intent = CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART);
        ((Activity)getContext()).startActivityForResult(intent, START_CREATE_ACCOUNT_REQUEST_CODE);
        ((Activity)getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    public void clickLogIn() {
        Intent intent = LoginActivity.newIntent(getContext());
        ((Activity)getContext()).startActivityForResult(intent, START_LOGIN_REQUEST_CODE);
        ((Activity)getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    public interface DummyView extends IView {
        void showMessage(String body);
    }

}
