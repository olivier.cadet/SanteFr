package com.adyax.srisgp.mvp.configuretext;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentConfigureTextBinding;
import com.adyax.srisgp.presentation.BaseFragment;

/**
 * Created by anton.kobylianskiy on 2/16/17.
 */

public class ConfigureTextFragment extends BaseFragment {

    private FragmentConfigureTextBinding binding;

    public static ConfigureTextFragment newInstance() {
        ConfigureTextFragment fragment = new ConfigureTextFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_configure_text, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private void bindViews() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.configure_text_title);

        binding.btnOpenSettings.setOnClickListener(v -> {
            startActivity(new Intent(Settings.ACTION_DISPLAY_SETTINGS));
        });
    }
}
