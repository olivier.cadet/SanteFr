package com.adyax.srisgp.mvp.history;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.Utils;

import java.net.URISyntaxException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 2/20/17.
 */

public class HistoryFragment extends ViewFragment implements HistoryPresenter.HistoryView {

    private static final String ARGUMENT_HISTORY = "history";

    @Inject
    HistoryPresenter presenter;
    @BindView(R.id.tvReceived)
    TextView tvReceived;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvLink)
    TextView tvLink;
    @BindView(R.id.tvPropose)
    TextView tvPropose;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.optionalContainer)
    LinearLayout optionalContainer;
    @BindView(R.id.ivFavorite)
    ImageView ivFavorite;
    @BindView(R.id.ivAction)
    ImageView ivAction;
    @BindView(R.id.tvAction)
    TextView tvAction;
    @BindView(R.id.llRead)
    LinearLayout llRead;
    @BindView(R.id.card_view_get_informed)
    CardView cardViewGetInformed;

    public static HistoryFragment newInstance(HistoryItem item) {
        Bundle args = new Bundle();
        args.putParcelable(ARGUMENT_HISTORY, item);
        HistoryFragment fragment = new HistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private HistoryItem getHistoryItem() {
        return getArguments().getParcelable(ARGUMENT_HISTORY);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private void bindViews() {
        HistoryItem item = getHistoryItem();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(item.getTitle());

        tvDate.setVisibility(View.GONE);

        ivFavorite.setSelected(item.isFavorite());
        ivFavorite.setOnClickListener(v -> {
            presenter.onFavoriteClicked(item, ivFavorite.isSelected());
        });
        tvTitle.setText(item.getTitle());
        tvContent.setText(item.getDescription());

        if (!TextUtils.isEmpty(item.source)) {
            String proposedString = App.getAppContext().getString(R.string.search_home_proposed_by);
            SpannableStringBuilder spannableProposed = new SpannableStringBuilder(proposedString + " " + item.source.toUpperCase());
            spannableProposed.setSpan(
                    new StyleSpan(Typeface.BOLD), proposedString.length(), spannableProposed.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvPropose.setText(spannableProposed);
            tvPropose.setVisibility(View.VISIBLE);
        } else {
            tvPropose.setVisibility(View.GONE);
        }

        tvReceived.setText(App.getAppContext().getString(R.string.notification_received_on, Utils.getDateString(item.last_visit * 1000L)));

        //
        if (!TextUtils.isEmpty(item.link)) {
            String link;
            try {
                link = Utils.getDomainName(item.link).concat("/..");
                tvLink.setText(App.getAppContext().getString(R.string.search_results_find, link));
                tvLink.setOnClickListener(v -> {
                    presenter.readClicked(item);
                });
                tvLink.setVisibility(View.VISIBLE);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                tvPropose.setVisibility(View.GONE);
                tvLink.setOnClickListener(null);
            }
        } else {
            tvLink.setVisibility(View.GONE);
            tvLink.setOnClickListener(null);
        }

        View.OnClickListener clickListener;
        switch (item.getNodeType()) {
            case APPLICATIONS:
                ivAction.setImageResource(R.drawable.telecharger);
                tvAction.setText(R.string.search_results_download);

                clickListener = v -> presenter.downloadClicked(item);
                llRead.setOnClickListener(clickListener);
                cardViewGetInformed.setOnClickListener(clickListener);
                break;
            case LIENS_EXTERNES:
                ivAction.setImageResource(R.drawable.ressource_distante);
                tvAction.setText(R.string.view);

                clickListener = v -> presenter.readClicked(item);
                llRead.setOnClickListener(clickListener);
                cardViewGetInformed.setOnClickListener(clickListener);
                break;
            case MEDICAMENTS:
                ivAction.setImageResource(R.drawable.ressource_distante);
                tvAction.setText(R.string.view);
                ivFavorite.setVisibility(View.GONE);

                clickListener = v -> presenter.readClicked(item);
                llRead.setOnClickListener(clickListener);
                cardViewGetInformed.setOnClickListener(clickListener);
                break;
            case NUMERO_TEL:
                ivAction.setImageResource(R.drawable.appeler_copier);
                tvAction.setText(R.string.around_me_more);

                if (!TextUtils.isEmpty(item.phone)) {
                    tvAction.setText(tvTitle.getContext().getString(R.string.search_results_call) + " " + item.phone);
                    clickListener = v -> {
//                        Utils.requestCall(item.phone, getContext());
                        new TrackerHelper(item).requestCall(getContext());
                    };
                    llRead.setOnClickListener(clickListener);
                    cardViewGetInformed.setOnClickListener(clickListener);
                } else {
                    tvAction.setText(tvTitle.getContext().getString(R.string.search_results_call));
                }
                break;
        }
    }

    @Override
    public void openRegistrationScreen() {
        Intent intent = CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void updateFavoriteStatus(boolean favorite) {
        ivFavorite.setSelected(favorite);
    }
}
