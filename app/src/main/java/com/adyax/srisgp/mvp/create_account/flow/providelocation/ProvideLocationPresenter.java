package com.adyax.srisgp.mvp.create_account.flow.providelocation;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;

import com.adyax.srisgp.data.net.command.CitiesCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.LocationPresenter;
import com.adyax.srisgp.mvp.LocationView;
import com.adyax.srisgp.utils.Utils;
import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/5/16.
 */
public class ProvideLocationPresenter extends LocationPresenter<ProvideLocationPresenter.ProvideLocationView>
        implements AppReceiver.AppReceiverCallback {

    public static final int STATE_LOCATION_DEFAULT = 2;
    public static final int STATE_LOCATION_SEARCH = 3;

//    private static final int COMMAND_GET_CITIES = 1;
    private static final int SEARCH_DELAY = 300;

//    private AppReceiver appReceiver = null;
    private Handler searchHandler = new Handler();

//    @Override
//    public void attachView(ProvideLocationView view) {
//        super.attachView(view);
//        if (appReceiver == null) {
//            appReceiver = new AppReceiver();
//        }
//        appReceiver.setListener(this);
//    }

    @Inject
    public ProvideLocationPresenter(IRepository repository) {
        super(repository);
    }

    @Override
    protected boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch) {
        try {
            String cityName = Utils.getCityName(getContext(), location);
            getView().setCurrentLocation(cityName);
        } catch (IOException e) {
            getView().setCurrentLocation("");
        }
        return false;
    }

    public void requestCities(String s) {
        searchHandler.removeCallbacksAndMessages(null);
        if (s.length() >= 3) {
            searchHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ExecutionService.sendCommand(getContext(), appReceiver,
                            new CitiesCommand(s), ExecutionService.COMMAND_GET_CITIES);
                }
            }, SEARCH_DELAY);
        } else {
            getView().showDefaultState(false);
        }
    }

//    @Override
//    public void detachView() {
//        super.detachView();
//        appReceiver.setListener(null);
//    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.COMMAND_GET_CITIES:
                final CitiesResponse citiesResponse = data.getParcelable(CommandExecutor.CITIES_RESPONSE);
                if (getView().getSearchFieldValue().length() >= 3) {
                    final String[] cites = citiesResponse.getCites();
                    if (cites != null && cites.length != 0) {
                        getView().showSearchState(citiesResponse);
                    } else {
                        getView().showDefaultState(false);
                    }
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try {
            if (errorMessage != null) {
                getView().showError(new Gson().fromJson(errorMessage.getErrorMessage(), ErrorResponse.class).getErrorMessage(null));
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public interface ProvideLocationView extends LocationView {
        void showSearchState(CitiesResponse response);

        void showError(String description);

        void setCurrentLocation(String city);

        void showLoading();

        void hideLoading();

        void showDefaultState(boolean bShowAlertDialog);

        String getSearchFieldValue();
    }
}
