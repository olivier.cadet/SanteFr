package com.adyax.srisgp.mvp.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;

import com.adyax.srisgp.R;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;
import com.google.android.gms.common.api.Status;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class SearchActivity extends MaintenanceBaseActivity implements ProvideLocationFragment.OnPromptLocationSettingsListener {

    private static final int REQUEST_CHECK_SETTINGS = 1;

    public static Intent newIntent(Context context, SearchArgument searchArgument) {
        Intent intent = new Intent(context, SearchActivity.class);
        if (searchArgument != null) {
            intent.putExtra(SearchArgument.BUNDLE_NAME, searchArgument);
        }
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        replaceFragment(R.id.fragment_container,
                SearchFragment.newInstance(getIntent().getParcelableExtra(SearchArgument.BUNDLE_NAME)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            ((SearchFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container))
                    .onPromptSettingsResult(resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    public void onPromptLocation(Status status) {
        try {
            status.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }
}
