package com.adyax.srisgp.mvp.searchresults.info;

import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.ViewSearchResultsEmptyBinding;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.searchresults.SearchResultsAdapter;
import com.adyax.srisgp.mvp.searchresults.SearchResultsFragment;
import com.adyax.srisgp.mvp.searchresults.SearchResultsPresenter;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

/**
 * Created by anton.kobylianskiy on 10/27/16.
 */
public class InfoSearchResultsFragment extends SearchResultsFragment {

    @Override
    public void updateScreenViewResearch(String suggestedType) {

    }

    @Override
    public void updateInfo(SearchResponseHuge response) {

    }

    @Override
    protected IPresenter getAdditionalPresenter() {
        return null;
    }

    @Override
    protected void onRelatedItemClicked(RelatedItem item) {
        listener.onInfoRelatedItemClicked(item);
    }

    @Override
    public void addResults(Cursor cursor, String suggestedType, boolean sortedByDistance, boolean hasMoreItems) {
        super.addResults(cursor, suggestedType, true, hasMoreItems);
        processFilterButtonVisibility();
    }

    @Override
    protected SearchResultsAdapter createAdapter(SearchResultsAdapter.ResultsAdapterListener adapterListener) {
        return new GetInformedResultsAdapter(adapterListener, new GetInformedResultsAdapter.InfoCardListener() {
            SearchResultsPresenter presenter = resultsPresenter;

            @Override
            public SearchResultsPresenter getPresenter() {
                return this.presenter;
            }

            @Override
            public void onRead(SearchItemHuge itemHuge) {
                if (!itemHuge.isContentMaintenanceMode()) {
                    if (itemHuge.getNodeType() == NodeType.MEDICAMENTS || itemHuge.getNodeType() == NodeType.SUBSTANCES) {
                        onClickOpenWebView(itemHuge);
                    } else {
                        onClickRead(itemHuge);
                    }
                }
            }

            @Override
            public void onUrlClicked(String url) {
                SearchItemHuge item = new SearchItemHuge();
                item.url = url;
                Intent intent;
                intent = WebViewActivity.newIntent(getContext(), new WebViewArg(item, WebPageType.SIMPLE, TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION));
                startActivityForResult(intent, REQUEST_READ);
                ((FragmentActivity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
            }

            @Override
            public void onFavorite(SearchItemHuge item) {
                presenter.clickFavorite(item);
            }

            @Override
            public void onDownloadClicked(SearchItemHuge itemHuge) {
//                resultsPresenter.addInHistory(itemHuge);
                new TrackerHelper(itemHuge).showAppsDialog(getContext(), TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION);
            }

            @Override
            public void onCall(SearchItemHuge item) {
//                resultsPresenter.addInHistory(item);
                new TrackerHelper(item).requestCall(getContext());
            }
        }, resultsPresenter.getRepository());
    }


    @Override
    protected void viewCreated(View view, Bundle savedInstanceState) {

    }

    @Override
    protected boolean shouldLoadData() {
        return true;
    }

    @Override
    protected void showEmptyView(ViewGroup parent, boolean locationDefined) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewSearchResultsEmptyBinding binding = DataBindingUtil.inflate(inflater, R.layout.view_search_results_empty, parent, false);
        binding.tvEmptyTitle.setText(getString(R.string.search_get_informed_empty_title));
        binding.tvEmptyText1.setText(getString(R.string.search_get_informed_empty_text));
        binding.tvEmptyText2.setText(resultsPresenter.getRepository().getConfig().getSearch().getEmpty_result_inactive_region());
        parent.addView(binding.getRoot());
        processFilterButtonVisibility();
    }

    private void processFilterButtonVisibility() {
        if (listener != null) {
            listener.onChangeFilterActionButtonVisibility(isFilterButtonShouldBeShown(), this);
        }
    }

}
