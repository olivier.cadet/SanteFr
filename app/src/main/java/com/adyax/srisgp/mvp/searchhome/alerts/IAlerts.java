package com.adyax.srisgp.mvp.searchhome.alerts;

import android.database.Cursor;

import com.adyax.srisgp.data.net.response.tags.AlertItem;

/**
 * Created by SUVOROV on 10/9/16.
 */

public interface IAlerts {
    boolean isEmpty();

    void setBadge(int count);

    AlertItem getUrgentAlert();
    Cursor getAlerts();
}
