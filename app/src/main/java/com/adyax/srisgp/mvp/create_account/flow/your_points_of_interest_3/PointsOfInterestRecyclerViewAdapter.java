package com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.adyax.srisgp.databinding.ItemPointsOfInterestBinding;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by SUVOROV on 10/2/16.
 */

public class PointsOfInterestRecyclerViewAdapter extends RecyclerView.Adapter<PointsOfInterestRecyclerViewAdapter.ViewHolder> {

    private TermsItem[] records;

    private Set<Long> ids =new HashSet<>();

    public PointsOfInterestRecyclerViewAdapter(TermsItem[] records) {
        this.records = records;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_points_of_interest, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.init(records[i]);
    }

    @Override
    public int getItemCount() {
        return records.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ItemPointsOfInterestBinding binding;

        public ViewHolder(ItemPointsOfInterestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void init(TermsItem termsItem) {
            binding.switchOpt.setText(termsItem.getName());
            binding.switchOpt.setTag(termsItem.getId());
            binding.switchOpt.setOnCheckedChangeListener((compoundButton, b) -> {
                final Long id = Long.valueOf(compoundButton.getTag().toString());
                if(b) {
                    ids.add(id);
                }else{
                    if(ids.contains(id)){
                        ids.remove(id);
                    }
                }
            });
        }
    }

    public Set<Long> getIds() {
        return ids;
    }
}
