package com.adyax.srisgp.mvp.around_me.get_location;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;
import com.google.android.gms.common.api.Status;

/**
 * Created by anton.kobylianskiy on 11/1/16.
 */

public class GetCityActivity extends MaintenanceBaseActivity implements ProvideLocationFragment.OnPromptLocationSettingsListener {
    private static final int REQUEST_CHECK_SETTINGS = 1;

    public static final String LAST_LOCATION_KEY = "lastLocation";

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, GetCityActivity.class);
        return intent;
    }

    public static Intent newIntent(Context context, String lastLocation) {
        Intent intent = new Intent(context, GetCityActivity.class);
        intent.putExtra(LAST_LOCATION_KEY, lastLocation);
        return intent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            ((GetCityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container))
                    .onPromptSettingsResult(resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_city);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            Fragment fragment;

            String lastLocation = getLastLocation();
            if (lastLocation != null && !lastLocation.isEmpty())
                fragment = GetCityFragment.newInstance(lastLocation);
            else
                fragment = GetCityFragment.newInstance();

            replaceFragment(R.id.fragment_container, fragment);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPromptLocation(Status status) {
        try {
            status.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private String getLastLocation() {
        if (getIntent().hasExtra(LAST_LOCATION_KEY))
            return getIntent().getStringExtra(LAST_LOCATION_KEY);
        return "";
    }
}
