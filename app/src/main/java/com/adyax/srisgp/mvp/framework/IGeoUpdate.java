package com.adyax.srisgp.mvp.framework;

import android.location.Location;

/**
 * Created by SUVOROV on 8/2/17.
 */

interface IGeoUpdate {
    void update(Location location);
}
