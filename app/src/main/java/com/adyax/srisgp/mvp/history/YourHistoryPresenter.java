package com.adyax.srisgp.mvp.history;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.ClearHistoryCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class YourHistoryPresenter extends Presenter<YourHistoryPresenter.YourHistoryView>
        implements AppReceiver.AppReceiverCallback {

    private static final int COMMAND_REMOVE = 0;

    private AppReceiver appReceiver = new AppReceiver();
    private INetWorkState netWorkState;
    private IDataBaseHelper dataBaseHelper;

    @Inject
    public YourHistoryPresenter(INetWorkState netWorkState, IDataBaseHelper dataBaseHelper) {
        this.netWorkState = netWorkState;
        this.dataBaseHelper = dataBaseHelper;
    }

    @Override
    public void attachView(@NonNull YourHistoryPresenter.YourHistoryView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void onHistoryRemove(List<HistoryKey> itemsToRemove, HistoryType type) {
        if(netWorkState.isLoggedIn()) {
            ClearHistoryCommand command =
                    new ClearHistoryCommand(itemsToRemove, type);
            ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_REMOVE);
        }else{
            dataBaseHelper.deleteHistory(TablesContentProvider.HISTORY_CONTENT_URI, itemsToRemove);

            if (getView() != null) {
                for (int i = 0; i < itemsToRemove.size(); i++) {
                    getView().onRemoved(itemsToRemove.get(i));
                }
                getView().onSuccessRemote();
                getView().onCloseEditMode();
            }
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == COMMAND_REMOVE) {

        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_REMOVE) {
            getView().onSuccessRemote();
            ClearHistoryCommand command = data.getParcelable(CommandExecutor.BUNDLE_CLEAR_HISTORY);
            removeItemsFromView(command);
        }
    }

    private void removeItemsFromView(ClearHistoryCommand command) {
        if (getView() != null) {
            for (int i = 0; i < command.getSids().size(); i++) {
                getView().onRemoved(command.getKeys().get(i));
            }

            getView().onCloseEditMode();
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public interface YourHistoryView extends IView {
        void onRemoved(HistoryKey removedId);

        void onCloseEditMode();

        void onSuccessRemote();

    }
}
