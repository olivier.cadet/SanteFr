package com.adyax.srisgp.mvp.around_me.filters;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHuge;
import com.adyax.srisgp.mvp.around_me.SearchFilterListener;
import com.adyax.srisgp.utils.FiltersHelper;
import com.adyax.srisgp.views.FiltersLinearLayout;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Kirill on 20.10.16.
 */

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.FiltersViewHolder> implements SearchFilterListener {
    private SearchType searchType;
    private FiltersHelper filtersHelper;
    private ArrayList<SearchFilter> currentFilters = new ArrayList<>();
    private ArrayList<SearchFilterHuge> visibleFilters = new ArrayList<>();

    private boolean isChanged = false;
    private OnChangedListener onChangedListener;

    public FiltersAdapter(FiltersHelper filtersHelper, SearchType searchType) {
        this.searchType = searchType;
        this.filtersHelper = filtersHelper;
        currentFilters.addAll(filtersHelper.getFiltersForSearch(searchType));
        updateVisibleFilters();
    }

    public void setOnChangedListener(OnChangedListener onChangedListener) {
        this.onChangedListener = onChangedListener;
    }

    public ArrayList<SearchFilter> getCurrentFilters() {
        return currentFilters;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    @Override
    public int getItemCount() {
        return visibleFilters.size();
    }

    @Override
    public FiltersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filters, parent, false);
        return new FiltersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FiltersViewHolder holder, int position) {
        SearchFilterHuge filter = visibleFilters.get(position);
        SearchFilter currentFilter = findCurrentFilter(filter.param);
        holder.bind(filter, currentFilter);
    }

    @Override
    public void onFilterChanged(SearchFilter newSearchFilter) {
        for (SearchFilter searchFilter : currentFilters) {
            if (searchFilter.getParam().equals(newSearchFilter.getParam())) {
                currentFilters.remove(searchFilter);
                break;
            }
        }
        currentFilters.add(newSearchFilter);
        updateVisibleFilters();

        isChanged = filtersHelper.isChanged(currentFilters, searchType);
        // On determine si le filtre changé doit déclencher une recherche automatique
        boolean launchSearch = searchType.searchAutomatic();
        if ("other-me".equals(newSearchFilter.getParam())) {
            if (newSearchFilter.getValue().size() > 0 && "1".equals(newSearchFilter.getValue().get(0))) {
                launchSearch = false;
            }
        }

        if (onChangedListener != null)
            onChangedListener.onFiltersChanged(isChanged, launchSearch);
    }

    private boolean isFilterVisible(SearchFilterHuge filterHuge) {
        if (filterHuge.getVisibilityCondition() == null)
            return true;

        String param = filterHuge.getVisibilityCondition().getParam();
        String value = filterHuge.getVisibilityCondition().getValue();
        SearchFilter searchFilter = findCurrentFilter(param);
        if (searchFilter != null) {
            for (String s : searchFilter.getValue()) {
                if (s.equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void updateVisibleFilters() {
        visibleFilters.clear();
        ArrayList<SearchFilterHuge> filters = filtersHelper.getFilters(searchType);
        for (SearchFilterHuge filterHuge : filters) {
            if (isFilterVisible(filterHuge))
                visibleFilters.add(filterHuge);
        }

        // TODO MRO 2019-07-10 : Order filters (use a filed order or other insteed of using hard coding)
        Collections.sort(visibleFilters, (filter1, filter2) -> {
            if ("other-me".equals(filter1.param) && "sex".equals(filter2.param))
                return -1;
            if ("other-me".equals(filter1.param) && "age".equals(filter2.param))
                return -1;
            if ("sex".equals(filter1.param) && "age".equals(filter2.param))
                return -1;

            return 0;
        });

        notifyDataSetChanged();
    }

    public class FiltersViewHolder extends RecyclerView.ViewHolder {

        TextView paramNameTextView;
        FiltersLinearLayout filtersLinearLayout;

        FiltersViewHolder(View itemView) {
            super(itemView);
            paramNameTextView = (TextView) itemView.findViewById(R.id.param_name);
            filtersLinearLayout = (FiltersLinearLayout) itemView.findViewById(R.id.filters_layout);
            filtersLinearLayout.setSearchFilterListener(FiltersAdapter.this);
        }

        public void resetValues() {
            filtersLinearLayout.clearAll();
        }

        public void bind(SearchFilterHuge filter, SearchFilter currentFilter) {
            //add margins to top and bottom if needed
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();

            //add top margin
            if (filter.isTopMargin()) {
                if (filter.doNotOpen == 1)
                    layoutParams.topMargin = itemView.getResources().getDimensionPixelSize(R.dimen.default_margin_x0_5);
                else
                    layoutParams.topMargin = itemView.getResources().getDimensionPixelSize(R.dimen.default_margin_x0_25);
                itemView.setPadding(itemView.getPaddingLeft(), itemView.getResources().getDimensionPixelSize(R.dimen.default_margin_x0_5),
                        itemView.getPaddingRight(), itemView.getPaddingBottom());
            } else {
                layoutParams.topMargin = 0;
                itemView.setPadding(itemView.getPaddingLeft(), 0,
                        itemView.getPaddingRight(), itemView.getPaddingBottom());
            }

            //add bottom margin
            if (filter.isBottomMargin()) {
                layoutParams.bottomMargin = itemView.getResources().getDimensionPixelSize(R.dimen.default_margin_x0_25);
                itemView.setPadding(itemView.getPaddingLeft(), itemView.getPaddingTop(), itemView.getPaddingRight(),
                        itemView.getResources().getDimensionPixelSize(R.dimen.default_margin_x0_5));
            } else {
                layoutParams.bottomMargin = 0;
                itemView.setPadding(itemView.getPaddingLeft(), itemView.getPaddingTop(), itemView.getPaddingRight(), 0);
            }

            itemView.setLayoutParams(layoutParams);

            if (filter.label == null || filter.label.isEmpty()) {
                paramNameTextView.setVisibility(View.GONE);
            } else {
                String paramName = filter.label;

                if (paramName.endsWith(":"))
                    paramName = paramName.substring(0, paramName.length() - 1);

//                if (BuildConfig.DEBUG)
//                    paramName += String.format(" (param: %s)", filter.param);

                paramNameTextView.setText(String.format("%s :", paramName));
                paramNameTextView.setVisibility(View.VISIBLE);
            }

            filtersLinearLayout.setFilter(filter, currentFilter);
        }
    }

    private SearchFilter findCurrentFilter(String param) {
        if (param == null || param.isEmpty())
            return null;

        for (SearchFilter searchFilter : currentFilters) {
            if (searchFilter.getParam().equals(param))
                return searchFilter;
        }

        return null;
    }

    public void reset() {
        currentFilters.clear();
        currentFilters.addAll(filtersHelper.getDefaultState(searchType));
        updateVisibleFilters();

        isChanged = filtersHelper.isChanged(currentFilters, searchType);
        if (onChangedListener != null) {
            onChangedListener.onFiltersChanged(isChanged, true);
        }

    }

    public void applyFilters() {
        if (isChanged) {
            filtersHelper.setSearchFilters(currentFilters, searchType);
        }
    }

    public boolean isChanged() {
        return isChanged;
    }

    public interface OnChangedListener {
        void onFiltersChanged(boolean isChanged, boolean launchSearch);
    }
}
