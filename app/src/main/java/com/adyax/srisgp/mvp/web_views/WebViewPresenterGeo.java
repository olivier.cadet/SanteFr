package com.adyax.srisgp.mvp.web_views;

import android.location.Location;

import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.framework.PresenterGeo;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/28/17.
 */

public class WebViewPresenterGeo extends PresenterGeo {

    @Inject
    public WebViewPresenterGeo(IGeoHelper geoHelper) {
        super(geoHelper);
    }

    @Override
    public void update(Location location) {

    }
}
