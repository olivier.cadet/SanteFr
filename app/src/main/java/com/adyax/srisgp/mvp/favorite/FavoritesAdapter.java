package com.adyax.srisgp.mvp.favorite;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class FavoritesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<FavoritesItem> favorites = new ArrayList<>();
    private OnItemClickedListener listener;
    private SwipeLayout.OnSwipeListener onSwipeListener;
    private SwipeCheckListener swipeCheckListener;

    public FavoritesAdapter(OnItemClickedListener listener) {
        this.listener = listener;
    }

    public void setOnSwipeListener(SwipeLayout.OnSwipeListener onSwipeListener) {
        this.onSwipeListener = onSwipeListener;
    }

    public void setSwipeCheckListener(SwipeCheckListener swipeCheckListener) {
        this.swipeCheckListener = swipeCheckListener;
    }

    public List<FavoritesItem> getFavorites() {
        return favorites;
    }

    public void addFavorites(List<FavoritesItem> favorites) {
        this.favorites.addAll(favorites);
        notifyDataSetChanged();
    }

    public void clearFavorites() {
        this.favorites.clear();
        notifyDataSetChanged();
    }

    public void selectAll() {
        notifyDataSetChanged();
    }

    public void requestRemove(FavoritesItem item) {
        if (listener != null) {
            listener.onRemove(item);
        }
    }

    public void removeItem(long id) {
        int itemPosition = getFavoriteItemPosition(id);
        if (itemPosition != -1) {
            favorites.remove(itemPosition);
            notifyItemRemoved(itemPosition);
            notifyDataSetChanged();
        }
    }

    private void removeFavoritesItem(FavoritesItem favoritesItem) {
        favorites.remove(favoritesItem);
        notifyDataSetChanged();
    }

    public void notifyItemChangedById(long id) {
        int itemPosition = getFavoriteItemPosition(id);
        if (itemPosition != -1) {
            notifyItemChanged(itemPosition);
        }
    }

    @Override
    public int getItemCount() {
        return favorites.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View notificationView = inflater.inflate(R.layout.item_notification, parent, false);
        return new FavoriteViewHolder(notificationView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FavoriteViewHolder viewHolder = (FavoriteViewHolder) holder;
        viewHolder.bind(getFavorite(position));
    }

    private int getFavoriteItemPosition(long id) {
        FavoritesItem item;
        for (int i = 0; i < favorites.size(); i++) {
            item = favorites.get(i);
            if (id == item.nid) {
                return i;
            }
        }
        return -1;
    }

    private boolean isLoadingItem(FavoritesItem item) {
        return listener.isLoadingItem(item);
    }

    private boolean isSelectedItem(FavoritesItem item) {
        return listener.isSelectedItem(item);
    }

    public void changeSelectionMode(FavoritesItem item) {
        int itemPosition = getFavoriteItemPosition(item.nid);
        if (itemPosition != -1) {
            notifyItemChanged(itemPosition);
        }
    }

    private FavoritesItem getFavorite(int position) {
        return favorites.get(position);
    }

    class FavoriteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.ivArrow)
        ImageView ivArrow;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.llContent)
        LinearLayout llContent;
        @BindView(R.id.item_delete)
        View deleteView;
        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;

        FavoriteViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(FavoritesItem item) {
            if (!isLoadingItem(item)) {
                final String getIcon_url = item.getIcon_url();
                if (getIcon_url != null) {
                    Glide.with(ivIcon.getContext())
                            .load(getIcon_url)
                            .error(item.getIconResource().getResourceId())
                            .into(ivIcon);
                } else {
                    ivIcon.setImageResource(item.getIconResource().getResourceId());
                }

                swipeLayout.setVisibility(View.VISIBLE);
                swipeLayout.reset();
                progressBar.setVisibility(View.GONE);
                if (listener.isEditMode()) {
                    checkBox.setVisibility(View.VISIBLE);
                    ivArrow.setVisibility(View.INVISIBLE);

                    if (isSelectedItem(item)) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                    swipeLayout.setSwipeEnabled(false);
                } else {
                    checkBox.setVisibility(View.GONE);
                    ivArrow.setVisibility(View.VISIBLE);
                    swipeLayout.setSwipeEnabled(true);
                }

                llContent.setOnClickListener(v -> {
                    if (swipeLayout.getOffset() != 0) {
                        swipeLayout.animateReset();
                    } else {
                        if (swipeCheckListener == null || swipeCheckListener.isAllSwipeClosed(swipeLayout)) {
                            if (listener.isEditMode()) {
                                listener.onSelect(item);
                            } else {
                                listener.onItemClicked(item);
                            }
                        }
                    }
                });

                tvTitle.setText(item.getTitle());
                tvDate.setText(App.getAppContext().getString(R.string.favorite_added, item.date_added));

                swipeLayout.setOnSwipeListener(onSwipeListener);
            } else {
                swipeLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            deleteView.setOnClickListener(v -> requestRemove(item));

            swipeLayout.reset();
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(FavoritesItem item);

        void onRemove(FavoritesItem item);

        void onSelect(FavoritesItem item);

        boolean isLoadingItem(FavoritesItem item);

        boolean isSelectedItem(FavoritesItem item);

        boolean isEditMode();
    }

    public interface SwipeCheckListener {
        boolean isAllSwipeClosed(SwipeLayout swipeLayout);
    }
}
