package com.adyax.srisgp.mvp.my_account;

import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.data.net.response.tags.IntIdValuePair;
import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.repository.IRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 7/10/17.
 */

class  KeyWordsHelper{

    private ArrayList<TermsItem> keywords;
    private Map<Long, TermsItem> setOfManualInterests=new HashMap<>();
    private IRepository repository;

    public KeyWordsHelper(IRepository repository) {
        this.repository = repository;
    }

    public ArrayList<TermsItem> removeChecked() {
        ArrayList<TermsItem> result = new ArrayList<>();
        User user = repository.getCredential().user;
        if (user != null) {

            ArrayList<IntIdValuePair> currentManualInterests = user.getManualInterests();
            ArrayList<IntIdValuePair> searchInterests = user.getSearchInterests();
            for (TermsItem item : getKeywords()) {
                boolean needToAdd = true;

                if (currentManualInterests != null) {
                    for (IntIdValuePair manualItem : currentManualInterests) {// TODO nullPointerException
                        if (manualItem.id == item.tid) {
                            needToAdd = false;
                            break;
                        }
                    }
                }

                if (searchInterests != null) {
                    if (needToAdd) {
                        for (IntIdValuePair manualItem : searchInterests) {
                            if (manualItem.id == item.tid) {
                                needToAdd = false;
                                break;
                            }
                        }
                    }
                }

                if (needToAdd) {
                    result.add(item);
                }
            }

            for (Long key:setOfManualInterests.keySet()){
                boolean needToAdd=true;

                for(IntIdValuePair valuePair: currentManualInterests){
                    final Long id = Long.valueOf(valuePair.id);
                    if(key.equals(id)){
                        needToAdd=false;
                        break;
                    }
                }

                if (searchInterests != null) {
                    if (needToAdd) {
                        for(IntIdValuePair valuePair: searchInterests){
                            final Long id = Long.valueOf(valuePair.id);
                            if(key.equals(id)){
                                needToAdd=false;
                                break;
                            }
                        }
                    }
                }

                if(needToAdd){
                    result.add(setOfManualInterests.get(key));
                }
            }

        }
        sortKeywords(result);
        return result;
    }

    private void sortKeywords(ArrayList<TermsItem> items) {
        User user = repository.getCredential().user;
        Map<Long, Integer> interestsNodeCount = repository.getInterestCount();
        if (user != null && items != null && interestsNodeCount != null) {
            Collections.sort(items, (first, second) -> {
                final int compare = -interestsNodeCount.get(first.tid).compareTo(interestsNodeCount.get(second.tid));
                if (compare == 0) {
                    return first.name.toLowerCase().compareTo(second.name.toLowerCase());
                }
                return compare;
            });
        }
    }

    public void setUncheckLocalInterest(TaxonomyVocabularyTermsResponse termsResponse) {
        setOfManualInterests = termsResponse.getData_map();
    }

    private ArrayList<TermsItem> getKeywords() {
        return keywords;
    }

    public void setKeywords(ArrayList<TermsItem> keywords) {
        this.keywords = keywords;
    }

    public boolean isKeywords() {
        return getKeywords()!=null;
    }

}