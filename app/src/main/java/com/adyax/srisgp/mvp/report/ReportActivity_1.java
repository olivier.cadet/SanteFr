package com.adyax.srisgp.mvp.report;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityFeedbackBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportActivity_1 extends MaintenanceBaseActivity {
//    private static final String INTENT_URL = "intent_url";
//    private static final String INTENT_FORM_NAME = "intent_form_name";

    private ActivityFeedbackBinding binding;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, WebViewArg webViewArg) {
        Intent intent = new Intent(context, ReportActivity_1.class);
//        intent.putExtra(ReportFormValue.FIELD_URL, webViewArg.getShortNode());
        intent.putExtra(WebViewArg.CONTENT_URL, webViewArg);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);
        setSupportActionBar(binding.toolbar);
        binding.toolbar.setPadding(0, 0, 0, 0);
        binding.toolbar.setTitleMarginStart(0);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        WebViewArg webViewArg = getIntent().getParcelableExtra(WebViewArg.CONTENT_URL);
        final String reportFormName = webViewArg.getReportFormName();
//        if (reportFormName != null) {
//            final ReportFormType reportFormType = new ReportFormType().fromInapp(reportFormName);
//            if (ReportFormType.FIND_CONTENT_ERROR_FORM_NAME.equals(reportFormType.getNameFormType())) {
//                if (webViewArg.isFiche()) {
//                    fragmentFactory.startReportFindFicheFragment(this, webViewArg);
//                } else {
//                    fragmentFactory.startReportFindUserFragment(this, webViewArg);
//                }
//            } else if (ReportFormType.INFO_CONTENT_ERROR_FORM_NAME.equals(reportFormType.getNameFormType())) {
//                fragmentFactory.startReportInfoFragment(this, webViewArg.getShortNode());
//            }
//        } else {
            if (webViewArg.isFindTypeForm()) {
                if (webViewArg.isFiche()) {
                    fragmentFactory.startReportFindFicheFragment(this, webViewArg);
                } else {
                    fragmentFactory.startReportFindUserFragment(this, webViewArg);
                }
            } else {
                fragmentFactory.startReportInfoFragment(this, webViewArg.getShortNode());
            }
//        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
