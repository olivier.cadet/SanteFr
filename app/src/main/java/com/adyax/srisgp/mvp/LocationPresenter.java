package com.adyax.srisgp.mvp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.framework.NetworkPresenter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 10/27/16.
 */

public abstract class LocationPresenter<V extends LocationView> extends NetworkPresenter<V> {

    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private boolean requestAndReturnLocation = false;
    public GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation =null;
    protected Location location;
    protected IRepository repository;

    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                getView().onEnableLocationButtonStateChanged();
            }
        }
    };

    public LocationPresenter(IRepository repository) {
        this.repository = repository;
    }

    @Override
    public void attachView(@NonNull V view) {
        super.attachView(view);
        getContext().registerReceiver(gpsReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
    }

    @Override
    public void detachView() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
        getContext().unregisterReceiver(gpsReceiver);

        super.detachView();
    }

    public void updateStatus(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        this.statusLocation= statusLocation;
    }

    public void onRequestCurrentLocation(boolean requestAndReturnLocation) {//AAA1
        updateStatus(GetAutoCompleteSearchLocationsResponse.StatusLocation.createEmpty());
        onRequestCurrentLocationMain(requestAndReturnLocation, false);
    }

    public boolean onRequestCurrentLocation2(boolean requestAndReturnLocation, boolean isDisableSendSearch) {//AAA1
        if(statusLocation==null) {
            updateStatus(GetAutoCompleteSearchLocationsResponse.StatusLocation.createEmpty());
        }
        return onRequestCurrentLocationMain(requestAndReturnLocation, isDisableSendSearch);
    }

    public void onRequestCurrentLocation(boolean requestAndReturnLocation, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        updateStatus(statusLocation);
        onRequestCurrentLocationMain(requestAndReturnLocation, false);
    }

    public boolean onRequestCurrentLocationMain(boolean requestAndReturnLocation, boolean isDisableSendSearch) {
        boolean bResult=false;
        this.requestAndReturnLocation = requestAndReturnLocation;
        if (googleApiClient == null || !googleApiClient.isConnected()) {
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addApi(LocationServices.API)//***1
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            final Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);//***8
//                            if (location==null&& BuildConfig.DEBUG) {
//                                location=new Location("fused");
//                                location.setLatitude(AroundMeArguments.TOURS_LATITUDE);
//                                location.setLongitude(AroundMeArguments.TOURS_LONGITUDE);
//                            }
                            repository.saveGeoLocation(location);//+
                            // Note that this can be NULL if last location isn't already known.
                            if (requestAndReturnLocation && location != null) {
                                setCurrentCityByLocation(location, false);
                            }
                            // Begin polling for new location updates.
                            startLocationUpdates();
                            getView().updateLocation(location);
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    })
                    .build();
            googleApiClient.connect();
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            repository.saveGeoLocation(location);//-
            if (requestAndReturnLocation && location != null) {
                bResult=setCurrentCityByLocation(location, isDisableSendSearch);
            } else {
                startLocationUpdates();
            }
        }
        return bResult;
    }

    private void startLocationUpdates() {
        // Create the location request
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(1000);
        // Request location updates

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                V view = getView();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        if(view!=null) {
                            view.onEnableLocationButtonStateChanged();
                        }
                        if (requestAndReturnLocation) {
                            requestLocationUpdates();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        if(view!=null) {
                            view.showEnableLocationDialog(status);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (requestAndReturnLocation) {
                setCurrentCityByLocation(location, false);
                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            }
        }
    };

    private void requestLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                    locationRequest, locationListener);
        }catch (java.lang.SecurityException e){
            Timber.e(e);
        }
    }

    private void removeLocationListener() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, locationListener);
    }


    public void onPromptSettingSuccess(boolean requestAndReturnLocation) {
        this.requestAndReturnLocation = requestAndReturnLocation;
        getView().onEnableLocationButtonStateChanged();
        requestLocationUpdates();
    }

    private boolean setCurrentCityByLocation(Location location, boolean isDisableSendSearch) {
        removeLocationListener();
        final boolean bResult=setCurrentLocation(location, statusLocation, isDisableSendSearch);
        requestAndReturnLocation = false;
        this.location = location;
        return bResult;
    }

    protected abstract boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch);
}
