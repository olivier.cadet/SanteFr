package com.adyax.srisgp.mvp.web_views;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public enum WhitelistHostsVerifier implements HostnameVerifier {
    // these hosts get whitelisted
    INSTANCE("localhost", "santefr.dev.klee.lan.net", "santefr.integration.asipsante.fr");

    private Set<Object> whitelist = new HashSet<>();
    private HostnameVerifier defaultHostnameVerifier =
            HttpsURLConnection.getDefaultHostnameVerifier();

    WhitelistHostsVerifier(String... hostnames) {
        Collections.addAll(whitelist, hostnames);
    }

    @Override
    public boolean verify(String host, SSLSession session) {
        if (whitelist.contains(host)) {
            return true;
        }
        // Revert validator for all other hosts !
        return defaultHostnameVerifier.verify(host, session);
    }
}
