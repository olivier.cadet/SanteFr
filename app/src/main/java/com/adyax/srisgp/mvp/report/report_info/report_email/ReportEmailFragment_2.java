package com.adyax.srisgp.mvp.report.report_info.report_email;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.databinding.FragmentReport2YourEmailBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportEmailFragment_2 extends ViewFragment implements ReportEmailPresenter_2.ReportPresenter_2View {

    public static final String TITLE = "title";

    @Inject
    ReportEmailPresenter_2 reportPresenter1;

    private FragmentReport2YourEmailBinding binding;
    private int title;

    public ReportEmailFragment_2() {
    }

    public static Fragment newInstance(int title, ReportBuilder builder) {
        ReportEmailFragment_2 fragment = new ReportEmailFragment_2();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER, builder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        title = getArguments().getInt(TITLE);
        reportPresenter1.setBuilder(getArguments().getParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report2_your_email, container, false);
        binding.backToCard.setOnClickListener(view -> {
            reportPresenter1.getBuilder().set(ReportBuilder.FIELD_EMAIL, null);
            reportPresenter1.clickBackToCard();
        });

        binding.reportErrorInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                reportPresenter1.onEmailTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.btnSubmit.setOnClickListener(v -> {
            reportPresenter1.getBuilder().set(ReportBuilder.FIELD_EMAIL, binding.reportErrorInfo.getText().toString().trim());
            reportPresenter1.onSubmitButtonClicked(binding.reportErrorInfo.getText().toString().trim());
        });

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
//        ((Toolbar)((AppCompatActivity) getActivity()).getSupportActionBar().getCustomView().getParent()).setPadding(0, 0, 0, 0);
        reportPresenter1.onViewCreated();

    }

    @Override
    public void setInvalidEmailError() {
        binding.reportErrorInfo.setError(getString(R.string.change_credentials_error_wrong_email));
    }

    @Override
    public void setSubmitButtonVisibility(boolean visible) {
        binding.btnSubmit.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setReturnButtonVisibility(boolean visible) {
        binding.backToCard.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showEmail(String email) {
        binding.reportErrorInfo.setText(email);
    }

    @NonNull
    @Override
    public ReportEmailPresenter_2 getPresenter() {
        return reportPresenter1;
    }

    @Override
    public void showCloseDialog() {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.close_report)
                .setCancelable(false)
                .setPositiveButton(R.string.text214, (dialog, which) -> {
                    dialog.dismiss();
                    getActivity().finish();
                })
                .show();
    }

    @Override
    public void onBack() {
        getActivity().finish();
    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
        binding.backToCard.setClickable(false);
    }

    @Override
    public void hideProgress() {
        binding.progressLayout.setVisibility(View.GONE);
        binding.backToCard.setClickable(true);
    }
}
