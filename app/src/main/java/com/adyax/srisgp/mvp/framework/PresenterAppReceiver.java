package com.adyax.srisgp.mvp.framework;

import android.os.Bundle;

import com.adyax.srisgp.data.net.executor.AppReceiver;

/**
 * Created by SUVOROV on 10/4/16.
 */

public class PresenterAppReceiver<V extends IView> extends Presenter<V> implements AppReceiver.AppReceiverCallback {
    private AppReceiver appReceiver =  new AppReceiver();

    @Override
    public void attachView(V view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        appReceiver.setListener(null);
    }

    public AppReceiver getAppReceiver() {
        return appReceiver;
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {

    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }
}
