package com.adyax.srisgp.mvp.around_me;

import android.text.TextUtils;

import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by SUVOROV on 8/29/17.
 */

public class SearchItemGroup implements ISearchItem{

    private final int offsetPosition;

    private List<SearchItemHuge> items =new LinkedList<>();

    public SearchItemGroup(int offsetPosition, SearchItemHuge item) {
        this.offsetPosition = offsetPosition;
        add(item);
    }

    public SearchItemGroup add(SearchItemHuge item) {
        items.add(item);
        return this;
    }

    @Override
    public String getSubtitle() {
//        if(isGroup()){
            return items.get(0).getSubtitle();
//        }
//        return items.get(0).getSubtitle();
    }

    @Override
    public String getAddress() {
//        if(isGroup()){
        return items.get(0).getAddress();
//        }
//        return items.get(0).getSubtitle();
    }

    @Override
    public boolean isGroup() {
        return items.size()>1;
    }

    @Override
    public String getGroupCount() {
        return String.valueOf(items.size());
    }

    @Override
    public boolean contains(long id) {
        for(SearchItemHuge item:items){
            if(item.getId()==id){
                return true;
            }
        }
        return false;
    }

    @Override
    public String getTitle() {
        if(isGroup()){
            return String.valueOf(items.size());
        }
        return items.get(0).getTitle();
    }

    @Override
    public IGetUrl getItem() {
        return items.get(0);
    }

    @Override
    public LatLng getLatLng() {
        return items.get(0).getLatLng();
    }

    public List<SearchItemHuge> getItems() {
        return items;
    }

    public int getOffsetPosition() {
        return offsetPosition;
    }

    public long getActiveId() {
        return items.get(0).getId();
    }

    public boolean exists(long activeId) {
        for(SearchItemHuge item:items){
            if(item.getId()==activeId){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEquals(Marker marker) {
        if (getLatLng().equals(marker.getPosition())) {
            if(equalsMarkerString(marker.getTitle(), getTitle())) {
//                if (isGroup()) {
//                    if (equalsMarkerString(marker.getSnippet(), getSubtitle())) {
//                        return true;
//                    }
//                } else {
//                    return true;
//                }
                String snippet = marker.getSnippet();
                if (isGroup()) {
                    if(snippet!=null && snippet.startsWith(AroundMeFragment.GROUP_MARKER)){
                        snippet=snippet.substring(AroundMeFragment.GROUP_MARKER.length());
                    }
                }
                if (equalsMarkerString(snippet, getAddress())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public long getId() {
        return -1;
    }

    private boolean equalsMarkerString(CharSequence a, CharSequence b) {
        if (a == null && b == null) {
            return true;
        }
        return TextUtils.equals(a, b);
    }

}
