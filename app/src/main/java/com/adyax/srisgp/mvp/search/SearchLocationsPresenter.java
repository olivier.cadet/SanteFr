package com.adyax.srisgp.mvp.search;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.ClearRecentSearchLocationsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchLocationsCommand;
import com.adyax.srisgp.data.net.command.GetRecentSearchLocationCommand;
import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.RecentLocationsResponse;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.LocationPresenter;
import com.adyax.srisgp.mvp.LocationView;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 11/1/16.
 */

public class SearchLocationsPresenter extends LocationPresenter<SearchLocationsPresenter.SearchLocationsView> implements AppReceiver.AppReceiverCallback {
//    public static final int STATE_LOCATION_DEFAULT = 2;
//    public static final int STATE_LOCATION_SEARCH = 3;

    private static final int COMMAND_CLEAR_SEARCH_LOCATIONS = 5;
    private static final int COMMAND_GET_RECENT_LOCATIONS = 7;
    private static final int COMMAND_GET_LOCATIONS_AUTOCOMPLETE = 8;
    private static final int COMMAND_REMOVE_SEARCH_LOCATION = 9;
    private Object syncGetAutoCompleteSearchLocationsResponse=new Object();
    private GetAutoCompleteSearchLocationsResponse autoCompleteSearchLocations;
    private List<RecentLocationViewModel> recentLocations;

//    @Retention(RetentionPolicy.SOURCE)
//    @IntDef({STATE_LOCATION_DEFAULT, STATE_LOCATION_SEARCH})
//    public @interface SearchCurrentState {}
//    private @SearchCurrentState
//    int currentState = STATE_LOCATION_DEFAULT;

    private Handler locationHandler = new Handler();

    private boolean autocompleteSelected = false;
    private String textFromAutoComplete;

    public SearchArgument getSearchArgument() {
        return searchArgument;
    }

    private SearchArgument searchArgument;
    //    private AppReceiver appReceiver = new AppReceiver();

    @Inject
    public SearchLocationsPresenter(IRepository repository) {
        super(repository);
    }

//    @Override
//    public void attachView(@NonNull SearchLocationsView view) {
//        super.attachView(view);
//        appReceiver.setListener(this);
//    }
//
//    @Override
//    public void detachView() {
//        appReceiver.setListener(null);
//        super.detachView();
//    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        switch (requestCode) {
            case COMMAND_GET_LOCATIONS_AUTOCOMPLETE:
                updateAutoCompleteSearchLocationsResponse(null);
                break;
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case COMMAND_REMOVE_SEARCH_LOCATION:
                List<Long> removedIds = (List<Long>) data.getSerializable(CommandExecutor.BUNDLE_REMOVE_SEARCH_IDS);
                if (removedIds != null && !removedIds.isEmpty()) {
                    long removedId = removedIds.get(0);
                    RecentLocationViewModel searchItemViewModel;
                    for (int i = 0; i < recentLocations.size(); i++) {
                        searchItemViewModel = recentLocations.get(i);
                        if (removedId == recentLocations.get(i).getId()) {
                            getView().onRecentSearchLocationRemoved(searchItemViewModel);
                            return;
                        }
                    }
                }
                break;
            case COMMAND_CLEAR_SEARCH_LOCATIONS:
                getView().onSearchLocationsHistoryRemoved();
                break;
            case COMMAND_GET_LOCATIONS_AUTOCOMPLETE:
                final GetAutoCompleteSearchLocationsResponse autoCompleteSearchLocationsResponse =
                        data.getParcelable(CommandExecutor.BUNDLE_GET_AUTOCOMPLETE_SEARCH_LOCATION);
                updateAutoCompleteSearchLocationsResponse(autoCompleteSearchLocationsResponse);
                final String locationText = getView().getLocationText();
                if (getView() != null && isLocationState() && SearchHelper.isSearchState(locationText)) {
                    if(!TextUtils.equals(textFromAutoComplete, autoCompleteSearchLocationsResponse.getLocation())) {
                        if (!autoCompleteSearchLocations.locations.isEmpty()
                            /*&&!TextUtils.equals(autoCompleteSearchLocations.location, locationText)*/) {
                            showLocationSearchState(autoCompleteSearchLocations);
                        } else {
                            showLocationDefaultState();
                        }
                    }else {
                        textFromAutoComplete = null;
                        showLocationDefaultState();
                        getView().disableSearchSuggestionAdapter();
                    }
                }
                break;
            case COMMAND_GET_RECENT_LOCATIONS:
                RecentLocationsResponse locationsResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_RECENT_LOCATIONS);
                recentLocations = transformRecentLocationItems(locationsResponse.items);
                if ((getView() != null) && isLocationState()) {
                    showLocationDefaultState();
                }
                break;
        }
    }

    private List<RecentLocationViewModel> transformRecentLocationItems(List<RecentLocationItem> items) {
        List<RecentLocationViewModel> viewModels = new ArrayList<>(items.size());
        for (int i = 0; i < items.size(); i++) {
            viewModels.add(new RecentLocationViewModel(items.get(i)));
        }
        return viewModels;
    }

    private boolean isLocationState() {
        if (getView() != null) {
            return getView().isLocationFieldHasFocus();
        }
        return false;
    }

    public Location getLocation() {
        return location;
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    @Override
    protected boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch) {
        getView().setCurrentLocation(location, statusLocation);
        return false;
    }

    public void loadAutocompleteData() {
        getRecentLocations();
    }

    public void onLocationFieldFocus(String text) {
//        if (SearchHelper.isSearchState(text)) {
//            if (autoCompleteSearchLocations == null) {
//                getLocationsAutocomplete(text);
//            } else {
//                showLocationSearchState(autoCompleteSearchLocations);
//            }
//        } else {
        showLocationDefaultState();
//        }
    }

    public void onSearchLocationSelected(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation2) {
        if (getView() != null) {
            autocompleteSelected = true;
            String locationText = statusLocation2.getLocation();//AAA
            GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation = autoCompleteSearchLocations != null ?
                    autoCompleteSearchLocations.getStatusLocation(locationText) : new GetAutoCompleteSearchLocationsResponse.StatusLocation(locationText);
            getView().onSelectSearchLocationEntry(statusLocation, repository.getConfig(), false);
        }
    }

    public void onLocationFieldTextChanged(String locationText) {//1
        autocompleteSelected = false;
        if (SearchHelper.isSearchState(locationText)) {
            locationHandler.removeCallbacksAndMessages(null);
            locationHandler.postDelayed(() -> {
                if (getView() != null) {
                    ExecutionService.sendCommand(getContext(), appReceiver,
                            new GetAutoCompleteSearchLocationsCommand(locationText), COMMAND_GET_LOCATIONS_AUTOCOMPLETE);
                }
            }, SearchHelper.SEARCH_DELAY);
        } else {
            showLocationDefaultState();
        }
    }

//    private void getLocationsAutocomplete(String locationText) {
//        ServiceCommand command = new GetAutoCompleteSearchLocationsCommand(locationText);
//        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_GET_LOCATIONS_AUTOCOMPLETE);
//    }


    public void showLocationDefaultState() {
        if (recentLocations != null) {
//            currentState = STATE_LOCATION_DEFAULT;
            if (getView() != null) {
                getView().showLocationsDefaultState(recentLocations);
            }
        }
    }

    private void showLocationSearchState(GetAutoCompleteSearchLocationsResponse response) {
//        currentState = STATE_LOCATION_SEARCH;
        if (getView() != null && response != null) {
            getView().showLocationsSearchState(response, repository.getConfig());
        }
    }

    public void onSearchClicked(String location_text) {
        if (!autocompleteSelected && autoCompleteSearchLocations != null) {
            final List<String> locations = autoCompleteSearchLocations.locations;
            Log.d("onSearchClicked", "locations size:"+locations.size());
            if (locations != null) {
                switch (locations.size()) {
                    case 0:
                        if (getView() != null) {
                            Log.d("onSearchClicked", "onSearchClicked");
                            getView().onAutoCompleteSearchLocationsIsEmpty();
                        }
                        break;
                    case 1:
                        // in case we got only one suggestion variant we work with it
                        if (getView() != null) {
                            getView().onSelectSearchLocationEntry(autoCompleteSearchLocations.getStatusLocation(0), repository.getConfig(), true);
                        }
                        break;
                    default:
                        // in case we have a lot of suggestion we try to find match one
                        if(!autoCompleteSearchLocations.existsLocation(location_text)){
                            if (getView() != null) {
                                getView().onAutoCompleteSearchLocationsIsEmpty();
                            }
                        }
                        break;
                }
            }
        }else{
            Log.d("onSearchClicked", "locations is null");
        }
    }

    public void removeSearchLocation(RecentLocationViewModel searchItem) {
        if (getView() != null)
            SearchHelper.removeSearchEntry(getContext(), appReceiver, searchItem.getId(), COMMAND_REMOVE_SEARCH_LOCATION, RemoveSearchItemCommand.ITEM_LOCATION);
    }

    public void removeSearchLocationsHistory() {
        if (getView() != null) {
            ExecutionService.sendCommand(getContext(), appReceiver, new ClearRecentSearchLocationsCommand(), COMMAND_CLEAR_SEARCH_LOCATIONS);
        }
    }

    private void getRecentLocations() {
        if (getView() != null) {
            ExecutionService.sendCommand(getContext(), appReceiver, new GetRecentSearchLocationCommand(), COMMAND_GET_RECENT_LOCATIONS);
        }
    }

    public void setSearchArgument(SearchArgument searchArgument) {
        this.searchArgument = searchArgument;
    }

    public interface SearchLocationsView extends LocationView {
        void onSearchLocationsHistoryRemoved();

        void onRecentSearchLocationRemoved(RecentLocationViewModel searchItem);

        void showLocationsDefaultState(List<RecentLocationViewModel> recentLocations);

        void showLocationsSearchState(GetAutoCompleteSearchLocationsResponse autocompleteItems, ConfigurationResponse config);

        void setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation);

        void onEnableLocationButtonStateChanged();

        void showEnableLocationDialog(Status status);

        void onSelectSearchLocationEntry(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, ConfigurationResponse config, boolean bAutoSelect);

        boolean isLocationFieldHasFocus();

        String getLocationText();

        void onAutoCompleteSearchLocationsIsEmpty();

        void disableSearchSuggestionAdapter();
    }

    public boolean isLocationUpdates(String location_text) {
        final String trim = location_text.trim();
        if(trim.length()>0) {
            if(!trim.equals(getString(R.string.search_around_me))) {
                synchronized (syncGetAutoCompleteSearchLocationsResponse) {
                    return autoCompleteSearchLocations != null && TextUtils.equals(autoCompleteSearchLocations.location, location_text);
                }
            }
        }
        return true;
    }

    private void updateAutoCompleteSearchLocationsResponse(GetAutoCompleteSearchLocationsResponse autoCompleteSearchLocationsResponse) {
        synchronized (syncGetAutoCompleteSearchLocationsResponse) {
            autoCompleteSearchLocations = autoCompleteSearchLocationsResponse;
        }
    }

    public void setTextFromAutoComplete(String textFromAutoComplete) {
        this.textFromAutoComplete = textFromAutoComplete;
    }
}
