package com.adyax.srisgp.mvp.notification;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Created by anton.kobylianskiy on 9/28/16.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<NotificationItem> notifications = new ArrayList<>();
    private OnItemClickedListener listener;
    private SwipeLayout.OnSwipeListener onSwipeListener;
    private SwipeCheckListener swipeCheckListener;

    public NotificationsAdapter(OnItemClickedListener listener) {
        this.listener = listener;
    }

    public List<NotificationItem> getNotifications() {
        return notifications;
    }

    public void setOnSwipeListener(SwipeLayout.OnSwipeListener onSwipeListener) {
        this.onSwipeListener = onSwipeListener;
    }

    public void setSwipeCheckListener(SwipeCheckListener swipeCheckListener) {
        this.swipeCheckListener = swipeCheckListener;
    }

    public void setAsRead(long id) {
        int notificationItemPosition = getNotificationItemPosition(id);
        if (notificationItemPosition != -1) {
            NotificationItem notificationItem = notifications.get(notificationItemPosition);
            notificationItem.setRead(true);
            notifyItemChanged(notificationItemPosition);
        }
    }

    public void addNotifications(List<NotificationItem> notifications, boolean firstPage) {
        if (firstPage) {
            this.notifications.clear();
        }
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void selectAll() {
        notifyDataSetChanged();
    }

    public void requestRemove(NotificationItem item) {
        if (listener != null) {
            listener.onRemove(item);
        }
    }

    public void removeItem(long id) {
        int notificationItemPosition = getNotificationItemPosition(id);
        if (notificationItemPosition != -1) {
            notifications.remove(notificationItemPosition);
            notifyItemRemoved(notificationItemPosition);
            notifyDataSetChanged();
        }
    }

    private void removeNotification(NotificationItem notificationItem) {
        notifications.remove(notificationItem);
        notifyDataSetChanged();
    }

    public void notifyItemChangedById(long id) {
        int notificationItemPosition = getNotificationItemPosition(id);
        if (notificationItemPosition != -1) {
            notifyItemChanged(notificationItemPosition);
        }
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View notificationView = inflater.inflate(R.layout.item_notification, parent, false);
        return new NotificationViewHolder(notificationView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NotificationViewHolder viewHolder = (NotificationViewHolder) holder;
        viewHolder.bind(getNotification(position), listener);
    }

    private int getNotificationItemPosition(long id) {
        NotificationItem item;
        for (int i = 0; i < notifications.size(); i++) {
            item = notifications.get(i);
            if (id == item.nid) {
                return i;
            }
        }
        return -1;
    }

    private boolean isLoadingItem(NotificationItem item) {
        return listener.isLoadingItem(item);
    }

    private boolean isSelectedItem(NotificationItem item) {
        return listener.isSelectedItem(item);
    }

    public void changeSelectionMode(NotificationItem item) {
        int notificationItemPosition = getNotificationItemPosition(item.nid);
        if (notificationItemPosition != -1) {
            notifyItemChanged(notificationItemPosition);
        }
    }

    private NotificationItem getNotification(int position) {
        return notifications.get(position);
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.ivArrow)
        ImageView ivArrow;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.llContent)
        LinearLayout llContent;
        @BindView(R.id.item_delete)
        View deleteView;
        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;

        NotificationViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(NotificationItem item, OnItemClickedListener listener) {
            ivIcon.setImageResource(item.getIconResource());
            if (item.isRead()) {
                itemView.setBackgroundResource(R.color.material_white);
            } else {
                itemView.setBackgroundResource(R.color.bgNotificationLightGreen);
            }

            if (!isLoadingItem(item)) {
                deleteView.setVisibility(View.VISIBLE);
                swipeLayout.setVisibility(View.VISIBLE);
                swipeLayout.reset();
                progressBar.setVisibility(View.GONE);
                if (listener.isEditMode()) {
                    checkBox.setVisibility(View.VISIBLE);

                    if (isSelectedItem(item)) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }

                    ivArrow.setVisibility(View.INVISIBLE);
                    swipeLayout.setSwipeEnabled(false);
                } else {
                    checkBox.setVisibility(View.GONE);
                    ivArrow.setVisibility(View.VISIBLE);
                    swipeLayout.setSwipeEnabled(true);
                }

                llContent.setOnClickListener(v -> {
                    if (swipeLayout.getOffset() != 0) {
                        swipeLayout.animateReset();
                    } else {
                        if (swipeCheckListener == null || swipeCheckListener.isAllSwipeClosed(swipeLayout)) {
                            if (listener.isEditMode()) {
                                listener.onSelect(item);
                            } else {
                                listener.onItemClicked(item);
                            }
                        }
                    }
                });

                tvTitle.setText(item.getTitle());
                tvDate.setText(App.getAppContext().getString(R.string.notification_received_on, item.created));

                swipeLayout.setOnSwipeListener(onSwipeListener);
            } else {
                deleteView.setVisibility(View.GONE);
                swipeLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            deleteView.setOnClickListener(v -> requestRemove(item));

            swipeLayout.reset();
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(NotificationItem item);

        void onRemove(NotificationItem item);

        void onSelect(NotificationItem item);

        boolean isLoadingItem(NotificationItem item);

        boolean isSelectedItem(NotificationItem item);

        boolean isEditMode();
    }

    public interface SwipeCheckListener {
        boolean isAllSwipeClosed(SwipeLayout swipeLayout);
    }
}
