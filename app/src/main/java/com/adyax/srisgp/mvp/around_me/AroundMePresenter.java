package com.adyax.srisgp.mvp.around_me;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.GetFiltersCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.SgpLocationCompleteCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.LocationPresenter;
import com.adyax.srisgp.mvp.LocationView;
import com.adyax.srisgp.mvp.common.FindCardListener;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.search.FiltersActivity;
import com.adyax.srisgp.mvp.searchresults.SearchMessageLoader;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.utils.DialogUtils;
import com.adyax.srisgp.utils.FiltersHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class AroundMePresenter extends LocationPresenter<AroundMePresenter.AroundMeView>
        implements LoaderManager.LoaderCallbacks<Cursor>, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, FindCardListener {

    private static final SearchType SEARCH_TYPE = SearchType.around;
    public static final int CHOICE_DATABASE_LOADER_ID = 32;
    public static final int LOADER_MESSAGE = 33;

    public static final boolean START_SEARCH_IMMEDIATLY = true;
    public static final boolean START_SEARCH = !START_SEARCH_IMMEDIATLY;

    //Injected
    private FragmentFactory fragmentFactory;
    private FiltersHelper filtersHelper;
    private IDataBaseHelper dataBaseHelper;
    private INetWorkState netWorkState;


    private final SearchRequestHelper searchRequestHelper;

    private GoogleApiClient googleApiClient;
    private AdvertAdapter advertAdapter;
    private IdfNotificationsAdapter idfAdapter;
    private Loader.ForceLoadContentObserver forceLoadContentObserver;
    private ITracker tracker;
    private SearchMessageLoader messageLoader;

    private boolean fromServer = false;
    private boolean firstStart = true;

    private LoaderManager.LoaderCallbacks<List<SearchMessage>> messagesLoaderCallbacks = new LoaderManager.LoaderCallbacks<List<SearchMessage>>() {
        @Override
        public Loader<List<SearchMessage>> onCreateLoader(int id, Bundle args) {
            messageLoader = new SearchMessageLoader(getContext(), dataBaseHelper, searchRequestHelper.getType());
            return messageLoader;
        }

        @Override
        public void onLoadFinished(Loader<List<SearchMessage>> loader, List<SearchMessage> data) {
            if (getView() != null) {
                getView().showMessages(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<List<SearchMessage>> loader) {
            if (getView() != null) {
                getView().showMessages(Collections.emptyList());
            }
        }
    };

    public ITracker getTracker() {
        return tracker;
    }

    public String getMapBoxStyle() {
//        return getContext().getString(R.string.default_mapbox_style_url);
        return repository.getConfig().getSearch().getMapbox_style();
    }

    public ConfigurationResponse getConfig() {
//        return getContext().getString(R.string.default_mapbox_style_url);
        return repository.getConfig();
    }

    /**
     * View
     */

    public interface AroundMeView extends LocationView {
        AroundMeArguments getAroundMeArguments();

        void initLocationText(AroundMeArguments arguments);

        void updateMap(SearchResponseHuge response);

        void updateMarkers(Location location, Cursor cursor, boolean fromServer);

        LoaderManager getLoaderManager();

        void setLoadingProgress(boolean isLoading);

        void startActivity(Intent intent);

        void showFindCount(int count);

        void setCurrentItem(int position);

        void showMessages(List<SearchMessage> messages);

        void onChangeFilterButtonVisibility(boolean visible);

        void invalidateMap();
    }

    /**
     * Constructor
     */

    @Inject
    public AroundMePresenter(FragmentFactory fragmentFactory, FiltersHelper filtersHelper, IDataBaseHelper dataBaseHelper,
                             INetWorkState netWorkState, IRepository repository, ITracker tracker) {
        super(repository);
        this.fragmentFactory = fragmentFactory;
        this.filtersHelper = filtersHelper;
        this.dataBaseHelper = dataBaseHelper;
        this.netWorkState = netWorkState;
        searchRequestHelper = new SearchRequestHelper(filtersHelper);
        this.tracker = tracker;

        advertAdapter = new AdvertAdapter(repository);
        advertAdapter.setCardListener(this);

        idfAdapter = new IdfNotificationsAdapter();
        idfAdapter.setMessageListener(searchMessage -> markMessageAsRead(searchMessage));
    }

    /**
     * Lifecycle
     */

    @Override
    public void attachView(@NonNull AroundMeView view) {
        super.attachView(view);

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        googleApiClient.connect();
    }

    @Override
    public void detachView() {
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }


        // TODO: 18.11.16 Detach/Attach presenter must be moved to start/stop
        try {
            getContext().getContentResolver().unregisterContentObserver(forceLoadContentObserver);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        super.detachView();
    }

    @Override
    protected boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch) {
        AroundMeArguments aroundMeArguments = getAroundMeArguments();
        aroundMeArguments.setLocation(location);
        getView().updateMap(null);
        if (!isDisableSendSearch) {
            return search(SearchRequestType.DEFAULT);
        }
        return true;
    }

    public void searchInLocation(double latitude, double longitude) {
        AroundMeArguments aroundMeArguments = getAroundMeArguments();
//        https://prj.adyax.com/issues/218177#note-21
//        aroundMeArguments.setLocation(latitude, longitude);
        aroundMeArguments.getSearchCommand().setGeo(latitude, longitude);
        aroundMeArguments.getSearchCommand().setPage(0);
        search(SearchRequestType.DEFAULT);
    }

    public void searchInLocation(double lat1, double lon1, double lat2, double lon2) {
        final AroundMeArguments aroundMeArguments = getAroundMeArguments();
        final SearchCommand searchCommand = aroundMeArguments.getSearchCommand();
        if (searchCommand != null) {
            searchCommand.initBbox(lat1, lon1, lat2, lon2);
            searchCommand.setPage(0);
        } else {
            Timber.w("searchCommand is null");
        }
        search(SearchRequestType.DEFAULT);
    }

    /**
     * Main methods
     */

    public void clickFilters() {
        if (getView() != null) {
            if (!filtersHelper.isEmpty(SEARCH_TYPE)) {
//                fragmentFactory.startFiltersFragment(getContext(), SEARCH_TYPE);
                Intent intent = FiltersActivity.getIntentForStart(getContext(), SEARCH_TYPE);
                getView().startActivity(intent);
            } else {
                getFilters();
            }
        }
    }

    public void clickAdvert(IGetUrl item) {
//        getTracker().addGesture(new TrackerAT.TrackerPageArgBuilder(new WebViewArg(item, TrackerLevel.UNKNOWN)).build());
        new TrackerHelper(item).addTouchGesture(TrackerLevel.UNKNOWN);
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, TrackerLevel.UNKNOWN), -1);
    }

    public AdvertAdapter getAdvertAdapter() {
        return advertAdapter;
    }

    public IdfNotificationsAdapter getIdfAdapter() {
        return idfAdapter;
    }

    public void loadAroundMeItems() {
        if (getView() != null) {
            if (getView().getLoaderManager().getLoader(CHOICE_DATABASE_LOADER_ID) == null) {
                getView().getLoaderManager().initLoader(CHOICE_DATABASE_LOADER_ID, null, this);
                forceLoadContentObserver = getView().getLoaderManager().getLoader(CHOICE_DATABASE_LOADER_ID).new ForceLoadContentObserver();
                getContext().getContentResolver().registerContentObserver(TablesContentProvider.ADVERT_CONTENT_URI, true, forceLoadContentObserver);
            }

            if (getView().getLoaderManager().getLoader(LOADER_MESSAGE) == null) {
                Loader<List<SearchMessage>> listLoader = getView().getLoaderManager().initLoader(LOADER_MESSAGE, null, messagesLoaderCallbacks);
                forceLoadContentObserver = getView().getLoaderManager().getLoader(LOADER_MESSAGE).new ForceLoadContentObserver();
                getContext().getContentResolver().registerContentObserver(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, true, forceLoadContentObserver);
                listLoader.forceLoad();
            }
        }
    }

    private void updateLocation() {
        if (getView() == null
//                || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                || ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        )
            return;
        getView().updateMap(null);
    }

    public SearchItemHuge getItemInPosition(int position) {
        return advertAdapter.getItem(position);
    }

    public SearchItemHuge getPositionByGeo(double lon, double lat) {
        return advertAdapter.getPositionByGeo(lon, lat);
    }

    /**
     * Requests
     */

    private void getFilters() {
        ServiceCommand command = new GetFiltersCommand(SEARCH_TYPE);
        sendCommand(command, ExecutionService.GET_FILTERS_REQUEST_CODE);
    }

    public boolean search(SearchRequestType searchRequestType) {
        if (getView() != null) {
            fromServer = false;
            final ServiceCommand serviceCommand = searchRequestHelper.buildNewSearchCommand(searchRequestType);
            repository.saveLastSearchRequest(searchRequestHelper.getSearchCommand());
            if (firstStart) {
                new TrackerHelper().addScreenViewResearch(((SearchCommand) serviceCommand), TrackerHelper.CARTE_MODE);
                firstStart = false;
            }
//            repository.saveLastSearchRequest(serviceCommand);
            return sendCommand(serviceCommand, ExecutionService.SEARCH_ACTION);
        }
        return false;
    }

    @Override
    public boolean sendCommand(ServiceCommand serviceCommand, int requestCode) {
        if (requestCode == ExecutionService.SEARCH_ACTION) {
            synchronized (searchRequestHelper) {
                if (searchRequestHelper.isAlreadyUpdate()) {
                    return false;
                }
            }
        }
        return super.sendCommand(serviceCommand, requestCode);
    }

    public void searchNext() {
        setLoadingProgress(true);
        final SearchCommand searchCommand = getSearchCommand();
        if (searchCommand != null) {
            searchCommand.nextPage();
            repository.saveLastSearchRequest(searchCommand);
            sendCommand(searchCommand, ExecutionService.SEARCH_ACTION);
        }
    }

    public boolean hasFilters() {
        SearchCommand searchCommand = getSearchCommand();
        if (searchCommand != null) {
            ArrayList<SearchFilter> filters = searchCommand.getFilters();
            if (filters != null && !filters.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public void loadMore() {
        if (advertAdapter.isMoreLoadedEnabled()) {
            searchNext();
        }
    }

    private void setBookMark(SearchItemHuge item) {
        TrackerLevel trackerLevel = TrackerLevel.UNKNOWN;
        if (item.getXiti() != null) {
            trackerLevel = item.getXiti().getScreenTrackerLevel();
        }
        ServiceCommand command = new AddOrRemoveFavoriteCommand(trackerLevel, item.getId(),
                item.isFavorite() ? AddOrRemoveFavoriteCommand.ACTION_REMOVE : AddOrRemoveFavoriteCommand.ACTION_ADD, item);
        sendCommand(command, ExecutionService.FAVORITE_ACTION);
    }

    /**
     * AppReceiverCallback
     */

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        switch (requestCode) {
            case ExecutionService.SEARCH_ACTION:
                Timber.i("");
                break;
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.SGP_LOCATION_COMPLETE:
                final GetAutoCompleteSearchLocationsResponse configurationResponse = data.getParcelable(CommandExecutor.BUNDLE_SGP_LOCATION_COMPLETE);
                if (!configurationResponse.isActive()) {
                    if (!idfAdapter.isAroundMeRegionMessage()) {
                        DialogUtils.showBadRegionDialog(getContext(), getConfig(), null);
                    }
                }
                break;
            case ExecutionService.GET_FILTERS_REQUEST_CODE:
                if (!filtersHelper.isEmpty(SEARCH_TYPE)) {
                    fragmentFactory.startFiltersFragment(getContext(), SEARCH_TYPE);
                }
                break;

            case ExecutionService.SEARCH_ACTION:
                if (getView() != null) {
                    fromServer = true;
                    setLoadingProgress(false);
                    final SearchResponseHuge searchResponseHuge = data.getParcelable(SearchResponseHuge.BUNDLE_SEARCH);
                    searchRequestHelper.update(searchResponseHuge);
                    getView().updateMap(searchResponseHuge);
                    getView().onChangeFilterButtonVisibility(isFilterButtonShouldBeShown(searchResponseHuge.isEmpty()));
                    if (getSearchCommand().getType() == SearchType.around) {
                        if (searchResponseHuge.isEmpty()) {
//                            searchRequestHelper.restore();
                            getView().initLocationText(searchRequestHelper.getAroundMeArguments());
                            DialogUtils.showResultsNotFound(getContext());
                        } else {
                            searchRequestHelper.save();
                            loadAroundMeItems();
                            searchRequestHelper.setHasMoreItems(searchResponseHuge.has_more > 0);
                            searchRequestHelper.setSortedByDistance(searchResponseHuge.distance_sort > 0);
                            getAdvertAdapter().setMoreLoadedStatus(searchRequestHelper.hasMoreItems());
                            getView().showFindCount(searchResponseHuge.getTotal());
                        }
                    } else {
                        if (searchResponseHuge.isEmpty()) {
                            DialogUtils.showResultsNotFound(getContext());
                        }

                        loadAroundMeItems();
                        searchRequestHelper.setHasMoreItems(searchResponseHuge.has_more > 0);
                        searchRequestHelper.setSortedByDistance(searchResponseHuge.distance_sort > 0);
                        getAdvertAdapter().setMoreLoadedStatus(searchRequestHelper.hasMoreItems());
                        getView().showFindCount(searchResponseHuge.getTotal());
                    }

                    if (!searchResponseHuge.isEmpty()) {
                        if (statusLocation != null) {
                            testActiveRegion(statusLocation);
                        } else {
                            GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation = searchRequestHelper.getAroundMeArguments().getStatusLocation();
                            if (statusLocation != null) {
                                testActiveRegion(statusLocation);
                            }
                        }
                    }

                    if (getSearchCommand().getPage() == 0 && getView() != null) {
                        getView().setCurrentItem(0);
                    }
//                    getView().invalidateMap();
                }
                break;

            case ExecutionService.FAVORITE_ACTION:
//                if (getView() != null) {
//                    search(getView().getAroundMeArguments());
//                }

                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                boolean isFavorite = command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD);
                long id = command.getNids().size() > 0 ? command.getNids().get(0) : -1;
                if (id > -1)
                    updateFavoritesObservable(id, isFavorite).subscribe(o -> {
//                        loadAroundMeItems();
                    }, throwable -> {

                    });
                break;
        }
    }

    private void testActiveRegion(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        // if we use only geo location we mustn't test region
        if (!getAroundMeArguments().isOnlyLocation()) {
            if (!statusLocation.isActive()) {
                if (!idfAdapter.isAroundMeRegionMessage()) {
                    DialogUtils.showBadRegionDialog(getContext(), getConfig(), null);
                }
            } else if (statusLocation.isFake()) {
                sendCommand(new SgpLocationCompleteCommand(statusLocation.getLocation(), 1),
                        ExecutionService.SGP_LOCATION_COMPLETE);
            }
        }
    }

    public boolean isFilterButtonShouldBeShown(boolean isEmpty) {
        if (filtersHelper.getFilters(getSearchCommand().getType()).isEmpty()) {
            return false;
        }

        if (isEmpty && !hasFilters()) {
            return false;
        }

        return true;
    }

    public Observable updateFavoritesObservable(long id, boolean isFavorite) {
        Observable observable = Observable.create(subscriber -> {
            try {
                dataBaseHelper.updateFavoriteAdvertCard(id, isFavorite ? 1 : 0);
                subscriber.onNext(null);
            } catch (Exception e) {
                subscriber.onError(e);
            }
        });
        return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private void setLoadingProgress(boolean isLoading) {
        advertAdapter.setLoadingProgress(isLoading);
        getView().setLoadingProgress(isLoading);
    }

    /**
     * Loader callback
     */

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (getView() != null)
            return new AdvertCursorLoader(getContext(), dataBaseHelper, searchRequestHelper.getType());
        else
            return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        advertAdapter.swapCursor(cursor);
        if (getView() != null) {
            getView().onChangeFilterButtonVisibility(isFilterButtonShouldBeShown(cursor.getCount() == 0));
            getView().updateMarkers(getAroundMeArguments().getMyLocation(), cursor, fromServer);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        advertAdapter.swapCursor(null);
    }

    /**
     * GoogleApiClient
     */

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        updateLocation();// around me 1
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // TODO for debuging
        updateLocation();
    }

    /**
     * Card Listener
     */

    @Override
    public void onItemClicked(SearchItemHuge item) {
        if (!item.isContentMaintenanceMode()) {
            clickAdvert(item);
        }
    }


    @Override
    public void onFavoriteClicked(SearchItemHuge item) {
        if (getView() != null) {
            if (netWorkState.isLoggedIn()) {
//                Toast.makeText(getContext(), String.format("Favorite: %d", item.getId()), Toast.LENGTH_SHORT).show();
                setBookMark(item);
            } else {
                getView().startActivity(CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART));
            }
        }
    }

    @Override
    public void onCallClicked(SearchItemHuge item) {
        if (getView() != null) {
//            Utils.requestCall(item.phones[0], getContext());
            new TrackerHelper(item).requestCall(getContext());
        }
    }

    public void markMessageAsRead(SearchMessage searchMessage) {
        dataBaseHelper.markSearchMessageAdRead(searchMessage.getKey());
    }

    @Override
    public void onRouteClicked(SearchItemHuge item) {
        if (getView() != null) {
            new TrackerHelper(item).clickRoute();
            String googleMapsPackage = "com.google.android.apps.maps";
            boolean isGoogleMapsInstalled;
            try {
                getContext().getPackageManager().getPackageInfo(googleMapsPackage, PackageManager.GET_ACTIVITIES);
                isGoogleMapsInstalled = true;
            } catch (PackageManager.NameNotFoundException e) {
                isGoogleMapsInstalled = false;
            }

            if (isGoogleMapsInstalled) {
                String url = String.format(Locale.US, "http://maps.google.com/maps?f=d&daddr=%f,%f&dirflg=d&layer=t", item.lat, item.lon);
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
                intent.setClassName(googleMapsPackage, "com.google.android.maps.MapsActivity");
                getView().startActivity(intent);
            } else {
                String url = String.format(Locale.US, "http://maps.google.com/maps?f=d&daddr=%f,%f&dirflg=d&layer=t", item.lat, item.lon);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                getView().startActivity(intent);
            }
        }
    }

    public AroundMeArguments getAroundMeArguments() {
        return searchRequestHelper.getAroundMeArguments();
    }

    public void init(AroundMeArguments aroundMeArguments) {
        searchRequestHelper.update(aroundMeArguments);
//        advertAdapter.setGeolocated(!aroundMeArguments.isCity());
        // TODO it need to change
        if (searchRequestHelper.getType() == SearchType.find) {
            getAdvertAdapter().setMoreLoadedStatus(searchRequestHelper.hasMoreItems());
            fromServer = true;
        }
    }

    public SearchCommand getSearchCommand() {
        return searchRequestHelper.getSearchCommand();
    }

    @Override
    public void updateStatus(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        final AroundMeArguments aroundMeArguments = getAroundMeArguments();
        if (aroundMeArguments != null) {
            aroundMeArguments.setStatusLocation(statusLocation);
        }
        super.updateStatus(statusLocation);
    }

    public void callFiltersActivity() {
        final Intent intent = FiltersActivity.getIntentForStart(getContext(), getAroundMeArguments().getType());
        ((Activity) getContext()).startActivityForResult(intent, AroundMeFragment.FILTERS_ACTIVITY_REQUEST_CODE);
    }

}
