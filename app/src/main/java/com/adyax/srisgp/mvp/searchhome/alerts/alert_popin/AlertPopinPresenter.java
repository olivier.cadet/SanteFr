package com.adyax.srisgp.mvp.searchhome.alerts.alert_popin;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertPopinPresenter extends Presenter<AlertPopinPresenter.AlertView> {

    private IDataBaseHelper iDataBaseHelper;

    @Inject
    public AlertPopinPresenter(IDataBaseHelper iDataBaseHelper){
        this.iDataBaseHelper = iDataBaseHelper;
    }

    public void clickEnSavoirPlus(AlertItem item) {
//        new TrackerHelper(item).clickEnSavoirPlus();
        new TrackerHelper(item).clickAlert();
        getView().close(true);
        iDataBaseHelper.closeUrgentAlert(item);
    }

    public interface AlertView extends IView {
        void close(boolean bRedBotton);
    }

}
