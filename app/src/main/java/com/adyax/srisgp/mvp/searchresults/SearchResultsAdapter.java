package com.adyax.srisgp.mvp.searchresults;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.QuestionnaireItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.mvp.common.MessageHolder;
import com.adyax.srisgp.mvp.framework.BaseCursorRecyclerAdapter;
import com.adyax.srisgp.mvp.searchresults.info.GetInformedResultsAdapter;
import com.adyax.srisgp.mvp.web_views.DefaultWebViewClient;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;
import com.adyax.srisgp.mvp.widget.WidgetViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/11/16.
 */

public abstract class SearchResultsAdapter extends BaseCursorRecyclerAdapter<RecyclerView.ViewHolder> {

    protected static final int ITEM_HEADER = 0;
    protected static final int ITEM_CARD = 1;
    protected static final int ITEM_LOAD_MORE = 2;
    protected static final int ITEM_HELPFUL = 3;
    protected static final int ITEM_RELATED = 4;
    protected static final int ITEM_DOCTOR = 5;
    protected static final int ITEM_MESSAGE = 6;
    protected static final int ITEM_WIDGET = 7;
    protected static final int ITEM_VIDEO = 8;

    private RecyclerView.ViewHolder viewHolder;

    //    private List<SearchItemHuge> items = new ArrayList<>();
    private List<Related> relatedList = new ArrayList<>();
    private ResultsAdapterListener listener;
    private List<SearchMessage> messages = Collections.emptyList();

    private boolean lastPage = false;
    private boolean showDoctorAlert = true;
    private boolean sortedByDistance = true;
    private boolean showWidget = true;
    public IRepository repository;

    public SearchResultsAdapter(ResultsAdapterListener listener, IRepository repository) {
        this.listener = listener;
        this.repository = repository;
    }

    public void onReminderClosed() {
        removeDoctorAlert();
    }

    public void addItems(Cursor cursor, boolean sortedByDistance, boolean hasMore) {
//        if (clearList) {
//            this.items.clear();
//        }
        this.sortedByDistance = sortedByDistance;
        lastPage = !hasMore;
        swapCursor(cursor);

//        this.items.addAll(items);
    }

    public void showMessages(List<SearchMessage> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void showRelated(List<Related> relatedList) {
        this.relatedList.clear();
        this.relatedList.addAll(relatedList);
        notifyDataSetChanged();
    }

//    public void onNoMoreItems() {
//        if (!lastPage) {
//            lastPage = true;
//            notifyItemRemoved(getSize() + 2);
//        }
//    }

    public void removeDoctorAlert() {
        if (showDoctorAlert) {
            showDoctorAlert = false;
            notifyItemRemoved(3 + messages.size());
        }
    }

    public void removeMessage(SearchMessage searchMessage) {
        SearchMessage message;
        for (int i = 0; i < messages.size(); i++) {
            message = messages.get(i);
            if (message.getKey().equals(searchMessage.getKey())) {
                messages.remove(i);
                notifyItemRemoved(i + 1);
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        final SearchItemHuge item = getItemForPosition(position);

        if (position == 0 && sortedByDistance) {
            return ITEM_HEADER;
        }

        if (position - (sortedByDistance ? 1 : 0) < messages.size()) {
            return ITEM_MESSAGE;
        }

        position = position - messages.size();

        if (!sortedByDistance) {
            position++;
        }

        if (showDoctorAlert && getSize() >= 2 && position == 3) {
            return ITEM_DOCTOR;
        }

        int correction = 0;
        if (showDoctorAlert && getSize() >= 2) {
            correction++;
        }

        if (!lastPage) {
            correction++;
        }

        if (!lastPage && position == getSize() + correction) {
            return ITEM_LOAD_MORE;
        }

        WidgetViewHelper widgetHelper = new WidgetViewHelper(getActivityLocationID());
        boolean showWidget = widgetHelper.getVisibility();
        if (showWidget) {

            if (position == getSize() + 1 + correction) {
                return ITEM_WIDGET;
            } else if (position == getSize() + 2 + correction) {
                return ITEM_HELPFUL;
            } else if (position >= getSize() + 3 + correction) {
                return ITEM_RELATED;
            }

        } else {
            if (position == getSize() + 1 + correction) {
                return ITEM_HELPFUL;
            } else if (position >= getSize() + 2 + correction) {
                return ITEM_RELATED;
            }
        }

        // Indique si une card classique ou une vidéo
        if (NodeType.VIDEO.equals(item.getNodeType())) {
            return ITEM_VIDEO;
        }
        return ITEM_CARD;
    }

    private int getSize() {
//        return items.size();
        return cursor != null ? cursor.getCount() : 0;
    }

    @Override
    public int getItemCount() {
        int correction = 0;
        if (!lastPage) {
            correction++;
        }

        if (!sortedByDistance) {
            correction--;
        }

        WidgetViewHelper widgetHelper = new WidgetViewHelper(getActivityLocationID());
        if (widgetHelper.getVisibility()) {
            correction++;
        }

        if (getSize() >= 3 && showDoctorAlert) {
            correction++;
        }
        return getSize() + 2 + correction + relatedList.size() + messages.size();
    }

    public abstract int getActivityLocationID();

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM_HEADER:
                View headerView = inflater.inflate(R.layout.item_get_informed_header, parent, false);
                viewHolder = new HeaderHolder(headerView, this);
                break;
            case ITEM_CARD:
            case ITEM_VIDEO:
                viewHolder = createCardViewHolder(inflater, parent, viewType);
                break;
            case ITEM_LOAD_MORE:
                View moreView = inflater.inflate(R.layout.item_get_informed_more, parent, false);
                viewHolder = new MoreHolder(moreView);
                break;
            case ITEM_HELPFUL:
                View helpfulView = inflater.inflate(R.layout.item_additional_action_holder, parent, false);
                viewHolder = new AdditionalActionHolder(helpfulView);
                break;
            case ITEM_RELATED:
                View relatedView = inflater.inflate(R.layout.item_get_informed_related, parent, false);
                viewHolder = new RelatedHolder(relatedView);
                break;
            case ITEM_DOCTOR:
                View doctorView = inflater.inflate(R.layout.item_talked_doctor, parent, false);
                viewHolder = new DoctorHolder(doctorView);
                break;
            case ITEM_MESSAGE:
                View messageView = inflater.inflate(R.layout.item_search_message, parent, false);
                viewHolder = new MessageHolder(messageView);
                break;
            case ITEM_WIDGET:
                View headerWidget = inflater.inflate(R.layout.item_search_home_widget, parent, false);
                viewHolder = new WidgetViewHolder(headerWidget);
                break;
            default:
                throw new RuntimeException("No such view type!");
        }
        return viewHolder;
    }

    protected abstract RecyclerView.ViewHolder createCardViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType);

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor, int position) {

    }

    private SearchItemHuge getItemForPosition(int position) {
        if (sortedByDistance) {
            position--;
        }

        position = position - messages.size();
        if (position >= 3 && showDoctorAlert) {
            position--;
        }
        SearchItemHuge searchItemHuge = new SearchItemHuge(position);
        // TODO it operation is extra
        if (position >= 0 && cursor != null) {
            cursor.moveToPosition(position);
            BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, searchItemHuge);
        }
        return searchItemHuge;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            switch (holder.getItemViewType()) {
                case ITEM_HEADER:
                    ((HeaderHolder) holder).bind();
                    break;
                case ITEM_CARD:
                case ITEM_VIDEO:
                    onBindCardViewHolder(holder, getItemForPosition(position));
                    break;
                case ITEM_LOAD_MORE:
                    ((MoreHolder) holder).bind();
                    break;
                case ITEM_HELPFUL:
                    ((AdditionalActionHolder) holder).bind();
                    break;
                case ITEM_RELATED:
                    ((RelatedHolder) holder).bind(getRelatedItem(position));
                    break;
                case ITEM_DOCTOR:
                    break;
                case ITEM_MESSAGE:
                    ((MessageHolder) holder).bind(getSearchMessage(position), listener);
                case ITEM_WIDGET:
                    // @TODO : Focus to the correct element.
                    if (holder instanceof WidgetViewHolder) {
                        ((WidgetViewHolder) holder).bind();
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
        }
    }

    protected abstract void onBindCardViewHolder(RecyclerView.ViewHolder holder, SearchItemHuge item);

//    private SearchItemHuge getItem(int position) {
//        if (position > 3 && showDoctorAlert) {
//            position--;
//        }
//        return items.get(position - 1);
//    }

    private SearchMessage getSearchMessage(int position) {
        position = sortedByDistance ? position - 1 : position;
        return messages.get(position);
    }

    private Related getRelatedItem(int position) {
        position = position - messages.size();

        if (position > 3 && showDoctorAlert) {
            position--;
        }

        if (!lastPage) {
            position--;
        }

        if (!sortedByDistance) {
            position++;
        }

        WidgetViewHelper widgetHelper = new WidgetViewHelper(getActivityLocationID());
        if (widgetHelper.getVisibility()) {
            return relatedList.get(position - getSize() - 3);
        }
        return relatedList.get(position - getSize() - 2);

    }

    class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llSearchInfoSortHeader)
        LinearLayout llSearchSort;
        @BindView(R.id.btnSortRelevant)
        Button btnSortRelevant;
        @BindView(R.id.btnSortDate)
        Button btnSortDate;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        List<Button> sortToggler = new ArrayList<Button>();

        HeaderHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            sortToggler.add(btnSortDate);
            sortToggler.add(btnSortRelevant);
            tvTitle.setText(getHeaderTitle());
        }

        HeaderHolder(View view, SearchResultsAdapter contextLayout) {
            super(view);
            ButterKnife.bind(this, view);

            sortToggler.add(btnSortDate);
            sortToggler.add(btnSortRelevant);
            tvTitle.setText(getHeaderTitle());

            if (contextLayout instanceof GetInformedResultsAdapter) {
                llSearchSort.setVisibility(View.VISIBLE);
            }
        }

        private void toggleActiveSort(Button newActiveElement) {
            // Appliquer le style désactivé sur tous les buttons
            for (Button el : sortToggler) {
                //el.setTypeface(null, Typeface.NORMAL);
                //newActiveElement.setBackgroundColor(Color.rgb(20,59,107));
                el.setTextColor(Color.WHITE);
                el.setBackgroundResource(R.drawable.border_button_inactive);

                // Activer le bouton sélectionné
                if (el == newActiveElement) {
                    //newActiveElement.setTypeface(null, Typeface.BOLD);
                    //newActiveElement.setBackgroundColor(Color.WHITE);
                    newActiveElement.setTextColor(Color.rgb(20, 59, 107));
                    newActiveElement.setBackgroundResource(R.drawable.border_button_active);
                }
            }
        }

        private void bind() {
            btnSortRelevant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleActiveSort(btnSortRelevant);
                    listener.updateSort(UrlExtras.RestUserSearch.SORT_RELEVANCE);
                }
            });

            btnSortDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleActiveSort(btnSortDate);
                    listener.updateSort(UrlExtras.RestUserSearch.SORT_DATE);
                }
            });

        }
    }

    protected abstract String getHeaderTitle();

    private String bmcValue = null;
    private QuestionnaireItem questionnaireItem = null;

    public void setBmcValue(String value) {
        this.bmcValue = value;
    }

    public void setQuestionnaireView(QuestionnaireItem value) {
        this.questionnaireItem = value;
    }

    class MoreHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnMore)
        Button btnMore;

        MoreHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind() {
            // Si la dernière page est atteinte alors cacher le bouton
            if (lastPage) btnMore.setVisibility(View.GONE);
            btnMore.setOnClickListener(v -> listener.onNextPage());
        }
    }

    class AdditionalActionHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnYes)
        Button btnYes;
        @BindView(R.id.btnNo)
        Button btnNo;
        @BindView(R.id.webview_bmc)
        WebView bmcWv;
        @BindView(R.id.questionnaire_holder)
        LinearLayout questionnaireHolder;

        AdditionalActionHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            btnYes.setOnClickListener(view1 -> {
                listener.onClickYes();
                view.findViewById(R.id.feedback_form).setVisibility(View.GONE);
            });
            btnNo.setOnClickListener(view12 -> {
                listener.onClickNo();
                view.findViewById(R.id.feedback_form).setVisibility(View.GONE);
            });
        }

        private void bind() {
            // MAJ BMC
            bmcWv.setVisibility(View.GONE);
            if (bmcValue != null && bmcWv != null) {
                bmcWv.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (0 != (bmcWv.getContext().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
                        WebView.setWebContentsDebuggingEnabled(true);
                    }
                }

                DefaultWebViewClient webViewClient = new DefaultWebViewClient(bmcWv.getContext());
                webViewClient.setHeadersForSantefr(repository);

                bmcWv.setWebViewClient(webViewClient);
                bmcWv = webViewClient.setDefaultWebviewSettingsForBMC(bmcWv);

                bmcWv.loadData(bmcValue, null, null);
            } else if (bmcWv != null) {
                bmcWv.setVisibility(View.GONE);
            }

            // MAJ questionnaire block
            if (questionnaireItem != null) {
                TextView txt_questionnaire = (TextView) questionnaireHolder.findViewById(R.id.txt_questionnaire);
                Button btn_questionnaire = (Button) questionnaireHolder.findViewById(R.id.btn_questionnaire);
                btn_questionnaire.setText(questionnaireItem.labelBlock);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    txt_questionnaire.setText(Html.fromHtml(questionnaireItem.txtBlock, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    txt_questionnaire.setText(Html.fromHtml(questionnaireItem.txtBlock));
                }
                questionnaireHolder.setBackgroundColor(Color.parseColor("#" + questionnaireItem.bgBlock));
                questionnaireHolder.setVisibility(View.VISIBLE);

                btn_questionnaire.setOnClickListener(v -> {
                    Intent intent;
                    intent = WebViewActivity.newIntent(v.getContext(), new WebViewArg(questionnaireItem.link));
                    v.getContext().startActivity(intent);
                });
            }
        }
    }

    class RelatedHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.llContent)
        LinearLayout llContent;

        RelatedHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(Related related) {
            tvTitle.setText(related.label);

            llContent.removeAllViews();

            Context context = llContent.getContext();
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, (int) context.getResources().getDimension(R.dimen.default_margin_x0_5), 0, 0);
            for (RelatedItem item : related.items) {
                TextView textView = new TextView(context);
                textView.setOnClickListener(v -> {
                    listener.onRelatedItemClicked(item);
                });
                textView.setTextColor(context.getResources().getColor(R.color.material_black));
                textView.setLayoutParams(layoutParams);
                textView.setText(item.title);
                llContent.addView(textView);
            }
        }
    }

    class DoctorHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;
        @BindView(R.id.tvClose)
        TextView tvClose;

        DoctorHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onReminderClose();
                }
            });
        }
    }

    public interface ResultsAdapterListener extends MessageHolder.MessageListener {
        void onNextPage();

        void onReminderClose();

        void onRelatedItemClicked(RelatedItem item);

        void onClickYes();

        void onClickNo();

        void updateSort(String sortValue);

        void onCloseMessage(SearchMessage message);
    }


}
