package com.adyax.srisgp.mvp.favorite;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.GetFavoritesCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FavoritesResponse;
import com.adyax.srisgp.data.net.response.tags.FavoriteUsers;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public class FavoritesPresenter extends Presenter<FavoritesPresenter.FavoritesView>
        implements AppReceiver.AppReceiverCallback {

//    private static final int COMMAND_FAVORITES = 0;

    private AppReceiver appReceiver = new AppReceiver();

    private boolean hasMore = true;
    private FavoriteType favoriteType;
    private FragmentFactory fragmentFactory;

    @Inject
    public FavoritesPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(@NonNull FavoritesPresenter.FavoritesView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    public void onStart() {
        resetFavorites();
    }

    public void onItemClicked(FavoritesItem item) {
        openFavoriteScreen(item);
    }

    public void setType(FavoriteType favoriteType) {
        this.favoriteType = favoriteType;
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void resetFavorites() {
        hasMore = true;
        if (getView() != null)
            getView().resetFavorites();
        requestFavorites(0);
    }

    public void requestFavorites(int offset) {
        if (hasMore || offset == 0) {
            GetFavoritesCommand command = new GetFavoritesCommand(offset, favoriteType);
            ExecutionService.sendCommand(getContext(), appReceiver, command, ExecutionService.COMMAND_FAVORITES);
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.COMMAND_FAVORITES) {
            FavoritesResponse response = data.getParcelable(CommandExecutor.BUNDLE_FAVORITES);
            FavoriteUsers favorites = null;
            if (favoriteType == FavoriteType.all) {
                favorites = response.all;
            } else if (favoriteType == FavoriteType.get_informed) {
                favorites = response.informed;
            } else if (favoriteType == FavoriteType.find) {
                favorites = response.find;
            }

            if (favorites != null) {
                hasMore = favorites.hasMore();
            } else {
                hasMore = false;
            }

            if (getView() != null) {
                getView().showFavorites(favorites);
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    private void openFavoriteScreen(FavoritesItem item) {
        new TrackerHelper(item).clickForWriteToHistory();
        switch (item.getNodeType()) {
            case APPLICATIONS:
            case NUMERO_TEL:
            case LIENS_EXTERNES:
                if (getView() != null) {
                    Intent intent = FavoriteActivity.newIntent(getView().getContext(), item);
                    getView().startActivity(intent);
                }
                break;
            default:
//                fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.FAVORITES), -1);
                new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.FAVORITES);
        }
    }

    public interface FavoritesView extends IView {
        void showFavorites(FavoriteUsers notifications);

        void resetFavorites();

        void showEmptyView();

        void startActivity(Intent intent);

        void showMessage(String show_card);
    }
}
