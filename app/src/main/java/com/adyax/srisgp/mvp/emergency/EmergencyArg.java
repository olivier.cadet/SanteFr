package com.adyax.srisgp.mvp.emergency;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.tags.EmergencyServiceItem;

import timber.log.Timber;

public class EmergencyArg implements Parcelable {

    public static final String BUNDLE = "EmergencyArg";
    private String link;
    private final AnchorResponse anchorResponse;

    public EmergencyArg(String link, AnchorResponse anchorResponse) {
        this.link = link;
        this.anchorResponse = anchorResponse;
    }

    protected EmergencyArg(Parcel in) {
        link = in.readString();
        anchorResponse = in.readParcelable(AnchorResponse.class.getClassLoader());
    }

    public static final Creator<EmergencyArg> CREATOR = new Creator<EmergencyArg>() {
        @Override
        public EmergencyArg createFromParcel(Parcel in) {
            return new EmergencyArg(in);
        }

        @Override
        public EmergencyArg[] newArray(int size) {
            return new EmergencyArg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(link);
        parcel.writeParcelable(anchorResponse, i);
    }

    public String[] getValues() {
        return anchorResponse.emergency != null ? anchorResponse.emergency.getValues() : new String[0];
    }

    public String[] getServices() {
        return anchorResponse.emergency != null ? anchorResponse.emergency.getServices() : new String[0];
    }

    public long getNid() {
        return anchorResponse.getNid();
    }

    public long getSid() {
        try {
            final String[] split = link.split("sid=");
            if (split.length == 2) {
                return Long.parseLong(split[1]);
            }
        } catch (Exception e) {
            Timber.e(e);
        }
        for (EmergencyServiceItem item : anchorResponse.emergency.services) {
            return item.sid != null ? item.sid : -1;
        }
        return -1;
    }

    public Long getValueIndex(String value) {
        return anchorResponse.getValueIndex(value);
    }

    public Long getServiceIndex(String value) {
        return anchorResponse.getServiceIndex(value);
    }

    public int getServiceSpinnerIndex() {
        return anchorResponse.getServiceSpinnerIndex(getSid());
    }

    public int getValueSpinnerIndex(int vote) {
        return anchorResponse.getValueSpinnerIndex(vote);
    }

    public AnchorResponse getAnchorResponse() {
        return anchorResponse;
    }
}
