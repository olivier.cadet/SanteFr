package com.adyax.srisgp.mvp.onboarding;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.presentation.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public class AccountOnboardingFragment extends BaseFragment {

    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.btnContinueWithoutAccount)
    Button btnContinueWithoutAccount;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    @BindView(R.id.skipBtn)
    TextView skipBtn;

    private OnboardingFragmentListener listener;

    public static AccountOnboardingFragment newInstance() {
        AccountOnboardingFragment fragment = new AccountOnboardingFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnboardingFragmentListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_onboarding_account, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private void bindViews() {
        btnContinueWithoutAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNext();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCreateAccountClicked();
            }
        });

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSkip();
            }
        });
    }
}
