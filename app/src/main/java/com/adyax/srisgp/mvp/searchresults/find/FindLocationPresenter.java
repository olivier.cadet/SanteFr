package com.adyax.srisgp.mvp.searchresults.find;

import android.location.Location;

import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.mvp.LocationPresenter;
import com.adyax.srisgp.mvp.LocationView;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/28/16.
 */

public class FindLocationPresenter extends LocationPresenter<FindLocationPresenter.FindLocationView> {

    private ITracker tracker;

    @Inject
    public FindLocationPresenter(IRepository repository, ITracker tracker) {
        super(repository);
        this.tracker = tracker;
    }

    @Override
    protected boolean setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, boolean isDisableSendSearch) {
        getView().setCurrentLocation(location);
        return false;
    }

    public ITracker getTracker() {
        return tracker;
    }

    public IRepository getRepository() {
        return repository;
    }

    public interface FindLocationView extends LocationView {
        void setCurrentLocation(Location location);
        void showMessage(String body);
    }
}
