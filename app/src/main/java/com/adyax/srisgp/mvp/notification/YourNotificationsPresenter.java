package com.adyax.srisgp.mvp.notification;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.adyax.srisgp.data.net.command.UpdateNotificationStatusCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.mvp.my_account.PersonalizeActivity;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 9/28/16.
 */

public class YourNotificationsPresenter extends Presenter<YourNotificationsPresenter.YourNotificationsView>
        implements AppReceiver.AppReceiverCallback {
    private static final int COMMAND_REMOVE = 0;

    private AppReceiver appReceiver = new AppReceiver();
    private FragmentFactory fragmentFactory;

    @Inject
    public YourNotificationsPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(@NonNull YourNotificationsView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void onNotificationRemove(List<Long> itemsToRemove) {
        UpdateNotificationStatusCommand command =
                new UpdateNotificationStatusCommand(UpdateNotificationStatusCommand.ACTION_DELETE, itemsToRemove);
        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_REMOVE);
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == COMMAND_REMOVE) {

        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_REMOVE) {
            UpdateNotificationStatusCommand command = data.getParcelable(CommandExecutor.BUNDLE_UPDATE_NOTIFICATION);
            if (getView() != null) {
                for (int i = 0; i < command.getNids().size(); i++) {
                    getView().onRemoved(command.getNids().get(i));
                }

                getView().onCloseEditMode();
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public void clickCustomize() {
        if (getView() != null)
            PersonalizeActivity.start(getView().getActivity());
    }

    public interface YourNotificationsView extends IView {
        void onRemoved(long removedId);

        void onCloseEditMode();

        FragmentActivity getActivity();
    }
}
