package com.adyax.srisgp.mvp.framework;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adyax.srisgp.presentation.BaseFragment;

import java.util.List;

/**
 * Created by anton.kobylianskiy on 10/27/16.
 */

public abstract class MultiplePresenterViewFragment extends BaseFragment implements IView {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        for (IPresenter presenter : getPresenters()) {
            presenter.attachView(this);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (IPresenter presenter : getPresenters()) {
            presenter.restoreState(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        for (IPresenter presenter : getPresenters()) {
            presenter.registerListeners();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        for (IPresenter presenter : getPresenters()) {
            presenter.unregisterListeners();
        }
    }

    @Override
    public void onDetach() {
        for (IPresenter presenter : getPresenters()) {
            presenter.detachView();
        }
        super.onDetach();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        for (IPresenter presenter : getPresenters()) {
            presenter.saveState(outState);
        }
    }

    @NonNull
    abstract public List<IPresenter> getPresenters();

    // don't delete! It for sample
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public boolean onBackPressed() {
        return false;
    }
}
