package com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.CursorLoader;

import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;

/**
 * Created by SUVOROV on 3/25/16.
 */
public class AlertsSwipeCursorLoader extends CursorLoader {

    private IAlerts alerts;

    public AlertsSwipeCursorLoader(Context context, IAlerts alerts) {
        super(context);
        this.alerts = alerts;
    }

    @Override
    public Cursor loadInBackground() {
        return alerts.getAlerts();
    }
}
