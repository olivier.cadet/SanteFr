package com.adyax.srisgp.mvp.history;

import android.os.Bundle;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 2/20/17.
 */

public class HistoryPresenter extends PresenterAppReceiver<HistoryPresenter.HistoryView> {

    public interface HistoryView extends IView {
        void updateFavoriteStatus(boolean favorite);
        void openRegistrationScreen();
    }

    private FragmentFactory fragmentFactory;
    private INetWorkState netWorkState;

    @Inject
    public HistoryPresenter(FragmentFactory fragmentFactory, INetWorkState netWorkState) {
        this.fragmentFactory = fragmentFactory;
        this.netWorkState = netWorkState;
    }

    public void downloadClicked(HistoryItem item) {
        if (getView() != null) {
            new TrackerHelper(item).showAppsDialog(getContext(), null);
        }
    }

    public void onFavoriteClicked(HistoryItem item, boolean favorite) {
        if (getView() != null) {
            if (netWorkState.isLoggedIn()) {
                ExecutionService.sendCommand(getContext(), getAppReceiver(),
                        new AddOrRemoveFavoriteCommand(TrackerLevel.HISTORICAL, item.nid, !favorite ? AddOrRemoveFavoriteCommand.ACTION_ADD :
                                AddOrRemoveFavoriteCommand.ACTION_REMOVE, item), ExecutionService.FAVORITE_ACTION);
            } else {
                getView().openRegistrationScreen();
            }
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.FAVORITE_ACTION) {
            if (getView() != null) {
                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                if (command != null && command.getNids() != null) {
                    if (getView() != null) {
                        getView().updateFavoriteStatus(command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD));
                    }
                }
            }
        }
    }

    public void readClicked(HistoryItem item) {
//        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.HISTORICAL), -1);
        new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.HISTORICAL);
    }
}
