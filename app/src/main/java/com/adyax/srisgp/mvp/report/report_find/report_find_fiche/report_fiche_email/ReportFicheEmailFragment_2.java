package com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_email;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.databinding.FragmentFindFicheEmail2Binding;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.Utils;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportFicheEmailFragment_2 extends ViewFragment implements ReportFicheEmailPresenter_2.ReportPresenter_2View {

//    public static final String TITLE = "title";

    @Inject
    ReportFicheEmailPresenter_2 reportPresenter1;

    private FragmentFindFicheEmail2Binding binding;
//    private int title;

    public ReportFicheEmailFragment_2() {
    }

    public static Fragment newInstance(int title, ReportBuilder builder) {
        ReportFicheEmailFragment_2 fragment = new ReportFicheEmailFragment_2();
        Bundle args = new Bundle();
//        args.putInt(TITLE, title);
        args.putParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER, builder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        title =getArguments().getInt(TITLE);
        reportPresenter1.setBuilder(getArguments().getParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_find_fiche_email_2, container, false);
        binding.btnReport4Action.setOnClickListener(view -> {

            final String email = binding.reportFicheEmail.getText().toString().trim();
            if (Utils.isEmailValid(email) && (binding.etIdRpps.getText().toString().length() == 0 || binding.etIdRpps.getText().toString().length() == 9 || binding.etIdRpps.getText().toString().length() == 10 || binding.etIdRpps.getText().toString().length() == 11)) {
                reportPresenter1.getBuilder()
                        .set(ReportBuilder.FIELD_IDENTIFIANT_RPPS, binding.etIdRpps.getText().toString())
                        .set(ReportBuilder.FIELD_PRENOM, binding.reportFichePrenom.getText().toString())
                        .set(ReportBuilder.FIELD_NOM, binding.reportFicheMom.getText().toString())
                        .set(ReportBuilder.FIELD_EMAIL, email)
                        .set(ReportBuilder.FIELD_TELEPHONE_FIXE, binding.reportFichePhone.getText().toString())
                        .set(ReportBuilder.FIELD_PROFESSION_OF_REPORTER, binding.reportFicheProfession.getText().toString());
                reportPresenter1.clickBackToCard();
            }
            else if (binding.etIdRpps.getText().toString().length() != 0 && binding.etIdRpps.getText().toString().length() != 9 && binding.etIdRpps.getText().toString().length() != 10 && binding.etIdRpps.getText().toString().length() != 11) {
                binding.etIdRpps.setError(getString(R.string.contact_id_rpps_error));
                binding.etIdRpps.requestFocus();
            }
            else if(!Utils.isEmailValid(email)){
                binding.reportFicheEmail.setError(getString(R.string.invalid_email_address));
                binding.reportFicheEmail.requestFocus();

            }

        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, true);
        reportPresenter1.onViewCreated();
    }

    //    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);
//    }


    @Override
    public void showEmail(String email) {
        binding.reportFicheEmail.setText(email);
    }

    @NonNull
    @Override
    public ReportFicheEmailPresenter_2 getPresenter() {
        return reportPresenter1;
    }

//    @Override
//    public void showCloseDialog() {
//        new AlertDialog.Builder(getContext())
//            .setMessage(R.string.close_report)
//            .setPositiveButton(R.string.text214, (dialog, which) -> {
//                dialog.dismiss();
//                reportPresenter1.clickBackToCard();
//                getActivity().finish();
//            })
//            .show();
//    }

    @Override
    public void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
        binding.btnReport4Action.setClickable(false);
    }

    @Override
    public void hideProgress() {
        binding.progressLayout.setVisibility(View.GONE);
        binding.btnReport4Action.setClickable(true);
    }
}
