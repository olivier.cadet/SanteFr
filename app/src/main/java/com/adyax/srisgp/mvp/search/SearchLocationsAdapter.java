package com.adyax.srisgp.mvp.search;

import androidx.annotation.IntDef;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/26/16.
 */
public class SearchLocationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int STATE_LOCATION_DEFAULT = 2;
    public static final int STATE_LOCATION_SEARCH = 3;

    private static final int ITEM_HEADER = 0;
    private static final int ITEM_PROVIDE_LOCATION = 1;
    private static final int ITEM_LOCATION = 2;
    private static final int ITEM_POPULAR = 3;
    private static final int ITEM_REMOVE_HISTORY = 4;

    private OnItemClickedLister onItemClickedLister;

    private List<RecentLocationViewModel> recentLocations = new ArrayList<>();
//    private GetAutoCompleteSearchLocationsResponse autocompleteItems = new ArrayList<>();
    private GetAutoCompleteSearchLocationsResponse autocompleteItems = new GetAutoCompleteSearchLocationsResponse();

    private @SearchCurrentState
    int currentState = STATE_LOCATION_DEFAULT;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({STATE_LOCATION_DEFAULT, STATE_LOCATION_SEARCH})
    public @interface SearchCurrentState {}

    public SearchLocationsAdapter(OnItemClickedLister onItemClickedLister) {
        this.onItemClickedLister = onItemClickedLister;
    }

    public void onEnableLocationButtonStateChanged() {
        if (currentState == STATE_LOCATION_DEFAULT) {
            notifyItemChanged(0);
        }
    }

    public void onSetProgressForRecentItem(RecentLocationViewModel locationItem) {
        for (int i = 0; i < recentLocations.size(); i++) {
            if (locationItem.getId() == recentLocations.get(i).getId()) {
                recentLocations.get(i).setProgress(true);
                if (currentState == STATE_LOCATION_DEFAULT) {
                    notifyItemChanged(2 + i);
                }
            }
        }
    }

    public void onSetProgressForSearchHistory() {
        RecentLocationViewModel searchItem;
        for (int i = 0; i < recentLocations.size(); i++) {
            searchItem = recentLocations.get(i);
            searchItem.setProgress(true);
        }

        if (currentState == STATE_LOCATION_DEFAULT) {
            notifyItemRangeChanged(2, recentLocations.size() + 2);
        }
    }

    public void onRemovedSearchHistory() {
        int recentLocationsCount = recentLocations.size();
        recentLocations.clear();
        if (currentState == STATE_LOCATION_DEFAULT) {
            notifyItemRangeRemoved(1, recentLocationsCount + 2);
        }
    }

    public void showDefaultState(List<RecentLocationViewModel> locations) {currentState = STATE_LOCATION_DEFAULT;
        this.recentLocations = locations;
        notifyDataSetChanged();
    }

    public void showSearchState(GetAutoCompleteSearchLocationsResponse autocompleteItems) {
        currentState = STATE_LOCATION_SEARCH;
        this.autocompleteItems = autocompleteItems;
        notifyDataSetChanged();
    }

    public void onRemovedSearchEntry(RecentLocationViewModel removedSearchItem) {
        RecentLocationViewModel searchItem;
        for (int i = 0; i < recentLocations.size(); i++) {
            searchItem = recentLocations.get(i);
            if (searchItem.getId() == removedSearchItem.getId()) {
                searchItem.setRemoved(true);

                int notRemovedRecentLocations = getNotRemovedRecentLocations();
                if (notRemovedRecentLocations == 0) {
                    onRemovedSearchHistory();
                } else if (currentState == STATE_LOCATION_DEFAULT) {
                    notifyItemChanged(i + 2);
                }
                return;
            }
        }
    }

    private int getNotRemovedRecentLocations() {
        int count = 0;
        for (int i = 0; i < recentLocations.size(); i++) {
            if (!recentLocations.get(i).isRemoved()) {
                count++;
            }
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        switch(currentState){
            case STATE_LOCATION_DEFAULT:
                if (position == 0) {
                    return ITEM_PROVIDE_LOCATION;
                } else if (position == 1) {
                    return ITEM_HEADER;
                } else if (position == recentLocations.size() + 2) {
                    return ITEM_REMOVE_HISTORY;
                }
                return ITEM_LOCATION;
            case STATE_LOCATION_SEARCH:
                return ITEM_POPULAR;
        }
        throw new RuntimeException("No such state");
    }

    @Override
    public int getItemCount() {
        if (currentState == STATE_LOCATION_DEFAULT) {
            if (recentLocations.isEmpty()) {
                return 1;
            }
            return recentLocations.size() + 3;
        } else {
            return autocompleteItems.locations.size();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM_HEADER:
                View headerView = inflater.inflate(R.layout.item_search_header, parent, false);
                viewHolder = new HeaderViewHolder(headerView);
                break;
            case ITEM_PROVIDE_LOCATION:
                View locationView = inflater.inflate(R.layout.item_search_provide_location, parent, false);
                viewHolder = new LocationViewHolder(locationView);
                break;
            case ITEM_LOCATION:
                View recentView = inflater.inflate(R.layout.item_search_recent_search, parent, false);
                viewHolder = new RecentViewHolder(recentView);
                break;
            case ITEM_POPULAR:
                View popularView = inflater.inflate(R.layout.item_search_location_autocomplete, parent, false);
                viewHolder = new AutocompleteViewHolder(popularView);
                break;
            case ITEM_REMOVE_HISTORY:
                View removeView = inflater.inflate(R.layout.item_search_remove_history, parent, false);
                viewHolder = new RemoveViewHolder(removeView);
                break;
            default:
                throw new RuntimeException("No such view type");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_HEADER:((HeaderViewHolder) holder).bind(App.getAppContext().getString(R.string.search_recent_locations));
                break;
            case ITEM_PROVIDE_LOCATION:
                ((LocationViewHolder) holder).bind();
                break;
            case ITEM_LOCATION:
                ((RecentViewHolder) holder).bind(getRecentSearchItem(position), onItemClickedLister);
                break;
            case ITEM_POPULAR:
                ((AutocompleteViewHolder) holder).bind(getPopularSearchItem(position), onItemClickedLister);
                break;
            case ITEM_REMOVE_HISTORY:
                ((RemoveViewHolder) holder).bind(onItemClickedLister);
                break;
        }
    }

    private RecentLocationViewModel getRecentSearchItem(int position) {
        return recentLocations.get(position - 2);
    }

    private GetAutoCompleteSearchLocationsResponse.StatusLocation getPopularSearchItem(int position) {
        return autocompleteItems.getStatusLocation(position);
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvHeader)
        TextView tvHeader;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(String text) {
            tvHeader.setText(text);
        }
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAroundMe)
        TextView tvAroundMe;
        @BindView(R.id.btnEnableLocation)
        Button btnEnableLocation;

        LocationViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind() {
            if (onItemClickedLister.hasLocationPermissions()) {
                btnEnableLocation.setVisibility(View.GONE);
            } else {
                btnEnableLocation.setVisibility(View.VISIBLE);
            }

            tvAroundMe.setOnClickListener(v -> onItemClickedLister.onAroundMeClicked());

            btnEnableLocation.setOnClickListener(v -> onItemClickedLister.onProvideLocationClicked());
        }
    }

    static class AutocompleteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        AutocompleteViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, OnItemClickedLister listener) {
            tvTitle.setText(statusLocation.getLocation());
            itemView.setOnClickListener(v -> listener.onSelectAutocomplete(statusLocation));
        }
    }

    static class RecentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvRemoved)
        TextView tvRemoved;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.ivRemove)
        ImageView ivRemove;
        @BindView(R.id.viewDivider)
        View viewDivider;

        RecentViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(RecentLocationViewModel searchItem, OnItemClickedLister listener) {
            if (searchItem.isRemoved()) {
                setVisibilityForAllViews(false);
                tvRemoved.setVisibility(View.VISIBLE);
                tvTitle.setVisibility(View.INVISIBLE);
                itemView.setOnClickListener(null);
                tvTitle.setText("");
            } else {
                setVisibilityForAllViews(true);
                tvRemoved.setVisibility(View.GONE);
                itemView.setOnClickListener(v -> listener.onSelectAutocomplete(
                        new GetAutoCompleteSearchLocationsResponse.StatusLocation(searchItem.getLocation())));
                tvTitle.setText(searchItem.getLocation());

                if (searchItem.isProgress()) {
                    ivRemove.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    viewDivider.setVisibility(View.VISIBLE);
                }

            }

            ivRemove.setOnClickListener(v -> listener.onRemoveSearchEntry(searchItem));
        }

        private void setVisibilityForAllViews(boolean visible) {
            ViewGroup root = (ViewGroup) itemView;
            for (int i = 0; i < root.getChildCount(); i++) {
                root.getChildAt(i).setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        }
    }

    static class RemoveViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        RemoveViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(OnItemClickedLister listener) {
            itemView.setOnClickListener(v -> listener.onRemoveSearchLocationsHistory());
        }
    }

    public interface OnItemClickedLister {
        boolean hasLocationPermissions();

        void onRemoveSearchEntry(RecentLocationViewModel searchItem);

        void onSelectAutocomplete(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation);

        void onRemoveSearchLocationsHistory();

        void onProvideLocationClicked();

        void onAroundMeClicked();
    }
}
