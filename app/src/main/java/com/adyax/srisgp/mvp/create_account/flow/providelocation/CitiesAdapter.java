package com.adyax.srisgp.mvp.create_account.flow.providelocation;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adyax.srisgp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/5/16.
 */

public class CitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM_PROVIDE_LOCATION = 1;
    private static final int ITEM_CITY = 2;

    private List<String> cities = new ArrayList<>();
    private OnItemClickedLister listener;
    private int currentState = ProvideLocationPresenter.STATE_LOCATION_DEFAULT;

    public CitiesAdapter(OnItemClickedLister lister) {
        this.listener = lister;
    }

    public void showSearchState(List<String> cities) {
        currentState = ProvideLocationPresenter.STATE_LOCATION_SEARCH;
        this.cities = cities;
        notifyDataSetChanged();
    }

    public void showDefaultState() {
        currentState = ProvideLocationPresenter.STATE_LOCATION_DEFAULT;
        notifyDataSetChanged();
    }

    public void onEnableLocationButtonStateChanged() {
        if (currentState == ProvideLocationPresenter.STATE_LOCATION_DEFAULT) {
            notifyItemChanged(0);
        }
    }

    public void clearData() {
        this.cities = Collections.emptyList();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (currentState == ProvideLocationPresenter.STATE_LOCATION_DEFAULT) {
            return 1;
        }
        return cities.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (currentState == ProvideLocationPresenter.STATE_LOCATION_DEFAULT && position == 0) {
            return ITEM_PROVIDE_LOCATION;
        }
        return ITEM_CITY;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM_PROVIDE_LOCATION:
                View locationView = inflater.inflate(R.layout.item_search_provide_location, parent, false);
                viewHolder = new LocationViewHolder(locationView);
                break;
            case ITEM_CITY:
                View cityView = inflater.inflate(R.layout.item_search_popular, parent, false);
                viewHolder = new CityViewHolder(cityView);
                break;
            default:
                throw new RuntimeException("No such view type");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_PROVIDE_LOCATION:
                ((LocationViewHolder) holder).bind();
                break;
            case ITEM_CITY:
                ((CityViewHolder) holder).bind(getCity(position));
                break;
        }
    }

    private String getCity(int position) {
        return cities.get(position);
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAroundMe)
        TextView tvAroundMe;
        @BindView(R.id.btnEnableLocation)
        Button btnEnableLocation;

        LocationViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind() {
            if (listener.hasLocationPermissions()) {
                btnEnableLocation.setVisibility(View.GONE);
            } else {
                btnEnableLocation.setVisibility(View.VISIBLE);
            }

            tvAroundMe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onAroundMeClicked();
                }
            });

            btnEnableLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProvideLocationClicked();
                }
            });
        }
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        CityViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(String popularPhrase) {
            tvTitle.setText(popularPhrase);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelectAutocomplete(popularPhrase);
                }
            });
        }
    }

    public interface OnItemClickedLister {
        boolean hasLocationPermissions();

        void onSelectAutocomplete(String searchText);

        void onProvideLocationClicked();

        void onAroundMeClicked();
    }
}
