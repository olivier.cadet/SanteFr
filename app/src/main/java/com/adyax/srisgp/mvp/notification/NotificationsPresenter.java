package com.adyax.srisgp.mvp.notification;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.GetNotificationCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.NotificationResponse;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.net.response.tags.NotificationUsers;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 9/28/16.
 */

public class NotificationsPresenter extends Presenter<NotificationsPresenter.NotificationsView>
        implements AppReceiver.AppReceiverCallback {

    private static final int COMMAND_NOTIFICATIONS = 0;
    public static final String ACTION_COUNT_NOTIFICATION = "ACTION_COUNT_NOTIFICATION";
    public static final String COUNT_NOTIFICATION = "COUNT_NOTIFICATION";

    private StatusNotification statusNotification;
    private AppReceiver appReceiver = new AppReceiver();

    private boolean hasMore = true;
    private FragmentFactory fragmentFactory;

    @Inject
    public NotificationsPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(@NonNull NotificationsView view) {
        super.attachView(view);
        appReceiver.setListener(this);
//        requestNotifications(0);
    }

    public void onItemClicked(NotificationItem item) {
        if (item == null)
            return;
        new TrackerHelper(item).clickForWriteToHistory();
        switch (item.getType()) {
            case APPLICATIONS:
            case NUMERO_TEL:
            case LIENS_EXTERNES:
                getView().openNotificationScreen(item);
                break;

            default:
//                fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.NOTIFICATIONS), -1);
                new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.NOTIFICATIONS);
                break;
        }
    }

    public void setStatusNotification(StatusNotification statusNotification) {
        this.statusNotification = statusNotification;
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void requestNotifications(int offset) {
        if (hasMore || offset == 0) {
            GetNotificationCommand command = new GetNotificationCommand(offset, statusNotification);
            ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_NOTIFICATIONS);
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_NOTIFICATIONS) {
            NotificationResponse response = data.getParcelable(CommandExecutor.BUNDLE_NOTIFICATIONS);
            NotificationUsers notifications = null;
            if (statusNotification == StatusNotification.all) {
                notifications = response.all;
            } else if (statusNotification == StatusNotification.read) {
                notifications = response.read;
            } else if (statusNotification == StatusNotification.unread) {
                notifications = response.unread;
            }

            boolean firstPage = data.getBoolean(CommandExecutor.BUNDLE_IS_FIRST_PAGE);

            if (notifications != null) {
                hasMore = notifications.hasMore();
            } else {
                hasMore = false;
            }

            if (getView() != null) {
                getView().showNotifications(notifications, firstPage);
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public interface NotificationsView extends IView {
        void openNotificationScreen(NotificationItem item);

        void showNotifications(NotificationUsers notifications, boolean firstPage);

        void showEmptyView();
    }
}
