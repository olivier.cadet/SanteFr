package com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.TaxonomyVocabularyTermsCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.GooglePlusPresenter;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class PointsOfInterest_3_Presenter extends GooglePlusPresenter<PointsOfInterest_3_Presenter.CreateRecipeView> {

    @Inject
    FragmentFactory fragmentFactory;

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch(requestCode) {
            case ExecutionService.TAXONOMY_VOCABULARY_TERMS_ACTION:
                final TaxonomyVocabularyTermsResponse termsResponse = data.getParcelable(CommandExecutor.TAXONOMY_VOCABULARY_TERMS_RESPONSE);
                getView().updateTaxonomyVocabularyTermsResponse(termsResponse);
                break;
            case ExecutionService.CREATE_GOOGLE_PLUS_ACCOUNT_ACTION:
                initGoogle();
                startConnectGooglePlus();
                break;
            case ExecutionService.SEND_EMAIL_CONFIRMATION_ACTION:
                break;
            case ExecutionService.LOGIN_ACTION:
                getView().onSuccess();
                fragmentFactory.startAnimateCreateAccount(getContext());
                ((Activity)getContext()).finish();
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try{
            if(errorMessage!=null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        }catch (Exception e){
        }
    }

    public void moveToNext() {
        final IViewPager activity = (IViewPager) getContext();
        activity.getCreateAccountHelper().sendCreateAccountCommand(getContext(), getAppReceiver(), CreateAccountCommand.START_REGISTER);
    }

    public void sendTaxonomyVocabularyTerms() {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new TaxonomyVocabularyTermsCommand(TaxonomyVocabularyTermsCommand.INTERESTS), ExecutionService.TAXONOMY_VOCABULARY_TERMS_ACTION);
    }

    public interface CreateRecipeView extends IView {
        void updateTaxonomyVocabularyTermsResponse(TaxonomyVocabularyTermsResponse taxonomyVocabularyTermsResponse);
        void onFail(ErrorResponse errorResponse);
        void onSuccess();
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
        App.getApplicationComponent().inject(this);
    }

}
