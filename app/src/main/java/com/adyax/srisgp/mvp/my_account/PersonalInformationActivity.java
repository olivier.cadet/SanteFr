package com.adyax.srisgp.mvp.my_account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.ActivityPersonalInformationBinding;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.YearsHelper;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;

public class PersonalInformationActivity extends MaintenanceBaseActivity implements
        PersonalInformationPresenter.PersonalInformationView {

    @Inject
    PersonalInformationPresenter presenter;

    private ActivityPersonalInformationBinding binding;

    public static void start(Activity activity) {
        new TrackerHelper().addScreenView(TrackerLevel.ACCOUNT_MANAGEMENT, TrackerAT.MES_INFORMATIONS_PERSONNELLES);
        Intent intent = new Intent(activity, PersonalInformationActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        presenter.attachView(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_personal_information);
        binding.ageSpinner.setObjects(YearsHelper.getYearsArray());
        binding.genderSpinner.setObjects(GenderType.getStringArray(this).toArray(new String[GenderType.getSize()]));

        binding.genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.selectGender(GenderType.getStringArray(getContext()).get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.ageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                presenter.selectAge(YearsHelper.getYearsArray()[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.cityTextView.setOnClickListener(v -> presenter.changeLocation());
        binding.validateButton.setOnClickListener(v -> presenter.validate());

//        binding.labelBottom.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.labelBottom, false);

        presenter.onViewCreated();

        initActionBar();
    }

    private void initActionBar() {
        setSupportActionBar((Toolbar) binding.getRoot().findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.my_account_title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setGender(GenderType gender) {
        int selection = gender.ordinal();
        if (selection >= 0 && selection < GenderType.getSize())
            binding.genderSpinner.setSelection(selection);
    }

    @Override
    public void setYear(String year) {
        int selection = YearsHelper.getPosition(year);
        if (selection >= 0 && selection < YearsHelper.getYearsArray().length)
            binding.ageSpinner.setSelection(selection);
    }

    @Override
    public void setCity(String city) {
        binding.cityTextView.setText(city);
    }

    @Override
    public void stateChanged(boolean isChanged) {
        binding.validateButton.setEnabled(isChanged);
    }

    @NonNull
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
