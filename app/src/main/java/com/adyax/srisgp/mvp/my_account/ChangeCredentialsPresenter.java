package com.adyax.srisgp.mvp.my_account;

import android.os.Bundle;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.NetworkPresenter;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.utils.Utils;

import javax.inject.Inject;

/**
 * Created by Kirill on 17.11.16.
 */

public class ChangeCredentialsPresenter extends NetworkPresenter<ChangeCredentialsPresenter.ChangeCredentialsView> {
    public interface ChangeCredentialsView extends IView {
        void setEmail(String email);

        String getNewEmail();

        String getOldPass();

        String getNewPass();

        void showValidateEmail(boolean isShow);

        void showValidatePass(boolean isShow);

        void showEmailChangedResult();

        void showPassChangedResult();

        void setNewEmailError(String error);

        void setOldPassError(String error);

        void setNewPassError(String error);
    }

    private static final int MIN_EMAIL_CHARS = 8;
    private static final int OLD_MIN_PASS_CHARS = 8;
    private static final int MIN_PASS_CHARS = 12;

    private IRepository repository;
    private FragmentFactory fragmentFactory;

    private String currentEmail;

    @Inject
    public ChangeCredentialsPresenter(IRepository repository, FragmentFactory fragmentFactory) {
        this.repository = repository;
        this.fragmentFactory = fragmentFactory;
    }

    public void onViewCreated() {
        initData();
    }

    private void initData() {
        User user = repository.getCredential().user;
        if (user != null) {
            String email = user.mail == null ? "" : user.mail;
            currentEmail = email;
            setEmail(email);
        }
    }

    private void setEmail(String email) {
        if (getView() != null) {
            getView().setEmail(email);
        }
    }

    public boolean isShowValidateEmail() {
        String newEmail = getView().getNewEmail();

        boolean isValid = true;

        if (newEmail.length() < MIN_EMAIL_CHARS) {
            isValid = false;
        }

        if (newEmail.equals(currentEmail)) {
            isValid = false;
        }

        if (getView() != null)
            getView().showValidateEmail(isValid);

        return isValid;
    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    public String validateNewEmail() {
        String newEmail = getView().getNewEmail();

        String error = null;

        if (newEmail.length() < MIN_PASS_CHARS)
            error = getString(R.string.change_credentials_error_wrong_email);

        if (!Utils.isEmailValid(newEmail))
            error = getString(R.string.change_credentials_error_wrong_email);

        return error;
    }

    public boolean isShowValidatePass() {
        String oldPass = getView().getOldPass();
        String newPass = getView().getNewPass();

        boolean isValid = true;

        if (oldPass.length() < OLD_MIN_PASS_CHARS)
            isValid = false;

        if (newPass.length() < MIN_PASS_CHARS)
            isValid = false;

        if (getView() != null)
            getView().showValidatePass(isValid);

        return isValid;
    }

    public String validateOldPassword() {
        String oldPass = getView().getOldPass();

        String error = null;

        if (oldPass.length() < OLD_MIN_PASS_CHARS)
            error = getString(R.string.change_credentials_error_wrong_pass);

        if (!Utils.isPasswordValid(oldPass))
            error = getString(R.string.change_credentials_error_wrong_pass);

        return error;
    }

    public String validateNewPassword() {
        String newPass = getView().getNewPass();

        String error = null;

        if (newPass.length() < MIN_PASS_CHARS)
            error = String.format(getString(R.string.change_credentials_error_pass_too_short), MIN_PASS_CHARS);

        if (!Utils.isPasswordValid(newPass))
            error = getString(R.string.password_rule);

        return error;
    }

    public void validateEmailClick() {
        String newEmail = getView().getNewEmail();

//        getView().setOldEmailError(getString(R.string.change_credentials_error_old_email));
//        getView().setNewEmailError(getString(R.string.change_credentials_error_new_email));

        String newEmailError = validateNewEmail();

        getView().setNewEmailError(newEmailError);

        if (newEmailError == null) {
            sendChangeEmail(newEmail, getView().getNewPass());
        }
    }

    public void validatePasswordClick() {
        String newPass = getView().getNewPass();

//        getView().setOldPassError(getString(R.string.change_credentials_error_old_pass));
//        getView().setNewPassError(String.format(getString(R.string.change_credentials_error_new_pass), MIN_PASS_CHARS));

        String oldPassError = validateOldPassword();
        String newPassError = validateNewPassword();

        getView().setOldPassError(oldPassError);
        getView().setNewPassError(newPassError);

        if (oldPassError == null && newPassError == null) {
            sendChangePass(newPass ,getView().getOldPass());
        }
    }

    private void sendChangeEmail(String email, String oldPass) {
        ChangeCredentialsCommand command = new ChangeCredentialsCommand(email, ChangeCredentialsCommand.CHANGE_EMAIL, oldPass);
        sendCommand(command, ExecutionService.CHANGE_EMAIL_REQUEST_CODE);
        showProgress();
    }

    private void sendChangePass(String newPass, String oldPass) {
        ChangeCredentialsCommand command = new ChangeCredentialsCommand(newPass, ChangeCredentialsCommand.CHANGE_PASS, oldPass);
        sendCommand(command, ExecutionService.CHANGE_PASS_REQUEST_CODE);
        showProgress();
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        hideProgress();
        switch (requestCode) {
            case ExecutionService.CHANGE_EMAIL_REQUEST_CODE:
                initData();
                if (getView() != null) {
                    getView().showEmailChangedResult();
                }
                break;

            case ExecutionService.CHANGE_PASS_REQUEST_CODE:
                if (getView() != null) {
                    getView().showPassChangedResult();
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try {
            if (errorMessage != null) {
                final ErrorResponse errorResponse = ErrorResponse.create(errorMessage);
                final ErrorItem errorItem = errorResponse.getErrorAnyItem();
                final String errorMessageText = errorItem.getLocalizeErrorMessage(getContext());
                switch(errorItem.getErrorCode()){
                    case ErrorItem.WRONG_PASSWORD:
                        getView().setOldPassError(errorMessageText);
                        break;
                    case ErrorItem.SAME_PASSWORD:
                        getView().setNewPassError(errorMessageText);
                        break;
                    case ErrorItem.EMAIL_ALREADY_EXISTS:
                        getView().setNewEmailError(errorMessageText);
                        break;
                }
//                getView().onFail(errorResponse);
            }
        } catch (Exception e) {

        }
        hideProgress();
    }
}
