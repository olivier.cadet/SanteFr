package com.adyax.srisgp.mvp.searchresults;

import androidx.fragment.app.Fragment;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;

/**
 * Created by anton.kobylianskiy on 10/26/16.
 */

public interface SearchResultsListener {
    void showGetInformedCount(int count);
    void showFindCount(int count);
    void onNoResults(SearchType searchType);
    void onReminderClose();
    void onInfoRelatedItemClicked(RelatedItem item);
    void onFindRelateItemClicked(RelatedItem item);
    void onChangeMapActionButtonVisibility(boolean visible);
    void onChangeFilterActionButtonVisibility(boolean visible, Fragment fragment);
    void openFindTab();
    void openInfoTab();
    void onRemoveSearchMessage(SearchMessage searchMessage);
    void onFirstPageLoaded(SearchType searchType, boolean empty);
    void addScreenViewResearch(int item);
}
