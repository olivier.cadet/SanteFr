package com.adyax.srisgp.mvp.ask_login;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentAskLoginBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.my_account.MyAccountFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AskLoginFragment extends ViewFragment implements AskLoginPresenter.DummyView {

    @Inject
    AskLoginPresenter presenter;
    private FragmentAskLoginBinding binding;
    private AskLoginArg askLoginArg;
    private MyAccountFragment.MyAccountListener listener;

    public AskLoginFragment() {
    }

    public static Fragment newInstance(AskLoginArg askLoginArg) {
        AskLoginFragment fragment = new AskLoginFragment();
        Bundle bundle=new Bundle();
        bundle.putParcelable(AskLoginArg.BUNDLE, askLoginArg);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (MyAccountFragment.MyAccountListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        askLoginArg=getArguments().getParcelable(AskLoginArg.BUNDLE);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ask_login, container, false);
        binding.imageAskLogin.setImageResource(askLoginArg.getImageResId());
        binding.titleAskLogin.setText(askLoginArg.getTitleResId());
        binding.bodyAskLogin.setText(askLoginArg.getBodyResId());
        binding.noThankYouLater.setOnClickListener(view -> getPresenter().clickLogIn());
        binding.btnEnableLocation.setOnClickListener(view -> getPresenter().clickCreateAccount());
        return binding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.log_in, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_in) {
            getPresenter().clickLogIn();
        }
        return super.onOptionsItemSelected(item);
    }

    @NonNull
    @Override
    public AskLoginPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

}
