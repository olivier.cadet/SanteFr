package com.adyax.srisgp.mvp.web_views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import androidx.appcompat.app.AlertDialog;

import android.os.Build;
import android.util.Base64;
import android.view.View;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.di.modules.NetworkModule;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class DefaultWebViewClient extends WebViewClient {
    public static final String REFERER = "referrer";
    public static final String HOME_PAGE_REFERER = "/";
    private Context context;
    Map<String, String> customHttpHeaders = null;

    public DefaultWebViewClient(Context context) {
        this.context = context;
    }

    @Override
    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
//            handler.proceed(UrlExtras.USER, UrlExtras.PASS);
        String[] spil = BuildConfig.CREDENTIAL_SERVER.split(":");
        handler.proceed(spil[0], spil[1]);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getResources().getString(R.string.invalid_certificate_text) + "\n\n" + error.getUrl());

        builder.setPositiveButton(R.string.continue_title, (dialog, which) -> handler.proceed());

        builder.setNegativeButton(R.string.cancel_title, (dialog, which) -> handler.cancel());
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
    }

    private String mapGetOrDefault(Map<String, ?> map, String key) {
        Object value = map.get(key);
        return value == null ? "" : value.toString();
    }

    public void setCustomHttpHeaders(Map<String, String> customHttpHeaders) {
        this.customHttpHeaders = customHttpHeaders;
    }

    public void setHeadersForSantefr(IRepository repository) {
        Map<String, String> additionalHttpHeaders = new HashMap<>();
        additionalHttpHeaders.clear();
        RegisterUserResponse credential = repository.getCredential();
        final String basic = NetworkModule.BASIC + Base64.encodeToString(BuildConfig.CREDENTIAL_SERVER.getBytes(), Base64.NO_WRAP);
        additionalHttpHeaders.put(NetworkModule.AUTHORIZATION, basic);
        additionalHttpHeaders.put(NetworkModule.COOKIE, String.format("%s=%s", credential.getSession_name(), credential.getSessid()));
        additionalHttpHeaders.put(NetworkModule.X_REQUESTED_WITH, NetworkModule.I_OS_VIEW);
        additionalHttpHeaders.put(DefaultWebViewClient.REFERER, DefaultWebViewClient.HOME_PAGE_REFERER);
        additionalHttpHeaders.put(NetworkModule.X_USER_AGENT, NetworkModule.getVersionString(context));

        this.setCustomHttpHeaders(additionalHttpHeaders);
    }

    public WebView setDefaultWebviewSettingsForBMC(WebView wv) {
        wv.setWebChromeClient(new DefaultChromeClient());
        wv.getSettings().setGeolocationEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDisplayZoomControls(false);
        wv.getSettings().setAllowFileAccess(true);
        wv.getSettings().setAppCacheEnabled(true);
        wv.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(false);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        return wv;
    }

    //Intercept WebView Requests for Android API < 21
    @Deprecated
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if (url.startsWith("http://")) {
            try {
                //change protocol of url string
                url = url.replace("http://","https://");

                //return modified response
                URL httpsUrl = new URL(url);
                URLConnection connection = httpsUrl.openConnection();
                return new WebResourceResponse(connection.getContentType(), connection.getContentEncoding(), connection.getInputStream());
            } catch (Exception e) {
                //an error occurred
            }
        }
        return super.shouldInterceptRequest(view, url);
    }

    //Intercept WebView Requests for Android API >= 21
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (UrlExtras.API_URL.equals("https://" + request.getUrl().getHost())) {
            if (customHttpHeaders != null) {
                request.getRequestHeaders().putAll(customHttpHeaders);
                request.getRequestHeaders().put("referrer", UrlExtras.API_URL); // fix referrer
                customHttpHeaders = request.getRequestHeaders();

                if (request.getUrl() != null && request.getMethod().equalsIgnoreCase("get")) {
                    String scheme = request.getUrl().getScheme().trim();
                    if (scheme.equalsIgnoreCase("https")) {
                        try {
                            URL url = new URL(request.getUrl().toString());
                            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                            connection.setHostnameVerifier(WhitelistHostsVerifier.INSTANCE);
                            for (Map.Entry<String, String> entry : customHttpHeaders.entrySet()) {
                                connection.setRequestProperty(entry.getKey(), entry.getValue());
                            }
                            return new WebResourceResponse(connection.getContentType(), connection.getHeaderField("encoding"), connection.getInputStream());
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return null;
            }
        }
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        // SAN-3022 Inject javascript
        // Log.v("DEV", "SAN-3022 ignore - JS Injected");
        view.loadUrl("javascript: window.useBrowserLocationFramework = true;");
    }
}
