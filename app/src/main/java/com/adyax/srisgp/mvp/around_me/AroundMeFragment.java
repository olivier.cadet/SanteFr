package com.adyax.srisgp.mvp.around_me;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.RectF;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.databinding.FragmentAroundMeBinding;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.mapbox.PulseMarkerView;
import com.adyax.srisgp.mvp.around_me.get_location.GetCityActivity;
import com.adyax.srisgp.mvp.around_me.get_location.GetCityFragment;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.utils.Utils;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.google.android.gms.common.api.Status;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.MarkerView;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.geometry.VisibleRegion;
import com.mapbox.mapboxsdk.maps.MapboxHelper;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import timber.log.Timber;

public class AroundMeFragment extends ViewFragment implements AroundMePresenter.AroundMeView, IAroundMe {

    private static final int REQUEST_LOCATION = 123;
    //    private static final int REQUEST_STORAGE_PERMISSION = 2;
    private static final String ARG_SEARCH_COMMAND = "arg_search_command";

    public static final int FILTERS_ACTIVITY_REQUEST_CODE = 234;
    public static final String GROUP_MARKER = "=";
    public int MY_POSITION_ID = 100000;

    @Inject
    AroundMePresenter presenter;
    @Inject
    ITracker tracker;

    private boolean isFirstStart = true;

    private MapboxMap mapboxMap;
    private final Object syncMarkers = new Object();
//    private MarkerView userMarker;

    private Map<String, SearchItemGroup> groupMarkers = new HashMap<>();
    private RecyclerView recyclerViewGroupMarker;
    private MarkerGroupItemAdapter adapterGroupMarker;
    private long activeIdForClusterMarker;

    private FragmentAroundMeBinding binding;

    private MarkerOptions selectedMarker;

    private int visibleThreshold = 1;
    private boolean shouldSelectMarker = true;
    private boolean shouldZoom = true;
    private LinearLayoutManager layoutManager;

    private SearchResultsAroundMeListener resultsListener;
    private Marker shownInfoWindowMarker;

    private List<DeferredAction> deferredActions = new ArrayList<>();

    private Map<Marker, ISearchItem> markerLinks = new HashMap<>();

    private boolean searchFromActivityResultDone = false;
    private boolean requestShouldBeDone;
    private boolean wasStop;

    private MapboxMap.OnCameraChangeListener cameraChangeListener = new MapboxMap.OnCameraChangeListener() {
        @Override
        public void onCameraChange(CameraPosition position) {
            if (prevCameraPosition == null) {
                return;
            }

            if (prevCameraPosition.zoom != position.zoom || prevCameraPosition.target.getLongitude() != position.target.getLongitude() || prevCameraPosition.target.getLatitude() != position.target.getLatitude()) {
                if (isRecenterMode()) {
                    Timber.d("onCameraChange");
                    setRecenterMode(View.VISIBLE);
                }
            }
            prevCameraPosition = position;
        }
    };

    private CameraPosition prevCameraPosition;
    private ISearchItem lastItem2;

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:
                    synchronized (groupMarkers) {
                        final int absPosition = ((LinearLayoutManager)
                                recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                        if (absPosition != -1) {
                            setAdvertPos(absPosition);
                        } else {
//                            setAdvertPos(((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition());
                        }
                    }
                    break;
            }
        }
    };

    private void setAdvertPos(int absPosition) {
        //                    final ISearchItem item = presenter.getItemInPosition(absPosition);
        final SearchItemHuge searchItemHuge = presenter.getItemInPosition(absPosition);
        if (searchItemHuge != null) {
            Timber.i("lastitem2: " + searchItemHuge.getTitle());
        }
        final ISearchItem item = getGroupItemInPosition(absPosition);
        if (item != null) {
            selectMarker(findByGeoAndData(item));
        }
        // TODO it for update green position
        getGroupItemInPosition(absPosition);
        if (adapterGroupMarker != null) {
            adapterGroupMarker.notifyDataSetChanged();

        }
    }

    public static Fragment newInstance(AroundMeArguments aroundMeArguments) {
        AroundMeFragment fragment = new AroundMeFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AroundMeArguments.BUNDLE_NAME, aroundMeArguments);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public SearchCommand getSearchCommand() {
        return presenter.getSearchCommand();
    }

    @Override
    public boolean isSortedByDistance() {
        return getAroundMeArguments().isSortedByDistance();
    }

    @Override
    public void onFiltersChanged() {

    }

    @Override
    public boolean hasMoreItems() {
        return getAroundMeArguments().hasMoreItems();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public AroundMeArguments getAroundMeArguments() {
        return presenter.getAroundMeArguments();
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        if (context instanceof SearchResultsActivity) {
            resultsListener = (SearchResultsAroundMeListener) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        requestShouldBeDone = true;

        Mapbox.getInstance(getActivity(), getString(R.string.access_token));
        presenter.init(getArguments().getParcelable(AroundMeArguments.BUNDLE_NAME));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_around_me, container, false);
        binding.mapview2.onCreate(savedInstanceState);

        binding.mapview2.getMapAsync(mapboxMap1 -> {
            AroundMeFragment.this.mapboxMap = mapboxMap1;
            mapboxMap.getMarkerViewManager().addMarkerViewAdapter(new PulseMarkerViewAdapter(getContext()));
            mapboxMap1.setStyleUrl(presenter.getMapBoxStyle());
            mapboxMap1.setOnMarkerClickListener(marker -> {
                return onClickMarker(marker);
            });

            mapboxMap1.setOnInfoWindowClickListener(marker -> {
                if (!isMyLocation(marker)) {
                    presenter.clickAdvert(markerLinks.get(marker).getItem());
                    return true;
                }
                return false;
            });

            mapboxMap1.setInfoWindowAdapter(marker -> makeInfoWindowAdapter(inflater, marker));

            mapboxMap.setOnCameraChangeListener(cameraChangeListener);

            doDeferredActions();
        });

        initAdvertRecyclerView();

        initLocationText(getAroundMeArguments());

//        binding.changeLocation.setOnClickListener(v -> changeLocation(binding.locationTextView.getText().toString()));
        binding.changeLocation.setOnClickListener(v -> changeLocation(getAroundMeArguments().getGeoString(true)));
        binding.clearLocation.setOnClickListener(v -> changeLocation(""));

        binding.listing.setOnClickListener(v -> {
            if (resultsListener != null) {
                resultsListener.onListButtonClicked();
            }
        });

        binding.filters.setOnClickListener(view -> {
            if (resultsListener != null) {
                resultsListener.onFiltersButtonClicked();
            }
            presenter.callFiltersActivity();
        });

        binding.recenter.setOnClickListener(view -> recenterAction());
        return binding.getRoot();
    }

    private void recenterAction() {
        setShouldSelectMarker();
        shouldZoom = false;
        if (mapboxMap != null) {
//            LatLng center = mapboxMap.getCameraPosition().target;
            final VisibleRegion visibleRegion = mapboxMap.getProjection().getVisibleRegion();

            double lat1 = visibleRegion.nearRight.getLatitude();
            double lon1 = visibleRegion.farLeft.getLongitude();
            double lat2 = visibleRegion.farLeft.getLatitude();
            double lon2 = visibleRegion.nearRight.getLongitude();

            float coeffVisible = getPercentOfVisibleRegion();
            double _lat1 = lat1 + (lat2 - lat1) * coeffVisible;

            presenter.searchInLocation(_lat1, lon1, lat2, lon2);
        }
//            presenter.searchInLocation(center.getLatitude(), center.getLongitude());
//            Timber.d(center.getLatitude() + " " + center.getLongitude());
        setRecenterMode(View.GONE);
    }

    @Nullable
    private View makeInfoWindowAdapter(LayoutInflater inflater, Marker marker) {
        ViewGroup infoWindow = null;
        if (!isMyLocation(marker)) {
            final SearchItemGroup searchItemGroup = groupMarkers.get(getId(marker));
            if (isGroupMarker(marker)) {
                if (getActiveIdForClusterMarker() == 0) {
                    setActiveIdForClusterMarker(searchItemGroup.getActiveId());
                }
                adapterGroupMarker = new MarkerGroupItemAdapter(getActiveIdForClusterMarker(), searchItemGroup, (item, position) -> {
                    setActiveIdForClusterMarker(item.getId());
                    adapterGroupMarker.setActiveId(item.getId());
                    scrollToAdvert(searchItemGroup.getOffsetPosition() + position);
                });


                infoWindow = (ViewGroup) inflater.inflate(adapterGroupMarker.getItemCount() <= 2 ? R.layout.item_marker_group2 : R.layout.item_marker_group, null, false);
                recyclerViewGroupMarker = (RecyclerView) infoWindow.findViewById(R.id.markerGroupRecyclerView);
//                recyclerView.addItemDecoration(
//                        new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.default_margin_x0_5)));
                recyclerViewGroupMarker.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

                recyclerViewGroupMarker.setLayoutManager(new LinearLayoutManager(getContext()));
//                recyclerViewGroupMarker.setLayoutManager(new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false));
                recyclerViewGroupMarker.setItemAnimator(new DefaultItemAnimator());
                recyclerViewGroupMarker.setAdapter(adapterGroupMarker);
                final ISearchItem lastItem3 = getLastItem();
                if (lastItem3 != null) {
                    getGroupItemInPosition(lastItem3.getPosition());
                    getGroupItemInPosition(lastItem3.getPosition());
                }
            } else {
                infoWindow = (ViewGroup) inflater.inflate(R.layout.item_marker_info, null, false);
                TextView tvTitle = (TextView) infoWindow.findViewById(R.id.tvTitle);
                TextView tvSubtitle = (TextView) infoWindow.findViewById(R.id.tvSubtitle);

                tvTitle.setText(marker.getTitle());

                final String subtitleText = searchItemGroup.getSubtitle();//getId(marker);
                if (!TextUtils.isEmpty(subtitleText)) {
                    tvSubtitle.setText(subtitleText);
                } else {
                    tvSubtitle.setVisibility(View.GONE);
                }
            }
        }
        return infoWindow;
    }

    private float getPercentOfVisibleRegion() {
        final int bottomCardHeight = (int) getResources().getDimension(R.dimen.around_me_card_height);
        final int defaultMargin = (int) getResources().getDimension(R.dimen.default_margin);
        float height = mapboxMap.getHeight();
        return (bottomCardHeight + defaultMargin) / height;
    }

    private boolean equalsPosition(CameraPosition first, CameraPosition second) {
        if (first.zoom == second.zoom && first.target.getLatitude() == second.target.getLatitude() &&
                first.target.getLongitude() == second.target.getLongitude()) {
            return true;
        }
        return false;
    }

    @Override
    public void onChangeFilterButtonVisibility(boolean visible) {
        binding.filters.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        switch (getAroundMeArguments().getType()) {
            case around:
                initActionBar();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.d("onStart()");
        binding.mapview2.onStart();

        if (requestShouldBeDone) {
            presenter.updateStatus(presenter.getAroundMeArguments().getStatusLocation());
            presenter.onRequestCurrentLocation2(true, false);
            requestData(false, true);
        } else {
            if (wasStop) {
                final boolean wasSearchRequest = presenter.onRequestCurrentLocation2(true,
                        getAroundMeArguments().isCity() || isRecenterMode());//2
                requestData(wasSearchRequest, isFirstStart);
                isFirstStart = false;
                wasStop = false;
            }
        }
    }

    private boolean isRecenterMode() {
        return binding.recenter.getVisibility() == View.GONE;
    }

    private void setRecenterMode(int visibility) {
        binding.recenter.setVisibility(visibility);
    }

    public void requestData(boolean wasSearchRequest, boolean isIgnoreRecentButton) {
        switch (getAroundMeArguments().getType()) {
            case around:
                binding.listing.setVisibility(View.GONE);
                if (getAroundMeArguments().isAnyLocation()) {
                    if (!searchFromActivityResultDone && !wasSearchRequest) {
                        if (isRecenterMode() && !isIgnoreRecentButton) {
                            recenterAction();
                        } else {
                            presenter.search(SearchRequestType.START);
                        }
                    }
                } else {
//                    presenter.onRequestCurrentLocation(true);
                }
                break;
            case find:
            default:
                binding.locationLL.setVisibility(View.GONE);
                presenter.loadAroundMeItems();
                break;
        }
    }

    private void initAdvertRecyclerView() {
        layoutManager = new LinearLayoutManager(binding.advertRecycleview.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        binding.advertRecycleview.setLayoutManager(layoutManager);

        new GravitySnapHelper(Gravity.START).attachToRecyclerView(binding.advertRecycleview);

        binding.advertRecycleview.setAdapter(presenter.getAdvertAdapter());

        binding.advertRecycleview.addOnScrollListener(onScrollListener);

        binding.advertRecycleview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                if (lastVisibleItem != -1) {
                    if (presenter.getAdvertAdapter().isMoreLoadedEnabled() && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        presenter.loadMore();
                    }
                }
            }
        });

        binding.rvNotifications.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvNotifications.setAdapter(presenter.getIdfAdapter());
    }

    @Override
    public void initLocationText(AroundMeArguments arguments) {
        binding.locationTextView.setText(arguments.getGeoString(true));
    }

    private void initActionBar() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.search_around_me);
    }

    @NonNull
    @Override
    public AroundMePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void updateMarkers(Location location, Cursor cursor, boolean fromServer) {

        final SearchItemHuge firstItem = remakeGroupMarkers(cursor, getLastItem());

        addDeferredAction(new DeferredAction() {
            @Override
            public void doAction() {
                synchronized (groupMarkers) {
                    Marker myLocation = clearMarkers();
//                    boolean bFirstItem = true;
                    for (String address : groupMarkers.keySet()) {
                        final ISearchItem item = groupMarkers.get(address);
                        if (findByGeoAndData(item) == null) {
                            final MarkerOptions markerOptions = new MarkerOptions()
                                    .position(item.getLatLng())
                                    .title(item.getTitle())
//                                    .snippet(item.isGroup() ? item.getSubtitle() : null)
                                    .snippet(item.isGroup() ? GROUP_MARKER + item.getAddress() : item.getAddress())
                                    .icon(getIconOff(getContext(), item));

                            boolean nShowTitle = false;
//                            if (bFirstItem) {
//                                bFirstItem = false;
                            if (shouldSelectMarker) {
                                if (firstItem != null && item.contains(firstItem.getId())) {
                                    if (markerOptions != null) {
                                        if (selectedMarker != markerOptions) {
                                            if (selectedMarker != null) {
                                                selectedMarker.icon(getIconOff(getContext(), item));
                                            }
                                            selectedMarker = markerOptions;
                                            markerOptions.icon(getIconOn(getContext(), item));
                                        }
                                    }
                                    setShouldSelectMarker();
                                    nShowTitle = true;
                                }
                            }
//                            }

                            Marker marker = mapboxMap.addMarker(markerOptions);
                            markerLinks.put(marker, item);

                            if (nShowTitle) {
                                mapboxMap.selectMarker(marker);
                                shownInfoWindowMarker = marker;
                            }
                        }

                    }
                }

                if (shouldZoom && isFirstStart) {
                    moveMapToGeoPoint(mapboxMap.getMarkers(), !presenter.getSearchCommand().isBbox());
                    isFirstStart = false;
                }
                invalidateMap();
            }
        });
    }

    @Override
    public void setLoadingProgress(boolean isLoading) {
        binding.progressLayout.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showFindCount(int count) {
        if (resultsListener != null)
            resultsListener.showFindCount(count);
    }

    @Override
    public void setCurrentItem(int position) {
        binding.advertRecycleview.removeOnScrollListener(onScrollListener);
//        layoutManager.scrollToPosition(position);
        scrollToAdvert(position);
        binding.advertRecycleview.addOnScrollListener(onScrollListener);
    }

    private void changeLocation(String lastLocation) {
        Intent intent = GetCityActivity.newIntent(getContext(), lastLocation);
        startActivityForResult(intent, REQUEST_LOCATION);
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (resultCode == Activity.RESULT_OK) {
                    setActiveIdForClusterMarker(0);
                    final GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation =
                            data.getParcelableExtra(GetCityFragment.INTENT_STATUS_LOCATION);
                    presenter.updateStatus(statusLocation);
                    //                if (!statusLocation.isActive()) {
                    //                    ViewUtils.hideSoftKeyboard(getActivity());
                    //                    DialogUtils.showBadRegionDialog(getContext(), presenter.getConfig());
                    //                }
                    AroundMeArguments arguments;
                    if (statusLocation.isEmpty()) {
                        final double longitude = data.getDoubleExtra(GetCityFragment.INTENT_LONGITUDE, 0);
                        final double latitude = data.getDoubleExtra(GetCityFragment.INTENT_LATITUDE, 0);
                        arguments = new AroundMeArguments(SearchType.around, longitude, latitude);
                    } else {
                        final double longitude = data.getDoubleExtra(GetCityFragment.INTENT_LONGITUDE, 0);
                        final double latitude = data.getDoubleExtra(GetCityFragment.INTENT_LATITUDE, 0);
                        arguments = new AroundMeArguments(SearchType.around, statusLocation.getLocation(), longitude, latitude);
                    }
                    presenter.init(arguments);
                    initLocationText(arguments);

                    setShouldSelectMarker();
                    shouldZoom = true;
                    searchFromActivityResultDone = true;
                    setRecenterMode(View.GONE);
                    //                if(!wasStop) {
                    presenter.search(SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS);
                    //                }
                }
                break;
            case FILTERS_ACTIVITY_REQUEST_CODE://AAA1
                if (resultCode == Activity.RESULT_OK) {
                    setShouldSelectMarker();
                    //shouldZoom = true;
                    requestShouldBeDone = false;
                    Timber.d("onActivityResult");
                    setRecenterMode(View.GONE);

                    switch (getAroundMeArguments().getType()) {
                        case find:
                            if (getAroundMeArguments().isCity()) {
                                presenter.search(SearchRequestType.IMMEDIATELY);//1
                            }
                            break;
                        case around:
                            requestData(false, true);
                            break;
                    }
                    presenter.search(SearchRequestType.DEFAULT);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onEnableLocationButtonStateChanged() {

    }

    @Override
    public void showEnableLocationDialog(Status status) {

    }

    @Override
    public void updateLocation(Location location) {
        if (!getAroundMeArguments().isCity()) {
            showMyPosition(location);
        }
    }

    @Override
    public void showMessages(List<SearchMessage> messages) {
        binding.rvNotifications.setVisibility(messages.isEmpty() ? View.GONE : View.VISIBLE);
        presenter.getIdfAdapter().showMessages(messages);
    }

    public interface SearchResultsAroundMeListener {
        void onFiltersButtonClicked();

        void showFindCount(int count);

        void onListButtonClicked();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview2.onResume();
        searchFromActivityResultDone = false;
        requestShouldBeDone = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.mapview2.onStop();
//        requestShouldBeDone=true;
        wasStop = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapview2.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview2.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.mapview2.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapview2.onSaveInstanceState(outState);
    }

    @Override
    public void updateMap(SearchResponseHuge response) {
        AroundMeArguments aroundMeArguments = getAroundMeArguments();
        initLocationText(aroundMeArguments);
        if (response != null) {
            aroundMeArguments.setHasMoreItems(response.has_more > 0);
            aroundMeArguments.setSortedByDistance(response.distance_sort > 0);
            presenter.getAdvertAdapter().setMoreLoadedStatus(aroundMeArguments.hasMoreItems());
        }

        addDeferredAction(() -> {
            Location myLocation = aroundMeArguments.getMyLocationNew();
            if (aroundMeArguments.isCity()) {
//                if(response.isAroundGeoLocation()) {
//                    // geo in response have to same myLocation
//                    showMyPosition(myLocation);m
//                }else{
//
//                }
                if (!aroundMeArguments.isSearchInPosition()) {
                    if (response != null && response.isPreciseAddress()) {
                        showMyPosition(response.getPreciseAddress());
                    } else {
                        showMyPosition(myLocation);
                    }
                }
            } else {
                if (aroundMeArguments.isGeoLocation()) {
                    showMyPosition(myLocation);
                }
            }
        });
    }

    private void showMyPosition(Location myLocation) {
        if (mapboxMap == null) {
            return;
        }
        if (myLocation != null) {
            synchronized (syncMarkers) {
                final LatLng latLng = new LatLng(myLocation);
                for (Marker m : mapboxMap.getMarkers()) {
                    if (isMyLocation(m)) {
                        if (m.getPosition().equals(latLng)) {
                            return;
                        }
                        mapboxMap.removeMarker(m);
                        break;
                    }
                }
                addMyPositionMarker(latLng);
            }
        } else {
            synchronized (syncMarkers) {
                for (Marker m : mapboxMap.getMarkers()) {
                    if (isMyLocation(m)) {
                        mapboxMap.removeMarker(m);
                        break;
                    }
                }
            }
        }
    }

    private void addMyPositionMarker(LatLng latLng) {
//        if(BuildConfig.DEBUG) {
//            // TODO disable pulsing user positio because it have trouble
//            userMarker = mapboxMap.addMarker(new PulseMarkerViewOptions()
//                .position(latLng)
//                .anchor(0.5f, 0.5f)
//                , markerView -> animateMarker(userMarker));
//            return;
//        }
        mapboxMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(IconFactory.getInstance(getContext()).fromResource(R.drawable._ic_maps_my_position_24dp)));

    }

    private void moveMapToGeoPoint(List<Marker> markers, boolean isEnableMyPosition) {//AAA
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : markers) {
            if (isEnableMyPosition || !isMyLocation(marker)) {
                builder.include(new LatLng(marker.getPosition()));
            }
        }
        if (markers.size() >= 2) {
            int bottomCardHeight = (int) getResources().getDimension(R.dimen.around_me_card_height);
            int defaultMargin = 3 * (int) getResources().getDimension(R.dimen.default_margin);

            Timber.d("change listener: null");
            mapboxMap.setOnCameraChangeListener(null);
            final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
                    builder.build(),
                    defaultMargin,
                    2 * defaultMargin,
                    defaultMargin,
                    bottomCardHeight + defaultMargin);

            MapboxHelper.moveCamera(mapboxMap, cameraUpdate, new MapboxMap.CancelableCallback() {

                @Override
                public void onCancel() {
                    mapboxMap.setOnCameraChangeListener(cameraChangeListener);
                }

                @Override
                public void onFinish() {
                    Timber.d("change listener success");
                    mapboxMap.setOnCameraChangeListener(cameraChangeListener);
                    prevCameraPosition = mapboxMap.getCameraPosition();
                }
            });
        } else {

            if (!markers.isEmpty()) {
                mapboxMap.setCameraPosition(
                        new CameraPosition.Builder()
                                .target(markers.get(0).getPosition())
                                .zoom(15)
                                .build());
            }
            if (BuildConfig.DEBUG) {
                Toast.makeText(getContext(), "marker size is less 2", Toast.LENGTH_SHORT).show();
            }
        }
        shouldZoom = true;
    }

//    private void clearMarkers() {
//        synchronized (syncMarkers) {
//            Set<Marker> set = new HashSet<>();
//            for (Marker m : mapboxMap.getMarkers()) {
//                if (!isMyLocation(m)) {
//                    set.add(m);
//                } 
//            }
//            for (com.mapbox.mapboxsdk.annotations.Marker m : set) {
//                mapboxMap.removeMarker(m);
//            }
//            markerLinks.clear();
//        }
//    }

    private Marker clearMarkers() {
        Marker result = null;
        synchronized (syncMarkers) {
            Set<Marker> set = new HashSet<>();
            for (Marker m : mapboxMap.getMarkers()) {
                if (!isMyLocation(m)) {
                    set.add(m);
                } else {
//                    m.setId(MY_POSITION_ID++);
                    result = m;
                }
            }
            for (Marker m : set) {
                mapboxMap.removeMarker(m);
            }
            final List<Marker> markers = mapboxMap.getMarkers();
            if (markers.size() > 1) {
                // we have more that one my position!!!
                // we need remove first position
                set.clear();
                for (int i = 0; i < (markers.size() - 1); i++) {
                    set.add(markers.get(i));
                }
                for (Marker m : set) {
                    mapboxMap.removeMarker(m);
                }
            }
            markerLinks.clear();
        }
        return result;
    }

    private boolean isMyLocation(Marker m) {
        return m.getTitle() == null && m.getSnippet() == null;
    }

    private Marker findByGeoAndData(ISearchItem item) {
        synchronized (syncMarkers) {
            for (Marker marker : mapboxMap.getMarkers()) {
                if (item.isEquals(marker)) {
                    return marker;
                }
////                final LatLng pos = new LatLng(item.getLat(), item.getLon());
//                if (item.getLatLng().equals(marker.getPosition())) {
//                    if(equalsMarkerString(marker.getTitle(), item.getTitle())) {
//                        if (item.isGroup()) {
//                            if (equalsMarkerString(marker.getSnippet(), item.getSubtitle())) {
//                                return marker;
//                            }
//                        } else {
//                            return marker;
//                        }
//                    }
//                }
            }
        }
        return null;
    }

//    private boolean equalsMarkerString(CharSequence a, CharSequence b) {
//        if (a == null && b == null) {
//            return true;
//        }
//        return TextUtils.equals(a, b);
//    }

    private void selectMarker(Marker activeMarker) {
        if (activeMarker != shownInfoWindowMarker) {
            if (shownInfoWindowMarker != null) {
//            shownInfoWindowMarker.setIcon(getIconOff(getContext(), isGroupMarker(shownInfoWindowMarker)));
                final String id = getId(shownInfoWindowMarker);
                shownInfoWindowMarker.setIcon(getIconOff(getContext(), groupMarkers.get(id)));
                shownInfoWindowMarker.hideInfoWindow();
            }

            if (activeMarker != null) {
//            activeMarker.setIcon(getIconOn(getContext(), isGroupMarker(activeMarker)));
                final String id = getId(activeMarker);
                activeMarker.setIcon(getIconOn(getContext(), groupMarkers.get(id)));
                mapboxMap.selectMarker(activeMarker);
                shownInfoWindowMarker = activeMarker;

                zoomIfMarkerNotVisible(activeMarker);
            } else {
                if (BuildConfig.DEBUG) {
                    Toast.makeText(getContext(), "marker is null", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void zoomIfMarkerNotVisible(Marker selectedMarker) {
//        List<MarkerView> markerViewsInRect = mapboxMap.getMarkerViewsInRect(new RectF(0, 0, binding.mapview2.getRight(), binding.mapview2.getBottom()));
//        Timber.d(markerViewsInRect.toString());

        Marker currentPositionMarker = null;
        List<Marker> markersInRect = MapboxHelper.getMarkersInRect(mapboxMap,
                new RectF(0, 0, binding.mapview2.getRight(), binding.mapview2.getBottom() - binding.mapview2.getTop() - (int) getResources().getDimension(R.dimen.around_me_card_height)));
        boolean zoomNeeded = true;
        for (Marker marker : markersInRect) {
            if (marker.getId() == selectedMarker.getId()) {
                zoomNeeded = false;
                break;
            }


        }

        for (Marker marker : mapboxMap.getMarkers()) {
            if (isMyLocation(marker)) {
                currentPositionMarker = marker;
            }
        }

        if (zoomNeeded) {
            if (currentPositionMarker != null) {
                List<Marker> markers = new ArrayList<>(2);
                markers.add(currentPositionMarker);
                markers.add(selectedMarker);
                moveMapToGeoPoint(markers, true);
            } else {
                mapboxMap.setCameraPosition(
                        new CameraPosition.Builder()
                                .target(selectedMarker.getPosition())
                                .build());

            }
        }
    }

    private void addDeferredAction(DeferredAction action) {
        if (mapboxMap == null) {
            deferredActions.add(action);
        } else {
            action.doAction();
        }
    }

    private void doDeferredActions() {
        for (DeferredAction action : deferredActions) {
            if (action != null) {
                action.doAction();
            }
        }

        deferredActions.clear();
    }

    public interface DeferredAction {
        void doAction();
    }

    // Custom marker view used for pulsing the background view of marker.
    private static class PulseMarkerViewAdapter extends MapboxMap.MarkerViewAdapter<PulseMarkerView> {

        private LayoutInflater inflater;

        public PulseMarkerViewAdapter(@NonNull Context context) {
            super(context, PulseMarkerView.class);
            this.inflater = LayoutInflater.from(context);
        }

        @Nullable
        @Override
        public View getView(@NonNull PulseMarkerView marker, @Nullable View convertView, @NonNull ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.view_pulse_marker, parent, false);
                viewHolder.foregroundImageView = (ImageView) convertView.findViewById(R.id.foreground_imageView);
                viewHolder.backgroundImageView = (ImageView) convertView.findViewById(R.id.background_imageview);
                convertView.setTag(viewHolder);
            }
            return convertView;
        }

        private static class ViewHolder {
            ImageView foregroundImageView;
            ImageView backgroundImageView;
        }
    }

    private void animateMarker(MarkerView marker) {

        View view = mapboxMap.getMarkerViewManager().getView(marker);
        if (view != null) {
            View backgroundView = view.findViewById(R.id.background_imageview);

            ValueAnimator scaleCircleX = ObjectAnimator.ofFloat(backgroundView, "scaleX", 5f);
            ValueAnimator scaleCircleY = ObjectAnimator.ofFloat(backgroundView, "scaleY", 5f);
            ObjectAnimator fadeOut = ObjectAnimator.ofFloat(backgroundView, "alpha", 1f, 0f);

            scaleCircleX.setRepeatCount(ValueAnimator.INFINITE);
            scaleCircleY.setRepeatCount(ValueAnimator.INFINITE);
            fadeOut.setRepeatCount(ObjectAnimator.INFINITE);

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.play(scaleCircleX).with(scaleCircleY).with(fadeOut);
            animatorSet.setDuration(3000);
            animatorSet.start();
        }
    }

    @NonNull
    private static Icon getIconOff(Context context, ISearchItem searchItem) {
        return searchItem != null && searchItem.isGroup() ? IconFactory.getInstance(context).fromBitmap(
                Utils.drawTextToBitmap(context,
                        Utils.convertDrawableResToBitmap(context, R.drawable.bg_cycle_very_dark, R.dimen.group_marker_size, R.dimen.group_marker_size),
                        searchItem.getGroupCount())) :
                IconFactory.getInstance(context).fromResource(R.drawable._ic_localisation_off2);
    }

    @NonNull
    private static Icon getIconOn(Context context, ISearchItem searchItem) {
        return searchItem != null && searchItem.isGroup() ? IconFactory.getInstance(context).fromBitmap(
                Utils.drawTextToBitmap(context,
                        Utils.convertDrawableResToBitmap(context, R.drawable.bg_cycle_green, R.dimen.group_marker_size, R.dimen.group_marker_size),
                        searchItem.getGroupCount())) :
                IconFactory.getInstance(context).fromResource(R.drawable._ic_localisation_on2);
    }

    public ISearchItem getGroupItemInPosition(int absPosition) {
        /*final SearchItemHuge*/
        setLastItem(presenter.getItemInPosition(absPosition));
        final ISearchItem lastItem3 = getLastItem();
        // TODO need to set position on screen
        if (lastItem3 != null) {
            synchronized (groupMarkers) {
                for (String s : groupMarkers.keySet()) {
                    if (lastItem3.getAddress().equals(s)) {
                        final SearchItemGroup searchItemGroup = groupMarkers.get(s);
                        final int relativePosition = absPosition - searchItemGroup.getOffsetPosition();
//                    searchItemGroup.setActive(relativePosition);
                        boolean isActive = false;
                        setActiveIdForClusterMarker(lastItem3.getId());
                        if (adapterGroupMarker != null) {
                            isActive = adapterGroupMarker.setActiveId(lastItem3.getId());
                        }
                        scrollToGroupMarker(relativePosition);
                        return searchItemGroup;
                    }
                }
            }
        }
        return lastItem3;
    }

    private void scrollToGroupMarker(int relativePosition) {
        if (recyclerViewGroupMarker != null) {
            final int itemCount = recyclerViewGroupMarker.getAdapter().getItemCount();
            Timber.i("scrollto: " + String.valueOf(relativePosition) + "/" + String.valueOf(itemCount));
            if (recyclerViewGroupMarker != null) {
                if (relativePosition >= itemCount) {
                    relativePosition = itemCount - 1;
                }
                recyclerViewGroupMarker.scrollToPosition(relativePosition);
            }
        }
    }

    private void scrollToAdvert(int position) {
//        binding.advertRecycleview.scrollToPosition(position);
//        layoutManager.scrollToPosition(position);
        layoutManager.scrollToPositionWithOffset(position, 0);
    }

    private SearchItemHuge remakeGroupMarkers(Cursor cursor, ISearchItem lastItem) {
        SearchItemHuge firstItem = null;
        synchronized (groupMarkers) {
            groupMarkers.clear();
            if (cursor.moveToFirst()) {
                do {
                    final int position = cursor.getPosition();
                    final SearchItemHuge item = new SearchItemHuge(position);
                    if (firstItem == null) {
                        firstItem = item;
                    }
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, item);
                    if (lastItem instanceof SearchItemHuge && item.isEquals((SearchItemHuge) lastItem)) {
                        firstItem = (SearchItemHuge) lastItem;
                    }
                    final String address = item.getAddress();
                    if (address != null) {
                        if (!groupMarkers.containsKey(address)) {
                            groupMarkers.put(address, new SearchItemGroup(position, item));
                        } else {
                            groupMarkers.get(address).add(item);
                        }
                    }

                } while (cursor.moveToNext());
            }
        }
        return firstItem;
    }

    public long getActiveIdForClusterMarker() {
        return activeIdForClusterMarker;
    }

    public void setActiveIdForClusterMarker(long activeId) {
        this.activeIdForClusterMarker = activeId;
    }

    private boolean isGroupMarker(Marker activeMarker) {
//        return SearchItemGroup.GROUP.equals(activeMarker.getSnippet());
        final String snippet = activeMarker.getSnippet();
        return snippet != null && snippet.startsWith(GROUP_MARKER);
    }

    private String getId(Marker marker) {
        final String snippet = marker.getSnippet();
        if (snippet != null && snippet.startsWith(GROUP_MARKER)) {
            return snippet.substring(GROUP_MARKER.length());
        }
        return snippet;
    }

    private boolean onClickMarker(Marker marker) {
        if (!isMyLocation(marker)) {
            final LatLng latLng = marker.getPosition();
            final SearchItemHuge itemHuge = presenter.getPositionByGeo(latLng.getLongitude(), latLng.getLatitude());
            if (isGroupMarker(marker)) {
                setLastItem(null);
                selectMarker(marker);
                final SearchItemGroup searchItemGroup = groupMarkers.get(getId(marker));
                if (searchItemGroup != null) {
                    scrollToAdvert(searchItemGroup.getItem().getPosition());
                    setLastItem(searchItemGroup.getItems().get(0));
                }
            } else {
                setLastItem(itemHuge);
            }
            if (itemHuge != null) {
                if (getActiveIdForClusterMarker() != itemHuge.getId()) {
                    setActiveIdForClusterMarker(itemHuge.getId());
//                    if (adapterGroupMarker != null) {
//                        adapterGroupMarker.notifyDataSetChanged();
//                    }
                }
                final int position = itemHuge.getPosition();
                if (position >= 0) {
                    selectMarker(marker);
                    scrollToAdvert(position);
                }
            } else {
                if (getActiveIdForClusterMarker() != 0) {
                    setActiveIdForClusterMarker(0);
//                    if (adapterGroupMarker != null) {
//                        adapterGroupMarker.notifyDataSetChanged();
//                    }
                }
            }

            return false;
        }
        return true;
    }

    private void setLastItem(ISearchItem item) {
        if (item != null) {
            Timber.i("lastitem: " + item.getTitle());
            final long id = item.getId();
            setActiveIdForClusterMarker(id);
            if (adapterGroupMarker != null) {
                adapterGroupMarker.setActiveId(id);
            }
        }
        lastItem2 = item;
    }

    public ISearchItem getLastItem() {
//        if (layoutManager != null) {
//            final int position = layoutManager.findFirstVisibleItemPosition();
//            if (position == -1) {
//                return null;
//            }
//            return presenter.getItemInPosition(position);
//        }
        return lastItem2;
    }

    private void setShouldSelectMarker() {
        shouldSelectMarker = true;
    }

    @Override
    public void invalidateMap() {
        binding.mapview2.invalidate();
    }

}
