package com.adyax.srisgp.mvp.history;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.CustomTabLayout;
import com.adyax.srisgp.presentation.CustomViewPager;
import com.adyax.srisgp.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class YourHistoryFragment extends ViewFragment implements YourHistoryPresenter.YourHistoryView,
        HistoriesFragment.HistoryListener {

    public static final int SEARCH_PAGE = 2;

    @Inject
    YourHistoryPresenter historyPresenter;

    @BindView(R.id.tabLayout)
    CustomTabLayout tabLayout;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.btnRemove)
    Button btnRemove;

    private boolean editMode = false;
    private boolean emptyState = false;

    private boolean[] emptyStates = new boolean[3];

    private HistoriesPagerAdapter pagerAdapter;
    private Set<HistoryKey> selectedItems = new HashSet<>();
    private Set<HistoryKey> loadingItems = new HashSet<>();

    private ActionMode actionMode;

    public static YourHistoryFragment newInstance() {
        new TrackerHelper().addScreenView(TrackerLevel.HISTORICAL, TrackerAT.MON_HISTORIQUE);
        Bundle args = new Bundle();
        YourHistoryFragment fragment = new YourHistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_your_history, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    private void bindViews(View view) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.history_your_history);

        pager.setPagingEnabled(false);
        pagerAdapter = new HistoriesPagerAdapter(getChildFragmentManager(), getContext());
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int currentPage;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
                if (actionMode != null) {
                    editMode = false;
                    actionMode.finish();
                    selectedItems.clear();
                    ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).updateAll();
                }

                HistoriesFragment previousFragment = (HistoriesFragment) pagerAdapter.getRegisteredFragment(currentPage);
                if (previousFragment != null) {
                    previousFragment.onPageChanged();
                }

                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setSwipeListener(new CustomTabLayout.SwipeListener() {
            @Override
            public void onLeft() {
                if (!emptyState) {
                    int prevItem = pager.getCurrentItem() - 1;
                    if (prevItem >= 0) {
                        pager.setCurrentItem(prevItem);
                    }
                }
            }

            @Override
            public void onRight() {
                if (!emptyState) {
                    int nextItem = pager.getCurrentItem() + 1;
                    if (nextItem >= 0) {
                        pager.setCurrentItem(nextItem);
                    }
                }
            }
        });

        ViewUtils.disableTabLayout(getContext(), tabLayout);
        tabLayout.setupWithViewPager(pager);
        createTabIcons();

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectedItems.isEmpty()) {
                    for (HistoryKey key : selectedItems) {
                        loadingItems.add(key);
                        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).addLoadingItem(key);
                    }
                    HistoryType historyType = ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).getHistoryType();
                    historyPresenter.onHistoryRemove(new ArrayList<>(selectedItems), historyType);
                }
            }
        });
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabOne.setText(pagerAdapter.getPageTitle(0).toString().toUpperCase());
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabTwo.setText(pagerAdapter.getPageTitle(1).toString().toUpperCase());
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabThree.setText(pagerAdapter.getPageTitle(SEARCH_PAGE).toString().toUpperCase());
        tabLayout.getTabAt(SEARCH_PAGE).setCustomView(tabThree);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                editMode = true;
                ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).changeEditMode();

                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new ActionMode.Callback() {
                    @Override
                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        btnRemove.setVisibility(View.VISIBLE);
                        MenuInflater inflater = mode.getMenuInflater();
                        inflater.inflate(R.menu.select_all, menu);
                        return true;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        menu.clear();
                        MenuInflater inflater = mode.getMenuInflater();
                        boolean selectedAll = ((HistoriesFragment) pagerAdapter
                                .getRegisteredFragment(pager.getCurrentItem())).getItems().size() == selectedItems.size();
                        if (selectedAll) {
                            inflater.inflate(R.menu.unselect_all, menu);
                        } else {
                            inflater.inflate(R.menu.select_all, menu);
                        }
                        return true;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_select_all:
//                                new TrackerHelper().clickSearch(ClickType.clic_recherche_suppression_tout_historique);
                                List<HistoryItem> items = ((HistoriesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).getItems();
                                for (HistoryItem history : items) {
                                    selectedItems.add(new HistoryKey(history.sid, history.nid));
                                }
                                ((HistoriesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).updateAll();
                                mode.invalidate();
                                return true;
                            case R.id.action_unselect_all:
                                selectedItems.clear();
                                ((HistoriesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).updateAll();
                                mode.invalidate();
                                return true;
                        }
                        return false;
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode mode) {
                        btnRemove.setVisibility(View.GONE);

                        editMode = false;
                        selectedItems.clear();
                        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).changeEditMode();
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        HistoriesFragment currentFragment = (HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment != null && currentFragment.getItems().size() > 0) {
            inflater.inflate(R.menu.remove, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRemoved(HistoryKey id) {
        selectedItems.remove(id);
        loadingItems.remove(id);
        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(0)).removeHistory(id);
        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(1)).removeHistory(id);
        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(SEARCH_PAGE)).removeHistory(id);
    }

    @Override
    public void onRemove(HistoryItem item) {
        HistoryKey key = new HistoryKey(item.sid, item.nid);
        loadingItems.add(key);
        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).addLoadingItem(key);

        List<HistoryKey> items = new ArrayList<>(1);
        items.add(new HistoryKey(item.sid, item.nid));
        historyPresenter.onHistoryRemove(items, ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).getHistoryType());
    }

    @Override
    public void onItemCountChanged(HistoriesFragment fragment, int count) {
        Fragment currentFragment = pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment == fragment) {
            invalidateOptionsMenu();
            if (editMode && actionMode != null) {
                actionMode.invalidate();
            }
        }
    }

    private void invalidateOptionsMenu() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().invalidateOptionsMenu();
    }

    @Override
    public void onChangeState(HistoryType type, boolean empty) {
        if (empty) {
            if (type == HistoryType.info) {
                emptyStates[0] = true;
            } else if (type == HistoryType.find) {
                emptyStates[1] = true;
            } else if (type == HistoryType.search) {
                emptyStates[SEARCH_PAGE] = true;
            }

            boolean allTabsEmpty = true;

            for (int i = 0; i < emptyStates.length; i++) {
                if (!emptyStates[i]) {
                    allTabsEmpty = false;
                    break;
                }
            }

            if (allTabsEmpty) {
                emptyState = true;
                pager.setCurrentItem(0);
                ViewUtils.disableTabLayout(getContext(), tabLayout);
            }
        } else {
            emptyState = false;
            ViewUtils.enableTabLayout(getContext(), tabLayout);
        }
    }

    @Override
    public void onAllItemsRemoved() {
        if (actionMode != null) {
            editMode = false;
            actionMode.finish();
            invalidateOptionsMenu();
        }
    }

    @Override
    public void onCloseEditMode() {
        if (actionMode != null) {
            editMode = false;
            actionMode.finish();
            invalidateOptionsMenu();
        }
    }

    @Override
    public void onSuccessRemote() {
        try {
            final int pageNumber = pager.getCurrentItem();
            if(pageNumber== SEARCH_PAGE) {
                List<HistoryItem> items = ((HistoriesFragment) pagerAdapter
                        .getRegisteredFragment(pageNumber)).getItems();
                if (selectedItems.size() == items.size()) {
                    new TrackerHelper().clickSearch(ClickType.clic_recherche_suppression_tout_historique);
                }
            }
        }catch (Exception e){

        }
    }

    @Override
    public boolean isEditMode() {
        return editMode;
    }

    @Override
    public boolean isLoadingItem(HistoryItem item) {
        return loadingItems.contains(new HistoryKey(item.sid, item.nid));
    }

    @Override
    public boolean isSelectedItem(HistoryItem item) {
        return selectedItems.contains(new HistoryKey(item.sid, item.nid));
    }

    @Override
    public void changeSelection(HistoryItem item) {
        if (selectedItems.contains(new HistoryKey(item.sid, item.nid))) {
            selectedItems.remove(new HistoryKey(item.sid, item.nid));
        } else {
            selectedItems.add(new HistoryKey(item.sid, item.nid));
        }
        ((HistoriesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).changeSelectionMode(item);
    }

    @NonNull
    @Override
    public IPresenter<YourHistoryPresenter.YourHistoryView> getPresenter() {
        return historyPresenter;
    }
}
