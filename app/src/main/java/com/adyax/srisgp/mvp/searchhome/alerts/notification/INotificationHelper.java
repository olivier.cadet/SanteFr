package com.adyax.srisgp.mvp.searchhome.alerts.notification;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by SUVOROV on 11/1/16.
 */

public interface INotificationHelper {
    void send(AlertItem alertItem);
    void send(RemoteMessage remoteMessage);
}
