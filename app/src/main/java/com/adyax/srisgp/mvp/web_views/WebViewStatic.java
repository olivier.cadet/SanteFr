package com.adyax.srisgp.mvp.web_views;

import androidx.appcompat.app.AppCompatActivity;
import com.adyax.srisgp.R;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class MyWebViewClient extends WebViewClient {

}

public class WebViewStatic extends AppCompatActivity {

    private WebViewArg webViewArg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_static);

        Intent intent = getIntent();
        this.webViewArg = intent.getParcelableExtra(WebViewArg.CONTENT_URL);

        WebView wv = (WebView) findViewById(R.id.webview);
        wv.setVerticalScrollBarEnabled(true);

        wv.setWebViewClient(new MyWebViewClient());
        wv.setWebChromeClient(new DefaultChromeClient());

        WebSettings wvSettings = wv.getSettings();
        wvSettings.setJavaScriptEnabled(true);
        wvSettings.setJavaScriptCanOpenWindowsAutomatically(true);

        if (this.webViewArg != null) {
            wv.loadUrl(this.webViewArg.getContentUrl());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Close entire App
        finishAffinity();
    }
}