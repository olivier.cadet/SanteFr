package com.adyax.srisgp.mvp.around_me;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.mvp.common.MessageHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 3/16/17.
 */

public class IdfNotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_MESSAGE = 0;

    private List<SearchMessage> messages = Collections.emptyList();
    private MessageHolder.MessageListener messageListener;

    public void setMessageListener(MessageHolder.MessageListener listener) {
        this.messageListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_MESSAGE:
                View messageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_message, parent, false);
                return new MessageHolder(messageView);
            default:
                throw new RuntimeException("No such view type: " + viewType);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_MESSAGE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MessageHolder) {
            ((MessageHolder) holder).bind(messages.get(position), messageListener);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void showMessages(List<SearchMessage> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public boolean isAroundMeRegionMessage() {
        if(messages!=null){
            for(SearchMessage msg:messages){
                if(msg.isAroundMeRegionMessage()){
                    return true;
                }
            }
        }
        return false;
    }

}
