package com.adyax.srisgp.mvp.around_me;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by SUVOROV on 8/30/17.
 */


import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;

public class MarkerGroupItemAdapter extends RecyclerView.Adapter<MarkerGroupItemAdapter.MyViewHolder> {

    private SearchItemGroup searchItemGroup;
    private OnItemClickListener listener;
    private long activeId;

    public interface OnItemClickListener {
        void onItemClick(SearchItemHuge item, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ImageView intoWindowAvatar;
        private final TextView infoWindowTitle;
//        private final ImageView infoWindowStatus;

        public MyViewHolder(View view) {
            super(view);
            intoWindowAvatar = (ImageView) view.findViewById(R.id.intoWindowAvatar);
            infoWindowTitle = (TextView) view.findViewById(R.id.infoWindowTitle);
//            infoWindowStatus = (ImageView) view.findViewById(R.id.infoWindowStatus);
        }
    }


    public MarkerGroupItemAdapter(long activeId, SearchItemGroup searchItemGroup, OnItemClickListener listener) {
        this.searchItemGroup = searchItemGroup;
        final boolean exists = setActiveId(activeId);
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SearchItemHuge item = searchItemGroup.getItems().get(position);
        holder.infoWindowTitle.setText(item.getTitle());
        holder.intoWindowAvatar.setImageResource(item.getIcon().getResourceId());
        holder.itemView.setOnClickListener(view -> {
            listener.onItemClick(item, position);
            notifyDataSetChanged();
        });
//        holder.infoWindowStatus.setVisibility(item.getId() == activeId? View.VISIBLE:View.INVISIBLE);
        holder.itemView.setBackgroundColor(holder.itemView.getResources().getColor(
                item.getId() == activeId ? R.color.select_line_bg : android.R.color.transparent));
    }

    @Override
    public int getItemCount() {
        return searchItemGroup != null ? searchItemGroup.getItems().size() : 0;
    }

    public boolean setActiveId(long activeId) {
        this.activeId = activeId;
        return searchItemGroup.exists(activeId);
    }

}