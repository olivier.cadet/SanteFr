package com.adyax.srisgp.mvp.around_me.filters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.*;
import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.search.FiltersActivity;

import javax.inject.Inject;

/**
 * Created by Kirill on 19.10.16.
 */

public class FiltersFragment extends ViewFragment implements FiltersPresenter.FiltersView, View.OnClickListener {

    private static final String SEARCH_TYPE_KEY = "search_type";

    @Inject
    FiltersPresenter presenter;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private View applyFiltersView;

    public static FiltersFragment getInstance(SearchType searchType) {
        Bundle bundle = new Bundle();
        bundle.putInt(SEARCH_TYPE_KEY, searchType == null ? -1 : searchType.ordinal());
        FiltersFragment fragment = new FiltersFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filters, container, false);
        initViews(view);
        initActionBar();
        return view;
    }

    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setAdapter(presenter.getFiltersAdapter());

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new BottomTopOffsetDecoration(
                getResources().getDimensionPixelSize(R.dimen.default_margin),
                getResources().getDimensionPixelSize(R.dimen.default_margin_x2)
        ));

        applyFiltersView = view.findViewById(R.id.applyFilters);
        applyFiltersView.setOnClickListener(this);

        if (isAutomaticSearch()) {
            applyFiltersView.setVisibility(View.INVISIBLE);
        }

    }

    private void initActionBar() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.filters_title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.applyFilters:
                presenter.applyFilters();
                break;
        }
    }

    @Override
    public void search() {
        if (getActivity() instanceof FiltersActivity) {
            getActivity().setResult(Activity.RESULT_OK);
        }
//        getActivity().onBackPressed();
        getActivity().finish();
    }

    @Override
    public void onFiltersChanged(boolean launchResearch) {
        if (isAutomaticSearch() && launchResearch) {
            presenter.applyFilters();
        } else {
            if (applyFiltersView != null) {
                applyFiltersView.setEnabled(presenter.isFilterChanged());
            }
        }
    }

    @Override
    public SearchType getSearchType() {
        int searchTypeOrdinal = getArguments().getInt(SEARCH_TYPE_KEY);
        return searchTypeOrdinal == -1 ? null : SearchType.values()[searchTypeOrdinal];
    }

    private boolean isAutomaticSearch() {
        return getSearchType() != null ? getSearchType().searchAutomatic() : false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filters, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reset:
//                final int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
//                final int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
//                for (int i = firstVisibleItemPosition; i <= lastVisibleItemPosition; ++i) {
//                    FiltersAdapter.FiltersViewHolder holder = (FiltersAdapter.FiltersViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
//                    if (holder != null) {
//                        holder.resetValues();
//                    }
//                }
                presenter.resetFilters();
                break;
        }

        return true;
    }

    static class BottomTopOffsetDecoration extends RecyclerView.ItemDecoration {
        private int mBottomOffset;
        private int mTopOffset;

        public BottomTopOffsetDecoration(int topOffset, int bottomOffset) {
            mTopOffset = topOffset;
            mBottomOffset = bottomOffset;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            int dataSize = state.getItemCount();
            int position = parent.getChildLayoutPosition(view);

            int top = 0;
            int bottom = 0;

            if (dataSize > 0) {
                if (position == 0) {
                    top = mTopOffset;
                }

                if (position == dataSize - 1) {
                    bottom = mBottomOffset;
                }
            }

            outRect.set(0, top, 0, bottom);
        }
    }
}
