package com.adyax.srisgp.mvp.feedback;

import android.os.Bundle;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.FeedbackFormValue;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.SubmitFeedbackFormCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class FeedbackPresenter extends PresenterAppReceiver<FeedbackPresenter.FeedbackView> {

    private INetWorkState netWorkState;
    private String url;
    private String reason;
    private String formName;

    private FeedbackFormResponse feedbackFormResponse;
    private long startTime;

    @Inject
    public FeedbackPresenter(INetWorkState netWorkState) {
        this.netWorkState = netWorkState;
    }

    public void onSendFeedbackClicked(String url, String reason, String helplessReasonFieldName) {
        this.url = url;
        this.reason = reason;
        getView().showLoading();
        submitForm(helplessReasonFieldName, feedbackFormResponse.secret, startTime);
    }

    public void getForm(String formName) {
        this.formName = formName;
        if (getView() != null) {
            getView().showLoading();
            GetFormCommand getFormCommand = new GetFormCommand(formName, null);
            ExecutionService.sendCommand(getContext(), getAppReceiver(), getFormCommand, ExecutionService.GET_FORM);
        }
    }

    public void onCheckedStateChanged() {
        if (getView() != null) {
            if (getView().hasCheckedValues()) {
                getView().enableButton();
            } else {
                getView().disableButton();
            }
        }
    }

    private void submitForm(String helplessReasonFieldName, String secret, long startTime) {
        boolean loggedIn = netWorkState.isLoggedIn();
        if (getView() != null) {
            List<String> reasons = getView().getCheckedValues();

            FeedbackFormValue formValue = new FeedbackFormValue(helplessReasonFieldName, "no", !reasons.isEmpty() ? reasons : null, url, reason);
            SubmitFeedbackFormCommand command = new SubmitFeedbackFormCommand(loggedIn, formName, secret, formValue, startTime);
            ExecutionService.sendCommand(getContext(), getAppReceiver(), command, ExecutionService.SUBMIT_FORM);
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (getView() != null) {
            getView().hideLoading();
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {
        super.onMessage(requestCode, data);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.GET_FORM:
                feedbackFormResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                startTime =data.getLong(CommandExecutor.START_TIME);
                if (getView() != null) {
                    getView().render(feedbackFormResponse);
                    getView().hideLoading();
                }
                break;
            case ExecutionService.SUBMIT_FORM:
                if (getView() != null) {
                    getView().hideLoading();
                    getView().back();
                }
                break;
        }
    }

    public interface FeedbackView extends IView {
        void showLoading();

        void hideLoading();

        void back();

        void render(FeedbackFormResponse response);

        boolean hasCheckedValues();

        List<String> getCheckedValues();

        void enableButton();

        void disableButton();
    }
}
