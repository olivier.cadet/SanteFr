package com.adyax.srisgp.mvp.around_me.get_location;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.databinding.FragmentGetLocationAroundMeBinding;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.PermissionUtils;
import com.google.android.gms.common.api.Status;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/31/16.
 */

public class GetLocationAroundMeFragment extends ViewFragment implements GetLocationAroundMePresenter.GetLocationAroundMeView {

    private static final int REQUEST_LOCATION_PERMISSION = 1;

    private static final int REQUEST_LOCATION = 123;

    @Inject
    GetLocationAroundMePresenter presenter;
    private GetLocationAroundMeListener listener;

    private FragmentGetLocationAroundMeBinding binding;

    public static GetLocationAroundMeFragment newInstance() {
        GetLocationAroundMeFragment fragment = new GetLocationAroundMeFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (GetLocationAroundMeListener) context;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOCATION) {
            if (resultCode == Activity.RESULT_OK) {
                openAroundMe(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_get_location_around_me, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
//        requestPermissions();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private void bindViews() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.search_around_me);
        binding.btnProvideLocation.setOnClickListener(v -> {
            if (PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                presenter.onRequestCurrentLocation(true);
            } else {
                PermissionUtils.requestLocationPermissions(this, REQUEST_LOCATION_PERMISSION);
            }
        });

        View.OnClickListener searchBarClickListener = v -> {
            Intent intent = GetCityActivity.newIntent(getContext());
            startActivityForResult(intent, REQUEST_LOCATION);
            getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        };

        binding.etSearch.setOnClickListener(searchBarClickListener);
        binding.llSearchBar.setOnClickListener(searchBarClickListener);

        binding.ivClose.setOnClickListener(v -> {
//            listener.onCloseGetLocationAroundMeFragment();
            presenter.startAroundMe(new AroundMeArguments(SearchType.around, ""));
        });
    }

    @Override
    public void setCurrentLocation(Location location) {
        presenter.startAroundMe(new AroundMeArguments(SearchType.around, location));
    }

    @Override
    public void onEnableLocationButtonStateChanged() {

    }

    private void openAroundMe(Intent intent) {
        final GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation = intent.getParcelableExtra(GetCityFragment.INTENT_STATUS_LOCATION);
        final String city = statusLocation.getLocation();
        if (city == null) {
            double longitude = intent.getDoubleExtra(GetCityFragment.INTENT_LONGITUDE, 0);
            double latitude = intent.getDoubleExtra(GetCityFragment.INTENT_LATITUDE, 0);
            presenter.startAroundMe(new AroundMeArguments(SearchType.around, longitude, latitude));
        } else {
            presenter.startAroundMe(new AroundMeArguments(SearchType.around, statusLocation));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.onRequestCurrentLocation(true);
                } else {
                    if (PermissionUtils.isNotAskAgainLocationPermission(getActivity())) {
                        showLocationPermissionNotAskAgain();
                    } else {
                        showLocationPermissionDenied();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void onPromptSettingsResult(boolean success) {
        if (success) {
            presenter.onPromptSettingSuccess(true);
        } else {
            showLocationNotEnabled();
        }
    }

    @Override
    public void showEnableLocationDialog(Status status) {
        listener.onPromptLocation(status);
    }

    @Override
    public void updateLocation(Location location) {

    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return presenter;
    }

    public interface GetLocationAroundMeListener extends ProvideLocationFragment.OnPromptLocationSettingsListener {
        void onCloseGetLocationAroundMeFragment();
    }
}
