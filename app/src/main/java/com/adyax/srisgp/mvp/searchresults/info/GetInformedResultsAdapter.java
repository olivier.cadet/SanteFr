package com.adyax.srisgp.mvp.searchresults.info;

import android.graphics.Typeface;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.response.SearchItemType;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.searchresults.SearchResultsAdapter;
import com.adyax.srisgp.mvp.searchresults.SearchResultsPresenter;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;
import com.adyax.srisgp.utils.Utils;
import com.bumptech.glide.Glide;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/26/16.
 */

public class GetInformedResultsAdapter extends SearchResultsAdapter {
    private InfoCardListener cardListener;

    public GetInformedResultsAdapter(ResultsAdapterListener resultsListener, InfoCardListener infoCardListener, IRepository repository) {
        super(resultsListener, repository);
        this.cardListener = infoCardListener;
    }

    @Override
    public int getActivityLocationID() {
        return WidgetViewHelper.LOCATION_INFO;
    }

    @Override
    protected RecyclerView.ViewHolder createCardViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_VIDEO:
                return new VideoCardHolder(inflater.inflate(R.layout.item_get_informed_card_video, parent, false));
            default:
                return new InfoCardHolder(inflater.inflate(R.layout.item_get_informed_card, parent, false));
        }
    }

    @Override
    protected void onBindCardViewHolder(RecyclerView.ViewHolder holder, SearchItemHuge item) {
        ((CardHolder) holder).bind(item);
    }

    @Override
    protected String getHeaderTitle() {
        return App.getAppContext().getString(R.string.search_results_sorted_header);
    }

    class InfoCardHolder extends RecyclerView.ViewHolder implements CardHolder {
        @BindView(R.id.card_view_get_informed)
        CardView card_view_get_informed;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;
        @BindView(R.id.tvPropose)
        TextView tvPropose;
        @BindView(R.id.ivFavorite)
        ImageView ivFavorite;
        @BindView(R.id.ivAction)
        ImageView ivAction;
        @BindView(R.id.tvAction)
        TextView tvAction;
        @BindView(R.id.llRead)
        LinearLayout llRead;
        @BindView(R.id.tvLink)
        TextView tvLink;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.maintenanceTv)
        TextView maintenanceTv;

        // Tag medicament:
        @BindView(R.id.llTag)
        LinearLayout llTag;

        InfoCardHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(SearchItemHuge itemHuge) {
            tvContent.setText(itemHuge.getDescription());
            ivFavorite.setSelected(itemHuge.isFavorite());
            ivFavorite.setOnClickListener(view -> {
                if (cardListener != null) {
                    cardListener.onFavorite(itemHuge);
                }
            });
            llTag.setVisibility(View.GONE); // Init visibility, due display bug after multiple search (acide, grippe).

            if (itemHuge.type != null && itemHuge.type == SearchItemType.ACTUALITE) {
                String actuality = App.getAppContext().getString(R.string.search_results_actuality);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(actuality + " " + itemHuge.getTitle());
                int greyColor = App.getAppContext().getResources().getColor(R.color.textGray);
                spannableProposed.setSpan(new ForegroundColorSpan(greyColor), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableProposed.setSpan(new StyleSpan(Typeface.BOLD), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvTitle.setText(spannableProposed);
                tvDate.setText(tvDate.getContext().getString(R.string.search_get_informed_published, itemHuge.date));
                tvDate.setVisibility(View.VISIBLE);
            } else {
                tvTitle.setText(itemHuge.getTitle());
                tvDate.setVisibility(View.GONE);
            }

            if (itemHuge.source != null && !itemHuge.source.isEmpty()) {
                String proposedString = App.getAppContext().getString(R.string.search_home_proposed_by);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(proposedString + " " + itemHuge.source.toUpperCase());
                spannableProposed.setSpan(
                        new StyleSpan(Typeface.BOLD), proposedString.length(), spannableProposed.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvPropose.setText(spannableProposed);
                tvPropose.setVisibility(View.VISIBLE);
                if (itemHuge.sourceUrl != null && itemHuge.sourceUrl.isEmpty() == false) {
                    tvPropose.setOnClickListener(v -> {
                        cardListener.onUrlClicked(itemHuge.sourceUrl);
                    });
                }
            } else {
                tvPropose.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(itemHuge.link)) {
                String link;
                try {
                    link = Utils.getDomainName(itemHuge.link).concat("/..");
                    tvLink.setText(App.getAppContext().getString(R.string.search_results_find, link));
                    tvLink.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    tvLink.setVisibility(View.VISIBLE);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    tvPropose.setVisibility(View.GONE);
                    tvLink.setOnClickListener(null);
                }
            } else {
                tvLink.setVisibility(View.GONE);
                tvLink.setOnClickListener(null);
            }


            llRead.setOnClickListener(null);
            card_view_get_informed.setOnClickListener(null);

            if (itemHuge.isContentMaintenanceMode()) {
                maintenanceTv.setVisibility(View.VISIBLE);
                maintenanceTv.setText(repository.getConfig().getContentMaintenance().getTeaserActionText());
                ivFavorite.setVisibility(View.GONE);
                llRead.setVisibility(View.GONE);
                if (BuildConfig.DEBUG) {
                    card_view_get_informed.setBackgroundColor(card_view_get_informed.getContext().getResources().getColor(R.color.bt_light_green));
                }
            } else {
                maintenanceTv.setVisibility(View.GONE);
                ivFavorite.setVisibility(View.VISIBLE);
                llRead.setVisibility(View.VISIBLE);
                if (BuildConfig.DEBUG) {
                    card_view_get_informed.setBackgroundColor(card_view_get_informed.getContext().getResources().getColor(R.color.material_white));
                }
            }

            String providerStr;
            SpannableStringBuilder spannableProviderStr;

            switch (itemHuge.getNodeType()) {
                case LIENS_EXTERNES:
                    ivAction.setImageResource(R.drawable.ressource_distante);
                    tvAction.setText(R.string.view);
                    llRead.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    card_view_get_informed.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    break;
                case MEDICAMENTS:
                    llTag.setVisibility(View.VISIBLE);

                    tvTitle.setText(itemHuge.getTitle());
                    tvContent.setText(R.string.item_desc_medicaments);

                    providerStr = App.getAppContext().getString(R.string.search_home_find_by);

                    spannableProviderStr = new SpannableStringBuilder(providerStr
                            + " "
                            + tvPropose.getContext().getString(R.string.item_desc_source).toUpperCase());
                    spannableProviderStr.setSpan(
                            new StyleSpan(Typeface.BOLD), providerStr.length(), spannableProviderStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvPropose.setText(spannableProviderStr);
                    tvPropose.setVisibility(View.VISIBLE);

                    ivAction.setImageResource(R.drawable.ressource_distante);
                    tvAction.setText(R.string.view);
                    ivFavorite.setVisibility(View.GONE);
                    llRead.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    card_view_get_informed.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    break;
                case SUBSTANCES:
                    llTag.setVisibility(View.VISIBLE);
                    tvTitle.setText(itemHuge.getTitle());
                    tvContent.setText(R.string.item_desc_substances);

                    providerStr = App.getAppContext().getString(R.string.search_home_find_by);

                    spannableProviderStr = new SpannableStringBuilder(providerStr
                            + " "
                            + tvPropose.getContext().getString(R.string.item_desc_source).toUpperCase()
                    );
                    spannableProviderStr.setSpan(
                            new StyleSpan(Typeface.BOLD), providerStr.length(), spannableProviderStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvPropose.setText(spannableProviderStr);
                    tvPropose.setVisibility(View.VISIBLE);

                    ivAction.setImageResource(R.drawable.ressource_distante);
                    tvAction.setText(R.string.view);
                    ivFavorite.setVisibility(View.GONE);
                    llRead.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    card_view_get_informed.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    break;
                case NUMERO_TEL:
                    ivAction.setImageResource(R.drawable.appeler_copier);
                    if (itemHuge.phones != null && itemHuge.phones.length > 0) {
                        tvAction.setText(tvTitle.getContext().getString(R.string.search_results_call) + " " + itemHuge.phones[0]);
                        llRead.setOnClickListener(v -> {
                            cardListener.onCall(itemHuge);
                        });
                        card_view_get_informed.setOnClickListener(v -> {
                            cardListener.onCall(itemHuge);
                        });
                    } else {
                        tvAction.setText(tvTitle.getContext().getString(R.string.search_results_call));
                    }
                    break;
                case APPLICATIONS:
                    ivAction.setImageResource(R.drawable.telecharger);
                    tvAction.setText(R.string.search_results_download);
                    llRead.setOnClickListener(v -> cardListener.onDownloadClicked(itemHuge));
                    card_view_get_informed.setOnClickListener(v -> cardListener.onDownloadClicked(itemHuge));
                    break;
                case VIDEO:
                    ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                    tvAction.setText(R.string.search_home_read);
                    llRead.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    card_view_get_informed.setOnClickListener(view -> cardListener.onRead(itemHuge));
                    break;
                case ENTITE_GEOGRAPHIQUE:
                case DOSSIER_THEMATIC:
                case FICHES_INFOS:
                    ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                    tvAction.setText(R.string.search_home_read);
                    llRead.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    card_view_get_informed.setOnClickListener(view -> cardListener.onRead(itemHuge));
                    break;
            }

            /*
             * Add date
             * lorsque le tri est fait par date
             */
            if (cardListener.getPresenter() != null) {
                if (cardListener.getPresenter().getOrder().equals(UrlExtras.RestUserSearch.SORT_DATE)) {
                    // Ajout d'une date bidon
                    String dateStr = "Date non renseignée";
                    if (itemHuge.date != null) {
                        SimpleDateFormat dateJson = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        SimpleDateFormat datePrint = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = dateJson.parse(itemHuge.date);
                            dateStr = datePrint.format(date);
                        } catch (ParseException e) {
                            // Si la date n'est pas parcable alors l'envoyer directement
                            dateStr = itemHuge.date;
                        }
                    }
                    tvDate.setText(tvDate.getContext().getString(R.string.search_get_informed_published, dateStr));
                    tvDate.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    class VideoCardHolder extends RecyclerView.ViewHolder implements CardHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;
        @BindView(R.id.tvPropose)
        TextView tvPropose;
        @BindView(R.id.ivFavorite)
        ImageView ivFavorite;
        @BindView(R.id.ivAction)
        ImageView ivAction;
        @BindView(R.id.tvAction)
        TextView tvAction;
        @BindView(R.id.tvLink)
        TextView tvLink;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.maintenanceTv)
        TextView maintenanceTv;
        @BindView(R.id.ivThumbnail)
        ImageView ivThumbnail;

        VideoCardHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(SearchItemHuge itemHuge) {
            tvContent.setText(itemHuge.getDescription());
            ivFavorite.setSelected(itemHuge.isFavorite());
            ivFavorite.setOnClickListener(view -> {
                if (cardListener != null) {
                    cardListener.onFavorite(itemHuge);
                }
            });

            if (itemHuge.type != null && itemHuge.type == SearchItemType.ACTUALITE) {
                String actuality = App.getAppContext().getString(R.string.search_results_actuality);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(actuality + " " + itemHuge.getTitle());
                int greyColor = App.getAppContext().getResources().getColor(R.color.textGray);
                spannableProposed.setSpan(new ForegroundColorSpan(greyColor), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableProposed.setSpan(new StyleSpan(Typeface.BOLD), 0, actuality.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvTitle.setText(spannableProposed);
                tvDate.setText(tvDate.getContext().getString(R.string.search_get_informed_published, itemHuge.date));
                tvDate.setVisibility(View.VISIBLE);
            } else {
                tvTitle.setText(itemHuge.getTitle());
                tvDate.setVisibility(View.GONE);
            }

            if (itemHuge.source != null && !itemHuge.source.isEmpty()) {
                String proposedString = App.getAppContext().getString(R.string.search_home_source);
                SpannableStringBuilder spannableProposed = new SpannableStringBuilder(proposedString + " " + itemHuge.source.toUpperCase());
                spannableProposed.setSpan(
                        new StyleSpan(Typeface.BOLD), proposedString.length(), spannableProposed.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tvPropose.setText(spannableProposed);
                tvPropose.setVisibility(View.VISIBLE);

            } else {
                if (itemHuge.sourceUrl != null && itemHuge.sourceUrl.isEmpty() == false) {
                    tvPropose.setText(itemHuge.sourceUrl);
                    tvPropose.setVisibility(View.VISIBLE);
                } else {
                    tvPropose.setVisibility(View.GONE);
                }
            }
            if (itemHuge.sourceUrl != null && itemHuge.sourceUrl.isEmpty() == false) {
                tvPropose.setOnClickListener(v -> {
                    cardListener.onUrlClicked(itemHuge.sourceUrl);
                });
            }

            // Load image
            if (itemHuge.image != null && itemHuge.image.isEmpty() == false) {
                Glide.with(ivThumbnail.getContext())
                        .load(itemHuge.image)
                        .error(R.drawable.adtracking48)
                        .into(ivThumbnail);
            }

            if (!TextUtils.isEmpty(itemHuge.link)) {
                String link;
                try {
                    link = Utils.getDomainName(itemHuge.link).concat("/..");
                    tvLink.setText(App.getAppContext().getString(R.string.search_results_find, link));
                    tvLink.setOnClickListener(v -> {
                        cardListener.onRead(itemHuge);
                    });
                    tvLink.setVisibility(View.VISIBLE);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    tvPropose.setVisibility(View.GONE);
                    tvLink.setOnClickListener(null);
                }
            } else {
                tvLink.setVisibility(View.GONE);
                tvLink.setOnClickListener(null);
            }

            if (itemHuge.isContentMaintenanceMode()) {
                maintenanceTv.setVisibility(View.VISIBLE);
                maintenanceTv.setText(repository.getConfig().getContentMaintenance().getTeaserActionText());
                ivFavorite.setVisibility(View.GONE);
                ivAction.setVisibility(View.GONE);
                tvAction.setVisibility(View.GONE);
            } else {
                maintenanceTv.setVisibility(View.GONE);
                ivFavorite.setVisibility(View.VISIBLE);
                ivAction.setVisibility(View.VISIBLE);
                tvAction.setVisibility(View.VISIBLE);
            }

            String providerStr;
            SpannableStringBuilder spannableProviderStr;

            switch (itemHuge.getNodeType()) {
                case VIDEO:
                    ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
                    tvAction.setText(R.string.search_home_read);
                    ivAction.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    tvAction.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    this.itemView.setOnClickListener(v -> cardListener.onRead(itemHuge));
                    break;
            }

            /*
             * Add date
             * lorsque le tri est fait par date
             */
            if (cardListener.getPresenter() != null) {
                if (cardListener.getPresenter().getOrder().equals(UrlExtras.RestUserSearch.SORT_DATE)) {
                    // Ajout d'une date bidon
                    String dateStr = "Date non renseignée";
                    if (itemHuge.date != null) {
                        SimpleDateFormat dateJson = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        SimpleDateFormat datePrint = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            Date date = dateJson.parse(itemHuge.date);
                            dateStr = datePrint.format(date);
                        } catch (ParseException e) {
                            // Si la date n'est pas parcable alors l'envoyer directement
                            dateStr = itemHuge.date;
                        }
                    }
                    tvDate.setText(tvDate.getContext().getString(R.string.search_get_informed_published, dateStr));
                    tvDate.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private interface CardHolder {
        void bind(SearchItemHuge itemHuge);
    }

    public interface InfoCardListener {
        SearchResultsPresenter getPresenter();

        void onRead(SearchItemHuge item);

        void onFavorite(SearchItemHuge item);

        void onDownloadClicked(SearchItemHuge itemHuge);

        void onUrlClicked(String url);

        void onCall(SearchItemHuge itemHuge);
    }
}
