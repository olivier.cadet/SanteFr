package com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.adyax.srisgp.databinding.FragmentPointsOfInterest3Binding;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class PointsOfInterest_3_Fragment extends ViewFragment implements PointsOfInterest_3_Presenter.CreateRecipeView {

    @Inject
    PointsOfInterest_3_Presenter presenter;
    private FragmentPointsOfInterest3Binding binding;
//    private PointsOfInterestRecyclerViewAdapter adapter;

    private Set<Long> ids = new HashSet<>();

    public PointsOfInterest_3_Fragment() {
    }

    public static Fragment newInstance() {
        PointsOfInterest_3_Fragment fragment = new PointsOfInterest_3_Fragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_points_of_interest_3, container, false);
        binding.validateCreateAccount.setOnClickListener(view -> {
            IViewPager activity = (IViewPager) getActivity();
            if (activity.getCreateAccountHelper().setIds(ids)) {
//                activity.moveToNext();
                binding.progressLayout.setVisibility(View.VISIBLE);
                presenter.moveToNext();
            }
        });
//        binding.recyclerView.setLayoutManager(new LinearLayoutManager(binding.recyclerView.getContext()));
//        binding.recyclerView.addItemDecoration(new DividerItemDecoration(binding.recyclerView.getContext(), R.drawable.line_points_of_interest));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.progressLayout.setVisibility(View.VISIBLE);
        presenter.sendTaxonomyVocabularyTerms();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.registration);
    }

    @NonNull
    @Override
    public PointsOfInterest_3_Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void updateTaxonomyVocabularyTermsResponse(TaxonomyVocabularyTermsResponse taxonomyVocabularyTermsResponse) {
//        adapter = new PointsOfInterestRecyclerViewAdapter(items.toArray(new TermsItem[items.size()]));
//        binding.recyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
        renderItems(taxonomyVocabularyTermsResponse.getArray());
        boolean bValidate = true;
        binding.validateCreateAccount.setEnabled(bValidate);
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    private void renderItems(TermsItem[] items) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (int i = 0; i < items.length; i++) {
            View row = inflater.inflate(R.layout.item_points_of_interest, (ViewGroup) getView(), false);
            Switch sw = (Switch) row.findViewById(R.id.switch_opt);
            TermsItem termsItem = items[i];
            sw.setText(termsItem.getName());
            sw.setTag(termsItem.getId());
            sw.setOnCheckedChangeListener((compoundButton, b) -> {
                final Long id = Long.valueOf(compoundButton.getTag().toString());
                if (b) {
                    ids.add(id);
                } else {
                    if (ids.contains(id)) {
                        ids.remove(id);
                    }
                }
            });
            binding.llOptions.addView(row);
        }
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        binding.progressLayout.setVisibility(View.INVISIBLE);
        if (BuildConfig.DEBUG) {
            if (errorResponse.isAnyError()) {
                Snackbar snackbar = Snackbar.make(binding.getRoot(), errorResponse.getErrorMessage(null), Snackbar.LENGTH_SHORT);
                View view = snackbar.getView();
                TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                tv.setTextColor(Color.RED);
                snackbar.show();
            }
        }
    }

    @Override
    public void onSuccess() {
//        fragmentFactory.startAnimateCreateAccount(getContext());
//        ((Activity)getContext()).finish();
//        break;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }
}
