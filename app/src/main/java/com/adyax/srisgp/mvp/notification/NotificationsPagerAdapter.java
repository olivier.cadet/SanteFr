package com.adyax.srisgp.mvp.notification;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.adyax.srisgp.R;

/**
 * Created by anton.kobylianskiy on 9/28/16.
 */

public class NotificationsPagerAdapter extends FragmentStatePagerAdapter {
    private int[] titleResources = new int[] {R.string.notification_all_title, R.string.notification_not_read_title, R.string.notification_read_title};
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private Context context;

    public NotificationsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        NotificationsFragment fragment = null;
        if (position == 0) {
            fragment = NotificationsFragment.newInstance(StatusNotification.all, position);
        } else if (position == 1) {
            fragment = NotificationsFragment.newInstance(StatusNotification.unread, position);
        } else {
            fragment = NotificationsFragment.newInstance(StatusNotification.read, position);
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(titleResources[position]);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
