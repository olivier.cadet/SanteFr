package com.adyax.srisgp.mvp.web_views;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.SearchResult;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.tracker.ItemAdapter;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.ContentResolverHelper;

import java.net.URI;
import java.net.URISyntaxException;


/**
 * Created by SUVOROV on 10/24/16.
 */

public class WebViewArg implements Parcelable {
    public static final String CONTENT_URL = "web_view_arg";

    private WebPageType webPageType = WebPageType.UNKNOWN;
    private String content_url;
    private String postData;
    private long nid;
    private String description;
    private boolean bFindType;
    private String title;
    private NodeType nodeType = NodeType.UNKNOWN;
    private String transmitter;
    // added for write to history for anonymus
    private HistoryItem historyItem = new HistoryItem();

    public void setXiti(Xiti xiti) {
        this.xiti = xiti;
    }

    private Xiti xiti;

    private int redButton = 1;

    private int fiche = 0;

    private String reportFormName;

    private boolean feedbackVisible = false;
    private boolean aroundMeActionVisible = false;

    public WebViewArg setPushAlert(AlertItem pushAlertId) {
        this.alertItem = pushAlertId;
        setWebPageType(WebPageType.STANDART);
        return this;
    }

    public WebViewArg setAlertItem(AlertItem alertItem) {
        this.alertItem = alertItem;
        return this;
    }

    private AlertItem alertItem = new AlertItem();


    public WebViewArg(HistoryItem historyItem, String content_url, WebPageType webPageType, TrackerLevel trackerLevel) {
        this.historyItem = historyItem;//
        this.content_url = content_url;
        this.webPageType = webPageType;
        this.xiti = new Xiti(trackerLevel);
    }

    public WebViewArg(String staticPage) {
        this.content_url = UrlExtras.getBaseURL() + staticPage;
        this.webPageType = WebPageType.STATIC_PAGE;
    }


    public WebViewArg(String staticPage, Boolean as_external) {
        if (as_external) {
            this.content_url = staticPage;
        } else {
            this.content_url = UrlExtras.getBaseURL() + staticPage;
        }
        this.webPageType = WebPageType.STATIC_PAGE;
    }

    public WebViewArg(String staticPage, WebPageType webType) {
        this.content_url = UrlExtras.getBaseURL() + staticPage;
        this.webPageType = webType;
    }

    protected WebViewArg(Parcel in) {
        webPageType = WebPageType.valueOf(in.readString());
        content_url = in.readString();
        postData = in.readString();
        alertItem = in.readParcelable(AlertItem.class.getClassLoader());
        redButton = in.readInt();
        nid = in.readLong();
        description = in.readString();
        bFindType = in.readInt() == 1;
        fiche = in.readInt();
        title = in.readString();
        transmitter = in.readString();
        nodeType = NodeType.valueOf(in.readString());
        xiti = in.readParcelable(Xiti.class.getClassLoader());
        historyItem = in.readParcelable(HistoryItem.class.getClassLoader());
        reportFormName = in.readString();
        feedbackVisible = in.readInt() == 1;
        aroundMeActionVisible = in.readInt() == 1;
    }

    public WebViewArg(IGetUrl item, WebPageType webPageType, TrackerLevel trackerLevel) {
        this(item, trackerLevel);
        this.webPageType = webPageType;
    }

    // TODO need save SearchItemHuge
    public WebViewArg(IGetUrl item, TrackerLevel trackerLevel) {
        this.historyItem = new ItemAdapter(item).getHistoryItem();
        this.content_url = item.getCardLink();
        this.postData = item.getPostData();
        this.nid = item.getNid();
        this.description = item.getDescription();
        this.bFindType = isFindType(item.getNodeType());
        this.title = item.getTitle();
        this.transmitter = item.getSourceOrPropose();
        this.nodeType = item.getNodeType();
        Xiti xiti = item.getXiti();
        if (xiti != null) {
            this.xiti = xiti.setTrackerLevel(trackerLevel);
        }
        if (this.webPageType == WebPageType.UNKNOWN) {
            this.webPageType = (content_url != null && content_url.contains(UrlExtras.getBaseURL())) ? WebPageType.SERVER : WebPageType.STANDART;
        }
    }

    public WebViewArg(long id, WebPageType server, TrackerLevel trackerLevel) {//
        this(null, String.format("%s/node/%d", UrlExtras.getBaseURL(), id), server, trackerLevel);
    }

    public WebViewArg(SearchResult searchResult, WebPageType server, TrackerLevel trackerLevel) {//
        this(null, String.format("%s%s", UrlExtras.getBaseURL(), searchResult.getUrl()), server, trackerLevel);
    }

    public static WebViewArg webViewArgForWidget(String url) {
        final WebViewArg args = new WebViewArg(url, true);
        args.feedbackVisible = true;
        args.aroundMeActionVisible = true;
        return args;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(webPageType.name());
        dest.writeString(content_url);
        dest.writeString(postData);
        dest.writeParcelable(alertItem, flags);
        dest.writeInt(redButton);
        dest.writeLong(nid);
        dest.writeString(description);
        dest.writeInt(bFindType ? 1 : 0);
        dest.writeInt(fiche);
        dest.writeString(title);
        dest.writeString(transmitter);
        dest.writeString(nodeType != null ? nodeType.name() : NodeType.UNKNOWN.name());
        dest.writeParcelable(xiti, flags);
        dest.writeParcelable(historyItem, flags);
        dest.writeString(reportFormName);
        dest.writeInt(feedbackVisible ? 1 : 0);
        dest.writeInt(aroundMeActionVisible ? 1 : 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<WebViewArg> CREATOR = new Creator<WebViewArg>() {
        @Override
        public WebViewArg createFromParcel(Parcel in) {
            return new WebViewArg(in);
        }

        @Override
        public WebViewArg[] newArray(int size) {
            return new WebViewArg[size];
        }
    };

    public URI getURI() {
        try {
            if (content_url != null)
                return new URI(content_url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getpostData() {
        return this.postData;
    }

    public String getContentUrl() {
        return content_url;
    }

    public WebPageType getWebPageType() {
        return webPageType;
    }

    public void setWebPageType(WebPageType webPageType) {
        this.webPageType = webPageType;
    }

    public boolean isContetntUrl() {
        return content_url != null;
    }

    public AlertItem getPushAlert() {
        return alertItem;
    }

    public WebViewArg setRedButtonFlag() {
        redButton = 1;
        return this;
    }

    public WebViewArg clearRedButtonFlag() {
        redButton = 0;
        return this;
    }

    public boolean isRedButton() {
        return redButton != 0;
    }

    public String getShortNode() {
        return String.format("node/%d", nid);
    }

    public String getDescription() {
        return description;
    }

    private boolean isFindType(NodeType nodeType) {
//        return nodeType!=null&&
//                ( nodeType== NodeType.PROFESSIONNEL_DE_SANTE||
//                        nodeType==NodeType.SERVICE_DE_SANTE||
//                        nodeType==NodeType.ESTABLISSEMENT_DE_SANTE||
//                        nodeType==NodeType.OFFRES_DE_SOINS);
        return nodeType != null && ContentResolverHelper.findType.contains(nodeType);
    }

    public boolean isFindType() {
        return bFindType;
    }

    public boolean isFindTypeForm() {
        final InapLinkHelper inapLinkHelper = new InapLinkHelper().fromInapp(reportFormName);
        return bFindType || InapLinkHelper.FIND_CONTENT_ERROR_FORM_NAME.equals(inapLinkHelper.getNameFormType());
    }

    public WebViewArg setFiche() {
        fiche = 1;
        return this;
    }

    public boolean isFiche() {
        return fiche != 0;
    }

    public WebViewArg clsFiche() {
        fiche = 0;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public String getTransmitter() {
        return transmitter;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public Xiti getXiti() {
        return xiti;
    }

    public IGetUrl getGetUrl(String url, String title) {
        if (title != null && title.length() > 0) {
            if (title.contains("|")) {
                title = title.substring(0, title.indexOf("|"));
            }
//            final String[] split=title.split("/|");
//            if(split.length>1){
//                title=split[0];
//            }
        }
        String finalTitle = title;
        return new IGetUrl() {

            @Override
            public IconType getIconType() {
                return IconType.UNKNOWN;
            }

            @Override
            public int getPosition() {
                if (BuildConfig.DEBUG) {
//                    throw new IllegalStateException();
                }
                return 0;
            }

            @Override
            public String getCardLink() {
                return url;
            }

            @Override
            public String getPostData() {
                return null;
            }

            @Override
            public long getNid() {
                return nid;
            }

            @Override
            public String getDescription() {
                return description;
            }

            @Override
            public NodeType getNodeType() {
                return nodeType;
            }

            @Override
            public String getTitle() {
                if (finalTitle != null && finalTitle.length() > 0) {
                    return finalTitle;
                }
                return WebViewArg.this.title;
            }

            @Override
            public String getSourceOrPropose() {
                return transmitter;
            }

            @Override
            public Xiti getXiti() {
                return xiti;
            }

            @Override
            public String getPhone() {
                return null;
            }

            @Override
            public String getApple() {
                return null;
            }

            @Override
            public String getGoogle() {
                return null;
            }
        };
    }

    public HistoryItem getHistory() {
        return historyItem;
    }

    public WebViewArg setTitle(String title) {
        this.title = title;
        return this;
    }

    public boolean isTransferTitle() {
        return webPageType != null && webPageType == WebPageType.TRANSFER && title != null && title.length() > 0;
    }

    public WebViewArg setReportFormName(String reportFormName) {
        this.reportFormName = reportFormName;
        return this;
    }

    public String getReportFormName() {
        return reportFormName;
    }

    public boolean isFeedbackVisible() {
        return this.feedbackVisible;
    }

    public boolean isAroundMeActionVisible() {
        return aroundMeActionVisible;
    }

    public void setFeedbackVisible(boolean feedbackVisible) {
        this.feedbackVisible = feedbackVisible;
    }

    public void setAroundMeActionVisible(boolean aroundMeActionVisible) {
        this.aroundMeActionVisible = aroundMeActionVisible;
    }


}
