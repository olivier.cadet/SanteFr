package com.adyax.srisgp.mvp.create_account.flow.input_password_1;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.databinding.FragmentInputPassword1Binding;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.create_account.CreateAccountHelper;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.DialogUtils;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class InputPassword_1_Fragment extends ViewFragment implements InputPassword_1_Presenter.CreateRecipeView {

    @Inject
    InputPassword_1_Presenter presenter;

    private FragmentInputPassword1Binding binding;

    public static Fragment newInstance() {
        InputPassword_1_Fragment fragment = new InputPassword_1_Fragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    public String getEmailValue() {
        return binding.emailCreateAccount.getText().toString().trim();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_input_password_1, container, false);
        if(binding==null){
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_input_password_1, container, false);
        }
        binding.emailCreateAccount.addTextChangedListener(watcher);
        binding.passwordCreateAccount.addTextChangedListener(watcher);
        binding.acceptCguCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.validateCreateAccount.setEnabled(CreateAccountHelper.getValidateButtonStatus(binding.emailCreateAccount, binding.passwordCreateAccount, binding.acceptCguCreateAccount.isChecked()));
            }
        });
        binding.validateCreateAccount.setOnClickListener(view -> {
            final IViewPager activity = (IViewPager) getActivity();
            Integer errStr = activity.getCreateAccountHelper().setEmailValidate(
                    getEmailValue());
            if (errStr == null) {
                errStr = activity.getCreateAccountHelper().setPasswordValidate(
                                binding.passwordCreateAccount.getText().toString(), false);
                if (errStr == null) {
                    binding.progressLayout.setVisibility(View.VISIBLE);
                    presenter.sendFakeRegistration();
                } else {
                    binding.passwordCreateAccount.setError(getString(errStr));
                    binding.passwordCreateAccount.requestFocus();
                }
            } else {
                binding.emailCreateAccount.setError(getString(errStr));
                binding.emailCreateAccount.requestFocus();
            }
        });
        binding.eyeSwitchCreateAccount.setOnCheckedChangeListener((compoundButton, b) -> {
            binding.passwordCreateAccount.setInputType(b ?
                    InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            if (!b) {
                binding.passwordCreateAccount.setTypeface(Typeface.DEFAULT);
            }
            final int textLength = binding.passwordCreateAccount.getText().length();
            binding.passwordCreateAccount.setSelection(textLength, textLength);
        });
        binding.passwordCreateAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                binding.eyeSwitchCreateAccount.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
            }
        });

//        binding.submitMessage.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.submitMessage, true);

//        binding.registrationRule.setOnClickListener(v -> {
//            presenter.onRegistrationRulesClicked();
//        });
        LoginFragment.setSubmitSpan(binding.registrationRule, true);

        if (BuildConfig.DEBUG) {
            binding.emailCreateAccount.setText("");
            binding.passwordCreateAccount.setText("");
        }

//        if (!BuildConfig.DEV_STATUS) {
            binding.facebookCreateAccount.setOnClickListener(v -> {
                ViewUtils.hideKeyboard(getActivity());
                presenter.facebookClick();
            });
            presenter.registerTwitterLoginButton(binding.twitterCreateAccount);

            binding.googleCreateAccount.setOnClickListener(v -> presenter.pickUserAccount());
//        } else {
//            binding.twitterCreateAccount.setClickable(false);
//        }

        binding.gotoLogin.setOnClickListener(v -> {
            presenter.gotoLogin();
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.registration);
    }

    @NonNull
    @Override
    public InputPassword_1_Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        if(errorResponse.isSSOError()){
            DialogUtils.showBadSSO(getContext(),
                    errorResponse.sso_client_id.getLocalizeErrorMessage(getContext()));
        }else {
            if (errorResponse.isEmailError()) {
                binding.emailCreateAccount.setError(errorResponse.getEmailItem().getLocalizeErrorMessage(getContext()));
                binding.emailCreateAccount.requestFocus();
            }
        }
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSuccess() {
        presenter.moveToNext();
        binding.progressLayout.setVisibility(View.INVISIBLE);
//        final IViewPager activity = (IViewPager) getActivity();
//        activity.moveToNext();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            binding.validateCreateAccount.setEnabled(CreateAccountHelper.
                    getValidateButtonStatus(binding.emailCreateAccount, binding.passwordCreateAccount, binding.acceptCguCreateAccount.isChecked()));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
}
