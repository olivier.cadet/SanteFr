package com.adyax.srisgp.mvp.cguHandler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.core.RestClient;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.my_account.CguDialogActivity;

public class CguHandler {
    private final Context context;
    private final RestClient restClient;
    private CommandExecutor commandExecutor;
    private CguActionHandler cguActionHandler;
    private boolean cguActivityVisible = false;

    private BroadcastReceiver mCguActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String cguAction = intent.getStringExtra("CGU_ACTION");

            cguActivityVisible = false;

            if (cguAction.equals("ACCEPTED"))
                cguActionHandler.getCgu_accepted_callback().run();
            else if (cguAction.equals("DECLINED")) {
                cguActionHandler.getCgu_declined_callback().run();
            }
        }
    };

    public CguHandler(Context context, RestClient restClient, CommandExecutor commandExecutor) {
        this.context = context;
        this.restClient = restClient;
        this.commandExecutor = commandExecutor;
    }

    private Context getContext() {
        return this.context;
    }

    private void cguStateReset() {
        this.commandExecutor.setPingState(false);
    }

    public void checkCguAgreement(String user_uid, final AppReceiver receiver, final int requestCode, Runnable cguOk) {
        restClient.checkCguReponseBaseCall(user_uid).subscribe(cguResponse -> {
            if (!cguResponse.toValidate) {
                cguOk.run();
                cguStateReset();
                commandExecutor.sendSuccess(requestCode, receiver);
            } else {
                askCguAgreement(
                        // on CGU Accepted
                        () -> {
                            restClient.sendCguReponseBaseCall(user_uid)
                                    .subscribe(acceptanceResponse ->
                                            {},
                                            (errorMessage, errorCode) -> commandExecutor.sendFail(requestCode, receiver, errorMessage));
                            cguOk.run();
                            cguStateReset();
                            commandExecutor.sendSuccess(requestCode, receiver);
                        },
                        // on CGU Declined
                        () -> {
                            commandExecutor.logout(receiver, requestCode);
                            cguStateReset();
                            commandExecutor.sendFail(requestCode, receiver, context.getString(R.string.cgu_declined));
                        }
                );
            }
        }, (errorMessage, errorCode) -> {
            commandExecutor.sendFail(requestCode, receiver, errorMessage);
            cguStateReset();
        });
    }

    public void acceptCgu(String user_uid) {

    }

    public void askCguAgreement(Runnable cguAccepted, Runnable cguDeclined) {
        if (cguActivityVisible) return; else cguActivityVisible = true;

        Intent intent = new Intent(context, CguDialogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        // Set Action Buffer
        this.cguActionHandler = new CguActionHandler(cguAccepted, cguDeclined);

        LocalBroadcastManager
                .getInstance(context)
                .registerReceiver(
                        mCguActionReceiver,
                        new IntentFilter("CGU_BROADCAST_EVENT")
                );
    }
}
