package com.adyax.srisgp.mvp.framework;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface IPresenter<V extends IView> {
    void attachView(@NonNull V view);

    void detachView();

    void registerListeners();

    void unregisterListeners();

    void restoreState(@Nullable Bundle savedInstanceState);

    void saveState(@NonNull Bundle outState);
}
