package com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes;

import android.database.Cursor;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_item.AlertItemFragment;

/**
 * Created by SUVOROV on 9/30/16.
 */

public abstract class AlertsSwipeAdapter extends FragmentStatePagerAdapter implements IPageUpdate {

    private Cursor cursor;

    public AlertsSwipeAdapter(FragmentManager fm, Cursor cursor) {
        super(fm);
        this.cursor = cursor;
    }

    @Override
    public Fragment getItem(int position) {
        if(cursor!=null&& cursor.moveToPosition(position)) {
            AlertItem alertItem = new AlertItem();
            BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, alertItem);
            return AlertItemFragment.newInstance(alertItem);
        }
        return null;
    }

    @Override
    public int getCount() {
        if(cursor!=null) {
            return cursor.getCount();
        }
        return 0;
    }

    public void swapCursor(Cursor cursor) {
        this.cursor = cursor;
        notifyDataSetChanged();
        if(cursor!=null) {
            update();
        }
    }

}
