package com.adyax.srisgp.mvp.report.report_info.report_email;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.utils.Utils;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportEmailPresenter_2 extends PresenterAppReceiver<ReportEmailPresenter_2.ReportPresenter_2View> {

    private static final int REPORT_WITH_EMAIL = 1;
    private static final int REPORT_WITHOUT_EMAIL = 2;

    private ReportBuilder builder = new ReportBuilder();
    private INetWorkState netWorkState;
    private IRepository repository;

    public <T extends Parcelable> void clickBackToCard() {
//        ExecutionService.sendCommand(getContext(), getAppReceiver(),
//                new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
//                        BaseFeedbackCommandCommand.SECRET_TOKEN, builder.build()), REPORT_WITHOUT_EMAIL);
        getView().showProgress();
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
            new GetFormCommand(getBuilder().getFromName(), null), ExecutionService.GET_FORM_REPORT_WITHOUT_EMAIL);
    }

    public void onSubmitButtonClicked(String email) {
        if (Utils.isEmailValid(email)) {
//            ExecutionService.sendCommand(getContext(), getAppReceiver(),
//                    new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
//                            BaseFeedbackCommandCommand.SECRET_TOKEN, builder.build()), REPORT_WITH_EMAIL);
            getView().showProgress();
            ExecutionService.sendCommand(getContext(), getAppReceiver(),
                    new GetFormCommand(getBuilder().getFromName(), null), ExecutionService.GET_FORM_REPORT_WITH_EMAIL);
        } else {
            if (getView() != null) {
                getView().setInvalidEmailError();
            }
        }
    }

    public interface ReportPresenter_2View extends IView {
        void showCloseDialog();

        void showEmail(String email);

        void setSubmitButtonVisibility(boolean visible);

        void setReturnButtonVisibility(boolean visible);

        void setInvalidEmailError();

        void onBack();

        void showMessage(String body);

        void showProgress();

        void hideProgress();
    }

    @Inject
    public ReportEmailPresenter_2(INetWorkState netWorkState, IRepository repository) {
        this.netWorkState = netWorkState;
        this.repository = repository;
    }

    public void onEmailTextChanged(String email) {
        if (TextUtils.isEmpty(email)) {
            if (getView() != null) {
                getView().setSubmitButtonVisibility(false);
                getView().setReturnButtonVisibility(true);
            }
        } else {
            if (getView() != null) {
                getView().setSubmitButtonVisibility(true);
                getView().setReturnButtonVisibility(false);
            }
        }
    }

    public void onViewCreated() {
        RegisterUserResponse credential = repository.getCredential();
        if (credential != null && credential.user != null && getView() != null) {
            getView().showEmail(credential.user.mail);
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.GET_FORM_REPORT_WITHOUT_EMAIL:
                final FeedbackFormResponse feedbackFormResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                if(feedbackFormResponse!=null) {
                    ExecutionService.sendCommand(getContext(), getAppReceiver(),
                            new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
                                    feedbackFormResponse.getSecret(), builder.build(), data.getLong(CommandExecutor.START_TIME)), REPORT_WITHOUT_EMAIL);
                }else{
                    getView().showMessage("ExecutionService.GET_FORM_REPORT_WITHOUT_EMAIL");
                }
                break;
            case ExecutionService.GET_FORM_REPORT_WITH_EMAIL:
                final FeedbackFormResponse feedbackFormResponse2 = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                if(feedbackFormResponse2!=null) {
                    ExecutionService.sendCommand(getContext(), getAppReceiver(),
                            new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
                                    feedbackFormResponse2.getSecret(), builder.build(), data.getLong(CommandExecutor.START_TIME)), REPORT_WITH_EMAIL);
                }else{
                    getView().showMessage("ExecutionService.GET_FORM_REPORT_WITH_EMAIL");
                }
                break;
            case REPORT_WITHOUT_EMAIL:
                getView().hideProgress();
                if (getView() != null) {
                    getView().onBack();
                }
                break;
            case REPORT_WITH_EMAIL:
                getView().hideProgress();
                if (getView() != null) {
                    getView().showCloseDialog();
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        getView().hideProgress();
        getView().showMessage(errorMessage.getErrorMessage());
    }

    public ReportBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(ReportBuilder builder) {
        this.builder = builder;
    }

}
