package com.adyax.srisgp.mvp.searchhome;

import android.location.Location;
import android.os.Bundle;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.GetQuickCardsCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.GetQuickCardsAnonymousResponse;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.repository.SharedPreferencesHelper;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterGeo;
import com.adyax.srisgp.mvp.my_account.PersonalizeActivity;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

/**
 * Created by anton.kobylianskiy on 9/21/16.
 */
public class SearchHomePresenter extends PresenterGeo<SearchHomePresenter.SearchHomeView> implements AppReceiver.AppReceiverCallback, SharedPreferencesHelper.LoggedInStateListener {

    private IRepository repository;
    private FragmentFactory fragmentFactory;
    private IDataBaseHelper dataBaseHelper;
    private INetWorkState netWorkState;

    @Inject
    public SearchHomePresenter(IRepository repository, FragmentFactory fragmentFactory, IDataBaseHelper dataBaseHelper,
                               INetWorkState netWorkState, IGeoHelper geoHelper) {
        super(geoHelper);
        this.repository = repository;
        this.fragmentFactory = fragmentFactory;
        this.dataBaseHelper = dataBaseHelper;
        this.netWorkState = netWorkState;
    }

    public void onLoggedInClicked() {
        if (getView() != null) {
            getView().openLogInScreen();
        }
    }

    @Override
    public void attachView(@NonNull SearchHomeView view) {
        super.attachView(view);
        repository.addLoggedInStateListener(this);
//        geoHelper.setOnLocationListener(new LocationListener(){
//
//            @Override
//            public void onLocationChanged(Location location) {
//                getCards(location);
//                geoHelper.disconnect();
//            }
//        });
//        startAskGeo();
    }

    private void getCards(Location location) {
        final ServiceCommand command = new GetQuickCardsCommand(location);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), command, ExecutionService.COMMAND_GET_CARDS);
    }

    @Override
    public void detachView() {
        super.detachView();
        repository.removeLoggedInStateListener(this);
//        geoHelper.disconnect();
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == ExecutionService.COMMAND_GET_CARDS) {
//            getView().
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == ExecutionService.COMMAND_GET_CARDS) {
            GetQuickCardsAnonymousResponse response = data.getParcelable(CommandExecutor.BUNDLE_GET_QUICK_CARDS);
            if (getView() != null) {
                getView().showCards(response.items);
            }
        } else if (requestCode == ExecutionService.FAVORITE_ACTION) {
            if (getView() != null) {
                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                if (command.getNids() != null) {
                    for (Long id : command.getNids()) {
                        getView().updateCard(id, command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD));
                    }
                }
            }
        }

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    @Override
    public void onLoggedIn() {
        startAskGeo();
        getView().onLoggedIn();
    }

    @Override
    public void onLoggedOut() {
        startAskGeo();
        getView().onLoggedOut();
    }

    public void clickFavorite(QuickCardsAnonymousItem item) {
        if (getView() != null) {
            if (netWorkState.isLoggedIn()) {
                ExecutionService.sendCommand(
                        getContext(),
                        getAppReceiver(),
                        new AddOrRemoveFavoriteCommand(
                                TrackerLevel.HOMEPAGE,
                                item.getId(),
                                item.isFavorite() ? AddOrRemoveFavoriteCommand.ACTION_REMOVE : AddOrRemoveFavoriteCommand.ACTION_ADD,
                                item
                        ),
                        ExecutionService.FAVORITE_ACTION
                );
            } else {
                getView().openRegistrationScreen();
            }
        }
    }

    public void addTouchGesture(QuickCardsAnonymousItem item, ClickType clickType) {
        try {
            if(item.getXiti()!=null) {
                if(clickType==ClickType.clic_homepage_acces_rapides_fermer){
                    new TrackerHelper(item).addTouchGesture(TrackerLevel.HOMEPAGE, clickType);
                }else {
                    new TrackerHelper(item).clickOnHomePage(TrackerLevel.HOMEPAGE, clickType);
                }
            }else{
                getView().showMessage("xiti is null");
            }
        }catch (Exception e){
            getView().showMessage(e.getMessage());
        }
    }

    public void clickRead(QuickCardsAnonymousItem card, boolean showFavorite) {
//        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(card, WebPageType.SERVER), showFavorite);
        fragmentFactory.startWebViewActivity(getContext(),
                new WebViewArg(card, card.isSimplePage()? WebPageType.SIMPLE: WebPageType.SERVER, TrackerLevel.HOMEPAGE), -1);
    }

    public void onPersonalizeButtonClicked() {
        if (getView() != null) {
            if (netWorkState.isLoggedIn()) {
                PersonalizeActivity.start(getView().getActivity());
            } else {
                getView().openRegistrationScreen();
            }
        }
    }

    public void onMoreInformationClicked() {
        new TrackerHelper().clickEnSavoirPlus();
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.ABOUT_HOME_PAGE), -1);
    }

    @Override
    public void update(Location location) {
        getCards(location);
    }

    public interface SearchHomeView extends IView {
        void showCards(List<QuickCardsAnonymousItem> items);

        void onLoggedIn();

        void onLoggedOut();

        void openRegistrationScreen();

        void openLogInScreen();

        void updateCard(long id, boolean favorite);

        FragmentActivity getActivity();

        void showMessage(String body);
    }

}
