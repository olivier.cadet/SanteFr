package com.adyax.srisgp.mvp.report.report_info.report_info_1;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.databinding.FragmentReport1Binding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportInfoFragment_1 extends ViewFragment implements ReportInfoPresenter_1.DummyView {

    public static final String TITLE = "title";

    @Inject
    ReportInfoPresenter_1 reportPresenter1;

    @Inject
    FragmentFactory fragmentFactory;

    private FragmentReport1Binding binding;
    private int title;

    public ReportInfoFragment_1() {
    }

    public static Fragment newInstance(int title, String stringExtra) {
        ReportInfoFragment_1 fragment = new ReportInfoFragment_1();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putString(ReportBuilder.FIELD_URL, stringExtra);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        title = getArguments().getInt(TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_1, container, false);

        binding.reportErrorInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.btnReport1Action.setEnabled(s.length() > 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.btnReport1Action.setOnClickListener(view -> {
            final ReportBuilder builder = reportPresenter1.getBuilder();
            builder.setFromName(InapLinkHelper.INFO_CONTENT_ERROR_FORM_NAME)
                    .set(ReportBuilder.FIELD_ERROR_INFO, binding.reportErrorInfo.getText().toString().trim())
                    .set(ReportBuilder.FIELD_PROPOSED_INFO, binding.text112.getText().toString())
                    .set(ReportBuilder.FIELD_URL, getArguments().getString(ReportBuilder.FIELD_URL));
            fragmentFactory.startReport2Fragment(getContext(), builder);
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    @NonNull
    @Override
    public ReportInfoPresenter_1 getPresenter() {
        return reportPresenter1;
    }
}
