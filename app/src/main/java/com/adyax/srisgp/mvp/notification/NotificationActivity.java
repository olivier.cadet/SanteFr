package com.adyax.srisgp.mvp.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

/**
 * Created by anton.kobylianskiy on 9/30/16.
 */

public class NotificationActivity extends MaintenanceBaseActivity {
    private static final String EXTRA_NOTIFICATION = "notification";

    public static Intent newIntent(Context context, NotificationItem notificationItem) {
        Intent intent = new Intent(context, NotificationActivity.class);
        intent.putExtra(EXTRA_NOTIFICATION, notificationItem);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null) {
                NotificationItem notification = intent.getParcelableExtra(EXTRA_NOTIFICATION);
                replaceFragment(R.id.frame, NotificationFragment.newInstance(notification));
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
