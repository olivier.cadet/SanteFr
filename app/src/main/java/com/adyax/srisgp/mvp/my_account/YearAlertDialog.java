package com.adyax.srisgp.mvp.my_account;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.adyax.srisgp.R;

import java.util.Calendar;

/**
 * Created by Kirill on 10.11.16.
 */

public class YearAlertDialog extends AlertDialog {

    private NumberPicker yearNumberPicker;

    protected YearAlertDialog(@NonNull Context context) {
        super(context);
    }

    public static void quickShow(Context context, int currentYear, YearListener listener) {
        Builder builder = new Builder(context);

        View view = LayoutInflater.from(context).inflate(R.layout.dialog_select_year, null, false);
        NumberPicker numberPicker = ((NumberPicker) view.findViewById(R.id.yearNumberPicker));
        int year = Calendar.getInstance().get(Calendar.YEAR);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker.setMinValue(year - 100);
        numberPicker.setMaxValue(year);
        numberPicker.setValue(currentYear);
        builder.setPositiveButton("Ok", (dialog, which) -> {
            if (listener != null)
                listener.onYearSelected(numberPicker.getValue());
        });
        builder.setNegativeButton("Cancel", null);
        builder.setView(view);
        builder.show();
    }

    public interface YearListener {
        void onYearSelected(int year);
    }
}
