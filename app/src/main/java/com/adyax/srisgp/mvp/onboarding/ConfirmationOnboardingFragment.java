package com.adyax.srisgp.mvp.onboarding;

import android.os.Bundle;

import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.presentation.BaseFragment;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public class ConfirmationOnboardingFragment extends BaseFragment {

    public static ConfirmationOnboardingFragment newInstance() {
        ConfirmationOnboardingFragment fragment = new ConfirmationOnboardingFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_onboarding_confirmation, container, false);
        return view;
    }
}
