package com.adyax.srisgp.mvp.favorite;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.FavoriteUsers;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

import static android.app.Activity.RESULT_OK;


public class FavoritesFragment extends ViewFragment implements FavoritesPresenter.FavoritesView {

    private static final int REQUEST_FAVORITES = 124;
    private static final String ARGUMENT_FAVORITE_TYPE = "favorite_type";

    @Inject
    FavoritesPresenter favoritesPresenter;
    @BindView(R.id.llEmptyView)
    LinearLayout llEmptyView;

    private FavoritesAdapter favoritesAdapter;
    private FavoritesListener listener;

    @BindView(R.id.rvFavorites)
    RecyclerView rvFavorites;

    public static FavoritesFragment newInstance(FavoriteType type) {
        Bundle args = new Bundle();
        FavoritesFragment fragment = new FavoritesFragment();
        args.putSerializable(ARGUMENT_FAVORITE_TYPE, type);
        fragment.setArguments(args);
        return fragment;
    }

    public void onPageChanged() {
        resetSwipeState(null);
    }

    public void updateAll() {
        favoritesAdapter.notifyDataSetChanged();
    }

    public void addLoadingItem(long id) {
        favoritesAdapter.notifyItemChangedById(id);
    }

    public void changeSelectionMode(FavoritesItem item) {
        favoritesAdapter.changeSelectionMode(item);
    }

    private FavoriteType getFavoriteType() {
        return (FavoriteType) getArguments().getSerializable(ARGUMENT_FAVORITE_TYPE);
    }

    public List<FavoritesItem> getItems() {
        return favoritesAdapter.getFavorites();
    }

    public void changeEditMode() {
        favoritesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FAVORITES) {
            if (resultCode == RESULT_OK) {
//                long id = data.getLongExtra(NotificationFragment.INTENT_NOTIFICATION_ID, -1);
//                favoritesAdapter.setAsRead(id);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.listener = (FavoritesListener) getParentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        favoritesPresenter.onStart();
    }

    private void bindViews(View view) {
        favoritesAdapter = new FavoritesAdapter(new FavoritesAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(FavoritesItem item) {
                favoritesPresenter.onItemClicked(item);
            }

            @Override
            public void onRemove(FavoritesItem item) {
                listener.onRemove(item);
            }

            @Override
            public boolean isEditMode() {
                return listener.isEditMode();
            }

            @Override
            public void onSelect(FavoritesItem item) {
                listener.changeSelection(item);
            }

            @Override
            public boolean isLoadingItem(FavoritesItem item) {
                return listener.isLoadingItem(item);
            }

            @Override
            public boolean isSelectedItem(FavoritesItem item) {
                return listener.isSelectedItem(item);
            }
        });

        favoritesAdapter.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {

            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {
                if (!moveToRight) {
                    for (int i = 0; i < favoritesAdapter.getItemCount(); i++) {
                        RecyclerView.ViewHolder viewHolder = rvFavorites.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null && viewHolder instanceof FavoritesAdapter.FavoriteViewHolder) {
                            SwipeLayout currentSwipeLayout = ((FavoritesAdapter.FavoriteViewHolder) viewHolder).swipeLayout;
                            if (currentSwipeLayout != swipeLayout) {
                                currentSwipeLayout.animateReset();
                            }
                        }
                    }
                }
            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }
        });

        favoritesAdapter.setSwipeCheckListener((swipeLayout) -> {
            return resetSwipeState(swipeLayout);
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvFavorites.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                favoritesPresenter.requestFavorites(totalItemsCount);
            }
        });
        rvFavorites.setHasFixedSize(true);
        rvFavorites.setLayoutManager(layoutManager);
        rvFavorites.setAdapter(favoritesAdapter);
    }

    private boolean resetSwipeState(SwipeLayout swipeLayout) {
        boolean isClosed = true;
        for (int i = 0; i < favoritesAdapter.getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = rvFavorites.findViewHolderForAdapterPosition(i);
            if (viewHolder != null && viewHolder instanceof FavoritesAdapter.FavoriteViewHolder) {
                SwipeLayout currentSwipeLayout = ((FavoritesAdapter.FavoriteViewHolder) viewHolder).swipeLayout;
                if (currentSwipeLayout != swipeLayout && currentSwipeLayout.getOffset() != 0) {
                    isClosed = false;
                    currentSwipeLayout.animateReset();
                }
            }
        }
        return isClosed;
    }

    @Override
    public void showEmptyView() {
        llEmptyView.setVisibility(View.VISIBLE);
        rvFavorites.setVisibility(View.GONE);

        if (getFavoriteType() == FavoriteType.all) {
            listener.disableTabs();
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(llEmptyView.getRootView(), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showFavorites(FavoriteUsers favorites) {
        if (favorites == null || (favoritesAdapter.getItemCount() == 0 && favorites.items.length == 0)) {
            showEmptyView();
            listener.onAllItemsRemoved(this);
        } else {
            listener.enableTabs();
            llEmptyView.setVisibility(View.GONE);
            rvFavorites.setVisibility(View.VISIBLE);
            favoritesAdapter.addFavorites(new ArrayList<>(Arrays.asList(favorites.items)));
            listener.onItemCountChanged(this, favoritesAdapter.getFavorites().size());
        }
    }

    @Override
    public void resetFavorites() {
        if (favoritesAdapter != null)
            favoritesAdapter.clearFavorites();
    }

    public void removeNotification(long removedId) {
        favoritesAdapter.removeItem(removedId);
        listener.onItemCountChanged(this, favoritesAdapter.getFavorites().size());
        if (favoritesAdapter.getFavorites().isEmpty()) {
            favoritesPresenter.requestFavorites(0);
//            showEmptyView();
//            listener.onAllItemsRemoved();
        }
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        favoritesPresenter.setType(getFavoriteType());
        super.onAttach(context);
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return favoritesPresenter;
    }

    public interface FavoritesListener {
        void onRemoved(long id);

        void onRemove(FavoritesItem item);

        void changeSelection(FavoritesItem item);

        boolean isLoadingItem(FavoritesItem item);

        boolean isSelectedItem(FavoritesItem item);

        boolean isEditMode();

        void onItemCountChanged(FavoritesFragment fragment, int count);

        void disableTabs();

        void enableTabs();

        void onAllItemsRemoved(FavoritesFragment favoritesFragment);
    }
}
