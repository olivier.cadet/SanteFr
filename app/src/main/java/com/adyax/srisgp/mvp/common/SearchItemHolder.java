package com.adyax.srisgp.mvp.common;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;

/**
 * Created by anton.kobylianskiy on 11/3/16.
 */

public abstract class SearchItemHolder extends RecyclerView.ViewHolder {
    public SearchItemHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(SearchItemHuge searchItemHuge, FindCardListener listener);
}
