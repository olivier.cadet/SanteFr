package com.adyax.srisgp.mvp.my_account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.ActivityChangeCredentialsBinding;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;

/**
 * Created by Kirill on 17.11.16.
 */

public class ChangeCredentialsActivity extends MaintenanceBaseActivity implements ChangeCredentialsPresenter.ChangeCredentialsView {
    @Inject
    ChangeCredentialsPresenter presenter;

    private ActivityChangeCredentialsBinding binding;

    public static void start(Activity activity) {
        new TrackerHelper().addScreenView(TrackerLevel.ACCOUNT_MANAGEMENT, TrackerAT.MES_IDENTIFIANTS_DE_CONNEXION);
        Intent intent = new Intent(activity, ChangeCredentialsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        presenter.attachView(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_credentials);
        binding.bChangeEmail.setOnClickListener(v -> changeEmailClick());
        binding.bChangePass.setOnClickListener(v -> changePassClick());
        binding.bReturn.setOnClickListener(v -> onBackPressed());
        binding.bValidateEmail.setOnClickListener(v -> presenter.validateEmailClick());
        binding.bValidatePassword.setOnClickListener(v -> presenter.validatePasswordClick());

        binding.ivNewEmailClear.setOnClickListener(v -> binding.etNewEmail.setText(""));
        binding.cbOldPasswordShow.setOnCheckedChangeListener((buttonView, isChecked) -> changePasswordState(binding.etOldPassword, isChecked));
        binding.cbNewPasswordShow.setOnCheckedChangeListener((buttonView, isChecked) -> changePasswordState(binding.etNewPassword, isChecked));

        binding.etNewEmail.addTextChangedListener(new ShowClearTextWatcher(binding.ivNewEmailClear, s -> presenter.isShowValidateEmail()));
        binding.etOldPassword.addTextChangedListener(new ShowClearTextWatcher(binding.cbOldPasswordShow, s -> presenter.isShowValidatePass()));
        binding.etNewPassword.addTextChangedListener(new ShowClearTextWatcher(binding.cbNewPasswordShow, s -> presenter.isShowValidatePass()));

//        binding.labelBottom.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.labelBottom, false);

//        binding.etOldEmail.setErrorListener(isShow -> setClearViewMargin(binding.ivOldEmailClear,
//                isShow, getResources().getDimensionPixelSize(R.dimen.default_margin_x0_5)));
//        binding.etNewEmail.setErrorListener(isShow -> setClearViewMargin(binding.ivNewEmailClear,
//                isShow, getResources().getDimensionPixelSize(R.dimen.default_margin_x0_5)));
//        binding.etOldPassword.setErrorListener(isShow -> setClearViewMargin(binding.cbOldPasswordShow,
//                isShow, getResources().getDimensionPixelSize(R.dimen.default_margin_x0_25)));
//        binding.etNewPassword.setErrorListener(isShow -> setClearViewMargin(binding.cbNewPasswordShow,
//                isShow, getResources().getDimensionPixelSize(R.dimen.default_margin_x0_25)));

        initActionBar();

        presenter.onViewCreated();
    }

    @NonNull
    @Override
    public Context getContext() {
        return this;
    }

    private void initActionBar() {
        setSupportActionBar((Toolbar) binding.getRoot().findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.my_account_title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!isButtonsShow())
            showButtons();
        else
            super.onBackPressed();
    }

    @Override
    public void setEmail(String email) {
        binding.tvLabel3.setText(String.format(getString(R.string.change_credentials_label_3), email));
        if (!TextUtils.isEmpty(email)) {
            binding.etNewEmail.setText(email);
            binding.etNewEmail.setSelection(email.length());
        }
    }

    @Override
    public String getNewEmail() {
        return binding.etNewEmail.getText().toString();
    }

    @Override
    public String getOldPass() {
        return binding.etOldPassword.getText().toString();
    }

    @Override
    public String getNewPass() {
        return binding.etNewPassword.getText().toString();
    }

    @Override
    public void showValidateEmail(boolean isShow) {
        binding.bValidateEmail.setEnabled(isShow);
    }

    @Override
    public void showValidatePass(boolean isShow) {
        binding.bValidatePassword.setEnabled(isShow);
    }

    private void hideAll() {
        binding.rlEditEmail.setVisibility(View.INVISIBLE);
        binding.rlEditPassword.setVisibility(View.INVISIBLE);
        binding.llButtons.setVisibility(View.INVISIBLE);
        binding.rlResult.setVisibility(View.INVISIBLE);
    }

    private void showButtons() {
        hideAll();
        binding.llButtons.setVisibility(View.VISIBLE);
    }

    private void showEmailEdit() {
        hideAll();
        binding.rlEditEmail.setVisibility(View.VISIBLE);
    }

    private void showPassEdit() {
        hideAll();
        binding.rlEditPassword.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmailChangedResult() {
        hideAll();
        binding.rlResult.setVisibility(View.VISIBLE);
        binding.tvLabelResultEmail.setVisibility(View.VISIBLE);
        binding.tvLabelResultPass.setVisibility(View.GONE);

//        binding.etNewEmail.setText("");
    }

    @Override
    public void showPassChangedResult() {
        hideAll();
        binding.rlResult.setVisibility(View.VISIBLE);
        binding.tvLabelResultEmail.setVisibility(View.GONE);
        binding.tvLabelResultPass.setVisibility(View.VISIBLE);

        binding.etOldPassword.setText("");
        binding.etNewPassword.setText("");
    }

    @Override
    public void setNewEmailError(String error) {
        binding.etNewEmail.setError(error);
        if(error!=null) {
            binding.etNewEmail.requestFocus();
        }
    }

    @Override
    public void setOldPassError(String error) {
        binding.etOldPassword.setError(error);
        if(error!=null) {
            binding.etOldPassword.requestFocus();
        }
    }

    @Override
    public void setNewPassError(String error) {
        binding.etNewPassword.setError(error);
        if(error!=null) {
            binding.etNewPassword.requestFocus();
        }
    }

    private boolean isButtonsShow() {
        return binding.llButtons.getVisibility() == View.VISIBLE;
    }

    private void changeEmailClick() {
        showEmailEdit();
    }

    private void changePassClick() {
        showPassEdit();
    }

    private void setClearViewMargin(View errorView, boolean isError, int smallMargin) {
        if (errorView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) errorView.getLayoutParams();
            int rightMargin = isError ?
                    getResources().getDimensionPixelSize(R.dimen.default_margin_x2_5) :
                    smallMargin;
            p.setMargins(p.leftMargin, p.topMargin, rightMargin, p.bottomMargin);
            errorView.requestLayout();
        }
    }

    private void changePasswordState(EditText passwordEditText, boolean isChecked) {
        passwordEditText.setInputType(isChecked ? InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        if (!isChecked) passwordEditText.setTypeface(Typeface.DEFAULT);
        final int textLength = passwordEditText.getText().length();
        passwordEditText.setSelection(textLength, textLength);
    }

    private class ShowClearTextWatcher implements TextWatcher {

        private View clearView;
        private TextChangeAction textChangeAction;

        public ShowClearTextWatcher(View clearView) {
            this(clearView, null);
        }

        public ShowClearTextWatcher(View clearView, TextChangeAction textChangeAction) {
            this.clearView = clearView;
            this.textChangeAction = textChangeAction;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 0)
                clearView.setVisibility(View.GONE);
            else
                clearView.setVisibility(View.VISIBLE);

            if (textChangeAction != null)
                textChangeAction.onTextChange(s.toString());
        }
    }

    private interface TextChangeAction {
        void onTextChange(String s);
    }
}
