package com.adyax.srisgp.mvp.notification;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.CursorLoader;

import com.adyax.srisgp.db.IDataBaseHelper;

/**
 * Created by SUVOROV on 3/25/16.
 */
public class NotificationCursorLoader extends CursorLoader {

    private IDataBaseHelper dataBaseHelper;

    public NotificationCursorLoader(Context context, IDataBaseHelper dataBaseHelper) {
        super(context);
        this.dataBaseHelper = dataBaseHelper;
    }

    @Override
    public Cursor loadInBackground() {
        return dataBaseHelper.getNotification(StatusNotification.all);
    }
}
