package com.adyax.srisgp.mvp.search;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.ClearRecentSearchPhrasesCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchPhrasesCommand;
import com.adyax.srisgp.data.net.command.GetPopularCommand;
import com.adyax.srisgp.data.net.command.GetRecentSearchPhraseCommand;
import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchPhrasesResponse;
import com.adyax.srisgp.data.net.response.PopularResponse;
import com.adyax.srisgp.data.net.response.SearchResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 11/1/16.
 */

public class SearchPhrasesPresenter extends Presenter<SearchPhrasesPresenter.SearchPhrasesView> implements AppReceiver.AppReceiverCallback {

    public static final int STATE_PHRASES_DEFAULT = 0;
    public static final int STATE_PHRASES_SEARCH = 1;

    private static final int COMMAND_GET_RECENT = 0;
    private static final int COMMAND_GET_POPULAR = 1;
    private static final int COMMAND_GET_AUTOCOMPLETE = 2;

    private static final int COMMAND_REMOVE_SEARCH_PHRASE = 3;
    private static final int COMMAND_CLEAR_SEARCH_PHRASES = 4;

    private static final int COMMAND_SEARCH = 6;

    private AtomicBoolean getResponse = new AtomicBoolean(false);

    private AppReceiver appReceiver = new AppReceiver();

    private List<SearchItemViewModel> recentSearchItems;
    private PopularResponse popularPhrases;

    private GetAutoCompleteSearchPhrasesResponse autoCompleteSearchPhrases;

    private int currentState = STATE_PHRASES_DEFAULT;

    private Handler searchHandler = new Handler();

    private String searchText = "";

    @Inject
    public SearchPhrasesPresenter() {
    }

    @Override
    public void attachView(@NonNull SearchPhrasesView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    public void loadAutocompleteData() {
        getPopularPhrases();
        getRecentPhrases();
    }

    private void getPopularPhrases() {
        ServiceCommand command = new GetPopularCommand();
        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_GET_POPULAR);
    }

    private void getRecentPhrases() {
        ServiceCommand command = new GetRecentSearchPhraseCommand();
        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_GET_RECENT);
    }


    @Override
    public void detachView() {
        appReceiver.setListener(null);
        super.detachView();
    }

    public void onSearchFieldFocus(String text) {
//        if (SearchHelper.isSearchState(text)) {
//            showPhrasesSearchState(autoCompleteSearchPhrases);
//        } else {
        showPhrasesDefaultState();
//        }
    }

    public void onSearchFieldTextChanged(String criteria) {
        getView().showPhrasesAutocomplete();
        searchText = criteria;
        getView().updateStatusLocation();
        if (SearchHelper.isSearchState(criteria)) {
            searchHandler.removeCallbacksAndMessages(null);
            searchHandler.postDelayed(() -> {
                if (getView() != null) {
                    ExecutionService.sendCommand(getContext(), appReceiver,
                            new GetAutoCompleteSearchPhrasesCommand(criteria), COMMAND_GET_AUTOCOMPLETE);
                }
            }, SearchHelper.SEARCH_DELAY);
        } else {
//            getView().disableSearchButton();
            showPhrasesDefaultState();
        }
    }

    public void removeSearchPhrase(SearchItemViewModel searchItem) {
        SearchHelper.removeSearchEntry(getContext(), appReceiver, searchItem.getId(), COMMAND_REMOVE_SEARCH_PHRASE, RemoveSearchItemCommand.ITEM_PHRASE);
    }

    public void removeSearchPhrasesHistory() {
        ServiceCommand command = new ClearRecentSearchPhrasesCommand();
        ExecutionService.sendCommand(getContext(), appReceiver, command, COMMAND_CLEAR_SEARCH_PHRASES);
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    private void showPhrasesDefaultState() {
        if (recentSearchItems != null && popularPhrases != null) {
            currentState = STATE_PHRASES_DEFAULT;
            getView().showPhrasesDefaultState(recentSearchItems, popularPhrases);
        }
    }


    public void showPhrasesSearchState(GetAutoCompleteSearchPhrasesResponse response) {
        currentState = STATE_PHRASES_SEARCH;
        getView().showPhrasesSearchState(response);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_GET_POPULAR) {
            popularPhrases = data.getParcelable(CommandExecutor.BUNDLE_GET_POPULAR_PHRASES);
            processResponse();
        } else if (requestCode == COMMAND_GET_RECENT) {
            SearchResponse recentSearchPhrases = data.getParcelable(CommandExecutor.BUNDLE_GET_RECENT_PHRASES);
            if (recentSearchPhrases != null) {
                recentSearchItems = transformSearchItems(recentSearchPhrases.items);
                getView().showPhrasesDefaultState(recentSearchItems);
            } else {
                recentSearchItems = new ArrayList<>();
            }
            processResponse();
        } else if (requestCode == COMMAND_GET_AUTOCOMPLETE) {
            autoCompleteSearchPhrases = data.getParcelable(CommandExecutor.BUNDLE_GET_AUTOCOMPLETE_SEARCH_PHRASE);
            if (SearchHelper.isSearchState(searchText) && isSearchPhrasesState()) {
                showPhrasesSearchState(autoCompleteSearchPhrases);
            }
        } else if (requestCode == COMMAND_REMOVE_SEARCH_PHRASE) {
            List<Long> removedIds = (List<Long>) data.getSerializable(CommandExecutor.BUNDLE_REMOVE_SEARCH_IDS);
            if (removedIds != null && !removedIds.isEmpty()) {
                long removedId = removedIds.get(0);
                SearchItemViewModel searchItemViewModel;
                for (int i = 0; i < recentSearchItems.size(); i++) {
                    searchItemViewModel = recentSearchItems.get(i);
                    if (removedId == recentSearchItems.get(i).getId()) {
                        getView().onRecentSearchItemRemoved(searchItemViewModel);
                        return;
                    }
                }
            }
        } else if (requestCode == COMMAND_CLEAR_SEARCH_PHRASES) {
            getView().onSearchPhrasesHistoryRemoved();
        } else if (requestCode == COMMAND_SEARCH) {
            SearchResponseHuge response = data.getParcelable(SearchResponseHuge.BUNDLE_SEARCH);
            Timber.d(response.items.toString());
        }
    }

    private boolean isSearchPhrasesState() {
        final SearchPhrasesView view = getView();
        return view != null ? view.isPhrasesFieldHasFocus() : false;
    }

    private void processResponse() {
        boolean getResponse = this.getResponse.getAndSet(true);
        if (getResponse) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (isSearchPhrasesState() && !SearchHelper.isSearchState(searchText)) {
                        showPhrasesDefaultState();
                    }
                }
            });
        }
    }

    private List<SearchItemViewModel> transformSearchItems(List<SearchItem> searchItems) {
        List<SearchItemViewModel> viewModels = new ArrayList<>(searchItems.size());
        for (int i = 0; i < searchItems.size(); i++) {
            viewModels.add(new SearchItemViewModel(searchItems.get(i)));
        }
        return viewModels;
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public String getSearchText() {
        return searchText;
    }

    public interface SearchPhrasesView extends IView {
        void showPhrasesDefaultState(List<SearchItemViewModel> recentSearches, PopularResponse popularResponse);

        void showPhrasesDefaultState(List<SearchItemViewModel> recentSearchItems);

        void showPhrasesSearchState(GetAutoCompleteSearchPhrasesResponse response);

        void onRecentSearchItemRemoved(SearchItemViewModel searchItem);

        void onSearchPhrasesHistoryRemoved();

        void showPhrasesAutocomplete();

        boolean isPhrasesFieldHasFocus();

        boolean updateStatusLocation();
    }

}
