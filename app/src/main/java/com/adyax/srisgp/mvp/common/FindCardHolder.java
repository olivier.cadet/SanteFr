package com.adyax.srisgp.mvp.common;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.databinding.ItemAroundMeBinding;
import com.adyax.srisgp.utils.PermissionUtils;
import com.bumptech.glide.Glide;

/**
 * Created by anton.kobylianskiy on 11/3/16.
 */

public class FindCardHolder extends SearchItemHolder {
    public SearchItemHuge searchItemHuge;
    private ItemAroundMeBinding binding;
    private final IRepository repository;

    public FindCardHolder(ItemAroundMeBinding binding, boolean mapMode, IRepository repository) {
        super(binding.getRoot());
        this.binding = binding;
        this.repository = repository;

        if (mapMode) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) binding.cardViewRoot.getContext().getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);

            int screenWidth = displayMetrics.widthPixels;
            ViewGroup.LayoutParams layoutParams = binding.cardViewRoot.getLayoutParams();
            layoutParams.width = (int) (screenWidth * 0.9);
            binding.cardViewRoot.setLayoutParams(layoutParams);
        }
    }

    @Override
    public void bind(SearchItemHuge itemHuge, FindCardListener listener) {
        this.searchItemHuge = itemHuge;

        itemView.setOnClickListener(v -> {
            listener.onItemClicked(itemHuge);
        });

        final String getIcon_url = itemHuge.getIcon_url();
        if (getIcon_url != null) {
            Glide.with(binding.ivIcon.getContext())
                    .load(getIcon_url)
                    .error(itemHuge.getIcon().getResourceId())
                    .into(binding.ivIcon);
        } else {
            binding.ivIcon.setImageResource(itemHuge.getIcon().getResourceId());
        }
        binding.tvTitle.setText(itemHuge.getName());
        binding.tvLocation.setText(itemHuge.getAddress());

        if (!TextUtils.isEmpty(itemHuge.subtitle)) {
            binding.tvSubtitle.setText(itemHuge.subtitle);
            binding.tvSubtitle.setVisibility(View.VISIBLE);
        } else {
            binding.tvSubtitle.setVisibility(View.GONE);
        }
        // TODO has been commented out for debugging
//            binding.getRoot().setOnClickListener(view -> presenter.clickAdvert(itemHuge));
        binding.getRoot().setTag(itemHuge);

        final String[] additionalLabels = itemHuge.getAdditionalLabels();
        binding.additionalLabelsContainer.removeAllViews();

        if(additionalLabels != null && additionalLabels.length > 0) {
            binding.additionalLabelsContainer.setVisibility(View.VISIBLE);

            final LayoutInflater layoutInflater = LayoutInflater.from(itemView.getContext());
            for(String additionalLabel: additionalLabels) {
                View view = layoutInflater.inflate(
                        R.layout.view_additional_label,
                        binding.additionalLabelsContainer,
                        false
                );
                TextView textView = view.findViewById(R.id.tvAdditionalLabel);
                textView.setText(additionalLabel);

                binding.additionalLabelsContainer.addView(view);
            }
        }
        else {
            binding.additionalLabelsContainer.setVisibility(View.GONE);
        }

        if (itemHuge.isOpen()) {
            binding.open.setVisibility(View.VISIBLE);
            int labelId;
            if (itemHuge.isTemporarilyClosed()) {
                labelId = R.string.around_me_temporarily_closed;
            }
            else {
                labelId = R.string.around_me_open;
            }
            binding.openTextView.setText(labelId);
        } else {
            binding.open.setVisibility(View.GONE);
        }

        if (itemHuge.isHandicapped()) {
            if (!itemHuge.isHandicappedCrossed()) {
                binding.handAccess.setVisibility(View.VISIBLE);
                binding.handAccessTextView.setPaintFlags(binding.openTextView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            } else {
                binding.handAccess.setVisibility(View.GONE);
                //binding.handAccessTextView.setPaintFlags(binding.openTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        } else {
            binding.handAccess.setVisibility(View.GONE);
        }


        String distance = itemHuge.getDistance();
        binding.distance.setVisibility((distance != null && !distance.isEmpty()) ? View.VISIBLE : View.GONE);
        if (distance != null) {
            binding.distanceTextView.setText(distance);
        }

        if (itemHuge.getPhones2() != null && itemHuge.getPhones2().length > 0 && !itemHuge.getPhones2()[0].isEmpty()) {
            binding.ivAction.setImageResource(R.drawable.appeler_copier);
            binding.tvAction.setText(String.format("%s %s", binding.getRoot().getContext().getString(R.string.search_results_call), itemHuge.getPhones2()[0]));
            binding.llRead.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onCallClicked(searchItemHuge);
                }
            });
        } else {
            binding.ivAction.setImageResource(R.drawable.ic_fiche_green_24dp);
            binding.tvAction.setText(binding.getRoot().getContext().getString(R.string.around_me_more));
            binding.llRead.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClicked(itemHuge);
                }
            });
        }
        binding.ivFavorite.setSelected(itemHuge.isFavorite());
        binding.ivFavorite.setOnClickListener(v -> {
            if (listener != null) {
                listener.onFavoriteClicked(searchItemHuge);
            }
        });

        if (PermissionUtils.hasPermissionsForRequestingLocation(binding.getRoot().getContext()) &&
                PermissionUtils.isGPSEnabled(binding.getRoot().getContext())) {
            if (itemHuge.isContentMaintenanceMode()) {
                binding.route.setVisibility(View.GONE);
            } else {
                binding.route.setVisibility(View.VISIBLE);
            }
        } else {
            binding.route.setVisibility(View.GONE);
        }

        binding.route.setOnClickListener(v -> {
            if (listener != null) {
                listener.onRouteClicked(searchItemHuge);
            }
        });
        if (itemHuge.isContentMaintenanceMode()) {
            binding.maintenanceTv.setVisibility(View.VISIBLE);
            binding.maintenanceTv.setText(repository.getConfig().getContentMaintenance().getTeaserActionText());
            binding.ivFavorite.setVisibility(View.GONE);
            binding.llRead.setVisibility(View.GONE);
            if (BuildConfig.DEBUG) {
                binding.getRoot().setBackgroundColor(binding.getRoot().getResources().getColor(R.color.bt_light_green));
            }
        } else {
            binding.maintenanceTv.setVisibility(View.GONE);
            binding.ivFavorite.setVisibility(View.VISIBLE);
            binding.llRead.setVisibility(View.VISIBLE);
            if (BuildConfig.DEBUG) {
                binding.getRoot().setBackgroundColor(binding.getRoot().getResources().getColor(R.color.material_white));
            }
        }

        /**
         * Temps d'attente
         * SAN-2855 Waiting time
         */
        if (itemHuge.isWaitingEnabled()) {
            binding.llwaitingTime.setVisibility(View.VISIBLE);
            binding.waitingTimeTitle.setText(itemHuge.getWaitingTimelabel());
            binding.waitingTimeText.setText(itemHuge.getWaitingTimeKnowMore());

            int colorTmp = R.color.waiting_time_level_0;
            int labelColor = Integer.parseInt(itemHuge.getWaitingTimelabelColor());
            if (labelColor == 1) {
                colorTmp = R.color.waiting_time_level_1;
            } else if (labelColor == 2) {
                colorTmp = R.color.waiting_time_level_2;
            } else if (labelColor == 3) {
                colorTmp = R.color.waiting_time_level_3;
            }

            binding.waitingTimeTitle.setTextColor(binding.getRoot().getResources().getColor(colorTmp));
            binding.waitingTimeIcon.getDrawable().setTint(binding.getRoot().getResources().getColor(colorTmp));
        }
    }
}
