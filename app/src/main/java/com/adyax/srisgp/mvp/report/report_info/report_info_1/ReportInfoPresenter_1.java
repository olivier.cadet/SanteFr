package com.adyax.srisgp.mvp.report.report_info.report_info_1;

import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.command.feedback.ReportInfoFormValue;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportInfoPresenter_1 extends Presenter<ReportInfoPresenter_1.DummyView> {

    public ReportBuilder getBuilder() {
        return builder;
    }

    private ReportBuilder builder=new ReportBuilder();

    public interface DummyView extends IView {
    }

    @Inject
    public ReportInfoPresenter_1() {
    }

}
