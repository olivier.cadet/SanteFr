package com.adyax.srisgp.mvp.searchhome.alerts.notification;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.db.IDataBaseHelper;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 11/2/16.
 */


public class DeleteNotificationBroadcastReceiver extends BroadcastReceiver {

    @Inject
    IDataBaseHelper dataBaseHelper;

    @Override
    public void onReceive(Context context, Intent intent) {
        App.getApplicationComponent().inject(this);
        try {
            final AlertItem alertItem=intent.getParcelableExtra(AlertItem.BUNDLE_NAME);
            if(alertItem!=null) {
                ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(alertItem.getIdForNotify());
                dataBaseHelper.setPushReadAlert(alertItem.getId());
            }
        }catch(Exception e){

        }
    }

}
