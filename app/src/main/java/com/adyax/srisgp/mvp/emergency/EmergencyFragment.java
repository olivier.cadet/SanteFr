package com.adyax.srisgp.mvp.emergency;

import android.app.Activity;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentEmergencyBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class EmergencyFragment extends ViewFragment implements EmergencyPresenter.EmergencyView {

    @Inject
    EmergencyPresenter emergencyPresenter;
    private FragmentEmergencyBinding binding;

    public EmergencyFragment() {
    }

    public static Fragment newInstance(EmergencyArg emergencyArg) {
        EmergencyFragment fragment = new EmergencyFragment();
        Bundle args = new Bundle();
        args.putParcelable(EmergencyArg.BUNDLE, emergencyArg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        emergencyPresenter.initEmergencyArg(getArguments().getParcelable(EmergencyArg.BUNDLE));
        binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_emergency, container, false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.report);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.signal_waiting_time);
        binding.serviceSpinner.setObjects(emergencyPresenter.getEmergencyArg().getServices());
        binding.serviceSpinner.setSelection(emergencyPresenter.getEmergencyArg().getServiceSpinnerIndex());
        binding.valueSpinner.setObjects(emergencyPresenter.getEmergencyArg().getValues());
        binding.valueSpinner.setSelection(
                emergencyPresenter.getEmergencyArg().getValueSpinnerIndex(emergencyPresenter.getRepository().
                        getVote(emergencyPresenter.getEmergencyArg())));
        binding.validateButton.setOnClickListener(view1 ->
                emergencyPresenter.clickSend(emergencyPresenter.getEmergencyCommand(
                        binding.serviceSpinner.getValue(), binding.valueSpinner.getValue())));
    }

    @NonNull
    @Override
    public EmergencyPresenter getPresenter() {
        return emergencyPresenter;
    }

    @Override
    public void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void closeFragment() {
////        getActivity().getSupportFragmentManager().beginTransaction().remove(EmergencyFragment.this).commit();
//////        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.app_name));
//////        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);
////        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_close_white_24dp);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getActivity().onBackPressed();
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }

    @Override
    public void showMessage(String errorMessage) {
        Snackbar.make(getView(), errorMessage, Snackbar.LENGTH_LONG).show();
    }

}
