package com.adyax.srisgp.mvp.ask_login;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.StringRes;

/**
 * Created by SUVOROV on 6/21/17.
 */

public class AskLoginArg implements Parcelable {

    public static final String BUNDLE = "AskLoginArg";

    @DrawableRes
    private int imageResId;

    @StringRes
    private int titleResId;

    @StringRes
    private int bodyResId;

    public AskLoginArg(@DrawableRes int imageResId, @StringRes int titleResId, @StringRes int bodyResId) {
        this.imageResId = imageResId;
        this.titleResId = titleResId;
        this.bodyResId = bodyResId;
    }

    protected AskLoginArg(Parcel in) {
        imageResId = in.readInt();
        titleResId = in.readInt();
        bodyResId = in.readInt();
    }

    public static final Creator<AskLoginArg> CREATOR = new Creator<AskLoginArg>() {
        @Override
        public AskLoginArg createFromParcel(Parcel in) {
            return new AskLoginArg(in);
        }

        @Override
        public AskLoginArg[] newArray(int size) {
            return new AskLoginArg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imageResId);
        parcel.writeInt(titleResId);
        parcel.writeInt(bodyResId);
    }

    public @DrawableRes int getImageResId() {
        return imageResId;
    }

    public @StringRes int getTitleResId() {
        return titleResId;
    }

    public @StringRes int getBodyResId() {
        return bodyResId;
    }
}
