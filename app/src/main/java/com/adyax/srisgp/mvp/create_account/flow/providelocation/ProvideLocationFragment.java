package com.adyax.srisgp.mvp.create_account.flow.providelocation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.CityResult;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.PermissionUtils;
import com.google.android.gms.common.api.Status;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by anton.kobylianskiy on 10/5/16.
 */

public class ProvideLocationFragment extends ViewFragment implements ProvideLocationPresenter.ProvideLocationView {
    public static final String INTENT_CITY = "city";

    private static final int REQUEST_LOCATION_PERMISSION = 1;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.iBtnClear)
    ImageButton iBtnClear;
    @BindView(R.id.rlSearchInput)
    RelativeLayout rlSearchInput;
    @BindView(R.id.rvAutocomplete)
    RecyclerView rvAutocomplete;

    @Inject
    ProvideLocationPresenter provideLocationPresenter;

    private CitiesAdapter citiesAdapter;
    private OnPromptLocationSettingsListener listener;

    private ProgressDialog progressDialog;
    private CitiesResponse citiesResponse;

    private boolean aroundMeClicked = false;

    public interface OnPromptLocationSettingsListener {
        void onPromptLocation(Status status);
    }

    public static ProvideLocationFragment newInstance() {
        ProvideLocationFragment fragment = new ProvideLocationFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (OnPromptLocationSettingsListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_provide_location, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    public void onPromptSettingsResult(boolean success) {
        if (success) {
            provideLocationPresenter.onPromptSettingSuccess(aroundMeClicked);
        } else {
            showLocationNotEnabled();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                provideLocationPresenter.onRequestCurrentLocation(aroundMeClicked);
            } else {
                if (PermissionUtils.isNotAskAgainLocationPermission(getActivity())) {
                    showLocationPermissionNotAskAgain();
                } else {
                    showLocationPermissionDenied();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void setResultAndFinish(String city) {
        Intent intent = new Intent();
        if (citiesResponse != null && city != null) {
            intent.putExtra(INTENT_CITY, new CityResult(city, citiesResponse.getId(city)));
        } else {
//            intent.putExtra(INTENT_CITY, new CityResult(null, 0));
            boolean bSameCity;
            if (city != null) {
                bSameCity = TextUtils.equals(etSearch.getText(), city);
                etSearch.setText(city);
            } else
                bSameCity = false;
            hideLoading();
            if (!bSameCity) {
                showPleaseEnterFrenchTown();
            }
            return;
        }
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
    }

    private void showPleaseEnterFrenchTown() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.please_eneter_french_town)
                .setPositiveButton(R.string.yes, (dialog, which) -> {

                })
                .show();
    }

    private void bindViews() {
        iBtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });

        citiesAdapter = new CitiesAdapter(new CitiesAdapter.OnItemClickedLister() {
            @Override
            public void onSelectAutocomplete(String searchText) {
                setResultAndFinish(searchText);
            }

            @Override
            public void onProvideLocationClicked() {
                aroundMeClicked = false;
                if (!hasPermissionsForRequestingLocation()) {
                    requestLocationPermissions();
                } else {
                    provideLocationPresenter.onRequestCurrentLocation(false);
                }
            }

            @Override
            public void onAroundMeClicked() {
                etSearch.setText("");
                aroundMeClicked = true;
                if (!hasPermissionsForRequestingLocation()) {
                    requestLocationPermissions();
                } else {
                    provideLocationPresenter.onRequestCurrentLocation(true);
                }
            }

            @Override
            public boolean hasLocationPermissions() {
                LocationManager service = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
                return service.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && hasPermissionsForRequestingLocation();
            }
        });

        rvAutocomplete.setHasFixedSize(true);
        rvAutocomplete.setAdapter(citiesAdapter);
        rvAutocomplete.setLayoutManager(new LinearLayoutManager(getContext()));

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                iBtnClear.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                provideLocationPresenter.requestCities(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void showEnableLocationDialog(Status status) {
        listener.onPromptLocation(status);
    }

    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void setCurrentLocation(String city) {
        setResultAndFinish(city);
    }

    @Override
    public void showSearchState(CitiesResponse response) {
        this.citiesResponse = response;
        citiesAdapter.showSearchState(Arrays.asList(response.getCites()));
    }

    private boolean hasPermissionsForRequestingLocation() {
        return ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermissions() {
        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_LOCATION_PERMISSION);
    }

    @Override
    public synchronized void showLoading() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
        }
        progressDialog.show();
    }

    @Override
    public synchronized void hideLoading() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.hide();
            }
            progressDialog = null;
        }
    }

    @Override
    public void onEnableLocationButtonStateChanged() {
        citiesAdapter.onEnableLocationButtonStateChanged();
    }

    @Override
    public void showDefaultState(boolean bShowAlertDialog) {
        citiesAdapter.showDefaultState();
        if (bShowAlertDialog) {
            showPleaseEnterFrenchTown();
        }
    }

    @Override
    public String getSearchFieldValue() {
        return etSearch.getText().toString();
    }

    @Override
    public void showError(String description) {

    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return provideLocationPresenter;
    }
}
