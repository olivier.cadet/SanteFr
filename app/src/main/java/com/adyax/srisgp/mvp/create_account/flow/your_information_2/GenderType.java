package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import android.content.Context;

import com.adyax.srisgp.R;

import java.util.ArrayList;

/**
 * Created by SUVOROV on 9/28/16.
 */

public enum GenderType {

    indifferent(R.string.indifferent),
    female(R.string.female),//Femme
    male(R.string.male),
    empty(-1);

    private int stringResId;

    GenderType(int stringResId) {
        this.stringResId = stringResId;
    }

    public static GenderType valueOf(int stringRes) {
        for (GenderType genderType : GenderType.values()) {
            if (genderType.stringResId == stringRes)
                return genderType;
        }

        return empty;
    }

    public static GenderType valueOf(Context context, String genderText) {
        if(genderText!=null) {
            for (GenderType genderType : GenderType.values()) {
                if (genderType.stringResId != -1 && context.getString(genderType.stringResId).equals(genderText)
                        || genderType.name().equals(genderText)) {
                    return genderType;
                }
            }
        }
        return empty;
    }

    public int getStringResId() {
        return stringResId;
    }

    public static ArrayList<String> getStringArray(Context context) {
        ArrayList<String> strings = new ArrayList<>();
        for (GenderType genderType : GenderType.values()) {
            if (genderType != empty)
                strings.add(context.getString(genderType.stringResId));
        }
        return strings;
    }

    public static CharSequence[] getCharSequence(Context context) {
        ArrayList<String> strings = getStringArray(context);
        return strings.toArray(new CharSequence[strings.size()]);
    }

    public static int getSize() {
        return values().length - 1;
    }

    public static int getOrdinal(Context context, String gender) {
        if(gender!=null) {
            for (int i = 0; i < values().length; i++) {
                GenderType genderType = values()[i];
                final int stringResId = genderType.getStringResId();
                if (stringResId!=-1&& context.getString(stringResId).equals(gender) || genderType.name().equals(gender)) {
                    return i;
                }
            }
        }
        return -1;
    }
}
