package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import java.util.Calendar;

/**
 * Created by SUVOROV on 10/2/16.
 */

public class YearsHelper {

    private static String[] years;

    private static void init() {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        final int maxYears = year - 1900 - 18 + 1;
        years = new String[maxYears];
        for (int i = year - 18, j = 0; j < maxYears; i--, j++) {
            years[j] = String.valueOf(i);
        }
    }

    public static String[] getYearsArray() {
        if (years == null)
            init();
        return years;
    }

    public static int getPosition(String year) {
        for (int i = 0; i < getYearsArray().length; i++) {
            if (getYearsArray()[i].equals(year))
                return i;
        }
        return -1;
    }
}
