package com.adyax.srisgp.mvp.contact;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.adyax.srisgp.R;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

/**
 * Created by anton.kobylianskiy on 11/21/16.
 */

public class ContactActivity extends MaintenanceBaseActivity {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ContactActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        if (savedInstanceState == null) {
            replaceFragment(R.id.fragment_container, ContactFragment.newInstance());
        }
    }
}
