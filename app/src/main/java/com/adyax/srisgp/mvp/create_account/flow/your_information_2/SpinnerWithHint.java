package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.adyax.srisgp.R;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by SUVOROV on 10/1/16.
 */

public class SpinnerWithHint extends androidx.appcompat.widget.AppCompatSpinner {
    private static final String INSTANCESTATE = "INSTANCESTATE";
    private static final String SHOWHINT = "SHOWHINT";
    private static final String ITEMSELECTED = "ITEMSELECTED";
    public static final String POSITION = "position";
    private boolean showHint = false;
    private String hint = "";
    private boolean itemSelected = false;
    private int dropdownResource = android.R.layout.simple_dropdown_item_1line;
    private int viewResource = R.layout.spinner_item;
    private Context context;
    private HintAdapter adapter;
    private CharSequence[] entires;
    private OnItemSelectedListener listener;
    private int position;
    private int hintTextSize = -1;

    public SpinnerWithHint(Context context, AttributeSet attrs) {
        super(context, attrs);
        buildView(context, attrs);
    }

    public SpinnerWithHint(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        buildView(context, attrs);
    }

    public SpinnerWithHint(Context context, AttributeSet attrs, int defStyle, int mode) {
        super(context, attrs, defStyle, mode);
        buildView(context, attrs);
    }

    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }

    private void buildView(Context context, AttributeSet attrs) {
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SpinnerWithHint);
        final int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.SpinnerWithHint_showPrompt:
                    showHint = a.getBoolean(attr, false);
                    break;
                case R.styleable.SpinnerWithHint_hint:
                    hint = a.getString(attr);
                    break;
                case R.styleable.SpinnerWithHint_dropdownResource:
                    dropdownResource = a.getResourceId(attr, android.R.layout.simple_spinner_item);
                    break;
                case R.styleable.SpinnerWithHint_viewResource:
                    viewResource = a.getResourceId(attr, android.R.layout.simple_spinner_item);
                    break;
                case R.styleable.SpinnerWithHint_entries:
                    entires = a.getTextArray(attr);
                    break;
            }
        }
        a.recycle();
        adapter = new HintAdapter(context, dropdownResource);
        if (entires != null) {
            ArrayList list = new ArrayList();
            list.addAll(Arrays.asList(entires));
            this.adapter.objects = list;
        }
        this.setAdapter(adapter);
    }

//    public void setObjects(ArrayList objects){
//        if(showHint){
//            this.adapter.objects.add(hint);
//        }
//        this.adapter.objects.addAll(objects);
//        this.adapter.notifyDataSetChanged();
//    }

    public void setObjects(String[] objects) {
        ArrayList list = new ArrayList();
//        if(showHint){
//            //only add the hint if we want it.
//            list.add(hint);
//        }
        list.addAll(Arrays.asList(objects));
        this.adapter.objects = list;
        this.adapter.notifyDataSetChanged();
    }

    public boolean isItemSelected() {
        return itemSelected;
    }


    @Override
    public void setSelection(int position) {
        //we are restoring from state.. if the hint exists, get rid of it.
//        showHint = false;
        itemSelected = true;
        this.position = position;
//        if(this.adapter.objects.get(0).equals(hint)){
//            this.adapter.objects.remove(0);
        this.adapter.notifyDataSetChanged();
//            super.setSelection(position);
//        }else{
        super.setSelection(position);
//        }
        if (listener != null)
            listener.onItemSelected(null, null, position, 0);
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(INSTANCESTATE, super.onSaveInstanceState());
        bundle.putBoolean(SHOWHINT, showHint);
        bundle.putBoolean(ITEMSELECTED, itemSelected);
        bundle.putInt(POSITION, position);
        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            showHint = bundle.getBoolean(SHOWHINT);
            itemSelected = bundle.getBoolean(ITEMSELECTED);
            position = bundle.getInt(POSITION);
//            if(!showHint && this.adapter.objects.get(0).equals(hint)){
//                //we are restoring after user has gotten rid of hint..
//                //the hint is there so lets get rid of it.
//                this.adapter.objects.remove(0);
//                this.adapter.notifyDataSetChanged();
//            }
            state = bundle.getParcelable(INSTANCESTATE);
        }
        super.onRestoreInstanceState(state);

    }

    public String getValue() {
        if (itemSelected) {
            final Object selectedItem = getSelectedItem();
            if (selectedItem != null) {
                return selectedItem.toString();
            }
        }
        return null;
    }

    public boolean isEmpty() {
        //return getValue()==null;
        return !itemSelected;
    }

    public void showHint() {
        itemSelected = false;
        adapter.notifyDataSetChanged();
    }

    public void showHint(int textSize) {
        hintTextSize = textSize;
        itemSelected = false;
        adapter.notifyDataSetChanged();
    }

    private class HintAdapter extends ArrayAdapter {
        protected ArrayList<String> objects = new ArrayList();

        public HintAdapter(Context context, int resource) {
            super(context, resource);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public String getItem(int position) {
            return objects.get(position);
//            return "1test tets test test test test tets test test 2test test tets test test test test tets test 3test test test tets test test test";//objects.get(position);
        }

        @Override
        public int getCount() {
            int size = objects.size();
//            if(showHint){
//                if(size>0) {
//                    return size - 1;
//                }
//            }
            return size;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            requestFocus();
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(viewResource, null);
            }
            if (showHint && /*objects.get(0).equals(hint)*/!itemSelected/*position>=objects.size()*/) {
//                ((TextView)convertView).setTypeface(null, Typeface.ITALIC);
                ((TextView) convertView).setText(hint);//android:textSize="@dimen/text_size_account"
                ((TextView) convertView).setTextColor(context.getResources().getColor(R.color.hint_color_account));
            } else {
                ((TextView) convertView).setText(objects.get(position));
                ((TextView) convertView).setTextColor(context.getResources().getColor(R.color.material_black));
//                ((TextView) convertView).setTextSize(19);
            }
            if (hintTextSize > 0) {
                ((TextView) convertView).setTextSize(TypedValue.COMPLEX_UNIT_PX, hintTextSize);
            }
            convertView.invalidate();
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent){
                requestFocus();
                if (convertView == null) {
                    convertView = LayoutInflater.from(context).inflate(dropdownResource, null);
                }
//                //this fires when the user expands the dropdown...set the hint to false now..
//                showHint = false;
//                //have to select something at this point.
//                itemSelected = true;
//                //is the first object still the hint?
//                if(objects.get(0).equals(hint)){
//                    //it was... Remove it and notify adapter of change
//                    objects.remove(0);
//                    notifyDataSetChanged();
//                }else{
//                    //okay we are back in the expanded view lets build list
//                    if(position<getCount())
//                            /*We do this to prevent index out of bounds
//                            when this fires after we remove the item from the list
//                            but this is still firing from the first press*/
                            ((TextView) convertView).setText(objects.get(position));
            ((TextView) convertView).setWidth(parent.getWidth());
            Float dimension = context.getResources().getDimension(R.dimen.spinner_popup_height);
//            ((TextView) convertView).setHeight(Utils.dp2px(context.getResources(), dimension.intValue()));
            ((TextView) convertView).setHeight(dimension.intValue());
//                }
            return convertView;
        }

//        @Override
//        public View getDropDownView(int position, View convertView, ViewGroup parent){
//                if (convertView == null) {
//                    convertView = LayoutInflater.from(context).inflate(dropdownResource, null);
//                }
//                //this fires when the user expands the dropdown...set the hint to false now..
//                showHint = false;
//                //have to select something at this point.
//                itemSelected = true;
//                //is the first object still the hint?
//                if(objects.get(0).equals(hint)){
//                    //it was... Remove it and notify adapter of change
//                    objects.remove(0);
//                    notifyDataSetChanged();
//                }else{
//                    //okay we are back in the expanded view lets build list
//                    if(position<getCount())
//                            /*We do this to prevent index out of bounds
//                            when this fires after we remove the item from the list
//                            but this is still firing from the first press*/
//                            ((TextView) convertView).setText(objects.get(position));
//                }
//            return convertView;
//        }

    }
}
