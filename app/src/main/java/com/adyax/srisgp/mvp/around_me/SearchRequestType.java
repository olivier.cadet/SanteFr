package com.adyax.srisgp.mvp.around_me;

/**
 * Created by SUVOROV on 11/14/16.
 */

public enum SearchRequestType {
    IMMEDIATELY,
    IMMEDIATELY_WITHOUT_FILTERS,
    START,
    DEFAULT
}
