package com.adyax.srisgp.mvp.around_me;

import com.adyax.srisgp.data.net.command.SearchFilter;

/**
 * Created by Kirill on 24.10.16.
 */

public interface SearchFilterListener {
    void onFilterChanged(SearchFilter searchFilter);
}
