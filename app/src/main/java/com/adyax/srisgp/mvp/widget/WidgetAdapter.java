package com.adyax.srisgp.mvp.widget;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

/**
 * Created by NW on 18/04/2019.
 */

public class WidgetAdapter {
    public interface OnItemClickedListener {
        void onWidgetInfoUpdated(String key, String value);

        void onWidgetSubmitted();
    }
}
