package com.adyax.srisgp.mvp.create_account.flow.your_information;

import java.util.Calendar;

/**
 * Created by SUVOROV on 10/2/16.
 */

public class YearsHelper {

    private String[] years;

    public YearsHelper() {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        final int maxYears = year - 1900 - 18+1;
        years=new String[maxYears];
//        for(int i=1900, j=0;i<year-18;i++, j++){
//            years[j]=String.valueOf(i);
//        }
        for(int i=year-18, j=0;j<maxYears;i--, j++){
            years[j]=String.valueOf(i);
        }
    }

    public String[] getYearsArray() {
        return years;
    }
}
