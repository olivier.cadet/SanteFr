package com.adyax.srisgp.mvp.favorite;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.CustomTabLayout;
import com.adyax.srisgp.presentation.CustomViewPager;
import com.adyax.srisgp.utils.ViewUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public class YourFavoritesFragment extends ViewFragment
        implements YourFavoritesPresenter.YourFavoritesView, FavoritesFragment.FavoritesListener {

    @Inject
    YourFavoritesPresenter yourFavoritesPresenter;

    @BindView(R.id.tabLayout)
    CustomTabLayout tabLayout;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.btnRemove)
    Button btnRemove;

    private boolean emptyState = false;
    private boolean editMode = false;

    private FavoritesPagerAdapter pagerAdapter;
    private Set<Long> selectedItems = new HashSet<>();
    private Set<Long> loadingItems = new HashSet<>();

    private ActionMode actionMode;

    public static YourFavoritesFragment newInstance() {
        new TrackerHelper().addScreenView(TrackerLevel.FAVORITES, TrackerAT.MES_FAVORIS);
        YourFavoritesFragment fragment = new YourFavoritesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_your_favorites, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    private void bindViews(View view) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.favorite_your_favorites);

        pager.setPagingEnabled(false);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int currentPage;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
                if (actionMode != null) {
                    editMode = false;
                    actionMode.finish();
                    selectedItems.clear();
                    ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).updateAll();
                }

                FavoritesFragment previousFragment = (FavoritesFragment) pagerAdapter.getRegisteredFragment(currentPage);
                if (previousFragment != null) {
                    previousFragment.onPageChanged();
                }

                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        pagerAdapter = new FavoritesPagerAdapter(getChildFragmentManager(), getContext());
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(pagerAdapter);

        tabLayout.setSwipeListener(new CustomTabLayout.SwipeListener() {
            @Override
            public void onLeft() {
                if (!emptyState) {
                    int prevItem = pager.getCurrentItem() - 1;
                    if (prevItem >= 0) {
                        pager.setCurrentItem(prevItem);
                    }
                }
            }

            @Override
            public void onRight() {
                if (!emptyState) {
                    int nextItem = pager.getCurrentItem() + 1;
                    if (nextItem >= 0) {
                        pager.setCurrentItem(nextItem);
                    }
                }
            }
        });

        disableTabs();
        tabLayout.setupWithViewPager(pager);
        createTabIcons();

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selectedItems.isEmpty()) {
                    for (long id : selectedItems) {
                        loadingItems.add(id);
                        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).addLoadingItem(id);
                    }
                    // TODO IGetIUrl=null
                    yourFavoritesPresenter.onFavoriteRemove(new ArrayList<>(selectedItems), null);
                }
            }
        });
    }

    private void createTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabOne.setText(pagerAdapter.getPageTitle(0).toString().toUpperCase());
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabTwo.setText(pagerAdapter.getPageTitle(1).toString().toUpperCase());
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.item_favorite_text_view, null);
        tabThree.setText(pagerAdapter.getPageTitle(2).toString().toUpperCase());
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                editMode = true;
                ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).updateAll();

                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new ActionMode.Callback() {
                    @Override
                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                        btnRemove.setVisibility(View.VISIBLE);
                        MenuInflater inflater = mode.getMenuInflater();
                        inflater.inflate(R.menu.select_all, menu);
                        return true;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                        menu.clear();
                        MenuInflater inflater = mode.getMenuInflater();
                        boolean selectedAll = ((FavoritesFragment) pagerAdapter
                                .getRegisteredFragment(pager.getCurrentItem())).getItems().size() == selectedItems.size();
                        if (selectedAll) {
                            inflater.inflate(R.menu.unselect_all, menu);
                        } else {
                            inflater.inflate(R.menu.select_all, menu);
                        }
                        return true;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_select_all:
                                List<FavoritesItem> items = ((FavoritesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).getItems();
                                for (FavoritesItem favorite : items) {
                                    selectedItems.add(favorite.nid);
                                }
                                ((FavoritesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).updateAll();
                                mode.invalidate();
                                return true;
                            case R.id.action_unselect_all:
                                selectedItems.clear();
                                ((FavoritesFragment) pagerAdapter
                                        .getRegisteredFragment(pager.getCurrentItem())).updateAll();
                                mode.invalidate();
                                return true;
                        }
                        return false;
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode mode) {
                        btnRemove.setVisibility(View.GONE);
                        editMode = false;
                        selectedItems.clear();
                        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).changeEditMode();
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        FavoritesFragment currentFragment = (FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment != null && currentFragment.getItems().size() > 0) {
            inflater.inflate(R.menu.remove, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onRemoved(long id) {
        selectedItems.remove(id);
        loadingItems.remove(id);
        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(0)).removeNotification(id);
        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(1)).removeNotification(id);
        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(2)).removeNotification(id);
    }

    @Override
    public void onRemove(FavoritesItem item) {
        long id = item.nid;
        loadingItems.add(id);
        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).addLoadingItem(id);

        List<Long> items = new ArrayList<>(1);
        items.add(item.nid);
        yourFavoritesPresenter.onFavoriteRemove(items, item);
    }

    @Override
    public void onItemCountChanged(FavoritesFragment fragment, int count) {
        Fragment currentFragment = pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment == fragment) {
            invalidateOptionsMenu();
            if (editMode && actionMode != null) {
                actionMode.invalidate();
            }
        }
    }

    private void invalidateOptionsMenu() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().invalidateOptionsMenu();
    }

    @Override
    public void changeSelection(FavoritesItem item) {
        if (selectedItems.contains(item.nid)) {
            selectedItems.remove(item.nid);
        } else {
            selectedItems.add(item.nid);
        }
        ((FavoritesFragment) pagerAdapter.getRegisteredFragment(pager.getCurrentItem())).changeSelectionMode(item);
    }

    @Override
    public void disableTabs() {
        emptyState = true;
        pager.setCurrentItem(0);
        ViewUtils.disableTabLayout(getContext(), tabLayout);
    }

    @Override
    public void enableTabs() {
        emptyState = false;
        ViewUtils.enableTabLayout(getContext(), tabLayout);
    }

    @Override
    public void onAllItemsRemoved(FavoritesFragment favoritesFragment) {
        Fragment currentFragment = pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment == favoritesFragment) {
            if (actionMode != null) {
                editMode = false;
                actionMode.finish();
                invalidateOptionsMenu();
            }
            pager.setCurrentItem(0);
        }
    }

    @Override
    public void onCloseEditMode() {
        if (actionMode != null) {
            editMode = false;
            actionMode.finish();
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean isEditMode() {
        return editMode;
    }

    @Override
    public boolean isLoadingItem(FavoritesItem item) {
        return loadingItems.contains(item.nid);
    }

    @Override
    public boolean isSelectedItem(FavoritesItem item) {
        return selectedItems.contains(item.nid);
    }

    @NonNull
    @Override
    public YourFavoritesPresenter getPresenter() {
        return yourFavoritesPresenter;
    }
}
