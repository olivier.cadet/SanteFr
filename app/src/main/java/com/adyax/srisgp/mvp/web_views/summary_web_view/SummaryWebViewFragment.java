package com.adyax.srisgp.mvp.web_views.summary_web_view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.tags.AnchorItem;
import com.adyax.srisgp.databinding.FragmentSummaryWebViewBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.web_views.WebViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class SummaryWebViewFragment extends ViewFragment implements SummaryWebViewPresenter.CreateRecipeView {

    @Inject
    SummaryWebViewPresenter summaryWebViewPresenter;
    private FragmentSummaryWebViewBinding binding;
    private AnchorResponse anchorResponse;

    public SummaryWebViewFragment() {
    }

    public static Fragment newInstance(AnchorResponse anchorResponse) {
        SummaryWebViewFragment fragment = new SummaryWebViewFragment();
        Bundle args = new Bundle();
        args.putParcelable(AnchorResponse.ANCHOR_RESPONSE, anchorResponse);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        anchorResponse = getArguments().getParcelable(AnchorResponse.ANCHOR_RESPONSE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_summary_web_view, container, false);
        binding.recyclerViewSummary.setLayoutManager(
                new LinearLayoutManager(binding.recyclerViewSummary.getContext(), LinearLayoutManager.VERTICAL, false));
//        binding.advertRecycleview.addItemDecoration(new DividerItemDecoration(binding.advertRecycleview.getContext(), R.drawable.advert_ine_divider))
        binding.relatedSummary.setOnClickListener(view -> sendBackToTop());
        binding.commentOnThisPageSummary.setOnClickListener(view -> sendBackToBottom());
        binding.getRoot().setOnTouchListener((v, event) -> true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.summary);
        if(anchorResponse!=null) {
            binding.titleSummary.setText(anchorResponse.getTitle());
            final SummaryWebRecyclerViewAdapter adapter = new SummaryWebRecyclerViewAdapter(anchorResponse);
            if(!adapter.is_en_relation()) {
                binding.relatedSummary.setVisibility(View.GONE);
            }
            binding.recyclerViewSummary.setAdapter(adapter);
            binding.recyclerViewSummary.addOnItemTouchListener(
                    new RecyclerItemClickListener(getContext(), binding.recyclerViewSummary, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            try {
                                sendBackArchor(anchorResponse.anchors[position]);
//                        getActivity().getSupportFragmentManager().popBackStack();
                            } catch (Exception e) {

                            }
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {
                        }
                    })
            );
        }
    }

    public void sendBackArchor(AnchorItem anchor) {
        Intent intent = new Intent();
        intent.putExtra(AnchorItem.ANCHOR_ITEM, anchor);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        closeFragment();
    }

    public void sendBackToTop() {
        Intent intent = new Intent();
        if(anchorResponse.containRelated()){
            intent.putExtra(AnchorItem.ANCHOR_ITEM, anchorResponse.getRelatedItem());
        }else {
            intent.putExtra(WebViewFragment.DIRECT_COMMAND, WebViewFragment.TO_TOP);
        }
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        closeFragment();
    }

    public void sendBackToBottom() {
        Intent intent = new Intent();
        intent.putExtra(WebViewFragment.DIRECT_COMMAND, WebViewFragment.TO_BOTTOM);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        closeFragment();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().beginTransaction().remove(SummaryWebViewFragment.this).commit();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.app_name));
        ((AppCompatActivity)getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);
    }

    @NonNull
    @Override
    public SummaryWebViewPresenter getPresenter() {
        return summaryWebViewPresenter;
    }

    @Override
    public boolean onBackPressed() {
        super.onBackPressed();
        closeFragment();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch(itemId){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        final MenuItem menuItem = menu.findItem(R.id.action_bookmark);
//        menuItem.setVisible(false);
//        getActivity().invalidateOptionsMenu();
//    }

    @Override
    public void  onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_bookmark).setVisible(false);
        menu.findItem(R.id.action_share).setVisible(false);
        getActivity().invalidateOptionsMenu();
    }

}
