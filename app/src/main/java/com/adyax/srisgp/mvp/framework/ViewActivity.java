package com.adyax.srisgp.mvp.framework;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adyax.srisgp.presentation.BaseActivity;

public abstract class ViewActivity extends BaseActivity implements IView {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectHere();
        getPresenter().attachView(this);
        getPresenter().restoreState(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().registerListeners();
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().unregisterListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getPresenter().detachView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getPresenter().saveState(outState);
    }

    @NonNull
    @Override
    public Context getContext() {
        return this;
    }

    @NonNull
    abstract public IPresenter getPresenter();

    abstract public void injectHere();
}
