package com.adyax.srisgp.mvp.authentication.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.widget.Toast;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.authentication.forgotten_password.TypeForrgotten;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.GooglePlusPresenter;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class LoginPresenter extends GooglePlusPresenter<LoginPresenter.CreateRecipeView>
        implements FacebookCallback<LoginResult>, GoogleApiClient.OnConnectionFailedListener {

    public interface CreateRecipeView extends IView {
        void onFail(ErrorResponse errorResponse);

        void onSuccess();

        void startActivityForResult(Intent intent, int requestCode);

        FragmentActivity getActivity();
    }

    private FragmentFactory fragmentFactory;

    @Inject
    public LoginPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
//        if (!BuildConfig.DEV_STATUS) {
            registerFacebookLoginButton();
            initGoogle();
//        }
    }

    public void moveToNext() {
        finishActivity();
    }

    private void finishActivity() {
        ((Activity) getContext()).finish();
        ((Activity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    public void clickForgottenPassword(String email) {
        if (email != null) {
            fragmentFactory.startForgottenPasswordFragment(getContext(), TypeForrgotten.first_screen, email);
        } else {
            fragmentFactory.startForgottenPasswordFragment(getContext(), TypeForrgotten.first_screen);
        }
    }

    public void gotoCreateAccount() {
        finishActivity();
        ((Activity) getContext()).startActivity(CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART));
    }

    public void sendFakeRegistration() {
        final IViewPager activity = (IViewPager) getContext();
        activity.getCreateAccountHelper().sendLoginCommand(getContext(), getAppReceiver());
    }

    /**
     * onActivityResult
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);

        if (twitterLoginButton != null) {
            try {
                twitterLoginButton.onActivityResult(requestCode, resultCode, data);
            } catch (IllegalStateException e) {

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Facebook
     */

    private LoginButton facebookLoginButton;
    private CallbackManager callbackManager;

    private void registerFacebookLoginButton() {
        if (getView() != null) {
            if (facebookLoginButton == null) {
                facebookLoginButton = new LoginButton(getContext());
                facebookLoginButton.setPermissions("email");
            }

            callbackManager = CallbackManager.Factory.create();
            facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    // App code
                }

                @Override
                public void onCancel() {
                    // App code
                }

                @Override
                public void onError(FacebookException exception) {
                    // App code
                }
            });
        }
    }

    public void facebookClick() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            if (facebookLoginButton != null) {
                Timber.d("Facebook login clicked");
                facebookLoginButton.performClick();
            }
        } else {
            getProfileData(accessToken);
        }
    }

    private void getProfileData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    if (getView() == null)
                        return;

                    String id = object.optString("id");
                    String name = object.optString("name");
                    String email = object.optString("email");
                    Timber.d("*** Facebook login ***\n" +
                            "id = %s\n" +
                            "name = %s\n" +
                            "email = %s\n", id, name, email);

                    final IViewPager activity = (IViewPager) getContext();
                    Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
                    if (emailResult == null) {
                        activity.getCreateAccountHelper().setSsoRegister(Sso_type.sgp_sso_facebook,
                                id, email, accessToken.getToken());

                        activity.getCreateAccountHelper().sendLoginSsoCommand(getContext(), getAppReceiver(), false);
                    } else {
                        Toast.makeText(getContext(), emailResult, Toast.LENGTH_SHORT).show();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Facebook callback
     */

    @Override
    public void onSuccess(LoginResult loginResult) {
        Timber.d("Facebook login success");
        getProfileData(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
        Timber.d("Facebook login canceled");
    }

    @Override
    public void onError(FacebookException error) {
        Timber.d("Facebook login error");
    }

    /**
     * Twitter
     */

    private TwitterLoginButton twitterLoginButton;

    public void registerTwitterLoginButton(TwitterLoginButton twitterLoginButton) {
        this.twitterLoginButton = twitterLoginButton;
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;

                TwitterCore.getInstance().getApiClient(session).getAccountService()
                        .verifyCredentials(true, true, false).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        User user = result.data;

                        String id = user.idStr;
                        String name = user.name;
                        String email = user.email;
                        String screenName = user.screenName;
                        Timber.d("*** Twitter login ***\n" +
                                "id = %s\n" +
                                "name = %s\n" +
                                "email = %s\n", id, name, email);

                        TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
                        twitterAuthClient.requestEmail(session, new Callback<String>() {
                            @Override
                            public void success(Result<String> result) {
                                String email = result.data;
                                Timber.d("Twitter email = %s", email);
                                if (email == null) {
                                    Timber.d("Twitter email NULL ! :( using default 'twitterConnect@sante.fr'");
                                    email = "twitterConnect@sante.fr";
                                }

                                loginTwitter(id, email, token, secret, screenName);
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Toast.makeText(getContext(), "Twitter error", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void failure(TwitterException exception) {

                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getContext(), "Twitter error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loginTwitter(String id, String email, String token, String twitterTokenSecret, String screenName) {
        final IViewPager activity = (IViewPager) getContext();
        Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
        if (emailResult == null) {
            activity.getCreateAccountHelper().setTwitterSsoRegister(id, email, token, twitterTokenSecret, screenName);
            activity.getCreateAccountHelper().sendLoginSsoCommand(getContext(), getAppReceiver(), false);
        } else {
            Toast.makeText(getContext(), emailResult, Toast.LENGTH_SHORT).show();
        }
    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    public void onMentionedLegalesClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.INFORMATION), -1);
    }

    public void googleClick() {
        if (getView() != null /*&& googleApiClient.isConnected()*/ /*&& !BuildConfig.DEV_STATUS)*/) {
            startConnectGooglePlus();
//            Auth.GoogleSignInApi.signOut(googleApiClient);
//            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
//            getView().startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * AppReceiver
     */

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        getView().onSuccess();
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try {
            if (errorMessage != null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        } catch (Exception e) {

        }
    }
}
