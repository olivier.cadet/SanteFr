package com.adyax.srisgp.mvp.report.report_find.report_find_user;

import android.os.Bundle;

import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportFindUserPresenter_1 extends PresenterAppReceiver<ReportFindUserPresenter_1.Report3View> {

    private WebViewArg webViewArg;

    public ReportBuilder getBuilder() {
        return builder;
    }

    private ReportBuilder builder=new ReportBuilder();

    public void setWebViewArg(WebViewArg webViewArg) {
        this.webViewArg = webViewArg;
    }

    public WebViewArg getWebViewArg() {
        return webViewArg;
    }

    public interface Report3View extends IView {
        void update(FeedbackFormResponse feedbackFormResponse);
    }

    @Inject
    public ReportFindUserPresenter_1() {
    }

    public void init() {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new GetFormCommand(InapLinkHelper.FIND_CONTENT_ERROR_FORM_NAME, getWebViewArg().getShortNode()), ExecutionService.GET_FORM);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch(requestCode){
            case ExecutionService.GET_FORM:
                final FeedbackFormResponse feedbackFormResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                getView().update(feedbackFormResponse);
                break;
        }
    }

}
