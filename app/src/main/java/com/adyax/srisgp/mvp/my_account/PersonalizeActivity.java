package com.adyax.srisgp.mvp.my_account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.IntIdValuePair;
import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.ActivityPersonalizeBinding;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by Kirill on 22.11.16.
 */

public class PersonalizeActivity extends MaintenanceBaseActivity implements PersonalizePresenter.PersonalizeView {

    @Inject
    PersonalizePresenter presenter;

    private ActivityPersonalizeBinding binding;

    public static void start(Activity activity) {
        new TrackerHelper().addScreenView(TrackerLevel.ACCOUNT_MANAGEMENT, TrackerAT.MES_PREFERENCES_SANTE);
        Intent intent = new Intent(activity, PersonalizeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        presenter.attachView(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_personalize);

        binding.cllManualInterests.setDefaultState(true);

        binding.cllManualInterests.setCheckboxListener((isChecked, item) -> {
            if (!isChecked) {
                presenter.switchUnchecked(item);
            }
        });
        binding.cllInterests.setCheckboxListener((isChecked, item) -> {
            if (isChecked) {
                presenter.switchChecked(item);
            }
        });
        binding.cllKeywords.setCheckboxListener((isChecked, item) -> {
            if (isChecked) {
                presenter.switchChecked(item);
            }
        });

//        binding.labelBottom.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.labelBottom, false);

        setNotificationSettingsListener();

        initActionBar();
        presenter.onViewCreated();
    }

    @NonNull
    @Override
    public Context getContext() {
        return this;
    }

    private void initActionBar() {
        setSupportActionBar((Toolbar) binding.getRoot().findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(R.string.notification_your_notifications);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setNotificationSettingsListener() {
        binding.scReceiveMobile.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.updatePersonalize());
//        binding.scReceiveEmail.setOnCheckedChangeListener((buttonView, isChecked) ->{
//            presenter.setReceiveEmail(isChecked);
//            binding.llNotificationEmailSettings.setVisibility(isChecked ? View.VISIBLE : View.GONE);
//        });
//        binding.scReceiveEmailDaily.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.updatePersonalize());
//        binding.scReceiveEmailWeekly.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.updatePersonalize());
    }

    private void removeNotificationSettingsListener() {
        binding.scReceiveMobile.setOnCheckedChangeListener(null);
//        binding.scReceiveEmail.setOnCheckedChangeListener(null);
//        binding.scReceiveEmailDaily.setOnCheckedChangeListener(null);
//        binding.scReceiveEmailWeekly.setOnCheckedChangeListener(null);
    }

    @Override
    public void setManualInterests(ArrayList<IntIdValuePair> items) {
        if (items.isEmpty()) {
            binding.tvManualInterests.setVisibility(View.GONE);
            binding.cllManualInterests.setVisibility(View.GONE);
        } else {
            binding.tvManualInterests.setVisibility(View.VISIBLE);
            binding.cllManualInterests.setVisibility(View.VISIBLE);

            binding.cllManualInterests.setItems(items);
        }
    }

    @Override
    public void setInterests(ArrayList<IntIdValuePair> items) {
        if (items.isEmpty()) {
            binding.tvInterests.setVisibility(View.GONE);
            binding.cllInterests.setVisibility(View.GONE);
        } else {
            binding.tvInterests.setVisibility(View.VISIBLE);
            binding.cllInterests.setVisibility(View.VISIBLE);

            binding.cllInterests.setItems(items);
        }
    }

    @Override
    public void setKeywords(ArrayList<TermsItem> items) {
        if (items.isEmpty()) {
            binding.tvKeywords.setVisibility(View.GONE);
            binding.cllKeywords.setVisibility(View.GONE);
        } else {
            binding.tvKeywords.setVisibility(View.VISIBLE);
            binding.cllKeywords.setVisibility(View.VISIBLE);

            ArrayList<IntIdValuePair> pairs = new ArrayList<>();
            for (TermsItem item : items) {
                pairs.add(new IntIdValuePair((int) item.tid, item.name));
            }
            binding.cllKeywords.setItems(pairs);
        }
    }

    @Override
    public void setNotificationReceive(User user) {
        removeNotificationSettingsListener();

        binding.scReceiveMobile.setChecked(user.isNotifyByMobile());
//        binding.scReceiveEmail.setChecked(user.isNotifyByEmail());
//        binding.scReceiveEmailDaily.setChecked(user.isNotifyByEmailDaily());
//        binding.scReceiveEmailWeekly.setChecked(user.isNotifyByEmailWeekly());

//        binding.llNotificationEmailSettings.setVisibility(user.isNotifyByEmail() ? View.VISIBLE : View.GONE);

        setNotificationSettingsListener();
    }

    @Override
    public boolean isReceiveByMobileChecked() {
        return binding.scReceiveMobile.isChecked();
    }

//    @Override
//    public boolean isReceiveByEmailChecked() {
//        return binding.scReceiveEmail.isChecked();
//    }

    @Override
    public boolean isReceiveByEmailDailyChecked() {
//        return binding.scReceiveEmailDaily.isChecked();
        return false;
    }

    @Override
    public boolean isReceiveByEmailWeeklyChecked() {
//        return binding.scReceiveEmailWeekly.isChecked();
        return false;
    }
}
