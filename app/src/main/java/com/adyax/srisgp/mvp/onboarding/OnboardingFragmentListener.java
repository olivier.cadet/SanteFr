package com.adyax.srisgp.mvp.onboarding;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public interface OnboardingFragmentListener {
    void onNext();
    void onSkip();
    void onCreateAccountClicked();
}
