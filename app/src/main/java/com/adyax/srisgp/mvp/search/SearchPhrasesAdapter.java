package com.adyax.srisgp.mvp.search;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchPhrasesResponse;
import com.adyax.srisgp.data.net.response.tags.SearchResult;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/22/16.
 */
public class SearchPhrasesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_HEADER = 0;
    private static final int ITEM_RECENT_SEARCH = 1;
    private static final int ITEM_REMOVE_HISTORY = 2;
    private static final int ITEM_POPULAR = 3;
    private static final int ITEM_SUGGESTED = 4;
    private static final int ITEM_HISTORY = 5;

    private int currentState = SearchPhrasesPresenter.STATE_PHRASES_DEFAULT;

    private OnItemClickedListener onItemClickedListener;

    private List<SearchItemViewModel> recentSearchPhrases = new ArrayList<>();
    private List<String> popularPhrases = new ArrayList<>();

    private GetAutoCompleteSearchPhrasesResponse autocompleteResponse;

    public SearchPhrasesAdapter(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public void showDefaultState(List<SearchItemViewModel> recentSearchItems, List<String> popularPhrases) {
        currentState = SearchPhrasesPresenter.STATE_PHRASES_DEFAULT;
        this.recentSearchPhrases = recentSearchItems;
        this.popularPhrases = popularPhrases;
        notifyDataSetChanged();
    }

    public void showDefaultState(List<SearchItemViewModel> recentSearchItems) {
//        currentState = SearchPhrasesPresenter.STATE_PHRASES_DEFAULT;
        this.recentSearchPhrases = recentSearchItems;
        notifyDataSetChanged();
    }

    public void showSearchState(GetAutoCompleteSearchPhrasesResponse response) {
        currentState = SearchPhrasesPresenter.STATE_PHRASES_SEARCH;
        this.autocompleteResponse = response;
        notifyDataSetChanged();
    }

    public void onRemovedSearchEntry(SearchItemViewModel removedSearchItem) {
        SearchItemViewModel searchItem;
        for (int i = 0; i < recentSearchPhrases.size(); i++) {
            searchItem = recentSearchPhrases.get(i);
            if (searchItem.getId() == removedSearchItem.getId()) {
                searchItem.setRemoved(true);

                int notRemovedRecentSearchPhrases = getNotRemovedRecentSearchPhrases();
                if (notRemovedRecentSearchPhrases == 0) {
                    onRemovedSearchHistory();
                } else if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
                    notifyItemChanged(i + 1);
                }
                return;
            }
        }
    }

    private int getNotRemovedRecentSearchPhrases() {
        int count = 0;
        for (int i = 0; i < recentSearchPhrases.size(); i++) {
            if (!recentSearchPhrases.get(i).isRemoved()) {
                count++;
            }
        }
        return count;
    }

    public void onRemovedSearchHistory() {
        int recentPhrasesCount = recentSearchPhrases.size();
        recentSearchPhrases.clear();
        if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
            notifyItemRangeRemoved(0, recentPhrasesCount + 2);
        }
    }

    public void onSetProgressForRecentItem(SearchItemViewModel searchItem) {
        for (int i = 0; i < recentSearchPhrases.size(); i++) {
            if (searchItem.getId() == recentSearchPhrases.get(i).getId()) {
                recentSearchPhrases.get(i).setProgress(true);
                if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
                    notifyItemChanged(1 + i);
                }
            }
        }
    }

    public void onSetProgressForSearchHistory() {
        SearchItemViewModel searchItem;
        for (int i = 0; i < recentSearchPhrases.size(); i++) {
            searchItem = recentSearchPhrases.get(i);
            searchItem.setProgress(true);
        }

        if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
            notifyItemRangeChanged(1, recentSearchPhrases.size() + 1);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
            int recentBlock = getRecentBlockItemCount();
            if (position < recentBlock) {
                if (position == 0) {
                    return ITEM_HEADER;
                } else if (position == recentBlock - 1) {
                    return ITEM_REMOVE_HISTORY;
                }
                return ITEM_RECENT_SEARCH;
            } else {
                position = position - recentBlock;
                if (position == 0) {
                    return ITEM_HEADER;
                }
                return ITEM_POPULAR;
            }
        } else if (currentState == SearchPhrasesPresenter.STATE_PHRASES_SEARCH) {
            if (isHistory() && position == 0) {
                return ITEM_HEADER;
            }
            final int autocompleteHistoryItemCount = getAutocompleteHistoryItemCount();
            if (position < autocompleteHistoryItemCount) {
                return ITEM_HISTORY;
            } else {
                int autocompleteSuggestedItemCount = getAutocompleteSuggestedItemCount();
                if (isHistory()) {
                    if (!isSuggestion()) {
                        position = position - autocompleteHistoryItemCount;
                        if (isHistory() && position == 0) {
                            return ITEM_HEADER;
                        }
                        if (position < autocompleteSuggestedItemCount) {
                            return ITEM_POPULAR;
                        }
                        return ITEM_SUGGESTED;
                    } else {
                        position = position - autocompleteHistoryItemCount;
                        if (isHistory() && position == 0) {
                            return ITEM_HEADER;
                        }
                        if (position < autocompleteSuggestedItemCount) {
                            return ITEM_POPULAR;
                        }
                        return ITEM_SUGGESTED;
                    }
                } else {
                    if (position < autocompleteSuggestedItemCount) {
                        return ITEM_POPULAR;
                    } else {
                        position = position - autocompleteSuggestedItemCount;
                        if (position == 0) {
                            return ITEM_HEADER;
                        }
                        return ITEM_SUGGESTED;
                    }
                }
            }
        }
        throw new RuntimeException("No such state");
    }

    private int getAutocompleteResultItemCount() {
        if (autocompleteResponse.search_results.size() == 0) {
            return 0;
        }
        return autocompleteResponse.search_results.size() + 1;
    }

    private int getAutocompleteSuggestedItemCount() {
        final int size = autocompleteResponse.phrase_suggestions.size();
        return isHistory() ? (!isSuggestion() ? size : size + 1) : size;
    }

    private int getAutocompleteHistoryItemCount() {
        return isHistory() ? autocompleteResponse.history.size() + 1 : 0;
    }

    private int getRecentBlockItemCount() {
        if (recentSearchPhrases.isEmpty()) {
            return 0;
        }
        return recentSearchPhrases.size() + 2;
    }

    private int getPopularPhraseItemCount() {
        if (popularPhrases.isEmpty()) {
            return 0;
        }
        return popularPhrases.size() + 1;
    }

    private boolean isRecentBlock(int position) {
        return position < getRecentBlockItemCount();
    }

    @Override
    public int getItemCount() {
        if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
            return getRecentBlockItemCount() + getPopularPhraseItemCount();
        } else if (currentState == SearchPhrasesPresenter.STATE_PHRASES_SEARCH) {
            return getAutocompleteResultItemCount() +
                    getAutocompleteSuggestedItemCount() + getAutocompleteHistoryItemCount();
        }
        throw new RuntimeException("No such state");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM_HEADER:
                View headerView = inflater.inflate(R.layout.item_search_header, parent, false);
                viewHolder = new HeaderViewHolder(headerView);
                break;
            case ITEM_RECENT_SEARCH:
                View recentView = inflater.inflate(R.layout.item_search_recent_search, parent, false);
                viewHolder = new RecentViewHolder(recentView);
                break;
            case ITEM_REMOVE_HISTORY:
                View removeView = inflater.inflate(R.layout.item_search_remove_history, parent, false);
                viewHolder = new RemoveViewHolder(removeView);
                break;
            case ITEM_POPULAR:
                View popularView = inflater.inflate(R.layout.item_search_popular, parent, false);
                viewHolder = new PopularViewHolder(popularView);
                break;
            case ITEM_SUGGESTED:
                View suggestedView = inflater.inflate(R.layout.item_search_suggested, parent, false);
                viewHolder = new SuggestedViewHolder(suggestedView);
                break;
            case ITEM_HISTORY:
                View historyView = inflater.inflate(R.layout.item_search_popular, parent, false);
                viewHolder = new PopularViewHolder(historyView).setHistoryIcon();
                break;
            default:
                throw new RuntimeException("No such view type!");
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_HEADER:
                String headerText = "";
                if (isHistory()) {
                    if (!isSuggestion() && position > 0) {
                        headerText = App.getAppContext().getString(R.string.search_suggested_content);
                    } else {
                        headerText = App.getAppContext().getString(position == 0 ?
                                R.string.search_last_searches : R.string.suggested_searches);
                    }
                } else {
                    if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
                        if (isRecentBlock(position)) {
                            headerText = App.getAppContext().getString(R.string.search_last_searches);
                        } else {
                            headerText = App.getAppContext().getString(R.string.search_popular_now);
                        }
                    } else if (currentState == SearchPhrasesPresenter.STATE_PHRASES_SEARCH) {
                        headerText = App.getAppContext().getString(R.string.search_suggested_content);
                    }
                }
                ((HeaderViewHolder) holder).bind(headerText);
                break;
            case ITEM_RECENT_SEARCH:
                ((RecentViewHolder) holder).bind(getRecentSearch(position), onItemClickedListener);
                break;
            case ITEM_REMOVE_HISTORY:
                ((RemoveViewHolder) holder).bind(onItemClickedListener);
                break;
            case ITEM_POPULAR:
                if (currentState == SearchPhrasesPresenter.STATE_PHRASES_DEFAULT) {
                    ((PopularViewHolder) holder).bind(getPopularSearch(position), onItemClickedListener);
                } else if (currentState == SearchPhrasesPresenter.STATE_PHRASES_SEARCH) {
                    ((PopularViewHolder) holder).bindSuggestion(getSuggestedItem(position), onItemClickedListener);
                }
                break;
            case ITEM_SUGGESTED:
                ((SuggestedViewHolder) holder).bind(getResultItem(position), onItemClickedListener);
                break;
            case ITEM_HISTORY:
                ((PopularViewHolder) holder).bind(getHistoryItem(position), onItemClickedListener);
                break;
        }
    }

    private SearchItemViewModel getRecentSearch(int position) {
        return recentSearchPhrases.get(position - 1);
    }

    private String getPopularSearch(int position) {
        return popularPhrases.get(position - getRecentBlockItemCount() - 1);
    }

    private SearchResult getResultItem(int position) {
        int i = position - getAutocompleteSuggestedItemCount() - 1;
        if (isHistory() && !isSuggestion()) {
            i -= 2;
        }
        if (BuildConfig.DEBUG) {
            if (i >= autocompleteResponse.search_results.size()) {
                return autocompleteResponse.search_results.get(autocompleteResponse.search_results.size() - 1);
            }
        }
        return autocompleteResponse.search_results.get(i);
    }

    private String getSuggestedItem(int position) {
        return autocompleteResponse.phrase_suggestions.get(position -
                getAutocompleteHistoryItemCount() - (isHistory() ? 1 : 0));
    }

    private String getHistoryItem(int position) {
        return autocompleteResponse.history.get(position - 1);
    }

    private boolean isHistory() {
//        return false;
        return autocompleteResponse != null && autocompleteResponse.history != null && !autocompleteResponse.history.isEmpty();
    }

    private boolean isSuggestion() {
//        return false;
        return autocompleteResponse != null && autocompleteResponse.phrase_suggestions != null && !autocompleteResponse.phrase_suggestions.isEmpty();
    }

    static class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvHeader)
        TextView tvHeader;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(String text) {
            tvHeader.setText(text);
        }
    }

    static class RecentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvRemoved)
        TextView tvRemoved;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.ivRemove)
        ImageView ivRemove;
        @BindView(R.id.viewDivider)
        View viewDivider;

        RecentViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(SearchItemViewModel searchItem, OnItemClickedListener listener) {
            if (searchItem.isRemoved()) {
                setVisibilityForAllViews(false);
                tvRemoved.setVisibility(View.VISIBLE);
                tvTitle.setVisibility(View.INVISIBLE);
                ivIcon.setVisibility(View.INVISIBLE);
                itemView.setOnClickListener(null);
                tvTitle.setText("");
            } else {
                setVisibilityForAllViews(true);
                tvRemoved.setVisibility(View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onSelectAutocomplete(searchItem.getPhrase());
                    }
                });
                tvTitle.setText(searchItem.getPhrase());

                if (searchItem.isProgress()) {
                    ivRemove.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                } else {
                    progressBar.setVisibility(View.GONE);
                    viewDivider.setVisibility(View.VISIBLE);
                }
            }

            ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRemoveSearchEntry(searchItem);
                }
            });
        }

        private void setVisibilityForAllViews(boolean visible) {
            ViewGroup root = (ViewGroup) itemView;
            for (int i = 0; i < root.getChildCount(); i++) {
                root.getChildAt(i).setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        }
    }

    static class RemoveViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        RemoveViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(OnItemClickedListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRemoveSearchPhrasesHistory();
                }
            });
        }
    }

    static class PopularViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        PopularViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public PopularViewHolder setHistoryIcon() {
            ivIcon.setImageResource(R.drawable.ic_historique_24dp);
            return this;
        }

        private void bind(String popularPhrase, OnItemClickedListener listener) {
            tvTitle.setText(popularPhrase);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelectAutocomplete(popularPhrase);
                }
            });
        }

        private void bindSuggestion(String phraseSuggestion, OnItemClickedListener listener) {
            tvTitle.setText(phraseSuggestion);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSelectAutocomplete(phraseSuggestion);
                }
            });
        }
    }

    static class SuggestedViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.ivOpen)
        ImageView ivOpen;

        SuggestedViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(SearchResult searchResult, OnItemClickedListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onOpenWebView(searchResult);
                }
            });
            tvTitle.setText(searchResult.title);
        }
    }

    public interface OnItemClickedListener {
        void onRemoveSearchEntry(SearchItemViewModel searchItem);

        void onRemoveSearchPhrasesHistory();

        void onSelectAutocomplete(String searchText);

        void onOpenWebView(SearchResult searchResult);
    }
}
