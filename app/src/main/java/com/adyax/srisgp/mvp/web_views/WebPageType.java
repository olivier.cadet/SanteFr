package com.adyax.srisgp.mvp.web_views;

/**
 * Created by SUVOROV on 11/10/16.
 */

public enum WebPageType {
    SERVER,
    // it is usual page from inet
    STANDART,
    // it is transfer in standard page
    TRANSFER,
    // it is page without favorite menu
    SIMPLE,
    STATIC_PAGE,
    EXTERNAL_REQUIRING_POSTURL,
    CGU_PAGE,
    DISABLE_ALL,
    ERROR,
    UNKNOWN
}
