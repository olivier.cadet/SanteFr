package com.adyax.srisgp.mvp.authentication.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.base.ITestUser;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.adyax.srisgp.databinding.FragmentLoginBinding;
import com.adyax.srisgp.mvp.create_account.CreateAccountHelper;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class LoginFragment extends ViewFragment implements LoginPresenter.CreateRecipeView {

    @Inject
    LoginPresenter presenter;

    private FragmentLoginBinding binding;

    private AlertDialog errorDialog;

    public static Fragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    public String getEmailValue() {
        return binding.emailCreateAccount.getText().toString().trim();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        binding.emailCreateAccount.addTextChangedListener(watcher);
        binding.passwordCreateAccount.addTextChangedListener(watcher);

        if (BuildConfig.DEBUG) {
            binding.emailCreateAccount.setText(ITestUser.DEFAULT_DEBUG_USER);
            binding.passwordCreateAccount.setText(ITestUser.DEFAULT_DEBUG_PASS);
        }

//        binding.submitMessage.setOnClickListener(v -> {
//            presenter.onPrivacyPolicyClicked();
//        });
        LoginFragment.setSubmitSpan(binding.submitMessage, true);

        binding.validateCreateAccount.setOnClickListener(view -> {
            final IViewPager activity = (IViewPager) getActivity();
            Integer errStr = activity.getCreateAccountHelper().setEmailValidate(
                    getEmailValue());
            if (errStr == null) {
                errStr = activity.getCreateAccountHelper().setPasswordValidate(
                        binding.passwordCreateAccount.getText().toString(), true);
                presenter.sendFakeRegistration();
            } else {
                binding.emailCreateAccount.setError(getString(errStr));
                binding.emailCreateAccount.requestFocus();
            }
        });
        binding.eyeSwitchCreateAccount.setOnCheckedChangeListener((compoundButton, b) -> {
            binding.passwordCreateAccount.setInputType(b ?
                    InputType.TYPE_CLASS_TEXT : InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            if (!b) {
                binding.passwordCreateAccount.setTypeface(Typeface.DEFAULT);
            }
            final int textLength = binding.passwordCreateAccount.getText().length();
            binding.passwordCreateAccount.setSelection(textLength, textLength);
        });
        binding.forgotPassword.setOnClickListener(view -> presenter.clickForgottenPassword(null));
        binding.gotoCreateAccount.setOnClickListener(view -> presenter.gotoCreateAccount());
        binding.passwordCreateAccount.setOnFocusChangeListener((v, hasFocus) -> {
            binding.eyeSwitchCreateAccount.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
        });

        binding.facebookCreateAccount.setOnClickListener(v -> presenter.facebookClick());
        presenter.registerTwitterLoginButton(binding.twitterCreateAccount);
        binding.googleCreateAccount.setOnClickListener(v -> presenter.googleClick());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.authentication_title);
    }

    @NonNull
    @Override
    public LoginPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        if (errorResponse.isEmailError()) {
            binding.emailCreateAccount.setError(errorResponse.getEmailItem().getLocalizeErrorMessage(getContext()));
            binding.emailCreateAccount.requestFocus();
        } else if (errorResponse.isError()) {
            switch (errorResponse.getErrorItem().getErrorCode()) {
                case ErrorItem.USER_HAS_NOT_BEEN_REGISTERED_VIA_SSO:
                    new AlertDialog.Builder(getContext())
                            .setMessage(errorResponse.getErrorMessage(getContext()))
                            .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                            .show();
                    break;
                case ErrorItem.OTHER_SOCIAL:
//                    Snackbar.make(binding.getRoot(), errorResponse.getErrorMessage(getContext()), Snackbar.LENGTH_SHORT).show();
                    new AlertDialog.Builder(getContext())
                            .setMessage(errorResponse.getErrorMessage(getContext()))
                            .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
                            .show();
                    break;
                case ErrorItem.EMAIL_HAS_NOT_BEEN_CONFIRMED:
                    badEmail(errorResponse);
                    break;
                case ErrorItem.ACCOUNT_TEMPORARY_BLOCKED:
//                    badEmail(errorResponse);
                    if (errorDialog == null) {
                        String text = getString(R.string.login_blocked_account_text);
                        String clickablePart = getString(R.string.login_blocked_account_clickable);

                        String fullMessage = text + " " + clickablePart;
                        SpannableString messageSpannable = new SpannableString(fullMessage);
                        ClickableSpan clickableSpan = new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                if (errorDialog != null) {
                                    errorDialog.dismiss();
                                }

                                presenter.clickForgottenPassword(getEmailValue());
                            }
                        };

                        messageSpannable.setSpan(clickableSpan, text.length() + 1, fullMessage.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                        errorDialog = new AlertDialog.Builder(getContext())
                                .setMessage(messageSpannable)
                                .setPositiveButton(R.string.ok_title, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();

                        ((TextView) errorDialog.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
                    } else {
                        errorDialog.show();
                    }
                    break;
                case ErrorItem.WRONG_USER_NAME:
                    badEmail(errorResponse);
                    break;
                case ErrorItem.WRONG_PASSWORD:
                    binding.passwordCreateAccount.setError(errorResponse.getErrorItem().getLocalizeErrorMessage(getContext()));
                    binding.passwordCreateAccount.requestFocus();
                    break;
            }
        } else {
            // it insurance in case from server we got message " missing password "
            final String errorMessage = errorResponse.getErrorMessage(null);
            if (errorMessage != null && errorMessage.contains("password")) {
                binding.passwordCreateAccount.setError(getString(R.string.incorrect_password_login));
                binding.passwordCreateAccount.requestFocus();
            }
            if (BuildConfig.DEBUG) {
                if (errorResponse.isAnyError()) {
                    Snackbar snackbar = Snackbar.make(binding.getRoot(), errorResponse.getErrorMessage(null), Snackbar.LENGTH_SHORT);
                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
            }
        }
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    private void badEmail(ErrorResponse errorResponse) {
        binding.emailCreateAccount.setError(errorResponse.getErrorItem().getLocalizeErrorMessage(getContext()));
//        SpannableString ss = new SpannableString("Android is a Software stack");
//        ClickableSpan clickableSpan = new ClickableSpan() {
//            @Override
//            public void onClick(View textView) {
//                getView().showContextMenu();
//            }
//        };
//        ss.setSpan(clickableSpan, 0, 20, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//
////        int ecolor = R.color.authentication_hint; // whatever color you want
////        String estring = "Please enter a valid email address";
////        ForegroundColorSpan fgcspan = new ForegroundColorSpan(ecolor);
////        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(estring);
////        ssbuilder.setSpan(fgcspan, 0, estring.length(), 0);
//        binding.emailCreateAccount.setLinksClickable(true);
//        binding.emailCreateAccount.setError(ss);
//        binding.emailCreateAccount.setMovementMethod(LinkTouchMovementMethod.getInstance());


        binding.emailCreateAccount.requestFocus();
    }


//    public abstract class TouchableSpan extends ClickableSpan {
//        private boolean mIsPressed;
//        private int mPressedBackgroundColor;
//        private int mNormalTextColor;
//        private int mPressedTextColor;
//
//        public TouchableSpan(int normalTextColor, int pressedTextColor, int pressedBackgroundColor) {
//            mNormalTextColor = normalTextColor;
//            mPressedTextColor = pressedTextColor;
//            mPressedBackgroundColor = pressedBackgroundColor;
//        }
//
//        public void setPressed(boolean isSelected) {
//            mIsPressed = isSelected;
//        }
//
//        @Override
//        public void updateDrawState(TextPaint ds) {
//            super.updateDrawState(ds);
//            ds.setColor(mIsPressed ? mPressedTextColor : mNormalTextColor);
//            ds.bgColor = mIsPressed ? mPressedBackgroundColor : 0xffeeeeee;
//            ds.setUnderlineText(false);
//        }
//    }
//
//    private class LinkTouchMovementMethod extends LinkMovementMethod {
//        private TouchableSpan mPressedSpan;
//
//        @Override
//        public boolean onTouchEvent(TextView textView, Spannable spannable, MotionEvent event) {
//            if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                mPressedSpan = getPressedSpan(textView, spannable, event);
//                if (mPressedSpan != null) {
//                    mPressedSpan.setPressed(true);
//                    Selection.setSelection(spannable, spannable.getSpanStart(mPressedSpan),
//                            spannable.getSpanEnd(mPressedSpan));
//                }
//            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
//                TouchableSpan touchedSpan = getPressedSpan(textView, spannable, event);
//                if (mPressedSpan != null && touchedSpan != mPressedSpan) {
//                    mPressedSpan.setPressed(false);
//                    mPressedSpan = null;
//                    Selection.removeSelection(spannable);
//                }
//            } else {
//                if (mPressedSpan != null) {
//                    mPressedSpan.setPressed(false);
//                    super.onTouchEvent(textView, spannable, event);
//                }
//                mPressedSpan = null;
//                Selection.removeSelection(spannable);
//            }
//            return true;
//        }
//
//        private TouchableSpan getPressedSpan(TextView textView, Spannable spannable, MotionEvent event) {
//
//            int x = (int) event.getX();
//            int y = (int) event.getY();
//
//            x -= textView.getTotalPaddingLeft();
//            y -= textView.getTotalPaddingTop();
//
//            x += textView.getScrollX();
//            y += textView.getScrollY();
//
//            Layout layout = textView.getLayout();
//            int line = layout.getLineForVertical(y);
//            int off = layout.getOffsetForHorizontal(line, x);
//
//            TouchableSpan[] link = spannable.getSpans(off, off, TouchableSpan.class);
//            TouchableSpan touchedSpan = null;
//            if (link.length > 0) {
//                touchedSpan = link[0];
//            }
//            return touchedSpan;
//        }
//
//    }

    @Override
    public void onSuccess() {
        presenter.moveToNext();
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            binding.validateCreateAccount.setEnabled(CreateAccountHelper.
                    getValidateButtonStatus(binding.emailCreateAccount, binding.passwordCreateAccount));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    public static void setSubmitSpan(TextView submitMessage, boolean isUpdateColor) {
        String text = submitMessage.getText().toString();
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);
        ClickableSpan legalesClickableSpan = new ClickableSpan() {

            @Override
            public void onClick(View view) {
                startWebViewActivity(submitMessage.getContext(), new WebViewArg(UrlExtras.INFORMATION), -1);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(submitMessage.getContext().getResources().getColor(R.color.material_black));
                textPaint.setUnderlineText(true);
            }

        };
        ClickableSpan privacyClickableSpan = new ClickableSpan() {

            @Override
            public void onClick(View view) {
                startWebViewActivity(submitMessage.getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                textPaint.setColor(submitMessage.getContext().getResources().getColor(R.color.material_black));
                textPaint.setUnderlineText(true);
            }

        };
        ClickableSpan regrulClickableSpan = new ClickableSpan() {

            @Override
            public void onClick(View view) {
                startWebViewActivity(submitMessage.getContext(), new WebViewArg(UrlExtras.PROTECTION), -1);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                    textPaint.setColor(submitMessage.getContext().getResources().getColor(R.color.material_black));
                    textPaint.setUnderlineText(true);
            }

        };
        String string = submitMessage.getContext().getString(R.string.login_span1);
        int start = text.indexOf(string);
        if (start > 0) {
            ssBuilder.setSpan(
                    legalesClickableSpan, // Span to add
                    start, // Start of the span (inclusive)
                    start + String.valueOf(string).length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        }
        string = submitMessage.getContext().getString(R.string.login_span2);
        start = text.indexOf(string);
        if (start > 0) {
            ssBuilder.setSpan(
                    privacyClickableSpan, // Span to add
                    start, // Start of the span (inclusive)
                    start + String.valueOf(string).length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        }
        string = submitMessage.getContext().getString(R.string.regrullink);
        start = text.indexOf(string);
        if (start > 0) {
            ssBuilder.setSpan(
                    regrulClickableSpan, // Span to add
                    start, // Start of the span (inclusive)
                    start + String.valueOf(string).length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        }
        submitMessage.setText(ssBuilder);
        submitMessage.setMovementMethod(LinkMovementMethod.getInstance());
        submitMessage.setHighlightColor(Color.BLUE);
    }

    private static void startWebViewActivity(Context context, WebViewArg webViewArg, int requestCode) {
        if (context instanceof FragmentActivity) {
            Intent intent = WebViewActivity.newIntent(context, webViewArg);
            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else
            throw new IllegalArgumentException("");
    }
}
