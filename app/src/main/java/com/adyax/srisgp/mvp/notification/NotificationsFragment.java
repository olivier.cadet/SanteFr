package com.adyax.srisgp.mvp.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.net.response.tags.NotificationUsers;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

import static android.app.Activity.RESULT_OK;

/**
 * Created by anton.kobylianskiy on 9/27/16.
 */

public class NotificationsFragment extends ViewFragment
        implements NotificationsPresenter.NotificationsView {

    private static final int REQUEST_NOTIFICATION = 124;
    private static final String ARGUMENT_STATUS_NOTIFICATION = "status_notification";
    private static final String ARGUMENT_PAGER_POSITION = "pager_position";

    @Inject
    NotificationsPresenter notificationsPresenter;
    @BindView(R.id.llEmptyView)
    LinearLayout llEmptyView;

    private NotificationsAdapter notificationsAdapter;
    private NotificationsListener listener;
    private int pagerPosition = -1;

    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;

    public static NotificationsFragment newInstance(StatusNotification statusNotification, int pagerPosition) {
        Bundle args = new Bundle();
        NotificationsFragment fragment = new NotificationsFragment();
        args.putSerializable(ARGUMENT_STATUS_NOTIFICATION, statusNotification);
        args.putInt(ARGUMENT_PAGER_POSITION, pagerPosition);
        fragment.setArguments(args);
        return fragment;
    }

    public void addLoadingItem(long id) {
        notificationsAdapter.notifyItemChangedById(id);
    }

    public void changeSelectionMode(NotificationItem item) {
        notificationsAdapter.changeSelectionMode(item);
    }

    public void onResetSwipeStates() {
        resetSwipeState(null);
    }

    private StatusNotification getStatusNotification() {
        return (StatusNotification) getArguments().getSerializable(ARGUMENT_STATUS_NOTIFICATION);
    }

    private int getPagerPosition() {
        return getArguments().getInt(ARGUMENT_PAGER_POSITION);
    }

    public List<NotificationItem> getItems() {
        return notificationsAdapter.getNotifications();
    }

    public void updateAll() {
        notificationsAdapter.notifyDataSetChanged();
    }

    public void changeEditMode() {
        notificationsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_NOTIFICATION) {
            if (resultCode == RESULT_OK) {
                long id = data.getLongExtra(NotificationFragment.INTENT_NOTIFICATION_ID, -1);
                notificationsAdapter.setAsRead(id);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.listener = (NotificationsListener) getParentFragment();
        this.pagerPosition = getPagerPosition();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        notificationsPresenter.requestNotifications(0);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    private void bindViews(View view) {
        notificationsAdapter = new NotificationsAdapter(new NotificationsAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(NotificationItem item) {
                notificationsPresenter.onItemClicked(item);
            }

            @Override
            public void onRemove(NotificationItem item) {
                listener.onRemove(item);
            }

            @Override
            public boolean isEditMode() {
                return listener.isEditMode();
            }

            @Override
            public void onSelect(NotificationItem item) {
                listener.changeSelection(item);
            }

            @Override
            public boolean isLoadingItem(NotificationItem item) {
                return listener.isLoadingItem(item);
            }

            @Override
            public boolean isSelectedItem(NotificationItem item) {
                return listener.isSelectedItem(item);
            }
        });

        notificationsAdapter.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {

            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {
                if (!moveToRight) {
                    for (int i = 0; i < notificationsAdapter.getItemCount(); i++) {
                        RecyclerView.ViewHolder viewHolder = rvNotifications.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null && viewHolder instanceof NotificationsAdapter.NotificationViewHolder) {
                            SwipeLayout currentSwipeLayout = ((NotificationsAdapter.NotificationViewHolder) viewHolder).swipeLayout;
                            if (currentSwipeLayout != swipeLayout) {
                                currentSwipeLayout.animateReset();
                            }
                        }
                    }
                }
            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }
        });

        notificationsAdapter.setSwipeCheckListener((swipeLayout) -> {
           return resetSwipeState(swipeLayout);
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvNotifications.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                notificationsPresenter.requestNotifications(totalItemsCount);
            }
        });
        rvNotifications.setHasFixedSize(true);
        rvNotifications.setLayoutManager(layoutManager);
        rvNotifications.setAdapter(notificationsAdapter);
    }

    private boolean resetSwipeState(SwipeLayout swipeLayout) {
        boolean isClosed = true;
        for (int i = 0; i < notificationsAdapter.getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = rvNotifications.findViewHolderForAdapterPosition(i);
            if (viewHolder != null && viewHolder instanceof NotificationsAdapter.NotificationViewHolder) {
                SwipeLayout currentSwipeLayout = ((NotificationsAdapter.NotificationViewHolder) viewHolder).swipeLayout;
                if (currentSwipeLayout != swipeLayout && currentSwipeLayout.getOffset() != 0) {
                    isClosed = false;
                    currentSwipeLayout.animateReset();
                }
            }
        }
        return isClosed;
    }


        @Override
    public void openNotificationScreen(NotificationItem item) {
        Intent intent = NotificationActivity.newIntent(getContext(), item);
        startActivityForResult(intent, REQUEST_NOTIFICATION);
    }

    @Override
    public void showEmptyView() {
        listener.updateState(pagerPosition, true);
        llEmptyView.setVisibility(View.VISIBLE);
        rvNotifications.setVisibility(View.GONE);
        if (getStatusNotification() == StatusNotification.all) {
            listener.disableTabs();
        }
    }

    @Override
    public void showNotifications(NotificationUsers notifications, boolean firstPage) {
        if (notifications == null || (notificationsAdapter.getItemCount() == 0 && notifications.items.length == 0)) {
            showEmptyView();
            listener.onAllItemsRemoved(this);
        } else {
            listener.updateState(pagerPosition, false);
            listener.enableTabs();
            llEmptyView.setVisibility(View.GONE);
            rvNotifications.setVisibility(View.VISIBLE);
            notificationsAdapter.addNotifications(new ArrayList<>(Arrays.asList(notifications.items)), firstPage);
            listener.onItemCountChanged(this, notificationsAdapter.getNotifications().size());
        }
    }

    public void removeNotification(long removedId) {
        notificationsAdapter.removeItem(removedId);
        listener.onItemCountChanged(this, notificationsAdapter.getNotifications().size());
        if (notificationsAdapter.getNotifications().isEmpty()) {
            notificationsPresenter.requestNotifications(0);
//            showEmptyView();
//            listener.onAllItemsRemoved();
        }
    }

    public boolean isEmpty() {
        return notificationsAdapter.getNotifications().isEmpty();
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        notificationsPresenter.setStatusNotification(getStatusNotification());
        super.onAttach(context);
    }

    @NonNull
    @Override
    public NotificationsPresenter getPresenter() {
        return notificationsPresenter;
    }

    public interface NotificationsListener {
        void onRemoved(long id);

        void onRemove(NotificationItem item);

        void changeSelection(NotificationItem item);

        boolean isLoadingItem(NotificationItem item);

        boolean isSelectedItem(NotificationItem item);

        boolean isEditMode();

        void onItemCountChanged(NotificationsFragment fragment, int count);

        void disableTabs();

        void enableTabs();

        void onAllItemsRemoved(NotificationsFragment fragment);

        void updateState(int pagerPosition, boolean empty);
    }
}
