package com.adyax.srisgp.mvp.around_me.listener;

/**
 * Created by SUVOROV on 8/3/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
