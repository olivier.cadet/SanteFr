package com.adyax.srisgp.mvp.create_account.flow.finish_create_account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentCreateAccountBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class FinishCreateAccountFragment extends ViewFragment implements FinishCreateAccountPresenter.CreateRecipeView {

    public static final int AUTO_SWIPE_DELAY = 3000;
    public static final String NEW_BULLET_PAGE_CREATE_ACCOUNT = "new_bullet_page_create_account";
    public static final String NUMBER_OF_PAGE = "number_of_page";

    @Inject
    FinishCreateAccountPresenter finishCreateAccountPresenter;
    private FragmentCreateAccountBinding binding;
    private FinishCreateAccountPagerAdapter adapter;


    private Timer timer;
    private SwipeTimerTask swipeTimerTask;
    private BroadcastReceiver mMessageReceiver=new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                final int item = intent.getIntExtra(NUMBER_OF_PAGE, -1);
                getActivity().runOnUiThread(() -> {
                    try {
                        binding.createAccountViewPagerr.setCurrentItem(item, true);
                    }catch (Exception e){

                    }
                });
            }catch (Exception e){

            }
        }
    };


    private class SwipeTimerTask extends TimerTask {

        @Override
        public void run() {
            getActivity().runOnUiThread(() -> {
                final int item=binding.createAccountViewPagerr.getCurrentItem()+1;
                if(item>=binding.createAccountViewPagerr.getAdapter().getCount()){
                    finishActivity();
                }else
                    binding.createAccountViewPagerr.setCurrentItem(item);
            });
        }
    }

    public synchronized void startTimer() {
//        if(timer==null) {
//            timer = new Timer();
//            swipeTimerTask = new SwipeTimerTask();
//            timer.schedule(swipeTimerTask, AUTO_SWIPE_DELAY, AUTO_SWIPE_DELAY);
//        }
    }

    public synchronized void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer=null;
        }
    }

    public FinishCreateAccountFragment() {
    }

    public static Fragment newInstance() {
        FinishCreateAccountFragment fragment = new FinishCreateAccountFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                                            new IntentFilter(NEW_BULLET_PAGE_CREATE_ACCOUNT));
    }

    @Override
    public void onDetach() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_account, container, false);
        adapter = new FinishCreateAccountPagerAdapter(getFragmentManager());
        binding.createAccountViewPagerr.setAdapter(adapter);
        binding.createAccountViewPagerr.setOnSwipeOutListener(new CustomViewPager.OnSwipeOutListener() {

            @Override
            public void onSwipeOutAtStart() {

            }

            @Override
            public void onSwipeOutAtEnd() {
                finishActivity();
            }
        });
        binding.createAccountViewPagerr.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                stopTimer();
                startTimer();
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        return binding.getRoot();
    }

    private void finishActivity() {
        getActivity().finish();
        getActivity().onBackPressed();
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("this is dummy");
    }

    @NonNull
    @Override
    public FinishCreateAccountPresenter getPresenter() {
        return finishCreateAccountPresenter;
    }

}
