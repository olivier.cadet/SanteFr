package com.adyax.srisgp.mvp.search;

import android.content.Context;

import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;

/**
 * Created by anton.kobylianskiy on 11/1/16.
 */

public class SearchHelper {
    public static final int SEARCH_DELAY = 400;

    public static boolean isSearchState(String text) {
        return text!=null&& text.length() >= 3;
    }

    public static void removeSearchEntry(Context context, AppReceiver appReceiver, long id, int code, int itemType) {
        ServiceCommand command = new RemoveSearchItemCommand(id, itemType);
        ExecutionService.sendCommand(context, appReceiver, command, code);
    }
}
