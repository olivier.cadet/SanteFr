package com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ask_geo;

import android.content.Context;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentAskGeoCreateAccountBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AskGeoFragment extends ViewFragment implements AskGeoPresenter.DummyView {

    @Inject
    AskGeoPresenter presenter;
    private FragmentAskGeoCreateAccountBinding binding;

    public AskGeoFragment() {
    }

    public static Fragment newInstance() {
        AskGeoFragment fragment = new AskGeoFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ask_geo_create_account, container, false);
        binding.noThankYouLater.setOnClickListener(view -> getPresenter().clickThanksBadlater());
        binding.btnEnableLocation.setOnClickListener(view -> getPresenter().clickEnableLocation());
        return binding.getRoot();
    }

//    @Override
//    public void onViewCreated(View view, Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("this is dummy");
//    }

    @NonNull
    @Override
    public AskGeoPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == GeoHelper.REQUEST_LOCATION_PERMISSION) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                searchPresenter.onRequestCurrentLocation(aroundMeClicked);
//            } else {
//                getPresenter().clickThanksBadlater();
//            }
//        } else {
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }
}
