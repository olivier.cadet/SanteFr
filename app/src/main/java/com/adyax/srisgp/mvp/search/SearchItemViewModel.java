package com.adyax.srisgp.mvp.search;

import com.adyax.srisgp.data.net.response.tags.SearchItem;

/**
 * Created by anton.kobylianskiy on 9/26/16.
 */
public class SearchItemViewModel {
    private long id;
    private String phrase;

    private boolean removed;
    private boolean progress;

    public SearchItemViewModel(SearchItem searchItem) {
        this.id = searchItem.id;
        this.phrase = searchItem.phrase;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public boolean isProgress() {
        return progress;
    }

    public void setProgress(boolean progress) {
        this.progress = progress;
    }
}
