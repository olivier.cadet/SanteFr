package com.adyax.srisgp.mvp.search;

import android.location.Location;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.response.tags.SearchResult;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 9/22/16.
 */
public class SearchPresenter extends Presenter<SearchPresenter.SearchView> {

    private FragmentFactory fragmentFactory;

    @Inject
    public SearchPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    public void searchAroundMe(SearchArgument searchArgument) {
        getView().openSearchResultsScreenAroundMe(searchArgument);
    }

    public void search(SearchArgument searchArgument) {
        getView().openSearchResultsScreen(searchArgument);
    }

    public void onSearchPhraseSelected(String searchText, String locationText) {
        getView().onSelectSearchPhrasesEntry(searchText);
        if (locationText.isEmpty()) {
            getView().showLocationProvideInvitation();
        }
    }

    public void startWebViewActivity(SearchResult searchResult) {
        if(BuildConfig.DEV_STATUS) {
            fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(searchResult, WebPageType.SERVER, TrackerLevel.UNKNOWN), -1);
        }else{
            fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(searchResult.getId(), WebPageType.SERVER, TrackerLevel.UNKNOWN), -1);
        }
    }

    public interface SearchView extends IView {
        void openSearchResultsScreenAroundMe(SearchArgument searchArgument);

        void openSearchResultsScreen(SearchArgument searchArgument);

        void showLocationProvideInvitation();

        void onSelectSearchPhrasesEntry(String text);
    }
}
