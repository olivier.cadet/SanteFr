package com.adyax.srisgp.mvp.searchresults.find;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.databinding.ViewFindEmptyLocationBinding;
import com.adyax.srisgp.databinding.ViewSearchResultsEmptyBinding;
import com.adyax.srisgp.mvp.common.FindCardListener;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.mvp.searchresults.SearchResultsAdapter;
import com.adyax.srisgp.mvp.searchresults.SearchResultsFragment;
import com.adyax.srisgp.utils.PermissionUtils;
import com.adyax.srisgp.utils.Utils;
import com.google.android.gms.common.api.Status;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/27/16.
 */

public class FindSearchResultsFragment extends SearchResultsFragment implements FindLocationPresenter.FindLocationView {

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    public static final String FIND = "find";

    @Inject
    FindLocationPresenter locationPresenter;

    private FindSearchResultsListener findResultsListener;
    private ViewFindEmptyLocationBinding binding;
    private boolean aroundMeClicked = false;
    private boolean visible = false;
    private boolean emptyViewState = false;

    public void setVisible(boolean visible) {
        this.visible = visible;
        if (visible) {
            processActionButtonsVisibility();
        }
    }

    @Override
    public void onChangeSearchCommand(SearchCommand searchCommand) {
        super.onChangeSearchCommand(searchCommand);
        findResultsListener.getResponseData().setSearchCommand(searchCommand);
    }

    private void processActionButtonsVisibility() {
        if (listener != null) {
            if (emptyViewState) {
                listener.onChangeMapActionButtonVisibility(false);
                listener.onChangeFilterActionButtonVisibility(resultsPresenter.hasFilters(), this);
            } else {
                listener.onChangeMapActionButtonVisibility(true);
                listener.onChangeFilterActionButtonVisibility(!resultsPresenter.isEmptyFilters(), this);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        findResultsListener = (FindSearchResultsListener) context;
    }

    @Override
    protected void onRelatedItemClicked(RelatedItem item) {
        listener.onFindRelateItemClicked(item);
    }

    @Override
    protected IPresenter getAdditionalPresenter() {
        return locationPresenter;
    }

    @Override
    public void addResults(Cursor cursor, String suggestedType, boolean sortedByDistance, boolean hasMoreItems) {
        emptyViewState = false;
        findResultsListener.getResponseData().setHasMoreItems(hasMoreItems);
        final boolean isSuggestionFind = !TextUtils.isEmpty(suggestedType) && suggestedType.equals(FIND);
        if (resultsPresenter.getCurrentPage() == 0 && isSuggestionFind) {
            listener.openFindTab();
        }
        updateScreenViewResearch(suggestedType);
        if (visible) {
            processActionButtonsVisibility();
        }
        super.addResults(cursor, suggestedType, sortedByDistance, hasMoreItems);
    }

    @Override
    public void updateScreenViewResearch(String suggestedType) {
        if (suggestedType != null) {
            listener.addScreenViewResearch(suggestedType.equals(FIND) ?
                    SearchResultsActivity.FIND_PAGE : SearchResultsActivity.INFO_PAGE);
        }
    }

    @Override
    public void updateInfo(SearchResponseHuge response) {

    }

    @Override
    public void onSetSortedByDistance(boolean sortedByDistance) {
        super.onSetSortedByDistance(sortedByDistance);
        findResultsListener.getResponseData().setSortedByDistance(sortedByDistance);
    }

    @Override
    protected boolean shouldLoadData() {
        if (findResultsListener.getResponseData() == null) {
            return true;
        }
        return findResultsListener.getResponseData().getSearchCommand() == null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SearchCommand findCommand = findResultsListener.getResponseData().getSearchCommand();
        if (findResultsListener.getResponseData().getSearchCommand() != null) {
            resultsPresenter.setSearchCommand(findCommand);
            resultsPresenter.setStateToken(findCommand.getStateToken());
            resultsPresenter.setCurrentPage(findCommand.getPage() + 1);
            resultsPresenter.setHasMoreItems(findResultsListener.getResponseData().isHasMoreItems());
        }
    }

    @Override
    public void onLocationUpdated(Location location) {
        super.onLocationUpdated(location);
    }

    @Override
    protected void showEmptyView(ViewGroup parent, boolean locationDefined) {
        emptyViewState = true;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (locationDefined) {
            ViewSearchResultsEmptyBinding binding = DataBindingUtil.inflate(inflater, R.layout.view_search_results_empty, parent, false);
            binding.tvEmptyTitle.setText(getString(R.string.search_find_empty_title));
            binding.tvEmptyText1.setText(getString(R.string.search_get_informed_empty_text));
            binding.tvEmptyText2.setText(locationPresenter.getRepository().getConfig().getSearch().getEmpty_result_inactive_region());
            parent.addView(binding.getRoot());
        } else {
            binding = DataBindingUtil.inflate(inflater, R.layout.view_find_empty_location, parent, false);

            prepareEmptyView();

            binding.btnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // is around me
                    if ((Boolean) v.getTag()) {
                        aroundMeClicked = true;
                        if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                            requestLocationPermissions();
                        } else {
                            locationPresenter.onRequestCurrentLocation(true);
                        }
                    } else {
                        aroundMeClicked = false;
                        if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                            requestLocationPermissions();
                        } else {
                            locationPresenter.onRequestCurrentLocation(false);
                        }
                    }
                }
            });

            parent.addView(binding.getRoot());
        }

        if (visible) {
            processActionButtonsVisibility();
        }
    }

    private void prepareEmptyView() {
        if (binding != null) {
            if (!hasLocationPermissions()) {
                binding.btnAction.setText(R.string.search_enable_location);
                binding.btnAction.setTag(false);
                binding.llActivateBlock.setVisibility(View.VISIBLE);
            } else {
                binding.btnAction.setText(R.string.search_around_me);
                binding.btnAction.setTag(true);
                binding.llActivateBlock.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationPresenter.onRequestCurrentLocation(aroundMeClicked);
            } else {
                if (PermissionUtils.isNotAskAgainLocationPermission(getActivity())) {
                    showLocationPermissionNotAskAgain();
                } else {
                    showLocationPermissionDenied();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected SearchResultsAdapter createAdapter(SearchResultsAdapter.ResultsAdapterListener adapterListener) {
        return new FindResultsAdapter(adapterListener, new FindCardListener() {
            @Override
            public void onCallClicked(SearchItemHuge item) {
//                String result=Utils.requestCall(item.getPhone(), getContext());
                String result = new TrackerHelper(item).requestCall(getContext());
                if (result != null) {
                    showMessage(result);
                } else {
//                    locationPresenter.getTracker().addGesture(new TrackerAT.TrackerPageArgBuilder(new WebViewArg(item, TrackerLevel.RESEARCH_RESULTS_FIND)).build());
//                    new TrackerHelper(item).addGesture(TrackerLevel.RESEARCH_RESULTS_FIND);
                }
            }

            @Override
            public void onFavoriteClicked(SearchItemHuge item) {
                resultsPresenter.clickFavorite(item);
            }

            @Override
            public void onRouteClicked(SearchItemHuge item) {
//                showMessage("onRouteClicked");
                getActivity().startActivity(Utils.viewOnMap(String.valueOf(item.getLat()),
                        String.valueOf(item.getLon())));
            }

            @Override
            public void onItemClicked(SearchItemHuge item) {
                if (BuildConfig.DEBUG || !item.isContentMaintenanceMode()) {
                    onClickRead(item);
                }
            }
        }, locationPresenter.getRepository());


    }

    @Override
    protected void viewCreated(View view, Bundle savedInstanceState) {

    }

    public void onPromptSettingsResult(boolean success) {
        if (success) {
            locationPresenter.onPromptSettingSuccess(aroundMeClicked);
        } else {
            showLocationNotEnabled();
        }
    }

    @Override
    public void onEnableLocationButtonStateChanged() {
        prepareEmptyView();
    }

    @Override
    public void showEnableLocationDialog(Status status) {
        findResultsListener.onPromptLocation(status);
    }

    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void setCurrentLocation(Location location) {//5
        findResultsListener.onLocationChanged(location);
    }

    private boolean hasLocationPermissions() {
        return PermissionUtils.isProviderEnabled(getContext()) && PermissionUtils.hasPermissionsForRequestingLocation(getContext());
    }

    private void requestLocationPermissions() {
        PermissionUtils.requestLocationPermissions(this, REQUEST_LOCATION_PERMISSION);
    }

    public SearchCommand getSearchCommand() {
        if (findResultsListener != null) {
            final SearchResultsActivity.FindSearchResponseData responseData = findResultsListener.getResponseData();
            if (responseData != null) {
                return responseData.getSearchCommand();
            }
        }
        return null;
    }

    public interface FindSearchResultsListener extends ProvideLocationFragment.OnPromptLocationSettingsListener {
        void onLocationChanged(Location location);

        SearchResultsActivity.FindSearchResponseData getResponseData();
    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

}
