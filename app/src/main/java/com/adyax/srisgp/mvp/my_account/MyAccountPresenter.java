package com.adyax.srisgp.mvp.my_account;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.CancelAccountCommand;
import com.adyax.srisgp.data.net.command.LogoutCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.presentation.MainActivity;

import javax.inject.Inject;

public class MyAccountPresenter extends PresenterAppReceiver<MyAccountPresenter.CreateRecipeView> {

    public interface CreateRecipeView extends IView {
        void showToast(String text);

        Activity getActivity();

        void showMessage(String body);
    }

    private FragmentFactory fragmentFactory;

    @Inject
    public MyAccountPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    public void logOut() {//updateMenu
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.sign_out_message)
                .setPositiveButton(R.string.sign_out_positive, (dialog, which) -> {
                    ExecutionService.sendCommand(getContext(), getAppReceiver(),
                            new LogoutCommand(), ExecutionService.LOGOUT_ACTION);
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();
    }

    public void deleteAccount() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.delete_account_title)
                .setMessage(R.string.delete_account_body)
                .setPositiveButton(R.string.delete_account_ok, (dialog, which) -> {
                    ExecutionService.sendCommand(getContext(), getAppReceiver(),
                                    new CancelAccountCommand(), ExecutionService.DELETE_ACCOUNT);
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();
    }

    public void startPersonalInformation() {
        if (getView() != null) {
            PersonalInformationActivity.start(getView().getActivity());
        }
    }

    public void startChangeCredentials() {
        if (getView() != null) {
            ChangeCredentialsActivity.start(getView().getActivity());
        }
    }

    public void startPersonalizeNotifications() {
        if (getView() != null) {
            PersonalizeActivity.start(getView().getActivity());
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode){
            case ExecutionService.LOGOUT_ACTION:
            case ExecutionService.DELETE_ACCOUNT:
                if(getContext() instanceof MainActivity){
                    ((MainActivity)getContext()).updateMenuAndFragments();
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        ErrorResponse errorResponse = ErrorResponse.create(errorMessage);
        if (errorResponse != null) {
            if (errorResponse.isAnyError()) {
                if (BuildConfig.DEBUG) {
                    getView().showMessage(errorResponse.getErrorMessage(null));
                }

            }
        }
    }
}
