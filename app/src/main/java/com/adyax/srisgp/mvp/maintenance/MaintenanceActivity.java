package com.adyax.srisgp.mvp.maintenance;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.executor.AppReceiver;

/**
 * Created by SUVOROV on 8/3/16.
 */

public class MaintenanceActivity extends MaintenanceBaseActivity{

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MaintenanceActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertpopin);

        if (savedInstanceState == null) {
            replaceFragment(R.id.fragment_container, MaintenanceFragment.newInstance(R.string.app_name));
        }
    }

    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        finish();
    }

}
