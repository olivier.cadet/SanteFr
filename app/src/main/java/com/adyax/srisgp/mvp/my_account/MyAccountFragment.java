package com.adyax.srisgp.mvp.my_account;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentMyAccountBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;

// TODO https://docs.google.com/presentation/d/1gSCRr96n3oGAJQ1-AFr_67HuD88VDovvgI1ljp6Zfmc/edit#slide=id.g134fd4a79e_0_0
public class MyAccountFragment extends ViewFragment implements MyAccountPresenter.CreateRecipeView {

    @Inject
    MyAccountPresenter myAccountPresenter;

    private FragmentMyAccountBinding binding;
    private MyAccountListener listener;

    public MyAccountFragment() {
    }

    public static Fragment newInstance() {
        MyAccountFragment fragment = new MyAccountFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (MyAccountListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_account, container, false);

        binding.personalInterestsTextView.setOnClickListener(v -> myAccountPresenter.startPersonalizeNotifications());

        binding.authenticationCredentialsTextView.setOnClickListener(v -> myAccountPresenter.startChangeCredentials());

        binding.personalInformationTextView.setOnClickListener(v -> myAccountPresenter.startPersonalInformation());

        binding.browserHistoryTextView.setOnClickListener(v -> {
            listener.onHistoryClicked();
        });

        binding.logOutTextView.setOnClickListener(v -> myAccountPresenter.logOut());
        binding.deleteAccountTextView.setOnClickListener(v -> myAccountPresenter.deleteAccount());

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initActionBar();
    }

    @NonNull
    @Override
    public MyAccountPresenter getPresenter() {
        return myAccountPresenter;
    }

    private void initActionBar() {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.my_account_title);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

    public interface MyAccountListener {
        void onHistoryClicked();
        void onFavoritClicked();
        void onNotificationsClicked();
        void onMyAccountClicked();
    }
}
