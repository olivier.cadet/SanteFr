package com.adyax.srisgp.mvp.maintenance;

import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.presentation.PlusAlertPopupActivity;

/**
 * Created by SUVOROV on 4/7/17.
 */

public class MaintenanceBaseActivity extends PlusAlertPopupActivity implements AppReceiver.AppReceiverCallback {

//    @Inject
//    IGeoHelper geoHelper;

//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        App.getApplicationComponent().inject(this);
//        super.onCreate(savedInstanceState);
//    }

    @Override
    public void onStart() {
        super.onStart();
        if (appReceiver == null) {
            appReceiver = new AppReceiver();
        }
        appReceiver.setListener(this);
//        if(geoHelper!=null) {
//            geoHelper.setOnLocationListener(location -> {
//                update(location);
//                geoHelper.disconnect();
//            });
//            startAskGeo();
//        }
    }

    protected void update(Location location) {

    }

//    private void startAskGeo() {
//        if(!geoHelper.askGeo(this)){
//            update(null);
//        }
//    }

    @Override
    public void onStop() {
        super.onStop();
        appReceiver.setListener(null);
//        if(geoHelper!=null) {
//            geoHelper.disconnect();
//        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

}
