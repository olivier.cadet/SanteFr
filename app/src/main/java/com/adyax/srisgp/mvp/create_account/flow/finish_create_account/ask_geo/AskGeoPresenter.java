package com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ask_geo;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.adyax.srisgp.R;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ICreateAccount;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class AskGeoPresenter extends Presenter<AskGeoPresenter.DummyView> {

    public void clickThanksBadlater() {
//        getView().showMessage("clickThanksBadlater");
        Fragment fragment = FinishCreateAccountFragment.newInstance();
        ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    public void clickEnableLocation() {
//        getView().showMessage("clickEnableLocation");
        if(getContext() instanceof ICreateAccount){
            ((ICreateAccount)getContext()).startAskGeoPermission();
        }
    }

    public interface DummyView extends IView {
        void showMessage(String body);
    }

    @Inject
    public AskGeoPresenter() {
    }

}
