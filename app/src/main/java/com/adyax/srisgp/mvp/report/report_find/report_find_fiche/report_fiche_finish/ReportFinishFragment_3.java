package com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_finish;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.databinding.FragmentFinishFiche3Binding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportFinishFragment_3 extends ViewFragment implements ReportFinishPresenter_3.ReportPresenter_2View {

    public static final String TITLE = "title";

    @Inject
    ReportFinishPresenter_3 reportPresenter1;

    private FragmentFinishFiche3Binding binding;
//    private int title;

    public ReportFinishFragment_3() {
    }

    public static Fragment newInstance(int title, ReportBuilder builder) {
        ReportFinishFragment_3 fragment = new ReportFinishFragment_3();
        Bundle args = new Bundle();
//        args.putInt(TITLE, title);
        args.putParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER, builder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        title =getArguments().getInt(TITLE);
        reportPresenter1.setBuilder(getArguments().getParcelable(ReportBuilder.REPORT_FORM_VALUE_BUILDER));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_finish_fiche_3, container, false);
//        binding.backToCard.setOnClickListener(view -> {
//            reportPresenter1.getBuilder().set(ReportBuilder.FIELD_EMAIL, binding.reportErrorInfo.getText().toString());
//            showCloseDialog();
//        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);
        new Handler().postDelayed(() -> getActivity().finish(), 500);
    }

    @NonNull
    @Override
    public ReportFinishPresenter_3 getPresenter() {
        return reportPresenter1;
    }

    @Override
    public void showCloseDialog() {
        new AlertDialog.Builder(getContext())
            .setMessage(R.string.close_report)
            .setPositiveButton(R.string.text214, (dialog, which) -> {
                dialog.dismiss();
                reportPresenter1.clickBackToCard();
                getActivity().finish();
            })
            .show();
    }

    @Override
    public void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progressLayout.setVisibility(View.GONE);
    }
}
