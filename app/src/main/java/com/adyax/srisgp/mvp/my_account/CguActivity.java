package com.adyax.srisgp.mvp.my_account;

import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.adyax.srisgp.R;

public class CguActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.cgu_action_dialog_title);
        alertDialog.setMessage(R.string.cgu_action_dialog_body);
        alertDialog.setPositiveButton(R.string.cgu_action_accept, (dialog, which) -> {
            sendMessage("ACCEPTED");
            finish();
        });
        alertDialog.setNegativeButton(R.string.cgu_action_decline, (dialog, which) -> {
            sendMessage("DECLINED");
            finish();
        });
        alertDialog.setOnDismissListener(dialog -> {
            sendMessage("DISMISS");
            finish();
        });

        alertDialog.create();
        alertDialog.show();
    }


    /*
     * Broadcast event to "CGU_BROADCAST_EVENT"
     * @see CguHandler.java for usage
     */
    private void sendMessage(String actionString) {
        Intent intent = new Intent("CGU_BROADCAST_EVENT");
        intent.putExtra("CGU_ACTION", actionString);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
