package com.adyax.srisgp.mvp.contact;

import android.os.Bundle;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddContactFormFeedbackCommand;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormField;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.FeedbackOption;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 11/21/16.
 */

public class ContactPresenter extends PresenterAppReceiver<ContactPresenter.ContactView> {

    private INetWorkState netWorkState;
    private FeedbackFormResponse feedbackFormResponse;
    private long startTime;
    private IRepository repository;

    @Inject
    public ContactPresenter(INetWorkState netWorkState, IRepository repository) {
        this.netWorkState = netWorkState;
        this.repository = repository;
    }

    public void onViewCreated() {
        RegisterUserResponse credential = repository.getCredential();
        if (credential != null && credential.user != null) {
            getView().showEmail(credential.user.mail);
        }
    }

    public void getForm() {
        if (getView() != null) {
            getView().showLoading();
            GetFormCommand getFormCommand = new GetFormCommand(BaseFeedbackCommandCommand.CONTACT_NAME, null);
            ExecutionService.sendCommand(getContext(), getAppReceiver(), getFormCommand, ExecutionService.GET_FORM);
        }
    }

    public void onSubmitForm(String surname, String name, String email, String transmitterQualitySelectValue, String idRppsValue, String subject, String message) {
        if (getView() != null) {
            getView().showLoading();
            AddContactFormFeedbackCommand.ContactFormValues contactFormValues =
                    new AddContactFormFeedbackCommand.ContactFormValues(
                            surname, name, email,
                            transmitterQualitySelectValue, idRppsValue,
                            subject, message
                    );
            AddContactFormFeedbackCommand contactFormFeedbackCommand =
                    new AddContactFormFeedbackCommand(
                            netWorkState.isLoggedIn(), BaseFeedbackCommandCommand.CONTACT_NAME, feedbackFormResponse.secret,
                            contactFormValues, startTime
                    );
            ExecutionService.sendCommand(getContext(), getAppReceiver(), contactFormFeedbackCommand, ExecutionService.ADD_CONTACT_FEEDBACK_FORM);
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        super.onFail(requestCode, errorMessage);
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {
        super.onMessage(requestCode, data);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.GET_FORM:
                feedbackFormResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                startTime = data.getLong(CommandExecutor.START_TIME);
                if (getView() != null) {
                    if (feedbackFormResponse.fields != null) {
                        FeedbackFormField feedbackFormField;
                        for (int i = 0; i < feedbackFormResponse.fields.size(); i++) {
                            feedbackFormField = feedbackFormResponse.fields.get(i);
                            if (feedbackFormField.name.equals("field_subject")) {
                                if (feedbackFormField.options != null) {
                                    getView().showSubjectValues(feedbackFormField.options);
                                }
                            } else if (feedbackFormField.name.equals("field_transmitter_quality")) {
                                if (feedbackFormField.options != null) {
                                    getView().showTransmitterQualityValues(feedbackFormField.options);
                                }
                            }
                        }
                    }
                    getView().hideLoading();
                }
                break;
            case ExecutionService.ADD_CONTACT_FEEDBACK_FORM:
                if (getView() != null) {
                    getView().clearForm(!netWorkState.isLoggedIn());
                    getView().showSuccessDialog();
                    getView().hideLoading();
                }
                break;
        }
    }

    public interface ContactView extends IView {
        void showLoading();

        void hideLoading();

        void showTransmitterQualityValues(List<FeedbackOption> options);

        void showSubjectValues(List<FeedbackOption> options);

        void showSuccessDialog();

        void showEmail(String email);

        void clearForm(boolean clearEmailField);
    }
}
