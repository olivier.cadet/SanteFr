package com.adyax.srisgp.mvp.create_account.flow.providelocation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;
import com.google.android.gms.common.api.Status;

/**
 * Created by anton.kobylianskiy on 10/6/16.
 */

public class ProvideLocationActivity extends MaintenanceBaseActivity implements ProvideLocationFragment.OnPromptLocationSettingsListener {

    private static final int REQUEST_CHECK_SETTINGS = 1;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ProvideLocationActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provide_location);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            replaceFragment(R.id.fragment_container, ProvideLocationFragment.newInstance());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            ((ProvideLocationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container))
                    .onPromptSettingsResult(resultCode == Activity.RESULT_OK);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPromptLocation(Status status) {
        try {
            status.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }
}
