package com.adyax.srisgp.mvp.searchhome.alerts.alert_popin;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.databinding.ActivityAlertpopinBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertPopinActivity extends MaintenanceBaseActivity implements IAlertPopinActivity{

    private ActivityAlertpopinBinding binding;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, AlertItem alertItem) {
        Intent intent = new Intent(context, AlertPopinActivity.class);
        intent.putExtra(AlertItem.BUNDLE_NAME, alertItem);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alertpopin);
        fragmentFactory.startAlertPopinFragment(this, getIntent().getParcelableExtra(AlertItem.BUNDLE_NAME));
    }

    @Override
    public void onBackPressed() {
        for(Fragment f:getSupportFragmentManager().getFragments()) {
            if(f!=null&& ((ViewFragment)f).onBackPressed()){
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch(itemId){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void closeAlert() {

    }

}
