package com.adyax.srisgp.mvp.web_views;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

/**
 * Created by SUVOROV on 10/19/16.
 */

public class TouchyWebView extends WebView {

    private ScrollView scrollView;
    private View parentView;
    private View root;
//    private LinearLayout rootLayout;
//    private int contentHeight=0;

    private OnScrollChangedListener onScrollChangedListener;

    public TouchyWebView(Context context) {
        super(context);
    }

    public TouchyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchyWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

//        //Check is required to prevent crash
//        if (MotionEventCompat.findPointerIndex(event, 0) == -1) {
//            return super.onTouchEvent(event);
//        }
//
//        if (event.getPointerCount() >= 2) {
//            requestDisallowInterceptTouchEvent(true);
//        } else {
//            requestDisallowInterceptTouchEvent(false);
//        }

//        final int contentHeight=getContentHeight();
//        if(this.contentHeight!=contentHeight){
//            this.contentHeight=contentHeight;
//
////            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rootLayout.getLayoutParams();
////            params.height = contentHeight;
////            params.width = LayoutParams.MATCH_PARENT;
////            rootLayout.setLayoutParams(params);
////
////            rootLayout.requestLayout();
//
//        }

        if (isParentVisible()) {
            requestDisallowInterceptTouchEvent(false);
            scrollToBottom();
//            int height=getHeight();
//            int conHeih=getContentHeight();
//            rootLayout.requestLayout();
////            updateHeight();
        } else {
            requestDisallowInterceptTouchEvent(true);
        }
        return super.onTouchEvent(event);
    }

    public void scrollToBottom() {
        scrollTo(0, 1000000000);
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        if (clampedY) {
            requestDisallowInterceptTouchEvent(false);
        } else {
            if (isParentVisible()) {
                requestDisallowInterceptTouchEvent(false);
            } else {
                requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    public void init(ScrollView scrollView, View parentView, View root, LinearLayout rootLayout) {
        this.scrollView = scrollView;
        this.parentView = parentView;
        this.root = root;
//        this.rootLayout = rootLayout;
    }

    private boolean isParentVisible() {
        try {
            if (scrollView != null && parentView != null) {
//                updateHeight();
                Rect scrollBounds = new Rect();
                scrollView.getHitRect(scrollBounds);
                return parentView.getLocalVisibleRect(scrollBounds);
            }
        } catch (Exception e) {

        }
        return false;
    }

    public void updateHeight() {
        if (root != null) {
            final int height = root.getHeight();
            if (height > 0) {
                getLayoutParams().height = height;
                requestLayout();
            }
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (onScrollChangedListener != null) {
            onScrollChangedListener.onScrollChange(this, l, t, oldl, oldt);
        }
    }

    public void setOnScrollChangedListener(OnScrollChangedListener onScrollChangedListener) {
        this.onScrollChangedListener = onScrollChangedListener;
    }

    public interface OnScrollChangedListener {
        void onScrollChange(WebView webView, int scrollX, int scrollY, int oldScrollX, int oldScrollY);
    }
}
