package com.adyax.srisgp.mvp.searchhome.alerts.alert_item;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertItemPresenter extends Presenter<AlertItemPresenter.CreateRecipeView> {

    private FragmentFactory fragmentFactory;

    public void clickAlertCaruselCard(AlertItem item) {
        new TrackerHelper(item).clickAlert();
        if(item.isContent()) {
            fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, TrackerLevel.UNKNOWN).setAlertItem(item), -1);
        }
    }

    public interface CreateRecipeView extends IView {
    }

    @Inject
    public AlertItemPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

}
