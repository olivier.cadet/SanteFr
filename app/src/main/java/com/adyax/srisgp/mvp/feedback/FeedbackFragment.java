package com.adyax.srisgp.mvp.feedback;

import android.content.Context;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.FeedbackFormField;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.FeedbackOption;
import com.adyax.srisgp.databinding.FragmentFeedbackBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.ViewUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class FeedbackFragment extends ViewFragment implements FeedbackPresenter.FeedbackView {
    private static final String ARGS_URL = "args_url";
    private static final String ARGS_FORM_NAME = "args_form_name";

    @Inject
    FeedbackPresenter feedbackPresenter;

    private FragmentFeedbackBinding binding;
    private String helplessReasonFieldName;

    public static Fragment newInstance(String url, String formName) {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_URL, url);
        args.putString(ARGS_FORM_NAME, formName);
        fragment.setArguments(args);
        return fragment;
    }

    private String getUrl() {
        return getArguments().getString(ARGS_URL);
    }

    private String getFormName() {
        return getArguments().getString(ARGS_FORM_NAME);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_feedback, container, false);
//        binding.getRoot().setOnTouchListener((v, event) -> true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, true);
        feedbackPresenter.getForm(getFormName());
        binding.btnSend.setOnClickListener(v -> {
            feedbackPresenter.onSendFeedbackClicked(getUrl(), binding.etReason.getText().toString(), helplessReasonFieldName);
        });
        binding.etReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                feedbackPresenter.onCheckedStateChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public List<String> getCheckedValues() {
        List<String> values = new ArrayList<>();
        for (int i = 0; i < binding.checkboxValues.getChildCount(); i++) {
            ViewGroup container = (ViewGroup) binding.checkboxValues.getChildAt(i);
            CheckBox checkBox = (CheckBox) container.findViewById(R.id.checkbox);
            if (checkBox.isChecked()) {
                String value = (String) checkBox.getTag();
                values.add(value);
            }
        }
        return values;
    }

    @Override
    public void render(FeedbackFormResponse response) {
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        supportActionBar.setTitle(response.title);

//        TextView tv = new TextView(getContext());
//
//        // Create a LayoutParams for TextView
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
//                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
//
//        // Apply the layout parameters to TextView widget
//        tv.setLayoutParams(lp);
//
//        // Set text to display in TextView
//        tv.setText(response.title); // ActionBar title text
//
//        // Set the text color of TextView to black
//        // This line change the ActionBar title text color
//        tv.setTextColor(Color.BLACK);
//
//        // Set the TextView text size in dp
//        // This will change the ActionBar title text size
//        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,10);
//
//        // Set the ActionBar display option
//        supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//
//        // Finally, set the newly created TextView as ActionBar custom view
//        supportActionBar.setCustomView(tv);

        binding.etReason.setVisibility(View.VISIBLE);
        binding.btnSend.setVisibility(View.VISIBLE);
        for (FeedbackFormField field : response.fields) {
            if (field.getType() == FeedbackFormField.FieldType.CHECKBOXES) {
                helplessReasonFieldName = field.name;
                LayoutInflater inflater = LayoutInflater.from(getContext());
                for (FeedbackOption option : field.options) {
                    ViewGroup checkboxContainer = (ViewGroup) inflater.inflate(R.layout.item_feedback_checkbox, null, false);
                    CheckBox checkBox = (CheckBox) checkboxContainer.findViewById(R.id.checkbox);
                    checkBox.setOnCheckedChangeListener(((buttonView, isChecked) -> {
                        feedbackPresenter.onCheckedStateChanged();
                    }));
                    checkBox.setTag(option.value);
                    TextView tvText = (TextView) checkboxContainer.findViewById(R.id.tvText);
                    tvText.setText(option.label);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, (int) getResources().getDimension(R.dimen.default_margin), 0, 0);
                    checkboxContainer.setLayoutParams(layoutParams);
                    binding.checkboxValues.addView(checkboxContainer);
                }
            }
        }
    }

    @Override
    public boolean hasCheckedValues() {
        if (binding.etReason.getText().toString().length() >= 3) {
            return true;
        }

        for (int i = 0; i < binding.checkboxValues.getChildCount(); i++) {
            ViewGroup container = (ViewGroup) binding.checkboxValues.getChildAt(i);
            CheckBox checkBox = (CheckBox) container.findViewById(R.id.checkbox);
            if (checkBox.isChecked()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void back() {
        getActivity().onBackPressed();
    }

    @Override
    public void showLoading() {
        binding.progressLayout.setVisibility(View.VISIBLE);
        binding.btnSend.setClickable(false);
    }

    @Override
    public void hideLoading() {
        binding.progressLayout.setVisibility(View.GONE);
        binding.btnSend.setClickable(true);
    }

    @Override
    public void enableButton() {
        binding.btnSend.setEnabled(true);
    }

    @Override
    public void disableButton() {
        binding.btnSend.setEnabled(false);
    }

    @NonNull
    @Override
    public FeedbackPresenter getPresenter() {
        return feedbackPresenter;
    }
}
