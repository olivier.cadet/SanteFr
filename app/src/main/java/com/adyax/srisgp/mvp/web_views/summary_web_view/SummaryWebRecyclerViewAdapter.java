package com.adyax.srisgp.mvp.web_views.summary_web_view;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.tags.AnchorItem;
import com.adyax.srisgp.databinding.ItemSummaryWebViewBinding;

/**
 * Created by SUVOROV on 10/12/16.
 */

public class SummaryWebRecyclerViewAdapter extends RecyclerView.Adapter<SummaryWebRecyclerViewAdapter.ViewHolder> {

    private final AnchorItem[] anchors;
    private final int count;

    private final boolean is_en_relation;

    public SummaryWebRecyclerViewAdapter(AnchorResponse anchorResponse) {
        this.anchors = anchorResponse.anchors;
//        is_en_relation = RELATED.equals(anchorResponse[anchorResponse.anchors.length - 1].title);
        is_en_relation=anchorResponse.containRelated();
        count= anchorResponse.anchors.length>0?(is_en_relation ? anchorResponse.anchors.length-1: anchorResponse.anchors.length): 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                                                    R.layout.item_summary_web_view, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final AnchorItem anchorItem = anchors[i];
        viewHolder.binding.title.setText(anchorItem.getTitle());
    }

    @Override
    public int getItemCount() {
        return count;//anchors.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemSummaryWebViewBinding binding;

        public ViewHolder(ItemSummaryWebViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public boolean is_en_relation() {
        return is_en_relation;
    }
}
