package com.adyax.srisgp.mvp.around_me;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.CursorLoader;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.db.IDataBaseHelper;

/**
 * Created by SUVOROV on 3/25/16.
 */
public class AdvertCursorLoader extends CursorLoader {

    private IDataBaseHelper dataBaseHelper;
    private SearchType searchType;

    public AdvertCursorLoader(Context context, IDataBaseHelper dataBaseHelper, SearchType searchType) {
        super(context);
        this.dataBaseHelper = dataBaseHelper;
        this.searchType = searchType;
    }

    @Override
    public Cursor loadInBackground() {
        return dataBaseHelper.getAdvertSearch(searchType);
    }
}
