package com.adyax.srisgp.mvp.searchresults;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.AsyncTaskLoader;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.db.IDataBaseHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 3/14/17.
 */

public class SearchMessageLoader extends AsyncTaskLoader<List<SearchMessage>> {
    private IDataBaseHelper dataBaseHelper;
    private SearchType searchType;

    public SearchMessageLoader(Context context, IDataBaseHelper dataBaseHelper, SearchType searchType) {
        super(context);
        this.dataBaseHelper = dataBaseHelper;
        this.searchType = searchType;
    }

    @Override
    public List<SearchMessage> loadInBackground() {
        List<SearchMessage> messages = new ArrayList<>();
        Cursor cursor = dataBaseHelper.getSearchMessages(searchType);
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    SearchMessage searchMessage = new SearchMessage();
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, searchMessage);
                    messages.add(searchMessage);
                }
                return messages;
            } finally {
                cursor.close();
            }
        }
        return Collections.emptyList();
    }
}
