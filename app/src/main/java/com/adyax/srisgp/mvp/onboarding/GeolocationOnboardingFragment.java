package com.adyax.srisgp.mvp.onboarding;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.presentation.BaseFragment;
import com.adyax.srisgp.utils.PermissionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
//import com.google.common.collect.Lists;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public class GeolocationOnboardingFragment extends BaseFragment {

    private static final int GET_LOCATION_PERMISSIONS = 1;

    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.btnLater)
    Button btnLater;
    @BindView(R.id.btnProvideLocation)
    Button btnProvideLocation;

    @BindView(R.id.skipBtn)
    TextView skipBtn;

    @Inject
    IRepository repository;

    private GoogleApiClient googleApiClient;

    private OnboardingFragmentListener listener;

    public static GeolocationOnboardingFragment newInstance() {
        Bundle args = new Bundle();
        GeolocationOnboardingFragment fragment = new GeolocationOnboardingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (OnboardingFragmentListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_onboarding_geolocation, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void onLocationResultRetrieved(boolean success) {
        if (success) {
            btnLater.setVisibility(View.INVISIBLE);
            btnProvideLocation.setVisibility(View.INVISIBLE);
        } else {
            showLocationWasNotRetrievedMessage();
        }
        listener.onNext();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private void bindViews() {
        if (PermissionUtils.hasPermissionsForRequestingLocation(getContext()) && PermissionUtils.isGPSEnabled(getContext())) {
            btnLater.setVisibility(View.INVISIBLE);
            btnProvideLocation.setVisibility(View.INVISIBLE);
        } else {
            btnLater.setVisibility(View.VISIBLE);
            btnProvideLocation.setVisibility(View.VISIBLE);

            btnLater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onNext();
                }
            });

            btnProvideLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                        requestPermissions();
                    } else {
                        requestLocation();
                    }
                }
            });
        }

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSkip();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GET_LOCATION_PERMISSIONS) {
            boolean allGranted = true;
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    allGranted = false;
                    break;
                }
            }

            if (!allGranted) {
                showLocationWasNotRetrievedMessage();
                listener.onNext();
            } else {
                requestLocation();
            }
        }
    }

    private void showLocationWasNotRetrievedMessage() {
        Snackbar.make(getView(), R.string.location_permissions_failed, Snackbar.LENGTH_SHORT).show();
    }

    private void requestLocation() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(@Nullable Bundle bundle) {
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO Afficher une modal ou autre pour accepter les perms
                                return;
                            }
                            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                            repository.saveGeoLocation(location);//+
                        }

                        @Override
                        public void onConnectionSuspended(int i) {

                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        }
                    }).build();
        }

        if (!googleApiClient.isConnected()) {
            googleApiClient.connect();
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO Afficher une modal ou autre pour accepter les perms
                            return;
                        }
                        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                        repository.saveGeoLocation(location);//+
                        onLocationResultRetrieved(true);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    getActivity(), OnboardingActivity.REQUEST_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient != null) {
            googleApiClient.disconnect();
        }
    }

    private void requestPermissions() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GET_LOCATION_PERMISSIONS);
    }
}
