package com.adyax.srisgp.mvp.common;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.SearchMessage;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 3/14/17.
 */

public class MessageHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvClose)
    TextView tvClose;

    public MessageHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bind(SearchMessage message, MessageListener listener) {
        tvContent.setText(message.getDescription());

        tvClose.setOnClickListener(v -> {
            if (listener != null) {
                listener.onCloseMessage(message);
            }
        });
    }

    public interface MessageListener {
        void onCloseMessage(SearchMessage searchMessage);
    }
}
