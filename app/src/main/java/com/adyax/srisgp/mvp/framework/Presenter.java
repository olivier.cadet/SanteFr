package com.adyax.srisgp.mvp.framework;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

public abstract class Presenter<V extends IView> implements IPresenter<V> {

    @Nullable
    private WeakReference<V> mViewWeakReference;

    public Presenter() {
        // This constructor is intentionally empty. Nothing special is needed here.
    }

    public void attachView(@NonNull V view) {
        mViewWeakReference = new WeakReference<>(view);
    }

    public void detachView() {
        if (mViewWeakReference != null && mViewWeakReference.get() != null) {
            mViewWeakReference.clear();
        }
        mViewWeakReference = null;
    }

    public boolean isViewAttached() {
        return mViewWeakReference != null && mViewWeakReference.get() != null;
    }

    @Nullable
    public V getView() {
        if (mViewWeakReference != null && mViewWeakReference.get() != null) {
            return mViewWeakReference.get();
        }
        return null;
    }

    @Override
    public void registerListeners() {
        //no implementation
    }

    @Override
    public void unregisterListeners() {
        //no implementation
    }

    @Override
    public void restoreState(@Nullable Bundle savedInstanceState) {
        //no implementation
    }

    @Override
    public void saveState(@NonNull Bundle outState) {
        //no implementation
    }

    protected String getString(int resId) {
        if (getView() != null)
            return getView().getContext().getString(resId);
        return "";
    }

    protected Context getContext() {
        return getView().getContext();
    }
}
