package com.adyax.srisgp.mvp.around_me;

import android.os.Bundle;

import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.builders.SearchRequestBuilder;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.utils.FiltersHelper;
import com.adyax.srisgp.utils.Utils;
//import com.facebook.stetho.common.Util;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by SUVOROV on 11/11/16.
 */


public class SearchRequestHelper implements ISearchRequestHelper {

    public static final String AROUND_ME_ARGUMENTS = "aroundMeArguments";
    public static final String FILTERS_HELPER = "filtersHelper";
    private String serializedObject;
    private Bundle bundle;
    private Gson gson = new Gson();

    private AroundMeArguments aroundMeArguments;
    private FiltersHelper filtersHelper;
    private SearchRequestType searchRequestTypeActive = SearchRequestType.DEFAULT;
    private SearchRequestType searchRequestTypeCurrent = SearchRequestType.DEFAULT;

    public SearchRequestHelper(FiltersHelper filtersHelper) {
        this.filtersHelper = filtersHelper;
        bundle = new Bundle();
        bundle.putSerializable(FILTERS_HELPER, filtersHelper);
        serializedObject = gson.toJson(filtersHelper);
    }

    public void update(AroundMeArguments aroundMeArguments) {
        if (this.aroundMeArguments != null) {
            bundle.putParcelable(AROUND_ME_ARGUMENTS, this.aroundMeArguments);
        }
        this.aroundMeArguments = aroundMeArguments;
    }

    public SearchRequestHelper setHasMoreItems(boolean response) {
        aroundMeArguments.setHasMoreItems(response);
        return this;
    }

    public SearchRequestHelper setSortedByDistance(boolean sortedByDistance) {
        aroundMeArguments.setSortedByDistance(sortedByDistance);
        return this;
    }

    public boolean isSortedByDistance() {
        return aroundMeArguments.isSortedByDistance();
    }

    public boolean hasMoreItems() {
        return aroundMeArguments.hasMoreItems();
    }

    public SearchType getType() {
        return aroundMeArguments.getType();
    }

    public AroundMeArguments getAroundMeArguments() {
        return aroundMeArguments;
    }

    public SearchCommand getSearchCommand() {
        return aroundMeArguments.getSearchCommand();
    }

    public ServiceCommand buildNewSearchCommand(SearchRequestType searchRequestType) {
        this.searchRequestTypeActive = searchRequestType;
        SearchCommand searchCommand = aroundMeArguments.getSearchCommand();
        if (searchCommand != null) {
            final SearchCommand searchCommandNew = new SearchRequestBuilder(searchCommand).build();
            if (searchCommandNew.compare(searchCommand) != SearchCommand.EQUAL) {
                aroundMeArguments.setSearchCommand(searchCommandNew);
                return getSearchCommand();
            }
        }
        if (searchCommand == null || searchRequestType == SearchRequestType.IMMEDIATELY ||
                searchRequestType == SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS) {
            SearchRequestBuilder builder;
            if (searchCommand != null) {
                builder = new SearchRequestBuilder(searchCommand);
            } else {
                builder = new SearchRequestBuilder(aroundMeArguments.getType());
                if (aroundMeArguments.isCity()) {
                    builder.setLocation(aroundMeArguments.getCity());
                    // TODO server don't work without this
//                    builder.setGeo(aroundMeArguments.getLatitude(), aroundMeArguments.getLongitude());
                } else {
                    builder.setGeo(aroundMeArguments.getLatitude(), aroundMeArguments.getLongitude());
                }
                builder.setItemsPerPage(10);
                builder.setAccuracy(100);
                if ((searchRequestType != SearchRequestType.IMMEDIATELY ||
                        searchRequestType != SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS) && searchCommand != null) {
                    throw new IllegalArgumentException("");
                }
            }

            SearchCommand command;
            if (searchRequestType == SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS) {
                filtersHelper.clearSearchFilters(aroundMeArguments.getType());
                command = builder.buildWithoutFilters();
            } else {
                command = builder.build();
            }

            aroundMeArguments.setSearchCommand(command);
        }
        return getSearchCommand();
    }

    @Override
    public void restore() {
        this.filtersHelper.init(getFiltersHelperFromStore());
    }

    private FiltersHelper getFiltersHelperFromStore() {
        final AroundMeArguments parcelable = bundle.getParcelable(AROUND_ME_ARGUMENTS);
        if (parcelable != null) {
            aroundMeArguments = parcelable;
        }
//        FiltersHelper filtersHelper = (FiltersHelper) bundle.getSerializable(FILTERS_HELPER);
        return gson.fromJson(serializedObject, FiltersHelper.class);
    }

    @Override
    public void save() {
        bundle.putParcelable(AROUND_ME_ARGUMENTS, aroundMeArguments);
        bundle.putSerializable(FILTERS_HELPER, filtersHelper);
        serializedObject = gson.toJson(filtersHelper);
    }

    @Override
    public boolean isFilterChanged(ArrayList<SearchFilter> searchFilters, SearchType searchType) {
        FiltersHelper filtersHelper = getFiltersHelperFromStore();
        ArrayList<SearchFilter> filtersFromStore = filtersHelper.getSearchFilters(searchType);
        ArrayList<SearchFilter> usedStore = getJustUsedFilters(filtersFromStore);
        ArrayList<SearchFilter> currentFilters = getJustUsedFilters(searchFilters);
        return !Utils.isEqualsArray(usedStore, currentFilters);
    }

    private ArrayList<SearchFilter> getJustUsedFilters(ArrayList<SearchFilter> searchFilters) {
        ArrayList<SearchFilter> usedFilters = new ArrayList<>();
        SearchFilter searchFilter;
        for (int i = 0; i < searchFilters.size(); i++) {
            searchFilter = searchFilters.get(i);
            if (searchFilter.getValue() != null && !searchFilter.getValue().isEmpty()) {
                usedFilters.add(searchFilter);
            }
        }
        return usedFilters;
    }

    public void update(SearchResponseHuge searchResponseHuge) {
        aroundMeArguments.update(searchResponseHuge);
    }

    public boolean isAlreadyUpdate() {
        boolean bResult = false;
        if (searchRequestTypeCurrent == SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS &&
                searchRequestTypeActive != SearchRequestType.IMMEDIATELY_WITHOUT_FILTERS) {
            bResult = true;
        }
        searchRequestTypeCurrent = searchRequestTypeActive;
        return bResult;
    }
}
