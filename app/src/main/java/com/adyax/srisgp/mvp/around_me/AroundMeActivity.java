package com.adyax.srisgp.mvp.around_me;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityAroundMeBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AroundMeActivity extends MaintenanceBaseActivity {

    @Inject
    FragmentFactory fragmentFactory;
    private ActivityAroundMeBinding binding;

    public static Intent newIntent(Context context, AroundMeArguments aroundMeArguments) {
        Intent intent = new Intent(context, AroundMeActivity.class);
        intent.putExtra(AroundMeArguments.BUNDLE_NAME, aroundMeArguments);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_around_me);
//        binding.createAccountViewPager.setAdapter(new CreateAccountPagerAdapter(getSupportFragmentManager()));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            fragmentFactory.startAroundMeFragment(this, getIntent().getParcelableExtra(AroundMeArguments.BUNDLE_NAME));
//            fragmentFactory.startGetLocationAroundMeFragment(this);
        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for(Fragment f:getSupportFragmentManager().getFragments()) {
            if(f!=null&& f.isVisible()){
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
//        if (requestCode != GeoHelper.REQUEST_CHECK_SETTINGS) {
//            Fragment fragment = fragmentFactory.getCurrentFragment(this);
//            if (fragment != null)
//                fragment.onActivityResult(requestCode, resultCode, data);
//        }
    }

}
