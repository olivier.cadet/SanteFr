package com.adyax.srisgp.mvp.searchhome.alerts;

import android.content.Context;
import android.database.Cursor;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.db.IDataBaseHelper;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by SUVOROV on 10/7/16.
 */

public class AlertsHelper implements IAlerts {

    private Context context;
    private IDataBaseHelper dataBaseHelper;

    public AlertsHelper(Context context, IDataBaseHelper dataBaseHelper) {
        this.context = context;
        this.dataBaseHelper = dataBaseHelper;
    }

    @Override
    public boolean isEmpty() {
        final int countAlerts = dataBaseHelper.getCountAlerts();
//        setBadge(countAlerts);
        return countAlerts == 0;
    }

    @Override
    public void setBadge(int count) {
        if (count == 0) {
            ShortcutBadger.removeCount(context);
        } else {
            ShortcutBadger.applyCount(context, count);
        }
    }

    @Override
    public AlertItem getUrgentAlert(){
        final Cursor cursor = dataBaseHelper.getUrgentAlerts();
        if(cursor!=null&& cursor.moveToFirst()) {
            AlertItem alertItem = new AlertItem();
            BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, alertItem);
            return alertItem;
        }
        return null;
    }

    @Override
    public Cursor getAlerts() {
        return dataBaseHelper.getAlerts(false);
    }



}
