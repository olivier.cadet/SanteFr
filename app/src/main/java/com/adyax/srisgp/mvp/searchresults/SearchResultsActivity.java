package com.adyax.srisgp.mvp.searchresults;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.adyax.srisgp.mvp.around_me.AroundMeFragment;
import com.adyax.srisgp.mvp.around_me.IAroundMe;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;
import com.adyax.srisgp.mvp.search.FiltersActivity;
import com.adyax.srisgp.mvp.search.SearchActivity;
import com.adyax.srisgp.mvp.search.SearchArgument;
import com.adyax.srisgp.mvp.search.SearchFragment;
import com.adyax.srisgp.mvp.searchresults.find.FindSearchResultsFragment;
import com.adyax.srisgp.presentation.CustomViewPager;
import com.adyax.srisgp.utils.FiltersHelper;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.common.api.Status;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 10/11/16.
 */

public class SearchResultsActivity extends MaintenanceBaseActivity implements SearchResultsListener, FindSearchResultsFragment.FindSearchResultsListener, AroundMeFragment.SearchResultsAroundMeListener {
    private static final int REQUEST_CHECK_SETTINGS = 1;

    private static final int FILTERS_ACTIVITY_REQUEST_CODE = 100;
    private static final int SEARCH_ACTIVITY_REQUEST_CODE = 101;

    public static final int INFO_PAGE = 0;
    public static final int FIND_PAGE = 1;
    public static final int UNKNOWN_PAGE = -1;
    private int item = UNKNOWN_PAGE;

    @Inject
    FragmentFactory fragmentFactory;
    @Inject
    FiltersHelper filtersHelper;
    @Inject
    IRepository repository;
    @Inject
    ITracker tracker;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    CustomViewPager pager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.tvAroundMe)
    TextView tvAroundMe;
    @BindView(R.id.ivFilters)
    ImageView ivFilters;
    @BindView(R.id.ivLocation)
    ImageView ivLocation;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivClear)
    ImageView ivClear;

    private boolean sortToggle = false; // false = pertinence

    private SearchResultsPagerAdapter pagerAdapter;

    private SearchArgument searchArgument;

    private Boolean infoEmpty;
    private Boolean findEmpty;
    private boolean mapState = false;

    private boolean bStartAroundMe;
    private FindSearchResponseData findSearchResponseData = new FindSearchResponseData();

    private boolean openedRequiredTab = false;

    private ViewPager.OnPageChangeListener onPageChangeListener;

    public static Intent newIntent(Context context, SearchArgument searchArgument) {//1
        Intent intent = new Intent(context, SearchResultsActivity.class);
        intent.putExtra(SearchArgument.BUNDLE_NAME, searchArgument);
        return intent;
    }

    private void setResultAndFinish(int result) {
        if (searchArgument.isQuickcardMode()) {
            Intent intent = SearchActivity.newIntent(getApplicationContext(),
                    result == SearchFragment.EDIT_CLICKED ? searchArgument : new SearchArgument(""));
            startActivityForResult(intent, SEARCH_ACTIVITY_REQUEST_CODE);
        } else {
            Intent intent = new Intent();
            intent.putExtra(SearchFragment.INTENT_CLICKED, result);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    public String getLocationText() {
        return searchArgument.isAroundMe() ? getString(R.string.search_around_me) : searchArgument.getWhereField();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        ButterKnife.bind(this);
        App.getApplicationComponent().inject(this);

        // Test AppFlyer
        AppsFlyerLib.getInstance().sendDeepLinkData(this);

        // Test AppFlyer

        // Test AppFlyer
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.LEVEL, 9);
        eventValue.put(AFInAppEventParameterName.SCORE, 100);
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.LEVEL_ACHIEVED, eventValue);

        tracker.getHistoryUpdateHelper().reset();

        toolbar.setTitleTextColor(getResources().getColor(R.color.material_black));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        ivEdit.setOnClickListener(v -> {
            setResultAndFinish(SearchFragment.EDIT_CLICKED);
        });

        ivClear.setOnClickListener(v -> {
            setResultAndFinish(SearchFragment.CLEAR_CLICKED);
        });

        searchArgument = (savedInstanceState != null) ? savedInstanceState.getParcelable(SearchArgument.BUNDLE_NAME) :
                getIntent().getParcelableExtra(SearchArgument.BUNDLE_NAME);

        ivLocation.setVisibility(View.INVISIBLE);
        pagerAdapter = new SearchResultsPagerAdapter(position -> {
            SearchType searchType1 = (position == 0) ? SearchType.info : SearchType.find;
            if (position == 0) {
                Fragment fragment = pagerAdapter.getRegisteredFragment(0);
                if (fragment != null) {
                    return fragment;
                }
            }

            if (position == 1 && mapState) {
                pager.setPagingEnabled(false);
                AroundMeArguments aroundMeArguments = new AroundMeArguments(findSearchResponseData.getSearchCommand(), findSearchResponseData.isSortedByDistance(), findSearchResponseData.isHasMoreItems());
                return AroundMeFragment.newInstance(aroundMeArguments);
            } else {
                pager.setPagingEnabled(true);
            }

            Fragment fragment;
            if (searchArgument.isAroundMe()) {
                fragment = SearchResultsFragment.newInstanceAroundMe(searchArgument.getWhatField(), searchArgument.getLatitude(), searchArgument.getLongitude(), searchType1);
            } else {
                fragment = SearchResultsFragment.newInstance(searchArgument.getWhatField(), getLocationText(), searchType1);
            }

            if (fragment instanceof FindSearchResultsFragment) {
                ((FindSearchResultsFragment) fragment).setVisible(true);
            }

            return fragment;
        }, getSupportFragmentManager(), this);

        onPageChangeListener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Timber.d(position + "++=");
                final Fragment fragment = pagerAdapter.getRegisteredFragment(FIND_PAGE);
                if (item != UNKNOWN_PAGE) {
                    addScreenViewResearch(position);
                }
                if (fragment != null) {
                    tracker.getHistoryUpdateHelper().tabChanged(position);
                    switch (position) {
                        case INFO_PAGE:
                            ivLocation.setVisibility(View.INVISIBLE);
                            pager.setPagingEnabled(true);

                            if (fragment instanceof FindSearchResultsFragment) {
                                FindSearchResultsFragment fragment1 = (FindSearchResultsFragment) fragment;
                                fragment1.setVisible(false);
                            }
                            setFilterVisible(INFO_PAGE);
                            break;
                        case FIND_PAGE:
                            if (fragment instanceof AroundMeFragment) {
                                ivLocation.setVisibility(View.INVISIBLE);
                                ivFilters.setVisibility(View.INVISIBLE);
                                pager.setPagingEnabled(false);
                            } else {
                                ((FindSearchResultsFragment) fragment).setVisible(true);
                                setFilterVisible(FIND_PAGE);
                            }
                            break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        pager.addOnPageChangeListener(onPageChangeListener);
//        pager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(pager);

        updateInfo();

        ivFilters.setOnClickListener(v -> {
            SearchType searchType = tabLayout.getSelectedTabPosition() == 0 ? SearchType.info : SearchType.find;
            openFilters(searchType);
        });

        ivLocation.setOnClickListener(v -> {
            setMap();
        });

        //new filters for each search
        filtersHelper.clearSearchFilters(SearchType.find);
        filtersHelper.clearSearchFilters(SearchType.info);
    }

    private void updateInfo() {
        pager.setAdapter(pagerAdapter);
        tvSearch.setText(searchArgument.getWhatField());
        if (getLocationText().isEmpty()) {
            tvAroundMe.setVisibility(View.GONE);
        } else {
            tvAroundMe.setVisibility(View.VISIBLE);
            tvAroundMe.setText(getLocationText());
            tvAroundMe.setSelected(searchArgument.isAroundMe());
        }
        pager.post(() -> onPageChangeListener.onPageSelected(pager.getCurrentItem()));
    }

    private void setFilterVisible(int index) {
        Fragment fragment = pagerAdapter.getRegisteredFragment(index);
        if (fragment instanceof SearchResultsFragment) {
            final SearchResultsFragment searchResultsFragment = (SearchResultsFragment) fragment;
            ivFilters.setVisibility(searchResultsFragment.isFilterButtonShouldBeShown() ? View.VISIBLE : View.INVISIBLE);
        } else {
            ivFilters.setVisibility(View.VISIBLE);
        }
    }

    private void setMap() {
        mapState = !mapState;
        if (mapState) {
            ivLocation.setVisibility(View.INVISIBLE);
            ivFilters.setVisibility(View.INVISIBLE);
        } else {
            ivLocation.setVisibility(View.VISIBLE);
            ivFilters.setVisibility(View.VISIBLE);
        }

        CharSequence infoTitle = tabLayout.getTabAt(INFO_PAGE).getText();
        CharSequence findTitle = tabLayout.getTabAt(FIND_PAGE).getText();
        pagerAdapter.notifyDataSetChanged();
        tabLayout.getTabAt(INFO_PAGE).setText(infoTitle);
        tabLayout.getTabAt(FIND_PAGE).setText(findTitle);
    }

    private void refreshData() {
        int currentItem = pager.getCurrentItem();

        if (currentItem == INFO_PAGE) {
            SearchResultsFragment results1 = (SearchResultsFragment) pagerAdapter.getRegisteredFragment(INFO_PAGE);
            if (results1 != null) {
                results1.onRefreshData((SearchCommand)null);
            }
        } else if (currentItem == FIND_PAGE) {
            Fragment fragment2 = pagerAdapter.getRegisteredFragment(FIND_PAGE);
            if (fragment2 != null) {
                if (fragment2 instanceof SearchResultsFragment) {
                    final SearchCommand lastSearchRequest = repository.readLastSearchRequest(SearchType.find);
                    ((SearchResultsFragment) fragment2).onRefreshData((SearchCommand)lastSearchRequest);
                } else {
                    ((IAroundMe) fragment2).onFiltersChanged();
                }
            }
        }
    }

    @Override
    public void onRemoveSearchMessage(SearchMessage searchMessage) {
        SearchResultsFragment results1 = (SearchResultsFragment) pagerAdapter.getRegisteredFragment(INFO_PAGE);
        if (results1 != null) {
            results1.onSearchMessageRemoved(searchMessage);
        }

        Fragment fragment2 = pagerAdapter.getRegisteredFragment(FIND_PAGE);
        if (fragment2 != null) {
            if (fragment2 instanceof SearchResultsFragment) {
                ((SearchResultsFragment) fragment2).onSearchMessageRemoved(searchMessage);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openFilters(SearchType searchType) {
        FiltersActivity.startForResult(this, searchType, FILTERS_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void showFindCount(int count) {
        tabLayout.getTabAt(FIND_PAGE).setText(getString(R.string.search_results_tab_professionals, count));
    }

    @Override
    public void showGetInformedCount(int count) {
        tabLayout.getTabAt(INFO_PAGE).setText(getString(R.string.search_results_tab_get_informed, count));
    }

    @Override
    public void onFirstPageLoaded(SearchType searchType, boolean empty) {
        if (searchType == SearchType.info) {
            infoEmpty = empty;
        } else if (searchType == SearchType.find) {
            findEmpty = empty;
        }

        if (infoEmpty != null && findEmpty != null && infoEmpty && !findEmpty) {
            openFindTab();
        }
    }

    @Override
    public void onNoResults(SearchType searchType) {
        if (searchType == SearchType.info) {
            infoEmpty = true;
        } else if (searchType == SearchType.find) {
            findEmpty = true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                ((FindSearchResultsFragment) pagerAdapter.getRegisteredFragment(FIND_PAGE))
                        .onPromptSettingsResult(resultCode == Activity.RESULT_OK);
                break;
            case FILTERS_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    refreshData();
                }
                break;
            case SEARCH_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    searchArgument = data.getParcelableExtra(SearchArgument.BUNDLE_NAME);
                    findSearchResponseData.setSearchCommand(null);
                    updateInfo();
                }
                break;
        }
        for(Fragment f:getSupportFragmentManager().getFragments()) {
            if(f!=null&& f.isVisible()){
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onPromptLocation(Status status) {
        try {
            status.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        searchArgument.updateAroundMe(location.getLatitude(), location.getLongitude());
        tvAroundMe.setVisibility(View.VISIBLE);
        tvAroundMe.setText(R.string.search_around_me);
        tvAroundMe.setSelected(searchArgument.isAroundMe());
        ((SearchResultsFragment) pagerAdapter.getRegisteredFragment(INFO_PAGE)).onLocationUpdated(location);
        ((SearchResultsFragment) pagerAdapter.getRegisteredFragment(FIND_PAGE)).onLocationUpdated(location);
        if (BuildConfig.DEBUG) {
            Toast.makeText(this, location.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(searchArgument.BUNDLE_NAME, searchArgument);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onReminderClose() {
        ((SearchResultsFragment) pagerAdapter.getRegisteredFragment(INFO_PAGE)).onReminderClosed();
        ((SearchResultsFragment) pagerAdapter.getRegisteredFragment(FIND_PAGE)).onReminderClosed();
    }

    @Override
    public void onFindRelateItemClicked(RelatedItem item) {
        if (item.filters != null) {
            SearchFilter searchFilter = new SearchFilter(item.getFilter().type, new String[]{item.getFilter().value});
            setCurrentPage(INFO_PAGE);
            SearchResultsFragment info = (SearchResultsFragment) pagerAdapter.getRegisteredFragment(0);
            if (info != null) {
                info.onRefreshData(searchFilter);
            }
        }
    }

    @Override
    public void onInfoRelatedItemClicked(RelatedItem item) {
        if (item.filters != null) {
            SearchFilter searchFilter = new SearchFilter(item.getFilter().type, new String[]{item.getFilter().value});
            setCurrentPage(FIND_PAGE);
            Fragment fragment = pagerAdapter.getRegisteredFragment(1);

            // TODO: no action in case of fragment instance of AroundMeFragment
            if (fragment != null && fragment instanceof FindSearchResultsFragment) {
                ((FindSearchResultsFragment) fragment).onRefreshData(searchFilter);
            }
        }
    }

    @Override
    public void onChangeFilterActionButtonVisibility(boolean visible, Fragment fragment) {
        Fragment currentFragment = pagerAdapter.getRegisteredFragment(pager.getCurrentItem());
        if (currentFragment == fragment) {
            ivFilters.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public void onChangeMapActionButtonVisibility(boolean visible) {
        ivLocation.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);//AAA3 false true
    }

    @Override
    public void onFiltersButtonClicked() {
//        ivFilters.performClick();
    }

    @Override
    public void onListButtonClicked() {
        Fragment fragment = pagerAdapter.getRegisteredFragment(1);
        if (fragment != null && fragment instanceof AroundMeFragment) {
            SearchCommand findCommand = ((IAroundMe) fragment).getSearchCommand();
            boolean hasMoreItems = ((IAroundMe) fragment).hasMoreItems();
            boolean sortedByDistance = ((IAroundMe) fragment).isSortedByDistance();
            findSearchResponseData.setSearchCommand(findCommand);
            findSearchResponseData.setSortedByDistance(sortedByDistance);
            findSearchResponseData.setHasMoreItems(hasMoreItems);
            new TrackerHelper().addScreenViewResearch(findCommand, TrackerHelper.LISTE_MODE);
            ivLocation.performClick();
        }
    }

    @Override
    public void openFindTab() {
        if (!openedRequiredTab) {
            setCurrentPage(FIND_PAGE);
            openedRequiredTab = true;
        }
    }

    @Override
    public void openInfoTab() {
        if (!openedRequiredTab) {
            setCurrentPage(INFO_PAGE);
            openedRequiredTab = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bStartAroundMe) {
            bStartAroundMe = false;
            setMap();
        }
    }

    @Override
    public FindSearchResponseData getResponseData() {
        return findSearchResponseData;
    }

    public static class FindSearchResponseData {
        private SearchCommand searchCommand;
        private boolean hasMoreItems;
        private boolean sortedByDistance;

        public SearchCommand getSearchCommand() {
            return searchCommand;
        }

        public void setSearchCommand(SearchCommand searchCommand) {
            this.searchCommand = searchCommand;
        }

        public boolean isHasMoreItems() {
            return hasMoreItems;
        }

        public void setHasMoreItems(boolean hasMoreItems) {
            this.hasMoreItems = hasMoreItems;
        }

        public boolean isSortedByDistance() {
            return sortedByDistance;
        }

        public void setSortedByDistance(boolean sortedByDistance) {
            this.sortedByDistance = sortedByDistance;
        }
    }

    private void setCurrentPage(int item) {
        pager.setCurrentItem(item);
    }

    @Override
    public void addScreenViewResearch(int item) {
        if (this.item != item) {
            this.item = item;
            final SearchCommand searchCommand = getResponseData().getSearchCommand().getClone();
            final Fragment fragment = pagerAdapter.getRegisteredFragment(FIND_PAGE);
            if (item == INFO_PAGE) {
                searchCommand.setType(SearchType.info);
            }
            final boolean aroundMePage = fragment != null && fragment instanceof AroundMeFragment;
            new TrackerHelper().addScreenViewResearch(searchCommand,
                    aroundMePage ? TrackerHelper.CARTE_MODE : TrackerHelper.LISTE_MODE);
        }
    }
}
