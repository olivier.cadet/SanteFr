package com.adyax.srisgp.mvp.around_me.get_location;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.databinding.FragmentGetCityBinding;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.search.RecentLocationViewModel;
import com.adyax.srisgp.mvp.search.SearchLocationsAdapter;
import com.adyax.srisgp.mvp.search.SearchLocationsPresenter;
import com.adyax.srisgp.utils.PermissionUtils;
import com.google.android.gms.common.api.Status;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 11/1/16.
 */

public class GetCityFragment extends ViewFragment implements SearchLocationsPresenter.SearchLocationsView {

    public static final String INTENT_LONGITUDE = "longitude";
    public static final String INTENT_LATITUDE = "latitude";
    public static final String INTENT_STATUS_LOCATION = "city";

    public static final String LAST_LOCATION_KEY = "lastLocation";

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    @Inject
    SearchLocationsPresenter locationsPresenter;

    private FragmentGetCityBinding binding;
    private SearchLocationsAdapter locationsAdapter;

    private boolean aroundMeState = false;
    private boolean aroundMeClicked = false;

    public boolean bFFF = false;
    public boolean bAAA = false;

    private ProvideLocationFragment.OnPromptLocationSettingsListener listener;

    public static GetCityFragment newInstance() {
        Bundle args = new Bundle();
        GetCityFragment fragment = new GetCityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static GetCityFragment newInstance(String lastLocation) {
        Bundle args = new Bundle();
        args.putString(LAST_LOCATION_KEY, lastLocation);
        GetCityFragment fragment = new GetCityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
        listener = (ProvideLocationFragment.OnPromptLocationSettingsListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_get_city, container, false);
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        locationsPresenter.loadAutocompleteData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationsPresenter.onRequestCurrentLocation(aroundMeClicked);
            } else {
                if (PermissionUtils.isNotAskAgainLocationPermission(getActivity())) {
                    showLocationPermissionNotAskAgain();
                } else {
                    showLocationPermissionDenied();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
    }

    private String getLastLocation() {
        if (getArguments().containsKey(LAST_LOCATION_KEY))
            return getArguments().getString(LAST_LOCATION_KEY);
        return "";
    }

    public void onPromptSettingsResult(boolean success) {
        if (success) {
            locationsPresenter.onPromptSettingSuccess(aroundMeClicked);
        } else {
            showLocationNotEnabled();
        }
    }

    private void bindViews() {
        String lastLocation = getLastLocation();
        if (lastLocation != null && !lastLocation.isEmpty()) {
//            if(TextUtils.equals(lastLocation, getString(R.string.search_around_me))){
//                binding.etSearch.setHint(lastLocation);
//            }
            binding.etSearch.setText(lastLocation);
            binding.etSearch.setSelection(lastLocation.length());
        }

        binding.iBtnClear.setVisibility(binding.etSearch.length() == 0 ? View.GONE : View.VISIBLE);

        binding.iBtnClear.setOnClickListener(v -> {
            binding.etSearch.setText("");
            if (binding.etSearch.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        binding.iBtnDismiss.setOnClickListener(v -> {
            getActivity().finish();
            getActivity().overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        });

        locationsAdapter = new SearchLocationsAdapter(new SearchLocationsAdapter.OnItemClickedLister() {
            @Override
            public void onRemoveSearchEntry(RecentLocationViewModel searchItem) {
                locationsAdapter.onSetProgressForRecentItem(searchItem);
                locationsPresenter.removeSearchLocation(searchItem);
            }

            @Override
            public void onSelectAutocomplete(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
                if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    locationsPresenter.onSearchLocationSelected(statusLocation);
                } else {
                    locationsPresenter.onRequestCurrentLocation(true, statusLocation);
                }
            }

            @Override
            public void onProvideLocationClicked() {
                aroundMeClicked = false;
                if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    requestLocationPermissions();
                } else {
                    locationsPresenter.onRequestCurrentLocation(false);
                }
            }

            @Override
            public void onAroundMeClicked() {
                aroundMeClicked = true;
                if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    requestLocationPermissions();
                } else {
                    locationsPresenter.onRequestCurrentLocation(true);
                }
            }

            @Override
            public void onRemoveSearchLocationsHistory() {
                locationsAdapter.onSetProgressForSearchHistory();
                locationsPresenter.removeSearchLocationsHistory();
            }

            @Override
            public boolean hasLocationPermissions() {
                return PermissionUtils.isProviderEnabled(getContext()) && PermissionUtils.hasPermissionsForRequestingLocation(getContext());
            }
        });

        binding.rvAutocomplete.setHasFixedSize(true);
        binding.rvAutocomplete.setAdapter(locationsAdapter);
        binding.rvAutocomplete.setLayoutManager(new LinearLayoutManager(getContext()));

        binding.etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    locationsPresenter.onLocationFieldFocus(binding.etSearch.getText().toString());
                }
            }
        });

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (!bFFF) {
                    if (TextUtils.equals(s, getString(R.string.search_around_me))) {
                        bFFF = true;
                        bAAA = true;
                    }
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (bAAA) {
                    bAAA = false;
                    String aaa = String.valueOf(s);
                    String string = getString(R.string.search_around_me);
                    if (aaa != null && aaa.startsWith(string)) {
                        String substring = aaa.substring(string.length());
                        binding.etSearch.setText(substring);
                        binding.etSearch.setSelection(substring.length());
                    }
                }
                binding.iBtnClear.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                locationsPresenter.onLocationFieldTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (aroundMeState) {
                    s.clear();
                    aroundMeState = false;
                }
            }
        });
    }

    @Override
    public String getLocationText() {
        return binding.etSearch.getText().toString();
    }

    @Override
    public void onAutoCompleteSearchLocationsIsEmpty() {

    }

    private void requestLocationPermissions() {
        PermissionUtils.requestLocationPermissions(this, REQUEST_LOCATION_PERMISSION);
    }

    @Override
    public boolean isLocationFieldHasFocus() {
        return binding.etSearch.hasFocus();
    }

    @Override
    public void onSearchLocationsHistoryRemoved() {
        locationsAdapter.onRemovedSearchHistory();
    }

    @Override
    public void onRecentSearchLocationRemoved(RecentLocationViewModel searchItem) {
        locationsAdapter.onRemovedSearchEntry(searchItem);
    }

    @Override
    public void showLocationsDefaultState(List<RecentLocationViewModel> recentLocations) {
        locationsAdapter.showDefaultState(recentLocations);
    }

    @Override
    public void showLocationsSearchState(GetAutoCompleteSearchLocationsResponse autocompleteItems, ConfigurationResponse config) {
        locationsAdapter.showSearchState(autocompleteItems);
    }

    @Override
    public void setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        binding.etSearch.setText(R.string.search_around_me);
        binding.etSearch.setSelection(binding.etSearch.getText().length());
        aroundMeState = true;

        Intent intent = new Intent();
        intent.putExtra(INTENT_LATITUDE, location.getLatitude());
        intent.putExtra(INTENT_LONGITUDE, location.getLongitude());
        if (statusLocation != null) {
            intent.putExtra(INTENT_STATUS_LOCATION, statusLocation);
        }
        setResultAndFinish(intent);
    }

    @Override
    public void onEnableLocationButtonStateChanged() {
        locationsAdapter.onEnableLocationButtonStateChanged();
    }

    @Override
    public void showEnableLocationDialog(Status status) {
        listener.onPromptLocation(status);
    }

    @Override
    public void updateLocation(Location location) {

    }

    @Override
    public void onSelectSearchLocationEntry(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, ConfigurationResponse config, boolean bAutoSelect) {
        final String text = statusLocation.getLocation();
        binding.etSearch.setText(text);
        binding.etSearch.setSelection(text.length());

        if (statusLocation.isActive()) {
            closeWindow(statusLocation);
        } else {
////            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
////            builder.setMessage(config.getSearch().getAutocomplete_inactive_region())
////                    .setCancelable(true)
////                    .setPositiveButton(R.string.yes, (dialogInterface, i) -> closeWindow(statusLocation));
////            builder.create().show();
//            DialogUtils.showBadRegionDialog(getContext(), config, (dialogInterface, i) -> closeWindow(statusLocation));
            closeWindow(statusLocation);
        }
    }

    private void closeWindow(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        final Intent intent = new Intent();
        intent.putExtra(INTENT_STATUS_LOCATION, statusLocation);
        setResultAndFinish(intent);
    }

    private void setResultAndFinish(Intent intent) {
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return locationsPresenter;
    }

    @Override
    public void disableSearchSuggestionAdapter() {
//        rvAutocomplete.setAdapter(null);
    }
}
