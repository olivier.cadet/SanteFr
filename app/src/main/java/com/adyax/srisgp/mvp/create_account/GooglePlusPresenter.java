package com.adyax.srisgp.mvp.create_account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3.PointsOfInterest_3_Presenter;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by SUVOROV on 1/25/17.
 */

public abstract class GooglePlusPresenter<V extends IView> extends PresenterAppReceiver<V> {

    private String TAG = this.getClass().getSimpleName();
    protected static final int RC_SIGN_IN = 1002;
    private GoogleApiClient googleApiClient;

    @Override
    public void detachView() {
        if (getView() != null && googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                googleApiClient.disconnect();
            }
        }
        super.detachView();
    }

    protected void startConnectGooglePlus() {
        googleApiClient.connect();
    }

    protected void initGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.google_plus))
                .requestServerAuthCode(getString(R.string.google_plus))
                .build();

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        Auth.GoogleSignInApi.signOut(googleApiClient);
                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                        ((Activity) getContext()).startActivityForResult(signInIntent, RC_SIGN_IN);
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        Log.d(TAG, "Google onConnectionSuspended");
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d(TAG, "Google onConnectionFailed");
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
//        googleApiClient.connect();
    }

    private void loginGoogle(String id, String token, String email) {
        final IViewPager activity = (IViewPager) getContext();
        Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
        if (emailResult == null) {
            activity.getCreateAccountHelper().setSsoRegister(Sso_type.sgp_sso_google, id, email, token);
            activity.getCreateAccountHelper().sendLoginSsoCommand(getContext(), getAppReceiver(), this instanceof PointsOfInterest_3_Presenter);
        } else {
            Toast.makeText(getContext(), emailResult, Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GooglePlusPresenter.RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                String id = account.getId();
                String email = account.getEmail();
                String tokenID = account.getIdToken();
//            String tokenID = account.getServerAuthCode();
                loginGoogle(id, tokenID, email);
            }
        }
    }
}
