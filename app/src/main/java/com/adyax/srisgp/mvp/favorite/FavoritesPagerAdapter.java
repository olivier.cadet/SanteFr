package com.adyax.srisgp.mvp.favorite;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.adyax.srisgp.R;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public class FavoritesPagerAdapter extends FragmentStatePagerAdapter {
    private int[] titleResources = new int[] {R.string.notification_all_title, R.string.favorite_professionals, R.string.favorite_get_informed};
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private Context context;

    public FavoritesPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return FavoritesFragment.newInstance(FavoriteType.all);
        } else if (position == 1) {
            return FavoritesFragment.newInstance(FavoriteType.find);
        } else {
            return FavoritesFragment.newInstance(FavoriteType.get_informed);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(titleResources[position]);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }
}
