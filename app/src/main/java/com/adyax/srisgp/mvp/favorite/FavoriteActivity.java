package com.adyax.srisgp.mvp.favorite;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

/**
 * Created by anton.kobylianskiy on 9/30/16.
 */

public class FavoriteActivity extends MaintenanceBaseActivity {
    private static final String EXTRA_FAVORITE = "favorite";

    public static Intent newIntent(Context context, FavoritesItem favoritesItem) {
        Intent intent = new Intent(context, FavoriteActivity.class);
        intent.putExtra(EXTRA_FAVORITE, favoritesItem);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null) {
                FavoritesItem item = intent.getParcelableExtra(EXTRA_FAVORITE);
                replaceFragment(R.id.frame, FavoriteFragment.newInstance(item));
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
