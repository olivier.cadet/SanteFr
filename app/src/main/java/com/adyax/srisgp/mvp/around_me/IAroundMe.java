package com.adyax.srisgp.mvp.around_me;

import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;

/**
 * Created by SUVOROV on 11/11/16.
 */
public interface IAroundMe {
    void onFiltersChanged();
    boolean hasMoreItems();
    boolean isSortedByDistance();
    SearchCommand getSearchCommand();
}
