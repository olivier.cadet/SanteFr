package com.adyax.srisgp.mvp.dummy;

import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class DummyPresenter extends Presenter<DummyPresenter.DummyView> {

    public interface DummyView extends IView {
    }

    @Inject
    public DummyPresenter() {
    }

}
