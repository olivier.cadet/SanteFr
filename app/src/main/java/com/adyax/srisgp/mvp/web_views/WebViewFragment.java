package com.adyax.srisgp.mvp.web_views;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.tags.AnchorItem;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.FragmentWebviewBinding;
import com.adyax.srisgp.di.modules.NetworkModule;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.GeoHelper;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.my_account.CguDialogActivity;
import com.adyax.srisgp.mvp.web_views.summary_web_view.SummaryWebViewFragment;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;
import com.adyax.srisgp.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import timber.log.Timber;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class WebViewFragment extends ViewFragment implements WebViewPresenter.WebViewView {

    public static final int ERROR404 = 404;

    public static final String EXTRA_RESULT_FAVORITE = "favorite";
    public static final String EXTRA_RESULT_ID = "id";

    public static final int REQUEST_CODE = 12132;
    public static final int TO_TOP = 0;
    public static final int TO_BOTTOM = 1;
    public static final String DIRECT_COMMAND = "DIRECT_COMMAND";
    public static final String ANCHOR_MARKER = "#";

    private static final String ARG_SHOW_FAVORITE = "arg_show_favorite";
    private static final int REQUEST_STORAGE_PERMISSION = 1;
    public static final String TEL = "tel";
    public static final String PDF = ".pdf";
    public static final String TAB_MARKER = "#tab";
    public static final String MAP = "#map";

    public static final int BOTTOM_UNKNOWN = -1;
    public static final int BOTTOM_MENU_NO = 1;
    public static final int BOTTOM_MENU_YES = 2;
    public static final int BOTTOM_MENU_SIGNALER = 3;
    public static final int BOTTOM_EMERGENCY = 4;

    //    public static final String OUI_BUTTON_CLASS = "btn btn-border btn-oui feedback-button tracking feedback-processed attrack-processed";
    public static final String OUI_BUTTON_CLASS = "btn btn-border btn-oui tracking";
    //    public static final String NON_BUTTON_CLASS = "btn btn-border btn-non feedback-button tracking feedback-processed attrack-processed";
    public static final String NON_BUTTON_CLASS = "btn btn-border btn-non tracking";
    //    public static final String SYNG_BUTTON_CLASS = "block-title";
    public static final String SYNG_BUTTON_CLASS = "btn-link tracking";
    public static final String REFERER = "referrer";
    public static final String HOME_PAGE_REFERER = "/";
    public static final String EMBEDDED_MAP_LINK_CLASS = "embedded-map-link";


//    public static final String OUT_BUTTON_CLASS_FIX = "btn btn-border btn-oui tracking attrack-processed";
//    public static final String NON_BUTTON_CLASS_FIX = "btn btn-border btn-non tracking attrack-processed";
//    public static final String SYNG_BUTTON_CLASS_FIX = "btn-link tracking attrack-processed";

    public static final String EMERGENCY_BUTTON_CLASS = "emergency__button";
    public static final String EMERGENCY_BUTTON_CLASS2 = "emergency__button-wrapper";

    private WebPageType lastWebPageType;
    public String lastStartUrl;

    private boolean is404Loaded = false;
    private boolean loadingFinished = false;

    @Inject
    WebViewPresenter webViewPresenter;
    @Inject
    FragmentFactory fragmentFactory;
    @Inject
    IGeoHelper geoHelper;
    @Inject
    IRepository repository;

    private FragmentWebviewBinding binding;
//    private WebViewArg webViewArg;

    private Map<String, String> additionalHttpHeaders = new HashMap<>();

    private CallbackManager facebookCallbackManager;
    private ShareDialog facebookShareDialog;
    private String saveUrl;
    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(downloadId);
            DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
            Cursor cur = manager.query(query);
            if (cur.moveToFirst()) {
                int columnIndex = cur.getColumnIndex(DownloadManager.COLUMN_STATUS);
                if (DownloadManager.STATUS_SUCCESSFUL == cur.getInt(columnIndex)) {
                    final String titleString = cur.getString(cur.getColumnIndex(DownloadManager.COLUMN_URI));
                    if (TextUtils.equals(lastStartUrl, titleString)) {

                        final String fileName = cur.getString(cur.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        final Uri uri = Uri.parse(fileName);
                        File file = new File(uri.getPath());

                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1);
                        String type = mime.getMimeTypeFromExtension(ext);
                        try {
                            Intent intent2 = new Intent();
                            intent2.setAction(Intent.ACTION_VIEW);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                intent2.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                Uri contentUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".fileProvider", file);
                                intent2.setDataAndType(contentUri, type);
                                startActivity(intent2);
                            } else {
//                                    intent2.setDataAndType(Uri.fromFile(file), type);
                                startActivity(Utils.makeIntentForOpenPDF(uri));
                            }
                        } catch (ActivityNotFoundException anfe) {

                        } catch (Exception e) {

                        }
                    }
                }
            }
        }
    };

    public WebViewFragment() {

    }

    public static Fragment newInstance(WebViewArg webViewArg) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putParcelable(WebViewArg.CONTENT_URL, webViewArg);
        fragment.setArguments(args);
        return fragment;
    }

    public static Fragment newInstance(WebViewArg webViewArg, boolean showFavorite) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_FAVORITE, showFavorite);
        args.putParcelable(WebViewArg.CONTENT_URL, webViewArg);
        fragment.setArguments(args);
        return fragment;
    }

    public boolean isShowFavorite() {
        return getArguments().getBoolean(ARG_SHOW_FAVORITE, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case R.id.action_share:
                getPresenter().clickShare();
                break;
            case R.id.action_bookmark:
                getPresenter().setBookMark();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onBackPressed() {
        if (binding.webView.canGoBack()) {
            final int size = binding.webView.copyBackForwardList().getSize();
//            if (size >= 3) {
//                final int steps = -(size - 1);
//                binding.webView.goBackOrForward(steps);
//            } else {
            binding.webView.goBack();
//            }
            return true;
        }
        return false;
    }

    @Override
    public String getUrl() {
        return binding.webView.getUrl();
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.web_view, menu);
        menu.findItem(R.id.action_bookmark).setVisible(false).setTitle(" " + getString(R.string.bookmark));
        menu.findItem(R.id.action_share).setVisible(false).setTitle(" " + getString(R.string.share));
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        getPresenter().setWebViewArg(getArguments().getParcelable(WebViewArg.CONTENT_URL));
        additionalHttpHeaders.clear();
        RegisterUserResponse credential = repository.getCredential();
        additionalHttpHeaders.put(NetworkModule.COOKIE, String.format("%s=%s", credential.getSession_name(), credential.getSessid()));
        additionalHttpHeaders.put(NetworkModule.X_REQUESTED_WITH, NetworkModule.I_OS_VIEW);
        additionalHttpHeaders.put(REFERER, HOME_PAGE_REFERER);
        additionalHttpHeaders.put(NetworkModule.X_USER_AGENT, NetworkModule.getVersionString(context));
        super.onAttach(context);
        initFacebook();
        getActivity().registerReceiver(onDownloadComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().unregisterReceiver(onDownloadComplete);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_webview, container, false);
        binding.webView.setOnScrollChangedListener(new TouchyWebView.OnScrollChangedListener() {
            public void onScrollChange(WebView webView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (!is404Loaded) {
                    if (isToTopBottomVisible()) {
                        Timber.d(scrollY + " " + oldScrollY);

                        binding.toTopOfWebview.setVisibility(scrollY > 0 ? View.VISIBLE : View.INVISIBLE);
                    }
                }
            }
        });

        binding.scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (!is404Loaded) {
                    if (isToTopBottomVisible()) {//binding.scrollContainerLayout.getTop()
                        if (binding.toTopOfWebview.getVisibility() != View.VISIBLE && binding.scrollView.canScrollVertically(-1)) {
                            binding.toTopOfWebview.setVisibility(View.VISIBLE);
                        } else if (!binding.scrollView.canScrollVertically(-1) && !binding.webView.canScrollVertically(-1) && binding.toTopOfWebview.getVisibility() == View.VISIBLE) {
                            binding.toTopOfWebview.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
        binding.toTopOfWebview.setOnClickListener(view -> goToTop());
        if (getPresenter().getWebViewArg().getWebPageType() == WebPageType.STATIC_PAGE) {
            binding.summaryWebview.setVisibility(View.INVISIBLE);
            binding.toTopOfWebview.setVisibility(View.INVISIBLE);
        } else if (getPresenter().getWebViewArg().getWebPageType() == WebPageType.CGU_PAGE) {
            binding.summaryWebview.setVisibility(View.INVISIBLE);
            binding.toTopOfWebview.setVisibility(View.INVISIBLE);
            binding.webCguBottom.setVisibility(View.VISIBLE);
            binding.btnAcceptCgu.setOnClickListener(view -> {
                new CguDialogActivity().sendMessage("WEBVIEW_ACCEPTED");
                getActivity().finish();
            });
        } else {
            binding.summaryWebview.setOnClickListener(view -> {
                Fragment fragment = SummaryWebViewFragment.newInstance(getPresenter().getAnchorResponse());
                fragment.setTargetFragment(WebViewFragment.this, REQUEST_CODE);
                ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            });
        }

        // Feedback view
        binding.yesWebview.setOnClickListener(view1 -> {
            getPresenter().clickYes(WebViewPresenter.WIDGET_HELPFUL_FORM_NAME);
        });
        binding.noWebview.setOnClickListener(view12 -> {
            getPresenter().clickNo(WebViewPresenter.WIDGET_HELPFUL_FORM_NAME);
        });
        binding.reportTv.setOnClickListener(view -> clickSignaler("inapp://widget_result"));
        // AroundMe view
        binding.webAroundMe.setOnClickListener(v -> {
            Intent arouneMeIntent = new Intent();
            arouneMeIntent.putExtra(WidgetViewHelper.SHOW_AROUNDME_RESPONSE, true);
            getActivity().setResult(Activity.RESULT_OK, arouneMeIntent);
            getActivity().onBackPressed();
        });
        return binding.getRoot();
    }

    private void clickSignaler(String string) {
        final WebViewArg webViewArg = getPresenter().getWebViewArg();
        webViewArg.setReportFormName(string);
        if (webViewArg.isFindType()) {
            getPresenter().getTrackerHelper().clickSignaler(TrackerLevel.UNIT_CONTENTS_FIND,
                    ClickType.clic_signaler_une_information_erronee);
        } else {
            getPresenter().getTrackerHelper().clickSignaler(TrackerLevel.UNIT_CONTENTS_INFORMATION,
                    ClickType.clic_raporter_une_information_erronee);
        }
        if (webViewArg.isFindTypeForm()) {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                    .setView(R.layout.fragment_report_find_selection)
                    .show();
            alertDialog.findViewById(R.id.btn_report_user).setOnClickListener(view1 -> {
                alertDialog.dismiss();
                fragmentFactory.startReportActivity(getContext(), webViewArg.clsFiche());
            });
            alertDialog.findViewById(R.id.btn_report_fiche).setOnClickListener(view1 -> {
                alertDialog.dismiss();
                fragmentFactory.startReportActivity(getContext(), webViewArg.setFiche());
            });
            alertDialog.findViewById(R.id.btn_close).setOnClickListener(view1 -> {
                alertDialog.dismiss();
            });
        } else {
            fragmentFactory.startReportActivity(getContext(), webViewArg);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        assert supportActionBar != null;
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);

        WebSettings webSettings = binding.webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
//        webSettings.getJavaScriptCanOpenWindowsAutomatically();
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);

        // for pdfviewer ?
//        webSettings.setPluginsEnabled(true);
        webSettings.setAllowFileAccess(true);

        MyWebViewClient webViewClient = new MyWebViewClient(getContext());
        binding.webView.setWebViewClient(webViewClient);
        binding.webView.setWebChromeClient(new DefaultChromeClient());
        binding.webView.getSettings().setGeolocationEnabled(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.getSettings().setBuiltInZoomControls(true);
        binding.webView.getSettings().setDisplayZoomControls(false);
        binding.webView.addJavascriptInterface(new MyJavaScriptInterface(getContext()), "Android");

        binding.webView.getSettings().setAllowFileAccess(true);
        binding.webView.getSettings().setAppCacheEnabled(true);

        binding.webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        // requestPermissions();

        if (getPresenter().getWebViewArg().getWebPageType() == WebPageType.STATIC_PAGE) {
            if (!Utils.isNetworkAvailable(getContext())) {
                binding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ONLY);
            } else {
                binding.webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
            }
            showProgress();
            loadUrl(getPresenter().getWebViewArg().getURI().toString());
        } else if (getPresenter().getWebViewArg().getWebPageType() == WebPageType.EXTERNAL_REQUIRING_POSTURL) {
            showProgress();
            try {
                postUrl(getPresenter().getWebViewArg().getURI().toString(), getPresenter().getWebViewArg().getpostData());
            } catch (UnsupportedEncodingException e) {
                // TODO exception handler
            }
        } else {
            showProgress();
            if (!getPresenter().sendGetAnchor(getPresenter().getWebViewArg().getURI())) {
                hideProgress();
            }
        }

        binding.webView.init(binding.scrollView, binding.webFeedback, binding.rootWebview, binding.rootLayout);

        if (0 != (getActivity().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
            WebView.setWebContentsDebuggingEnabled(true);
        }


        binding.webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            if (requestPermissions()) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                final String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader(NetworkModule.COOKIE, cookies);

                request.addRequestHeader(NetworkModule.USER_AGENT, userAgent);

                for (String key : additionalHttpHeaders.keySet()) {
                    request.addRequestHeader(key, additionalHttpHeaders.get(key));
                }
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, null, null));

                final String basic = NetworkModule.BASIC + Base64.encodeToString(BuildConfig.CREDENTIAL_SERVER.getBytes(), Base64.NO_WRAP);
                request.addRequestHeader(NetworkModule.AUTHORIZATION, basic);

                DownloadManager dm = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            } else {
                saveUrl = url;
            }
        });

        binding.webView.getSettings().setJavaScriptEnabled(true);

        setWebFeedbackVisible(getPresenter().getWebViewArg().isFeedbackVisible() ? View.VISIBLE : View.GONE);
        setWebAroundMeVisible(getPresenter().getWebViewArg().isAroundMeActionVisible() ? View.VISIBLE : View.GONE);

    }

    private void initFacebook() {
        facebookCallbackManager = CallbackManager.Factory.create();
        facebookShareDialog = new ShareDialog(this);
        facebookShareDialog.registerCallback(facebookCallbackManager, getPresenter());
    }

    private void setVisibleFloatingBottoms(int visibility) {
        if (visibility == View.VISIBLE) {
            if (binding.webView.getTop() > 0) {
                binding.toTopOfWebview.setVisibility(View.VISIBLE);
            } else {
                binding.toTopOfWebview.setVisibility(View.INVISIBLE);
            }
        } else {
            binding.toTopOfWebview.setVisibility(visibility);
        }
        switch (getPresenter().getWebViewArg().getWebPageType()) {
            case STATIC_PAGE:
                binding.summaryWebview.setVisibility(View.INVISIBLE);
                break;
            default:
                binding.summaryWebview.setVisibility(visibility);
                break;
        }
    }

    private void goToTop() {
        binding.scrollView.fullScroll(View.FOCUS_UP);
        binding.webView.scrollTo(0, 0);
    }

    private void goToBottom() {
        // binding.webView.scrollTo(0, 1000000000);
        binding.webView.scrollToBottom();
        binding.scrollView.fullScroll(View.FOCUS_DOWN);
    }

    private void hideProgress() {
        try {
            binding.progressLayout.setVisibility(View.INVISIBLE);
            AnchorResponse anchorResponse = getPresenter().getAnchorResponse();
            if (isServerPage() && anchorResponse != null && anchorResponse.anchors != null && anchorResponse.anchors.length > 0) {
                update();
            }
            getActivity().invalidateOptionsMenu();
        } catch (Exception e) {
            Timber.e(e);
        }
    }

    @Override
    public void update() {
        if (is404Loaded) {
            setVisibleFloatingBottoms(View.GONE);
            setWebFeedbackVisible(View.GONE);
            setWebAroundMeVisible(View.GONE);
        } else {
            switch (getPresenter().getWebViewArg().getWebPageType()) {
                case STATIC_PAGE:
                    setVisibleFloatingBottoms(View.VISIBLE);
                    break;
                case SERVER:
                    setVisibleFloatingBottoms(View.VISIBLE);
                    break;
                case DISABLE_ALL:
                case ERROR:
                    setVisibleFloatingBottoms(View.GONE);
                    break;
            }
        }
        final AnchorResponse anchorResponse = getPresenter().getAnchorResponse();
        if (anchorResponse != null && anchorResponse.isContentMaintenanceMode()) {
            setVisibleFloatingBottoms(View.GONE);
            setWebFeedbackVisible(View.GONE);
            setWebAroundMeVisible(View.GONE);
        }
    }

    private void setWebFeedbackVisible(int visibility) {
        binding.webFeedback.setVisibility(visibility);
    }

    private void setWebAroundMeVisible(int visibility) {
        binding.webAroundMe.setVisibility(visibility);
    }

    @NonNull
    @Override
    public WebViewPresenter getPresenter() {
        return webViewPresenter;
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        final ErrorItem item = errorResponse.getErrorAnyItem();
        if (item != null && (item.error_code == ErrorItem.NODE_RESOURCE_IS_NOT_FOUND ||
                item.error_code == ErrorItem.INVALID_PARAMETER_URL)) {
            showMessage(item.getLocalizeErrorMessage());
            getPresenter().getWebViewArg().setWebPageType(WebPageType.STANDART);
            final URI uri = getPresenter().getWebViewArg().getURI();
            loadUrl(uri.toString());
            binding.webView.updateHeight();
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(uri.getHost());
            setTitle(uri.getHost());
            setVisibleFloatingBottoms(View.GONE);
            getActivity().invalidateOptionsMenu();
        } else
            hideProgress();
    }

    @Override
    public void onSuccessWeb(AnchorResponse anchorResponse) {
//        setVisible(View.VISIBLE);
        if (anchorResponse == null || anchorResponse.anchors == null || anchorResponse.anchors.length <= 0) {
            binding.summaryWebview.setVisibility(View.INVISIBLE);
            binding.toTopOfWebview.setVisibility(View.INVISIBLE);
            this.setWebFeedbackVisible(View.GONE);
        }
        getPresenter().setAnchorResponse(anchorResponse);
        getActivity().invalidateOptionsMenu();
        goToAnchor(null);
    }

    @Override
    public void showMessage(String message) {
        if (BuildConfig.DEBUG) {
            Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).show();
        }
    }

    private void goToAnchor(String anchor) {
        final WebViewArg webViewArg = getPresenter().getWebViewArg();
        final URI uri = webViewArg.getURI();
        if (anchor == null) {
            setTitle(webViewArg.isTransferTitle() ? webViewArg.getTitle() : getString(R.string.app_name));
            loadUrl(uri.toString());
        } else {
            // it code will work if anchor==null also
            if (anchor.startsWith(ANCHOR_MARKER)) {
                final String sectionName = anchor.substring(1);
                binding.webView.loadUrl("javascript:document.getElementById('" + sectionName + "').scrollIntoView(true);");
                setTitle(getString(R.string.app_name));
            } else {
                loadUrl(String.format("%s/node/%d/%s", UrlExtras.getBaseURL(),
                        getPresenter().getAnchorResponse().nid, anchor != null ? anchor : ""));
            }
        }
        binding.webView.updateHeight();
        binding.scrollView.fullScroll(View.FOCUS_UP);
    }

    private class MyWebViewClient extends DefaultWebViewClient {

        public MyWebViewClient(Context context) {
            super(context);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            boolean inWidgetResult = url.matches("(.*)/nos-preventions/(.*)");
            final WebViewArg webViewArg = getPresenter().getWebViewArg();
            webViewArg.setFeedbackVisible(inWidgetResult);
            webViewArg.setAroundMeActionVisible(inWidgetResult);
            setWebFeedbackVisible(getPresenter().getWebViewArg().isFeedbackVisible() ? View.VISIBLE : View.GONE);
            setWebAroundMeVisible(getPresenter().getWebViewArg().isAroundMeActionVisible() ? View.VISIBLE : View.GONE);

            lastStartUrl = url;
        }

        //Intercept WebView Requests for Android API < 21
        @Deprecated
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            if (url.startsWith("http://")) {
                try {
                    //change protocol of url string
                    url = url.replace("http://","https://");

                    //return modified response
                    URL httpsUrl = new URL(url);
                    URLConnection connection = httpsUrl.openConnection();
                    return new WebResourceResponse(connection.getContentType(), connection.getContentEncoding(), connection.getInputStream());
                } catch (Exception e) {
                    //an error occurred
                }
            }
            return super.shouldInterceptRequest(view, url);
        }

        //Intercept WebView Requests for Android API >= 21
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            if (url.startsWith("http://")) {
                try {
                    //change protocol of url string
                    url = url.replace("http://","https://");

                    //return modified response
                    URL httpsUrl = new URL(url);
                    URLConnection connection = httpsUrl.openConnection();
                    return new WebResourceResponse(connection.getContentType(), connection.getContentEncoding(), connection.getInputStream());
                } catch (Exception e) {
                    //an error occurred
                }
            }
            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (getPresenter().isSearchAction(url)) {
                return true;
            }
            final Uri uri = Uri.parse(url);
            if (TextUtils.equals(uri.getScheme(), TEL)) {
                Utils.requestCall(uri, getContext());
            } else {
                if (url.endsWith(PDF)) {
//                // Load "url" in google docs
//                loadUrl("http://docs.google.com/gview?embedded=true&url=" + url);
                    if (requestPermissions()) {
                        loadUrl(url);
                    } else {
                        saveUrl = url;
                    }
                } else if (url.startsWith("http://")) {
                    url = url.replace("http://","https://");

                    //return modified response
                    loadUrl(url);
                } else {
                    final WebViewArg webViewArg = getPresenter().getWebViewArg();
                    switch (webViewArg.getWebPageType()) {
                        case TRANSFER:
                        case STANDART:
                        case DISABLE_ALL:
                        case ERROR:
                            final int size = binding.webView.copyBackForwardList().getSize();
                            if (size >= 2) {
                                callNewWebView(url, webViewArg, true);
                            } else {
                                loadUrl(url);
                            }
                            break;
                        default:
                            callNewWebView(url, webViewArg, true);
                            break;
                    }
                }
            }
            return true;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            String url = request.getUrl().toString();
            // HARD CODED POPUP BAN
            if (url.matches("(.*)base-donnees-publique.medicaments.gouv.fr/enquete.php(.*)")) {
                return true;
            }

            if (TextUtils.equals(request.getUrl().getScheme(), TEL)) {
                Utils.requestCall(request.getUrl(), getContext());
            } else if (url.startsWith("http://")) {
                //change protocol of url string
                url = url.replace("http://","https://");

                //return modified response
                loadUrl(url);
            } else {
                loadUrl(request.getUrl().toString());
            }

            return true;
        }

        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);

            if (errorResponse.getStatusCode() == ERROR404) {
                is404Loaded = true;
                binding.toTopOfWebview.setVisibility(View.GONE);
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//            Log.i("WEB_VIEW_TEST", "error code:" + errorCode);
            super.onReceivedError(view, errorCode, description, failingUrl);
            hideProgress();
            getPresenter().getWebViewArg().setWebPageType(WebPageType.ERROR);
            showHttpErrorDialog(failingUrl);
//            getActivity().invalidateOptionsMenu();
        }

        @Override
        public void onPageFinished(WebView webView, String url) {
            // SAN-3022 Inject javascript
            // Log.v("DEV", "SAN-3022 ignore - JS Injected");
            webView.loadUrl("javascript: window.useBrowserLocationFramework = true;");

            try {
                // #81519 : Yes/No on WebView. Set listener for all.
                // Load JS for any not only for 'if (BuildConfig.FEATURE_TEST)'
                final String geoLocation = repository.getGeoLocation();
                webView.loadUrl(getInjectJavaScript(getPresenter().getAnchorResponse() != null && getPresenter().getAnchorResponse().isEmergencyButton(geoLocation)));
            } catch (Exception e) {
                Timber.e(e);
            }

            final WebViewArg webViewArg = getPresenter().getWebViewArg();
            switch (webViewArg.getWebPageType()) {
                case SERVER:
                    if (webViewArg.getPushAlert().isContent()) {
                        // Load page after AlertPopinWindow
                        getPresenter().writeToHostory(url, webView.getTitle());
                    }
                    break;
                case TRANSFER:
                    getPresenter().writeToHostory(url, webView.getTitle());
                    break;
                case SIMPLE:
                    new TrackerHelper().addScreenView(TrackerLevel.TRANSVERSE_PAGES, WebViewPresenter.makeTitle(webView.getTitle()));
                    break;
            }
            if (!loadingFinished) {
                switch (webViewArg.getWebPageType()) {
                    default:
                        webViewPresenter.trackScreen(true);
                        break;
                    case TRANSFER:
//                            webViewPresenter.trackScreen(false);
                        break;
                    case STATIC_PAGE:
                        new TrackerHelper().clickOpenStaticPage(WebViewPresenter.makeTitle(webView.getTitle()));
                        break;
                }
                loadingFinished = true;
            }
            if (!url.contains(UrlExtras.getBaseURL())) {
                webViewArg.setWebPageType(WebPageType.STANDART);
            } else {
                final WebPageType webPageType = webViewArg.getWebPageType();
                switch (webPageType) {
                    case ERROR:
                        setTitle(getString(R.string.app_name));
                        break;
                    case DISABLE_ALL:
                        setTitle(webViewArg.getTitle());
                    case TRANSFER:
                    case STANDART:
                        webViewArg.setWebPageType(WebPageType.SERVER);
                        break;
                }
            }
            updatebackground(webView, url);
            if (!TextUtils.equals(lastStartUrl, url)) {
                if (lastStartUrl != null) {
                    int anchorCount;
                    // TODO it is hack!!!
                    if (lastStartUrl.startsWith(url)) {
                        anchorCount = Utils.countMatches(lastStartUrl.substring(url.length()), ANCHOR_MARKER);
                        if (anchorCount == 1) {
                            // if we work with tab we ignore archor alhoritm
                            if (lastStartUrl.contains(TAB_MARKER)) {
                                anchorCount = 0;
                            }
                        }
                        if (anchorCount == 1 && !binding.webView.canGoBack()) {
                            if (getActivity() != null) {
                                getActivity().onBackPressed();
                            }
                        } else {
                            webViewArg.setWebPageType(lastWebPageType);
                            showProgress();
                            webView.reload();
                            lastStartUrl = url;
                        }
                        return;
                    }
                    if (isIntersectingPages(url, lastStartUrl)) {
                        lastWebPageType = webViewArg.getWebPageType();
                        anchorCount = Utils.countMatches(url/*.substring(lastStartUrl.length())*/, ANCHOR_MARKER);
                        if (anchorCount == 1) {
                            if (!url.endsWith(MAP)) {
                                if (url.contains(TAB_MARKER)) {
                                    webViewArg.setWebPageType(WebPageType.DISABLE_ALL);
                                    setTitle(/*webViewArg.getTitle()*/WebViewPresenter.makeTitle(binding.webView.getTitle()));
                                } else {
                                    webViewArg.setWebPageType(WebPageType.STANDART);
                                }
                                update();
                            } else {
//                                    showProgress();
//                                    webView.reload();
//                                    lastStartUrl = url;
//                                    return;
                            }
                        }
                    }
                    Timber.d("lastStartUrl!=url");
                }
                lastStartUrl = url;
            }
            if (!url.contains(UrlExtras.getBaseURL())) {
                webViewArg.setWebPageType(WebPageType.STANDART);
            } else {
                if (webViewArg.getWebPageType() == WebPageType.STANDART ||
                        webViewArg.getWebPageType() == WebPageType.TRANSFER) {
                    webViewArg.setWebPageType(WebPageType.SERVER);
                }
            }
            hideProgress();
        }
    }

    private void updatebackground(WebView webView, String url) {
        if (getContext() != null) {
            if (url.contains(TAB_MARKER)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    webView.setBackground(getContext().getResources().getDrawable(R.drawable.bg_gradient, null));
                } else {
                    webView.setBackground(getContext().getResources().getDrawable(R.drawable.bg_gradient));
                }
                // webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
            } else {
                Drawable d = webView.getBackground();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    webView.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent, null));
                } else {
                    webView.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
                }
                // webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
            }
        }
    }

    private boolean isIntersectingPages(String url1, String url2) {
        if (url1.contains(ANCHOR_MARKER) && url2.contains(ANCHOR_MARKER)) {
            url1 = url1.substring(0, url1.indexOf(ANCHOR_MARKER));
            url2 = url2.substring(0, url2.indexOf(ANCHOR_MARKER));
            return TextUtils.equals(url1, url2);
        } else {
            if (url1.length() > url2.length()) {
                return url1.contains(url2);
            } else {
                return url2.contains(url1);
            }
        }
    }

    private void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case WebViewPresenter.REQUEST_CODE:
                    showProgress();
                    if (!getPresenter().sendGetAnchor(getPresenter().getWebViewArg().getURI())) {
                        hideProgress();
                    }
                    break;
                case REQUEST_CODE:
                    final Parcelable parcelable = data.getParcelableExtra(AnchorItem.ANCHOR_ITEM);
                    if (parcelable != null) {
                        final AnchorItem anchorItem = (AnchorItem) parcelable;
//                    binding.webView.loadUrl("javascript:scrollAnchor(" + anchorItem.getAnchor() + ");");
                        goToAnchor(anchorItem.getAnchor());
//                    binding.webView.loadUrl(String.format("%s/node/%d%s", UrlExtras.getBaseURL(), anchorResponse.nid, anchorItem.getAnchor()), additionalHttpHeaders);
                    } else {
                        final int command = data.getIntExtra(DIRECT_COMMAND, TO_TOP);
                        switch (command) {
                            case TO_TOP:
                                goToTop();
                                break;
                            case TO_BOTTOM:
                                goToBottom();
                                break;
                        }
                    }
                    break;
            }
        }
    }

    @Override
    public void showExitDialog() {
        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                .setView(R.layout.dialog_feedback_close)
                .setPositiveButton(R.string.to_close, (dialog, which) -> {

                })
                .show();
    }

    @Override
    public void showHttpErrorDialog(String url) {
        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_http_error, null);
        TextView tv = customLayout.findViewById(R.id.webview_error_occured_url);
        tv.setText(url);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(customLayout)
                .setPositiveButton(R.string.webview_error_occured_btn, (dialog, which) -> {
                    onBackPressed();
                });
        builder.show();
    }

    @Override
    public void onSuccessFavorite(long id, boolean favorite) {
        getActivity().invalidateOptionsMenu();

        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_RESULT_FAVORITE, favorite);
        resultIntent.putExtra(EXTRA_RESULT_ID, id);
        getActivity().setResult(Activity.RESULT_OK, resultIntent);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (loadingFinished) {
            final MenuItem shareItem = menu.findItem(R.id.action_share);
            final AnchorResponse anchorResponse = getPresenter().getAnchorResponse();
            if (anchorResponse != null) {
                MenuItem favorMenuItem = menu.findItem(R.id.action_bookmark);
                if (isServerPage()) {
                    favorMenuItem.setVisible(!is404Loaded);
                    favorMenuItem.setIcon(anchorResponse.isFavorite() ? R.drawable.ic_favoris_24dp_green : R.drawable.ic_favoris_24dp_white);

                    shareItem.setVisible(!is404Loaded);
                } else {
                    favorMenuItem.setVisible(false);
                    switch (getPresenter().getWebViewArg().getWebPageType()) {
                        case DISABLE_ALL:
                        case ERROR:
                            shareItem.setVisible(false);
                            break;
                        case TRANSFER:
                        case STANDART:
                            shareItem.setVisible(false);
                            break;
                        case STATIC_PAGE:
                            shareItem.setVisible(false);
                            break;
                        case CGU_PAGE:
                            favorMenuItem.setVisible(false);
                            shareItem.setVisible(false);
                            break;

                        default:
                            shareItem.setVisible(!is404Loaded);
                    }
                }
                if (anchorResponse.isContentMaintenanceMode()) {
                    favorMenuItem.setVisible(false);
                    shareItem.setVisible(false);
                }
            } else {
                final MenuItem menuItem = menu.findItem(R.id.action_bookmark);
                menuItem.setVisible(false);
                switch (getPresenter().getWebViewArg().getWebPageType()) {
                    case STATIC_PAGE:
                    case STANDART:
                    case CGU_PAGE:
                        shareItem.setVisible(false);
                        break;
                    case TRANSFER:
                        shareItem.setVisible(false);
                        break;
                    case ERROR:
                        shareItem.setVisible(false);
                        break;
                    default:
                        shareItem.setVisible(!is404Loaded);
                        break;
                }
            }
        }
    }

    private boolean isServerPage() {
        switch (getPresenter().getWebViewArg().getWebPageType()) {
            case STATIC_PAGE:
                return false;
            case CGU_PAGE:
                return false;
            case TRANSFER:
            case STANDART:
                return false;
            case DISABLE_ALL:
            case ERROR:
                return false;
            case SIMPLE:
                return false;
            default:
            case SERVER:
                return true;
        }
    }

    @Override
    public void shareFacebook(ShareLinkContent shareLinkContent) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            facebookShareDialog.show(shareLinkContent);
        }
    }

    @Override
    public void hideLoading() {
        hideProgress();
    }

    @Override
    public void showLoading() {
        showProgress();
    }

    @Override
    public void showRegistrationScreen() {
        Intent intent = CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    private void loadUrl(String url) {
        if (BuildConfig.DEBUG) {
//            url=url+"?sgp_force_webview=1";
//            url = "https://dev.sris-gp.adyax-dev.com/policy-page";
//            url = "https://m.dbbest.com";
//            url = "http://www.pravda.com.ua/";
//            url="https://www.iledefrance.ars.sante.fr";
        }
        is404Loaded = false;
        CookieManager.getInstance().setCookie(UrlExtras.getBaseURL(), additionalHttpHeaders.get(NetworkModule.COOKIE));
        CookieManager.getInstance().setCookie(url, additionalHttpHeaders.get(NetworkModule.COOKIE));
        binding.webView.loadUrl(url, additionalHttpHeaders);
//        binding.webView.loadUrl(url);
    }

    private void postUrl(final String url, final String postData) throws UnsupportedEncodingException {
        is404Loaded = false;
        binding.webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        binding.webView.postUrl(url, getBytes(postData, "UTF-8"));
    }

    public static byte[] getBytes(final String data, final String charset) {


        if (data == null) {
            throw new IllegalArgumentException("data may not be null");
        }


        if (charset == null || charset.length() == 0) {
            throw new IllegalArgumentException("charset may not be null or empty");
        }


        try {
            return data.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            return data.getBytes();
        }
    }

    private boolean requestPermissions() {
        final boolean b = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (!b) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSION);
        }
        return b;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (saveUrl != null) {
                        loadUrl(saveUrl);
                    }
                }
                return;
            }
            case GeoHelper.REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPresenter().startAskGeoPermission();
                }
                break;

        }

    }

    private boolean isToTopBottomVisible() {
        switch (getPresenter().getWebViewArg().getWebPageType()) {
            case STATIC_PAGE:
                return true;
            case DISABLE_ALL:
            case ERROR:
                return false;
            default:
                return binding.summaryWebview.getVisibility() == View.VISIBLE;
        }
    }

    private void callNewWebView(String url, WebViewArg webViewArg, boolean bStartAsActivity) {
        final Xiti xiti = webViewArg.getXiti();
        final TrackerLevel screenTrackerLevel = (xiti == null) ? TrackerLevel.UNKNOWN : xiti.getScreenTrackerLevel();
//        String title = webViewArg.getTitle();
        final String title = WebViewPresenter.makeTitle(binding.webView.getTitle());
        webViewArg = new WebViewArg(webViewArg.getHistory(), url, WebPageType.TRANSFER/*getPresenter().getWebViewArg().getWebPageType()*/, screenTrackerLevel)
                .setTitle(title);
        if (bStartAsActivity) {
            fragmentFactory.startWebViewActivity(getContext(), webViewArg, -1);
        } else {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction()
                    //                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, WebViewFragment.newInstance(webViewArg));
            if (loadingFinished) {
                fragmentTransaction.addToBackStack(null);
            }
            fragmentTransaction
                    //                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
                    .commit();
        }
    }

    private void setTitle(String title) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(title);
    }

    private class MyJavaScriptInterface {

        private Context context;

        MyJavaScriptInterface(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void processHTML(int data, String string) {
            binding.getRoot().post(() -> {
                switch (data) {
                    case BOTTOM_MENU_NO:
                        getPresenter().clickNo(WebViewPresenter.FEEDBACK_HELPFUL_FORM_NAME);
                        break;
                    case BOTTOM_MENU_YES:
                        getPresenter().clickYes(WebViewPresenter.FEEDBACK_HELPFUL_FORM_NAME);
                        break;
                    case BOTTOM_MENU_SIGNALER:
                        clickSignaler(string);
                        break;
                    case BOTTOM_UNKNOWN:
                        break;
                    case BOTTOM_EMERGENCY:
                        getPresenter().emergency(string);
                        break;
                }
            });

//            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//            builder.setTitle("AlertDialog from app")
//                .setMessage(data)
//                .setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss())
//                .setCancelable(false)
//                .show();

        }
    }

    private String getInjectJavaScript(boolean bEnableButton) {

//        StringBuilder buf=new StringBuilder();
//        InputStream json=getActivity().getAssets().open("webview.js");
//        BufferedReader in=
//                new BufferedReader(new InputStreamReader(json, "UTF-8"));
//        String str;
//
//        while ((str=in.readLine()) != null) {
//            buf.append(str);
//        }
//
//        in.close();

        return "javascript:(function() {" +

                "function nonFunction() {" +
                "window.Android.processHTML(" + String.valueOf(BOTTOM_MENU_NO) + ", '');" +
                "};" +
                "function ouiFunction() {" +
                "window.Android.processHTML(" + String.valueOf(BOTTOM_MENU_YES) + ", '');" +
                "};" +
                "function signalerFunction(arg) {" +
                "window.Android.processHTML(" + String.valueOf(BOTTOM_MENU_SIGNALER) + ", arg);" +
                "};" +
                "function testFunction() {" +
                "window.Android.processHTML(" + String.valueOf(BOTTOM_UNKNOWN) + ", '');" +
                "};" +
                "function emergencyFunction(arg) {" +
                "window.Android.processHTML(" + String.valueOf(BOTTOM_EMERGENCY) + ", arg);" +
                "};" +

                "var btn_oui =document.getElementsByClassName('" + OUI_BUTTON_CLASS + "')[0]; " +
                "var btn_non =document.getElementsByClassName('" + NON_BUTTON_CLASS + "')[0]; " +
                "var btn_signaler =document.getElementsByClassName('" + SYNG_BUTTON_CLASS + "')[0]; " +

                "var btn_embedded_map_link1 =document.getElementsByClassName('" + EMBEDDED_MAP_LINK_CLASS + "')[0]; " +
                "var btn_embedded_map_link2 =document.getElementsByClassName('" + EMBEDDED_MAP_LINK_CLASS + "')[1]; " +

                "var btn_emergency =document.getElementsByClassName('" + EMERGENCY_BUTTON_CLASS + "')[0]; " +

                "var block_emergency =document.getElementsByClassName('" + EMERGENCY_BUTTON_CLASS2 + "')[0]; " +

                (bEnableButton ? "if(block_emergency!=null) $(block_emergency).removeClass('element-hidden'); " : "") +

                "var btn_signaler_href=(btn_signaler!=null)? btn_signaler.href: null; " +
                "function signalerFunctionShort(){" +
                "signalerFunction(btn_signaler_href);" +
                "}" +

                "if(btn_oui!=null) btn_oui.href=\"javascript: void(0)\"; " +
                "if(btn_non!=null) btn_non.href=\"javascript: void(0)\"; " +
                "if(btn_signaler!=null) btn_signaler.href=\"javascript: void(0)\"; " +

                "if (btn_oui != null && btn_non != null) { " +
                "btn_oui.addEventListener('click', function () { setTimeout(function () { btn_oui.parentElement.parentElement.remove() }, 500); }); " +
                "btn_non.addEventListener('click', function () { setTimeout(function () { btn_non.parentElement.parentElement.remove() }, 500); }); " +
                "}" +

                "if(btn_embedded_map_link1!=null) btn_embedded_map_link1.childNodes[0].href=\"javascript: void(0)\"; " +
                "if(btn_embedded_map_link2!=null) btn_embedded_map_link2.childNodes[0].href=\"javascript: void(0)\"; " +

                "var btn_emergency_href=(btn_emergency!=null)?btn_emergency.childNodes[1].toString(): null; " +

                "function emergencyFunctionShort(){" +
                "emergencyFunction(btn_emergency_href);" +
                "}" +
                "if(btn_emergency!=null) btn_emergency.childNodes[1].href='javascript: void(0)'; " +

                "if(btn_oui!=null) btn_oui.addEventListener('click',ouiFunction, false);" +
                "if(btn_non!=null) btn_non.addEventListener('click',nonFunction, false);" +
                "if(btn_signaler!=null) btn_signaler.addEventListener('click',signalerFunctionShort, false);" +

                "if(btn_embedded_map_link1!=null) btn_embedded_map_link1.addEventListener('click',testFunction, false);" +
                "if(btn_embedded_map_link2!=null) btn_embedded_map_link2.addEventListener('click',testFunction, false);" +

                "if(btn_embedded_map_link1!=null) btn_embedded_map_link1.childNodes[0].addEventListener('click', testFunction, false);" +
                "if(btn_embedded_map_link1!=null) btn_embedded_map_link2.childNodes[0].addEventListener('click', testFunction, false);" +

                "if(btn_emergency!=null) btn_emergency.addEventListener('click', emergencyFunctionShort , false);" +

                "})()";
    }

    public void updateLocation() {
        final AnchorResponse anchorResponse = getPresenter().getAnchorResponse();
        if (anchorResponse != null) {
            final String geoLocation = repository.getGeoLocation();
            final boolean isEmergencyButtonPrev = anchorResponse.isEmergecyVisible();
            final boolean emergencyButton = anchorResponse.isEmergencyButton(geoLocation);
            if (emergencyButton != isEmergencyButtonPrev) {
                showProgress();
                loadUrl(getPresenter().getWebViewArg().getURI().toString());
            }
        }
    }

}
