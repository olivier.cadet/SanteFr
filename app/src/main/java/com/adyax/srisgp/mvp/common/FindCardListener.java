package com.adyax.srisgp.mvp.common;

import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;

/**
 * Created by anton.kobylianskiy on 11/3/16.
 */

public interface FindCardListener {
    void onCallClicked(SearchItemHuge item);
    void onFavoriteClicked(SearchItemHuge item);
    void onRouteClicked(SearchItemHuge item);
    void onItemClicked(SearchItemHuge item);
}
