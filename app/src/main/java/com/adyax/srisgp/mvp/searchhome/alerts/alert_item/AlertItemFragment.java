package com.adyax.srisgp.mvp.searchhome.alerts.alert_item;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.databinding.FragmentAlertsBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertItemFragment extends ViewFragment implements AlertItemPresenter.CreateRecipeView {

    public static final String ALERT_SWIPE_FRAGMENT = "AlertSwipeFragment";
    @Inject
    AlertItemPresenter alertItemPresenter;
    private FragmentAlertsBinding binding;
    private AlertItem alertItem;

    public AlertItemFragment() {
    }

    public static Fragment newInstance(AlertItem alertItem) {
        AlertItemFragment fragment = new AlertItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(ALERT_SWIPE_FRAGMENT, alertItem);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        alertItem =getArguments().getParcelable(ALERT_SWIPE_FRAGMENT);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alerts, container, false);
        binding.title.setText(String.format("%s", alertItem.getTitle()));
        binding.title.setTextColor(getContext().getResources().getColor(alertItem.getColor()));
        binding.description.setText(alertItem.getDescription());
        binding.getRoot().setOnClickListener(view -> {
            getPresenter().clickAlertCaruselCard(alertItem);
        });
        if(!alertItem.isRedAlert()){
            binding.title.setTextColor(getActivity().getResources().getColor(R.color.orange));
            binding.alarmImage.setImageResource(R.drawable._ic_alert_attention_s);
        }
        if(!alertItem.isContent()){
            binding.bottonText.setVisibility(View.GONE);
        }else{
            binding.bottonText.setText(alertItem.getBottonText());
        }
        return binding.getRoot();
    }

    @NonNull
    @Override
    public AlertItemPresenter getPresenter() {
        return alertItemPresenter;
    }
}
