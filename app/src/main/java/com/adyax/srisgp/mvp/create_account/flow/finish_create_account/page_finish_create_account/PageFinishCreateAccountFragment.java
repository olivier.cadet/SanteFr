package com.adyax.srisgp.mvp.create_account.flow.finish_create_account.page_finish_create_account;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentFinishedCreateAccountBinding;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountFragment;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class PageFinishCreateAccountFragment extends ViewFragment implements PageFinishCreateAccountPresenter.CreateRecipeView {

    public static final String TYPE_CREATE_ACCOUNT = "TypeCreateAccount";

    @Inject
    PageFinishCreateAccountPresenter pageFinishCreateAccountPresenter;
    private FragmentFinishedCreateAccountBinding binding;

    public PageFinishCreateAccountFragment() {
    }

    public static Fragment newInstance(TypeCreateAccount typeCreateAccount) {
        PageFinishCreateAccountFragment fragment = new PageFinishCreateAccountFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE_CREATE_ACCOUNT, typeCreateAccount.name());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TypeCreateAccount typeCreateAccount=TypeCreateAccount.valueOf(getArguments().getString(TYPE_CREATE_ACCOUNT));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_finished_create_account, container, false);
        switch (typeCreateAccount) {
            case favorite:
                binding.imageFinished.setImageResource(R.drawable.ic_favoris_72dp);
                binding.moreFinished.setText(R.string.favorites);
                binding.toAccessYourAccount.setText(R.string.find_your_favorite);
//                binding.finishPoint1.setImageResource(R.drawable.bg_cycle_light);
                binding.finishPoint1.setChecked(true);
                break;
            case notification:
                binding.imageFinished.setImageResource(R.drawable.ic_notif_72dp);
                binding.moreFinished.setText(R.string.notifications);
                binding.toAccessYourAccount.setText(R.string.access_your_notifications);
//                binding.finishPoint2.setImageResource(R.drawable.bg_cycle_light);
                binding.finishPoint2.setChecked(true);
                break;
            case more:
                binding.imageFinished.setImageResource(R.drawable.ic_plus_72dp);
                binding.moreFinished.setText(R.string.more);
                binding.toAccessYourAccount.setText(R.string.to_access_your_account);
//                binding.finishPoint3.setImageResource(R.drawable.bg_cycle_light);
                binding.finishPoint3.setChecked(true);
                break;
            case search:
                binding.imageFinished.setImageResource(R.drawable.ic_loupe_72dp);
                binding.moreFinished.setText(R.string.search_search_title);
                binding.toAccessYourAccount.setText(R.string.find_something_useful);
//                binding.finishPoint4.setImageResource(R.drawable.bg_cycle_light);
                binding.finishPoint4.setChecked(true);
                break;
        }
        binding.finishBullet1.setTag(0);
        binding.finishBullet1.setOnClickListener(v);
        binding.finishBullet2.setTag(1);
        binding.finishBullet2.setOnClickListener(v);
        binding.finishBullet3.setTag(2);
        binding.finishBullet3.setOnClickListener(v);
        binding.finishBullet4.setTag(3);
        binding.finishBullet4.setOnClickListener(v);
        return binding.getRoot();
    }

    private View.OnClickListener v= view -> {
        Intent intent = new Intent(FinishCreateAccountFragment.NEW_BULLET_PAGE_CREATE_ACCOUNT);
        intent.putExtra(FinishCreateAccountFragment.NUMBER_OF_PAGE, (int)view.getTag());
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    };

//    private CompoundButton.OnCheckedChangeListener listener = (compoundButton, b) -> {
//        if (b) {
//            compoundButton.setChecked(false);
//            Intent intent = new Intent(FinishCreateAccountFragment.NEW_BULLET_PAGE_CREATE_ACCOUNT);
//            intent.putExtra(FinishCreateAccountFragment.NUMBER_OF_PAGE, (int)compoundButton.getTag());
//            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
//        }
//    };

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("this is dummy");
    }

    @NonNull
    @Override
    public PageFinishCreateAccountPresenter getPresenter() {
        return pageFinishCreateAccountPresenter;
    }
}
