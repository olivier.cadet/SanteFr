package com.adyax.srisgp.mvp.history;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public enum HistoryType {
    info,
    find,
    search;
}
