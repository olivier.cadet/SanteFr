package com.adyax.srisgp.mvp.report.report_find.report_find_user;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.data.net.response.FeedbackFormField;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.databinding.FragmentReportFindUser1Binding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.utils.ViewUtils;

import java.util.Map;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportFindUserFragment_1 extends ViewFragment implements ReportFindUserPresenter_1.Report3View {

    public static final String TITLE = "title";

    @Inject
    ReportFindUserPresenter_1 reportFindUserPresenter_1;

    @Inject
    FragmentFactory fragmentFactory;

    private FragmentReportFindUser1Binding binding;
    private int title;
    private Map<String, FeedbackFormField> map;

    public ReportFindUserFragment_1() {
    }

    public static Fragment newInstance(int title, WebViewArg webViewArg) {
        ReportFindUserFragment_1 fragment = new ReportFindUserFragment_1();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        args.putParcelable(WebViewArg.CONTENT_URL, webViewArg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        reportFindUserPresenter_1.setWebViewArg((WebViewArg) getArguments().getParcelable(WebViewArg.CONTENT_URL));
        title =getArguments().getInt(TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_find_user_1, container, false);
        binding.btnReport1Action.setOnClickListener(view -> {
            final ReportBuilder builder = reportFindUserPresenter_1.getBuilder();
//            WebViewArg webViewArg = (WebViewArg) getArguments().getParcelable(WebViewArg.CONTENT_URL);
            builder.setFromName(InapLinkHelper.FIND_CONTENT_ERROR_FORM_NAME)
                .set(ReportBuilder.FIELD_ERROR_INFO, binding.reportErrorInfo.getText().toString().trim())
                .set(ReportBuilder.FIELD_PROPOSED_INFO, binding.reportProposedInfo.getText().toString())
                .set(ReportBuilder.FIELD_URL, reportFindUserPresenter_1.getWebViewArg().getShortNode())
                .set(FeedbackFormResponse.FIELD_CONCERNED_PARTY,
                    getValue(FeedbackFormResponse.FIELD_CONCERNED_PARTY, binding.spinnerConcernedParty.getValue()))
                .set(FeedbackFormResponse.FIELD_ERROR_ATTENTION_REASON,
                    getValue(FeedbackFormResponse.FIELD_ERROR_ATTENTION_REASON, binding.spinnerAttentionReason.getValue()));
            fragmentFactory.startReport2Fragment(getContext(), builder);
        });

        binding.spinnerConcernedParty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                validateData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                validateData();
            }
        });

        binding.reportErrorInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.reportProposedInfo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateData();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.spinnerAttentionReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                validateData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                validateData();
            }
        });
//        binding.spinnerConcernedParty.setFocusable(true);
//        binding.spinnerConcernedParty.setFocusableInTouchMode(true);
//        binding.spinnerAttentionReason.setFocusable(true);
//        binding.spinnerAttentionReason.setFocusableInTouchMode(true);
        return binding.getRoot();
    }

    private void validateData() {
        if (binding.spinnerAttentionReason.isEmpty() || binding.reportProposedInfo.getText().length() == 0 ||
                binding.reportErrorInfo.getText().length() == 0 || binding.spinnerConcernedParty.isEmpty()) {
            binding.btnReport1Action.setEnabled(false);
        } else {
            binding.btnReport1Action.setEnabled(true);
        }
    }

    private String getValue(String key, String field) {
        if(map!=null&& map.containsKey(key)){
            return map.get(key).getValue(field);
        }
        return "";
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);
        reportFindUserPresenter_1.init();
    }

    @NonNull
    @Override
    public ReportFindUserPresenter_1 getPresenter() {
        return reportFindUserPresenter_1;
    }

    @Override
    public void update(FeedbackFormResponse feedbackFormResponse) {
        map=feedbackFormResponse.getFields();
        if(map.containsKey(FeedbackFormResponse.FIELD_CONCERNED_PARTY)){
            binding.spinnerConcernedParty.setObjects(map.get(FeedbackFormResponse.FIELD_CONCERNED_PARTY).getItems());
        }
        if(map.containsKey(FeedbackFormResponse.FIELD_ERROR_INFO)){
            binding.reportErrorInfo.setHint(map.get(FeedbackFormResponse.FIELD_ERROR_INFO).getLabel());
        }
        if(map.containsKey(FeedbackFormResponse.FIELD_PROPOSED_INFO)){
            binding.reportProposedInfo.setHint(map.get(FeedbackFormResponse.FIELD_PROPOSED_INFO).getLabel());
        }
        if(map.containsKey(FeedbackFormResponse.FIELD_ERROR_ATTENTION_REASON)){
            binding.spinnerAttentionReason.setObjects(map.get(FeedbackFormResponse.FIELD_ERROR_ATTENTION_REASON).getItems());
        }
        binding.spinnerConcernedParty.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                ViewUtils.hideSoftKeyboard(getActivity());
                binding.focusThief.requestFocus();
            }
            return false;
        });
        binding.spinnerAttentionReason.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                ViewUtils.hideSoftKeyboard(getActivity());
                binding.focusThief.requestFocus();
            }
            return false;
        });
    }
}
