package com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_finish;

import android.os.Bundle;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class ReportFinishPresenter_3 extends PresenterAppReceiver<ReportFinishPresenter_3.ReportPresenter_2View> {

    private ReportBuilder builder=new ReportBuilder();
    private INetWorkState netWorkState;

    public <T extends Parcelable> void clickBackToCard(){
//        ExecutionService.sendCommand(getContext(), getAppReceiver(),
//                new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
//                                    BaseFeedbackCommandCommand.SECRET_TOKEN, builder.build()), ExecutionService.REPORT_ACTION);
        getView().showProgress();
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new GetFormCommand(getBuilder().getFromName(), null), ExecutionService.GET_FORM);
    }

    public interface ReportPresenter_2View extends IView {
        void showCloseDialog();

        void showProgress();

        void hideProgress();
    }

    @Inject
    public ReportFinishPresenter_3(INetWorkState netWorkState) {
        this.netWorkState = netWorkState;
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch(requestCode){
            case ExecutionService.GET_FORM:
                final FeedbackFormResponse feedbackFormResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                if(feedbackFormResponse!=null) {
                    ExecutionService.sendCommand(getContext(), getAppReceiver(),
                            new BaseFeedbackCommandCommand(netWorkState.isLoggedIn(), getBuilder().getFromName(),
                                    feedbackFormResponse.getSecret(), builder.build(), data.getLong(CommandExecutor.START_TIME)), ExecutionService.REPORT_ACTION);
                }
                break;
            case ExecutionService.REPORT_ACTION:
                getView().hideProgress();
//                ExecutionService.sendCommand(getContext(), getAppReceiver(), new GetFormCommand(ReportCommand.FORM_NAME, null), ExecutionService.GET_FORM);
//                getView().showCloseDialog();
                break;
        }
    }

    public ReportBuilder getBuilder() {
        return builder;
    }

    public void setBuilder(ReportBuilder builder) {
        this.builder = builder;
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        getView().hideProgress();
    }

}
