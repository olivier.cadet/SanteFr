package com.adyax.srisgp.mvp.framework;

import android.content.Context;
import androidx.annotation.NonNull;

public interface IView {

    @NonNull
    Context getContext();
}
