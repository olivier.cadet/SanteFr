package com.adyax.srisgp.mvp.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.GetHistoryResponse;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.EndlessRecyclerViewScrollListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class HistoriesFragment extends ViewFragment implements HistoriesPresenter.HistoriesView {
    private static final String ARGUMENT_HISTORY_TYPE = "history_type";

    @Inject
    HistoriesPresenter historiesPresenter;

    @BindView(R.id.rvHistories)
    RecyclerView rvHistories;
    @BindView(R.id.llEmptyView)
    LinearLayout llEmptyView;

    private HistoriesAdapter historiesAdapter;
    private HistoryListener listener;
    private HistoryType type;

    public static HistoriesFragment newInstance(HistoryType type) {
        Bundle args = new Bundle();
        args.putSerializable(ARGUMENT_HISTORY_TYPE, type);
        HistoriesFragment fragment = new HistoriesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void addLoadingItem(HistoryKey id) {
        historiesAdapter.notifyItemChangedById(id);
    }

    public void changeSelectionMode(HistoryItem item) {
        historiesAdapter.changeSelectionMode(item);
    }

    public void updateAll() {
        historiesAdapter.notifyDataSetChanged();
    }

    public void changeEditMode() {
        historiesAdapter.notifyDataSetChanged();
    }

    public void onPageChanged() {
        resetSwipeState(null);
    }

    public HistoryType getHistoryType() {
        return type;
    }

    public void removeHistory(HistoryKey removedId) {
        historiesAdapter.removeItem(removedId);
        listener.onItemCountChanged(this, historiesAdapter.getItems().size());
        if (historiesAdapter.getItems().isEmpty()) {
            historiesPresenter.requestHistories(0);
//            showEmptyView();
//            listener.onAllItemsRemoved();
        }
    }

    public List<HistoryItem> getItems() {
        return historiesAdapter.getItems();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.listener = (HistoryListener) getParentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_histories, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        historiesPresenter.requestHistories(0);
    }

    private void bindViews(View view) {
        historiesAdapter = new HistoriesAdapter(getHistoryType(), new HistoriesAdapter.OnItemClickedListener() {
            @Override
            public void onItemClicked(HistoryItem item) {
                historiesPresenter.onItemClicked(item);
            }

            @Override
            public void onRemove(HistoryItem item) {
                listener.onRemove(item);
            }

            @Override
            public boolean isEditMode() {
                return listener.isEditMode();
            }

            @Override
            public void onSelect(HistoryItem item) {
                listener.changeSelection(item);
            }

            @Override
            public boolean isLoadingItem(HistoryItem item) {
                return listener.isLoadingItem(item);
            }

            @Override
            public boolean isSelectedItem(HistoryItem item) {
                return listener.isSelectedItem(item);
            }
        });

        historiesAdapter.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {

            }

            @Override
            public void onSwipeClampReached(SwipeLayout swipeLayout, boolean moveToRight) {
                if (!moveToRight) {
                    for (int i = 0; i < historiesAdapter.getItemCount(); i++) {
                        RecyclerView.ViewHolder viewHolder = rvHistories.findViewHolderForAdapterPosition(i);
                        if (viewHolder != null && viewHolder instanceof HistoriesAdapter.HistoryViewHolder) {
                            SwipeLayout currentSwipeLayout = ((HistoriesAdapter.HistoryViewHolder) viewHolder).swipeLayout;
                            if (currentSwipeLayout != swipeLayout) {
                                currentSwipeLayout.animateReset();
                            }
                        }
                    }
                }
            }

            @Override
            public void onLeftStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onRightStickyEdge(SwipeLayout swipeLayout, boolean moveToRight) {
            }
        });

        historiesAdapter.setSwipeCheckListener((swipeLayout) -> {
           return resetSwipeState(swipeLayout);
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvHistories.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                historiesPresenter.requestHistories(totalItemsCount);
            }
        });
        rvHistories.setHasFixedSize(true);
        rvHistories.setLayoutManager(layoutManager);
        rvHistories.setAdapter(historiesAdapter);
    }

    private boolean resetSwipeState(SwipeLayout swipeLayout) {
        boolean isClosed = true;
        for (int i = 0; i < historiesAdapter.getItemCount(); i++) {
            RecyclerView.ViewHolder viewHolder = rvHistories.findViewHolderForAdapterPosition(i);
            if (viewHolder != null && viewHolder instanceof HistoriesAdapter.HistoryViewHolder) {
                SwipeLayout currentSwipeLayout = ((HistoriesAdapter.HistoryViewHolder) viewHolder).swipeLayout;
                if (currentSwipeLayout != swipeLayout && currentSwipeLayout.getOffset() != 0) {
                    isClosed = false;
                    currentSwipeLayout.animateReset();
                }
            }
        }
        return isClosed;
    }

    @Override
    public void showEmptyView() {
        llEmptyView.setVisibility(View.VISIBLE);
        rvHistories.setVisibility(View.GONE);
        listener.onChangeState(type, true);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        this.type = (HistoryType) getArguments().getSerializable(ARGUMENT_HISTORY_TYPE);
        historiesPresenter.setType(type);
        super.onAttach(context);
    }

    @Override
    public void openHistoryScreen(HistoryItem item) {
        Intent intent = HistoryActivity.newIntent(getContext(), item);
        startActivity(intent);
    }

    @Override
    public void showHistories(GetHistoryResponse response) {
        if (response == null || (historiesAdapter.getItemCount() == 0 && response.items.size() == 0)) {
            showEmptyView();
            listener.onAllItemsRemoved();
        } else {
            listener.onChangeState(type, false);
            llEmptyView.setVisibility(View.GONE);
            rvHistories.setVisibility(View.VISIBLE);
            historiesAdapter.addHistories(response.items);
            listener.onItemCountChanged(this, historiesAdapter.getItems().size());
        }
    }

    @Override
    public void resetHistories() {
        if (historiesAdapter != null) {
            historiesAdapter.clearHistories();
        }
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return historiesPresenter;
    }

    public interface HistoryListener {
        void onRemoved(HistoryKey id);

        void onRemove(HistoryItem item);

        void changeSelection(HistoryItem item);

        boolean isLoadingItem(HistoryItem item);

        boolean isSelectedItem(HistoryItem item);

        boolean isEditMode();

        void onItemCountChanged(HistoriesFragment fragment, int count);

        void onAllItemsRemoved();

        void onChangeState(HistoryType type, boolean empty);
    }
}
