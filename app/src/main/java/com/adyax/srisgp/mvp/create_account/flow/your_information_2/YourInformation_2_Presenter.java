package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import android.os.Bundle;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.CitiesCommand;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class YourInformation_2_Presenter extends PresenterAppReceiver<YourInformation_2_Presenter.CreateRecipeView> {

    @Inject
    FragmentFactory fragmentFactory;

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch(requestCode) {
            case ExecutionService.CREATE_ACCOUNT_ACTION:
                getView().onSuccess();
                break;
            case ExecutionService.CITIES_ACTION:
                final CitiesResponse citiesResponse = data.getParcelable(CommandExecutor.CITIES_RESPONSE);
                getView().updateCities(citiesResponse);
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try{
            if(errorMessage!=null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        }catch (Exception e){

        }
    }

    public void sendRequest(String s) {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                    new CitiesCommand(s), ExecutionService.CITIES_ACTION);
    }

    public void moveToNext() {
        fragmentFactory.startPointsOfInterest_3_Fragment(getContext());
    }

    public interface CreateRecipeView extends IView {
        void updateCities(CitiesResponse termsResponse);
        void onFail(ErrorResponse errorResponse);
        void onSuccess();
    }

    public YourInformation_2_Presenter() {
    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
        App.getApplicationComponent().inject(this);
    }


    public void sendFakeRegistration() {
        final IViewPager activity = (IViewPager) getContext();
        activity.getCreateAccountHelper().sendCreateAccountCommand(getContext(), getAppReceiver(), CreateAccountCommand.FAKE_REGISTER);
    }

}
