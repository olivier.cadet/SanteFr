package com.adyax.srisgp.mvp.framework;

import android.database.Cursor;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseCursorRecyclerAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    protected Cursor cursor;
    protected int currentPosition = -1;

    public BaseCursorRecyclerAdapter() {
        super();
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        if (currentPosition != position) {
            if (cursor != null && cursor.moveToPosition(position)) {
                onBindViewHolder(holder, cursor, position);
            }
        } else
            onBindViewHolder(holder, null, position);
    }

    public abstract void onBindViewHolder(T holder, Cursor cursor, int position);

    @Override
    public int getItemCount() {
        if (cursor != null && !cursor.isClosed()) {
            return cursor.getCount();
        }
        return 0;
    }

    public void swapCursor(Cursor cursor) {
        if (this.cursor != null && !this.cursor.isClosed()){
            if(cursor!=null&& !this.cursor.equals(cursor)) {
                this.cursor.close();
            }
        }
        this.cursor = cursor;
        notifyDataSetChanged();
    }

    public Cursor getCursor() {
        return cursor;
    }

}
