package com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes;

import com.adyax.srisgp.App;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertsSwipePresenter extends Presenter<AlertsSwipePresenter.CreateRecipeView> {

    @Inject
    IAlerts alerts;

    public IAlerts getAlerts() {
        return alerts;
    }

    public interface CreateRecipeView extends IView {
    }

    public AlertsSwipePresenter() {
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
        App.getApplicationComponent().inject(this);
    }

}
