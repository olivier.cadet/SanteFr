package com.adyax.srisgp.mvp.contact;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.FeedbackOption;
import com.adyax.srisgp.databinding.FragmentContactBinding;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.Utils;
import com.adyax.srisgp.utils.ViewUtils;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 11/21/16.
 */

public class ContactFragment extends ViewFragment implements ContactPresenter.ContactView {

    @Inject
    ContactPresenter contactPresenter;

    private FragmentContactBinding binding;

    private List<FeedbackOption> subjectValues;
    private List<FeedbackOption> transmitterQualityValues;

    public static ContactFragment newInstance() {
        ContactFragment fragment = new ContactFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        contactPresenter.getForm();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        contactPresenter.onViewCreated();
    }

    private void bindViews(View view) {
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.contact);

        ViewUtils.setCloseKeyboardAfterTouchNonEditTextView(view, false);

        binding.transmitterQualitySelect.showHint((int) binding.etSurname.getTextSize());
        binding.spinnerSubjects.showHint((int) binding.etSurname.getTextSize());

        String footerText = getString(R.string.contact_footer_text)+" ";
        String emailText = getString(R.string.contact_footer_email);

        SpannableString ss = new SpannableString(footerText + emailText);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }

            @Override
            public void onClick(View widget) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                emailIntent.setType("vnd.android.cursor.item/email");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {emailText});
                startActivity(Intent.createChooser(emailIntent, "Send mail using..."));
            }
        };
        ss.setSpan(clickableSpan, footerText.length(), footerText.length() + emailText.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        binding.tvFooter.setText(ss);
        binding.tvFooter.setMovementMethod(LinkMovementMethod.getInstance());

        binding.transmitterQualitySelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String transmitterQualitySelectValue = getValueForLabel(binding.transmitterQualitySelect.getSelectedItem().toString(), transmitterQualityValues);

                if (transmitterQualitySelectValue != null) {
                    if (transmitterQualitySelectValue.equals("professionnel")) {
                        binding.etIdRpps.setVisibility(View.VISIBLE);
                        binding.etIdRppsHelp.setVisibility(View.VISIBLE);
                    } else {
                        binding.etIdRppsHelp.setVisibility(View.GONE);
                        binding.etIdRpps.setVisibility(View.GONE);
                        binding.etIdRpps.setText("");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.btnSend.setOnClickListener(v -> {
            if (binding.etSurname.getText().toString().isEmpty()) {
                setError(binding.etSurname, getString(R.string.contact_surname_error));
            } else if (binding.etName.getText().toString().isEmpty()) {
                setError(binding.etName, getString(R.string.contact_name_error));
            } else if (binding.etEmail.getText().toString().isEmpty()) {
                setError(binding.etEmail, getString(R.string.contact_email_error));
            } else if (!Utils.isEmailValid(binding.etEmail.getText().toString().trim())) {
                setError(binding.etEmail, getString(R.string.invalid_email_address));
            } else if (binding.transmitterQualitySelect.isEmpty()) {
                TextView errorText = (TextView) binding.transmitterQualitySelect.getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(R.string.contact_transmitterQuality_error);
            } else if (binding.etIdRpps.getVisibility() == View.VISIBLE && (binding.etIdRpps.getText().toString().length() != 0 && binding.etIdRpps.getText().toString().length() != 9 && binding.etIdRpps.getText().toString().length() != 10 && binding.etIdRpps.getText().toString().length() != 11)) {
                setError(binding.etIdRpps, getString(R.string.contact_id_rpps_error));

            } else if (binding.spinnerSubjects.isEmpty()) {
                TextView errorText = (TextView) binding.spinnerSubjects.getSelectedView();
                errorText.setError("");
                errorText.setTextColor(Color.RED);
                errorText.setText(R.string.contact_subject_error);

//                new AlertDialog.Builder(getContext())
//                        .setMessage(R.string.contact_subject_error)
//                        .setPositiveButton("OK", null)
//                        .show();
            } else if (binding.etMessage.getText().toString().isEmpty()) {
                setError(binding.etMessage, getString(R.string.contact_message_error));
            } else {
                String transmitterQualitySelectValue = getValueForLabel(binding.transmitterQualitySelect.getSelectedItem().toString(), transmitterQualityValues);
                String subjectValue = getValueForLabel(binding.spinnerSubjects.getSelectedItem().toString(), subjectValues);

                contactPresenter.onSubmitForm(
                        binding.etSurname.getText().toString(),
                        binding.etName.getText().toString(),
                        binding.etEmail.getText().toString(),
                        transmitterQualitySelectValue,
                        binding.etIdRpps.getText().toString(),
                        subjectValue,
                        binding.etMessage.getText().toString()
                );
            }
        });
    }

    private String getValueForLabel(String label, List<FeedbackOption> values) {
        if (values != null) {
            FeedbackOption option;
            for (int i = 0; i < values.size(); i++) {
                option = values.get(i);
                if (option.label.equals(label)) {
                    return option.value;
                }
            }
        }

        return null;
    }

    @Override
    public void clearForm(boolean clearEmailField) {
        binding.etSurname.setText("");
        binding.etName.setText("");

        if (clearEmailField) {
            binding.etEmail.setText("");
        }
        binding.spinnerSubjects.showHint();
        binding.etMessage.setText("");

        binding.etSurname.requestFocus();
    }

    private void setError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
        editText.requestFocus();
    }

    @Override
    public void showTransmitterQualityValues(List<FeedbackOption> options) {
        this.transmitterQualityValues = options;

        String[] optionTitles = new String[options.size()];
        FeedbackOption option;
        for (int i = 0; i < options.size(); i++) {
            option = options.get(i);
            optionTitles[i] = option.label;
        }

        binding.transmitterQualitySelect.setObjects(optionTitles);
    }

    @Override
    public void showSubjectValues(List<FeedbackOption> options) {
        this.subjectValues = options;

        String[] optionTitles = new String[options.size()];
        FeedbackOption option;
        for (int i = 0; i < options.size(); i++) {
            option = options.get(i);
            optionTitles[i] = option.label;
        }

        binding.spinnerSubjects.setObjects(optionTitles);
    }

    @Override
    public void showEmail(String email) {
        if (binding.etEmail.getText().length() == 0) {
            binding.etEmail.setText(email);
        }
    }

    @Override
    public void hideLoading() {
        binding.progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSuccessDialog() {
        new AlertDialog.Builder(getContext())
                .setMessage(R.string.contact_success_message)
                .setPositiveButton("OK", null)
                .show();
    }

    @NonNull
    @Override
    public IPresenter getPresenter() {
        return contactPresenter;
    }
}
