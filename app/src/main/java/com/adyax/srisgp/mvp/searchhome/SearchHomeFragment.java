package com.adyax.srisgp.mvp.searchhome;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.UrlUtility;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.authentication.login.LoginActivity;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.mvp.search.SearchActivity;
import com.adyax.srisgp.mvp.search.SearchArgument;
import com.adyax.srisgp.mvp.search.SearchFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.mvp.web_views.WebViewFragment;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/21/16.
 */
public class SearchHomeFragment extends ViewFragment
        implements SearchHomePresenter.SearchHomeView {

    private static final int REQUEST_READ = 127;

    @BindView(R.id.rvCards)
    RecyclerView rvCards;

    @Inject
    SearchHomePresenter searchHomePresenter;
    @Inject
    INetWorkState netWorkState;
    @Inject
    IAlerts alerts;
    @Inject
    IRepository repository;
    @Inject
    FragmentFactory fragmentFactory;
    @Inject
    ITracker tracker;

    private SearchHomeAdapter searchHomeAdapter;
    private ContentObserver contentObserver = new ContentObserver(new Handler()) {
        @Override
        public void onChange(boolean selfChange) {
            searchHomeAdapter.update();
            searchHomeAdapter.notifyDataSetChanged();
        }
    };

    public static SearchHomeFragment newInstance() {
        new TrackerHelper().addScreenView(TrackerLevel.HOMEPAGE, TrackerAT.HOMEPAGE);
        Bundle args = new Bundle();
        SearchHomeFragment fragment = new SearchHomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_search_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        getActivity().getContentResolver().registerContentObserver(TablesContentProvider.ALERTS_CONTENT_URI, true, contentObserver);
//        getActivity().getContentResolver().notifyChange(TablesContentProvider.ALERTS_CONTENT_URI, null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_READ) {
            if (resultCode == Activity.RESULT_OK) {
                boolean favorite = data.getBooleanExtra(WebViewFragment.EXTRA_RESULT_FAVORITE, false);
                long id = data.getLongExtra(WebViewFragment.EXTRA_RESULT_ID, -1);
                if (id != -1) {
                    searchHomeAdapter.notifyCardChanged(id, favorite);
                }
            } else {

            }
        } else if (requestCode == SearchFragment.REQUEST_SEARCH_RESULTS) {
            if (resultCode == Activity.RESULT_OK) {
                final int clicked = data.getIntExtra(SearchFragment.INTENT_CLICKED, SearchFragment.EDIT_CLICKED);
                if (clicked == SearchFragment.EDIT_CLICKED) {
//                    etSearch.requestFocus();
                } else if (clicked == SearchFragment.CLEAR_CLICKED) {
//                    etSearch.getText().clear();
//                    etLocation.getText().clear();
//                    etSearch.requestFocus();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getContentResolver().unregisterContentObserver(contentObserver);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!netWorkState.isLoggedIn()) {
            inflater.inflate(R.menu.log_in, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_log_in) {
            searchHomePresenter.onLoggedInClicked();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openRegistrationScreen() {
        Intent intent = CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    private void bindViews(View view) {
        searchHomeAdapter = new SearchHomeAdapter(repository, alerts, getChildFragmentManager(),
                netWorkState.isLoggedIn(), new SearchHomeAdapter.OnItemClickedListener() {
            String urgenceButtonUrl = null;

            HashMap<String, String> widgetData = new HashMap<String, String>();
            JSONObject widgetJSON = null;

            @Override
            public void onSearchBarClicked() {
                startActivity(SearchActivity.newIntent(getContext(), null));
            }

            @Override
            public void urgenceButtonClicked() {
                fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(urgenceButtonUrl, true), -1);
            }

            @Override
            public void setUrgenceButtonUrl(String url) {
                urgenceButtonUrl = url;
            }

            @Override
            public void onWidgetInfoUpdated(String key, String value) {
                if (key != null) {
                    // Handle particular cases.
                    if (key.equals("sous_cas") && value != null) {
                        try {
                            JSONObject options = widgetJSON.getJSONObject("checkboxes_fieldset").getJSONObject("sous_cas").getJSONObject("#options");
                            Iterator<String> keys = options.keys();
                            value = keys.next();
                            value = value.substring(value.lastIndexOf("|") + 1);
                        } catch (Exception e) {
                            value = null;
                        }
                    }

                    if (value == null) {
                        widgetData.remove(key);
                    } else {
                        widgetData.put(key, value);
                    }
                    Log.e("HeaderWidgetViewHolder", "onWidgetInfoUpdated");
                }
                getWidgetJSON(true);

                // Update View
                // Set Sous_cas text and visibility.
                CheckBox widgetCheckBox = (CheckBox) view.findViewById(R.id.widgetCheckBox);
                try {
                    JSONObject options = widgetJSON.getJSONObject("checkboxes_fieldset").getJSONObject("sous_cas").getJSONObject("#options");
                    value = options.keys().next();
                    widgetCheckBox.setText(options.optString(value));
                    widgetCheckBox.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    widgetCheckBox.setVisibility(View.GONE);
                    Log.e("onWidgetSubmitted", e.getMessage());
                }
            }

            @Override
            public void onWidgetSubmitted() {
                try {
                    String url = widgetJSON.getJSONObject("sgp_widget_page_prev").get("#value").toString();
                    if (!url.equals("null")) {
                        // Clear current data.
                        widgetJSON = null;
                        // Open web view.
                        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(url, true), -1);
                    }
                } catch (Exception e) {
                    Log.e("onWidgetSubmitted", e.getMessage());
                }
            }

            @Override
            public JSONObject getWidgetJSON(Boolean update) {
                if (update || widgetJSON == null) {
                    String params = "?";
                    if (widgetData.get("age") != null) {
                        params += "&age=" + widgetData.get("age");
                    }
                    if (widgetData.get("sexe") != null) {
                        params += "&sexe=" + widgetData.get("sexe");
                    }
                    if (widgetData.get("sous_cas") != null) {
                        params += "&sous_cas=" + widgetData.get("sous_cas");
                    }

                    // Cache to avoid multiple url calls.
                    String cache_params = widgetData.get("cache_params");
                    if (cache_params == null || !cache_params.equals(params) || widgetJSON == null) {
                        UrlUtility urlutility = new UrlUtility(UrlExtras.getBaseURL() + UrlExtras.API_WIDGET_FORM + params);
                        urlutility.setBasicAuth(BuildConfig.CREDENTIAL_SERVER);
                        widgetJSON = urlutility.readUrlJson();
                        widgetData.put("cache_params", params);
                    }
                }
                return widgetJSON;
            }

            @Override
            public void onNoMoreCards() {
                Snackbar.make(view, R.string.search_home_no_more_cards, Snackbar.LENGTH_SHORT);
            }

            @Override
            public void onPersonalizeButtonClicked() {
                getPresenter().onPersonalizeButtonClicked();
            }

            @Override
            public void onMoreInformationClicked() {
                getPresenter().onMoreInformationClicked();
            }

            @Override
            public void onReadClicked(QuickCardsAnonymousItem item) {
                searchHomePresenter.addTouchGesture(item, ClickType.clic_contenu_consultation);
                if (item.getNodeType() == NodeType.RECHERCHE_ENREGISTREE) {
                    startActivityForResult(SearchResultsActivity.newIntent(getContext(), new SearchArgument(item.getCardLink())),
                            SearchFragment.REQUEST_SEARCH_RESULTS);
                } else {
                    final Intent intent = WebViewActivity.newIntent(getContext(),
                            new WebViewArg(item, item.isSimplePage() ? WebPageType.SIMPLE : WebPageType.SERVER, TrackerLevel.HOMEPAGE));
                    startActivityForResult(intent, REQUEST_READ);
                }
                ((FragmentActivity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
            }

            @Override
            public void onFavorite(QuickCardsAnonymousItem card) {
                searchHomePresenter.clickFavorite(card);
            }

            @Override
            public void onCall(QuickCardsAnonymousItem item) {
                new TrackerHelper(item).requestCall(getContext());
            }

            @Override
            public void onDownloadClicked(QuickCardsAnonymousItem item) {
                new TrackerHelper(item).showAppsDialog(getContext(), null);
            }

            @Override
            public void removeCard(QuickCardsAnonymousItem item) {
                searchHomePresenter.addTouchGesture(item, ClickType.clic_homepage_acces_rapides_fermer);
            }

            @Override
            public void onEssaiClinicClicked(String url) {
                // Open web view.
                fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(url, true), -1);
            }
        });
        rvCards.setHasFixedSize(true);
        rvCards.setAdapter(searchHomeAdapter);
        rvCards.setLayoutManager(new LinearLayoutManager(getContext()));
        setUpItemTouchHelper();
    }

    @Override
    public void updateCard(long id, boolean favorite) {
        searchHomeAdapter.notifyCardChanged(id, favorite);
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback touchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                QuickCardsAnonymousItem item = searchHomeAdapter.remove(viewHolder.getAdapterPosition());
//                searchHomePresenter.trackClick(item, ClickType.clic_homepage_acces_rapides_fermer);
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (!searchHomeAdapter.isCardView(viewHolder.getAdapterPosition())) {
                    return 0;
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }
        };

//        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(touchCallback);
//        itemTouchHelper.attachToRecyclerView(rvCards);
    }

    @Override
    public void onLoggedIn() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().invalidateOptionsMenu();
        searchHomeAdapter.notifyLoggedInStateChanged(true);
    }

    @Override
    public void onLoggedOut() {
        ((AppCompatActivity) getActivity()).getSupportActionBar().invalidateOptionsMenu();
        searchHomeAdapter.notifyLoggedInStateChanged(false);
    }

    @Override
    public void openLogInScreen() {
        Intent intent = LoginActivity.newIntent(getContext());
        startActivity(intent);
    }

    @NonNull
    @Override
    public SearchHomePresenter getPresenter() {
        return searchHomePresenter;
    }

    @Override
    public void showCards(List<QuickCardsAnonymousItem> items) {
        searchHomeAdapter.showData(items);
    }

//    @Override
//    public void onResume(){
//        super.onResume();
//        tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(new TrackerPageData(TrackerLevel.HOMEPAGE), TrackerAT.HOMEPAGE).build());
//    }

    @Override
    public void showMessage(String body) {
        Snackbar.make(getView(), body, Snackbar.LENGTH_LONG).show();
    }

}
