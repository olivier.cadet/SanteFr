package com.adyax.srisgp.mvp.web_views;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityCreateAccountBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.presentation.AskGeoPlusAlertPopupActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class WebViewActivity extends AskGeoPlusAlertPopupActivity {

    public static final int WEB_VIEW_REQUEST_CODE = 126;
    public static final int WEB_VIEW_REQUEST_CODE2 = 127;

    private static final String ARG_SHOW_FAVORITE = "arg_show_favorite";

    private ActivityCreateAccountBinding binding;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, WebViewArg webViewArg) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(WebViewArg.CONTENT_URL, webViewArg);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_account);
//        binding.createAccountViewPager.setAdapter(new CreateAccountPagerAdapter(getSupportFragmentManager()));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        WebViewArg webViewArg = getIntent().getParcelableExtra(WebViewArg.CONTENT_URL);
        if (!webViewArg.isRedButton()) {
            setPushAlert(webViewArg.getPushAlert());
        }
        fragmentFactory.startWebViewFragment(this, webViewArg, getIntent().getBooleanExtra(ARG_SHOW_FAVORITE, true));
    }

    @Override
    public void onBackPressed() {
        for (Fragment f : getSupportFragmentManager().getFragments()) {
            if (f != null && f.isVisible() && ((ViewFragment) f).onBackPressed()) {
//                getSupportActionBar().setTitle(R.string.summary);
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
//            case R.id.action_bookmark:
//                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        for (Fragment f : getSupportFragmentManager().getFragments()) {
            if (f != null && f.isVisible()) {
                f.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    protected void startSwipe(boolean bAnimaton) {

    }

    @Override
    public void onStart() {
        super.onStart();
        final IGeoHelper geoHelper = getGeoHelper();
        if (geoHelper != null) {
            geoHelper.setOnLocationListener(location -> {
                update(location);
                geoHelper.disconnect();
            });
            startAskGeo();
        }
    }

    @Override
    public void onStop() {
        final IGeoHelper geoHelper = getGeoHelper();
        super.onStop();
        if (geoHelper != null) {
            geoHelper.disconnect();
        }
    }

    private void startAskGeo() {
        final IGeoHelper geoHelper = getGeoHelper();
        if (!geoHelper.askGeo(this)) {
            update(null);
        }
    }

    protected void update(Location location) {
        repository.saveGeoLocation(location);
        Fragment fragment = fragmentFactory.getCurrentFragment(this);
        if (fragment != null && fragment instanceof WebViewFragment) {
            ((WebViewFragment) fragment).updateLocation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment f : getSupportFragmentManager().getFragments()) {
            if (f != null && f.isVisible()) {
                f.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
