package com.adyax.srisgp.mvp.emergency;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityFeedbackSmallTitleBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class EmergencyActivity extends MaintenanceBaseActivity {

    public static final String EMERGENCY_ARG_2 = "EmergencyArg2";
    private ActivityFeedbackSmallTitleBinding binding;
    public

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, EmergencyArg emergencyArg) {
        Intent intent = new Intent(context, EmergencyActivity.class);
        intent.putExtra(EMERGENCY_ARG_2, emergencyArg);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback_small_title);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final EmergencyArg emergencyArg = getIntent().getParcelableExtra(EMERGENCY_ARG_2);
        fragmentFactory.startEmergencyFragment(this, emergencyArg);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
