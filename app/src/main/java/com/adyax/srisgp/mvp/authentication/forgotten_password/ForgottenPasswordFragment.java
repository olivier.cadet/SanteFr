package com.adyax.srisgp.mvp.authentication.forgotten_password;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.adyax.srisgp.mvp.create_account.CreateAccountHelper;
import com.adyax.srisgp.mvp.framework.ViewFragment;
import com.adyax.srisgp.utils.Utils;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class ForgottenPasswordFragment extends ViewFragment implements ForgottenPasswordPresenter.CreateRecipeView {

    public static final String TYPE_FORRGOTTEN_FRAGMENT = "TypeForrgottenFragment";
    public static final String ARG_EMAIL = "email";

    @Inject
    ForgottenPasswordPresenter forgottenPasswordPresenter;
    private com.adyax.srisgp.databinding.FragmentForgottenPasswordBinding binding;
    private TypeForrgotten typeCreateAccount;

    public ForgottenPasswordFragment() {
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            binding.toSendButton.setEnabled(CreateAccountHelper.
                    getValidateButtonStatus(binding.emailCreateAccount, null));
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public static Fragment newInstance(TypeForrgotten typeCreateAccount) {
        ForgottenPasswordFragment fragment = new ForgottenPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE_FORRGOTTEN_FRAGMENT, typeCreateAccount.name());
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Fragment newInstance(TypeForrgotten typeCreateAccount, String email) {
        ForgottenPasswordFragment fragment = new ForgottenPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TYPE_FORRGOTTEN_FRAGMENT, typeCreateAccount.name());
        bundle.putString(ARG_EMAIL, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        typeCreateAccount= TypeForrgotten.valueOf(getArguments().getString(TYPE_FORRGOTTEN_FRAGMENT));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgotten_password, container, false);

        switch(typeCreateAccount){
            case first_screen:
                binding.toSendButton.setOnClickListener(view -> {
                    ViewUtils.hideKeyBoard(getActivity(), binding.emailCreateAccount);
                    binding.progressLayout.setVisibility(View.VISIBLE);
                    String email = binding.emailCreateAccount.getText().toString().trim();
                    if(Utils.isEmailValid(email)) {
                        getPresenter().sendResetPassword(email);
                    }else{
                        binding.progressLayout.setVisibility(View.INVISIBLE);
                        binding.emailCreateAccount.setError(getString(R.string.invalid_email_address));
                        binding.emailCreateAccount.requestFocus();
                    }
                });
                break;
            case finish_screen:
                binding.submitMessage.setText(R.string.recieve_email);
                binding.toSendButton.setVisibility(View.INVISIBLE);
                binding.enailContainer.setVisibility(View.INVISIBLE);
                binding.backToLogin.setVisibility(View.VISIBLE);
                binding.backToLogin.setOnClickListener(view -> {
                    binding.progressLayout.setVisibility(View.INVISIBLE);
                    getPresenter().startLoginFragment();
                });
                break;
        }
        binding.emailCreateAccount.addTextChangedListener(watcher);
        if (getArguments() != null) {
            String email = getArguments().getString(ARG_EMAIL);
            if (!TextUtils.isEmpty(email) && binding.emailCreateAccount.getText().toString().trim().isEmpty()) {
                binding.emailCreateAccount.setText(email);
                binding.emailCreateAccount.setSelection(email.length());
            }
        }
        return binding.getRoot();
    }

    @NonNull
    @Override
    public ForgottenPasswordPresenter getPresenter() {
        return forgottenPasswordPresenter;
    }

    @Override
    public void onFail(ErrorResponse errorResponse) {
        if(errorResponse.isEmailError()){
            binding.emailCreateAccount.setError(errorResponse.getEmailItem().getLocalizeErrorMessage(getContext()));
            binding.emailCreateAccount.requestFocus();
        }
        if(errorResponse.isError()){
            switch(errorResponse.getErrorItem().getErrorCode()){
                case ErrorItem.WRONG_USER_NAME:
                    binding.emailCreateAccount.setError(errorResponse.getErrorItem().getLocalizeErrorMessage(getContext()));
                    binding.emailCreateAccount.requestFocus();
                    break;
            }
        }
        binding.progressLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onSuccess() {
        binding.progressLayout.setVisibility(View.INVISIBLE);
        switch(typeCreateAccount) {
            case first_screen:
                getPresenter().startShowLastScreen();
                break;
        }
    }
}
