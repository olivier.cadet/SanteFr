package com.adyax.srisgp.mvp.web_views;

import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;

public class DefaultChromeClient extends WebChromeClient {
    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        callback.invoke(origin, true, false);

        // TODO optional if user did not accept localisation prompt the user ?
        // Needed for sante.fr ?
    }
}
