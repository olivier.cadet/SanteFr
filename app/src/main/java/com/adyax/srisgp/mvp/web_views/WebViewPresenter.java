package com.adyax.srisgp.mvp.web_views;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.FeedbackFormValue;
import com.adyax.srisgp.data.net.command.GetAnchorsCommand;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.SubmitFeedbackFormCommand;
import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.emergency.EmergencyArg;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.mvp.search.SearchArgument;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.presentation.AskGeoPlusAlertPopupActivity;
import com.adyax.srisgp.utils.Utils;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.URI;
import java.net.URL;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class WebViewPresenter extends PresenterAppReceiver<WebViewPresenter.WebViewView> implements FacebookCallback<Sharer.Result> {

    public static final String FEEDBACK_HELPFUL_FORM_NAME = "content_helpful";
    public static final String WIDGET_HELPFUL_FORM_NAME = "widget_helpful";
    public static final int SHARE_EMAIL = 0;
    public static final int SHARE_TWITTER = 1;
    public static final int SHARE_FACEBOOK = 2;
    public static final int REQUEST_CODE = 12992;

    private FragmentFactory fragmentFactory;
    private INetWorkState netWorkState;
    private AnchorResponse anchorResponse;
    private WebViewArg webViewArg;
    private ITracker tracker;
    private IDataBaseHelper databaseHelper;
    private SearchArgument searchArgument;

    @Override
    public void attachView(WebViewView view) {
        super.attachView(view);
    }

    @Override
    public void registerListeners() {
        try {
            final WebViewView view = getView();
            if (view != null) {
                final Activity activity = (Activity) view.getContext();
                if (activity instanceof AskGeoPlusAlertPopupActivity) {
                    final IGeoHelper geoHelper = ((AskGeoPlusAlertPopupActivity) activity).getGeoHelper();
                    geoHelper.setOnLocationListener(location -> {
                        try {
                            geoHelper.disconnect();
                            if (searchArgument != null && searchArgument.isAroundMe()) {
                                Intent intent = SearchResultsActivity.newIntent(getContext(),
                                        new SearchArgument(searchArgument.getWhatField(), location).setQuickcardMode());
                                searchArgument = null;
                                ((Activity) view.getContext()).startActivity(intent);
                            }
                        } catch (Exception e) {

                        }
                    });
                }
                ;
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void unregisterListeners() {
        searchArgument = null;
    }

    public boolean isSearchAction(String url) {
        if (InapLinkHelper.isInapp(url)) {
            searchArgument = new SearchArgument(url);
            if (searchArgument.isAroundMe()) {
                startAskGeoPermission();
            } else {
                Intent intent = SearchResultsActivity.newIntent(getContext(), searchArgument);
                ((Activity) getView().getContext()).startActivity(intent);
            }
            return true;
        }
        return false;
    }

    public void startAskGeoPermission() {
        final Activity activity = (Activity) getView().getContext();
        if (activity instanceof AskGeoPlusAlertPopupActivity) {
            ((AskGeoPlusAlertPopupActivity) activity).startAskGeoPermission();
        }
    }


    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.FAVORITE_ACTION:
                if (getView() != null) {
                    AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                    if (command != null && command.getNids() != null && !command.getNids().isEmpty()) {
                        getView().onSuccessFavorite(command.getNids().get(0), command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD));
                    }
                }
                break;
            case ExecutionService.ANCHORS_ACTION:
                if (getView() != null) {
                    getView().onSuccessWeb(data.getParcelable(CommandExecutor.ANCHOR_RESPONSE));
                }
                break;
            case ExecutionService.GET_FORM:
                FeedbackFormResponse formResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                GetFormCommand formCmd = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM_CMD);
                submitForm(formResponse.secret, data.getLong(CommandExecutor.START_TIME), (formCmd != null) ? formCmd.getFormName() : WIDGET_HELPFUL_FORM_NAME);
                break;
            case ExecutionService.SUBMIT_FORM:
                if (getView() != null) {
                    getView().showExitDialog();
                    getView().hideLoading();
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try {
            if (errorMessage != null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        } catch (Exception e) {

        }
    }

    public void clickYes(String contentType) {//ContentResolverHelper
        getTrackerHelper().clickYesNo(ClickType.clic_utile_oui);
        getView().showLoading();
        getForm(contentType);
    }

    public void clickNo(String contentType) {
        getTrackerHelper().clickYesNo(ClickType.clic_utile_non);
        // TODO: // FIXME: 11/18/16
        fragmentFactory.startFeedbackActivity(getContext(), getNodeLink(), contentType);
    }

    private String getNodeLink() {
        return "node/" + ((anchorResponse != null) ? anchorResponse.getId() : "unknown");
    }

    public void clickShare() {
        showChooseShareDialog();
    }

    private void shareEmail() {
        Utils.sendEMail(getContext(), getShareSiteUrl() + " - " + getTitle(), "",
                getContext().getString(R.string.share_mail_start_body) + " " + getTitle() + " " + getView().getUrl());
    }

    private void shareFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentTitle(getTitle())
                .setContentDescription(getWebViewArg().getDescription())
                .setContentUrl(Uri.parse(getView().getUrl()))
                .build();

        if (getView() != null) {
            getView().shareFacebook(content);
        }
    }

    private void shareTwitter() {
        try {
            new TweetComposer.Builder(getContext())
                    .text(getShareSiteUrl() + " - " + getTitle() + "\n")
                    .url(new URL(getView().getUrl()))
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getShareSiteUrl() {
//        return UrlExtras.getBaseURL();
        return getContext().getString(R.string.app_name);
    }

    @NonNull
    private String getTitle() {
        return anchorResponse.getTitle();
    }

    private void showChooseShareDialog() {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.share_via)
                .setItems(R.array.share_types, (dialog, which) -> {
                    switch (which) {
                        case SHARE_EMAIL:
                            shareEmail();
                            break;
                        case SHARE_TWITTER:
                            shareTwitter();
                            break;
                        case SHARE_FACEBOOK:
                            shareFacebook();
                            break;
                    }
                })
                .show();
    }

    public void setAnchorResponse(AnchorResponse anchorResponse) {
        this.anchorResponse = anchorResponse;
    }

    public void setBookMark() {
        if (netWorkState.isLoggedIn()) {
            anchorResponse.setFavorite(!anchorResponse.isFavorite());
            setBookMark(anchorResponse);
        } else {
            getView().showRegistrationScreen();
        }
    }

    private void setBookMark(AnchorResponse anchorResponse) {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new AddOrRemoveFavoriteCommand(getXiti(), anchorResponse.getId(),
                        anchorResponse.isFavorite() ? AddOrRemoveFavoriteCommand.ACTION_ADD :
                                AddOrRemoveFavoriteCommand.ACTION_REMOVE), ExecutionService.FAVORITE_ACTION);
    }

    @Nullable
    private Xiti getXiti() {
        Xiti xiti = anchorResponse.getXiti();
        if (xiti == null) {
            xiti = webViewArg.getXiti();
        } else {
            if (!anchorResponse.fixUrl()) {
                anchorResponse.setUrl(webViewArg.getURI().toString());
            }
            final int level2 = anchorResponse.xiti.getLevel2();
            anchorResponse.xiti.setTrackerLevel(level2 > 0 ? TrackerLevel.valueOf(level2) :
                    webViewArg.getXiti().getScreenTrackerLevel());
        }
        if (xiti == null) {
            // TODO may be need to add nodeType to push info
            // for push notification for example
            xiti = new Xiti(TrackerLevel.UNIT_CONTENTS_INFORMATION);
            webViewArg.setXiti(xiti);
            anchorResponse.setXiti(xiti);
        }
        return xiti;
    }


    private void getForm(String contentType) {
        GetFormCommand getForm = new GetFormCommand(contentType, null);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), getForm, ExecutionService.GET_FORM);
    }

    private void submitForm(String secret, long startTime, String contentType) {
        boolean loggedIn = netWorkState.isLoggedIn();
        FeedbackFormValue value = new FeedbackFormValue(null, "yes", null, getNodeLink(), null);
        SubmitFeedbackFormCommand submitCommand = new SubmitFeedbackFormCommand(loggedIn, contentType, secret, value, startTime);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), submitCommand, ExecutionService.SUBMIT_FORM);
    }

    public void setWebViewArg(WebViewArg webViewArg) {
        this.webViewArg = webViewArg;
    }

    public WebViewArg getWebViewArg() {
        return webViewArg;
    }

    public String writeToHostory(String url, String webTitle) {
        if (!netWorkState.isLoggedIn()) {
//        WebViewArg webViewArg = getWebViewArg();
//        new TrackerHelper(webViewArg.getGetUrl(url, title)).clickForWriteToHistory();
            if (anchorResponse.getXiti() != null) {
                new TrackerHelper(anchorResponse).clickForWriteToHistory();
            } else {
                HistoryItem historyItem = webViewArg.getHistory();
                webTitle = makeTitle(webTitle);
                final String newTitle = webTitle != null ? webTitle : historyItem.getTitle();
                historyItem.setAnonymous(HistoryItem.TYPE_ANONIMUS_HISTORY);
                historyItem.setTitle(newTitle);
                historyItem.setUrl(url);
                historyItem.updateTime();
                databaseHelper.insertOrUpdateHistory(historyItem);
                return newTitle;
            }
        }
        return makeTitle(webTitle);
    }

    public void trackScreen(boolean bOldStyle) {
        if (bOldStyle) {
            switch (getWebViewArg().getWebPageType()) {
                case TRANSFER:
                    break;
                default:
                    // for LIENS_EXTERNES, APPLICATIONS, NUMERO_TEL
                    tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(getWebViewArg()).build());
                    break;
            }
        } else {
            if (anchorResponse != null && anchorResponse.getXiti() != null) {
                new TrackerHelper(anchorResponse).addScreenView();
            }
        }
    }

    public static String makeTitle(String title) {
        if (title != null && title.length() > 0) {
            if (!title.startsWith("http://")) {
                if (title.contains("|")) {
                    return title.substring(0, title.indexOf("|")).trim();
                } else {
                    return title;
                }
            }
        }
        return null;
    }

    public interface WebViewView extends IView {
        String getUrl();

        String getPostData();

        void update();

        void onFail(ErrorResponse errorResponse);

        void onSuccessWeb(AnchorResponse anchorResponse);

        void showMessage(String message);

        void showExitDialog();

        void showHttpErrorDialog(String url);

        void onSuccessFavorite(long id, boolean favorite);

        void shareFacebook(ShareLinkContent shareLinkContent);

        void startActivity(Intent intent);

        void showLoading();

        void hideLoading();

        void showRegistrationScreen();
    }

    @Inject
    public WebViewPresenter(FragmentFactory fragmentFactory, INetWorkState netWorkState, ITracker tracker, IDataBaseHelper databaseHelper) {
        this.fragmentFactory = fragmentFactory;
        this.netWorkState = netWorkState;
        this.tracker = tracker;
        this.databaseHelper = databaseHelper;
    }

    public boolean sendGetAnchor(URI uri) {
        if (uri != null) {
            ExecutionService.sendCommand(getContext(), getAppReceiver(),
                    new GetAnchorsCommand(uri.getPath()), ExecutionService.ANCHORS_ACTION);
            return true;
        }
        return false;
    }

    public AnchorResponse getAnchorResponse() {
        return anchorResponse;
    }

    /**
     * Facebook
     */

    @Override
    public void onSuccess(Sharer.Result result) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    @NonNull
    public TrackerHelper getTrackerHelper() {
//        if(netWorkState.isLoggedIn()) {
//            return new TrackerHelper(webViewArg);
//        }
        // it has to work with authorized also
        if (anchorResponse != null && anchorResponse.getXiti() != null) {
            return new TrackerHelper(anchorResponse);
        }
        return new TrackerHelper(webViewArg);
    }

    public void emergency(String link) {
        getTrackerHelper().clickSignaler(TrackerLevel.UNIT_CONTENTS_FIND,
                ClickType.clic_signaler_temps_urgence);
        fragmentFactory.startEmergencyActivity(getContext(),
                new EmergencyArg(link, getAnchorResponse()), REQUEST_CODE);
    }

}
