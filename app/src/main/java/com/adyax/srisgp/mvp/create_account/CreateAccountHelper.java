package com.adyax.srisgp.mvp.create_account;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.EditText;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.LoginUserCommand;
import com.adyax.srisgp.data.net.command.LoginUserSsoCommand;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.adyax.srisgp.utils.Utils;

import java.util.Set;

/**
 * Created by SUVOROV on 9/30/16.
 */

public class CreateAccountHelper implements Parcelable {

    public static final int MINIMUM_SYMBOLS_LOGIN = 8;
    public static final int MINIMUM_SYMBOLS_CREATE = 12;
    public static final boolean CREATE_ACCOUNT_ACTION = true;
    public static final boolean LOGIN_ACTION = !CREATE_ACCOUNT_ACTION;
    private CreateAccountCommand createAccountCommand = new CreateAccountCommand();
    private boolean bTypeAction;

    public CreateAccountHelper(boolean bTypeAction) {
        this.bTypeAction = bTypeAction;
    }

    protected CreateAccountHelper(Parcel in) {
        createAccountCommand = in.readParcelable(CreateAccountCommand.class.getClassLoader());
    }

    public static final Creator<CreateAccountHelper> CREATOR = new Creator<CreateAccountHelper>() {
        @Override
        public CreateAccountHelper createFromParcel(Parcel in) {
            return new CreateAccountHelper(in);
        }

        @Override
        public CreateAccountHelper[] newArray(int size) {
            return new CreateAccountHelper[size];
        }
    };

    public Integer setEmailValidate(String mail) {
        if (!Utils.isEmailValid(mail)) {
            return R.string.invalid_email_address;
        }
        createAccountCommand.setMail(mail);
        return null;
    }

    public Integer setPasswordValidate(String pass, boolean bLogin) {
        int minimumSymbols = (bLogin) ? MINIMUM_SYMBOLS_LOGIN : MINIMUM_SYMBOLS_CREATE;
        if(bLogin){
            createAccountCommand.setPass(pass);
        }
        if (pass.length() < minimumSymbols) {
            return R.string.a_bit_characters;
        }
        if (!Utils.isPasswordValid(pass)) {
            return bTypeAction == CREATE_ACCOUNT_ACTION ? R.string.password_rule : R.string.incorrect_password_login;
        }
        createAccountCommand.setPass(pass);
        return null;
    }

    public boolean validateSex(GenderType gender, String age, Long postal) {
        if (!(gender == null || age == null || postal == null)) {
            setSex(gender);
            setYear(Integer.parseInt(age));
            createAccountCommand.setPostal(postal);
            return true;
        }
        return false;
    }

    public boolean setSex(GenderType gender) {
        if (gender != null && gender != GenderType.empty) {
            createAccountCommand.setGender(gender);
            return true;
        }
        return false;
    }

    public void setGender(GenderType gender) {
        createAccountCommand.setGender(gender!=null? gender: GenderType.empty);
    }

    public void setYear(int year) {
        createAccountCommand.setAge(year);
    }

    public String getSex() {
        GenderType genderType = createAccountCommand.field_gender;
        if (genderType != null)
            return genderType.name();
        return "";
    }

    public boolean setIds(Set<Long> ids) {
        createAccountCommand.setIds(ids);
        return true;
    }

    public void setSsoRegister(Sso_type type, String clientId, String email, String token) {
        createAccountCommand.setSsoRegister(type, clientId, email, token);
    }

    public void setTwitterSsoRegister(String clientId, String email, String token, String twitterTokenSecret, String twitterScreenName) {
        createAccountCommand.setSsoRegisterTwitter(Sso_type.sgp_sso_twitter, clientId, email, token, twitterTokenSecret, twitterScreenName);
    }

    public void sendCreateAccountCommand(Context context, AppReceiver appReceiver, int register) {
        createAccountCommand.setRegister(register);
        ExecutionService.sendCommand(context, appReceiver, createAccountCommand, ExecutionService.CREATE_ACCOUNT_ACTION);
    }

    public void sendLoginCommand(Context context, AppReceiver appReceiver) {
        ExecutionService.sendCommand(context, appReceiver, new LoginUserCommand(createAccountCommand), ExecutionService.CREATE_ACCOUNT_ACTION);
    }

    public void sendLoginSsoCommand(Context context, AppReceiver appReceiver, boolean bCreateAccount) {
        LoginUserSsoCommand loginUserSsoCommand = new LoginUserSsoCommand(createAccountCommand);
        if(bCreateAccount&& createAccountCommand.isGooglePlus()){
            createAccountCommand.setFirstLogin();
        }
        ExecutionService.sendCommand(context, appReceiver, loginUserSsoCommand,
                createAccountCommand.isGooglePlus()? ExecutionService.LOGIN_ACTION: ExecutionService.CREATE_ACCOUNT_ACTION);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(createAccountCommand, i);
    }

    public static boolean getValidateButtonStatus(EditText email_ed, EditText pass_ed) {
        final String email = email_ed.getText().toString().trim();
        return email.length() > 0 && (pass_ed == null || pass_ed.getText().length() >= MINIMUM_SYMBOLS_LOGIN) && Utils.isEmailValid(email);
    }
    public static boolean getValidateButtonStatus(EditText email_ed, EditText pass_ed, boolean cgu_cb) {
        final String email = email_ed.getText().toString().trim();
        return cgu_cb && email.length() > 0 && (pass_ed == null || pass_ed.getText().length() >= MINIMUM_SYMBOLS_CREATE) && Utils.isEmailValid(email);
    }

}
