package com.adyax.srisgp.mvp.authentication.forgotten_password;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.command.ResetCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class ForgottenPasswordPresenter extends PresenterAppReceiver<ForgottenPasswordPresenter.CreateRecipeView> {

    @Inject
    FragmentFactory fragmentFactory;

    public void startLoginFragment() {
        fragmentFactory.startLoginFragmentAnimate(getContext());
    }

    public interface CreateRecipeView extends IView {
        void onFail(ErrorResponse errorResponse);
        void onSuccess();
    }

    public ForgottenPasswordPresenter() {
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        getView().onSuccess();
    }

    public void startShowLastScreen() {
        fragmentFactory.startForgottenPasswordFragmentFinish(getContext());
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        /*
        try{
            if(errorMessage!=null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        }catch (Exception e){

        }
        */
        getView().onSuccess();
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
        App.getApplicationComponent().inject(this);
    }

    public void sendResetPassword(String email) {
        App.getApplicationComponent().inject(this);
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                                new ResetCommand(email), ExecutionService.RESET_ACTION);
    }

}
