package com.adyax.srisgp.mvp.framework;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;

/**
 * Created by Kirill on 30.09.16.
 */

public class BasePresenter<V extends IView> extends Presenter<V> implements AppReceiver.AppReceiverCallback {

    private AppReceiver appReceiver = new AppReceiver();

    @Override
    public void attachView(@NonNull V view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        appReceiver.setListener(null);
    }

    public void sendCommand(int requestCode, ServiceCommand command) {
        if (getView() != null) {
            ExecutionService.sendCommand(getContext(), appReceiver, command, requestCode);
        }
    }

    /**
     * AppReceiverCallback
     */

    @Override
    public void onSuccess(int requestCode, Bundle data) {

    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }
}
