package com.adyax.srisgp.mvp.history;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.utils.Utils;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.rambler.libs.swipe_layout.SwipeLayout;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class HistoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<HistoryItem> items = new ArrayList<>();
    private OnItemClickedListener listener;
    private SwipeLayout.OnSwipeListener onSwipeListener;
    private SwipeCheckListener swipeCheckListener;

    private HistoryType historyType;

    public HistoriesAdapter(HistoryType historyType, OnItemClickedListener listener) {
        this.historyType = historyType;
        this.listener = listener;
    }

    public void clearHistories() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public void setOnSwipeListener(SwipeLayout.OnSwipeListener onSwipeListener) {
        this.onSwipeListener = onSwipeListener;
    }

    public void setSwipeCheckListener(SwipeCheckListener swipeCheckListener) {
        this.swipeCheckListener = swipeCheckListener;
    }

    public List<HistoryItem> getItems() {
        return items;
    }

    public void addHistories(List<HistoryItem> histories) {
        this.items.addAll(histories);
        notifyDataSetChanged();
    }

    public void requestRemove(HistoryItem item) {
        if (listener != null) {
            listener.onRemove(item);
        }
    }

    public void removeItem(HistoryKey id) {
        int itemPosition = getHistoryItemPosition(id);
        if (itemPosition != -1) {
            removeHistoryItem(itemPosition);
        }
    }

    private void removeHistoryItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    public void notifyItemChangedById(HistoryKey id) {
        int itemPosition = getHistoryItemPosition(id);
        if (itemPosition != -1) {
            notifyItemChanged(itemPosition);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View notificationView = inflater.inflate(R.layout.item_notification, parent, false);
        return new HistoryViewHolder(notificationView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HistoryViewHolder viewHolder = (HistoryViewHolder) holder;
        viewHolder.bind(getHistory(position));
    }

    private int getHistoryItemPosition(HistoryKey id) {
        HistoryItem item;
        for (int i = 0; i < items.size(); i++) {
            item = items.get(i);
            if (id.sid == item.sid && id.nid == item.nid) {
                return i;
            }
        }
        return -1;
    }

    private boolean isLoadingItem(HistoryItem item) {
        return listener.isLoadingItem(item);
    }

    private boolean isSelectedItem(HistoryItem item) {
        return listener.isSelectedItem(item);
    }

    public void changeSelectionMode(HistoryItem item) {
        int itemPosition = getHistoryItemPosition(new HistoryKey(item.sid, item.nid));
        if (itemPosition != -1) {
            notifyItemChanged(itemPosition);
        }
    }

    private HistoryItem getHistory(int position) {
        return items.get(position);
    }

    class HistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivIcon)
        ImageView ivIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.ivArrow)
        ImageView ivArrow;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.llContent)
        LinearLayout llContent;
        @BindView(R.id.item_delete)
        View deleteView;
        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;

        HistoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void bind(HistoryItem item) {
            if (!isLoadingItem(item)) {
                final String getIcon_url = item.getIcon_url();
                if (getIcon_url != null) {
                    Glide.with(ivIcon.getContext())
                            .load(getIcon_url)
                            .error(item.getIcon().getResourceId())
                            .into(ivIcon);
                } else {
                    ivIcon.setImageResource(item.getIcon().getResourceId());
                }
                swipeLayout.setVisibility(View.VISIBLE);
                swipeLayout.reset();
                progressBar.setVisibility(View.GONE);
                if (listener.isEditMode()) {
                    checkBox.setVisibility(View.VISIBLE);
                    ivArrow.setVisibility(View.INVISIBLE);

                    if (isSelectedItem(item)) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                    swipeLayout.setSwipeEnabled(false);
                } else {
                    checkBox.setVisibility(View.GONE);

                    if (historyType == HistoryType.search) {
                        ivArrow.setVisibility(View.GONE);
                    } else {
                        ivArrow.setVisibility(View.VISIBLE);
                    }
                    swipeLayout.setSwipeEnabled(true);
                }

                llContent.setOnClickListener(v -> {
                    if (swipeLayout.getOffset() != 0) {
                        swipeLayout.animateReset();
                    } else {
                        if (swipeCheckListener == null || swipeCheckListener.isAllSwipeClosed(swipeLayout)) {
                            if (listener.isEditMode()) {
                                listener.onSelect(item);
                            } else if (historyType != HistoryType.search) {
                                listener.onItemClicked(item);
                            }
                        }
                    }
                });

                tvTitle.setText(item.getTitle());
                tvDate.setText(App.getAppContext().getString(R.string.history_read, Utils.getDateString(item.last_visit * 1000L)));

                swipeLayout.setOnSwipeListener(onSwipeListener);
            } else {
                swipeLayout.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            deleteView.setOnClickListener(v -> requestRemove(item));

            swipeLayout.reset();
        }
    }

    public interface OnItemClickedListener {
        void onItemClicked(HistoryItem item);

        void onRemove(HistoryItem item);

        void onSelect(HistoryItem item);

        boolean isLoadingItem(HistoryItem item);

        boolean isSelectedItem(HistoryItem item);

        boolean isEditMode();
    }

    public interface SwipeCheckListener {
        boolean isAllSwipeClosed(SwipeLayout swipeLayout);
    }
}
