package com.adyax.srisgp.mvp.search;

import android.location.Location;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.feedback.InapLinkHelper;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.tracker.InternalSearchTrackerHelper;

import java.util.List;

/**
 * Created by SUVOROV on 6/15/17.
 */

public class SearchArgument implements Parcelable {

    public static final String BUNDLE_NAME = "searchArgument";

    public static final String TEXT = "text";
    public static final String LOC = "loc";
    public static final String WHAT_FIELD = "WhatField";
    public static final String WHERE_FIELD = "WhereField";

    private String whatField;
    private String whereField;
    private double latitude;
    private double longitude;
    private boolean isAroundMe;
    private boolean isQuickcardMode;

    private GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation;

    public void setStatusLocation(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        this.statusLocation = statusLocation;
    }


    protected SearchArgument(Parcel in) {
        whatField = in.readString();
        whereField = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        isAroundMe = in.readByte() != 0;
        isQuickcardMode = in.readByte() != 0;

        this.statusLocation = in.readParcelable(GetAutoCompleteSearchLocationsResponse.StatusLocation.class.getClassLoader());
    }

    public static final Creator<SearchArgument> CREATOR = new Creator<SearchArgument>() {
        @Override
        public SearchArgument createFromParcel(Parcel in) {
            return new SearchArgument(in);
        }

        @Override
        public SearchArgument[] newArray(int size) {
            return new SearchArgument[size];
        }
    };

    public SearchArgument(String whatField, Location location) {
        this(whatField, location.getLatitude(), location.getLongitude());
    }

    public SearchArgument(String whatField, double latitude, double longitude) {
        this.whatField = whatField;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isAroundMe = true;
        isQuickcardMode = false;
    }

    public boolean isQuickcardMode() {
        return isQuickcardMode;
    }

    public SearchArgument(String whatField, String whereField) {
        this.whatField = whatField;
        this.whereField = whereField;
        this.isAroundMe = false;
        isQuickcardMode = false;
    }

    public SearchArgument(String url) {
        // like https://stage.sris-gp.adyax-dev.com/recherche/find#!text=docteur&loc=Paris
        final Uri uri = Uri.parse(url.replace("#!", "?"));
        final boolean isInapp = InapLinkHelper.INAPP.equals(uri.getScheme());
        final List<String> whatFields = uri.getQueryParameters(isInapp ? WHAT_FIELD : TEXT);
        final List<String> whereFields = uri.getQueryParameters(isInapp ? WHERE_FIELD : LOC);
        whatField = whatFields.size() == 1 ? whatFields.get(0) : "";
        whereField = whereFields.size() == 1 ? whereFields.get(0) : "";
        isAroundMe = InternalSearchTrackerHelper.AUTOUR_DE_MOI.equals(whereField);
        isQuickcardMode = true;
    }

    public boolean isAroundMe() {
        return isAroundMe;
    }

    public String getWhatField() {
        return whatField;
    }

    public String getWhereField() {
        return isAroundMe ? InternalSearchTrackerHelper.AUTOUR_DE_MOI : whereField;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(whatField);
        parcel.writeString(whereField);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        parcel.writeByte((byte) (isAroundMe ? 1 : 0));
        parcel.writeByte((byte) (isQuickcardMode ? 1 : 0));

        if (this.statusLocation != null) {
            parcel.writeParcelable(this.statusLocation, flags);
        }
    }

    public void updateAroundMe(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public SearchArgument setQuickcardMode() {
        isQuickcardMode = true;
        return this;
    }

}
