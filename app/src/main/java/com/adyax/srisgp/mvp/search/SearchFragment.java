package com.adyax.srisgp.mvp.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchPhrasesResponse;
import com.adyax.srisgp.data.net.response.PopularResponse;
import com.adyax.srisgp.data.net.response.tags.SearchResult;
import com.adyax.srisgp.data.tracker.InternalSearchTrackerHelper;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.MultiplePresenterViewFragment;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.presentation.BaseActivity;
import com.adyax.srisgp.utils.DialogUtils;
import com.adyax.srisgp.utils.PermissionUtils;
import com.adyax.srisgp.utils.Utils;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 9/22/16.
 */
public class SearchFragment extends MultiplePresenterViewFragment
        implements SearchPresenter.SearchView, SearchLocationsPresenter.SearchLocationsView, SearchPhrasesPresenter.SearchPhrasesView {

    public static final String INTENT_CLICKED = "clicked";
    public static final int EDIT_CLICKED = 1;
    public static final int CLEAR_CLICKED = 2;

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    public static final int REQUEST_SEARCH_RESULTS = 2;

    private static final String BUNDLE_AROUND_ME_STATE = "around_me_state";

    @BindView(R.id.iBtnDismiss)
    ImageButton iBtnDismiss;
    @BindView(R.id.llSearch)
    LinearLayout llSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.rlSearchInput)
    RelativeLayout rlSearchInput;
    @BindView(R.id.etLocation)
    EditText etLocation;
    @BindView(R.id.rlLocationInput)
    RelativeLayout rlLocationInput;
    @BindView(R.id.rvAutocomplete)
    RecyclerView rvAutocomplete;
    @BindView(R.id.iBtnClear)
    ImageButton iBtnClear;
    @BindView(R.id.tvProvideLocation)
    TextView tvProvideLocation;
    @BindView(R.id.llProvideLocation)
    LinearLayout llProvideLocation;
    @BindView(R.id.ivLocationImage)
    ImageView ivLocationImage;
    @BindView(R.id.iBtnLocationClear)
    ImageButton iBtnLocationClear;

    @Inject
    SearchPresenter searchPresenter;
    @Inject
    SearchLocationsPresenter locationsPresenter;
    @Inject
    SearchPhrasesPresenter phrasesPresenter;

    private SearchPhrasesAdapter searchPhrasesAdapter;
    private SearchLocationsAdapter searchLocationsAdapter;
    private ProvideLocationFragment.OnPromptLocationSettingsListener listener;

    private boolean aroundMeClicked = false;
    private boolean aroundMeState = false;
    private boolean willBeAroundMeState = false;

    private boolean selectedWhereAutoComplete = false;

    private List<IPresenter> presenters;
    private GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation = GetAutoCompleteSearchLocationsResponse.StatusLocation.createEmpty();
    private int count;

    public static SearchFragment newInstance(SearchArgument searchArgument) {
        Bundle bundle = new Bundle();
        if (searchArgument != null) {
            bundle.putParcelable(SearchArgument.BUNDLE_NAME, searchArgument);
        }
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(BUNDLE_AROUND_ME_STATE, aroundMeState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            aroundMeState = savedInstanceState.getBoolean(BUNDLE_AROUND_ME_STATE, false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SEARCH_RESULTS) {
            if (resultCode == Activity.RESULT_OK) {
                int clicked = data.getIntExtra(INTENT_CLICKED, EDIT_CLICKED);
                if (clicked == EDIT_CLICKED) {
                    etSearch.requestFocus();
                } else if (clicked == CLEAR_CLICKED) {
                    etSearch.getText().clear();
                    etLocation.getText().clear();
                    etSearch.requestFocus();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        final Object object = getArguments().get(SearchArgument.BUNDLE_NAME);
        if (object instanceof SearchArgument) {
//            searchArgument=(SearchArgument)object;
            locationsPresenter.setSearchArgument((SearchArgument) object);
        }
        presenters = new ArrayList<IPresenter>(3) {{
            add(searchPresenter);
            add(locationsPresenter);
            add(phrasesPresenter);
        }};
        super.onAttach(context);
        listener = (ProvideLocationFragment.OnPromptLocationSettingsListener) context;
    }

    @NonNull
    @Override
    public List<IPresenter> getPresenters() {
        return presenters;
    }

    @Override
    public void onStart() {
        super.onStart();
        locationsPresenter.loadAutocompleteData();
        phrasesPresenter.loadAutocompleteData();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews(view);
    }

    private void bindViews(View view) {
        String locationHeader = getString(R.string.search_specify_location_header);
        String locationText = getString(R.string.search_specify_location_text);

        final SpannableStringBuilder str = new SpannableStringBuilder(locationHeader + "\n" + locationText);
        str.setSpan(new StyleSpan(Typeface.BOLD), 0, locationHeader.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvProvideLocation.setText(str);

        searchPhrasesAdapter = new SearchPhrasesAdapter(new SearchPhrasesAdapter.OnItemClickedListener() {
            @Override
            public void onRemoveSearchEntry(SearchItemViewModel searchItem) {
                searchPhrasesAdapter.onSetProgressForRecentItem(searchItem);
                phrasesPresenter.removeSearchPhrase(searchItem);
            }

            @Override
            public void onRemoveSearchPhrasesHistory() {
                searchPhrasesAdapter.onSetProgressForSearchHistory();
                phrasesPresenter.removeSearchPhrasesHistory();
            }

            @Override
            public void onSelectAutocomplete(String searchText) {
                searchPresenter.onSearchPhraseSelected(searchText, etLocation.getText().toString());
            }

            @Override
            public void onOpenWebView(SearchResult searchResult) {
                searchPresenter.startWebViewActivity(searchResult);
            }
        });

        searchLocationsAdapter = new SearchLocationsAdapter(new SearchLocationsAdapter.OnItemClickedLister() {
            @Override
            public void onRemoveSearchEntry(RecentLocationViewModel searchItem) {
                searchLocationsAdapter.onSetProgressForRecentItem(searchItem);
                locationsPresenter.removeSearchLocation(searchItem);
            }

            @Override
            public void onSelectAutocomplete(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
//                locationSetText("");
                final String location = statusLocation.getLocation();
                locationsPresenter.setTextFromAutoComplete(location);
                locationSetText(location);
                selectedWhereAutoComplete = true;
                disableSearchSuggestionAdapter();
                locationsPresenter.onSearchLocationSelected(statusLocation);
            }

            @Override
            public void onProvideLocationClicked() {
                aroundMeClicked = false;
                if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    requestLocationPermissions();
                } else {
                    locationsPresenter.onRequestCurrentLocation(false);
                }
            }

            @Override
            public void onAroundMeClicked() {
                aroundMeClicked = true;
                if (!PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    requestLocationPermissions();
                } else {
                    locationsPresenter.onRequestCurrentLocation(true);
                }
            }

            @Override
            public void onRemoveSearchLocationsHistory() {
                searchLocationsAdapter.onSetProgressForSearchHistory();
                locationsPresenter.removeSearchLocationsHistory();
            }

            @Override
            public boolean hasLocationPermissions() {
                return PermissionUtils.isProviderEnabled(getContext()) && PermissionUtils.hasPermissionsForRequestingLocation(getContext());
            }
        });

        rvAutocomplete.setHasFixedSize(true);
        rvAutocomplete.setAdapter(searchPhrasesAdapter);
        rvAutocomplete.setLayoutManager(new LinearLayoutManager(getContext()));

        // Default location
        aroundMeClicked = true;
        if (PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
            locationsPresenter.onRequestCurrentLocation(true);
        }

        iBtnDismiss.setOnClickListener(v -> getActivity().onBackPressed());
        iBtnClear.setOnClickListener(v -> {
            etSearch.setText("");
            if (etSearch.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        iBtnLocationClear.setOnClickListener(v -> {
            locationSetText("");
            resetStatusLocation();
            if (etLocation.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final boolean b = s.length() == 0;
                iBtnClear.setVisibility(b ? View.GONE : View.VISIBLE);
                phrasesPresenter.onSearchFieldTextChanged(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etSearch.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                phrasesPresenter.onSearchFieldFocus(etSearch.getText().toString());
            }
        });

        llSearch.setOnClickListener(v -> onSearchButtonClicked());

        etLocation.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                locationsPresenter.onLocationFieldFocus(etLocation.getText().toString());
            }
        });

        etLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                iBtnLocationClear.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
                if (ivLocationImage.isSelected()) {
                    ivLocationImage.setSelected(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                if(TextUtils.equals(textFromAutoComplete, s)){
//                    locationsPresenter.showLocationDefaultState();
//                }else {
//                    textFromAutoComplete = null;
//                }

                if (!selectedWhereAutoComplete && !willBeAroundMeState) {
                    locationsPresenter.onLocationFieldTextChanged(s.toString());
                }

                selectedWhereAutoComplete = false;
                if (aroundMeState && !s.toString().equals(getString(R.string.search_around_me))) {
                    aroundMeState = false;
                    willBeAroundMeState = false;
                    rvAutocomplete.setAdapter(searchLocationsAdapter);
                    s.clear();
                }
            }
        });

        TextView.OnEditorActionListener searchActionListener = (v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (isSearchEnabled()) {
                    onSearchButtonClicked();
                }
                return true;
            }
            return false;
        };

        etSearch.setOnEditorActionListener(searchActionListener);
        etLocation.setOnEditorActionListener(searchActionListener);

        final SearchArgument searchArgument = locationsPresenter.getSearchArgument();
        if (searchArgument != null) {
            etSearch.setText(searchArgument.getWhatField());
            locationSetText(searchArgument.getWhereField());
        }
    }

    public void onSearchButtonClicked() {//1=2
        count = 0;
        searchButtonAction();
    }

    private void searchButtonAction() {
        if (Utils.isNetworkAvailable(getContext()) && count < 6) {
            count++;
            String location_text = etLocation.getText().toString();

            // IF location empty alors selectionné autour de moi
            if (location_text.isEmpty()) {
                aroundMeClicked = true;
                if (PermissionUtils.hasPermissionsForRequestingLocation(getContext())) {
                    locationsPresenter.onRequestCurrentLocation(true);
                }
                location_text = etLocation.getText().toString();
            }

            if (locationsPresenter.isLocationUpdates(location_text)) {
                locationsPresenter.onSearchClicked(location_text);
                // we correct in this place because it spoil in locationsPresenter.onSearchClicked(location_text);
                aroundMeState = location_text != null && InternalSearchTrackerHelper.AUTOUR_DE_MOI.equals(location_text);
//                final String whereField = location_text != null && location_text.equals(getString(R.string.search_around_me)) ?
//                        location_text : etLocation.getText().toString();
                final String whereField = etLocation.getText().toString();
                final String whatField = etSearch.getText().toString();
                if (aroundMeState) {
                    searchPresenter.searchAroundMe(new SearchArgument(whatField, locationsPresenter.getLocation()));
                } else {
                    Log.d("onSearchClicked", "loc:" + whereField);
                    SearchArgument searchArgs = new SearchArgument(whatField, whereField);
                    searchArgs.setStatusLocation(this.statusLocation);
                    searchPresenter.search(searchArgs);
                }
            } else {
                Log.d("onSearchClicked", "delay");
                etLocation.postDelayed(() -> searchButtonAction(), 500);
            }
        } else {
            ((BaseActivity) getActivity()).onNetworkUnavailable();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationsPresenter.onRequestCurrentLocation(aroundMeClicked);
            } else {
                if (PermissionUtils.isNotAskAgainLocationPermission(getActivity())) {
                    showLocationPermissionNotAskAgain();
                } else {
                    showLocationPermissionDenied();
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void requestLocationPermissions() {
        PermissionUtils.requestLocationPermissions(this, REQUEST_LOCATION_PERMISSION);
    }

    private void setSearchLocationsAdapterIfNeeded() {
        rvAutocomplete.setVisibility(View.VISIBLE);
        llProvideLocation.setVisibility(View.GONE);
        RecyclerView.Adapter adapter = rvAutocomplete.getAdapter();
        if (!(adapter instanceof SearchLocationsAdapter)) {
            rvAutocomplete.setAdapter(searchLocationsAdapter);
        }
    }

    private void setSearchPhrasesAdapterIfNeeded() {
        RecyclerView.Adapter adapter = rvAutocomplete.getAdapter();
        if (!(adapter instanceof SearchPhrasesAdapter)) {
            rvAutocomplete.setAdapter(searchPhrasesAdapter);
        }
    }

    @Override
    public void showPhrasesDefaultState(List<SearchItemViewModel> recentSearchItems, PopularResponse popularResponse) {
        setSearchPhrasesAdapterIfNeeded();
        searchPhrasesAdapter.showDefaultState(recentSearchItems, popularResponse.items);
    }

    @Override
    public void showPhrasesDefaultState(List<SearchItemViewModel> recentSearchItems) {
        searchPhrasesAdapter.showDefaultState(recentSearchItems);
    }

    @Override
    public void showPhrasesSearchState(GetAutoCompleteSearchPhrasesResponse response) {
        setSearchPhrasesAdapterIfNeeded();
        searchPhrasesAdapter.showSearchState(response);
    }

    @Override
    public void onRecentSearchItemRemoved(SearchItemViewModel searchItem) {
        searchPhrasesAdapter.onRemovedSearchEntry(searchItem);
    }

    public void disableSearchButton() {
        llSearch.setVisibility(View.GONE);
    }

    private boolean isSearchEnabled() {
        return llSearch.getVisibility() == View.VISIBLE;
    }

    public void enableSearchButton() {
        llSearch.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSearchPhrasesHistoryRemoved() {
        searchPhrasesAdapter.onRemovedSearchHistory();
    }

    @Override
    public void onSearchLocationsHistoryRemoved() {
        searchLocationsAdapter.onRemovedSearchHistory();
    }

    @Override
    public void showPhrasesAutocomplete() {
        rvAutocomplete.setVisibility(View.VISIBLE);
        llProvideLocation.setVisibility(View.GONE);
    }

    @Override
    public void showLocationProvideInvitation() {
        etLocation.requestFocus();
        // Inutile
        // rvAutocomplete.setVisibility(View.GONE);
        // llProvideLocation.setVisibility(View.VISIBLE);

    }

    @Override
    public void onSelectSearchPhrasesEntry(String searchText) {
        if (searchText == null) {
            searchText = "";
        }
        etSearch.setText(searchText);
        etSearch.setSelection(searchText.length());
    }

    @Override
    public void showLocationsDefaultState(List<RecentLocationViewModel> recentLocations) {
        setSearchLocationsAdapterIfNeeded();
        searchLocationsAdapter.showDefaultState(recentLocations);
    }

    @Override
    public void showLocationsSearchState(GetAutoCompleteSearchLocationsResponse autocompleteItems, ConfigurationResponse config) {
        setSearchLocationsAdapterIfNeeded();
        searchLocationsAdapter.showSearchState(autocompleteItems);
        if (statusLocation.isActive() && !autocompleteItems.isActive()) {
            DialogUtils.showBadRegionDialog(getContext(), config, null);
        }
    }

    @Override
    public void onRecentSearchLocationRemoved(RecentLocationViewModel searchItem) {
        searchLocationsAdapter.onRemovedSearchEntry(searchItem);
    }

    @Override
    public void onSelectSearchLocationEntry(GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation, ConfigurationResponse config, boolean bAutoSelect) {
        this.statusLocation = statusLocation;
        updateStatusLocation();
        final String locations = statusLocation.getLocation();
        locationSetText(locations);
        if (!statusLocation.isEmpty()) {
            etLocation.setSelection(locations.length());
        }
        if (!bAutoSelect && !statusLocation.isActive()) {
            DialogUtils.showBadRegionDialog(getContext(), config, null);
        }
    }

    @Override
    public void setCurrentLocation(Location location, GetAutoCompleteSearchLocationsResponse.StatusLocation statusLocation) {
        willBeAroundMeState = true;
        locationSetText(getContext().getString(R.string.search_around_me));
        etLocation.setSelection(etLocation.getText().length());
        ivLocationImage.setSelected(true);
        aroundMeState = true;
        disableSearchSuggestionAdapter();
    }

    @Override
    public void showEnableLocationDialog(Status status) {
        listener.onPromptLocation(status);
    }

    @Override
    public void updateLocation(Location location) {

    }

    public void onPromptSettingsResult(boolean success) {
        if (success) {
            locationsPresenter.onPromptSettingSuccess(aroundMeClicked);
        } else {
            showLocationNotEnabled();
        }
    }

    @Override
    public String getLocationText() {
        return etLocation.getText().toString();
    }

    @Override
    public void onAutoCompleteSearchLocationsIsEmpty() {//AAABBB
        etLocation.setText(null);
    }

    @Override
    public boolean isLocationFieldHasFocus() {
        return etLocation.hasFocus();
    }

    @Override
    public boolean isPhrasesFieldHasFocus() {
        return etSearch.hasFocus();
    }

    @Override
    public void onEnableLocationButtonStateChanged() {
        searchLocationsAdapter.onEnableLocationButtonStateChanged();
    }

    @Override
    public void openSearchResultsScreen(SearchArgument searchArgument) {
        final SearchArgument searchArgumentIn = locationsPresenter.getSearchArgument();
        if (searchArgumentIn != null && searchArgumentIn.isQuickcardMode()) {
            Intent intent = new Intent();
            searchArgument.setQuickcardMode();
            intent.putExtra(SearchArgument.BUNDLE_NAME, searchArgument);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        } else {
            Intent intent = SearchResultsActivity.newIntent(getContext(), searchArgument);
            startActivityForResult(intent, REQUEST_SEARCH_RESULTS);
        }
    }

    @Override
    public void openSearchResultsScreenAroundMe(SearchArgument searchArgument) {
        Intent intent = SearchResultsActivity.newIntent(getContext(), searchArgument);
        startActivityForResult(intent, REQUEST_SEARCH_RESULTS);
    }

    @Override
    public boolean updateStatusLocation() {
        if (SearchHelper.isSearchState(phrasesPresenter.getSearchText())) {
            if (statusLocation.isEmpty() || statusLocation.isActive()) {
                enableSearchButton();
                return true;
            } else {
                enableSearchButton();
                return true;
            }
        } else {
            disableSearchButton();
            return false;
        }
    }

    private void resetStatusLocation() {
        statusLocation = GetAutoCompleteSearchLocationsResponse.StatusLocation.createEmpty();
        updateStatusLocation();
    }

    private void locationSetText(String text) {
        etLocation.setText(text);
    }

    @Override
    public void disableSearchSuggestionAdapter() {
        rvAutocomplete.setAdapter(null);
    }

}
