package com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper;

import android.app.Activity;

import com.google.android.gms.location.LocationListener;

/**
 * Created by SUVOROV on 10/20/16.
 */

public interface IGeoHelper {
    void requestLocationUpdates();

    boolean isShowGeoRequest();

    boolean askPermission(Activity activity);

    void disconnect();

    boolean isFinished();

    void setFinished();

    boolean askGeo(Activity activity);

    void setOnLocationListener(LocationListener onLocationListener);
}
