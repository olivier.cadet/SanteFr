package com.adyax.srisgp.mvp.create_account.flow.your_information_2;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SUVOROV on 10/6/16.
 */

public class CityResult implements Parcelable{

    private long postalCode;
    private String title;

    public CityResult(String title, long postalCode) {
        this.title = title;
        this.postalCode = postalCode;
    }


    protected CityResult(Parcel in) {
        postalCode = in.readLong();
        title = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(postalCode);
        dest.writeString(title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CityResult> CREATOR = new Creator<CityResult>() {
        @Override
        public CityResult createFromParcel(Parcel in) {
            return new CityResult(in);
        }

        @Override
        public CityResult[] newArray(int size) {
            return new CityResult[size];
        }
    };

    public boolean isEmpty() {
        return title==null/*|| postalCode==null*/;
    }

    public String getTitle() {
        return title;
    }

    public Long getPostalCode() {
        return postalCode;
    }
}
