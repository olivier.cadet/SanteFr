package com.adyax.srisgp.mvp.around_me.filters;

import androidx.annotation.NonNull;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.mvp.around_me.ISearchRequestHelper;
import com.adyax.srisgp.mvp.around_me.SearchRequestHelper;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;
import com.adyax.srisgp.utils.FiltersHelper;

import javax.inject.Inject;

/**
 * Created by Kirill on 19.10.16.
 */

public class FiltersPresenter extends Presenter<FiltersPresenter.FiltersView> {

    private final ISearchRequestHelper searchRequestHelper;
    private FiltersHelper filtersHelper;
    private FiltersAdapter filtersAdapter;

    @Inject
    public FiltersPresenter(FiltersHelper filtersHelper) {
        this.filtersHelper = filtersHelper;
        searchRequestHelper = new SearchRequestHelper(filtersHelper);
        searchRequestHelper.save();
    }

    @Override
    public void attachView(@NonNull FiltersView view) {
        super.attachView(view);
        filtersAdapter = new FiltersAdapter(filtersHelper, view.getSearchType());
        filtersAdapter.setOnChangedListener((isChanged, launchSearch) -> {
            if (getView() != null)
                getView().onFiltersChanged(launchSearch);
        });
    }

    public FiltersAdapter getFiltersAdapter() {
        return filtersAdapter;
    }

    public void applyFilters() {
        new TrackerHelper().clickFilter(getView().getSearchType());
        filtersAdapter.applyFilters();
        if (getView() != null) {
            getView().search();
        }
    }

    public void resetFilters() {
        filtersAdapter.reset();
    }

    public boolean isFilterChanged() {
//        filtersAdapter.applyFilters();
        if (getView() != null) {
            return searchRequestHelper.isFilterChanged(filtersAdapter.getCurrentFilters(), getView().getSearchType());
        }
        return false;
    }

    public interface FiltersView extends IView {
        void search();

        void onFiltersChanged(boolean launchResearch);

        SearchType getSearchType();
    }
}
