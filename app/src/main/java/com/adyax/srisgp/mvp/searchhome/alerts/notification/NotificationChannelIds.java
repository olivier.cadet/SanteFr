package com.adyax.srisgp.mvp.searchhome.alerts.notification;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;

public final class NotificationChannelIds {
    public static final String NEWS = App.getAppContext().getString(R.string.news_channel_id);
    public static final String ALERT = App.getAppContext().getString(R.string.alert_channel_id);
}
