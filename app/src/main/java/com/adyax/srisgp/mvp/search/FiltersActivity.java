package com.adyax.srisgp.mvp.search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.mvp.around_me.filters.FiltersFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

/**
 * Created by Kirill on 25.10.16.
 */

public class FiltersActivity extends MaintenanceBaseActivity {

    private static final String SEARCH_TYPE_KEY = "search_type";

    public static Intent getIntentForStart(Context context, SearchType searchType) {
        Intent intent = new Intent(context, FiltersActivity.class);
        intent.putExtra(SEARCH_TYPE_KEY, searchType == null ? -1 : searchType.ordinal());
        return intent;
    }

    public static void startForResult(Activity activity, SearchType searchType, int requestCode) {
        activity.startActivityForResult(getIntentForStart(activity, searchType), requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        initActionBar();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, FiltersFragment.getInstance(getSearchType()))
                .commit();
    }

    private void initActionBar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private SearchType getSearchType() {
        int ordinal = getIntent().getIntExtra(SEARCH_TYPE_KEY, -1);
        return ordinal == -1 ? null : SearchType.values()[ordinal];
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
