package com.adyax.srisgp.mvp.create_account.flow.input_password_1;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.widget.Toast;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.GetSsoInfoCommand;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.GetSsoInfoResponse;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.authentication.login.LoginActivity;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class InputPassword_1_Presenter extends PresenterAppReceiver<InputPassword_1_Presenter.CreateRecipeView>
        implements FacebookCallback<LoginResult> {

    public interface CreateRecipeView extends IView {
        void onFail(ErrorResponse errorResponse);

        void onSuccess();

        FragmentActivity getActivity();

        void startActivityForResult(Intent intent, int requestCode);
    }

//    private static final int GET_SSO_INFO_REQUEST = 1;
    private static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    private static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;

    private FragmentFactory fragmentFactory;

    @Inject
    public InputPassword_1_Presenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
//        if (!BuildConfig.DEV_STATUS) {
            registerFacebookLoginButton();
//        }
    }

    public void moveToNext() {
        fragmentFactory.startYourInformation_2_Fragment(getContext());
    }

    public void sendFakeRegistration() {
        final IViewPager activity = (IViewPager) getContext();
        activity.getCreateAccountHelper().sendCreateAccountCommand(getContext(), getAppReceiver(), CreateAccountCommand.FAKE_REGISTER);
    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    public void onRegistrationRulesClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PROTECTION), -1);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.GET_SSO_INFO_REQUEST:
                GetSsoInfoResponse response = data.getParcelable(CommandExecutor.BUNDLE_DATA);
                registerGooglePlus(response.getId(), response.getEmail(), response.getToken(), response.getGender());
                break;

            case ExecutionService.CREATE_ACCOUNT_ACTION:
                if(data!=null&& data.getString(CreateAccountCommand.GENDER_TYPE)!=null){
                    final String gender = data.getString(CreateAccountCommand.GENDER_TYPE);
                    fragmentFactory.startYourInformation_2_Fragment(getContext(), gender, "");
                }else {
                    getView().onSuccess();
                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        try {
            if (errorMessage != null) {
                getView().onFail(ErrorResponse.create(errorMessage));
            }
        } catch (Exception e) {

        }
    }

    /**
     * onActivityResult
     */

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        try {
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }catch (IllegalStateException e){
            e.printStackTrace();
        }

        if (requestCode == REQUEST_CODE_PICK_ACCOUNT) {
            if (resultCode == RESULT_OK) {
                getGoogleToken(data);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getContext(), "Google error", Toast.LENGTH_SHORT).show();
            }
        }

        if ((requestCode == REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR) && resultCode == RESULT_OK) {
            getGoogleToken(data);
        }
    }

    /**
     * Facebook
     */

    private LoginButton facebookLoginButton;
    private CallbackManager callbackManager;

    private void registerFacebookLoginButton() {
        if (getView() != null) {
            if (facebookLoginButton == null) {
                facebookLoginButton = new LoginButton(getContext());
                // Verification de l'application requise pour récupérer la date d'anniversaire
                // facebookLoginButton.setPermissions(Arrays.asList("email", "user_birthday"));
                facebookLoginButton.setPermissions("email");
            }

            callbackManager = CallbackManager.Factory.create();
            facebookLoginButton.registerCallback(callbackManager, this);
        }
    }

    public void facebookClick() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            if (facebookLoginButton != null) {
                Timber.d("Facebook login clicked");
                facebookLoginButton.performClick();
            }
        } else {
            getProfileData(accessToken);
        }
    }

    private void getProfileData(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                (object, response) -> {
                    if (getView() == null)
                        return;

                    String id = object.optString("id");
                    String name = object.optString("name");
                    String email = object.optString("email");
                    String gender = object.optString("gender");
                    String birthday = object.optString("birthday");
                    Timber.d("*** Facebook login ***\n" +
                            "id = %s\n" +
                            "name = %s\n" +
                            "email = %s\n" +
                            "gender = %s\n" +
                            "birthday = %s\n", id, name, email, gender, birthday);

                    registerFacebook(accessToken, id, email, birthday);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * Facebook callback
     */

    @Override
    public void onSuccess(LoginResult loginResult) {
        Timber.d("Facebook login success");
        getProfileData(loginResult.getAccessToken());
    }

    @Override
    public void onCancel() {
        Timber.d("Facebook login canceled");
    }

    @Override
    public void onError(FacebookException error) {
        Timber.d("Facebook login error");
    }

    /**
     * Twitter
     */

    private TwitterLoginButton twitterLoginButton;

    public void registerTwitterLoginButton(TwitterLoginButton twitterLoginButton) {
        this.twitterLoginButton = twitterLoginButton;
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;

                TwitterCore.getInstance().getApiClient(session).getAccountService()
                        .verifyCredentials(true, true, false).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        User user = result.data;

                        String id = user.idStr;
                        String name = user.name;
                        String email = user.email;
                        String screenName = user.screenName;
                        Timber.d("*** Facebook login ***\n" +
                                "id = %s\n" +
                                "name = %s\n" +
                                "email = %s\n", id, name, email);

                        TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
                        twitterAuthClient.requestEmail(session, new Callback<String>() {
                            @Override
                            public void success(Result<String> result) {
                                String email = result.data;
                                Timber.d("email = %s", email);

                                registerTwitter(id, email, token, secret, screenName);
                            }

                            @Override
                            public void failure(TwitterException exception) {
                                Toast.makeText(getContext(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void failure(TwitterException exception) {

                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(getContext(), "Twitter error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void registerFacebook(AccessToken accessToken, String id, String email, String birthday) {
        final IViewPager activity = (IViewPager) getContext();
        Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
        if (emailResult == null) {
            activity.getCreateAccountHelper().setSsoRegister(Sso_type.sgp_sso_facebook,
                    id, email, accessToken.getToken());

            String year = "";
            try {
                Date date = new SimpleDateFormat("MM/dd/yyyy").parse(birthday);
                year = new SimpleDateFormat("yyyy").format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            sendFakeRegistration();
//                        fragmentFactory.startYourInformation_2_Fragment(getContext(), gender, year);
        } else {
            Toast.makeText(getContext(), emailResult, Toast.LENGTH_SHORT).show();
        }
    }

    private void registerTwitter(String id, String email, String token, String twitterTokenSecret, String screenName) {
        final IViewPager activity = (IViewPager) getContext();
        Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
        if (emailResult == null) {
            activity.getCreateAccountHelper().setTwitterSsoRegister(id, email, token, twitterTokenSecret, screenName);
//            fragmentFactory.startYourInformation_2_Fragment(getContext());
            sendFakeRegistration();
        }
    }

    /**
     * Google sign-in
     */

    public void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        getView().startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    private void sendGetSsoInfoCommand(String token, String email) {
        GetSsoInfoCommand command = new GetSsoInfoCommand(token, email);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), command, ExecutionService.GET_SSO_INFO_REQUEST);
    }

    private Observable<String> getGoogleTokenObservable(Context context, Account account) {
        Observable<String> observable = Observable.create(subscriber -> {
            final String USER_INFO_PROFILE_SCOPE = "https://www.googleapis.com/auth/userinfo.profile";
            final String GPLUS_SCOPE = "https://www.googleapis.com/auth/plus.login";
            final String mScopes
                    = "oauth2:" + USER_INFO_PROFILE_SCOPE;

            String token = null;
            try {
                token = GoogleAuthUtil.getToken(context, account, mScopes);
            } catch (UserRecoverableAuthException e) {
                e.printStackTrace();
                subscriber.onError(e);
            } catch (GoogleAuthException e) {
                e.printStackTrace();
                subscriber.onError(e);
            } catch (IOException e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
            subscriber.onNext(token);
        });
        return observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private void getGoogleToken(Intent data) {
        String email = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        String type = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
        Account account = new Account(email, type);
        getGoogleTokenObservable(getContext(), account).subscribe(
                token -> sendGetSsoInfoCommand(token, email),
                throwable -> {
                    if (throwable instanceof UserRecoverableAuthException) {
                        Intent intent = ((UserRecoverableAuthException) throwable).getIntent();
                        if (getView() != null)
                            getView().startActivityForResult(intent, REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    }else{
                        Toast.makeText(getContext(), throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void registerGooglePlus(String id, String email, String token, String gender) {
        final IViewPager activity = (IViewPager) getContext();
        Integer emailResult = activity.getCreateAccountHelper().setEmailValidate(email);
        if (emailResult == null) {
            activity.getCreateAccountHelper().setSsoRegister(Sso_type.sgp_sso_google, id, email, token);
            activity.getCreateAccountHelper().setGender(GenderType.valueOf(getContext(), gender));
            activity.getCreateAccountHelper().sendCreateAccountCommand(getContext(), getAppReceiver(), CreateAccountCommand.FAKE_REGISTER);
//            fragmentFactory.startYourInformation_2_Fragment(getContext(), gender, "");
        }
    }

    private void finishActivity() {
        ((Activity) getContext()).finish();
        ((Activity) getContext()).onBackPressed();
        ((Activity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    public void gotoLogin() {
        finishActivity();
        ((Activity) getContext()).startActivity(LoginActivity.newIntent(getContext()));
    }
}
