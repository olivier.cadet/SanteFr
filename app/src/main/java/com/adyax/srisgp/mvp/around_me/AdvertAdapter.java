package com.adyax.srisgp.mvp.around_me;

import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.databinding.LayoutLoadingItemBinding;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.mvp.common.FindCardHolder;
import com.adyax.srisgp.mvp.common.FindCardListener;
import com.adyax.srisgp.mvp.framework.BaseCursorRecyclerAdapter;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class AdvertAdapter extends BaseCursorRecyclerAdapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private boolean isLoading;

    //    private int visibleThreshold = 3;
//    private int lastVisibleItem, totalItemCount;
    private boolean bMoreLoadedEnabled;

    private FindCardListener cardListener;
    private IRepository repository;

    public AdvertAdapter(IRepository repository) {
        this.repository = repository;
    }

    public void setCardListener(FindCardListener cardListener) {
        this.cardListener = cardListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                return new FindCardHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.item_around_me, parent, false), true, repository);
            default:
                return new LoadingItemViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                        R.layout.layout_loading_item, parent, false));
        }
    }



    @Override
    public int getItemViewType(int position) {
        return position >= cursor.getCount() ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Cursor cursor, int position) {
        SearchItemHuge searchItemHuge = new SearchItemHuge(position);
        BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, searchItemHuge);
        if (holder instanceof FindCardHolder) {
            ((FindCardHolder) holder).bind(searchItemHuge, cardListener);
        } else if (holder instanceof LoadingItemViewHolder) {
            ((LoadingItemViewHolder) holder).init(searchItemHuge);
        }
    }

    @Nullable
    public SearchItemHuge getItem(int position) {
        Cursor cursor = getCursor();

        if (cursor == null || cursor.getCount() <= position)
            return null;

        cursor.moveToPosition(position);
        SearchItemHuge item = new SearchItemHuge(position);
        BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, item);
        return item;
    }

    public SearchItemHuge getPositionByGeo(double lon, double lat) {
        final Cursor cursor = getCursor();
        if (cursor != null) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToPosition(i);
                SearchItemHuge item = new SearchItemHuge(i);
                BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, item);
                if (item.lon == lon && item.lat == lat) {
//                    return i;
                    return item;
                }
            }
        }
//        return -1;
        return null;
    }

    @Override
    public int getItemCount() {
        if (cursor != null && !cursor.isClosed()) {
            return cursor.getCount() + (isVisibleProgress() ? 1 : 0);
        }
        return 0;
    }

    private boolean isVisibleProgress(){
//        return true;
////        return isLoading;
        return bMoreLoadedEnabled;
    }

    public void setLoadingProgress(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isMoreLoadedEnabled() {
        return bMoreLoadedEnabled && !isLoading;
    }

    public void setMoreLoadedStatus(boolean bMoreLoadedEnabled) {
        this.bMoreLoadedEnabled = bMoreLoadedEnabled;
    }

    public abstract class AbstractViewHolder extends RecyclerView.ViewHolder {

        public AbstractViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void init(final SearchItemHuge itemHuge);

    }

    public class LoadingItemViewHolder extends AbstractViewHolder {
        public SearchItemHuge searchItemHuge;
        private LayoutLoadingItemBinding binding;

        public LoadingItemViewHolder(LayoutLoadingItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void init(final SearchItemHuge itemHuge) {
            this.searchItemHuge = itemHuge;
//            binding.tvTitle.setText(itemHuge.getName());
//            binding.tvLocation.setText(itemHuge.getAddress());
//            binding.tvSubtitle.setText(itemHuge.getSubtitles());
//            // TODO has been commented out for debugging
//            binding.getRoot().setOnClickListener(view -> presenter.clickAdvert(itemHuge));
            binding.getRoot().setTag(itemHuge);
        }
    }

    //    public void setLoaded() {
//
//    }
//
//    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener, RecyclerView recyclerView) {
//        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                totalItemCount = linearLayoutManager.getItemCount();
//                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
//
//                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                    if (mOnLoadMoreListener != null) {
//                        mOnLoadMoreListener.onLoadMore();
//                    }
//                    isLoading = true;
//                }
//            }
//        });
//    }
}
