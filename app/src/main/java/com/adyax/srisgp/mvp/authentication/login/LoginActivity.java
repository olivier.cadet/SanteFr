package com.adyax.srisgp.mvp.authentication.login;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityCreateAccountBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.CreateAccountHelper;
import com.adyax.srisgp.mvp.create_account.IViewPager;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.GeoHelper;
import com.adyax.srisgp.presentation.AskGeoPlusAlertPopupActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class LoginActivity extends AskGeoPlusAlertPopupActivity implements IViewPager {

    public static final String CREATE_ACCOUNT_HELPER = "CreateAccountHelper";
    private ActivityCreateAccountBinding binding;
    private CreateAccountHelper createAccountHelper;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        createAccountHelper = new CreateAccountHelper(CreateAccountHelper.LOGIN_ACTION);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_account);
//        binding.createAccountViewPager.setAdapter(new CreateAccountPagerAdapter(getSupportFragmentManager()));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentFactory.startLoginFragment(this);
//        startFragment();
        startGetGeo();
    }

    @Override
    protected void startSwipe(boolean bAnimaton) {
        FragmentTransaction fragmentTransaction = ((FragmentActivity) this).getSupportFragmentManager().beginTransaction();
        if(bAnimaton) {
            fragmentTransaction
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        }
//        fragmentTransaction
//                .replace(R.id.fragment_container, FinishCreateAccountFragment.newInstance())
//                .commit();
        fragmentFactory.startLoginFragment(this);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(CREATE_ACCOUNT_HELPER, createAccountHelper);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        createAccountHelper = savedInstanceState.getParcelable(CREATE_ACCOUNT_HELPER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != GeoHelper.REQUEST_CHECK_SETTINGS) {
            Fragment fragment = fragmentFactory.getCurrentFragment(this);
            if (fragment != null)
                fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        final int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        if (backStackEntryCount == 2) {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void moveToNext() {
//        final int currentItem=binding.createAccountViewPager.getCurrentItem();
//        final int count=binding.createAccountViewPager.getAdapter().getCount();
//        if(currentItem<(count-1)){
//            binding.createAccountViewPager.setCurrentItem(currentItem+1);
//        }
    }

    @Override
    public CreateAccountHelper getCreateAccountHelper() {
        return createAccountHelper;
    }

}
