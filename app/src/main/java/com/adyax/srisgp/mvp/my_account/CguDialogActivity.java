package com.adyax.srisgp.mvp.my_account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CguDialogActivity extends AppCompatActivity {

    public static final int CGU_WEBVIEW_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cgu_dialog);

        // Bind events
        final Button btnAccept = (Button) findViewById(R.id.btnAcceptCgu);
        final Button btnDecline = (Button) findViewById(R.id.btnDeclineCgu);
        final TextView submitMessage = (TextView) findViewById(R.id.submit_message);

        CguDialogActivity.setSubmitSpan(submitMessage, true);


        btnAccept.setOnClickListener(view -> {
            sendMessage("ACCEPTED");
            finish();
        });
        btnDecline.setOnClickListener(view -> {
            sendMessage("DECLINED");
            finish();
        });

        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        mCguDialogActionReceiver,
                        new IntentFilter("CGU_BROADCAST_EVENT")
                );
    }

    private BroadcastReceiver mCguDialogActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String cguAction = intent.getStringExtra("CGU_ACTION");

            if (cguAction.equals("WEBVIEW_ACCEPTED")) {
                sendMessage("ACCEPTED");
                finish();
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCguDialogActionReceiver);
    }

    @Override
    public void onBackPressed() {
        // Ne rien faire pour empecher la fermeture de la dialog sans la réponse utilisateur
    }

    public static void setSubmitSpan(TextView submitMessage, boolean isUpdateColor) {
        String text = submitMessage.getText().toString();
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);
        ClickableSpan legalesClickableSpan = new ClickableSpan() {

            @Override
            public void onClick(View view) {
                startWebViewActivity(submitMessage.getContext(), new WebViewArg(UrlExtras.INFORMATION, WebPageType.CGU_PAGE), CGU_WEBVIEW_REQUEST);
            }

            @Override
            public void updateDrawState(final TextPaint textPaint) {
                if (isUpdateColor) {
                    textPaint.setColor(submitMessage.getContext().getResources().getColor(R.color.colorPrimary));
                } else {
                    textPaint.setUnderlineText(true);
                }
            }

        };

        String string = submitMessage.getContext().getString(R.string.login_span3);
        int start = text.indexOf(string);
        if (start > 0) {
            ssBuilder.setSpan(
                    legalesClickableSpan, // Span to add
                    start, // Start of the span (inclusive)
                    start + String.valueOf(string).length(), // End of the span (exclusive)
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extend the span when text add later
            );
        }
        submitMessage.setText(ssBuilder);
        submitMessage.setMovementMethod(LinkMovementMethod.getInstance());
        submitMessage.setHighlightColor(Color.TRANSPARENT);
    }

    private static void startWebViewActivity(Context context, WebViewArg webViewArg, int requestCode) {
        if (context instanceof FragmentActivity) {
            Intent intent = WebViewActivity.newIntent(context, webViewArg);
            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else
            throw new IllegalArgumentException("");
    }

    /*
     * Broadcast event to "CGU_BROADCAST_EVENT"
     * @see CguHandler.java for usage
     */
    public void sendMessage(String actionString) {
        Intent intent = new Intent("CGU_BROADCAST_EVENT");
        intent.putExtra("CGU_ACTION", actionString);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == CguDialogActivity.RESULT_CANCELED) {
            // code to handle cancelled state
        }
        else if (requestCode == CGU_WEBVIEW_REQUEST) {
            // code to handle data from CGU_WEBVIEW_REQUEST
        }
    }
}
