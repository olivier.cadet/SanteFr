package com.adyax.srisgp.mvp.create_account;

/**
 * Created by SUVOROV on 9/30/16.
 */

public interface IViewPager {
    void moveToNext();
    CreateAccountHelper getCreateAccountHelper();
}
