package com.adyax.srisgp.mvp.framework;

import android.app.Activity;

import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;

/**
 * Created by SUVOROV on 8/2/17.
 */

public abstract class PresenterGeo<V extends IView> extends PresenterAppReceiver<V> implements IGeoUpdate{

    private IGeoHelper geoHelper;

    public PresenterGeo(IGeoHelper geoHelper) {
        this.geoHelper = geoHelper;
    }

    @Override
    public void registerListeners() {
        geoHelper.setOnLocationListener(location -> {
            update(location);
            geoHelper.disconnect();
        });
        startAskGeo();
    }

    @Override
    public void unregisterListeners() {
        geoHelper.disconnect();
    }

    public void startAskGeo() {
        if(!geoHelper.askGeo((Activity) getView().getContext())){
            update(null);
        }
    }

}
