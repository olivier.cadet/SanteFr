package com.adyax.srisgp.mvp.history;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anton.kobylianskiy on 4/25/17.
 */

public class HistoryKey implements Parcelable {
    public long sid;
    public Long nid;

    public HistoryKey(long sid, Long nid) {
        this.sid = sid;
        this.nid = nid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoryKey key = (HistoryKey) o;

        if (sid != key.sid) return false;
        return nid != null ? nid.equals(key.nid) : key.nid == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (sid ^ (sid >>> 32));
        result = 31 * result + (nid != null ? nid.hashCode() : 0);
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.sid);
        dest.writeValue(this.nid);
    }

    protected HistoryKey(Parcel in) {
        this.sid = in.readLong();
        this.nid = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<HistoryKey> CREATOR = new Parcelable.Creator<HistoryKey>() {
        @Override
        public HistoryKey createFromParcel(Parcel source) {
            return new HistoryKey(source);
        }

        @Override
        public HistoryKey[] newArray(int size) {
            return new HistoryKey[size];
        }
    };
}
