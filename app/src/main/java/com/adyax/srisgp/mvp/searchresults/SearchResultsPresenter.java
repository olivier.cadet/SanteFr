package com.adyax.srisgp.mvp.searchresults;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import android.text.TextUtils;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.FeedbackFormValue;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.SubmitFeedbackFormCommand;
import com.adyax.srisgp.data.net.command.builders.SearchRequestBuilder;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.QuestionnaireItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.ContentResolverHelper;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.around_me.AdvertCursorLoader;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;
import com.adyax.srisgp.utils.FiltersHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 10/11/16.
 */

public class SearchResultsPresenter extends PresenterAppReceiver<SearchResultsPresenter.GetInformedResultsView>
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private final static int RESULTS_LOADER_ID = 12210;
    private final static int RELATED_LOADER_ID = 12211;
    private static final int MESSAGES_LOADER_ID = 122112;

    private AppReceiver appReceiver = new AppReceiver();
    private double latitude;
    private double longitude;
    private boolean aroundMe;
    private String criteria;
    private String location;
    private String order;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }


    private SearchType searchType;
    private int currentPage = 0;
    private String stateToken;

    private final FragmentFactory fragmentFactory;

    private final IDataBaseHelper dataBaseHelper;
    private Loader.ForceLoadContentObserver forceLoadContentObserver;
    private SearchCommand searchCommand;
    private FiltersHelper filtersHelper;
    private INetWorkState netWorkState;
    private boolean hasMoreItems = true;
    private boolean sortedByDistance = true;
    private String suggestedType;

    private RelatedLoader relatedLoader;
    private SearchMessageLoader messageLoader;
    //    private ContentObserver relatedContentObserver = new ContentObserver(new Handler()) {
//        @Override
//        public void onChange(boolean selfChange) {
//            if (relatedLoader != null) {
//                relatedLoader.onContentChanged();
//            }
//        }
//    };
    private LoaderManager.LoaderCallbacks<List<Related>> relatedLoaderCallbacks = new LoaderManager.LoaderCallbacks<List<Related>>() {
        @Override
        public Loader<List<Related>> onCreateLoader(int id, Bundle args) {
            relatedLoader = new RelatedLoader(getContext(), dataBaseHelper, searchType);
            return relatedLoader;
        }

        @Override
        public void onLoadFinished(Loader<List<Related>> loader, List<Related> data) {
            if (getView() != null) {
                getView().showRelated(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<List<Related>> loader) {
            if (getView() != null) {
                getView().showRelated(Collections.emptyList());
            }
        }
    };

    private LoaderManager.LoaderCallbacks<List<SearchMessage>> messagesLoaderCallbacks = new LoaderManager.LoaderCallbacks<List<SearchMessage>>() {
        @Override
        public Loader<List<SearchMessage>> onCreateLoader(int id, Bundle args) {
            messageLoader = new SearchMessageLoader(getContext(), dataBaseHelper, searchType);
            return messageLoader;
        }

        @Override
        public void onLoadFinished(Loader<List<SearchMessage>> loader, List<SearchMessage> data) {
            if (getView() != null) {
                getView().showMessages(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<List<SearchMessage>> loader) {
            if (getView() != null) {
                getView().showMessages(Collections.emptyList());
            }
        }
    };

    private Loader.ForceLoadContentObserver forceRelatedLoadContentObserver;
    private ITracker tracker;
    private IRepository repository;

    @Inject
    public SearchResultsPresenter(FragmentFactory fragmentFactory, IDataBaseHelper dataBaseHelper, FiltersHelper filtersHelper,
                                  INetWorkState networkState, ITracker tracker, IRepository repository) {
        this.fragmentFactory = fragmentFactory;
        this.dataBaseHelper = dataBaseHelper;
        this.filtersHelper = filtersHelper;
        this.netWorkState = networkState;
        this.tracker = tracker;
        this.repository = repository;
    }

    @Override
    public void attachView(@NonNull GetInformedResultsView view) {
        super.attachView(view);
        appReceiver.setListener(this);
    }

    public void setHasMoreItems(boolean hasMoreItems) {
        this.hasMoreItems = hasMoreItems;
    }

    public void setSortedByDistance(boolean sortedByDistance) {
        this.sortedByDistance = sortedByDistance;
    }

    public boolean hasFilters() {
        if (searchCommand != null) {
            ArrayList<SearchFilter> filters = searchCommand.getFilters();
            if (filters != null && !filters.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public boolean isEmptyFilters() {
        return filtersHelper.getFilters(searchType).isEmpty();
    }

    public void setSearchCommand(SearchCommand searchCommand) {
        this.searchCommand = searchCommand;
    }

    public void setData(String criteria, String location, SearchType searchType) {
        this.criteria = criteria;
        this.location = location;
        this.searchType = searchType;
        this.aroundMe = false;
    }

    public void setData(String criteria, String location, SearchType searchType, String orderType) {
        this.criteria = criteria;
        this.location = location;
        this.searchType = searchType;
        this.aroundMe = false;
        this.order = orderType;
    }

    public void setData(String criteria, double latitude, double longitude, SearchType searchType) {
        this.criteria = criteria;
        this.latitude = latitude;
        this.longitude = longitude;
        this.searchType = searchType;
        this.aroundMe = true;
    }

    public void setData(String criteria, double latitude, double longitude, SearchType searchType, String orderType) {
        this.criteria = criteria;
        this.latitude = latitude;
        this.longitude = longitude;
        this.searchType = searchType;
        this.aroundMe = true;
        this.order = orderType;
    }

    public boolean isAroundMe() {
        return aroundMe;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public void detachView() {
        appReceiver.setListener(null);
        if (forceLoadContentObserver != null) {
            getContext().getContentResolver().unregisterContentObserver(forceLoadContentObserver);
        }
//        getContext().getContentResolver().unregisterContentObserver(relatedContentObserver);
        if (forceRelatedLoadContentObserver != null) {
            getContext().getContentResolver().unregisterContentObserver(forceRelatedLoadContentObserver);
        }
        super.detachView();
    }

    public boolean isLocationProvided() {
        return aroundMe || !TextUtils.isEmpty(location);
    }

    public void clearPagination() {
        currentPage = 0;
        stateToken = null;
        searchCommand = null;
        onChangeSearchCommand();
    }

    public int getCurrentPage() {
        return currentPage - 1;
    }

    private void onChangeSearchCommand() {
        if (getView() != null) {
            getView().onChangeSearchCommand(searchCommand);
        }
    }

    public void clearPagination(SearchFilter searchFilter) {
        filtersHelper.clearSearchFilters(searchType);
        filtersHelper.addFilter(searchFilter, searchType);
        currentPage = 0;
        stateToken = null;
        searchCommand = null;
        onChangeSearchCommand();
    }

    public void search(SearchCommand argSearchCommand) {
        getView().showLoading();

        if (searchCommand == null) {
            SearchRequestBuilder searchRequestBuilder;
            if (aroundMe) {
                searchRequestBuilder = new SearchRequestBuilder(searchType)
                        .setPage(currentPage++)
                        .setText(criteria)
                        .setLocation(getString(R.string.search_around_me))
                        .setGeo(latitude, longitude)
                        .setOrder(order);
            } else {
                searchRequestBuilder = new SearchRequestBuilder(searchType)
                        .setPage(currentPage++)
                        .setText(criteria)
                        .setLocation(location)
                        .setOrder(order);
            }

            if (stateToken != null && currentPage > 1) {
                searchRequestBuilder.setStateToken(stateToken);
            }

            searchRequestBuilder.setSuggestedType(true);
            searchRequestBuilder.addExtraMessages(true);
            searchCommand = searchRequestBuilder.build();
            onChangeSearchCommand();
        } else {
            searchCommand.setPage(currentPage++);

            if (currentPage > 1) {
                searchCommand.setStateToken(stateToken);
            }
        }
        if(argSearchCommand!=null) {
            searchCommand.setBbox(argSearchCommand.getBbox());
        }
        repository.saveLastSearchRequest(searchCommand);
        ExecutionService.sendCommand(getContext(), appReceiver, searchCommand, ExecutionService.SEARCH_ACTION);
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        getView().hideLoading();
    }

    public void updateFavoriteStatus(long id, boolean favorite) {
        dataBaseHelper.updateFavoriteAdvertCard(id, favorite ? 1 : 0);
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.FAVORITE_ACTION:
                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                boolean isFavorite = command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD);
                for (long id : command.getNids()) {
                    dataBaseHelper.updateFavoriteAdvertCard(id, isFavorite ? 1 : 0);
                }
                break;
            case ExecutionService.SEARCH_ACTION:
                final SearchResponseHuge response = data.getParcelable(SearchResponseHuge.BUNDLE_SEARCH);

                if (response.isActive()) {
                    getView().updateInfo(response);
                }
                stateToken = response.stateToken;
                hasMoreItems = response.hasMore();
                if (getView() != null) {
                    // Set BMC
                    getView().updateBmcView(response.bmc);
                    getView().updateQuestionnaireView(response.questionnaire);

                    if (response.items == null || response.items.isEmpty()) {
                        getView().onNoResultsFromServer();//1 1+ 1+++
                        if (searchType == SearchType.find) {
                            getContext().getContentResolver().notifyChange(TablesContentProvider.ADVERT_CONTENT_URI, null);
                        }
                    }// 1++

                    if (currentPage == 1 && (response.items == null || response.items.isEmpty())) {
                        getView().onFirstPageLoaded(searchType, true);//2 2+ 2+++
                    } else {
                        getView().onFirstPageLoaded(searchType, false);// 2++
                    }

                    sortedByDistance = response.distance_sort == 1;
                    suggestedType = response.suggestedType;
                    getView().onSetSortedByDistance(sortedByDistance);

                    if (searchType == SearchType.info) {
                        getView().showGetInformedCount(response.total);//3 3++
                    } else if (searchType == SearchType.find) {
                        if (searchCommand != null) {
                            searchCommand.update(response);//3+
                        }
                        getView().showFindCount(response.total);
                    }

                    getView().hideLoading();
                }
                break;
            case ExecutionService.GET_FORM:
                FeedbackFormResponse formResponse = data.getParcelable(CommandExecutor.BUNDLE_GET_FORM);
                submitForm(formResponse.secret, data.getLong(CommandExecutor.START_TIME));
                break;
            case ExecutionService.SUBMIT_FORM:
                getView().showExitDialog();
                getView().hideLoading();
                break;
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {
    }

    public void clickFavorite(SearchItemHuge item) {//1
        if (getView() != null) {
            if (netWorkState.isLoggedIn()) {
                ExecutionService.sendCommand(getContext(), getAppReceiver(),
                        new AddOrRemoveFavoriteCommand(TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION, item.getId(), item.isFavorite() ? AddOrRemoveFavoriteCommand.ACTION_REMOVE :
                                AddOrRemoveFavoriteCommand.ACTION_ADD,
                                item), ExecutionService.FAVORITE_ACTION);
            } else {
                getView().showRegistrationScreen();
            }
        }
    }

//    public void addInHistory(SearchItemHuge item) {
//        if (getView() != null) {
//            if (netWorkState.isLoggedIn()) {
//                ExecutionService.sendCommand(getContext(), getAppReceiver(),
//                        new AddInHistoryCommand(item.getId()), ExecutionService.ADD_IN_HISTORY);
//            }
//        }
//    }

    public void markMessageAsRead(SearchMessage searchMessage) {
        dataBaseHelper.markSearchMessageAdRead(searchMessage.getKey());
    }

    public void setStateToken(String stateToken) {
        this.stateToken = stateToken;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (getView() != null)
            return new AdvertCursorLoader(getContext(), dataBaseHelper, searchType);
        else
            return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (getView() != null) {
            if (cursor.getCount() == 0) {
                getView().showEmptyView();
                getView().updateScreenViewResearch(suggestedType);
            } else {
                getView().addResults(cursor, suggestedType, sortedByDistance, hasMoreItems);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
//        if (getView() != null) {
//            getView().addResults(null, suggestedType, sortedByDistance, false);
//        }
    }

    public void loadAroundMeItems() {
        if (getView() != null) {
            if (getView().getLoaderManager().getLoader(RESULTS_LOADER_ID) == null) {
                getView().getLoaderManager().initLoader(RESULTS_LOADER_ID, null, this);
                forceLoadContentObserver = getView().getLoaderManager().getLoader(RESULTS_LOADER_ID).new ForceLoadContentObserver();
                getContext().getContentResolver().registerContentObserver(TablesContentProvider.ADVERT_CONTENT_URI, true, forceLoadContentObserver);
            }

            if (getView().getLoaderManager().getLoader(RELATED_LOADER_ID) == null) {
                Loader<List<Related>> listLoader = getView().getLoaderManager().initLoader(RELATED_LOADER_ID, null, relatedLoaderCallbacks);
                forceRelatedLoadContentObserver = getView().getLoaderManager().getLoader(RELATED_LOADER_ID).new ForceLoadContentObserver();
                Uri tt = Uri.parse(String.format("%s#%s", TablesContentProvider.RELATED_VIRTUAL_CONTENT_URI.toString(), searchCommand.getType().name()));
                getContext().getContentResolver().registerContentObserver(tt/*TablesContentProvider.RELATED_VIRTUAL_CONTENT_URI*/, true, forceRelatedLoadContentObserver);
                listLoader.forceLoad();
            }

            if (getView().getLoaderManager().getLoader(MESSAGES_LOADER_ID) == null) {
                Loader<List<SearchMessage>> listLoader = getView().getLoaderManager().initLoader(MESSAGES_LOADER_ID, null, messagesLoaderCallbacks);
                forceLoadContentObserver = getView().getLoaderManager().getLoader(MESSAGES_LOADER_ID).new ForceLoadContentObserver();
                getContext().getContentResolver().registerContentObserver(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, true, forceLoadContentObserver);
                listLoader.forceLoad();
            }
            if (searchCommand != null) {
                final SearchCommand lastSearchRequest = repository.readLastSearchRequest(searchCommand.getType());
                final int compareResult = searchCommand.compare(lastSearchRequest);
//                searchCommand.setBbox(lastSearchRequest.getBbox());
                // to block same request after filter was changed
                if (compareResult == SearchCommand.NOT_EQUAL) {
                    clearPagination();
                    search(lastSearchRequest);
                }
            }
        }
    }

    public ITracker getTracker() {
        return tracker;
    }

    public IRepository getRepository() {
        return repository;
    }

    public interface GetInformedResultsView extends IView {
        void addResults(Cursor cursor, String suggestedType, boolean sortedByDistance, boolean hasMoreItems);

        void showEmptyView();

        void showFindCount(int count);

        void showGetInformedCount(int count);

        void showLoading();

        void hideLoading();

        void updateBmcView(String html);

        void updateQuestionnaireView(QuestionnaireItem item);

        boolean isProgress();

        LoaderManager getLoaderManager();

        void showExitDialog();

        void showRegistrationScreen();

        void showRelated(List<Related> relatedList);

        void onNoResultsFromServer();

        void onChangeSearchCommand(SearchCommand searchCommand);

        void onSetSortedByDistance(boolean sortedByDistance);

        void showMessages(List<SearchMessage> messages);

        void onFirstPageLoaded(SearchType searchType, boolean empty);

        void updateScreenViewResearch(String suggestedType);

        void updateInfo(SearchResponseHuge response);

    }

    private void getForm() {
        GetFormCommand getFormCommand = new GetFormCommand(BaseFeedbackCommandCommand.FEEDBACK_FORM_NAME, null);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), getFormCommand, ExecutionService.GET_FORM);
    }

    private void submitForm(String secret, long startTime) {
        boolean loggedIn = netWorkState.isLoggedIn();
        // /recherche/info?text=test&loc=(null)";
        String url = buildUrl();
        FeedbackFormValue formValue = new FeedbackFormValue(null, "yes", null, url, null);
        SubmitFeedbackFormCommand command = new SubmitFeedbackFormCommand(loggedIn, BaseFeedbackCommandCommand.FEEDBACK_FORM_NAME, secret, formValue, startTime);
        ExecutionService.sendCommand(getContext(), getAppReceiver(), command, ExecutionService.SUBMIT_FORM);
    }

    private String buildUrl() {
        String url = "recherche/" + (searchType == SearchType.info ? "info" : "find") + "?text=" + criteria;
        if (!TextUtils.isEmpty(location)) {
            url += "&loc=" + location;
        }
        return url;
    }

    public void clickYes() {
        if (!getView().isProgress()) {
            new TrackerHelper(getGetUrl()).clickYesNo(ClickType.clic_utile_oui);
            if (getView() != null) {
                getView().showLoading();
            }
            getForm();
        }
    }

    public void clickNo() {
        new TrackerHelper(getGetUrl()).clickYesNo(ClickType.clic_utile_non);
        fragmentFactory.startFeedbackActivity(getContext(), buildUrl(), BaseFeedbackCommandCommand.FEEDBACK_FORM_NAME);
    }

    @NonNull
    private IGetUrl getGetUrl() {
        return new IGetUrl() {

            @Override
            public int getPosition() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getCardLink() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getPostData() {
                return null;
            }

            @Override
            public long getNid() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getDescription() {
                throw new IllegalArgumentException();
            }

            @Override
            public NodeType getNodeType() {
                return searchType == SearchType.info ? ContentResolverHelper.INFO_NODE_TYPES[0] : ContentResolverHelper.FIND_NODE_TYPES[0];
            }

            @Override
            public String getTitle() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getSourceOrPropose() {
                throw new IllegalArgumentException();
            }

            @Override
            public Xiti getXiti() {
                return null;
            }

            @Override
            public IconType getIconType() {
                return IconType.UNKNOWN;
            }

            @Override
            public String getPhone() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getApple() {
                throw new IllegalArgumentException();
            }

            @Override
            public String getGoogle() {
                throw new IllegalArgumentException();
            }
        };
    }
}
