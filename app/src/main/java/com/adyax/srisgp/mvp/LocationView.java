package com.adyax.srisgp.mvp;

import android.location.Location;

import com.adyax.srisgp.mvp.framework.IView;
import com.google.android.gms.common.api.Status;

/**
 * Created by anton.kobylianskiy on 10/27/16.
 */

public interface LocationView extends IView {
    void onEnableLocationButtonStateChanged();
    void showEnableLocationDialog(Status status);
    void updateLocation(Location location);
}
