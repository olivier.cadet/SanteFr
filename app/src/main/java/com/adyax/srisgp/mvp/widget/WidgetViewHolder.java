package com.adyax.srisgp.mvp.widget;


import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.adyax.srisgp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

// Clone from HeaderWidgetViewHolder
public class WidgetViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.widgetLayout)
    View widgetLayout;
    @BindView(R.id.widgetTitle)
    TextView widgetTitle;
    @BindView(R.id.widgetDescription)
    TextView widgetDescription;
    @BindView(R.id.widgetBtnFemme)
    RadioButton widgetBtnFemme;
    @BindView(R.id.widgetBtnHomme)
    RadioButton widgetBtnHomme;
    @BindView(R.id.widgetAge)
    EditText widgetAge;
    @BindView(R.id.widgetCheckBox)
    CheckBox widgetCheckBox;
    @BindView(R.id.widgetButtonSubmit)
    TextView widgetButtonSubmit;
    @BindView(R.id.widgetErrText)
    TextView widgetErrText;

    // From global instances.
    View view;
    WidgetAdapter.OnItemClickedListener listener;

    // Widget view Contexts
    Boolean widgetVisibility;

    public WidgetViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.view = view;
        listener = new WidgetItemClickedListener(this.view);
    }

    public void bind() {
        // Get data from web service.
        // To cash widget data.
        WidgetViewHelper.init();

        if (widgetLayout.getVisibility() != View.VISIBLE) {
            widgetLayout.setVisibility(View.VISIBLE);
        }

        widgetBtnFemme.setChecked(false);
        widgetBtnFemme.setBackgroundResource(R.drawable.widget_radio_inactive);
        widgetBtnFemme.setTextColor(Color.parseColor("#ff0d47a1"));
        widgetBtnFemme.setOnClickListener(v -> {
                    widgetBtnFemme.setBackgroundResource(R.drawable.widget_radio_active);
                    listener.onWidgetInfoUpdated("sexe", "femme");
                    widgetBtnFemme.setTextColor(Color.WHITE);
                    widgetBtnHomme.setBackgroundResource(R.drawable.widget_radio_inactive);
                    widgetBtnHomme.setTextColor(Color.parseColor("#ff0d47a1"));
                }
        );
        widgetBtnHomme.setChecked(false);
        widgetBtnHomme.setBackgroundResource(R.drawable.widget_radio_inactive);
        widgetBtnHomme.setTextColor(Color.parseColor("#ff0d47a1"));
        widgetBtnHomme.setOnClickListener(v -> {
                    widgetBtnHomme.setBackgroundResource(R.drawable.widget_radio_active);
                    listener.onWidgetInfoUpdated("sexe", "homme");
                    widgetBtnHomme.setTextColor(Color.WHITE);
                    widgetBtnFemme.setBackgroundResource(R.drawable.widget_radio_inactive);
                    widgetBtnFemme.setTextColor(Color.parseColor("#ff0d47a1"));
                }
        );
        widgetAge.setText("");
        widgetAge.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.onWidgetInfoUpdated("age", charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        widgetCheckBox.setChecked(false);
        widgetCheckBox.setOnClickListener(v -> {
                    listener.onWidgetInfoUpdated("sous_cas", widgetCheckBox.isChecked() ? "1" : null);
                }
        );

        widgetButtonSubmit.setOnClickListener(v -> {
                    listener.onWidgetInfoUpdated(null, null);
                    listener.onWidgetSubmitted();
                }
        );
    }
}
