package com.adyax.srisgp.mvp.create_account;

import android.content.Context;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityCreateAccountBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.GeoHelper;
import com.adyax.srisgp.presentation.AskGeoPlusAlertPopupActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class CreateAccountActivity extends AskGeoPlusAlertPopupActivity implements IViewPager{

    public static final String CREATE_ACCOUNT_HELPER = "CreateAccountHelper";

    public static final String START_MODE = "start_mode";

    public static final int START_STANDART = 0;
    public static final int START_FACEBOOK = 1;
    public static final int START_TWITTER = 2;
    public static final int START_GOOGLE_PLUS = 3;

    private ActivityCreateAccountBinding binding;
    private CreateAccountHelper createAccountHelper;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, int startMode) {
        Intent intent = new Intent(context, CreateAccountActivity.class);
        intent.putExtra(START_MODE, startMode);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int startMode=getIntent().getIntExtra(START_MODE, START_STANDART);
        createAccountHelper = new CreateAccountHelper(CreateAccountHelper.CREATE_ACCOUNT_ACTION);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_account);
//        binding.createAccountViewPager.setAdapter(new CreateAccountPagerAdapter(getSupportFragmentManager()));
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            if(startMode!=START_STANDART){
                fragmentFactory.startYourInformation_2_Fragment(this);
            }else {
                fragmentFactory.startInputPassword_1_Fragment(this);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putParcelable(CREATE_ACCOUNT_HELPER, createAccountHelper);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        createAccountHelper = savedInstanceState.getParcelable(CREATE_ACCOUNT_HELPER);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void moveToNext() {
//        final int currentItem=binding.createAccountViewPager.getCurrentItem();
//        final int count=binding.createAccountViewPager.getAdapter().getCount();
//        if(currentItem<(count-1)){
//            binding.createAccountViewPager.setCurrentItem(currentItem+1);
//        }
    }

    @Override
    public CreateAccountHelper getCreateAccountHelper() {
        return createAccountHelper;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != GeoHelper.REQUEST_CHECK_SETTINGS) {
            Fragment fragment = fragmentFactory.getCurrentFragment(this);
            if (fragment != null)
                fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void startSwipe(boolean bAnimaton) {
        FragmentTransaction fragmentTransaction = ((FragmentActivity) this).getSupportFragmentManager().beginTransaction();
        if(bAnimaton) {
            fragmentTransaction
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        }
//        fragmentTransaction
//                .replace(R.id.fragment_container, FinishCreateAccountFragment.newInstance())
//                .commit();
        fragmentFactory.startInputPassword_1_Fragment(this);

    }
}
