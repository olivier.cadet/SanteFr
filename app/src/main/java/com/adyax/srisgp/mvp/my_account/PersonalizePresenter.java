package com.adyax.srisgp.mvp.my_account;

import android.os.Bundle;

import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.NotifySettingsBody;
import com.adyax.srisgp.data.net.command.PingCommandAuth;
import com.adyax.srisgp.data.net.command.TaxonomyVocabularyTermsCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalizeCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.data.net.response.tags.IntIdValuePair;
import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.NetworkPresenter;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

/**
 * Created by Kirill on 17.11.16.
 */

public class PersonalizePresenter extends NetworkPresenter<PersonalizePresenter.PersonalizeView> {

    public interface PersonalizeView extends IView {
        void setManualInterests(ArrayList<IntIdValuePair> items);

        void setInterests(ArrayList<IntIdValuePair> items);

        void setKeywords(ArrayList<TermsItem> items);

        void setNotificationReceive(User user);

        boolean isReceiveByMobileChecked();

//        boolean isReceiveByEmailChecked();

        boolean isReceiveByEmailDailyChecked();

        boolean isReceiveByEmailWeeklyChecked();
    }

//    private final static int GET_INTERESTS_REQUEST = 1;
//    private final static int GET_KEYWORDS_REQUEST = 2;
//    private final static int PING = 3;
//    private static final int UPDATE_PERSONALIZE_REQUEST = 4;

    private IRepository repository;
    private FragmentFactory fragmentFactory;
    private KeyWordsHelper keyWordsHelper;

    @Inject
    public PersonalizePresenter(IRepository repository, FragmentFactory fragmentFactory) {
        this.repository = repository;
        this.fragmentFactory = fragmentFactory;
        keyWordsHelper=new KeyWordsHelper(repository);
    }

    public void onViewCreated() {
        setManualInterests();
        setNotificationReceive();
        setInterests();
        setKeywords();

        sendTaxonomyVocabularyTerms();
        sendPing();
    }

    public void onPrivacyPolicyClicked() {
        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(UrlExtras.PRIVACY_POLICY), -1);
    }

    public void switchChecked(IntIdValuePair valuePair) {
        final String nameChangegeableInterest=valuePair.value;
        new TrackerHelper().clickRemoveNotification(nameChangegeableInterest, true);
        final ArrayList<String> interests = new ArrayList<>();
        final User user = repository.getCredential().user;
        if (user != null && user.getManualInterests() != null) {
            for (IntIdValuePair pair : user.getManualInterests()) {
                interests.add(pair.value);
            }

            interests.add(nameChangegeableInterest);

            final ArrayList<String> searchInterests = new ArrayList<>();
            if (user.getSearchInterests() != null) {
                for (IntIdValuePair pair : user.getSearchInterests()) {
                    searchInterests.add(pair.value);
                }
            }

            updatePersonalize(interests, searchInterests);
        }
    }

    public void switchUnchecked(IntIdValuePair valuePair) {
        final String interest=valuePair.value;
        new TrackerHelper().clickRemoveNotification(interest, false);
        ArrayList<String> interests = new ArrayList<>();
        User user = repository.getCredential().user;
        if (user != null && user.getManualInterests() != null) {
            for (IntIdValuePair pair : user.getManualInterests()) {
                if (!pair.value.equals(interest)) {
                    interests.add(pair.value);
                }
            }

            ArrayList<String> searchInterests = new ArrayList<>();
            if (user.getSearchInterests() != null) {
                for (IntIdValuePair pair : user.getSearchInterests()) {
                    searchInterests.add(pair.value);
                }
            }

            updatePersonalize(interests, searchInterests);
        }
    }

    public void updatePersonalize(ArrayList<String> interests, ArrayList<String> searchInterests) {
        NotifySettingsBody notifySettingsBody = getNotifySettings();
        if (notifySettingsBody != null) {
            sendUpdatePersonalize(interests, searchInterests, notifySettingsBody);
        }
    }

    public void updatePersonalize() {
        // TODO: check is it code correct
        User user = repository.getCredential().user;
        if (user != null && user.getManualInterests() != null) {
            ArrayList<String> interests = new ArrayList<>();
            for (IntIdValuePair pair : user.getManualInterests()) {
                interests.add(pair.value);
            }

            ArrayList<String> searchInterests = new ArrayList<>();
            if (user.getSearchInterests() != null) {
                for (IntIdValuePair pair : user.getSearchInterests()) {
                    searchInterests.add(pair.value);
                }
            }

            NotifySettingsBody notifySettingsBody = getNotifySettings();
            if (notifySettingsBody != null)
                sendUpdatePersonalize(interests, searchInterests, notifySettingsBody);
        }
    }

    private NotifySettingsBody getNotifySettings() {
        if (getView() == null)
            return null;

        return new NotifySettingsBody(
                getView().isReceiveByMobileChecked(),
//                getView().isReceiveByEmailChecked(),
                getView().isReceiveByEmailDailyChecked(),
                getView().isReceiveByEmailWeeklyChecked()
        );
    }

    private void sendGetInterests() {
        sendCommand(new TaxonomyVocabularyTermsCommand(TaxonomyVocabularyTermsCommand.KEYWORDS), ExecutionService.GET_INTERESTS_REQUEST);
    }

    public void sendTaxonomyVocabularyTerms() {
        sendCommand(new TaxonomyVocabularyTermsCommand(TaxonomyVocabularyTermsCommand.INTERESTS), ExecutionService.TAXONOMY_VOCABULARY_TERMS_ACTION);
    }

    private void sendPing() {
        sendCommand(new PingCommandAuth(), ExecutionService.PING_AUTH_ACTION);
    }

    private void sendUpdatePersonalize(ArrayList<String> interests, ArrayList<String> searchInterests, NotifySettingsBody notifySettingsBody) {
        sendCommand(new UpdatePersonalizeCommand(interests, searchInterests, notifySettingsBody), ExecutionService.UPDATE_PERSONALIZE_REQUEST);
    }

    private void setInterests() {
//        if (getView() != null && interests != null) {
//            ArrayList<TermsItem> termsItems = removeChecked(interests);
//            Collections.sort(termsItems, (first, second) -> first.name.toLowerCase().compareTo(second.name.toLowerCase()));
//            getView().setInterests(termsItems);
//        }
        User user = repository.getCredential().user;
        if (user != null && user.getSearchInterests() != null) {
            if (getView() != null) {

                ArrayList<IntIdValuePair> result = new ArrayList<>();

                for (IntIdValuePair pair : user.getSearchInterests()) {
                    boolean needToAdd = true;

                    if (user.getManualInterests() != null) {
                        for (IntIdValuePair manualItem : user.getManualInterests()) {// TODO nullPointerException
                            if (manualItem.id == pair.id) {
                                needToAdd = false;
                                break;
                            }
                        }
                    }

                    if (needToAdd) {
                        result.add(pair);
                    }
                }

                Collections.sort(result, (first, second) -> first.value.toLowerCase().compareTo(second.value.toLowerCase()));
                getView().setInterests(result);
            }
        }
    }

    private void setKeywords() {
        if (getView() != null && keyWordsHelper.isKeywords()) {
            getView().setKeywords(keyWordsHelper.removeChecked());
        }
    }

    private void setManualInterests() {
        User user = repository.getCredential().user;
        if (user != null){
            final ArrayList<IntIdValuePair> manualInterests = user.getManualInterests();
            if(manualInterests != null) {
                if (getView() != null) {
                    Collections.sort(manualInterests, (first, second) -> first.value.toLowerCase().compareTo(second.value.toLowerCase()));
                    getView().setManualInterests(manualInterests);
                }
            }
        }
    }

    private void setNotificationReceive() {
        User user = repository.getCredential().user;
        if (user != null) {
            if (getView() != null)
                getView().setNotificationReceive(user);
        }
    }

    /**
     * AppReceiver
     */

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        switch (requestCode) {
            case ExecutionService.TAXONOMY_VOCABULARY_TERMS_ACTION:
                keyWordsHelper.setUncheckLocalInterest(data.getParcelable(CommandExecutor.TAXONOMY_VOCABULARY_TERMS_RESPONSE));
                sendGetInterests();
                break;
            case ExecutionService.GET_INTERESTS_REQUEST:
                if (data.containsKey(CommandExecutor.TAXONOMY_VOCABULARY_TERMS_RESPONSE)) {
                    final TaxonomyVocabularyTermsResponse response = data.getParcelable(CommandExecutor.TAXONOMY_VOCABULARY_TERMS_RESPONSE);
                    if (response != null) {
                        keyWordsHelper.setKeywords(response.getList());
                        setKeywords();
                    }
                }
                break;
            case ExecutionService.UPDATE_PERSONALIZE_REQUEST:
//                keyWordsHelper.update();
                setKeywords();
            case ExecutionService.PING_AUTH_ACTION:
//                if(!BuildConfig.DEBUG) {
                setManualInterests();
                setNotificationReceive();
                setInterests();
                setKeywords();
//                }
                break;
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        super.onFail(requestCode, errorMessage);
    }

    public void setReceiveEmail(boolean isChecked) {
        new TrackerHelper().clickReceiveEmail(isChecked);
    }

}
