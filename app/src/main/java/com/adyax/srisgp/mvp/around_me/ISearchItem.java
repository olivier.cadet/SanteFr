package com.adyax.srisgp.mvp.around_me;

import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by SUVOROV on 8/29/17.
 */

public interface ISearchItem {
    String getSubtitle();
    String getTitle();
    IGetUrl getItem();
    LatLng getLatLng();

    String getAddress();

    boolean isGroup();
    String getGroupCount();
    boolean contains(long id);
    boolean isEquals(Marker marker);
    int getPosition();
    long getId();
}
