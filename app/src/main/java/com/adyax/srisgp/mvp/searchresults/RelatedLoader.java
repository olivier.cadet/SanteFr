package com.adyax.srisgp.mvp.searchresults;

import android.content.Context;
import android.database.Cursor;
import androidx.loader.content.AsyncTaskLoader;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.db.IDataBaseHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 11/17/16.
 */

public class RelatedLoader extends AsyncTaskLoader<List<Related>> {
    private IDataBaseHelper databaseHelper;
    private SearchType searchType;

    public RelatedLoader(Context context, IDataBaseHelper databaseHelper, SearchType searchType) {
        super(context);
        this.databaseHelper = databaseHelper;
        this.searchType = searchType;
    }

    @Override
    public List<Related> loadInBackground() {
        List<Related> related = new LinkedList<>();
        Cursor cursor = databaseHelper.getRelated(searchType);
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    Related relatedItem = new Related();
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, relatedItem);
                    related.add(relatedItem);
                }
            } finally {
                cursor.close();
            }
        }
        for (Related relatedItem : related) {
            cursor = databaseHelper.getRelatedItem(searchType);
            relatedItem.items = new LinkedList<>();
            if (cursor != null) {
                try {
                    while (cursor.moveToNext()) {
                        RelatedItem relatedItem2 = new RelatedItem();
                        BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, relatedItem2);
                        relatedItem.items.add(relatedItem2);
                    }
                } finally {
                    cursor.close();
                }
            }
        }
        return related;
    }
}
