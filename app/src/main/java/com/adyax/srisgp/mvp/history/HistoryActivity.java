package com.adyax.srisgp.mvp.history;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

/**
 * Created by anton.kobylianskiy on 2/20/17.
 */

public class HistoryActivity extends MaintenanceBaseActivity {
    private static final String EXTRA_HISTORY = "history";

    public static Intent newIntent(Context context, HistoryItem item) {
        Intent intent = new Intent(context, HistoryActivity.class);
        intent.putExtra(EXTRA_HISTORY, item);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            if (intent != null) {
                HistoryItem item = intent.getParcelableExtra(EXTRA_HISTORY);
                replaceFragment(R.id.frame, HistoryFragment.newInstance(item));
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}