package com.adyax.srisgp.mvp.maintenance;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentMaintenanceBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class MaintenanceFragment extends ViewFragment implements MaintenancePresenter.DummyView {

    public static final String TITLE = "title";

    @Inject
    MaintenancePresenter maintenancePresenter;
    private FragmentMaintenanceBinding binding;
    private int title;

    public MaintenanceFragment() {
    }

    public static Fragment newInstance(int title) {
        MaintenanceFragment fragment = new MaintenanceFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        title =getArguments().getInt(TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_maintenance, container, false);
//        binding.info.setText(title);
        return binding.getRoot();
    }

    @NonNull
    @Override
    public MaintenancePresenter getPresenter() {
        return maintenancePresenter;
    }
}
