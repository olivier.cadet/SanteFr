package com.adyax.srisgp.mvp.dummy;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.FragmentDummyBinding;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class DummyFragment extends ViewFragment implements DummyPresenter.DummyView {

    public static final String TITLE = "title";

    @Inject
    DummyPresenter dummyPresenter;
    private FragmentDummyBinding binding;
    private int title;

    public DummyFragment() {
    }

    public static Fragment newInstance(int title) {
        DummyFragment fragment = new DummyFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        title =getArguments().getInt(TITLE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dummy, container, false);
        binding.info.setText(title);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(title);
    }

    @NonNull
    @Override
    public DummyPresenter getPresenter() {
        return dummyPresenter;
    }
}
