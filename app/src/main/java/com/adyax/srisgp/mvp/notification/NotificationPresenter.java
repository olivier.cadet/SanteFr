package com.adyax.srisgp.mvp.notification;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.UpdateNotificationStatusCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.PresenterAppReceiver;

import javax.inject.Inject;

/**
 * Created by anton.kobylianskiy on 9/30/16.
 */

public class NotificationPresenter extends PresenterAppReceiver<NotificationPresenter.NotificationView>
        implements AppReceiver.AppReceiverCallback {

    private static final int COMMAND_READ_NOTIFICATION = 0;

    private FragmentFactory fragmentFactory;

    @Inject
    public NotificationPresenter(FragmentFactory fragmentFactory) {
        this.fragmentFactory = fragmentFactory;
    }

    @Override
    public void attachView(@NonNull NotificationView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    public void readNotification(NotificationItem item) {
        if (!item.isRead()) {
            UpdateNotificationStatusCommand command =
                    new UpdateNotificationStatusCommand(UpdateNotificationStatusCommand.ACTION_MARK_AS_READ, item.nid);
            ExecutionService.sendCommand(getContext(), getAppReceiver(), command, COMMAND_READ_NOTIFICATION);
        }
    }

    public void downloadClicked(NotificationItem item) {
        if (getView() != null) {
//            ViewUtils.showAppsDialog(getContext(), item.appStoreLink, item.playStoreLink);
            new TrackerHelper(item).showAppsDialog(getContext(), null);
        }
    }

    public void readClicked(NotificationItem item) {
//        fragmentFactory.startWebViewActivity(getContext(), new WebViewArg(item, WebPageType.SERVER, TrackerLevel.NOTIFICATIONS), -1);
        new TrackerHelper(item).startWebViewActivity(getContext(), TrackerLevel.NOTIFICATIONS);
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        if (requestCode == COMMAND_READ_NOTIFICATION) {

        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (requestCode == COMMAND_READ_NOTIFICATION) {
            if (getView() != null) {
                getView().onSuccessReadRequest();
            }
        } else if (requestCode == ExecutionService.FAVORITE_ACTION) {
            if (getView() != null) {
                AddOrRemoveFavoriteCommand command = data.getParcelable(CommandExecutor.BUNDLE_ADD_REMOVE_FAVORITE);
                if (command != null && command.getNids() != null) {
                    if (getView() != null) {
                        getView().updateFavoriteStatus(command.getAction().equals(AddOrRemoveFavoriteCommand.ACTION_ADD));
                    }
                }
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    public void onFavoriteClicked(NotificationItem item) {
        ExecutionService.sendCommand(getContext(), getAppReceiver(),
                new AddOrRemoveFavoriteCommand(TrackerLevel.NOTIFICATIONS, item.nid, !item.isFavorite() ? AddOrRemoveFavoriteCommand.ACTION_ADD :
                        AddOrRemoveFavoriteCommand.ACTION_REMOVE, item), ExecutionService.FAVORITE_ACTION);
    }

    public interface NotificationView extends IView {
        void onSuccessReadRequest();

        void updateFavoriteStatus(boolean favorite);
    }
}
