package com.adyax.srisgp.mvp.searchresults;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.QuestionnaireItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.framework.IPresenter;
import com.adyax.srisgp.mvp.framework.MultiplePresenterViewFragment;
import com.adyax.srisgp.mvp.searchresults.find.FindSearchResultsFragment;
import com.adyax.srisgp.mvp.searchresults.info.InfoSearchResultsFragment;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.mvp.web_views.WebViewFragment;
import com.adyax.srisgp.presentation.VerticalSpaceItemDecoration;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anton.kobylianskiy on 10/11/16.
 */

public abstract class SearchResultsFragment extends MultiplePresenterViewFragment implements SearchResultsPresenter.GetInformedResultsView {

    public static final int REQUEST_READ = 125;

    private static final String ARG_AROUND_ME = "around_me";
    private static final String ARG_LONGITUDE = "longitude";
    private static final String ARG_LATITUDE = "latitude";

    private static final String ARG_CRITERIA = "criteria";
    private static final String ARG_LOCATION = "location";
    private static final String ARG_SEARCH_TYPE = "search_type";

    @Inject
    protected SearchResultsPresenter resultsPresenter;

    @BindView(R.id.rvCards)
    RecyclerView rvCards;
    @BindView(R.id.emptyView)
    LinearLayout emptyView;

    private SearchResultsAdapter resultsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    protected SearchResultsListener listener;
    private ProgressDialog progressDialog;
    private SearchType searchType;
    private String criteria;
    private boolean isProgress;

    public static SearchResultsFragment newInstance(String criteria, String location, SearchType searchType) {
        Bundle args = new Bundle();
        args.putString(ARG_CRITERIA, criteria);
        args.putString(ARG_LOCATION, location);
        args.putSerializable(ARG_SEARCH_TYPE, searchType);
        args.putBoolean(ARG_AROUND_ME, false);
        SearchResultsFragment fragment = createFragmentBySearchType(searchType);
        fragment.setArguments(args);
        return fragment;
    }

    public static SearchResultsFragment newInstanceAroundMe(String criteria, double latitude, double longitude, SearchType searchType) {
        Bundle args = new Bundle();
        args.putString(ARG_CRITERIA, criteria);
        args.putDouble(ARG_LATITUDE, latitude);
        args.putDouble(ARG_LONGITUDE, longitude);
        args.putSerializable(ARG_SEARCH_TYPE, searchType);
        args.putBoolean(ARG_AROUND_ME, true);
        SearchResultsFragment fragment = createFragmentBySearchType(searchType);
        fragment.setArguments(args);
        return fragment;
    }

    public void onLocationUpdated(Location location) {
        resultsPresenter.clearPagination();
        resultsPresenter.setData(criteria, location.getLatitude(), location.getLongitude(), searchType, UrlExtras.RestUserSearch.SORT_RELEVANCE);
        resultsPresenter.search(null);
    }

    public void onRefreshData(SearchCommand argSearchCommand) {
        resultsPresenter.clearPagination();
        resultsPresenter.search(argSearchCommand);
    }

    public void onRefreshData(SearchFilter searchFilter) {
        resultsPresenter.clearPagination(searchFilter);
        resultsPresenter.search(null);
    }

    private static SearchResultsFragment createFragmentBySearchType(SearchType searchType) {
        SearchResultsFragment fragment;
        if (searchType == SearchType.find) {
            fragment = new FindSearchResultsFragment();
        } else if (searchType == SearchType.info) {
            fragment = new InfoSearchResultsFragment();
        } else {
            throw new RuntimeException("No fragment for such view type: " + searchType.name());
        }
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        resultsPresenter.loadAroundMeItems();
    }

    public boolean isFilterButtonShouldBeShown() {
        if (isEmptyFilters()) {
            return false;
        }

        if (isEmpty() && !hasFilters()) {
            return false;
        }

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_READ) {
            if (resultCode == Activity.RESULT_OK) {
                boolean favorite = data.getBooleanExtra(WebViewFragment.EXTRA_RESULT_FAVORITE, false);
                long id = data.getLongExtra(WebViewFragment.EXTRA_RESULT_ID, -1);
                if (id != -1) {
                    resultsPresenter.updateFavoriteStatus(id, favorite);
                }
            } else {

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onAttach(Context context) {
        progressDialog = new ProgressDialog(getContext());
        App.getApplicationComponent().inject(this);
        presenters.clear();
        IPresenter additionalPresenter = getAdditionalPresenter();

        presenters = new ArrayList<>(additionalPresenter == null ? 1 : 2);
        presenters.add(resultsPresenter);
        if (additionalPresenter != null) {
            presenters.add(additionalPresenter);
        }

        super.onAttach(context);
        listener = (SearchResultsListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        criteria = arguments.getString(ARG_CRITERIA);
        this.searchType = (SearchType) arguments.getSerializable(ARG_SEARCH_TYPE);
        boolean aroundMe = arguments.getBoolean(ARG_AROUND_ME);

        double latitude;
        double longitude;
        if (savedInstanceState != null && savedInstanceState.getBoolean(ARG_AROUND_ME, false)) {
            latitude = savedInstanceState.getDouble(ARG_LATITUDE);
            longitude = savedInstanceState.getDouble(ARG_LONGITUDE);
            resultsPresenter.setData(criteria, latitude, longitude, searchType, UrlExtras.RestUserSearch.SORT_RELEVANCE);
        } else {
            if (aroundMe) {
                latitude = arguments.getDouble(ARG_LATITUDE);
                longitude = arguments.getDouble(ARG_LONGITUDE);
                resultsPresenter.setData(criteria, latitude, longitude, searchType, UrlExtras.RestUserSearch.SORT_RELEVANCE);
            } else {
                final String location = arguments.getString(ARG_LOCATION);
                resultsPresenter.setData(criteria, location, searchType, UrlExtras.RestUserSearch.SORT_RELEVANCE);
            }
        }

        if (shouldLoadData()) {
            resultsPresenter.search(null);
        }
    }

    public void onClickOpenWebView(SearchItemHuge item) {
        if (item.getNodeType() == NodeType.MEDICAMENTS) {
            // send page=1&affliste=0&affNumero=0&isAlphabet=0&inClauseSubst=0&nomSubstances=&typeRecherche=0&choixRecherche=medicament&paginationUsed=0&txtCaracteres=doliprane&radLibelle=1&txtCaracteresSub=&radLibelleSub=4
            item.url = UrlExtras.ExternalSearch.MEDICS;
            item.urlPost = "1&affliste=0&affNumero=0&isAlphabet=0&inClauseSubst=0&nomSubstances=&typeRecherche=0&choixRecherche=medicament&paginationUsed=0&radLibelle=1&txtCaracteresSub=&radLibelleSub=4&txtCaracteres=";
            item.urlPost += Normalizer.normalize(item.getTitle(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
            Intent intent;
            intent = WebViewActivity.newIntent(getContext(), new WebViewArg(item, WebPageType.EXTERNAL_REQUIRING_POSTURL, TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION));
            startActivityForResult(intent, REQUEST_READ);
            ((FragmentActivity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else if (item.getNodeType() == NodeType.SUBSTANCES) {
            // send page=1&affliste=0&affNumero=0&isAlphabet=0&inClauseSubst=0&nomSubstances=&typeRecherche=0&choixRecherche=medicament&paginationUsed=0&txtCaracteres=doliprane&radLibelle=1&txtCaracteresSub=&radLibelleSub=4
            item.url = UrlExtras.ExternalSearch.MEDICS;
            item.urlPost = "page=1&affliste=0&affNumero=0&isAlphabet=0&inClauseSubst=0&nomSubstances=&typeRecherche=1&choixRecherche=substance&paginationUsed=0&txtCaracteres=&radLibelle=2&radLibelleSub=3&txtCaracteresSub=";
            item.urlPost += Normalizer.normalize(item.getTitle(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
            Intent intent;
            intent = WebViewActivity.newIntent(getContext(), new WebViewArg(item, WebPageType.EXTERNAL_REQUIRING_POSTURL, TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION));
            startActivityForResult(intent, REQUEST_READ);
            ((FragmentActivity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        }
    }

    public void onClickRead(SearchItemHuge item) {
        new TrackerHelper(item).clickOnHomePage(TrackerLevel.RESEARCH_RESULTS_FIND, ClickType.clic_contenu_consultation);
        TrackerLevel trackLevel;
        Intent intent;
        if(isInfoResult()){
            switch (item.getNodeType()){
                case VIDEO:
                case FICHES_INFOS:
                case LIENS_EXTERNES:
                    trackLevel=TrackerLevel.UNIT_CONTENTS_INFORMATION;
                    break;
                default:
                    // for others nodes we don't track page in webview
                    trackLevel=TrackerLevel.UNKNOWN;
                    break;
            }
            intent = WebViewActivity.newIntent(getContext(), new WebViewArg(item, trackLevel));
        }else{
            intent = WebViewActivity.newIntent(getContext(), new WebViewArg(item, TrackerLevel.UNIT_CONTENTS_FIND));
        }
        startActivityForResult(intent, REQUEST_READ);
        ((FragmentActivity) getContext()).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    protected abstract boolean shouldLoadData();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_get_informed_results, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViews();
        viewCreated(view, savedInstanceState);
    }

    protected abstract void viewCreated(View view, Bundle savedInstanceState);

    protected abstract SearchResultsAdapter createAdapter(SearchResultsAdapter.ResultsAdapterListener adapterListener);

    private void bindViews() {
        SearchResultsAdapter.ResultsAdapterListener adapterListener = new SearchResultsAdapter.ResultsAdapterListener() {
            @Override
            public void onNextPage() {
                resultsPresenter.search(null);
            }

            @Override
            public void onReminderClose() {
                listener.onReminderClose();
            }

            @Override
            public void onRelatedItemClicked(RelatedItem item) {
                SearchResultsFragment.this.onRelatedItemClicked(item);
            }

            @Override
            public void onClickYes() {
                resultsPresenter.clickYes();
            }

            @Override
            public void updateSort(String sortValue) {
                if (!resultsPresenter.getOrder().equals(sortValue)) {
                    resultsPresenter.clearPagination();
                    resultsPresenter.setOrder(sortValue);
                    resultsPresenter.search(null);
                }

            }

            @Override
            public void onClickNo() {
                resultsPresenter.clickNo();
            }

            @Override
            public void onCloseMessage(SearchMessage message) {
                resultsPresenter.markMessageAsRead(message);
            }
        };

        resultsAdapter = createAdapter(adapterListener);

        VerticalSpaceItemDecoration decoration = new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.default_margin_x0_5));
        rvCards.addItemDecoration(decoration);

        rvCards.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        rvCards.setLayoutManager(layoutManager);
        rvCards.setAdapter(resultsAdapter);
    }

    protected abstract void onRelatedItemClicked(RelatedItem item);

    private List<IPresenter> presenters = new ArrayList<>();

    @NonNull
    @Override
    public List<IPresenter> getPresenters() {
        return presenters;
    }

    protected abstract IPresenter getAdditionalPresenter();

    public void onSearchMessageRemoved(SearchMessage searchMessage) {
        resultsAdapter.removeMessage(searchMessage);
    }

    @Override
    public void addResults(Cursor cursor, String suggestedType, boolean sortedByDistance, boolean hasMoreItems) {
        emptyView.setVisibility(View.GONE);
        rvCards.setVisibility(View.VISIBLE);

        if (resultsPresenter.getCurrentPage() == 0) {
            layoutManager.scrollToPosition(0);
        }

        resultsAdapter.addItems(cursor, sortedByDistance, hasMoreItems);
//        listener.onChangeFilterActionButtonVisibility(true, this);
    }

    @Override
    public void showEmptyView() {
        emptyView.removeAllViews();
        emptyView.setVisibility(View.VISIBLE);
        showEmptyView(emptyView, resultsPresenter.isLocationProvided());
        rvCards.setVisibility(View.GONE);
//        listener.onChangeFilterActionButtonVisibility(resultsPresenter.hasFilters(), this);
    }

    public boolean isEmpty() {
        return rvCards.getVisibility() == View.GONE;
    }

    public boolean isEmptyFilters() {
        return resultsPresenter.isEmptyFilters();
    }

    protected abstract void showEmptyView(ViewGroup parent, boolean locationDefined);

    @Override
    public void showFindCount(int count) {
        listener.showFindCount(count);
    }

    @Override
    public void showGetInformedCount(int count) {
        listener.showGetInformedCount(count);
    }

    @Override
    public void hideLoading() {
        progressDialog.dismiss();
        isProgress=false;
    }

    @Override
    public void updateBmcView(String html) {
        resultsAdapter.setBmcValue(html);
    }

    @Override
    public void updateQuestionnaireView(QuestionnaireItem item) {
        resultsAdapter.setQuestionnaireView(item);
    }

    @Override
    public void showLoading() {
        progressDialog.show();
        isProgress=true;
    }

    @Override
    public boolean isProgress(){
//        return progressDialog.isShowing();
        return isProgress;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (resultsPresenter.isAroundMe() != getArguments().getBoolean(ARG_AROUND_ME, false)) {
            outState.putBoolean(ARG_AROUND_ME, resultsPresenter.isAroundMe());
            outState.putDouble(ARG_LATITUDE, resultsPresenter.getLatitude());
            outState.putDouble(ARG_LONGITUDE, resultsPresenter.getLatitude());
        }
        super.onSaveInstanceState(outState);
    }

    public void onReminderClosed() {
        resultsAdapter.onReminderClosed();
    }

    @Override
    public void showExitDialog() {
        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                .setView(R.layout.dialog_feedback_close)
                .setPositiveButton(R.string.to_close, (dialog, which) -> {

                })
                .show();
    }

    @Override
    public void onChangeSearchCommand(SearchCommand searchCommand) {

    }

    @Override
    public void onSetSortedByDistance(boolean sortedByDistance) {

    }

    @Override
    public void showRelated(List<Related> relatedList) {
        resultsAdapter.showRelated(relatedList);
    }

    @Override
    public void showMessages(List<SearchMessage> messages) {
        resultsAdapter.showMessages(messages);
    }

    @Override
    public void showRegistrationScreen() {
        Intent intent = CreateAccountActivity.newIntent(getContext(), CreateAccountActivity.START_STANDART);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
    }

    @Override
    public void onNoResultsFromServer() {
        listener.onNoResults(searchType);
    }

    @Override
    public void onFirstPageLoaded(SearchType searchType, boolean empty) {
        listener.onFirstPageLoaded(searchType, empty);
    }

    private boolean isInfoResult() {
        return this instanceof InfoSearchResultsFragment;
    }

    public boolean hasFilters() {
        return resultsPresenter.hasFilters();
    }

}
