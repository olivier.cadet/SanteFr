package com.adyax.srisgp.mvp.framework;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;

public abstract class NetworkPresenter<V extends IView> extends Presenter<V> implements AppReceiver.AppReceiverCallback {
    public AppReceiver appReceiver;
    private ProgressDialog progressDialog;

    @Override
    public void attachView(@NonNull V view) {
        super.attachView(view);
        if (appReceiver == null)
            appReceiver = new AppReceiver();
        appReceiver.setListener(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        appReceiver.setListener(null);
    }

    public boolean sendCommand(ServiceCommand serviceCommand, int requestCode) {
        if (getView() != null) {
            ExecutionService.sendCommand(getContext(), appReceiver, serviceCommand, requestCode);
            return true;
        } else {
            return false;
        }
    }

    public void showProgress() {
        if (getView() != null) {
            hideProgress();
            progressDialog = ProgressDialog.show(getContext(), null, null, true, false);
        }
    }

    public void hideProgress() {
        if (getView() != null) {
            if (progressDialog != null)
                progressDialog.dismiss();
        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {

    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {

    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }
}
