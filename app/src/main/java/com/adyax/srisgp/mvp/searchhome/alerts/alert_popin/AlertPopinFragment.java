package com.adyax.srisgp.mvp.searchhome.alerts.alert_popin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.databinding.FragmentBigAlertsBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.framework.ViewFragment;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class AlertPopinFragment extends ViewFragment implements AlertPopinPresenter.AlertView {


    @Inject
    AlertPopinPresenter alertPopinPresenter;
    @Inject
    FragmentFactory fragmentFactory;

    private FragmentBigAlertsBinding binding;
    private AlertItem alertItem;

    public AlertPopinFragment() {
    }

    public static Fragment newInstance(AlertItem alertItem) {
        AlertPopinFragment fragment = new AlertPopinFragment();
        Bundle args = new Bundle();
        args.putParcelable(AlertItem.BUNDLE_NAME, alertItem);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        App.getApplicationComponent().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        alertItem =getArguments().getParcelable(AlertItem.BUNDLE_NAME);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_big_alerts, container, false);
        binding.closeAlert.setOnClickListener(view -> {
            close(false);
        });
        binding.title.setText(alertItem.getTitle());
        binding.title.setTextColor(getContext().getResources().getColor(alertItem.getColor()));
        binding.description.setText(alertItem.getDescription());
        binding.bottonText.setOnClickListener(view -> {
            alertPopinPresenter.clickEnSavoirPlus(alertItem);
        });
        if(!alertItem.isContent()) {
            binding.bottonText.setVisibility(View.GONE);
        }else{
            binding.bottonText.setText(alertItem.getBottonText());
        }
        if(!alertItem.isRedAlert()){
            binding.bottonText.setBackground(getActivity().getResources().getDrawable(R.drawable.shape_rectangle_rounded_orange));
            binding.title.setTextColor(getActivity().getResources().getColor(R.color.orange));
            binding.alarmImage.setImageResource(R.drawable._ic_alert_attention);
        }
        binding.getRoot().setOnTouchListener((v, event) -> true);
        return binding.getRoot();
    }

    @Override
    public void close(boolean bRedBotton) {
        Intent intent = new Intent();
        getActivity().setResult(bRedBotton? Activity.RESULT_OK: Activity.RESULT_CANCELED, intent);
        getActivity().finish();
        if(getActivity() instanceof IAlertPopinActivity){
            ((IAlertPopinActivity)getActivity()).closeAlert();
        }
    }


    @NonNull
    @Override
    public AlertPopinPresenter getPresenter() {
        return alertPopinPresenter;
    }
}
