package com.adyax.srisgp.mvp.favorite;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public enum FavoriteType {

    all(0),
    find(1),
    get_informed(2);

    private int value;

    FavoriteType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
