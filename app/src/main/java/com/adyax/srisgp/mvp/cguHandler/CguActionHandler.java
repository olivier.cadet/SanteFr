package com.adyax.srisgp.mvp.cguHandler;

public class CguActionHandler {
    private Runnable cgu_accepted_callback;
    private Runnable cgu_declined_callback;

    public CguActionHandler(Runnable cgu_accepted_callback, Runnable cgu_declined_callback) {
        this.cgu_accepted_callback = cgu_accepted_callback;
        this.cgu_declined_callback = cgu_declined_callback;
    }

    public Runnable getCgu_accepted_callback() {
        return cgu_accepted_callback;
    }

    public Runnable getCgu_declined_callback() {
        return cgu_declined_callback;
    }
}
