package com.adyax.srisgp.mvp.create_account.flow.finish_create_account;

import com.adyax.srisgp.App;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class FinishCreateAccountPresenter extends Presenter<FinishCreateAccountPresenter.CreateRecipeView> {

    public interface CreateRecipeView extends IView {
    }

    public FinishCreateAccountPresenter() {
    }

    @Override
    public void attachView(CreateRecipeView view) {
        super.attachView(view);
        App.getApplicationComponent().inject(this);
    }

}
