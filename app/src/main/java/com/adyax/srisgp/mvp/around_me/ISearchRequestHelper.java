package com.adyax.srisgp.mvp.around_me;

import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;

import java.util.ArrayList;

/**
 * Created by SUVOROV on 12/29/16.
 */

public interface ISearchRequestHelper {
    void restore();
    void save();
    boolean isFilterChanged(ArrayList<SearchFilter> filters, SearchType searchType);
}
