package com.adyax.srisgp.mvp.web_views.summary_web_view;

import android.app.Activity;
import android.content.Intent;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.response.tags.AnchorItem;
import com.adyax.srisgp.mvp.framework.IView;
import com.adyax.srisgp.mvp.framework.Presenter;

/**
 * Created by SUVOROV on 8/3/16.
 */
public class SummaryWebViewPresenter extends Presenter<SummaryWebViewPresenter.CreateRecipeView> {

    public interface CreateRecipeView extends IView {
    }

    public SummaryWebViewPresenter() {
    }

}
