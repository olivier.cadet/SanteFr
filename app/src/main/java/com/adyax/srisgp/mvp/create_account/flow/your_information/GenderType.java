package com.adyax.srisgp.mvp.create_account.flow.your_information;

import com.adyax.srisgp.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 9/28/16.
 */

public enum GenderType {

    indifferent(R.string.indifferent),
    female(R.string.female),//Femme
    male(R.string.male),
    empty(-1);

    private int value;
    private static Map map = new HashMap<>();

    private GenderType(int value) {
        this.value = value;
    }

    static {
        for (GenderType pageType : GenderType.values()) {
            map.put(pageType.value, pageType);
        }
    }

    public static GenderType valueOf(int pageType) {
        return (GenderType) map.get(pageType);
    }

    public int getValue() {
        return value;
    }

}
