package com.adyax.srisgp.mvp.feedback;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.databinding.ActivityFeedbackSmallTitleBinding;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;


/**
 * Created by SUVOROV on 8/3/16.
 */
public class FeedbackActivity extends MaintenanceBaseActivity {
    private static final String INTENT_URL = "intent_url";
    private static final String INTENT_FORM_NAME = "intent_form_name";

    private ActivityFeedbackSmallTitleBinding binding;

    @Inject
    FragmentFactory fragmentFactory;

    public static Intent newIntent(Context context, String url, String formName) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra(INTENT_URL, url);
        intent.putExtra(INTENT_FORM_NAME, formName);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback_small_title);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        fragmentFactory.startFeedbackFragment(this, getIntent().getStringExtra(INTENT_URL), getIntent().getStringExtra(INTENT_FORM_NAME));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
