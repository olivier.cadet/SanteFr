package com.adyax.srisgp.mvp.create_account.flow.finish_create_account;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.page_finish_create_account.PageFinishCreateAccountFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.page_finish_create_account.TypeCreateAccount;

/**
 * Created by SUVOROV on 9/30/16.
 */

public class FinishCreateAccountPagerAdapter extends FragmentPagerAdapter {

//    public static final int SCR1 = 0;
//    public static final int SCR2 = 1;
//    public static final int SCR3 = 2;
//    public static final int SCR4 = 3;
//    public static final int SCR5 = 4;
//    public static final int SCR6 = 5;
//    public static final int SCR7 = 6;
//public static final int AMOUNT = 6 + 1;

    public static final int SCR4 = 0;
    public static final int SCR5 = 1;
    public static final int SCR6 = 2;
    public static final int SCR7 = 3;
    public static final int AMOUNT = 3 + 1;

    public FinishCreateAccountPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
//            case SCR1:
//                return InputPassword_1_Fragment.newInstance();
//            case SCR2:
//                return YourInformation_2_Fragment.newInstance();
//            case SCR3:
//                return PointsOfInterest_3_Fragment.newInstance();
            case SCR4:
                return PageFinishCreateAccountFragment.newInstance(TypeCreateAccount.favorite);
            case SCR5:
                return PageFinishCreateAccountFragment.newInstance(TypeCreateAccount.notification);
            case SCR6:
                return PageFinishCreateAccountFragment.newInstance(TypeCreateAccount.more);
            case SCR7:
                return PageFinishCreateAccountFragment.newInstance(TypeCreateAccount.search);
        }
        throw new RuntimeException("too much fragments");
    }

    @Override
    public int getCount() {
        return AMOUNT;
    }
}
