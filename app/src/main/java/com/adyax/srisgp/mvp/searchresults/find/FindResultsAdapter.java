package com.adyax.srisgp.mvp.searchresults.find;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.common.FindCardHolder;
import com.adyax.srisgp.mvp.common.FindCardListener;
import com.adyax.srisgp.mvp.common.SearchItemHolder;
import com.adyax.srisgp.mvp.searchresults.SearchResultsAdapter;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;

/**
 * Created by anton.kobylianskiy on 10/26/16.
 */

public class FindResultsAdapter extends SearchResultsAdapter {
    private FindCardListener findCardListener;

    public FindResultsAdapter(ResultsAdapterListener resultsListener, FindCardListener findCardListener, IRepository repository) {
        super(resultsListener, repository);
        this.findCardListener = findCardListener;
    }

    @Override
    public int getActivityLocationID() {
        return WidgetViewHelper.LOCATION_FIND;
    }

    @Override
    protected SearchItemHolder createCardViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        return new FindCardHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_around_me, parent, false), false, repository);
    }

    @Override
    protected void onBindCardViewHolder(RecyclerView.ViewHolder holder, SearchItemHuge item) {
        ((FindCardHolder) holder).bind(item, findCardListener);
    }

    @Override
    protected String getHeaderTitle() {
        return App.getAppContext().getString(R.string.search_results_sort_distance);
    }
}
