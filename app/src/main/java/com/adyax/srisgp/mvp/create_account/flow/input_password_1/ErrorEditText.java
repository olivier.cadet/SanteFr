package com.adyax.srisgp.mvp.create_account.flow.input_password_1;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.appcompat.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.adyax.srisgp.R;

/**
 * Created by SUVOROV on 10/20/16.
 */

public class ErrorEditText extends AppCompatEditText {

    private static final int[] STATE_ERROR = {R.attr.state_error};

    private boolean mIsError = false;
    private ErrorListener errorListener;

    public interface ErrorListener {
        void onErrorStateChanged(boolean isShow);
    }

    public ErrorEditText(Context context) {
        this(context, null, R.style.EditTextStyle);
        init();
    }

    public ErrorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ErrorEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public ErrorEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        init();
//    }

    private void init() {
        setError(null);

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                // empty
            }
        });
    }

    public void setErrorListener(ErrorListener errorListener) {
        this.errorListener = errorListener;
    }

    @Override
    public void setError(CharSequence error) {
        mIsError = error != null;
        super.setError(error);
        refreshDrawableState();

        if (errorListener != null)
            errorListener.onErrorStateChanged(mIsError);
    }

    @Override
    public void setError(CharSequence error, Drawable icon) {
        mIsError = error != null;
        super.setError(error, icon);
        refreshDrawableState();

        if (errorListener != null)
            errorListener.onErrorStateChanged(mIsError);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (mIsError) {
            mergeDrawableStates(drawableState, STATE_ERROR);
        }
        return drawableState;
    }
}