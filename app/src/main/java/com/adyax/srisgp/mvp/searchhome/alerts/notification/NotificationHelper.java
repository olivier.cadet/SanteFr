package com.adyax.srisgp.mvp.searchhome.alerts.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.presentation.MainActivity;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by SUVOROV on 11/1/16.
 */

public class NotificationHelper implements INotificationHelper {

    public static final String ALERT_LEVEL = "alert_level";
    public static final String LINK_LABEL = "link_label";
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String CONTENT_URL = "content_url";

    private Context context;
    private static NotificationHelper notificationHelper;

    private NotificationHelper(Context context) {
        this.context = context;
    }

    public static synchronized NotificationHelper getInstance(Context context){
        if(notificationHelper == null) {
            notificationHelper = new NotificationHelper(context);
        }
        return notificationHelper;
    }

    @Override
    public void send(AlertItem alertItem) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, NotificationChannelIds.NEWS);

//Create the intent that’ll fire when the user taps the builder//

//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.androidauthority.com/"));
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
//
//        builder.setContentIntent(pendingIntent);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if(alertItem.isContent()){
//            final String bottonText = alertItem.getBottonText();
//            if(bottonText !=null) {
//                builder.addAction(R.drawable.ic_fiche_green_24dp, bottonText, getPendingIntent(alertItem));
//            }else{
                builder.addAction(R.drawable.ic_fiche_green_24dp, context.getString(R.string.search_home_read), getPendingIntent(alertItem));
//            }
        }

        if(alertItem.isSoundAlert()){
            builder.setSound(soundUri)
            .setVibrate(new long[]{500, 500, 500, 500, 500});
        }

        builder
//        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable._icon_notify_24dp))
//        .setSmallIcon(R.drawable.notification_icon);
        .setSmallIcon(R.drawable._ic_logo_for_notification_24dp)
//        .setStyle(new Notification.BigPictureStyle())
        .setContentTitle(alertItem.getTitle())
        .setContentText(alertItem.getDescription())
        .setContentIntent(getPendingIntent(alertItem))
//        .setUsesChronometer(true)
//        .setWhen(alertItem.getDate())
//        .setStyle(new Notification.MediaStyle())
        .setColor(context.getResources().getColor(R.color.nav_header_center))
        .setAutoCancel(true)
        .addAction(R.drawable.ic_fermer_24dp, context.getString(R.string.to_close), getPendingDeleteIntent(alertItem))
        .setDeleteIntent(getPendingDeleteIntent(alertItem));


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(alertItem.getIdForNotify(), builder.build());
    }

    @Override
    public void send(RemoteMessage remoteMessage) {
        AlertItem alertItem=new AlertItem();
        alertItem.button_text="";
        Map<String, String> data = remoteMessage.getData();
//        final String content_url = data.get(CONTENT_URL);
//        alertItem.content_url= (content_url==null&& BuildConfig.DEBUG)? "https://dev.sris-gp.adyax-dev.com/node/539736": content_url;
        alertItem.content_url = data.get(CONTENT_URL);
        final RemoteMessage.Notification notification = remoteMessage.getNotification();
        if(notification!=null) {
            alertItem.title = notification.getTitle();
            alertItem.description = notification.getBody();
        }else{
            alertItem.title = data.get(TITLE);
            alertItem.description = data.get(BODY);
        }
        try {
            alertItem.level = Integer.parseInt(data.get(ALERT_LEVEL));
        }catch (Exception e){
        }
        alertItem.id=remoteMessage.getSentTime();
        alertItem.push_read =true;
        alertItem.button_text = data.get(LINK_LABEL);
        if(alertItem.content_url!=null&& alertItem.button_text==null){
            alertItem.button_text=context.getString(R.string.search_home_read);
        }
        send(alertItem);

////        remoteMessage.getNotification().getTag();
//
//        NotificationCompat.Builder builder =
//                new NotificationCompat.Builder(context);
//
//        builder
////        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable._icon_notify_24dp))
////        .setSmallIcon(R.drawable.notification_icon);
//                .setSmallIcon(R.drawable._ic_logo_for_notification_24dp)
////        .setStyle(new Notification.BigPictureStyle())
//                .setContentTitle(remoteMessage.getNotification().getTitle())
////                .setContentText(remoteMessage.getNotification().getBody())
//                .setContentText(remoteMessage.getNotification().getBody());
//        if(true) {
//            builder.setContentIntent(getPendingIntent(new AlertItem("https://dev.sris-gp.adyax-dev.com/node/539736")));
//        }
////        .setUsesChronometer(true)
////        .setWhen(alertItem.getDate())
////        .setStyle(new Notification.MediaStyle())
//        builder
//                .setColor(context.getResources().getColor(R.color.nav_header_center))
//                .setAutoCancel(true);
////                .addAction(R.drawable.ic_fermer_24dp, context.getString(R.string.to_close), getPendingDeleteIntent(alertItem))
////                .setDeleteIntent(getPendingDeleteIntent(alertItem));
//
//
//
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(new Long(remoteMessage.getSentTime()&0xffffffff).intValue(), builder.build());



    }

    private PendingIntent getPendingDeleteIntent(AlertItem alertItem) {
        Intent intent = new Intent(context, DeleteNotificationBroadcastReceiver.class);
        intent.putExtra(AlertItem.BUNDLE_NAME, alertItem);
        return PendingIntent.getBroadcast(context, alertItem.getIdForNotify(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent getPendingIntent(AlertItem alertItem) {
        Intent intent=new Intent(context, MainActivity.class);
        intent.putExtra(AlertItem.BUNDLE_NAME, alertItem);
        return PendingIntent.getActivity(context, alertItem.getIdForNotify(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


}
