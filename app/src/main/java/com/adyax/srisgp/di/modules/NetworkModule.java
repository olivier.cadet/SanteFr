package com.adyax.srisgp.di.modules;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Base64;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.gson_extention.InterestNodeCountDeserializer;
import com.adyax.srisgp.data.gson_extention.NonStandardConverter;
import com.adyax.srisgp.data.gson_extention.RelatedFiltersDeserializer;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.NetWorkState;
import com.adyax.srisgp.data.net.command.AlertsCommand;
import com.adyax.srisgp.data.net.command.builders.AlertsIdsBuilder;
import com.adyax.srisgp.data.net.core.RestClient;
import com.adyax.srisgp.data.net.core.RetrofitService;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.INotificationHelper;
import com.adyax.srisgp.sound.ISoundPlayer;
import com.adyax.srisgp.utils.FiltersHelper;
import com.adyax.srisgp.utils.UUIDUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

@Module
public class NetworkModule {

    public static final String COOKIE = "Cookie";
    public static final String X_REQUESTED_WITH = "X-Requested-With";
    public static final String I_OS_VIEW = "iOS_view";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BASIC = "Basic ";
    public static final String USER_AGENT = "User-Agent";
    public static final String X_ALERT_INFO = "X-Alert-Info";
    public static final String X_CSRF_TOKEN = "X-CSRF-Token";
    public static final String X_ALERTS_SINCE = "X-Alerts-Since";
    public static final String X_COORDINATES = "X-Coordinates";
    public static final String X_COORDINATES_CHANGED = "X-Coordinates-Changed";
    public static final String X_USER_AGENT = "X-User-Agent";

    private String baseUrl;

    public NetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Singleton
    @Provides
    CommandExecutor provideCommandExecutor(Context context, RestClient restClient, IRepository repository,
                                           IDataBaseHelper chatDataBaseHelper, INetWorkState netWorkState,
                                           FiltersHelper filtersHelper, ISoundPlayer soundPlayer,
                                           INotificationHelper notificationHelper, ITracker tracker) {
        return new CommandExecutor(context, restClient, repository, chatDataBaseHelper,
                netWorkState, filtersHelper, soundPlayer, notificationHelper, tracker);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        Gson gson = new GsonBuilder()
//                .setDateFormat(dateFormat)
                .registerTypeAdapter(Timestamp.class, new JsonDeserializer<Timestamp>() {

                    final DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

                    @Override
                    public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                        try {
                            final String asString = json.getAsString();
                            if (asString != null) {
                                final String[] split = asString.split("/");
                                if (split.length == 3 && split[0].length() == 2 && split[1].length() == 2 && split[2].length() == 4) {
                                    return new Timestamp(df.parse(asString).getTime());
                                } else {
                                    return new Timestamp(Long.parseLong(asString) * 1000);
                                }
                            }
                        } catch (final java.text.ParseException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                })
                .registerTypeAdapter(RelatedFilters.class, new RelatedFiltersDeserializer())
//                .registerTypeAdapter(MinistryLogoUrl.class, new MinistryLogoUrlDeserializer())
                .registerTypeAdapter(new TypeToken<Map<Long, Integer>>() {
                }.getType(), new InterestNodeCountDeserializer())
                .create();
        return gson;
    }

//    @Provides
//    @Singleton
//    Gson provideGson() {
//        return new GsonBuilder()
//                .create();
//    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Context context, IRepository repository) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        if (android.os.Build.VERSION.SDK_INT == android.os.Build.VERSION_CODES.N) {
            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2)
                    .cipherSuites(
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                    .build();
            builder.connectionSpecs(Collections.singletonList(spec));
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(logging);

//        builder.setRequestInterceptor(new RequestInterceptor() {
//            @Override
//            public void intercept(RequestFacade request) {
//                // assuming `cookieKey` and `cookieValue` are not null
//                request.addHeader("Cookie", cookieKey + "=" + cookieValue);
//            }
//        })

//        builder.addInterceptor(new Interceptor() {
//              @Override
//              public Response intercept(Interceptor.Chain chain) throws IOException {
//                  Request original = chain.request();
//
//                  Request request = original.newBuilder()
//                          .header("test", "test")
//                          .method(original.method(), original.body())
//                          .build();
//
//                  return chain.proceed(request);
//              }
//          });

        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };

        // Install the all-trusting trust manager
        SSLContext sslContext = null;
        try {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

        builder.sslSocketFactory(sslSocketFactory);
        builder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        builder.interceptors().add(chain -> {
            Request original = chain.request();
//            final String credentials = String.format("%s:%s", UrlExtras.USER, UrlExtras.PASS);
            final String basic = BASIC + Base64.encodeToString(BuildConfig.CREDENTIAL_SERVER.getBytes(), Base64.NO_WRAP);
            Request.Builder requestBuilder = original.newBuilder();
            requestBuilder.addHeader(AUTHORIZATION, basic);
            requestBuilder.addHeader("Accept", "application/json");
            requestBuilder.addHeader("Content-Type", "application/json");

            // Adding xdebug cookie if app is in debug variant
            if(BuildConfig.DEBUG) {
                requestBuilder.addHeader(COOKIE, String.format("%s=%s", "XDEBUG_SESSION", "PHPSTORM"));
            }

            requestBuilder.addHeader(X_ALERTS_SINCE, "0");
            requestBuilder.addHeader(NetworkModule.X_USER_AGENT, getVersionString(context));

//            Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            final String geoLocation = repository.getGeoLocation();
            if (geoLocation != null) {
                requestBuilder.addHeader(X_COORDINATES, geoLocation);
//                if (BuildConfig.DEV_STATUS) {
                requestBuilder.addHeader(X_COORDINATES_CHANGED, UUIDUtils.generateUniqueID(context));
//                }else{
//                    requestBuilder.addHeader(X_COORDINATES_CHANGED, "1");
//                }
            }

//            requestBuilder.addHeader("X-Requested-With", "iOS_view");


            RegisterUserResponse credential = repository.getCredential();
            if (!credential.isEmpty()) {
                requestBuilder.addHeader(X_CSRF_TOKEN, credential.getToken());
                requestBuilder.addHeader(COOKIE, String.format("%s=%s", credential.getSession_name(), credential.getSessid()));
            }
            Request request = requestBuilder.build();
            Response proceed = chain.proceed(request);
            final String header = proceed.header(X_ALERT_INFO);

            /*
             * impossible d'utiliser la fonction isRedirect()
             * proceed.isRedirect()
             * car elle renvoie false même en cas de redirection serveur
             */
            // if (!request.url().uri().getPath().equals(proceed.request().url().uri().getPath())) {
            //    // Log.d("Faisal", "Redirection detected " + (proceed.isRedirect() ? "OUI " : "NON ") + request.url().uri().getPath());
            // }
            if (AlertsIdsBuilder.alertsCompare(header, repository.getTemporaryHeader())) {
                ExecutionService.sendCommand(context, null,
                        new AlertsCommand(header), ExecutionService.ALERTS_ACTION);
            }
            repository.setTemporaryHeader(header);
            return proceed;
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            builder.networkInterceptors().add(httpLoggingInterceptor);
        }

        OkHttpClient okHttpClient = builder.build();
        Glide.get(context).getRegistry().replace(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(okHttpClient));
        return okHttpClient;
    }

    @Provides
    @Singleton
    GsonConverterFactory providerGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    RxJavaCallAdapterFactory provideRxJavaCallFactory() {
        return RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             GsonConverterFactory gsonConverterFactory,
                             RxJavaCallAdapterFactory rxJavaCallAdapterFactory,
                             NonStandardConverter nonStandardConverter) {

        Retrofit retrofit = (new Retrofit.Builder())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(nonStandardConverter)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    RestClient provideRestClient(Retrofit retrofit) {
        RetrofitService retrofitService = retrofit.create(RetrofitService.class);
        return new RestClient(retrofitService);
    }

    @Provides
    @Singleton
    NonStandardConverter provideNonStandartResponse() {
        return new NonStandardConverter();
    }

    @Singleton
    @Provides
    INetWorkState provideRepository(Context context, IRepository repository, IDataBaseHelper dataBaseHelper) {
        return new NetWorkState(context, repository, dataBaseHelper);
    }

    public static String getVersionString(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            return String.format("Sante.fr/%s_%d (Android %s)", info.versionName, info.versionCode, Build.VERSION.RELEASE);
        } catch (PackageManager.NameNotFoundException e) {
            Timber.e(e);
        }
        return String.format("Sante.fr/x.x.x_x(Android %s)", Build.VERSION.RELEASE);
    }
}
