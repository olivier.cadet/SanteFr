package com.adyax.srisgp.di.modules;

import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationPresenter;
import com.adyax.srisgp.mvp.authentication.forgotten_password.ForgottenPasswordPresenter;
import com.adyax.srisgp.mvp.authentication.login.LoginPresenter;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountPresenter;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.page_finish_create_account.PageFinishCreateAccountPresenter;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.YourInformation_2_Presenter;
import com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3.PointsOfInterest_3_Presenter;
import com.adyax.srisgp.mvp.favorite.FavoritePresenter;
import com.adyax.srisgp.mvp.favorite.YourFavoritesPresenter;
import com.adyax.srisgp.mvp.history.YourHistoryPresenter;
import com.adyax.srisgp.mvp.notification.NotificationPresenter;
import com.adyax.srisgp.mvp.my_account.MyAccountPresenter;
import com.adyax.srisgp.mvp.search.SearchPresenter;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinPresenter;
import com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes.AlertsSwipePresenter;
import com.adyax.srisgp.mvp.web_views.summary_web_view.SummaryWebViewPresenter;

import dagger.Module;
import dagger.Provides;

//import com.cybopob.edit_pager_recipe.EditPagerRecipePresenter;

/**
 * Created by SUVOROV on 3/20/16.
 */
@Module
public class PresentersModule {

//    @Provides
//    SearchPresenter provideSearchPresenter() {
//        return new SearchPresenter();
//    }

    @Provides
    FinishCreateAccountPresenter provideSignInPresenter() {
        return new FinishCreateAccountPresenter();
    }

    @Provides
    YourInformation_2_Presenter provideYourInformation_2_Presenter() {
        return new YourInformation_2_Presenter();
    }

    @Provides
    PointsOfInterest_3_Presenter provideDialog_3_Presenter() {
        return new PointsOfInterest_3_Presenter();
    }

    @Provides
    PageFinishCreateAccountPresenter provideFinishCreateAccountPresenter() {
        return new PageFinishCreateAccountPresenter();
    }

//    @Provides
//    ProvideLocationPresenter provideProvideLocationPresenter() {
//        return new ProvideLocationPresenter();
//    }

    @Provides
    ForgottenPasswordPresenter provideForgottenPasswordPresenter() {
        return new ForgottenPasswordPresenter();
    }

    @Provides
    YourFavoritesPresenter provideYourFavoritesPresenter() {
        return new YourFavoritesPresenter();
    }

    @Provides
    AlertsSwipePresenter provideAlertsSwipePresenter() {
        return new AlertsSwipePresenter();
    }

//    @Provides
//    YourHistoryPresenter provideYourHistoryPresenter() {
//        return new YourHistoryPresenter();
//    }

    @Provides
    SummaryWebViewPresenter provideSummaryWebViewPresenter() {
        return new SummaryWebViewPresenter();
    }

//    @Provides
//    SummaryWebViewPresenter provideSummaryWebViewPresenter() {
//        return new SummaryWebViewPresenter();
//    }
}
