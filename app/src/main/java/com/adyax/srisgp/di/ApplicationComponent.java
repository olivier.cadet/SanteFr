package com.adyax.srisgp.di;

import com.adyax.srisgp.appwidget.AppWidgetListRemoteViewsFactory;
import com.adyax.srisgp.data.net.command.builders.SearchRequestBuilder;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.di.modules.ApplicationModule;
import com.adyax.srisgp.di.modules.NetworkModule;
import com.adyax.srisgp.di.modules.PresentersModule;
import com.adyax.srisgp.firebase.AsipMessagingService;
import com.adyax.srisgp.mvp.emergency.EmergencyActivity;
import com.adyax.srisgp.mvp.emergency.EmergencyFragment;
import com.adyax.srisgp.mvp.around_me.AroundMeActivity;
import com.adyax.srisgp.mvp.around_me.AroundMeFragment;
import com.adyax.srisgp.mvp.around_me.AroundMePresenter;
import com.adyax.srisgp.mvp.around_me.filters.FiltersFragment;
import com.adyax.srisgp.mvp.around_me.get_location.GetCityFragment;
import com.adyax.srisgp.mvp.around_me.get_location.GetLocationAroundMeFragment;
import com.adyax.srisgp.mvp.ask_login.AskLoginFragment;
import com.adyax.srisgp.mvp.authentication.forgotten_password.ForgottenPasswordFragment;
import com.adyax.srisgp.mvp.authentication.forgotten_password.ForgottenPasswordPresenter;
import com.adyax.srisgp.mvp.authentication.login.LoginActivity;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.contact.ContactFragment;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountPresenter;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ask_geo.AskGeoFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.page_finish_create_account.PageFinishCreateAccountFragment;
import com.adyax.srisgp.mvp.create_account.flow.input_password_1.InputPassword_1_Fragment;
import com.adyax.srisgp.mvp.create_account.flow.input_password_1.InputPassword_1_Presenter;
import com.adyax.srisgp.mvp.create_account.flow.providelocation.ProvideLocationFragment;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.YourInformation_2_Fragment;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.YourInformation_2_Presenter;
import com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3.PointsOfInterest_3_Fragment;
import com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3.PointsOfInterest_3_Presenter;
import com.adyax.srisgp.mvp.dummy.DummyFragment;
import com.adyax.srisgp.mvp.favorite.FavoriteFragment;
import com.adyax.srisgp.mvp.favorite.FavoritesFragment;
import com.adyax.srisgp.mvp.favorite.YourFavoritesFragment;
import com.adyax.srisgp.mvp.feedback.FeedbackActivity;
import com.adyax.srisgp.mvp.feedback.FeedbackFragment;
import com.adyax.srisgp.mvp.history.HistoriesFragment;
import com.adyax.srisgp.mvp.history.HistoryFragment;
import com.adyax.srisgp.mvp.history.YourHistoryFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceFragment;
import com.adyax.srisgp.mvp.my_account.ChangeCredentialsActivity;
import com.adyax.srisgp.mvp.my_account.MyAccountFragment;
import com.adyax.srisgp.mvp.my_account.MyAccountPresenter;
import com.adyax.srisgp.mvp.my_account.PersonalInformationActivity;
import com.adyax.srisgp.mvp.my_account.PersonalizeActivity;
import com.adyax.srisgp.mvp.notification.NotificationFragment;
import com.adyax.srisgp.mvp.notification.NotificationsFragment;
import com.adyax.srisgp.mvp.notification.YourNotificationsFragment;
import com.adyax.srisgp.mvp.onboarding.GeolocationOnboardingFragment;
import com.adyax.srisgp.mvp.onboarding.OnboardingActivity;
import com.adyax.srisgp.mvp.report.ReportActivity_1;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_email.ReportFicheEmailFragment_2;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_finish.ReportFinishFragment_3;
import com.adyax.srisgp.mvp.report.report_find.report_find_user.ReportFindUserFragment_1;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_find_fiche.ReportFindFicheFragment_1;
import com.adyax.srisgp.mvp.report.report_info.report_info_1.ReportInfoFragment_1;
import com.adyax.srisgp.mvp.report.report_info.report_email.ReportEmailFragment_2;
import com.adyax.srisgp.mvp.search.SearchFragment;
import com.adyax.srisgp.mvp.searchhome.SearchHomeFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_item.AlertItemFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_item.AlertItemPresenter;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinActivity;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes.AlertsSwipeFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.alerts_swipes.AlertsSwipePresenter;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.DeleteNotificationBroadcastReceiver;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;
import com.adyax.srisgp.mvp.searchresults.SearchResultsFragment;
import com.adyax.srisgp.mvp.searchresults.find.FindSearchResultsFragment;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.summary_web_view.SummaryWebViewFragment;
import com.adyax.srisgp.presentation.AskGeoPlusAlertPopupActivity;
import com.adyax.srisgp.presentation.MainActivity;
import com.adyax.srisgp.presentation.PlusAlertPopupActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class, PresentersModule.class})
public interface ApplicationComponent {
    void inject(ExecutionService executionService);

    void inject(AroundMeFragment aroundMeFragment);

    void inject(AroundMePresenter aroundMePresenter);

    void inject(MainActivity mainActivity);

    void inject(OnboardingActivity activity);

    void inject(SearchHomeFragment searchHomeFragment);

    void inject(SearchFragment searchFragment);

    void inject(TablesContentProvider tablesContentProvider);

    void inject(DummyFragment dummyFragment);

    void inject(NotificationsFragment fragment);

    void inject(YourNotificationsFragment fragment);

    void inject(FavoritesFragment favoriteFragment);

    void inject(MyAccountFragment myAccountFragment);

    void inject(MyAccountPresenter myAccountPresenter);

    void inject(NotificationFragment fragment);

    void inject(FinishCreateAccountFragment finishCreateAccountFragment);

    void inject(FinishCreateAccountPresenter finishCreateAccountPresenter);

    void inject(InputPassword_1_Fragment inputPassword1Fragment);

    void inject(YourInformation_2_Fragment yourInformation_2_fragment);

    void inject(PointsOfInterest_3_Fragment pointsOfInterest_3_fragment);

    void inject(PageFinishCreateAccountFragment pageFinishCreateAccountFragment);

    void inject(CreateAccountActivity createAccountActivity);

    void inject(InputPassword_1_Presenter inputPassword_1_presenter);

    void inject(YourInformation_2_Presenter yourInformation_2_presenter);

    void inject(PointsOfInterest_3_Presenter pointsOfInterest_3_presenter);

    void inject(FinishCreateAccountActivity finishCreateAccountActivity);

    void inject(ProvideLocationFragment fragment);

    void inject(LoginFragment loginFragment);

    void inject(LoginActivity loginActivity);

    void inject(ForgottenPasswordFragment forgottenPasswordFragment);

    void inject(ForgottenPasswordPresenter forgottenPasswordPresenter);

    void inject(YourFavoritesFragment fragment);

    void inject(FavoriteFragment fragment);

    void inject(AlertsSwipePresenter alertsSwipePresenter);

    void inject(AlertsSwipeFragment alertsSwipeFragment);

    void inject(AlertItemFragment alertItemFragment);

    void inject(AlertItemPresenter alertItemPresenter);

    void inject(HistoriesFragment fragment);

    void inject(YourHistoryFragment fragment);

    void inject(AlertPopinFragment alertPopinFragment);

    void inject(com.adyax.srisgp.mvp.web_views.WebViewFragment webViewFragment);

    void inject(WebViewActivity webViewActivity);

    void inject(SearchResultsFragment fragment);

    void inject(SummaryWebViewFragment summaryWebViewFragment);

    void inject(FiltersFragment fragment);

    void inject(AskGeoFragment askGeoFragment);

    void inject(FeedbackFragment feedbackFragment);

    void inject(SearchResultsActivity activity);

    void inject(SearchRequestBuilder builder);

    void inject(AroundMeActivity aroundMeActivity);

    void inject(FindSearchResultsFragment fragment);

    void inject(GetLocationAroundMeFragment fragment);

    void inject(GetCityFragment fragment);

    void inject(DeleteNotificationBroadcastReceiver deleteNotificationBroadcastReceiver);

    void inject(PersonalInformationActivity activity);

    void inject(AlertPopinActivity alertPopinActivity);

    void inject(FeedbackActivity feedbackActivity);

    void inject(ChangeCredentialsActivity activity);

    void inject(ContactFragment contactFragment);

    void inject(PersonalizeActivity activity);

    void inject(AsipMessagingService asipMessagingService);

    void inject(PlusAlertPopupActivity plusAlertPopupActivity);

    void inject(ReportInfoFragment_1 reportInfoFragment_1);

    void inject(ReportActivity_1 reportActivity_1);

    void inject(AskGeoPlusAlertPopupActivity plusAlertPopupActivity);

    void inject(ReportEmailFragment_2 reportEmailFragment_2);

    void inject(HistoryFragment fragment);

    void inject(ReportFindUserFragment_1 reportFindUserFragment_1);

    void inject(ReportFindFicheFragment_1 reportFindFicheFragment_1);

    void inject(ReportFicheEmailFragment_2 reportFicheEmailFragment_2);

    void inject(ReportFinishFragment_3 reportFinishFragment_3);

    void inject(TrackerHelper trackerHelper);

    void inject(GeolocationOnboardingFragment geolocationOnboardingFragment);

    void inject(MaintenanceFragment maintenanceFragment);

    void inject(AskLoginFragment askLoginFragment);

    void inject(EmergencyFragment emergencyFragment_);

    void inject(EmergencyActivity emergencyActivity);

    void inject(AppWidgetListRemoteViewsFactory appWidgetListRemoteViewsFactory);

//    void inject(MaintenanceBaseActivity maintenanceBaseActivity);

}
