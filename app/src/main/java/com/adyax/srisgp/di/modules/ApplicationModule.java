package com.adyax.srisgp.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.repository.SharedPreferencesHelper;
import com.adyax.srisgp.data.tracker.HistoryRecentUpdateHelper;
import com.adyax.srisgp.data.tracker.IHistoryRecentUpdateHelper;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.data.tracker.TrackerAT;
import com.adyax.srisgp.db.ContentResolverHelper;
import com.adyax.srisgp.db.DataBaseMaster;
import com.adyax.srisgp.db.DatabaseHelper;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.ITablesDataBaseProvider;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.GeoHelper;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.AlertsHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.INotificationHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.NotificationHelper;
import com.adyax.srisgp.sound.ISoundPlayer;
import com.adyax.srisgp.sound.SoundPlayer;
import com.adyax.srisgp.utils.FiltersHelper;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    IRepository provideRepository(Context context) {
        return new SharedPreferencesHelper(context);
    }

    @Singleton
    @Provides
    FragmentFactory provideFragmentFactory() {
        return new FragmentFactory();
    }

    @Provides
    @Singleton
    public DatabaseHelper provideDatabaseHelper(Context context) {
        return new DatabaseHelper(context);
    }

    @Provides
    @Singleton
    public ITablesDataBaseProvider provideDataBaseMaster(DatabaseHelper chatDatabaseHelper) {
        return new DataBaseMaster(chatDatabaseHelper);
    }

    @Provides
    @Singleton
    public IDataBaseHelper provideContentResolverHelper(Context context) {
        return new ContentResolverHelper(context);
    }

    @Provides
    @Singleton
    public IAlerts provideAlerts(Context context, IDataBaseHelper dataBaseHelper) {
        return new AlertsHelper(context, dataBaseHelper);
    }

    @Provides
    @Singleton
    FiltersHelper provideFiltersHelper() {
        return new FiltersHelper();
    }

    @Provides
    public IGeoHelper provideGeoHelper(Context context, IRepository repository) {
        return new GeoHelper(context, repository);
    }

    @Provides
    @Singleton
    public ISoundPlayer provideSoundPlayer(Context context) {
        return new SoundPlayer(context);
    }

    @Provides
    @Singleton
    public INotificationHelper provideNotificationHelper(Context context) {
        return NotificationHelper.getInstance(context);
    }

    @Provides
    public IHistoryRecentUpdateHelper provideHistoryUpdateHelper(Context context, IDataBaseHelper dataBaseHelper, INetWorkState netWorkState) {
        return new HistoryRecentUpdateHelper(context, dataBaseHelper, netWorkState);
    }

    @Singleton
    @Provides
    ITracker provideTracker(Context context, IHistoryRecentUpdateHelper historyUpdateHelper, IRepository repository, INetWorkState netWorkState) {
        return new TrackerAT(context, historyUpdateHelper, repository, netWorkState);
    }

}
