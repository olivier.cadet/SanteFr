package com.adyax.srisgp;

import android.app.Application;
import android.content.Context;

import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.di.ApplicationComponent;
import com.adyax.srisgp.di.DaggerApplicationComponent;
import com.adyax.srisgp.di.modules.ApplicationModule;
import com.adyax.srisgp.di.modules.NetworkModule;
import com.adyax.srisgp.di.modules.PresentersModule;
import com.atinternet.tracker.Tracker;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class App extends Application {

    private static final String AF_DEV_KEY = "F7unpMYjNCGckdSyENYmFN";

    private static App sInstance;

    private ApplicationComponent applicationComponent;

    private Tracker tracker;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Init app Kits
        ApplicationUtil.init(this);

        sInstance = this;
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(App.this))
                .networkModule(new NetworkModule(UrlExtras.API_URL))
                .presentersModule(new PresentersModule())
                .build();

        getContentResolver().delete(TablesContentProvider.BMC_ITEM_CONTENT_URI, null, null);
        getContentResolver().delete(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, null, null);
    }

    public static ApplicationComponent getApplicationComponent() {
        return sInstance.applicationComponent;
    }

    public static Context getAppContext() {
        return sInstance;
    }

    public Tracker getTracker() {
        return tracker;
    }
}
