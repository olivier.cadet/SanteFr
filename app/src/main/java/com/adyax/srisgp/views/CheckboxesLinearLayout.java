package com.adyax.srisgp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.IntIdValuePair;

import java.util.ArrayList;

/**
 * Created by Kirill on 21.10.16.
 */

public class CheckboxesLinearLayout extends LinearLayout implements CompoundButton.OnCheckedChangeListener {

    private boolean defaultState = false;
    private boolean isCollapsed = true;
    private ArrayList<IntIdValuePair> items = new ArrayList<>();

    private CheckboxListener checkboxListener;

    public CheckboxesLinearLayout(Context context) {
        super(context);
    }

    public CheckboxesLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckboxesLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCheckboxListener(CheckboxListener checkboxListener) {
        this.checkboxListener = checkboxListener;
    }

    public void setItems(ArrayList<IntIdValuePair> items) {
        this.items.clear();
        this.items.addAll(items);
        fillLayout();
    }

    public void setDefaultState(boolean defaultState) {
        this.defaultState = defaultState;
    }

    public ArrayList<IntIdValuePair> getItems() {
        return items;
    }

    public void fillLayout() {
        removeAllViews();

        for (int i = 0; i < items.size(); i++) {

            if (isCollapsed && i > 4) {
                break;
            }

            CompoundButton compoundButton = (CompoundButton) LayoutInflater.from(getContext())
                    .inflate(R.layout.item_filter_switch, null, true);

            LayoutParams compoundButtonLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            compoundButton.setLayoutParams(compoundButtonLayoutParams);

            fillCompoundButton(compoundButton, items.get(i));
            addView(compoundButton);
            addDivider();
        }

        if (isCollapsed && items.size() > 5) {
            addMoreButton();
        }
    }

    private void addMoreButton() {
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.default_margin_x0_5);
        layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.default_margin_x0_5);

        TextView textView = (TextView) LayoutInflater.from(getContext())
                .inflate(R.layout.item_filter_more, null, true);
        textView.setLayoutParams(layoutParams);

        addView(textView);

        textView.setOnClickListener(v -> openLayout());
    }

    private void openLayout() {
        isCollapsed = false;
        fillLayout();
    }

    private void addDivider() {
        View divider = new View(getContext());
        int dividerHeight = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        divider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, dividerHeight));
        divider.setBackgroundColor(getResources().getColor(R.color.filters_divider_color));
        addView(divider);
    }

    private void fillCompoundButton(CompoundButton compoundButton, IntIdValuePair item) {
        if (compoundButton != null) {
            compoundButton.setTag(item);

            compoundButton.setText(item.value);
            compoundButton.setChecked(defaultState);

            compoundButton.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (checkboxListener != null) {
            checkboxListener.onCheckboxChanged(isChecked, (IntIdValuePair) buttonView.getTag());
        }
    }

    public interface CheckboxListener {
        void onCheckboxChanged(boolean isChecked, IntIdValuePair item);
    }
}
