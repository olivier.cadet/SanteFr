package com.adyax.srisgp.views;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHuge;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHugeItem;
import com.adyax.srisgp.mvp.around_me.SearchFilterListener;

import java.util.ArrayList;

/**
 * Created by Kirill on 21.10.16.
 */

public class FiltersLinearLayout extends LinearLayout implements CompoundButton.OnCheckedChangeListener {
    private boolean isSingleChoice = false;

    private SearchFilterHuge filter;
    private SearchFilter currentFilter;

    private SearchFilterListener searchFilterListener;

    private boolean isCollapsed = true;

    public FiltersLinearLayout(Context context) {
        super(context);
    }

    public FiltersLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FiltersLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSearchFilterListener(SearchFilterListener searchFilterListener) {
        this.searchFilterListener = searchFilterListener;
    }

    public void setIsSingleChoise(boolean isSingleChoice) {
        this.isSingleChoice = isSingleChoice;
    }

    public void clearAll() {
        clearAllExceptView(null);
    }

    private void clearAllExceptView(View exceptView) {
        clearAllExceptView(this, exceptView);
    }

    private void clearAllExceptView(ViewGroup parent, View exceptView) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View childView = parent.getChildAt(i);
            if (childView instanceof ViewGroup) {
                clearAllExceptView((ViewGroup) childView, exceptView);
            } else {
                if (childView instanceof CompoundButton && childView != exceptView)
                    ((CompoundButton) childView).setChecked(false);
            }
        }
    }

    public void resetValues(ViewGroup parent) {
        View childView;
        for (int i = 0; i < parent.getChildCount(); i++) {
            childView = parent.getChildAt(i);
            if (childView instanceof ViewGroup) {
                resetValues((ViewGroup) childView);
            } else if (childView instanceof CompoundButton) {
                ((CompoundButton) childView).setChecked(false);
            }
        }
    }

    public void setFilter(SearchFilterHuge filter, SearchFilter currentFilter) {
        this.filter = filter;
        this.currentFilter = currentFilter;

        removeAllViews();

        SearchFilterHuge.FilterType filterType = filter.getFilterType();

        for (int i = 0; i < filter.items.length; i++) {
            SearchFilterHugeItem item = filter.items[i];
            CompoundButton compoundButton = null;

            if (filterType != null) {
                switch (filterType) {
                    case RADIO_BUTTONS:
                        if ((i + 1) % 2 == 0 || ((i + 1) % 2 == 1 && i + 1 == filter.items.length)) {
                            LinearLayout.LayoutParams linearLayoutParams = new LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            LinearLayout linearLayout = new LinearLayout(getContext());
                            linearLayout.setPadding(getResources().getDimensionPixelSize(R.dimen.default_margin),
                                    0, getResources().getDimensionPixelSize(R.dimen.default_margin), 0);
                            linearLayout.setLayoutParams(linearLayoutParams);
                            linearLayout.setOrientation(HORIZONTAL);
                            linearLayout.setGravity(Gravity.CENTER_VERTICAL);

                            compoundButton = (CompoundButton) LayoutInflater.from(getContext())
                                    .inflate(R.layout.item_filter_radio_button, null, true);
                            LinearLayout.LayoutParams compoundButtonLayoutParams = new LayoutParams(0,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                            compoundButton.setLayoutParams(compoundButtonLayoutParams);

                            linearLayout.addView(compoundButton);
                            fillCompoundButton(compoundButton, (i + 1) % 2 == 1 ? item : filter.items[i - 1], currentFilter);

                            if ((i + 1) % 2 == 0) {
                                compoundButton = (CompoundButton) LayoutInflater.from(getContext())
                                        .inflate(R.layout.item_filter_radio_button, null, true);
                                compoundButtonLayoutParams = new LayoutParams(0,
                                        ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                                compoundButtonLayoutParams.leftMargin = getResources().getDimensionPixelSize(R.dimen.default_margin);
                                compoundButton.setLayoutParams(compoundButtonLayoutParams);

                                linearLayout.addView(compoundButton);
                                fillCompoundButton(compoundButton, item, currentFilter);
                            }

                            addView(linearLayout);

                            if (i != filter.items.length - 1 || !filter.isBottomMargin()) {
                                addDivider();
                            }
                        }
                        break;

                    case SWITCHES:
                    case CHECKBOXES:
                        if (isCollapsed && i > 4) {
                            break;
                        }

                        compoundButton = (CompoundButton) LayoutInflater.from(getContext())
                                .inflate(R.layout.item_filter_switch, null, true);

                        LinearLayout.LayoutParams compoundButtonLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        compoundButton.setLayoutParams(compoundButtonLayoutParams);

                        fillCompoundButton(compoundButton, item, currentFilter);
                        addView(compoundButton);

                        if (i != filter.items.length - 1 || !filter.isBottomMargin()) {
                            addDivider();
                        }
                        break;
                }
            }
        }

        if ((filterType == SearchFilterHuge.FilterType.SWITCHES || filterType == SearchFilterHuge.FilterType.CHECKBOXES) &&
                filter.items.length > 5) {
            addFilterMore();
        }

        if (filter.singular == 1)
            setIsSingleChoise(true);
        else
            setIsSingleChoise(false);
    }

    private void addFilterMore() {
        LinearLayout.LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        layoutParams.topMargin = (int) getResources().getDimension(R.dimen.default_margin_x0_5);
        layoutParams.bottomMargin = (int) getResources().getDimension(R.dimen.default_margin_x0_5);

        TextView textView = (TextView) LayoutInflater.from(getContext())
                .inflate(R.layout.item_filter_more, null, true);
        textView.setLayoutParams(layoutParams);

        textView.setText(isCollapsed ? R.string.filters_more : R.string.filters_less);

        addView(textView);

        textView.setOnClickListener(v -> {
            SearchFilter searchFilter = getSearchFilter();
            isCollapsed = !isCollapsed;
            setFilter(filter, searchFilter);
        });
    }

    private void addDivider() {
        View divider = new View(getContext());
        int dividerHeight = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
        divider.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, dividerHeight));
        divider.setBackgroundColor(getResources().getColor(R.color.filters_divider_color));
        addView(divider);
    }

    private void fillCompoundButton(CompoundButton compoundButton, SearchFilterHugeItem item, SearchFilter currentFilter) {
        if (compoundButton != null) {
            compoundButton.setTag(item);

            //set text
            String text = item.label;

            if (item.count > 0)
                text += String.format(" (%d)", item.count);

//            if (BuildConfig.DEBUG)
//                text += String.format(" (value: %s)", item.value);

            if (item.count > 0) {
                Spannable spannable = new SpannableString(text);
                int start = item.label.length() + 1;
                int end = start + Integer.toString(item.count).length() + 2;
                spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.textGray)),
                        start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                compoundButton.setText(spannable);
            } else {
                compoundButton.setText(text);
            }

            //set checked
            if ((currentFilter == null && item.checked == 1) ||
                    (currentFilter != null && currentFilter.getValue().contains(item.value)))
                compoundButton.setChecked(true);

            compoundButton.setOnCheckedChangeListener(this);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked && isSingleChoice)
            clearAllExceptView(buttonView);

        if (searchFilterListener != null)
            searchFilterListener.onFilterChanged(getSearchFilter());
    }

    private SearchFilter getSearchFilter() {
        return new SearchFilter(filter.param, getValues(this));
    }

    private ArrayList<String> getValues(ViewGroup parent) {
        ArrayList<String> values = new ArrayList<>();
        for (int i = 0; i < parent.getChildCount(); i++) {
            View childView = parent.getChildAt(i);
            if (childView instanceof ViewGroup) {
                values.addAll(getValues((ViewGroup) childView));
            } else {
                if (childView instanceof CompoundButton && ((CompoundButton) childView).isChecked()) {
                    if (childView.getTag() instanceof SearchFilterHugeItem)
                        values.add(((SearchFilterHugeItem) childView.getTag()).value);
                }
            }
        }

        //add last items if filter is collapsed
        if (isCollapsed && currentFilter != null && !currentFilter.getValue().isEmpty()) {
            for (int i = 5; i < filter.items.length; i++) {
                if (currentFilter.getValue().contains(filter.items[i].value))
                    values.add(filter.items[i].value);
            }
        }

        return values;
    }
}
