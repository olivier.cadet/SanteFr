package com.adyax.srisgp.views;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Kirill on 21.10.16.
 */

public class DiselectableRadioButton extends androidx.appcompat.widget.AppCompatRadioButton {
    public DiselectableRadioButton(Context context) {
        super(context);
    }

    public DiselectableRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DiselectableRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }
}
