package com.adyax.srisgp.views;

import android.content.Context;
import android.util.AttributeSet;

import com.twitter.sdk.android.core.identity.TwitterLoginButton;

/**
 * Created by Kirill on 15.11.16.
 */

public class CustomTwitterLoginButton extends TwitterLoginButton {
    public CustomTwitterLoginButton(Context context) {
        super(context);
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomTwitterLoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
//        if (isInEditMode()) {
//            return;
//        }

        String androidNS = "http://schemas.android.com/apk/res/android";

        setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        setBackgroundResource(attrs.getAttributeResourceValue(androidNS, "src", com.twitter.sdk.android.core.R.drawable.tw__ic_logo_default));
        setText("");
        setTextSize(20);
        setPadding(0, 0, 0, 0);
    }
}
