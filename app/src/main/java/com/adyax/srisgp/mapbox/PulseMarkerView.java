package com.adyax.srisgp.mapbox;

import com.mapbox.mapboxsdk.annotations.BaseMarkerViewOptions;
import com.mapbox.mapboxsdk.annotations.MarkerView;

public class PulseMarkerView extends MarkerView {

  public PulseMarkerView(BaseMarkerViewOptions baseMarkerViewOptions) {
    super(baseMarkerViewOptions);
  }
}