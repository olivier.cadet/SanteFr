package com.adyax.srisgp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by anton.kobylianskiy on 10/31/16.
 */

public class PermissionUtils {

    private PermissionUtils() {
    }

    public static void requestLocationPermissions(Fragment fragment, int requestCode) {
        fragment.requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                requestCode);
    }

    public static boolean hasPermissionsForRequestingLocation(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isProviderEnabled(Context context) {
        LocationManager service = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        return service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public static boolean isGPSEnabled(Context context) {
//        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        boolean isEnabled = false;
//        try {
//            isEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        } catch (Exception ex) {
//        }
//        return isEnabled;
        return isProviderEnabled(context);
    }

    public static boolean isNotAskAgainLocationPermission(Activity activity) {
        return !ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
    }
}
