package com.adyax.srisgp.utils;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.recyclerview.widget.SortedList;

import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHuge;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHugeItem;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Kirill on 20.10.16.
 */

public class FiltersHelper implements Parcelable, Serializable {

    @SerializedName("aroundFilters")
    private ArrayList<SearchFilterHuge> aroundFilters = new ArrayList<>();

    @SerializedName("infoFilters")
    private ArrayList<SearchFilterHuge> infoFilters = new ArrayList<>();

    @SerializedName("findFilters")
    private ArrayList<SearchFilterHuge> findFilters = new ArrayList<>();

    @SerializedName("aroundSearchFilters")
    private ArrayList<SearchFilter> aroundSearchFilters = new ArrayList<>();

    @SerializedName("infoSearchFilters")
    private ArrayList<SearchFilter> infoSearchFilters = new ArrayList<>();

    @SerializedName("findSearchFilters")
    private ArrayList<SearchFilter> findSearchFilters = new ArrayList<>();

    public FiltersHelper() {

    }

    public void init(FiltersHelper filtersHelper) {
        aroundFilters = filtersHelper.aroundFilters;
        infoFilters = filtersHelper.infoFilters;
        findFilters = filtersHelper.findFilters;
        aroundSearchFilters = filtersHelper.aroundSearchFilters;
        infoSearchFilters = filtersHelper.infoSearchFilters;
        findSearchFilters = filtersHelper.findSearchFilters;
    }

    public ArrayList<SearchFilter> getDefaultState(SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = new ArrayList<>();
        for (SearchFilterHuge filterHuge : getFilters(searchType)) {
            if (!filterHuge.getValues().isEmpty())
                searchFilters.add(new SearchFilter(filterHuge.param, new ArrayList<>()));
        }
        return searchFilters;
    }

    public boolean isChanged(ArrayList<SearchFilter> currentFilters, SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = getFiltersForSearch(searchType);

        if (searchFilters == null) {
            return true;
        }

        if (searchFilters.size() != currentFilters.size()) {
            return true;
        }

        for (SearchFilter filter : searchFilters) {
            SearchFilter item = findFilterByParam(currentFilters, filter.getParam());
            if (item == null) {
                return true;
            } else {
                if (filter.getValue().size() != item.getValue().size())
                    return true;

                for (String s : filter.getValue())
                    if (!item.getValue().contains(s))
                        return true;
            }
        }

        return false;
    }

    public ArrayList<SearchFilterHuge> getFilters(SearchType searchType) {
        switch (searchType) {
            case around:
                return aroundFilters;

            case find:
                return findFilters;

            case info:
                return infoFilters;
        }

        return null;
    }

    public ArrayList<SearchFilter> getFiltersForSearch(SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = new ArrayList<>();
        switch (searchType) {
            case around:
                searchFilters = aroundSearchFilters;
                break;

            case find:
                searchFilters = findSearchFilters;//+
                break;

            case info:
                searchFilters = infoSearchFilters;
                break;
        }

        return getOnlyActiveFiltersForSearch(searchFilters);
    }

    public ArrayList<SearchFilter> getSearchFilters(SearchType searchType) {
        switch (searchType) {
            case around:
                return aroundSearchFilters;

            case find:
                return findSearchFilters;

            case info:
                return infoSearchFilters;
        }

        return null;
    }

    public void setSearchFilters(ArrayList<SearchFilter> newSearchFilters, SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = getSearchFilters(searchType);
        if (searchFilters != null) {
            for (SearchFilter newSearchFilter : newSearchFilters) {
                SearchFilter searchFilter = findFilterByParam(searchFilters, newSearchFilter.getParam());
                if (searchFilter != null) {
                    searchFilters.remove(searchFilter);
                }
                searchFilters.add(newSearchFilter);
            }
        }
    }

    public void clearSearchFilters(SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = null;
        switch (searchType) {
            case around:
                searchFilters = aroundSearchFilters;
                break;

            case find:
                searchFilters = findSearchFilters;
                break;

            case info:
                searchFilters = infoSearchFilters;
                break;
        }
        if (searchFilters != null)
            searchFilters.clear();
    }

    private ArrayList<SearchFilter> getOnlyActiveFiltersForSearch(ArrayList<SearchFilter> filters) {
        ArrayList<SearchFilter> temp = new ArrayList<>();
        for (SearchFilter searchFilter : filters) {
            if (!searchFilter.getValue().isEmpty())
                temp.add(searchFilter);
        }
        return temp;
    }

    public void addFilter(SearchFilter searchFilter, SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = getSearchFilters(searchType);
        if (searchFilters != null) {
            SearchFilter currentFilter = findFilterByParam(searchFilters, searchFilter.getParam());
            if (currentFilter != null)
                searchFilters.remove(currentFilter);
            searchFilters.add(searchFilter);
        }
    }

    public void removeFilter(String param, SearchType searchType) {
        ArrayList<SearchFilter> searchFilters = getSearchFilters(searchType);
        if (searchFilters != null) {
            SearchFilter currentFilter = findFilterByParam(searchFilters, param);
            if (currentFilter != null) {
                searchFilters.remove(currentFilter);
                searchFilters.add(new SearchFilter(param, new ArrayList<>()));
            }
        }
    }

    public void setFilters(ArrayList<SearchFilterHuge> filters, SearchType searchType) {
        switch (searchType) {
            case around:
                aroundFilters = filters;
                break;

            case find:
                findFilters = filters;
                break;

            case info:
                infoFilters = filters;
                break;
        }

        ArrayList<SearchFilter> searchFilters = getSearchFilters(searchType);
        if (searchFilters != null) {
            for (SearchFilterHuge filterHuge : filters) {
                if (findFilterByParam(searchFilters, filterHuge.param) == null) {
                    ArrayList<String> values = new ArrayList<>();
                    for (SearchFilterHugeItem item : filterHuge.items) {
                        if (item.checked == 1) {
                            values.add(item.value);
                        }
                    }

                    getSearchFilters(searchType).add(new SearchFilter(filterHuge.param, values));
                }
            }
        }
    }

    private SearchFilter findFilterByParam(ArrayList<SearchFilter> searchFilters, String param) {
        for (SearchFilter searchFilter : searchFilters) {
            if (searchFilter.getParam().equals(param))
                return searchFilter;
        }
        return null;
    }

    public boolean isEmpty(SearchType searchType) {
        ArrayList<SearchFilterHuge> filters = getFilters(searchType);
        return filters == null || filters.isEmpty();
    }

    protected FiltersHelper(Parcel in) {
        aroundFilters = in.createTypedArrayList(SearchFilterHuge.CREATOR);
        infoFilters = in.createTypedArrayList(SearchFilterHuge.CREATOR);
        findFilters = in.createTypedArrayList(SearchFilterHuge.CREATOR);
        aroundSearchFilters = in.createTypedArrayList(SearchFilter.CREATOR);
        infoSearchFilters = in.createTypedArrayList(SearchFilter.CREATOR);
        findSearchFilters = in.createTypedArrayList(SearchFilter.CREATOR);
    }

    public static final Creator<FiltersHelper> CREATOR = new Creator<FiltersHelper>() {
        @Override
        public FiltersHelper createFromParcel(Parcel in) {
            return new FiltersHelper(in);
        }

        @Override
        public FiltersHelper[] newArray(int size) {
            return new FiltersHelper[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(aroundFilters);
        parcel.writeTypedList(infoFilters);
        parcel.writeTypedList(findFilters);
        parcel.writeTypedList(aroundSearchFilters);
        parcel.writeTypedList(infoSearchFilters);
        parcel.writeTypedList(findSearchFilters);
    }

    @Override
    public boolean equals(Object obj) {
        FiltersHelper filtersHelper=(FiltersHelper)obj;
        if(!Utils.isEqualsArray(aroundFilters, filtersHelper.aroundFilters)){
            return false;
        }
        if(!Utils.isEqualsArray(infoFilters, filtersHelper.infoFilters)){
            return false;
        }
        if(!Utils.isEqualsArray(findFilters, filtersHelper.findFilters)){
            return false;
        }
        if(!Utils.isEqualsArray(aroundSearchFilters, filtersHelper.aroundSearchFilters)){
            return false;
        }
        if(!Utils.isEqualsArray(infoSearchFilters, filtersHelper.infoSearchFilters)){
            return false;
        }
        if(!Utils.isEqualsArray(findSearchFilters, filtersHelper.findSearchFilters)){
            return false;
        }
        return true;
    }
}
