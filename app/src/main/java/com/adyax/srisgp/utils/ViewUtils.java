package com.adyax.srisgp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.appcompat.app.AlertDialog;

import com.adyax.srisgp.R;
import com.google.android.material.tabs.TabLayout;

/**
 * Created by anton.kobylianskiy on 10/21/16.
 */

public class ViewUtils {

    public static void disableTabLayout(Context context, TabLayout tabLayout) {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }

        tabLayout.setBackgroundResource(R.color.disabledTabLayoutBackground);
        tabLayout.setSelectedTabIndicatorHeight(0);
    }

    public static void enableTabLayout(Context context, TabLayout tabLayout) {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        tabStrip.setEnabled(true);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(true);
        }

        TypedArray a = context.obtainStyledAttributes(null, R.styleable.TabLayout,
                0, R.style.Widget_Design_TabLayout);

        tabLayout.setSelectedTabIndicatorHeight(
                a.getDimensionPixelSize(R.styleable.TabLayout_tabIndicatorHeight, 0));
        a.recycle();
        tabLayout.setBackgroundResource(R.color.lightGray);
    }

    public static void showAppsDialog(Context context, String iosLink, String androidLink) {
        String[] items = {context.getString(R.string.search_results_app_store), context.getString(R.string.search_results_google_play)};
        new AlertDialog.Builder(context)
                .setTitle(R.string.search_results_select_platform)
                .setItems(items, (dialog, which) -> {
                    switch (which) {
                        case 0:
                            Utils.openUrl(iosLink, context);
                            break;
                        case 1:
                            Utils.openUrl(androidLink, context);
                            break;
                    }
                })
                .setNegativeButton(R.string.search_results_cancel, (dialog, which) -> {

                })
                .show();
    }

    public static void closeKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideKeyBoard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity) {
        final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideSoftKeyboard(Activity activity) {
        final View view = activity.getCurrentFocus();
        if (view != null) {
            final InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void setCloseKeyboardAfterTouchNonEditTextView(View view, boolean isRoot) {
        if (view instanceof EditText) {
            view.setOnFocusChangeListener((v, hasFocus) -> {
                if (!hasFocus) {
                    ViewUtils.closeKeyboard(v);
                }
            });
        } else if (view instanceof ViewGroup) {
            boolean newRoot = isRoot;
            if (isRoot && !(view instanceof ScrollView)) {
                view.setClickable(true);
                view.setFocusableInTouchMode(true);
                newRoot = false;
            }
//            if(isRoot&& !(view instanceof ScrollView)){
//                view.setClickable(true);
//                view.setFocusableInTouchMode(true);
//            }
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setCloseKeyboardAfterTouchNonEditTextView(((ViewGroup) view).getChildAt(i), newRoot);
            }
        }
    }
}
