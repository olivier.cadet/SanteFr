package com.adyax.srisgp.utils;

import android.graphics.Matrix;
import android.widget.ImageView;

public class ImageUtils {

    public static void fitImageToWidth(ImageView imageView) {
        final Matrix matrix = imageView.getImageMatrix();
        final float imageWidth = imageView.getDrawable().getIntrinsicWidth();
        final int screenWidth = imageView.getContext().getResources().getDisplayMetrics().widthPixels;
        final float scaleRatio = screenWidth / imageWidth;
        matrix.postScale(scaleRatio, scaleRatio);
        imageView.setImageMatrix(matrix);
    }

    public static void fitImageToHeight(ImageView imageView) {
        final Matrix matrix = imageView.getImageMatrix();
        final float imageHeight = imageView.getDrawable().getIntrinsicHeight();
        final int screenHeight = imageView.getContext().getResources().getDisplayMetrics().heightPixels;
        final float scaleRatio = screenHeight / imageHeight;
        matrix.postScale(scaleRatio, scaleRatio);
        imageView.setImageMatrix(matrix);
    }

}
