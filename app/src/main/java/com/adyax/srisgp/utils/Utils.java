package com.adyax.srisgp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.annotation.DimenRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.TypedValue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Kirill on 30.09.16.
 */

public class Utils {

    public static final String TEL = "tel";

    public static boolean isEmailValid(String email) {
//        if(BuildConfig.DEBUG){
//            if("admin".equals(email)){
//                return true;
//            }
//        }
        boolean isValid = false;//@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$

//        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
//        String expression = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,4}$";
        String expression = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    // 8 characters, one uppercase letter, one number and one special character like
//    (			# Start of group
//            (?=.*\d)		#   must contains one digit from 0-9
//            (?=.*[a-z])		#   must contains one lowercase characters
//            (?=.*[A-Z])		#   must contains one uppercase characters
//            (?=.*[@#$%])		#   must contains one special symbols in the list "@#$%"
//            .		#     match anything with previous condition checking
//    {6,20}	#        length at least 6 characters and maximum of 20
//            )			# End of group
    public static boolean isPasswordValid(String pass) {
        //@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$";
// ^
// (?=.*?[A-Z])
// (?=.*?[a-z])
// (?=.*[!@#$%^&*()_+=-|}{\"?:><,./;'\\[\\]])
// (?=.*?[0-9])
// .{8,}$"
        final String regex = "^" +//      # start-of-string
                "(?=.*[0-9])" +//       # a digit must occur at least once
                "(?=.*[a-z])" +//       # a lower case letter must occur at least once
                "(?=.*[A-Z])" +//       # an upper case letter must occur at least once
//                "(?=.*[!@#$%])"+//  # a special character must occur at least once you can replace with your special characters
//                "(?=.*[@\\$%&#_()=+?»«<>£§€{}.[\\]-])"+
//                "[\\!\\\"\\#\\$\\%\\&\\'\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\>\\=\\?\\@\\[\\]\\{\\}\\\\\\^\\_\\`\\~]"+
                "(?=\\S+$)" +//         # no whitespace allowed in the entire string
                ".{8,}" +//             # anything, at least six places though
                "$";//                 # end-of-string

        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pass);
        if (matcher.matches()) {
            String patternToMatch = "[\\\\!\"#$%&()*+,./:;<=>?@\\[\\]^_{|}~]+";
            Pattern p = Pattern.compile(patternToMatch);
            Matcher m = p.matcher(pass);
            if(!m.find()){
                return false;
            }
            // For case @1tester
//            patternToMatch = "^(?=.*[A-Z])$";
//            p = Pattern.compile(patternToMatch, Pattern.CASE_INSENSITIVE);
//            m = p.matcher(pass);
//            if(!m.matches()){
//                return false;
//            }
            if(!pass.matches("^(?=.*[A-Z]).*$")){
                return false;
            }
            // For case @1TESTER
//            patternToMatch = "^(?=.*[a-z])$";
//            p = Pattern.compile(patternToMatch, Pattern.CASE_INSENSITIVE);
//            m = p.matcher(pass);
//            return m.matches();
            return pass.matches("^(?=.*[a-z]).*$");
        }
        return false;
    }

    public static String getCityName(Context context, Location location) throws IOException {
        if (location != null) {
            Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses.size() > 0) {
                return addresses.get(0).getLocality();
            }
        }
        return "";
    }

    public static void openUrl(String url, Context context) {
        if (url == null || url.isEmpty())
            return;

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    public static String requestCall(String phone, Context context) {
        return requestCall(Uri.fromParts(TEL, phone, null), context);
    }

    public static String requestCall(Uri uri, Context context) {
        try {
            Intent intent = new Intent(Intent.ACTION_DIAL, uri);
            context.startActivity(intent);
        }catch (Exception e){
            return e.getMessage();
        }
        return null;
    }

    public static String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String result = uri.getHost();
        String scheme = uri.getScheme();
        if (!TextUtils.isEmpty(scheme)) {
            result = scheme + "://" + result;
        }
        return result;
    }

    public static String getDateString(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(calendar.getTime());
    }

    public static <T extends Comparable> boolean isEqualsArray(ArrayList<T> cmp1_, ArrayList<T> cmp2_) {
        final int size1 = cmp1_.size();
        final int size2 = cmp2_.size();
        if (size1 == 0 && size2 == 0) {
            return true;
        }
        if (size1 != size2) {
            return false;
        }
        // TODO need to refactoring
        ArrayList<T> cmp1 = new ArrayList<T>(size1);
        ArrayList<T> cmp2 = new ArrayList<T>(size2);
        cmp1.addAll(cmp1_);
        cmp2.addAll(cmp2_);
        Collections.sort(cmp1);
        Collections.sort(cmp2);
        for (int i = 0; i < size1; i++) {
            if (cmp1.get(i).compareTo(cmp2.get(i)) != 0) {
                return false;
            }
        }
        return true;
    }

    public static String sha1Hash(String toHash){
        String hash = null;
        try
        {
            MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
            byte[] bytes = toHash.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();

            // This is ~55x faster than looping and String.formating()
            hash = bytesToHex( bytes );
        }
        catch( NoSuchAlgorithmException e )
        {
            e.printStackTrace();
        }
        catch( UnsupportedEncodingException e )
        {
            e.printStackTrace();
        }
        return hash;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex( byte[] bytes )
    {
        char[] hexChars = new char[ bytes.length * 2 ];
        for( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[ j ] & 0xFF;
            hexChars[ j * 2 ] = hexArray[ v >>> 4 ];
            hexChars[ j * 2 + 1 ] = hexArray[ v & 0x0F ];
        }
        return new String( hexChars );
    }

    public static final String TYPE_MIME_PDF = "application/pdf";

    public static Intent makeIntentForOpenPDF(@NonNull Uri pathUri) {
        String mimeType = URLConnection.guessContentTypeFromName(pathUri.toString());
        if (!TYPE_MIME_PDF.equalsIgnoreCase(mimeType)) {
            return null;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(pathUri, TYPE_MIME_PDF);
        return intent;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Intent viewOnMap(String lat, String lng) {
        return new Intent(Intent.ACTION_VIEW,
//                Uri.parse(String.format("geo:%s,%s", lat, lng)));
                Uri.parse(String.format("google.navigation:q=%s,%s", lat, lng)));

//        return new Intent(android.content.Intent.ACTION_VIEW,
//                Uri.parse(String.format("http://maps.google.com/maps?daddr=%s,%s", lat, lng)));

    }

    public static void sendEMail(Context context, String subject, String mail, String body) {
//        Intent intent = new Intent(Intent.ACTION_SENDTO);
////        intent.setType("message/rfc822");
//        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
//        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{mail});
//        intent.putExtra(Intent.EXTRA_TEXT, body);
////        Intent mailer = Intent.createChooser(intent, null);
        String mailto = "mailto:";//+ mail;//
////                "?cc=" + "alice@example.com" +
//                "&subject=" + Uri.encode(subject) +
//                "&body=" + Uri.encode(body);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse(mailto));
//        intent.putExtra(Intent.EXTRA_EMAIL, mail);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { mail });
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        context.startActivity(intent);
    }

    public static int dp2px(Resources resource, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,   dp,resource.getDisplayMetrics());
    }

    public static int countMatches(String str, String sub) {
        if (isEmpty(str) || isEmpty(sub)) {
            return 0;
        }
        int count = 0;
        int idx = 0;
        while ((idx = str.indexOf(sub, idx)) != -1) {
            count++;
            idx += sub.length();
        }
        return count;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

//    public static Map<String, String> getSegmentsFromUrl(String url) {
//        Map<String, String> result=new HashMap<>();
//        final String[] split=url.split("(\\?)|(&)");
//        if(split!=null&& split.length>1){
//            for(int i=1;i<split.length;i++){
//                final String[] sss=split[i].split("=");
//                if(sss.length==2){
//                    result.put(sss[0], sss[1]);
//                }
//            }
//        }
//        return result;
//    }

    public static Bitmap convertDrawableResToBitmap(Context context, @DrawableRes int drawableId, @DimenRes int widthId, @DimenRes int heightId) {
        Drawable d = context.getResources().getDrawable(drawableId);

        if (d instanceof BitmapDrawable) {
            return ((BitmapDrawable) d).getBitmap();
        }

        if (d instanceof GradientDrawable) {
            GradientDrawable g = (GradientDrawable) d;

            int w = d.getIntrinsicWidth() > 0 ? d.getIntrinsicWidth() : context.getResources().getDimensionPixelSize(widthId);
            int h = d.getIntrinsicHeight() > 0 ? d.getIntrinsicHeight() : context.getResources().getDimensionPixelSize(heightId);

            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            g.setBounds(0, 0, w, h);
            g.setStroke(1, Color.BLACK);
            g.setFilterBitmap(true);
            g.draw(canvas);
            return bitmap;
        }

        Bitmap bit = BitmapFactory.decodeResource(context.getResources(), drawableId);
        return bit.copy(Bitmap.Config.ARGB_8888, true);
    }

    public static Bitmap drawTextToBitmap(Context context, Bitmap bitmap, String text) {
        Resources resources = context.getResources();
        float scale = resources.getDisplayMetrics().density;

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
//        paint.setColor(Color.rgb(61, 61, 61));
        paint.setColor(Color.WHITE);
        // text size in pixels
        paint.setTextSize((int) (14 * scale));
        // text shadow
//        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width())/2;
        int y = (bitmap.getHeight() + bounds.height())/2;

        canvas.drawText(text, x, y, paint);

        return bitmap;
    }

}
