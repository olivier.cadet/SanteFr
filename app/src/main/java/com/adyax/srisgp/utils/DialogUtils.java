package com.adyax.srisgp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;

/**
 * Created by SUVOROV on 6/26/17.
 */

public class DialogUtils {

    public static void showBadRegionDialog(Context context, ConfigurationResponse config, DialogInterface.OnClickListener onClickListener) {
        new AlertDialog.Builder(context)
                .setMessage(config.getSearch().getAutocomplete_inactive_region())
                .setCancelable(true)
                .setPositiveButton(R.string.yes, onClickListener)
                .create()
                .show();
    }

    public static void showResultsNotFound(Context context) {
        new androidx.appcompat.app.AlertDialog.Builder(context)
                .setView(R.layout.dialog_results_not_found)
                .setPositiveButton(R.string.yes, (dialog, which) -> {

                })
                .show();
    }

    public static void showBadSSO(Context context, String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.yes, (dialog, which) -> {

                })
                .show();
    }

}
