package com.adyax.srisgp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by anton.kobylianskiy on 1/31/17.
 */

public class NetworkStateReceiver extends BroadcastReceiver {
    private Set<NetworkStateListener> listeners = new HashSet<>();
    private Boolean connected;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent == null || intent.getExtras() == null) {
            return;
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
            connected = false;
        }

        notifyListeners();
    }

    public void addNetworkStateListener(NetworkStateListener listener) {
        listeners.add(listener);
    }

    public void removeNetworkStateListener(NetworkStateListener listener) {
        listeners.remove(listener);
    }

    private void notifyListeners() {
        for (NetworkStateListener listener : listeners) {
            notifyListener(listener);
        }
    }

    private void notifyListener(NetworkStateListener listener) {
        if (connected == null || listener == null) {
            return;
        }

        if (connected) {
            listener.onNetworkAvailable();
        } else {
            listener.onNetworkUnavailable();
        }
    }

    public interface NetworkStateListener {
        void onNetworkAvailable();
        void onNetworkUnavailable();
    }
}
