package com.adyax.srisgp.sound;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;


import com.adyax.srisgp.R;

import java.io.IOException;
import java.util.List;

import timber.log.Timber;

/**
 * Created by SUVOROV on 8/17/16.
 */
public class SoundPlayer implements ISoundPlayer {

//    private SharedPreferencesHelper sharedPreferencesHelper;
    private SoundQueue soundQueue = new SoundQueue();
    private MediaPlayer mediaPlayer = getMediaPlayer();
    private static String lastContext;
    private Context context;
//    private NotificationsSettingsModel notifications;


    public SoundPlayer(Context context/*, SharedPreferencesHelper sharedPreferencesHelper*/) {
        this.context = context;
//        this.sharedPreferencesHelper = sharedPreferencesHelper;
    }

    @Override
    public void playSound(SoundEnum soundEnum) {
        try {
            if (mediaPlayer == null) {
                mediaPlayer = getMediaPlayer();
            }
            ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(500);

//            if(soundEnum == EVENT_MESSAGE_IS_RECIEVED || soundEnum == EVENT_USER_IS_MATCHED) {
//
//                notifications = sharedPreferencesHelper.getNotificationSettings();
//                String needSound = notifications.getMatchAppSound();
//
//                if (soundEnum == EVENT_USER_IS_MATCHED) {
//                    if ("matchCelebration.aif".equals(needSound)) {
//                        soundEnum = MATCH_CELEBRATION;
//                    } else if ("matchSwingin.aif".equals(needSound)) {
//                        soundEnum = MATCH_SWINGING;
//                    } else if ("matchWoohoo.aif".equals(needSound)) {
//                        soundEnum = MATCH_WOOHOO;
//                    } else if ("matchRetro.aif".equals(needSound)) {
//                        soundEnum = MATCH_RETRO;
//                    } else if ("matchPop.aif".equals(needSound)) {
//                        soundEnum = MATCH_POP;
//                    }
//                }
//
//                if (soundEnum == EVENT_MESSAGE_IS_RECIEVED) {
//                    needSound = notifications.getMessageAppSound();
//
//                    if ("messagePop.aif".equals(needSound)) {
//                        soundEnum = MESSAGE_POP;
//                    } else if ("messageTypewriter.aif".equals(needSound)) {
//                        soundEnum = MESSAGE_TYPEWRITER;
//                    } else if ("messageKnock.aif".equals(needSound)) {
//                        soundEnum = MESSAGE_KNOCK;
//                    } else if ("messageRetro.aif".equals(needSound)) {
//                        soundEnum = MESSAGE_RETRO;
//                    } else if ("messageHi.aif".equals(needSound)) {
//                        soundEnum = MESSAGE_HI;
//                    }
//                }
//            }

            int rawResource = R.raw.demo_pop;//getRawResource(soundEnum);
            if (-1 != rawResource) {
                setRawResource(rawResource);
                mediaPlayer.prepare();
            }
        } catch (IOException | IllegalStateException | NullPointerException e) {
            Timber.e(e);
        }

        mediaPlayer.setOnPreparedListener(mp -> mp.start());

        mediaPlayer.setOnCompletionListener(mp -> {
            try {
                mediaPlayer.reset();
                soundQueue.removeCurrent();
                playQueue();
            } catch (Exception e) {
//                    MyLog.getMyLog().e(e);
            }
        });
    }

//    private int getRawResource(int type) {
//        switch (type) {
//            case MATCH_CELEBRATION:
//                return R.raw.match_celebration;
//            case MATCH_POP:
//                return R.raw.match_pop;
//            case MATCH_RETRO:
//                return R.raw.match_retro;
//            case MATCH_SWINGING:
//                return R.raw.match_swingin;
//            case MATCH_WOOHOO:
//                return R.raw.match_woohoo;
//            case MESSAGE_HI:
//                return R.raw.message_hi;
//            case MESSAGE_KNOCK:
//                return R.raw.message_knock;
//            case MESSAGE_POP:
//                return R.raw.message_pop;
//            case MESSAGE_RETRO:
//                return R.raw.message_retro;
//            case MESSAGE_TYPEWRITER:
//                return R.raw.message_typewriter;
//            default:
//                return -1;
//        }
//    }

    private MediaPlayer getMediaPlayer() {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//        removeMediaPlayerErrorFromLog(mediaPlayer);
        return mediaPlayer;
    }

    private boolean setRawResource(int rawResource) throws IOException {
        AssetFileDescriptor afd = context.getResources().openRawResourceFd(rawResource);
        if (afd == null) return true;
        mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        return false;
    }

    public void playEmoji(Context context, int unicode, int bundleId) { //TODO: add method playEmojiAsync()
//        Emoji emoji = ETApp.getInstance().getBundleManager().getEmoji(bundleId, unicode);
//        addToQueue(context.toString(), Collections.singletonList(emoji));
    }

    public void playQueue() {
        if (soundQueue.isEmpty()) {
            return;
        }

        final ItemSound itemSound = soundQueue.getCurrent();
        try {
            if (mediaPlayer == null) {
                mediaPlayer = getMediaPlayer();
//                MyLog.getMyLog().w("mediaPlayer was null");
            }
            if (itemSound == null) {
//                if (setDefaultSound())
                return;
            } else {
                final String sound = itemSound.getSound();
                if (sound == null) {
//                    if (setDefaultSound())
                    return;
                } else
                    mediaPlayer.setDataSource(sound);
            }
            mediaPlayer.prepare();
        } catch (IOException | IllegalStateException | NullPointerException e) {
//            MyLog.getMyLog().e(e);
            return;
        }
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                    mediaPlayer.reset();
                    soundQueue.removeCurrent();
                    playQueue();
                } catch (Exception e) {
//                    MyLog.getMyLog().e(e);
                }
            }
        });
    }


    public void addToQueue(String context, List<ItemSound> itemSounds) {
        this.lastContext = context;
        if (itemSounds == null || itemSounds.isEmpty())
            return;

        for (ItemSound itemSound : itemSounds) {
            if (itemSound != null)
                itemSound.setContext(context);
        }

        interruptQueue(null);

        soundQueue.addAll(itemSounds);

        playQueue();
    }

    public boolean interruptQueue(String context) {
        if (context == null) {
            stopPlay(null);
        }
        return soundQueue.clear(context);
    }

    public void stopPlay(String context) {
        if (mediaPlayer != null && mediaPlayer.isPlaying() /*&& (context == null || (context != null && TextUtils.equals(context, lastContext)))*/) {
            mediaPlayer.stop();
            mediaPlayer.reset();
//            MyLog.getMyLog().e(String.format("**STOP** %s", context==null?"0":"1"));
        }
    }

    public void release(String context) {
        if (interruptQueue(context)) {
            interruptQueue(null);
            if (mediaPlayer != null) {
                mediaPlayer.release();
                mediaPlayer = null;
            }
//            MyLog.getMyLog().e(String.format("**STOP** RELEASE"));
        }
//        soundPlayer = null;
    }
}
