package com.adyax.srisgp.sound;

/**
 * Created by SUVOROV on 8/17/16.
 */

public interface ISoundPlayer {
    void playSound(SoundEnum soundEnum);
}
