package com.adyax.srisgp.sound;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by SUVOROV on 8/17/16.
 */
public class SoundQueue {

    private List<ItemSound> queue = Collections.synchronizedList(new ArrayList<ItemSound>());

    public void removeCurrent() {
        queue.remove(0);
    }

    public void add(ItemSound itemSound) {
        queue.add(itemSound);
    }

    public void addAll(List<ItemSound> itemSounds) {
        queue.addAll(itemSounds);
    }

    public ItemSound getCurrent() {
        return queue.get(0);
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public boolean clear(String context) {
        boolean bResult = false;
        if(context!=null){
            boolean bRemove;
            do {
                bRemove=false;
                for (ItemSound itemSound : queue) {
                    if (itemSound ==null || itemSound.getContext().equals(context)) {
                        queue.remove(itemSound);
                        bRemove=true;
                        bResult=true;
                        break;
                    }
                }
            }while(bRemove);
        }else
            queue.clear();
        return bResult;
    }
}
