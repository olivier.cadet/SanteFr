package com.adyax.srisgp.factory;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.feedback.ReportBuilder;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.adyax.srisgp.mvp.around_me.AroundMeFragment;
import com.adyax.srisgp.mvp.around_me.filters.FiltersFragment;
import com.adyax.srisgp.mvp.around_me.get_location.GetLocationAroundMeFragment;
import com.adyax.srisgp.mvp.ask_login.AskLoginArg;
import com.adyax.srisgp.mvp.ask_login.AskLoginFragment;
import com.adyax.srisgp.mvp.authentication.forgotten_password.ForgottenPasswordFragment;
import com.adyax.srisgp.mvp.authentication.forgotten_password.TypeForrgotten;
import com.adyax.srisgp.mvp.authentication.login.LoginActivity;
import com.adyax.srisgp.mvp.authentication.login.LoginFragment;
import com.adyax.srisgp.mvp.configuretext.ConfigureTextFragment;
import com.adyax.srisgp.mvp.contact.ContactFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.FinishCreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.flow.input_password_1.InputPassword_1_Fragment;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.YourInformation_2_Fragment;
import com.adyax.srisgp.mvp.create_account.flow.your_points_of_interest_3.PointsOfInterest_3_Fragment;
import com.adyax.srisgp.mvp.dummy.DummyFragment;
import com.adyax.srisgp.mvp.emergency.EmergencyActivity;
import com.adyax.srisgp.mvp.emergency.EmergencyArg;
import com.adyax.srisgp.mvp.emergency.EmergencyFragment;
import com.adyax.srisgp.mvp.favorite.YourFavoritesFragment;
import com.adyax.srisgp.mvp.feedback.FeedbackActivity;
import com.adyax.srisgp.mvp.feedback.FeedbackFragment;
import com.adyax.srisgp.mvp.history.YourHistoryFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceActivity;
import com.adyax.srisgp.mvp.maintenance.MaintenanceFragment;
import com.adyax.srisgp.mvp.my_account.MyAccountFragment;
import com.adyax.srisgp.mvp.notification.YourNotificationsFragment;
import com.adyax.srisgp.mvp.report.ReportActivity_1;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_email.ReportFicheEmailFragment_2;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_fiche_finish.ReportFinishFragment_3;
import com.adyax.srisgp.mvp.report.report_find.report_find_fiche.report_find_fiche.ReportFindFicheFragment_1;
import com.adyax.srisgp.mvp.report.report_find.report_find_user.ReportFindUserFragment_1;
import com.adyax.srisgp.mvp.report.report_info.report_email.ReportEmailFragment_2;
import com.adyax.srisgp.mvp.report.report_info.report_info_1.ReportInfoFragment_1;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinActivity;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinFragment;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.mvp.web_views.WebViewFragment;
import com.adyax.srisgp.mvp.web_views.summary_web_view.SummaryWebViewFragment;

/**
 * Created by SUVOROV on 9/21/16.
 */

public class FragmentFactory {

    public static final String FORGOTTEN_PASSWORD_FRAGMENT = "ForgottenPasswordFragment";

//    public void startAroundMeActivity(Context context, AroundMeArguments aroundMeArguments) {
//        if (context instanceof FragmentActivity) {
////            ((IViewPager)context).disableAppBar();
////            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
////                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
////                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
////                    .commit();
//            Intent intent = AroundMeActivity.newIntent(context, aroundMeArguments);
//            ((FragmentActivity) context).startActivity(intent);
//            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
//        } else {
//            throw new IllegalArgumentException("");
//        }
//    }

    public Fragment getCurrentFragment(Context context) {
        if (context instanceof FragmentActivity) {
            return ((FragmentActivity) context).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        }
        return null;
    }

    public void startSummaryWebViewFragment(Context context, AnchorResponse anchorResponse) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, SummaryWebViewFragment.newInstance(anchorResponse))
                    .addToBackStack(null)
//                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startWebViewActivity(Context context, WebViewArg webViewArg, int requestCode) {
        if (context instanceof FragmentActivity) {
//            ((IViewPager)context).disableAppBar();
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
//                    .commit();
            Intent intent = WebViewActivity.newIntent(context, webViewArg);
            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else {
            throw new IllegalArgumentException("");
        }
    }

//    public void startWebViewActivityForResult(Context context, WebViewArg webViewArg, int requestCode) {
//        if (context instanceof FragmentActivity) {
//            Intent intent = WebViewActivity.newIntent(context, webViewArg);
//            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
//            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
//        } else {
//            throw new IllegalArgumentException("");
//        }
//    }

//    public void startWebViewActivity(Context context, WebViewArg webViewArg, boolean showFavorite) {
//        if (context instanceof FragmentActivity) {
//            Intent intent = WebViewActivity.newIntent(context, webViewArg, showFavorite);
//            context.startActivity(intent);
//            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
//        } else {
//            throw new IllegalArgumentException("");
//        }
//    }

//    public void startWebViewFragment(Context context, WebViewArg webViewArg) {
//        if (context instanceof FragmentActivity) {
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
////                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, WebViewFragment.newInstance(webViewArg))
////                    .addToBackStack(null)
////                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
//                    .commit();
//        } else
//            throw new IllegalArgumentException("");
//    }

    public void startWebViewFragment(Context context, WebViewArg webViewArg, boolean showFavorite) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, WebViewFragment.newInstance(webViewArg, showFavorite))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startForgottenPasswordFragment(Context context, TypeForrgotten typeCreateAccount) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(typeCreateAccount))
                    .addToBackStack(null)
//                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startForgottenPasswordFragment(Context context, TypeForrgotten typeCreateAccount, String email) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(typeCreateAccount, email))
                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startForgottenPasswordFragmentFinish(Context context) {
        if (context instanceof FragmentActivity) {
            FragmentManager manager = ((FragmentActivity) context).getSupportFragmentManager();
//                    clearBackStack(manager);
            int id = manager.beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgotten.finish_screen))
//                    .addToBackStack(FORGOTTEN_PASSWORD_FRAGMENT)
////                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
                    .commit();
//            ((FragmentActivity) context).runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    clearBackStack(manager, id);
//                }
//            });
        } else {
            throw new IllegalArgumentException("");
        }
    }

    private void clearBackStack(FragmentManager manager, int id) {
        int aaa = manager.getBackStackEntryCount();
        manager.popBackStack(FORGOTTEN_PASSWORD_FRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        if (manager.getBackStackEntryCount() > 0) {
//            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
//            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        }
//        int count = manager.getBackStackEntryCount();
//        for(int i = 0; i < count; ++i) {
//            manager.popBackStackImmediate();
//        }
        aaa = manager.getBackStackEntryCount();
    }

    public void startLoginActivity(Context context) {
        if (context instanceof FragmentActivity) {
//            ((IViewPager)context).disableAppBar();
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
//                    .commit();
            Intent intent = LoginActivity.newIntent(context);
            ((FragmentActivity) context).startActivity(intent);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else
            throw new IllegalArgumentException("");
    }

    public void startLoginFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, LoginFragment.newInstance())
                    .commit();
        } else
            throw new IllegalArgumentException("");
    }

    public void startLoginFragmentAnimate(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, LoginFragment.newInstance())
                    .commit();
        } else
            throw new IllegalArgumentException("");
    }

    public void startAnimateCreateAccount(Context context) {
        if (context instanceof FragmentActivity) {
//            ((IViewPager)context).disableAppBar();
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
//                    .commit();
            Intent intent = FinishCreateAccountActivity.newIntent(context);
            ((FragmentActivity) context).startActivity(intent);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else
            throw new IllegalArgumentException("");
    }

    public void startInputPassword_1_Fragment(Context context) {
//        startAnimateCreateAccount(context);
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, InputPassword_1_Fragment.newInstance())

//                    .replace(R.id.fragment_container, AskGeoFragment.newInstance())

                    .commit();
        } else
            throw new IllegalArgumentException("");
    }

    public void startYourInformation_2_Fragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, YourInformation_2_Fragment.newInstance())
                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startYourInformation_2_Fragment(Context context, String gender, String year) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, YourInformation_2_Fragment.newInstance(gender, year))
                    .addToBackStack(null)
                    .commitAllowingStateLoss();// java.lang.IllegalStateException: Can not perform this action after onSaveInstanceState
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startPointsOfInterest_3_Fragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, PointsOfInterest_3_Fragment.newInstance())
                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startHistoryFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, YourHistoryFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startMyAccountFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, MyAccountFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startFavoriteFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, YourFavoritesFragment.newInstance())
                    .commit();
        } else
            throw new IllegalArgumentException("");
    }

    public void startGetLocationAroundMeFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, GetLocationAroundMeFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startAroundMeFragment(Context context, AroundMeArguments aroundMeArguments) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, AroundMeFragment.newInstance(aroundMeArguments))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startNotificationFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, YourNotificationsFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startDummyFragment(Context context, int resId) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, DummyFragment.newInstance(resId))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startConfigureTextFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, ConfigureTextFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startFeedbackFragment(Context context, String url, String formName) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, FeedbackFragment.newInstance(url, formName))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startFiltersFragment(Context context, SearchType searchType) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, FiltersFragment.getInstance(searchType))
                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

//    public void startPersonalizeFragment(Context context) {
//        if (context instanceof FragmentActivity) {
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.fragment_container, PersonalizeFragment.newInstance(R.string.configure_and_customize_your_notifications))
//                    .addToBackStack(null)
//                    .commit();
//        } else
//            throw new IllegalArgumentException("");
//    }

    public void startAlertPopinFragment(Context context, AlertItem alertItem) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, AlertPopinFragment.newInstance(alertItem))
//                    .addToBackStack(null)
//                    .replace(R.id.fragment_container, ForgottenPasswordFragment.newInstance(TypeForrgottenFragment.last_screen))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startAlertPopinActivity(Context context, AlertItem alertItem, int requestCode) {
        if (context instanceof FragmentActivity) {
//            ((IViewPager)context).disableAppBar();
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
//                    .commit();
            Intent intent = AlertPopinActivity.newIntent(context, alertItem);
            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startFeedbackActivity(Context context, String url, String formName) {
        if (context instanceof FragmentActivity) {
//            ((IViewPager)context).disableAppBar();
//            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
//                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
//                    .replace(R.id.fragment_container, CreateAccountFragment.newInstance())
//                    .commit();
            Intent intent = FeedbackActivity.newIntent(context, url, formName);
            ((FragmentActivity) context).startActivity(intent);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startContactFragment(Context context) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, ContactFragment.newInstance())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReportActivity(Context context, WebViewArg webViewArg) {
        if (context instanceof FragmentActivity) {
            Intent intent = ReportActivity_1.newIntent(context, webViewArg);
            ((FragmentActivity) context).startActivity(intent);
            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReportInfoFragment(Context context, String stringExtra) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportInfoFragment_1.newInstance(R.string.report, stringExtra))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReport2Fragment(Context context, ReportBuilder builder) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportEmailFragment_2.newInstance(R.string.feedback_toolbar_title, builder))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReportFindUserFragment(Context context, WebViewArg webViewArg) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportFindUserFragment_1.newInstance(R.string.report, webViewArg))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReportFindFicheFragment(Context context, WebViewArg webViewArg) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportFindFicheFragment_1.newInstance(R.string.report, webViewArg))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReport5Fragment(Context context, ReportBuilder builder) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportFicheEmailFragment_2.newInstance(R.string.feedback_toolbar_title, builder))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startReport6Fragment(Context context, ReportBuilder builder) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left, R.anim.anim_slide_right_in, R.anim.anim_slide_right_out)
                    .replace(R.id.fragment_container, ReportFinishFragment_3.newInstance(R.string.feedback_toolbar_title, builder))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startMaintenanceFragment(Context context, int resId) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, MaintenanceFragment.newInstance(resId), MaintenanceFragment.class.getCanonicalName())
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startMaintenanceActivity(Context context) {
        if (context instanceof FragmentActivity) {
            Intent intent = MaintenanceActivity.newIntent(context);
            ((FragmentActivity) context).startActivity(intent);
//            ((FragmentActivity) context).overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startAskLoginFragment(Context context, AskLoginArg askLoginArg) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, AskLoginFragment.newInstance(askLoginArg))
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startEmergencyActivity(Context context, EmergencyArg emergencyArg, int requestCode) {
        if (context instanceof FragmentActivity) {
            Intent intent = EmergencyActivity.newIntent(context, emergencyArg);
            ((FragmentActivity) context).startActivityForResult(intent, requestCode);
        } else {
            throw new IllegalArgumentException("");
        }
    }

    public void startEmergencyFragment(Context context, EmergencyArg emergencyArg) {
        if (context instanceof FragmentActivity) {
            ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, EmergencyFragment.newInstance(emergencyArg))
//                    .addToBackStack(null)
                    .commit();
        } else {
            throw new IllegalArgumentException("");
        }
    }
}
