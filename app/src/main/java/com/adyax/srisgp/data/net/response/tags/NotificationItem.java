package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.mvp.notification.StatusNotification;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by SUVOROV on 9/19/16.
 */
@DatabaseTable(tableName = "notification")
public class NotificationItem implements Parcelable, IGetUrl, INewFields {

    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String TITLE = "title";
    public static final String TYPE = "node_type";
    public static final String DESCRIPTION = "description";
    public static final String CREATED = "created";
    public static final String IS_READ = "is_read";
    public static final String STATUS = "status";
    public static final String FAVORITE = "favorite";
    public static final String DATE_ADDED = "date_added";
    public static final String APP_STORE_LINK = "app_store_link";
    public static final String PLAY_STORE_LINK = "play_store_link";

    @SerializedName(NID)
    @DatabaseField(columnName = NID, uniqueCombo = true)
    public long nid;

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(URL)
    @DatabaseField(columnName = URL_DB)
    public String url;

    @SerializedName(TYPE)
    @DatabaseField(columnName = TYPE)
    public NodeType type;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(CREATED)
    @DatabaseField(columnName = CREATED)
    public String created;

    @SerializedName(IS_READ)
    @DatabaseField(columnName = IS_READ)
    public int is_read;

    @SerializedName(FAVORITE)
    @DatabaseField(columnName = FAVORITE)
    public int favorite;

    @SerializedName(PROPOSE)
    @DatabaseField(columnName = PROPOSE_DB)
    public String propose;

    @SerializedName(LINK)
    @DatabaseField(columnName = LINK_DB)
    public String link;

    /* 0 all 1 read 2 unread*/
    @DatabaseField(columnName = STATUS, uniqueCombo = true)
    public int status;


    @SerializedName(DATE_ADDED)
    @DatabaseField(columnName = DATE_ADDED)
    public String date_added;

    @SerializedName(APP_STORE_LINK)
    @DatabaseField(columnName = APP_STORE_LINK)
    public String appStoreLink;

    @SerializedName(PLAY_STORE_LINK)
    @DatabaseField(columnName = PLAY_STORE_LINK)
    public String playStoreLink;

    @SerializedName(PHONE)
    @DatabaseField(columnName = PHONE_DB)
    public String phone;

    public void setStatus(StatusNotification status) {
        this.status = status.getValue();
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return propose;
    }

    public String getDescription() {
        return description;
    }

    public NodeType getNodeType() {
        if (type == null)
            return NodeType.UNKNOWN;
        return type;
    }

    public boolean isRead() {
        return is_read == 1;
    }

    public void setRead(boolean read) {
        is_read = read ? 1 : 0;
    }

    public boolean isFavorite() {
        return favorite == 1;
    }

    public String getTypeStr() {
        if(type==null){
            return NodeType.UNKNOWN.toString();
        }
        return type.name().toUpperCase();
//        return !TextUtils.isEmpty(type) ? type.toUpperCase() : "";
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite ? 1 : 0;
    }

    public NodeType getType() {
        if (type == null)
            return NodeType.UNKNOWN;
        return type;
    }

    public String getContentUrl() {
        return url;
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getCardLink() {
        switch (type){
            case VIDEO:
            case FICHES_INFOS:
                return url;
            case LIENS_EXTERNES:
                if(!TextUtils.isEmpty(link)) {
                    return link;
                }
        }
        return url;
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return nid;
    }

    public String getDateAdded() {
        return date_added;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.nid);
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.description);
        dest.writeString(this.created);
        dest.writeInt(this.is_read);
        dest.writeInt(this.favorite);
        dest.writeString(this.propose);
        dest.writeString(this.link);
        dest.writeInt(this.status);
        dest.writeString(this.date_added);
        dest.writeString(this.appStoreLink);
        dest.writeString(this.playStoreLink);
        dest.writeString(this.phone);
        if(this.xiti==null){
            this.xiti=new Xiti();
        }
        dest.writeParcelable(this.xiti, flags);
    }

    public NotificationItem() {
    }

    protected NotificationItem(Parcel in) {
        this.nid = in.readLong();
        this.title = in.readString();
        this.url = in.readString();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : NodeType.values()[tmpType];
        this.description = in.readString();
        this.created = in.readString();
        this.is_read = in.readInt();
        this.favorite = in.readInt();
        this.propose = in.readString();
        this.link = in.readString();
        this.status = in.readInt();
        this.date_added = in.readString();
        this.appStoreLink = in.readString();
        this.playStoreLink = in.readString();
        this.phone = in.readString();
        this.xiti=in.readParcelable(Xiti.class.getClassLoader());
    }

    public static final Creator<NotificationItem> CREATOR = new Creator<NotificationItem>() {
        @Override
        public NotificationItem createFromParcel(Parcel source) {
            return new NotificationItem(source);
        }

        @Override
        public NotificationItem[] newArray(int size) {
            return new NotificationItem[size];
        }
    };

    public int getIconResource() {
        return IconType.NodeToIcon(this);
    }

    @Override
    public IconType getIconType() {
        return IconType.getIcon(this);
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getApple() {
        return appStoreLink;
    }

    @Override
    public String getGoogle() {
        return playStoreLink;
    }
}
