package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 11/14/16.
 */

public class GetFormCommand extends ServiceCommand implements INewFields {

    @SerializedName(BaseFeedbackCommandCommand.FORM_NAME_FIELD)
    private String formName;
    @SerializedName(URL_OTHER)
    private String url;

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getForm(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formName);
        dest.writeString(this.url);
    }

    public GetFormCommand(String formName, String url) {
        this.formName = formName;
        this.url = url;
    }

    public GetFormCommand() {
    }

    protected GetFormCommand(Parcel in) {
        this.formName = in.readString();
        this.url = in.readString();
    }

    public static final Creator<GetFormCommand> CREATOR = new Creator<GetFormCommand>() {
        @Override
        public GetFormCommand createFromParcel(Parcel source) {
            return new GetFormCommand(source);
        }

        @Override
        public GetFormCommand[] newArray(int size) {
            return new GetFormCommand[size];
        }
    };

    public String getFormName() {
        return formName;
    }


}
