package com.adyax.srisgp.data.tracker;

import android.content.Context;
import android.os.Parcel;
import android.util.Log;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.net.response.tags.xiti.XitiType;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.atinternet.tracker.ATInternet;
import com.atinternet.tracker.Gesture;
import com.atinternet.tracker.Screen;
import com.atinternet.tracker.Tracker;

import java.util.HashMap;

import timber.log.Timber;

/**
 * Created by SUVOROV on 3/7/17.
 */

public class TrackerAT implements ITracker {

    // Title (Screens)
    public static final String HOMEPAGE = "Homepage";
    public static final String S_INFORMER = "S’informer : ";
    public static final String RECHERCHE_AUTOUR_DE_MOI = "Recherche_Autour_de_moi";
    public static final String TROUVER_CARTE = "Trouver carte : ";
    public static final String TROUVER_LISTE = "Trouver liste : ";
    public static final String MES_FAVORIS = "Mes_favoris";
    public static final String MES_NOTIFICATIONS = "Mes_notifications";
    public static final String MON_HISTORIQUE = "Mon_historique";
    public static final String MES_PREFERENCES_SANTE = "Mes_preferences_sante";
    public static final String MES_INFORMATIONS_PERSONNELLES = "Mes_informations_personnelles";
    public static final String MES_IDENTIFIANTS_DE_CONNEXION = "Mes_identifiants_de_connexion";

    // Chapter1
    public static final String ENTETE_ET_RECHERCHE = "Entête et recherche";
    public static final String ALERTES = "Alertes";
    public static final String FEEDBACKS_DE_RECHERCHE = "Feedbacks de recherche";
    public static final String FILTRES_DE_RECHERCHE = "Filtres de recherche";
    public static final String MES_PRÉFÉRENCES_SANTÉ = "Mes préférences santé";
    public static final String RECHERCHES_SAUVEGARDEES = "Recherches sauvegardées";
    public static final String SUPPRESSION_DES_CENTRES_D_INTERET = "Suppression des centres d'intérêt";

    // Chapter2
    public static final String EN_SAVOIR_PLUS = "En Savoir Plus";
    public static final String RECHERCHES_TROUVER = "Recherche TROUVER";
    public static final String RECHERCHES_S_INFORMER = "Recherche S'INFORMER";
    public static final String TROUVER_AUTOUR_DE_MOI = "Trouver Autour de moi";
    public static final String TROUVER_VUE_CARTE = "Trouver Vue carte";
    public static final String TROUVER_VUE_LISTE = "Trouver Vue liste";

    // Chapter 3
    public static final String NIVEAU = "Niveau ";

    public static final String RECHERCHE_INFORMER = "Recherche_INFORMER";
    public static final String RECHERCHE_TROUVER = "Recherche_TROUVER";

    public static final int INFO = 0;
    public static final int FIND = 1;
    public static final int AROUND = 2;

    private InternalSearchTrackerHelper[] internalSearchTrackerHelper;

    private Tracker tracker;

    private final Context context;
    private IHistoryRecentUpdateHelper historyUpdateHelper;
    private final IRepository repository;
    private INetWorkState netWorkState;


    public static class TrackerPageArg {

        //        private String title;
        private NodeType nodeType;
        private Xiti xiti;

        public int getPosition() {
            return position;
        }

        private int position;

        public TrackerPageArg(Xiti xiti) {
//            this.xiti = xiti;
            Parcel p = Parcel.obtain();
            p.writeValue(xiti);
            p.setDataPosition(0);
            this.xiti = (Xiti) p.readValue(Xiti.class.getClassLoader());
            p.recycle();
            nodeType = NodeType.UNKNOWN;
        }

        public TrackerPageArg(WebViewArg webViewArg) {
            xiti = webViewArg.getXiti();
            nodeType = webViewArg.getNodeType() != null ? webViewArg.getNodeType() : NodeType.UNKNOWN;
        }

        public String getTitle() {
            return xiti.getTitle();
        }

        public NodeType getNodeType() {
            return nodeType;
        }

        public int getLevel2() {
            return xiti.getLevel2();
        }

        public String getChapter1() {
            return xiti.getChapter1();
        }

        public String getChapter2() {
            return xiti.getChapter2();
        }

        public String getChapter3() {
            return xiti.getChapter3();
        }

        public String getChapter3Click() {
            return xiti.getChapter3Click();
        }

        public String getChapter3Page() {
            return xiti.getChapter3Page();
        }

        public void update(IGetUrl getUrl) {
            nodeType = getUrl.getNodeType() != null ? getUrl.getNodeType() : NodeType.UNKNOWN;
            position = getUrl.getPosition();
        }

        public Xiti getXiti() {
            return xiti;
        }

        public XitiType makeXitiType() {
            switch (getNodeType()) {
                case VIDEO:
                case FICHES_INFOS:
                    return XitiType.NAVIGATION;
                case LIENS_EXTERNES:
                    return XitiType.EXIT;
                case NUMERO_TEL:
                    return XitiType.ACTION;
                case APPLICATIONS:
                    return XitiType.DOWNLOAD;
                case SERVICE_DE_SANTE:
                    return XitiType.ACTION;
                case ESTABLISSEMENT_DE_SANTE:
                    return XitiType.ACTION;
                case OFFRES_DE_SOINS:
                    return XitiType.ACTION;
                case PROFESSIONNEL_DE_SANTE:
                    return XitiType.ACTION;
                default:
                    if (BuildConfig.DEBUG) {
                        return XitiType.ACTION;
                    }
                    break;
            }
            return XitiType.ACTION;
        }

        public ClickType getClickType() {
            return xiti.getClickType();
        }

        public void setXiti(Xiti xiti) {
            this.xiti = xiti;
        }
    }

    public static class TrackerPageArgBuilder {

        private TrackerPageArg trackerPageArg;

        public TrackerPageArgBuilder(WebViewArg webViewArg) {
            trackerPageArg = new TrackerPageArg(webViewArg);
        }

        public TrackerPageArgBuilder(Xiti xiti/*, String title*/) {
            trackerPageArg = new TrackerPageArg(xiti);
        }

        public TrackerPageArgBuilder(TrackerLevel trackerLevel, IGetUrl getUrl) {
            final Xiti xiti = getUrl.getXiti();
            trackerPageArg = new TrackerPageArg(xiti);
            if (xiti != null) {
                trackerPageArg.getXiti().setTrackerLevel(trackerLevel);
            }
            trackerPageArg.update(getUrl);
        }

        public TrackerPageArgBuilder(TrackerLevel trackerLevel, IGetUrl getUrl, ClickType clickType) {
            final Xiti xiti = getUrl.getXiti();
            trackerPageArg = new TrackerPageArg(xiti);
            if (xiti != null) {
                trackerPageArg.getXiti().setTrackerLevel(trackerLevel).setClickType(clickType);
            } else {
                if (BuildConfig.DEBUG) {
                    //throw new IllegalArgumentException("xiti is null");
                }
            }
            trackerPageArg.update(getUrl);
        }

        public TrackerPageArg build() {
            return trackerPageArg;
        }
    }

    public static void init(Context context) {
        com.atinternet.tracker.Tracker tracker = ATInternet.getInstance().getDefaultTracker();
        HashMap config = new HashMap<String, Object>() {{
            put("log", context.getString(R.string.at_internet_log));
            put("logSSL", context.getString(R.string.at_internet_logssl));
            put("site", context.getString(R.string.at_internet_site));
            put("secure", false);
            put("hashUserId", false);
            put("storage", "required");
            put("pixelPath", "/hit.xiti");
            put("plugins", "tvtracking");
            put("domain", "xiti.com");
            put("identifier", "androidId");//?
            put("persistIdentifiedVisitor", true);
            put("enableCrashDetection", true);
            put("tvtVisitDuration", 10);//1
            put("tvtURL", "");
            put("campaignLastPersistence", false);//true
            put("campaignLifetime", 30);//1
            put("sessionBackgroundDuration", 60);//10
        }};
        tracker.setConfig(config, () -> Log.d("TrackerAT", "Configuration is now set"));
//        if (context instanceof Activity) {
//            if (BuildConfig.DEBUG) {
//                try {
//                    Debugger.remove();
//                    Debugger.create(context, tracker);
//                } catch (Exception e) {
//
//                }
//            }
//        }
    }

//    public TrackerAT(Context context, IDataBaseHelper dataBaseHelper, INetWorkState netWorkState) {
//
//    }

    public TrackerAT(Context context, IHistoryRecentUpdateHelper historyUpdateHelper, IRepository repository, INetWorkState netWorkState) {
        tracker = ATInternet.getInstance().getDefaultTracker();
        this.context = context;
        this.historyUpdateHelper = historyUpdateHelper;
        this.repository = repository;
        this.netWorkState = netWorkState;
//        if(!BuildConfig.DEBUG) {
        internalSearchTrackerHelper = new InternalSearchTrackerHelper[3];
        internalSearchTrackerHelper[0] = new InternalSearchTrackerHelper(tracker);
        internalSearchTrackerHelper[1] = new InternalSearchTrackerHelper(tracker);
        internalSearchTrackerHelper[2] = new InternalSearchTrackerHelper(tracker);
//        }
    }

    @Override
    public void addScreen(TrackerPageArg trackerPageArg) {
        if (isTrack()) {
            try {
                if (trackerPageArg.getXiti() != null) {
                    final TrackerLevel trackLavel = trackerPageArg.xiti.getScreenTrackerLevel();
                    if (trackLavel == TrackerLevel.UNIT_CONTENTS_INFORMATION ||
                            trackLavel == TrackerLevel.UNIT_CONTENTS_FIND) {
                        if (trackerPageArg != null && trackerPageArg.getNodeType() != null) {
                            final NodeType nodeType = trackerPageArg.getNodeType();
                            switch (nodeType) {
//                            case FICHES_INFOS:
                                case LIENS_EXTERNES:
                                case APPLICATIONS:
                                case NUMERO_TEL:
                                    sendScreen(trackerPageArg);
                                    break;
                            }
                        } else {
                            if (BuildConfig.DEBUG) {
                                throw new IllegalArgumentException();
                            }
                        }
                    } else {
                        sendScreen(trackerPageArg);
                    }
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    throw e;
                }
            }
        }
    }

    @Override
    public void sendScreen(TrackerPageArg trackerPageArg) {
        if (isTrack()) {
            final Screen screen = tracker.Screens()
                    .add(trackerPageArg.getTitle())
                    .setLevel2(trackerPageArg.getLevel2());
            final String chapter1 = trackerPageArg.getChapter1();
            if (chapter1 != null) {
                screen.setChapter1(chapter1);
            }
            final String chapter2 = trackerPageArg.getChapter2();
            if (chapter2 != null) {
                screen.setChapter2(chapter2);
            }
            final String chapter3 = trackerPageArg.getChapter3Page();
            if (chapter3 != null) {
                screen.setChapter3(chapter3);
            }
            sendAction(trackerPageArg, screen);
        }
    }

    @Override
    public void addGesture(TrackerPageArg trackerPageArg, boolean isOnePageClick) {
        if (isTrack()) {
            try {
                if (trackerPageArg.getXiti() != null) {
                    switch (trackerPageArg.getXiti().getAppTrackerLevel()) {
                        case UNIT_CONTENTS_INFORMATION:
                        case UNIT_CONTENTS_FIND:
                            internalSearch(trackerPageArg);
                            break;
                    }
                    final Gesture gesture = tracker.Gestures()
                            .add(trackerPageArg.getTitle())
                            .setLevel2(trackerPageArg.getLevel2());
                    final String chapter1 = trackerPageArg.getChapter1();
                    if (chapter1 != null) {
                        gesture.setChapter1(chapter1);
                    }
                    final String chapter2 = trackerPageArg.getChapter2();
                    if (chapter2 != null) {
                        gesture.setChapter2(chapter2);
                    }
                    final String chapter3 = isOnePageClick ? trackerPageArg.getChapter3Click() : trackerPageArg.getChapter3();
                    if (chapter3 != null) {
                        gesture.setChapter3(chapter3);
                    }
                    sendAction(trackerPageArg, gesture);
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    throw e;
                }
            }
        }
    }

    private void internalSearch(TrackerPageArg trackerPageArg) {
        if (internalSearchTrackerHelper != null) {
            final Xiti xiti = trackerPageArg.getXiti();
            if (xiti != null) {
                if (xiti.getClickType() == ClickType.clic_contenu_consultation) {
                    switch (xiti.getScreenTrackerLevel()) {
                        case UNIT_CONTENTS_INFORMATION:
                            if (xiti.getAppTrackerLevel() == TrackerLevel.UNKNOWN) {
                                internalSearchTrackerHelper[AROUND].addGesture(trackerPageArg);
                            } else {
                                internalSearchTrackerHelper[INFO].addGesture(trackerPageArg);
                            }
                            break;
                        case UNIT_CONTENTS_FIND:
                            if (xiti.getAppTrackerLevel() == TrackerLevel.UNKNOWN) {
                                internalSearchTrackerHelper[AROUND].addGesture(trackerPageArg);
                            } else {
                                internalSearchTrackerHelper[FIND].addGesture(trackerPageArg);
                            }
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void addSearchInfo(SearchCommand searchCommand, SearchResponseHuge response) {
        historyUpdateHelper.add(searchCommand, response);
        if (isTrack()) {
            if (internalSearchTrackerHelper != null) {
                switch (searchCommand.getType()) {
                    case info:
                        internalSearchTrackerHelper[INFO].addSearchInfo(searchCommand, response);
                        break;
                    case find:
                        internalSearchTrackerHelper[FIND].addSearchInfo(searchCommand, response);
                        break;
                    case around:
                        internalSearchTrackerHelper[AROUND].addSearchInfo(searchCommand, response);
                        break;
                }
            }
        }
    }

    private void sendAction(TrackerPageArg trackerPageArg, Object object) {
        if (object instanceof Gesture) {
            final Gesture gesture = (Gesture) object;
            XitiType type = trackerPageArg.getXiti().getType();
            if (type == null || type == XitiType.UNKNOWN) {
                type = trackerPageArg.makeXitiType();
                Timber.w("type is unknown");
            } else {
                if (type != trackerPageArg.makeXitiType()) {
                    Timber.w("type is not same");
                }
            }
            switch (type) {
                case NAVIGATION:
                    gesture.sendNavigation();
                    break;
                case DOWNLOAD:
                    gesture.sendDownload();
                    break;
                case ACTION:
                    gesture.sendExit();
                    break;
                case EXIT:
                    gesture.sendExit();
                    break;
                case TOUCH:
                    gesture.sendTouch();
                    break;
                case UNKNOWN:
                default:
                    gesture.sendExit();
                    break;
            }
        } else {
            if (object instanceof Screen) {
                ((Screen) object).sendView();
            }
        }
    }

    @Override
    public IHistoryRecentUpdateHelper getHistoryUpdateHelper() {
        return historyUpdateHelper;
    }

    private boolean isTrack() {
        if (netWorkState.isLoggedIn()) {
            final RegisterUserResponse credential = repository.getCredential();
            if (credential != null) {
                final User user = credential.getUser();
                if (user != null) {
                    return user.track_activity == 1;
                }
            }
        }
        return true;
    }

}
