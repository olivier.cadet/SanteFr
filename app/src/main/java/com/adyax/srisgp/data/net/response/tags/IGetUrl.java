package com.adyax.srisgp.data.net.response.tags;

import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;

/**
 * Created by SUVOROV on 11/24/16.
 */
public interface IGetUrl {
    int getPosition();

    String getCardLink();
    String getPostData();

    long getNid();
    String getDescription();
    NodeType getNodeType();
    String getTitle();
    String getSourceOrPropose();
    Xiti getXiti();
    IconType getIconType();
    String getPhone();
    String getApple();
    String getGoogle();
}
