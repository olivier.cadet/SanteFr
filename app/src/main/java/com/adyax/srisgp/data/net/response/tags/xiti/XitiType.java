package com.adyax.srisgp.data.net.response.tags.xiti;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 10/25/16.
 */

public enum XitiType {
    @SerializedName("navigation")
    NAVIGATION,

    @SerializedName("action")
    ACTION,

    @SerializedName("download")
    DOWNLOAD,

    @SerializedName("exit")
    EXIT,

    @SerializedName("touch")
    TOUCH,

    UNKNOWN;
}
