package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/19/16.
 */
public class NotificationUsers implements Parcelable {

    @SerializedName("items")
    public NotificationItem[] items;

    @SerializedName("has_more")
    public int has_more;

    public boolean hasMore() {
        return has_more == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(this.items, flags);
        dest.writeInt(this.has_more);
    }

    public NotificationUsers() {
    }

    protected NotificationUsers(Parcel in) {
        this.items = in.createTypedArray(NotificationItem.CREATOR);
        this.has_more = in.readInt();
    }

    public static final Parcelable.Creator<NotificationUsers> CREATOR = new Parcelable.Creator<NotificationUsers>() {
        @Override
        public NotificationUsers createFromParcel(Parcel source) {
            return new NotificationUsers(source);
        }

        @Override
        public NotificationUsers[] newArray(int size) {
            return new NotificationUsers[size];
        }
    };
}
