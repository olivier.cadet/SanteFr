package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 11/14/16.
 */

// TODO need to base to BaseFeedbackCommandCommand
public class SubmitFeedbackFormCommand extends ServiceCommand {

    @SerializedName(BaseFeedbackCommandCommand.FORM_NAME_FIELD)
    private String formName;

    @SerializedName(BaseFeedbackCommandCommand.SECRET_FIELD)
    private String secret;

    @SerializedName(BaseFeedbackCommandCommand.FIELD_VALUES_FIELD)
    private FeedbackFormValue value;

    private long startTime;

    private transient boolean authenticated;

    public SubmitFeedbackFormCommand(boolean authenticated, String formName, String secret, FeedbackFormValue value, long startTime) {
        this.authenticated = authenticated;
        this.formName = formName;
        this.secret = secret;
        this.value = value;
        this.startTime = startTime;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        final long waitTime=6000-(System.currentTimeMillis()-startTime);
        if(startTime>0) {
            if (waitTime > 0) {
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    Timber.e(e);
                }
            }
        }else{
            Timber.w("startTime is 0");
        }
        if (authenticated) {
            executor.submitForm(this, receiver, requestCode);
        } else {
            executor.submitFormAnonymous(this, receiver, requestCode);
        }
    }

    public String getFormName() {
        return formName;
    }

    public String getSecret() {
        return secret;
    }

    public FeedbackFormValue getValue() {
        return value;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formName);
        dest.writeString(this.secret);
        dest.writeParcelable(this.value, flags);
        dest.writeByte((byte) (this.authenticated ? 1 : 0));
        dest.writeLong(this.startTime);
    }

    protected SubmitFeedbackFormCommand(Parcel in) {
        this.formName = in.readString();
        this.secret = in.readString();
        this.value = in.readParcelable(FeedbackFormValue.class.getClassLoader());
        this.authenticated = in.readByte() != 0;
        this.startTime = in.readLong();
    }

    public static final Creator<SubmitFeedbackFormCommand> CREATOR = new Creator<SubmitFeedbackFormCommand>() {
        @Override
        public SubmitFeedbackFormCommand createFromParcel(Parcel source) {
            return new SubmitFeedbackFormCommand(source);
        }

        @Override
        public SubmitFeedbackFormCommand[] newArray(int size) {
            return new SubmitFeedbackFormCommand[size];
        }
    };
}
