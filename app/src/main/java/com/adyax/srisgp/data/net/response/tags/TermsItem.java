package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class TermsItem implements Parcelable{

    @SerializedName("tid")
    public long tid;

    @SerializedName("vid")
    public long vid;

    @SerializedName("name")
    public String name;

    @SerializedName("description")
    public String description;

    @SerializedName("format")
    public String format;

    @SerializedName("weight")
    public int weight;

    @SerializedName("uuid")
    public String uuid;

    @SerializedName("parent")
    public long parent;

    protected TermsItem(Parcel in) {
        tid = in.readLong();
        vid = in.readLong();
        name = in.readString();
        description = in.readString();
        format = in.readString();
        weight = in.readInt();
        uuid = in.readString();
        parent = in.readLong();
    }

    public static final Creator<TermsItem> CREATOR = new Creator<TermsItem>() {
        @Override
        public TermsItem createFromParcel(Parcel in) {
            return new TermsItem(in);
        }

        @Override
        public TermsItem[] newArray(int size) {
            return new TermsItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(tid);
        parcel.writeLong(vid);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(format);
        parcel.writeInt(weight);
        parcel.writeString(uuid);
        parcel.writeLong(parent);
    }

    public String getId() {
        return String.valueOf(tid);
    }

    public String getName() {
        return name;
    }
}
