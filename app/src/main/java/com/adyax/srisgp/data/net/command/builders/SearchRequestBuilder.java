package com.adyax.srisgp.data.net.command.builders;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SearchFilter;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.utils.FiltersHelper;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 9/28/16.
 */

public class SearchRequestBuilder {

    @Inject
    FiltersHelper filtersHelper;

    private SearchType type = SearchType.info;
    private String text;
    private String location;
    private Double latitude;
    private Double longitude;
    private Integer accuracy;
    private int page = 0;
    private String stateToken;
    private int itemsPerPage = 10;
    private boolean includeSuggestedType = false;
    private boolean includeMessages = true;
    private double bbox[];
    private String order;

    public SearchRequestBuilder(SearchCommand searchCommand) {
        type = searchCommand.getType();
        text = searchCommand.getSearch();
        latitude = searchCommand.getLatitude();
        longitude = searchCommand.getLongitude();
        location = searchCommand.getCityLocation();
        accuracy = searchCommand.getAccuracy();
        stateToken = searchCommand.getStateToken();
        bbox = searchCommand.getBbox();
        order = searchCommand.getOrder();
        App.getApplicationComponent().inject(this);
    }

    public SearchRequestBuilder(SearchType type) {
        this.type = type;
        App.getApplicationComponent().inject(this);
    }

    public SearchRequestBuilder setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
        return this;
    }

    public SearchRequestBuilder setType(SearchType type) {
        this.type = type;
        return this;
    }

    public SearchRequestBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public SearchRequestBuilder setLocation(String location) {
        this.location = location;
        return this;
    }

    public SearchRequestBuilder setGeo(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        return this;
    }

    public SearchRequestBuilder setPage(int page) {
        this.page = page;
        return this;
    }

    public SearchRequestBuilder setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
        return this;
    }

    public SearchRequestBuilder setStateToken(String stateToken) {
        this.stateToken = stateToken;
        return this;
    }

    public SearchRequestBuilder setSuggestedType(boolean includeSuggestedType) {
        this.includeSuggestedType = includeSuggestedType;
        return this;
    }

    public SearchRequestBuilder setOrder(String order) {
        // Set default value if value is null or empty
        this.order = (order == null || order.isEmpty()) ? UrlExtras.RestUserSearch.SORT_RELEVANCE : order;
        return this;
    }

    public SearchRequestBuilder addExtraMessages(boolean includeMessages) {
        this.includeMessages = includeMessages;
        return this;
    }

    public SearchCommand build() {
        SearchCommand searchCommand = buildWithoutFilters();

        //create filters list
        ArrayList<SearchFilter> searchFilters = new ArrayList<>();
        searchFilters.addAll(filtersHelper.getFiltersForSearch(type));
        searchCommand.setFilters(searchFilters);
        return searchCommand;
    }

    public SearchCommand buildWithoutFilters() {
        SearchCommand searchCommand = new SearchCommand(text);
        searchCommand.setStateToken(stateToken);
        searchCommand.setText(text);
        searchCommand.setType(type);
        searchCommand.setAccuracy(accuracy);
        searchCommand.setPage(page);
        searchCommand.setItemsPerPage(itemsPerPage);
        searchCommand.setBbox(bbox);
        searchCommand.setLocation(location);
        searchCommand.setGeo(latitude, longitude);
        searchCommand.setOrder(order);

        if (includeSuggestedType) {
            searchCommand.setExtraFields(
                    new ArrayList<String>() {{
                        add("suggested_type");
                    }}
            );
        }

        if (includeMessages) {
            String extraMessages = "messages";
            if (searchCommand.getExtraFields() == null) {
                searchCommand.setExtraFields(new ArrayList<>());
            }

            searchCommand.getExtraFields().add(extraMessages);
        }

        return searchCommand;
    }
}
