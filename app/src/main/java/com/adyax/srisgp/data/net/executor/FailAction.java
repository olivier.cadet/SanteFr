package com.adyax.srisgp.data.net.executor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public interface FailAction {
    void onFail(String errorMessage, int errorCode);
}
