package com.adyax.srisgp.data.net.response.tags;

import android.database.Cursor;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.db.IExpandArray;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by SUVOROV on 9/20/16.
 */
@DatabaseTable(tableName = "related")
public class Related implements IExpandArray {

    public static final String LABEL = "label";
    public static final String SEARCH_TYPE = "type";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert=true, columnName = "id", canBeNull = false)
    public long id;

    @SerializedName(LABEL)
    @DatabaseField(columnName = LABEL)
    public String label;

    @SerializedName("items")
    @ForeignCollectionField()
    public Collection<RelatedItem> items;

    @DatabaseField(columnName = SEARCH_TYPE)
    public SearchType type;

    public Related(){
    }

    @Override
    public void collapse() {

    }

    @Override
    public void expand(Cursor cursor) {

    }

    public void setId(long id) {
        this.id = id;
    }

    public void setType(SearchType type) {
        this.type = type;
    }
}
