package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Kirill on 07.11.16.
 */

public class SearchFilterValue implements Parcelable, Serializable {
    @SerializedName("param")
    private String param;

    @SerializedName("value")
    private String value;

    public String getParam() {
        return param;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.param);
        dest.writeString(this.value);
    }

    public SearchFilterValue() {
    }

    protected SearchFilterValue(Parcel in) {
        this.param = in.readString();
        this.value = in.readString();
    }

    public static final Parcelable.Creator<SearchFilterValue> CREATOR = new Parcelable.Creator<SearchFilterValue>() {
        @Override
        public SearchFilterValue createFromParcel(Parcel source) {
            return new SearchFilterValue(source);
        }

        @Override
        public SearchFilterValue[] newArray(int size) {
            return new SearchFilterValue[size];
        }
    };
}
