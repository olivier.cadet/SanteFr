package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class CitiesResponse extends BaseResponse  implements Parcelable {

    private Map<String, Long> cites=new HashMap<>();

    private String[] result;

    public CitiesResponse(String json) {
        final Map<Long, String> citesIdAsKey = new Gson().fromJson(json, new TypeToken<HashMap<Long, String>>(){}.getType());
        final Map<String, Long> citesPostal=new HashMap<>();

        cites = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o, String t1) {
                try {
                    return citesPostal.get(o).compareTo(citesPostal.get(t1));
                }catch (Exception e){
                    return  0;
                }
            }
        });
        try {
            for (Long key : citesIdAsKey.keySet()) {
                final String cityName = citesIdAsKey.get(key);
                final Long postal = Long.valueOf(cityName.substring(cityName.indexOf("(") + 1, cityName.indexOf(")")));
                citesPostal.put(cityName, postal);
                cites.put(cityName, key);
            }
        }catch (Exception e){
        }
    }

    protected CitiesResponse(Parcel source) {
        final int N = source.readInt();
        for (int i=0; i<N; i++) {
            String termsItem = source.readString();
            Long key = source.readLong();
            cites.put(termsItem, key);
        }
    }

    public static final Creator<CitiesResponse> CREATOR = new Creator<CitiesResponse>() {
        @Override
        public CitiesResponse createFromParcel(Parcel in) {
            return new CitiesResponse(in);
        }

        @Override
        public CitiesResponse[] newArray(int size) {
            return new CitiesResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int parcelableFlags) {
        final int N = cites.size();
        dest.writeInt(N);
        if (N > 0) {
            for (Map.Entry<String, Long> entry : cites.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeLong(entry.getValue());
            }
        }
    }

    public synchronized String[] getCites() {
        if(result==null) {
            final Set<String> strings = cites.keySet();
            final int size = strings.size();
//            if(size>0) {
                result = strings.toArray(new String[size]);
//            }
        }
        return result;
    }

    public Long getId(String cityName) {
        return cites.get(cityName);
    }
}
