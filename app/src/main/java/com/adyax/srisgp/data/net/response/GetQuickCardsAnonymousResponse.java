package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class GetQuickCardsAnonymousResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public List<QuickCardsAnonymousItem> items = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
    }

    public GetQuickCardsAnonymousResponse() {
    }

    protected GetQuickCardsAnonymousResponse(Parcel in) {
        this.items = new ArrayList<QuickCardsAnonymousItem>();
        in.readList(this.items, QuickCardsAnonymousItem.class.getClassLoader());
    }

    public static final Creator<GetQuickCardsAnonymousResponse> CREATOR = new Creator<GetQuickCardsAnonymousResponse>() {
        @Override
        public GetQuickCardsAnonymousResponse createFromParcel(Parcel source) {
            return new GetQuickCardsAnonymousResponse(source);
        }

        @Override
        public GetQuickCardsAnonymousResponse[] newArray(int size) {
            return new GetQuickCardsAnonymousResponse[size];
        }
    };
}
