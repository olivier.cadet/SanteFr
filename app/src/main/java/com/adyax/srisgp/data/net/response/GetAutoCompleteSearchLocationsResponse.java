package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class GetAutoCompleteSearchLocationsResponse extends BaseResponse implements Parcelable {

    @SerializedName("locations")
    public List<String> locations;

    @SerializedName("regions")
    public List<String> regions;

    @SerializedName("active")
    public List<String> active;

    public String location;

    public StatusLocation getStatusLocation(String locationText) {
        if(locationText!=null&& locations.size()==regions.size()&& locations.size()==active.size()) {
            for (int i=0;i<locations.size();i++) {
                if(locationText.equals(locations.get(i))){
                    return new StatusLocation(locations.get(i), regions.get(i), active.get(i));
                }
            }
        }
        return new StatusLocation(locationText);
    }

    public StatusLocation getStatusLocation(int i) {
        return new StatusLocation(locations.get(i), regions.get(i), active.get(i));
    }

    public boolean isActive() {
        final int size = locations.size();
        if(size>0){
            final int size1 = regions.size();
            if(size1 >0&& size1==size){
                int size2 = active.size();
                if(size2 >0&& size2==size){
                    return getStatusLocation(0).isActive();
                }
            }
        }
        return true;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static class StatusLocation implements Parcelable{

        public static final String YES = "yes";
        public static final String UNKNOWN = "unknown";

        public StatusLocation(String location) {
            this.location = location;
            regions=null;
            active=YES;
        }

        protected StatusLocation(Parcel in) {
            location = in.readString();
            regions = in.readString();
            active = in.readString();
        }

        public static final Creator<StatusLocation> CREATOR = new Creator<StatusLocation>() {
            @Override
            public StatusLocation createFromParcel(Parcel in) {
                return new StatusLocation(in);
            }

            @Override
            public StatusLocation[] newArray(int size) {
                return new StatusLocation[size];
            }
        };

        public String getLocation() {
            return location;
        }

        final private String location;
        final private String regions;
        private String active;

        private StatusLocation(){
            this.location = null;
            regions=null;
            active=null;
        }

        public static StatusLocation createEmpty(){
            return new StatusLocation();
        }

        public StatusLocation(String location, String regions, String active) {
            this.location = location;
            this.regions = regions;
            this.active = active;
        }

        public boolean isEmpty() {
            return location ==null;
        }

        public boolean isActive() {
            return TextUtils.equals(active, YES)|| TextUtils.equals(active, UNKNOWN);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(location);
            parcel.writeString(regions);
            parcel.writeString(active);
        }

        public boolean isFake() {
            return regions==null;
        }

        public void setActive() {
            active= YES;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.locations);
        dest.writeStringList(this.regions);
        dest.writeStringList(this.active);
        dest.writeString(location);
    }

    public GetAutoCompleteSearchLocationsResponse() {
        locations=new ArrayList<>();
    }

    protected GetAutoCompleteSearchLocationsResponse(Parcel in) {
        this.locations = in.createStringArrayList();
        this.regions = in.createStringArrayList();
        this.active = in.createStringArrayList();
        location=in.readString();
    }

    public static final Parcelable.Creator<GetAutoCompleteSearchLocationsResponse> CREATOR = new Parcelable.Creator<GetAutoCompleteSearchLocationsResponse>() {
        @Override
        public GetAutoCompleteSearchLocationsResponse createFromParcel(Parcel source) {
            return new GetAutoCompleteSearchLocationsResponse(source);
        }

        @Override
        public GetAutoCompleteSearchLocationsResponse[] newArray(int size) {
            return new GetAutoCompleteSearchLocationsResponse[size];
        }
    };

    public boolean existsLocation(String location_text) {
        if(locations!=null&& location_text!=null){
            for(String s:locations){
                if(location_text.trim().equals(s.trim())){
                    return true;
                }
            }
        }
        return false;
    }

    public String getLocation() {
        return location;
    }

}
