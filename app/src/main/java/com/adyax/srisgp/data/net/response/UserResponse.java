package com.adyax.srisgp.data.net.response;

import com.adyax.srisgp.data.net.response.tags.User;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class UserResponse extends BaseResponse{

    public static final String UID = "uid";
    public static final String NEW_NOTIFICATIONS = "new_notifications";
    public static final String INTERESTS_NODE_COUNT = "interests_keywords_terms_with_nodes_count";

    @SerializedName("user")
    public User user;

    @SerializedName(NEW_NOTIFICATIONS)
    public int new_notifications;

    @SerializedName(UID)
    public int uid;

    @SerializedName(INTERESTS_NODE_COUNT)
    public Map<Long, Integer> interestsNodeCount;


    public int getNewNotificationCount() {
        return new_notifications;
    }

//    "interests_keywords_terms_with_nodes_count": [
//    {1752: 83},
//    {1754: 56}
//    ]

}
