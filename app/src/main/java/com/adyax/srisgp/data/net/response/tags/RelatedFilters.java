package com.adyax.srisgp.data.net.response.tags;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by SUVOROV on 9/20/16.
 */
@DatabaseTable(tableName = "related_filters")
public class RelatedFilters {

    public static final String TYPE_FILTERS = "type_filters";
    public static final String SEARCH_TYPE = "search_type_filter";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert=true, columnName = "id_filters", canBeNull = false)
    public long id;

    @DatabaseField(foreign=true, columnName="parent_id_filters", uniqueCombo = true)
    public RelatedItem related;

    @DatabaseField(columnName = TYPE_FILTERS, uniqueCombo = true)
    public String type;

    @DatabaseField(columnName = SEARCH_TYPE, uniqueCombo = true)
    public SearchType searchType;

    @DatabaseField(columnName = "value_filters", uniqueCombo = true)
    public String value;

    public RelatedFilters(){
    }

    public RelatedFilters(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public void setParentId(RelatedItem parentId) {
        this.related = parentId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }
}
