package com.adyax.srisgp.data.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 11/4/16.
 */

public enum SearchItemType {
    @SerializedName("1")
    ACTUALITE,
    @SerializedName("2")
    FICHE_INFO_GENERIQUE,
    @SerializedName("3")
    ALERTE_SANITAIRE;
}
