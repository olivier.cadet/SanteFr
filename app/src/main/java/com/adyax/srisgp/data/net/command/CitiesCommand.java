package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class CitiesCommand extends ServiceCommand {

    public String string;

    public CitiesCommand(Parcel in) {
        string = in.readString();
    }

    public static final Creator<CitiesCommand> CREATOR = new Creator<CitiesCommand>() {
        @Override
        public CitiesCommand createFromParcel(Parcel in) {
            return new CitiesCommand(in);
        }

        @Override
        public CitiesCommand[] newArray(int size) {
            return new CitiesCommand[size];
        }
    };

    public CitiesCommand(String string) {
        this.string = string;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(string);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.cities(this, receiver, requestCode);
    }

}
