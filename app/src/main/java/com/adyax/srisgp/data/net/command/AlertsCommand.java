package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.builders.AlertsIdsBuilder;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class AlertsCommand implements Parcelable {
public class AlertsCommand extends ServiceCommand {

    private long ids[];

    public AlertsCommand(String header) {
        ids = new AlertsIdsBuilder().add(header).build();
    }

    public AlertsCommand(long idAlert) {
        ids = new long[]{idAlert};
    }

    protected AlertsCommand(Parcel in) {
        ids = in.createLongArray();
    }

    public static final Creator<AlertsCommand> CREATOR = new Creator<AlertsCommand>() {
        @Override
        public AlertsCommand createFromParcel(Parcel in) {
            return new AlertsCommand(in);
        }

        @Override
        public AlertsCommand[] newArray(int size) {
            return new AlertsCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.alerts(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLongArray(ids);
    }

    public boolean isAlertsForQuery() {
        return ids != null && ids.length > 0;
    }
}
