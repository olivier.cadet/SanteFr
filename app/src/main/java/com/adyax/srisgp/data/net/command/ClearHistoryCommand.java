package com.adyax.srisgp.data.net.command;

import android.app.Service;
import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.history.HistoryKey;
import com.adyax.srisgp.mvp.history.HistoryType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 10/10/16.
 */

public class ClearHistoryCommand extends ServiceCommand {
    private HistoryType type;
    private List<Long> sids;

    private transient List<HistoryKey> keys;

    public ClearHistoryCommand(List<HistoryKey> keys, HistoryType type) {
        this.keys = keys;
        this.sids = new ArrayList<>();
        for (HistoryKey key : keys) {
            this.sids.add(key.sid);
        }
        this.type = type;
    }

    public List<HistoryKey> getKeys() {
        return keys;
    }

    public List<Long> getSids() {
        return sids;
    }

    public HistoryType getType() {
        return type;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.clearHistory(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeList(this.sids);
        dest.writeTypedList(this.keys);
    }

    protected ClearHistoryCommand(Parcel in) {
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : HistoryType.values()[tmpType];
        this.sids = new ArrayList<Long>();
        in.readList(this.sids, Long.class.getClassLoader());
        this.keys = in.createTypedArrayList(HistoryKey.CREATOR);
    }

    public static final Creator<ClearHistoryCommand> CREATOR = new Creator<ClearHistoryCommand>() {
        @Override
        public ClearHistoryCommand createFromParcel(Parcel source) {
            return new ClearHistoryCommand(source);
        }

        @Override
        public ClearHistoryCommand[] newArray(int size) {
            return new ClearHistoryCommand[size];
        }
    };
}
