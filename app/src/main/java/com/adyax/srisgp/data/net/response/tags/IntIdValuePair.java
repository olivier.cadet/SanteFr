package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class IntIdValuePair {

    @SerializedName("id")
    public int id;

    @SerializedName("value")
    public String value;

    public IntIdValuePair(int id, String value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static IntIdValuePair fromString(String s) {
        Gson gson = new Gson();
        return gson.fromJson(s, IntIdValuePair.class);
    }
}
