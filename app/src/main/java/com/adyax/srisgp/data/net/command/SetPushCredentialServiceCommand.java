package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

public class SetPushCredentialServiceCommand extends LoginBaseServiceCommand {
    private final String uid;

    SetPushCredentialServiceCommand(Parcel in) {
        super(in);
        uid = in.readString();
    }

    public SetPushCredentialServiceCommand(String deviceToken, String uid) {
        super(deviceToken);
        this.uid = uid;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(uid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SetPushCredentialServiceCommand> CREATOR = new Creator<SetPushCredentialServiceCommand>() {
        @Override
        public SetPushCredentialServiceCommand createFromParcel(Parcel in) {
            return new SetPushCredentialServiceCommand(in);
        }

        @Override
        public SetPushCredentialServiceCommand[] newArray(int size) {
            return new SetPushCredentialServiceCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.setPushCredential(this, receiver, requestCode);
    }
}
