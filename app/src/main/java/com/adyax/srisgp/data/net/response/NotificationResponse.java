package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.NotificationUsers;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class NotificationResponse extends BaseResponse implements Parcelable {

    @SerializedName("all")
    public NotificationUsers all;

    @SerializedName("read")
    public NotificationUsers read;

    @SerializedName("unread")
    public NotificationUsers unread;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.all, flags);
        dest.writeParcelable(this.read, flags);
        dest.writeParcelable(this.unread, flags);
    }

    public NotificationResponse() {
    }

    protected NotificationResponse(Parcel in) {
        this.all = in.readParcelable(NotificationUsers.class.getClassLoader());
        this.read = in.readParcelable(NotificationUsers.class.getClassLoader());
        this.unread = in.readParcelable(NotificationUsers.class.getClassLoader());
    }

    public static final Parcelable.Creator<NotificationResponse> CREATOR = new Parcelable.Creator<NotificationResponse>() {
        @Override
        public NotificationResponse createFromParcel(Parcel source) {
            return new NotificationResponse(source);
        }

        @Override
        public NotificationResponse[] newArray(int size) {
            return new NotificationResponse[size];
        }
    };
}
