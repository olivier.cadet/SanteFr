package com.adyax.srisgp.data.tracker;

import android.content.Context;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddInHistoryCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.net.response.tags.xiti.XitiType;
import com.adyax.srisgp.db.ContentResolverHelper;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.utils.Utils;
import com.adyax.srisgp.utils.ViewUtils;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 3/17/17.
 */

public class TrackerHelper {

    public static final boolean CARTE_MODE = true;
    public static final boolean LISTE_MODE = !CARTE_MODE;

    @Inject
    public ITracker tracker;

    @Inject
    IDataBaseHelper databaseHelper;

    @Inject
    INetWorkState netWorkState;

    @Inject
    FragmentFactory fragmentFactory;

    private IGetUrl getUrl;

    public TrackerHelper() {
        this((IGetUrl) null);
    }

    public TrackerHelper(WebViewArg webViewArg) {
        this(webViewArg.getGetUrl(webViewArg.getContentUrl(), webViewArg.getTitle()));
    }

    public TrackerHelper(IGetUrl getUrl) {
        this.getUrl = getUrl;
        App.getApplicationComponent().inject(this);
    }

    public void showAppsDialog(Context context, TrackerLevel trackerLevel) {
        addTouchGesture(new TrackerAT.TrackerPageArgBuilder(trackerLevel, getUrl, ClickType.clic_contenu_consultation).build());
        tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(TrackerLevel.UNIT_CONTENTS_INFORMATION, getUrl).build());
        ViewUtils.showAppsDialog(context, getUrl.getApple(), getUrl.getGoogle());
        addInHistory(context);
    }

    public String requestCall(Context context) {
        //----------------------------------------
        // do not change order!!!
        //----------------------------------------
        tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(
                TrackerLevel.UNIT_CONTENTS_INFORMATION, this.getUrl).build());
        addTouchGesture(new TrackerAT.TrackerPageArgBuilder(
                TrackerLevel.SEARCH_RESULTS_FOR_INFORMATION, this.getUrl, ClickType.clic_contenu_consultation).build());
        //----------------------------------------
        addInHistory(context);
        return Utils.requestCall(getUrl.getPhone(), context);
    }

    public void addInHistory(Context context) {
//        if (netWorkState.isLoggedIn()) {
        ExecutionService.sendCommand(context, null,
                new AddInHistoryCommand(getUrl.getNid()), ExecutionService.ADD_IN_HISTORY);
//        }
    }

    public void clickOnHomePage(TrackerLevel trackerLevel, ClickType clickType) {
        addTouchGesture(new TrackerAT.TrackerPageArgBuilder(trackerLevel, getUrl, clickType).build());
    }

    public void clickForWriteToHistory() {
        writeToHistory(HistoryItem.TYPE_ANONIMUS_HISTORY);
    }

    public void addTouchGesture(TrackerLevel trackerLevel) {
//        addGesture(new TrackerAT.TrackerPageArgBuilder(new WebViewArg(getUrl, trackerLevel)).build());
        addTouchGesture(new TrackerAT.TrackerPageArgBuilder(trackerLevel, getUrl, ClickType.clic_contenu_consultation).build());
    }

    public void addTouchGesture(TrackerAT.TrackerPageArg trackerPageArg) {
        writeToHistory(HistoryItem.TYPE_ANONIMUS_HISTORY);
        tracker.addGesture(trackerPageArg, false);
    }

    public void writeToHistory(@HistoryItem.TypeHistoryItem int anonymous) {
        if (!netWorkState.isLoggedIn() && anonymous == HistoryItem.TYPE_ANONIMUS_HISTORY) {
            final HistoryItem historyItem = new ItemAdapter(getUrl).getHistoryItem();
            if (!(getUrl instanceof HistoryItem)) {
                if (historyItem != null) {
                    historyItem.setAnonymous(anonymous);
                    databaseHelper.insertOrUpdateHistory(historyItem);
                }
            } else {
                databaseHelper.updateHistory(historyItem);
            }
        }
    }

    public void startWebViewActivity(Context context, TrackerLevel trackerLevel) {
        addTouchGesture(trackerLevel);
        fragmentFactory.startWebViewActivity(context, new WebViewArg(getUrl, WebPageType.SERVER, trackerLevel), -1);
    }

    public void clickEnSavoirPlus() {
        final Xiti xiti = new Xiti(TrackerLevel.HOMEPAGE)
                .setType(XitiType.NAVIGATION)
                .setChapter1(TrackerAT.ENTETE_ET_RECHERCHE)
                .setChapter2(TrackerAT.EN_SAVOIR_PLUS)
                .setClickType(ClickType.clic_homepage_en_savoir_plus);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
    }

    public void clickAlert() {
        final Xiti xiti = new Xiti(TrackerLevel.HOMEPAGE)
                .setType(XitiType.NAVIGATION)
                .setChapter1(TrackerAT.ENTETE_ET_RECHERCHE)
                .setChapter2(TrackerAT.ALERTES)
                .setChapter3(TrackerAT.NIVEAU + String.valueOf(getUrl instanceof AlertItem ? ((AlertItem) getUrl).level : 0))
                .setClickType(ClickType.clic_homepage_alerts);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
//        if(getUrl instanceof AlertItem){
//            if(((AlertItem)getUrl).isContent()){
//                writeToHistory(HistoryItem.TYPE_ANONIMUS_HISTORY);
//            }
//        }
    }

    public void clickYesNo(ClickType clickType) {
        Xiti xiti = getUrl.getXiti();
        if (xiti != null) {
            xiti = xiti.getClone()
                    .setType(XitiType.TOUCH)
                    .setClickType(clickType)
                    .setTrackerLevel(ContentResolverHelper.findType.contains(getUrl.getNodeType()) ? TrackerLevel.UNIT_CONTENTS_INFORMATION : TrackerLevel.UNIT_CONTENTS_FIND);
        } else {
            xiti = new Xiti(TrackerLevel.RESEARCH)
                    .setType(XitiType.TOUCH)
                    .setChapter1(TrackerAT.FEEDBACKS_DE_RECHERCHE)
                    .setChapter2(ContentResolverHelper.findType.contains(getUrl.getNodeType()) ? TrackerAT.RECHERCHES_TROUVER : TrackerAT.RECHERCHES_S_INFORMER)
                    .setClickType(clickType)
            ;
        }
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), true);
    }

    public void clickFilter(SearchType searchType) {
        final Xiti xiti = new Xiti(TrackerLevel.RESEARCH)
                .setType(XitiType.TOUCH)
                .setChapter1(TrackerAT.FILTRES_DE_RECHERCHE)
                // TODO what need to do if searchType is around
                .setChapter2(searchType == SearchType.find ? TrackerAT.RECHERCHES_TROUVER : TrackerAT.RECHERCHES_S_INFORMER)
                .setClickType(ClickType.clic_recherche_appliquer_les_filtres);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
    }

    public void addTouchGesture(TrackerLevel trackerLevel, ClickType clickType) {
        try {
            final TrackerAT.TrackerPageArg trackerPageArg = new TrackerAT.TrackerPageArgBuilder(trackerLevel, getUrl, clickType).build();
            final Xiti x = trackerPageArg.getXiti().getClone();
            x.setType(XitiType.TOUCH);
            trackerPageArg.setXiti(x);
            tracker.addGesture(trackerPageArg, false);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                throw e;
            }
        }
    }

    public void clickRoute() {
        try {
            final TrackerLevel trackerLevel = TrackerLevel.RESEARCH_RESULTS_FIND;
            final TrackerAT.TrackerPageArg trackerPageArg = new TrackerAT.TrackerPageArgBuilder(trackerLevel,
                    getUrl, ClickType.clic_contenu_itineraire).build();
            final Xiti x = trackerPageArg.getXiti().getClone();
            x.setType(XitiType.EXIT);
            x.setLevel2Click(trackerLevel);
            trackerPageArg.setXiti(x);
            tracker.addGesture(trackerPageArg, false);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                throw e;
            }
        }
    }

    public void clickReceiveEmail(boolean isChecked) {
        final Xiti xiti = new Xiti(TrackerLevel.ACCOUNT_MANAGEMENT)
                .setType(XitiType.NAVIGATION)
                .setChapter1(TrackerAT.MES_PRÉFÉRENCES_SANTÉ)
                .setClickType(isChecked ? ClickType.clic_notification_par_email_oui : ClickType.clic_notification_par_email_non);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
    }

    public void clickSignaler(TrackerLevel trackerLevel, ClickType clickType) {
        try {
            final TrackerAT.TrackerPageArg trackerPageArg = new TrackerAT.
                    TrackerPageArgBuilder(trackerLevel, getUrl, clickType).build();
            if (trackerPageArg.getXiti() != null) {
                final Xiti x = trackerPageArg.getXiti().getClone()
                        .setType(XitiType.TOUCH)
                        .setLevel2Click(trackerLevel)
                        .setClickType(clickType);
                trackerPageArg.setXiti(x);
                tracker.addGesture(trackerPageArg, true);
            }
        } catch (Exception e) {
            // Do not throw exception for tracking
            if (BuildConfig.DEBUG) {
                throw e;
            }
        }
    }

    public void clickSearch(ClickType clickType) {
        final Xiti xiti = new Xiti(TrackerLevel.HISTORICAL)
                .setType(XitiType.NAVIGATION)
                .setChapter1(TrackerAT.RECHERCHES_SAUVEGARDEES)
                .setClickType(clickType);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
    }

    public void clickRemoveNotification(String interest, boolean bChecket) {
        if (!bChecket) {
            final Xiti xiti = new Xiti(TrackerLevel.NOTIFICATIONS)
                    .setType(XitiType.TOUCH)
                    .setChapter1(TrackerAT.SUPPRESSION_DES_CENTRES_D_INTERET)
                    .setChapter2(interest)
                    .setClickType(ClickType.clic_notifications_suppression_centre_interet);
            tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
        }
    }

    public void clickOpenStaticPage(String title) {
        final Xiti xiti = new Xiti(TrackerLevel.TRANSVERSE_PAGES)
                .setType(XitiType.NAVIGATION)
                .setTitle(title);
        tracker.addGesture(new TrackerAT.TrackerPageArgBuilder(xiti).build(), false);
    }

    public void addScreenView() {
        tracker.sendScreen(new TrackerAT.TrackerPageArgBuilder(getUrl.getXiti()).build());
    }

    public void addScreenView(TrackerLevel trackerLevel, String title) {
        final Xiti xiti = new Xiti(trackerLevel)
                .setType(XitiType.ACTION)
                .setTitle(title);
        tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(xiti).build());
    }

    public void addScreenViewResearch(SearchCommand searchCommand, boolean bMapModeNotList) {
        final Xiti xiti = new Xiti(TrackerLevel.RESEARCH)
                .setType(XitiType.ACTION)
                .setChapter1(searchCommand.getType() == SearchType.info ? TrackerAT.RECHERCHES_S_INFORMER : TrackerAT.RECHERCHE_TROUVER);
        switch (searchCommand.getType()) {
            case info:
                xiti.setTitle(TrackerAT.S_INFORMER + searchCommand.getLocationATformat());
                break;
            case around:
                xiti.setChapter2(TrackerAT.TROUVER_AUTOUR_DE_MOI);
                xiti.setTitle(TrackerAT.RECHERCHE_AUTOUR_DE_MOI);
                break;
            case find:
                xiti.setChapter2(bMapModeNotList == CARTE_MODE ? TrackerAT.TROUVER_VUE_CARTE : TrackerAT.TROUVER_VUE_LISTE);
                xiti.setTitle((bMapModeNotList == CARTE_MODE ? TrackerAT.TROUVER_CARTE : TrackerAT.TROUVER_LISTE) + searchCommand.getLocationATformat());
                break;
        }
        tracker.addScreen(new TrackerAT.TrackerPageArgBuilder(xiti).build());
    }

    public void addTouchGesture(Xiti xiti, ClickType clickType) {
        try {
            xiti = xiti.getClone()
                    .setType(XitiType.TOUCH)
                    .setClickType(clickType)
                    .updateTrackLevel2();
            final TrackerAT.TrackerPageArg trackerPageArg = new TrackerAT.TrackerPageArgBuilder(xiti).build();
            tracker.addGesture(trackerPageArg, true);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                throw e;
            }
        }
    }
}
