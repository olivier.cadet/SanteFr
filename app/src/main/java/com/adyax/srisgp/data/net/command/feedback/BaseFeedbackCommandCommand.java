package com.adyax.srisgp.data.net.command.feedback;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.FeedbackFormValue;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

import timber.log.Timber;

/**
 * Created by SUVOROV on 2/14/17.
 */

public class BaseFeedbackCommandCommand<T extends Parcelable> extends ServiceCommand {

    public static final String CONTACT_NAME = "contact";
    public static final String FEEDBACK_FORM_NAME = "search_helpful";

    public static final String FORM_NAME_FIELD = "form_name";
    public static final String SECRET_FIELD = "secret";
    public static final String FIELD_VALUES_FIELD = "field_values";
    public static final String FIELD_URL = "field_url";

//    // TODO instead of call this method it is nessessary use starttime from getForm
//    public static long getStartTime(){
//        return System.currentTimeMillis();
//    }

//    public static final String SECRET_TOKEN = "secret token";

    @SerializedName(FORM_NAME_FIELD)
    private String formName;

    @SerializedName(SECRET_FIELD)
    private String secret;

    @SerializedName(FIELD_VALUES_FIELD)
    private T value;

    private long startTime;

    private transient boolean authenticated;

    public BaseFeedbackCommandCommand(boolean authenticated, String formName, String secret, T value, long startTime) {
        this.authenticated = authenticated;
        this.formName = formName;
        this.secret = secret;
        this.value = value;
        this.startTime = startTime;
    }

    public String getFormName() {
        return formName;
    }

    public String getSecret() {
        return secret;
    }

    public T getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formName);
        dest.writeString(this.secret);
        dest.writeParcelable(this.value, flags);
        dest.writeInt(this.authenticated? 1: 0);
        dest.writeLong(startTime);
    }

    protected BaseFeedbackCommandCommand(Parcel in) {
        this.formName = in.readString();
        this.secret = in.readString();
        this.value = in.readParcelable(FeedbackFormValue.class.getClassLoader());
        this.authenticated = in.readInt()==1;
        this.startTime=in.readLong();
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        final long waitTime=6000-(System.currentTimeMillis()-startTime);
        if(startTime>0) {
            if (waitTime > 0) {
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    Timber.e(e);
                }
            }
        }else{
            Timber.w("startTime is 0");
        }
        if(authenticated){
            executor.sendSubmitFormA(this, receiver, requestCode);
        }else
            executor.sendSubmitForm(this, receiver, requestCode);
    }

    public static final Creator<BaseFeedbackCommandCommand> CREATOR = new Creator<BaseFeedbackCommandCommand>() {
        @Override
        public BaseFeedbackCommandCommand createFromParcel(Parcel in) {
            return new BaseFeedbackCommandCommand(in);
        }

        @Override
        public BaseFeedbackCommandCommand[] newArray(int size) {
            return new BaseFeedbackCommandCommand[size];
        }
    };


}
