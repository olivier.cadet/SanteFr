package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class PingCommandAuth extends ServiceCommand {

    public PingCommandAuth() {
    }

    protected PingCommandAuth(Parcel in) {
    }

    public static final Creator<PingCommandAuth> CREATOR = new Creator<PingCommandAuth>() {
        @Override
        public PingCommandAuth createFromParcel(Parcel in) {
            return new PingCommandAuth(in);
        }

        @Override
        public PingCommandAuth[] newArray(int size) {
            return new PingCommandAuth[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        if (!executor.isPingState())
            executor.ping_auth(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
