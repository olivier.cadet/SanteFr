package com.adyax.srisgp.data.net.command.feedback;

public class InapLinkHelper {

    public static final String INFO_CONTENT_ERROR_FORM_NAME = "info_content_error";
    public static final String FIND_CONTENT_ERROR_FORM_NAME = "find_content_error";

    public static final String INAPP = "inapp";

    private String NameFormType;

    public InapLinkHelper fromInapp(String reportFormName) {
        final String[] splits = reportFormName.split("form=");
        if (splits != null && splits.length == 2) {
            NameFormType = splits[1];
        }
        return this;
    }

    public String getNameFormType() {
        return NameFormType;
    }

    // inapp://reportClick?form=find_content_error
    // https://stage.sris-gp.adyax-dev.com/recherche/find#!text=docteur&loc=Paris
    public static boolean isInapp(String url) {
        url = url.replace("/", "-");
        url = url.replace("?", "+");
        return url.contains("-find+") || url.contains(INAPP + "://");
    }
}

