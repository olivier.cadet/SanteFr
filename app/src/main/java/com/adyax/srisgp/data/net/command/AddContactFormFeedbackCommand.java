package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 11/21/16.
 */

public class AddContactFormFeedbackCommand extends ServiceCommand {

    @SerializedName(BaseFeedbackCommandCommand.FORM_NAME_FIELD)
    private String formName;

    @SerializedName(BaseFeedbackCommandCommand.SECRET_FIELD)
    private String secret;

    @SerializedName(BaseFeedbackCommandCommand.FIELD_VALUES_FIELD)
    private ContactFormValues values;
    private long startTime;

    private transient boolean authenticated;

    public AddContactFormFeedbackCommand(boolean authenticated, String formName, String secret, ContactFormValues values, long startTime) {
        this.authenticated = authenticated;
        this.formName = formName;
        this.secret = secret;
        this.values = values;
        this.startTime = startTime;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        final long waitTime=6000-(System.currentTimeMillis()-startTime);
        if(startTime>0) {
            if (waitTime > 0) {
                try {
                    Thread.sleep(waitTime);
                } catch (InterruptedException e) {
                    Timber.e(e);
                }
            }
        }else{
            Timber.w("startTime is 0");
        }
        if (authenticated) {
            executor.addContactFormFeedback(this, receiver, requestCode);
        } else {
            executor.addContactFormFeedbackAnonymous(this, receiver, requestCode);
        }
    }

    public static class ContactFormValues implements android.os.Parcelable {
        @SerializedName("field_prenom")
        private String surname;
        @SerializedName("field_nom")
        private String name;
        @SerializedName("field_email")
        private String email;
        @SerializedName("field_transmitter_quality")
        private String transmitterQuality;
        @SerializedName("field_identifiant_rpps")
        private String identifiantRpps;
        @SerializedName("field_subject")
        private String subject;
        @SerializedName("field_message")
        private String message;

        public ContactFormValues(String surname, String name, String email,
                                 String transmitterQuality, String identifiantRpps,
                                 String subject, String message) {
            this.surname = surname;
            this.name = name;
            this.email = email;
            this.transmitterQuality = transmitterQuality;
            this.identifiantRpps = identifiantRpps;
            this.subject = subject;
            this.message = message;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.surname);
            dest.writeString(this.name);
            dest.writeString(this.email);
            dest.writeString(this.transmitterQuality);
            dest.writeString(this.identifiantRpps);
            dest.writeString(this.subject);
            dest.writeString(this.message);
        }

        protected ContactFormValues(Parcel in) {
            this.surname = in.readString();
            this.name = in.readString();
            this.email = in.readString();
            this.transmitterQuality = in.readString();
            this.identifiantRpps = in.readString();
            this.subject = in.readString();
            this.message = in.readString();
        }

        public static final Creator<ContactFormValues> CREATOR = new Creator<ContactFormValues>() {
            @Override
            public ContactFormValues createFromParcel(Parcel source) {
                return new ContactFormValues(source);
            }

            @Override
            public ContactFormValues[] newArray(int size) {
                return new ContactFormValues[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.formName);
        dest.writeString(this.secret);
        dest.writeParcelable(this.values, flags);
        dest.writeByte(this.authenticated ? (byte) 1 : (byte) 0);
        dest.writeLong(this.startTime);
    }

    protected AddContactFormFeedbackCommand(Parcel in) {
        this.formName = in.readString();
        this.secret = in.readString();
        this.values = in.readParcelable(ContactFormValues.class.getClassLoader());
        this.authenticated = in.readByte() != 0;
        this.startTime = in.readLong();
    }

    public static final Creator<AddContactFormFeedbackCommand> CREATOR = new Creator<AddContactFormFeedbackCommand>() {
        @Override
        public AddContactFormFeedbackCommand createFromParcel(Parcel source) {
            return new AddContactFormFeedbackCommand(source);
        }

        @Override
        public AddContactFormFeedbackCommand[] newArray(int size) {
            return new AddContactFormFeedbackCommand[size];
        }
    };
}
