package com.adyax.srisgp.data.tracker;

import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;

/**
 * Created by SUVOROV on 3/7/17.
 */

public interface ITracker {
    void addScreen(TrackerAT.TrackerPageArg trackerPageArg);

    void sendScreen(TrackerAT.TrackerPageArg trackerPageArg);

    void addGesture(TrackerAT.TrackerPageArg trackerPageArg, boolean onePageClick);
    void addSearchInfo(SearchCommand searchCommand, SearchResponseHuge response);

    IHistoryRecentUpdateHelper getHistoryUpdateHelper();
}
