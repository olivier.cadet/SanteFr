package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 9/27/16.
 */

public class RecentLocationsResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public List<RecentLocationItem> items;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
    }

    public RecentLocationsResponse() {
    }

    protected RecentLocationsResponse(Parcel in) {
        this.items = new ArrayList<RecentLocationItem>();
        in.readList(this.items, RecentLocationItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<RecentLocationsResponse> CREATOR = new Parcelable.Creator<RecentLocationsResponse>() {
        @Override
        public RecentLocationsResponse createFromParcel(Parcel source) {
            return new RecentLocationsResponse(source);
        }

        @Override
        public RecentLocationsResponse[] newArray(int size) {
            return new RecentLocationsResponse[size];
        }
    };
}
