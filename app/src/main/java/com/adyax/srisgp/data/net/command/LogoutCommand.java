package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class LogoutCommand extends ServiceCommand {

    public LogoutCommand() {
    }

    protected LogoutCommand(Parcel in) {
    }

    public static final Creator<LogoutCommand> CREATOR = new Creator<LogoutCommand>() {
        @Override
        public LogoutCommand createFromParcel(Parcel in) {
            return new LogoutCommand(in);
        }

        @Override
        public LogoutCommand[] newArray(int size) {
            return new LogoutCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.logout(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
