package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class ResetCommand extends ServiceCommand implements INewFields{

    // Do not change username to email !!!!
    @SerializedName(USERNAME)
    public String username;

    public ResetCommand(Parcel in) {
        username = in.readString();
    }

    public static final Creator<ResetCommand> CREATOR = new Creator<ResetCommand>() {
        @Override
        public ResetCommand createFromParcel(Parcel in) {
            return new ResetCommand(in);
        }

        @Override
        public ResetCommand[] newArray(int size) {
            return new ResetCommand[size];
        }
    };

    public ResetCommand(String username) {
        this.username = username;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.reset(this, receiver, requestCode);
    }

}
