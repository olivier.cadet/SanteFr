package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.FavoriteUsers;
import com.adyax.srisgp.data.net.response.tags.FavoritesItem;
import com.adyax.srisgp.data.net.response.tags.NotificationUsers;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class FavoritesResponse extends BaseResponse implements Parcelable {

    @SerializedName("all")
    public FavoriteUsers all;

    @SerializedName("find")
    public FavoriteUsers find;

    @SerializedName("get_informed")
    public FavoriteUsers informed;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.all, flags);
        dest.writeParcelable(this.find, flags);
        dest.writeParcelable(this.informed, flags);
    }

    public FavoritesResponse() {
    }

    protected FavoritesResponse(Parcel in) {
        this.all = in.readParcelable(FavoriteUsers.class.getClassLoader());
        this.find = in.readParcelable(FavoriteUsers.class.getClassLoader());
        this.informed = in.readParcelable(FavoriteUsers.class.getClassLoader());
    }

    public static final Parcelable.Creator<FavoritesResponse> CREATOR = new Parcelable.Creator<FavoritesResponse>() {
        @Override
        public FavoritesResponse createFromParcel(Parcel source) {
            return new FavoritesResponse(source);
        }

        @Override
        public FavoritesResponse[] newArray(int size) {
            return new FavoritesResponse[size];
        }
    };
}
