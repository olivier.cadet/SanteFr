package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kirill on 22.11.16.
 */

public class NotifySettingsBody implements android.os.Parcelable {
    @SerializedName("notify_mobile")
    private int byMobile;

//    @SerializedName("by_email")
//    private int byEmail;

    @SerializedName("by_email_daily")
    private int byEmailDaily;

    @SerializedName("by_email_weekly")
    private int byEmailWeekly;

    public NotifySettingsBody(boolean byMobile, boolean byEmailDaily, boolean byEmailWeekly) {
        this.byMobile = byMobile ? 1 : 0;
//        this.byEmail = byEmail ? 1 : 0;
        this.byEmailDaily = byEmailDaily ? 1 : 0;
        this.byEmailWeekly = byEmailWeekly ? 1 : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.byMobile);
//        dest.writeInt(this.byEmail);
        dest.writeInt(this.byEmailDaily);
        dest.writeInt(this.byEmailWeekly);
    }

    protected NotifySettingsBody(Parcel in) {
        this.byMobile = in.readInt();
//        this.byEmail = in.readInt();
        this.byEmailDaily = in.readInt();
        this.byEmailWeekly = in.readInt();
    }

    public static final Creator<NotifySettingsBody> CREATOR = new Creator<NotifySettingsBody>() {
        @Override
        public NotifySettingsBody createFromParcel(Parcel source) {
            return new NotifySettingsBody(source);
        }

        @Override
        public NotifySettingsBody[] newArray(int size) {
            return new NotifySettingsBody[size];
        }
    };
}
