package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/19/16.
 */
public class SearchResult {

    @SerializedName("id")
    public long id;

    @SerializedName("title")
    public String title;

    @SerializedName("url")
    public String url;

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
