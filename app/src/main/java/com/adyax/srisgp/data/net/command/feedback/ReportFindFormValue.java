package com.adyax.srisgp.data.net.command.feedback;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by SUVOROV on 2/15/17.
 */

public class ReportFindFormValue implements Parcelable {

    @SerializedName(ReportBuilder.FIELD_ERROR_INFO)
    private String field_error_info;

    @SerializedName(ReportBuilder.FIELD_PROPOSED_INFO)
    private String field_proposed_info;

    @SerializedName(ReportBuilder.FIELD_URL)
    private String field_url;

    @SerializedName(ReportBuilder.FIELD_EMAIL)
    private String field_email;

    @SerializedName(ReportBuilder.FIELD_CONCERNED_PARTY)
    private String field_concerned_party;

    @SerializedName(ReportBuilder.FIELD_ERROR_ATTENTION_REASON)
    private String field_error_attention_reason;

    public ReportFindFormValue(Map<String, String> map){
        this.field_error_info = map.get(ReportBuilder.FIELD_ERROR_INFO);
        this.field_proposed_info = map.get(ReportBuilder.FIELD_PROPOSED_INFO);
        this.field_url = map.get(ReportBuilder.FIELD_URL);
        this.field_email = map.get(ReportBuilder.FIELD_EMAIL);
        this.field_concerned_party = map.get(ReportBuilder.FIELD_CONCERNED_PARTY);
        this.field_error_attention_reason = map.get(ReportBuilder.FIELD_ERROR_ATTENTION_REASON);
    }

    protected ReportFindFormValue(Parcel in) {
        field_error_info = in.readString();
        field_proposed_info = in.readString();
        field_url = in.readString();
        field_email = in.readString();
        field_concerned_party = in.readString();
        field_error_attention_reason = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(field_error_info);
        dest.writeString(field_proposed_info);
        dest.writeString(field_url);
        dest.writeString(field_email);
        dest.writeString(field_concerned_party);
        dest.writeString(field_error_attention_reason);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportFindFormValue> CREATOR = new Creator<ReportFindFormValue>() {
        @Override
        public ReportFindFormValue createFromParcel(Parcel in) {
            return new ReportFindFormValue(in);
        }

        @Override
        public ReportFindFormValue[] newArray(int size) {
            return new ReportFindFormValue[size];
        }
    };
}
