package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 11/18/16.
 */

public class FeedbackOption implements Parcelable {

    @SerializedName("value")
    public String value;
    @SerializedName("label")
    public String label;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.label);
    }

    public FeedbackOption() {
    }

    protected FeedbackOption(Parcel in) {
        this.value = in.readString();
        this.label = in.readString();
    }

    public static final Creator<FeedbackOption> CREATOR = new Creator<FeedbackOption>() {
        @Override
        public FeedbackOption createFromParcel(Parcel source) {
            return new FeedbackOption(source);
        }

        @Override
        public FeedbackOption[] newArray(int size) {
            return new FeedbackOption[size];
        }
    };

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
