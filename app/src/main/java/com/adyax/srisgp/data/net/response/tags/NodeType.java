package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 10/25/16.
 */

public enum NodeType {
    /*
        Attention!!! Need to modify ContentResolverHelper after added a new type
    */
    @SerializedName("apps")
    APPLICATIONS,

    @SerializedName("info_sheet")
    FICHES_INFOS,

    @SerializedName("ext_links")
    LIENS_EXTERNES,

    @SerializedName("video")
    VIDEO,

    @SerializedName("resources")
    NUMERO_TEL,

    @SerializedName("care_deals")
    OFFRES_DE_SOINS,

    @SerializedName("page")
    PAGE_SIMPLE,

    @SerializedName("professionnel_de_sante")
    PROFESSIONNEL_DE_SANTE,

    @SerializedName("service_de_sante")
    SERVICE_DE_SANTE,

    @SerializedName("health_institution")
    HEALTH_INSTITUTION,

    @SerializedName("dossier_thematic")
    ESTABLISSEMENT_DE_SANTE,

    @SerializedName("search_phrases_for_front")
    RECHERCHE_ENREGISTREE,

    @SerializedName("finess_institution")
    FINESS_INSTITUTION,

    @SerializedName("entite_geographique")
    ENTITE_GEOGRAPHIQUE,

    @SerializedName("dossier_thematic")
    DOSSIER_THEMATIC,

    @SerializedName("medicaments")
    MEDICAMENTS,

    @SerializedName("substances")
    SUBSTANCES,

    @SerializedName("ecl_hp_card")
    ESSAI_CLINIC,

    UNKNOWN

    /*
        Attention!!! Need to modify ContentResolverHelper after added a new type
    */
}
