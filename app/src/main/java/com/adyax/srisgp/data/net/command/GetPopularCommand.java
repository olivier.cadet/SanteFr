package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class GetPopularCommand implements Parcelable {
public class GetPopularCommand extends ServiceCommand {

    public Float latitude = 0.123F;
    public Float longitude = 10.456F;

    public GetPopularCommand() {
    }

    protected GetPopularCommand(Parcel in) {
        latitude = in.readFloat();
        longitude = in.readFloat();
    }

    public static final Creator<GetPopularCommand> CREATOR = new Creator<GetPopularCommand>() {
        @Override
        public GetPopularCommand createFromParcel(Parcel in) {
            return new GetPopularCommand(in);
        }

        @Override
        public GetPopularCommand[] newArray(int size) {
            return new GetPopularCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getPopular(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(latitude);
        parcel.writeFloat(longitude);
    }
}
