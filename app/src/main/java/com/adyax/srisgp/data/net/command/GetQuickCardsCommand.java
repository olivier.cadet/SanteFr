package com.adyax.srisgp.data.net.command;

import android.location.Location;
import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class GetQuickCardsCommand implements Parcelable {
public class GetQuickCardsCommand extends ServiceCommand {

    @SerializedName("latitude")
    private Double latitude;

    @SerializedName("longitude")
    private Double longitude;

    public GetQuickCardsCommand(Location location) {
        if (location != null) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }
    }

    protected GetQuickCardsCommand(Parcel in) {
        this.latitude = (Double) in.readValue(Double.class.getClassLoader());
        this.longitude = (Double) in.readValue(Double.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.latitude);
        dest.writeValue(this.longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<GetQuickCardsCommand> CREATOR = new Creator<GetQuickCardsCommand>() {
        @Override
        public GetQuickCardsCommand createFromParcel(Parcel in) {
            return new GetQuickCardsCommand(in);
        }

        @Override
        public GetQuickCardsCommand[] newArray(int size) {
            return new GetQuickCardsCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getQuickCards(this, receiver, requestCode);
    }
}
