package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class EmergencyServiceItem implements Parcelable{

    @SerializedName("name")
    public String name;

    @SerializedName("sid")
    public Long sid;

    protected EmergencyServiceItem(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            sid = null;
        } else {
            sid = in.readLong();
        }
    }

    public static final Creator<EmergencyServiceItem> CREATOR = new Creator<EmergencyServiceItem>() {
        @Override
        public EmergencyServiceItem createFromParcel(Parcel in) {
            return new EmergencyServiceItem(in);
        }

        @Override
        public EmergencyServiceItem[] newArray(int size) {
            return new EmergencyServiceItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        if (sid == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(sid);
        }
    }
}
