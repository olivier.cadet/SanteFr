package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kirill on 14.11.16.
 */

public class LoginUserSsoCommand extends LoginBaseServiceCommand implements INewFields{

    @SerializedName(USERNAME)
    private String email;

    @SerializedName("sso_token")
    private String token;

    @SerializedName("sso_type")
    private Sso_type ssoType;

    @SerializedName("device_type")
    private String deviceType = "android";

    @SerializedName("oauth_token_secret")
    private String twitterOathTokenSecret;

    @SerializedName("twitter_screen_name")
    private String twitterScreenName;

//    private transient boolean isFirstLogin;

//    public LoginUserSsoCommand(String email, String token, Sso_type ssoType) {
//        super("");
//        this.email = email;
//        this.token = token;
//        this.ssoType = ssoType;
//    }

    public LoginUserSsoCommand(CreateAccountCommand command) {
        super(command.getPushToken());
        this.email = command.mail;
        // sso_token
        this.token = command.token;
        this.ssoType = command.sso_type;
        this.twitterOathTokenSecret = command.twitterTokenSecret;
        this.twitterScreenName = command.twitterScreenName;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.loginUserSso(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.email);
        dest.writeString(this.token);
        dest.writeInt(this.ssoType == null ? -1 : this.ssoType.ordinal());
        dest.writeString(this.deviceType);
        dest.writeString(this.twitterOathTokenSecret);
        dest.writeString(this.twitterScreenName);
    }

    protected LoginUserSsoCommand(Parcel in) {
        super(in);
        this.email = in.readString();
        this.token = in.readString();
        int tmpSsoType = in.readInt();
        this.ssoType = tmpSsoType == -1 ? null : Sso_type.values()[tmpSsoType];
        this.deviceType = in.readString();
        this.twitterOathTokenSecret = in.readString();
        this.twitterScreenName = in.readString();
    }

    public static final Creator<LoginUserSsoCommand> CREATOR = new Creator<LoginUserSsoCommand>() {
        @Override
        public LoginUserSsoCommand createFromParcel(Parcel source) {
            return new LoginUserSsoCommand(source);
        }

        @Override
        public LoginUserSsoCommand[] newArray(int size) {
            return new LoginUserSsoCommand[size];
        }
    };

//    public boolean isFirstLogin() {
//        return isFirstLogin;
//    }
//
//    public LoginUserSsoCommand setFirstLogin() {
//        isFirstLogin = true;
//        return this;
//    }
}
