package com.adyax.srisgp.data.gson_extention;

import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

/**
 * Created by anton.kobylianskiy on 11/3/16.
 */

public class RelatedFiltersDeserializer implements JsonDeserializer<RelatedFilters> {
    @Override
    public RelatedFilters deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Set<Map.Entry<String, JsonElement>> entries = ((JsonObject) json).entrySet();
        if (!entries.isEmpty()) {
            Map.Entry<String, JsonElement> element = entries.iterator().next();
            return new RelatedFilters(element.getKey(), element.getValue().getAsString());

        }
        return null;
    }
}
