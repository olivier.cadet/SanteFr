package com.adyax.srisgp.data.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.text.TextUtils;
import android.util.Base64;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.UserResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.User;
import com.adyax.srisgp.mvp.emergency.EmergencyArg;
import com.adyax.srisgp.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import me.leolin.shortcutbadger.ShortcutBadger;
import timber.log.Timber;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class SharedPreferencesHelper implements IRepository {

    private static final String KEY_APP_VER = "app_version";
    private static final String KEY_FIRST_RUN = "first_run";
    private static final String SHOW_GEO_REQUEST = "show_geo_request";

    private static final String SHARED_PREFERENCES_NAME = "settings";
    public static final String URGENT_ALERT = "urgent_alert";
    public static final String PASS = "SESSION_STATUS";
    public static final String LOCATION = "location";
    public static final String URGENT_ALERT_CONTENT = "urgent_alert_content";
    public static final String READ_SEARCH_MESSAGES = "read_search_messages";

    public static final AlertItem EMPTY_ALERT_ITEM = null;
    public static final String СONFIGURATION = "сonfiguration";
    public static final String LAST_SEARCH_REQUEST = "LastSearchRequest";

    public static final String USER_PASSWORD = "user_password";
    public static final String USER_SSO_CREDENTIALS = "user_sso_creds";

    private final SharedPreferences sharedPreferences;
    private Context context;

    public static final String NO_ACTION = "NO_ACTION";
    public static final String RESET = "RESET";

    private Set<LoggedInStateListener> loggedInStateListeners = new HashSet<>();

    // TODO it is path!!! need to replace
    private String temporaryHeader;

    private SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(RegisterUserResponse.TOKEN)) {
                Timber.d("Token value changed");
                String token = sharedPreferences.getString(RegisterUserResponse.TOKEN, null);
                if (token == null) {
                    for (LoggedInStateListener listener : loggedInStateListeners) {
                        listener.onLoggedOut();
                    }
                } else {
                    for (LoggedInStateListener listener : loggedInStateListeners) {
                        listener.onLoggedIn();
                    }
                }
            }
        }
    };

    public SharedPreferencesHelper(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.sharedPreferences.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
        saveLastUrgentAlert(EMPTY_ALERT_ITEM);
    }

    @Override
    public void addLoggedInStateListener(LoggedInStateListener loggedInStateListener) {
        loggedInStateListeners.add(loggedInStateListener);
    }

    @Override
    public void removeLoggedInStateListener(LoggedInStateListener loggedInStateListener) {
        loggedInStateListeners.remove(loggedInStateListener);
    }

    @Override
    public void saveLastUrgentAlert(AlertItem alertItem) {
        sharedPreferences.edit()
                .putLong(URGENT_ALERT, alertItem != EMPTY_ALERT_ITEM ? alertItem.getId() : -1)
                .putString(URGENT_ALERT_CONTENT, alertItem != EMPTY_ALERT_ITEM ? alertItem.getContentUrl() : null)
                .apply();
    }

    @Override
    public AlertItem getLastUrgentAlert() {
        AlertItem alertItem = new AlertItem();
        alertItem.id = sharedPreferences.getLong(URGENT_ALERT, -1);
        alertItem.content_url = sharedPreferences.getString(URGENT_ALERT_CONTENT, null);
        return alertItem;
    }

    private void saveEncryptedData(SharedPreferences.Editor editor, String key, String value) {
        String keyBase64String = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP);
        String valueBase64String = Base64.encodeToString(value.getBytes(), Base64.NO_WRAP);
        editor.putString(keyBase64String, valueBase64String);

    }

    private String getEncryptedData(SharedPreferences preferences, String key) {
        String keyBase64String = Base64.encodeToString(key.getBytes(), Base64.NO_WRAP);
        String base64EncryptedString = preferences.getString(keyBase64String, "default");
        byte[] encryptedBytes = Base64.decode(base64EncryptedString, Base64.NO_WRAP);
        final String value = new String(encryptedBytes);
        return value;
    }

    @Override
    public void saveCredential(RegisterUserResponse user, String password) {
        if (user == null) {
            return;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(RegisterUserResponse.SESSID, user.getSessid());
        editor.putString(RegisterUserResponse.SESSION_NAME, user.getSession_name());
        editor.putString(RegisterUserResponse.TOKEN, user.getToken());

        if (user.user != null) {
            editor.putString(RegisterUserResponse.USER, user.user.toString());
        } else {
            editor.putString(RegisterUserResponse.USER, new User().toString());
        }
        if (password != null) {
            switch (password) {
                case RESET:
                    editor.putString(PASS, null);
                    break;
                case NO_ACTION:
                    break;
                default:
                    editor.putString(PASS, Utils.sha1Hash(password));
                    this.saveEncryptedData(editor, USER_PASSWORD, password);
                    break;
            }
        } else {
            editor.putString(PASS, null);
        }

        editor.apply();
    }

    public void saveUser(UserResponse userResponse) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (userResponse.user != null) {
            editor.putString(RegisterUserResponse.USER, userResponse.user.toString());
        } else {
            editor.putString(RegisterUserResponse.USER, new User().toString());
        }

        editor.putString(UserResponse.UID, userResponse.user.uid);
        editor.putString(UserResponse.INTERESTS_NODE_COUNT, new Gson().toJson(userResponse.interestsNodeCount));
        editor.apply();
    }

    public String getUserUID() {
        return sharedPreferences.getString(UserResponse.UID, "0");
    }

    public String getUserPassword() {
        return getEncryptedData(sharedPreferences, USER_PASSWORD);
    }

    public String getUserSsoCredentials() {
        return getEncryptedData(sharedPreferences, USER_SSO_CREDENTIALS);
    }


    public Map<Long, Integer> getInterestCount() {
        final String string = sharedPreferences.getString(UserResponse.INTERESTS_NODE_COUNT, null);
        return new Gson().fromJson(string, new TypeToken<Map<Long, Integer>>() {
        }.getType());
    }

    @Override
    public boolean checkCredential(ChangeCredentialsCommand changeCredentialsCommand) {
        if (changeCredentialsCommand.isChangeEmail()) {
//            String string = sharedPreferences.getString(RegisterUserResponse.USER, null);
//            return TextUtils.equals(string, changeCredentialsCommand.getEmail());
            return true;
        } else {
            return TextUtils.equals(sharedPreferences.getString(PASS, null),
                    Utils.sha1Hash(changeCredentialsCommand.getOldPassword()));
        }
    }

    @Override
    public void updateCredentials(ChangeCredentialsCommand changeCredentialsCommand) {
        if (changeCredentialsCommand.isChangeEmail()) {
            RegisterUserResponse registerUserResponse = getCredential();
            User user = registerUserResponse.user;
            if (user != null) {
                user.mail = changeCredentialsCommand.getEmail();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(RegisterUserResponse.USER, user.toString());
                editor.apply();
            }
        } else {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(PASS, Utils.sha1Hash(changeCredentialsCommand.getNewPassword()));
            editor.apply();
        }
    }

    @Override
    public void saveGeoLocation(Location location) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (location != null) {//"50.004000,36.240000,10.000000");
            if (location != null) {
                final double latitude = location.getLatitude();
                final double longitude = location.getLongitude();
                final float accuracy = location.getAccuracy();
                final String latitude_str = String.valueOf(latitude);
                final String longitude_str = String.valueOf(longitude);
                final String accuracy_str = String.valueOf(accuracy);
                editor.putString(LOCATION, String.format("%s,%s,%s", latitude_str, longitude_str, accuracy_str));
            } else {
                editor.putString(LOCATION, null);
            }
        } else {
            editor.putString(LOCATION, null);
        }
        editor.apply();
    }

    @Override
    public String getGeoLocation() {
        return sharedPreferences.getString(LOCATION, null);
    }

    @Override
    public void setNewNotificationCount(int newNotificationCount) {
        if (newNotificationCount == 0) {
            ShortcutBadger.removeCount(context);
        } else {
            ShortcutBadger.applyCount(context, newNotificationCount);
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(UserResponse.NEW_NOTIFICATIONS, newNotificationCount);
        editor.apply();
    }

    @Override
    public int getNewNotificationCount() {
        return sharedPreferences.getInt(UserResponse.NEW_NOTIFICATIONS, 0);
    }

    @Override
    public RegisterUserResponse getCredential() {
        final RegisterUserResponse registerUserResponse = new RegisterUserResponse();
        registerUserResponse.setSessid(sharedPreferences.getString(RegisterUserResponse.SESSID, ""));
        registerUserResponse.setToken(sharedPreferences.getString(RegisterUserResponse.TOKEN, ""));
        registerUserResponse.setSession_name(sharedPreferences.getString(RegisterUserResponse.SESSION_NAME, ""));
        registerUserResponse.setUser(User.fromString(sharedPreferences.getString(RegisterUserResponse.USER, "")));
        return registerUserResponse;
    }

    @Override
    public void reset() {
        saveCredential(new RegisterUserResponse(), SharedPreferencesHelper.RESET);
        setNewNotificationCount(0);
    }

    @Override
    public void setTemporaryHeader(String temporaryHeader) {
        if (!TextUtils.equals(this.temporaryHeader, temporaryHeader)) {
            this.temporaryHeader = temporaryHeader;
        }
    }

    @Override
    public String getTemporaryHeader() {
        return temporaryHeader;
    }

    @Override
    public boolean appVersionHasChanged() {
        int prevVersion = sharedPreferences.getInt(KEY_APP_VER, 0);
        return prevVersion < BuildConfig.VERSION_CODE;
    }

    @Override
    public void setAppVersion() {
        sharedPreferences.edit().putInt(KEY_APP_VER, BuildConfig.VERSION_CODE).apply();
    }

    @Override
    public boolean isFirstRun() {
        return sharedPreferences.getBoolean(KEY_FIRST_RUN, true);
    }

    @Override
    public void setNotFirstRun() {
        if (isFirstRun()) {
            sharedPreferences.edit().putBoolean(KEY_FIRST_RUN, false).apply();
        }
    }

    public interface LoggedInStateListener {
        void onLoggedIn();

        void onLoggedOut();
    }

    @Override
    public boolean hasBeenShownGeoRequest() {
        return false;
//        return sharedPreferences.getBoolean(SHOW_GEO_REQUEST, false);
    }

    @Override
    public void setStatusShowGeoRequest() {
        if (!hasBeenShownGeoRequest()) {
            sharedPreferences.edit().putBoolean(SHOW_GEO_REQUEST, true).apply();
        }
    }

    @Override
    public void saveConfig(ConfigurationResponse configurationResponse) {
        sharedPreferences.edit().putString(СONFIGURATION,
                new Gson().toJson(configurationResponse)).apply();
    }

    @Override
    public ConfigurationResponse getConfig() {
        return new Gson().fromJson(sharedPreferences.getString(СONFIGURATION,
                new Gson().toJson(new ConfigurationResponse(context))), ConfigurationResponse.class);
    }

    @Override
    public void saveLastSearchRequest(SearchCommand searchCommand) {
        sharedPreferences.edit().putString(LAST_SEARCH_REQUEST + searchCommand.getType().name(),
                new Gson().toJson(searchCommand)).apply();
    }

    @Override
    public SearchCommand readLastSearchRequest(SearchType type) {
        return new Gson().fromJson(sharedPreferences.getString(LAST_SEARCH_REQUEST + type.name(),
                new Gson().toJson(new SearchCommand())), SearchCommand.class);
    }

    @Override
    public void saveVote(EmergencyCommand emergencyCommand) {
        sharedPreferences.edit().putInt(emergencyCommand.getKey(), emergencyCommand.getValue().intValue()).apply();
    }

    @Override
    public int getVote(EmergencyArg emergencyArg) {
        EmergencyCommand emergencyCommand = new EmergencyCommand(emergencyArg.getNid(), emergencyArg.getSid(), -1);
        final int value = sharedPreferences.getInt(emergencyCommand.getKey(), -1);
        return value == -1 ? 0 : value;
    }

    @Override
    public boolean isVote(EmergencyArg emergencyArg) {
        EmergencyCommand emergencyCommand = new EmergencyCommand(emergencyArg.getNid(), emergencyArg.getSid(), -1);
        return sharedPreferences.getInt(emergencyCommand.getKey(), -1) != -1;
    }
}
