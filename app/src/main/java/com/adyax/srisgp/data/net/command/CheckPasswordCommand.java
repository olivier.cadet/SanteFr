package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class LoginUserCommand implements Parcelable {
public class CheckPasswordCommand extends ServiceCommand implements INewFields{

    @SerializedName(PASSWORD)
    public String password;


    public CheckPasswordCommand(ChangeCredentialsCommand changeCredentialsCommand) {
        this.password = changeCredentialsCommand.getOldPassword();
    }

    protected CheckPasswordCommand(Parcel in) {
        password = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(password);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CheckPasswordCommand> CREATOR = new Creator<CheckPasswordCommand>() {
        @Override
        public CheckPasswordCommand createFromParcel(Parcel in) {
            return new CheckPasswordCommand(in);
        }

        @Override
        public CheckPasswordCommand[] newArray(int size) {
            return new CheckPasswordCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.checkPassword(this, receiver, requestCode);
    }
}
