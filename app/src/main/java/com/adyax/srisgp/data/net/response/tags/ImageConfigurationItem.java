package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class ImageConfigurationItem implements Parcelable{

    public static final String HEADER_LOGO_IOS = "header_logo_ios";
    public static final String HEADER_LOGO_ANDROID = "header_logo_android";
    public static final String HEADER_LOGO_SUBTITLE = "logo_subtitle";

    @SerializedName(HEADER_LOGO_IOS)
    public String header_logo_ios;

    @SerializedName(HEADER_LOGO_ANDROID)
    public String header_logo_android;

    @SerializedName(HEADER_LOGO_SUBTITLE)
    public String logo_subtitle;

    protected ImageConfigurationItem(Parcel in) {
        header_logo_ios = in.readString();
        header_logo_android = in.readString();
        logo_subtitle = in.readString();
    }

    public static final Creator<ImageConfigurationItem> CREATOR = new Creator<ImageConfigurationItem>() {
        @Override
        public ImageConfigurationItem createFromParcel(Parcel in) {
            return new ImageConfigurationItem(in);
        }

        @Override
        public ImageConfigurationItem[] newArray(int size) {
            return new ImageConfigurationItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(header_logo_ios);
        parcel.writeString(header_logo_android);
        parcel.writeString(logo_subtitle);
    }

    public String getHeader_logo_android() {
        return header_logo_android;
    }

    public String getHeader_logo_subtitle() {
        return logo_subtitle;
    }

}
