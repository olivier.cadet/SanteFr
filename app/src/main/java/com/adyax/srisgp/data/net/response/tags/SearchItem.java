package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by SUVOROV on 9/19/16.
 */
@DatabaseTable(tableName = "search_item")
public class SearchItem {

    public static final String ID = "id";
    public static final String PHRASE = "phrase";

    @SerializedName(ID)
    @DatabaseField(columnName = ID, unique = true)
    public long id;

    @SerializedName(PHRASE)
    @DatabaseField(columnName = PHRASE, unique = true)
    public String phrase;

    public SearchItem(long id, String phrase) {
        this.id = id;
        this.phrase = phrase;
    }

    public SearchItem() {
    }

    public static SearchItem createLocal(String phrase) {
        return new SearchItem(System.currentTimeMillis(), phrase);
    }
}
