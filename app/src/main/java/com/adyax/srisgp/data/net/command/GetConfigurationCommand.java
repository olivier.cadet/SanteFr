package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 5/12/17.
 */

public class GetConfigurationCommand extends ServiceCommand {

    private List<String> sections;

    public GetConfigurationCommand() {
        this.sections = new ArrayList<>();
        this.sections.add(ConfigurationResponse.IMAGES);
        this.sections.add(ConfigurationResponse.SEARCH);
        this.sections.add(SearchItemHuge.CONTENT_MAINTENANCE);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getConfiguration(this, receiver, requestCode);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.sections);
    }

    protected GetConfigurationCommand(Parcel in) {
        this.sections = in.createStringArrayList();
    }

    public static final Creator<GetConfigurationCommand> CREATOR = new Creator<GetConfigurationCommand>() {
        @Override
        public GetConfigurationCommand createFromParcel(Parcel source) {
            return new GetConfigurationCommand(source);
        }

        @Override
        public GetConfigurationCommand[] newArray(int size) {
            return new GetConfigurationCommand[size];
        }
    };
}
