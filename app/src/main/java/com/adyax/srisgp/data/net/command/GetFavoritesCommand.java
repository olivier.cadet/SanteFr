package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.favorite.FavoriteType;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class GetFavoritesCommand implements Parcelable {
public class GetFavoritesCommand extends ServiceCommand {

    // TODO Hardcore
    private String[] statuses = new String[]{"all", "find", "get_informed"};
    private int page_offset = 0;
    private int items_per_page = 10;
    private int page = 0;

    public GetFavoritesCommand(int pageOffset, FavoriteType type) {
        this.page_offset = pageOffset;
        this.statuses = new String[]{type.name()};
    }

    public int getPage_offset() {
        return page_offset;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getFavorites(this, receiver, requestCode);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(this.statuses);
        dest.writeInt(this.page_offset);
        dest.writeInt(this.items_per_page);
        dest.writeInt(this.page);
    }

    protected GetFavoritesCommand(Parcel in) {
        this.statuses = in.createStringArray();
        this.page_offset = in.readInt();
        this.items_per_page = in.readInt();
        this.page = in.readInt();
    }

    public static final Creator<GetFavoritesCommand> CREATOR = new Creator<GetFavoritesCommand>() {
        @Override
        public GetFavoritesCommand createFromParcel(Parcel source) {
            return new GetFavoritesCommand(source);
        }

        @Override
        public GetFavoritesCommand[] newArray(int size) {
            return new GetFavoritesCommand[size];
        }
    };
}
