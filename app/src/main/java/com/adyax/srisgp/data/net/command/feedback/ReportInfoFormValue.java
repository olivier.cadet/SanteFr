package com.adyax.srisgp.data.net.command.feedback;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 2/15/17.
 */

public class ReportInfoFormValue implements Parcelable {

    @SerializedName(ReportBuilder.FIELD_ERROR_INFO)
    private String field_error_info;

    @SerializedName(ReportBuilder.FIELD_PROPOSED_INFO)
    private String field_proposed_info;

    @SerializedName(ReportBuilder.FIELD_URL)
    private String field_url;

    @SerializedName(ReportBuilder.FIELD_EMAIL)
    private String field_email;

    public ReportInfoFormValue(Map<String, String> map){
        this.field_error_info = map.get(ReportBuilder.FIELD_ERROR_INFO);
        this.field_proposed_info = map.get(ReportBuilder.FIELD_PROPOSED_INFO);
        this.field_url = map.get(ReportBuilder.FIELD_URL);
        this.field_email = map.get(ReportBuilder.FIELD_EMAIL);
    }

    protected ReportInfoFormValue(Parcel in) {
        field_error_info = in.readString();
        field_proposed_info = in.readString();
        field_url = in.readString();
        field_email = in.readString();
    }

    public static final Creator<ReportInfoFormValue> CREATOR = new Creator<ReportInfoFormValue>() {
        @Override
        public ReportInfoFormValue createFromParcel(Parcel in) {
            return new ReportInfoFormValue(in);
        }

        @Override
        public ReportInfoFormValue[] newArray(int size) {
            return new ReportInfoFormValue[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(field_error_info);
        parcel.writeString(field_proposed_info);
        parcel.writeString(field_url);
        parcel.writeString(field_email);
    }
}
