package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class CompleteCommand implements Parcelable {
public class SgpLocationCompleteCommand extends ServiceCommand {

    public String loc;
    public Integer strict;

    public SgpLocationCompleteCommand(String loc, Integer strict) {
        this.loc = loc;
        this.strict = strict;
    }
    
    protected SgpLocationCompleteCommand(Parcel in) {
        loc = in.readString();
        strict = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(loc);
        dest.writeInt(strict);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SgpLocationCompleteCommand> CREATOR = new Creator<SgpLocationCompleteCommand>() {
        @Override
        public SgpLocationCompleteCommand createFromParcel(Parcel in) {
            return new SgpLocationCompleteCommand(in);
        }

        @Override
        public SgpLocationCompleteCommand[] newArray(int size) {
            return new SgpLocationCompleteCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.sgpLocationComplete(this, receiver, requestCode);
    }

    public String getLoc() {
        return loc;
    }

    public Integer getStrict() {
        return strict;
    }
}
