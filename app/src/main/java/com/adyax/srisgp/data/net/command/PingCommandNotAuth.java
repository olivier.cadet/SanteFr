package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class PingCommandNotAuth extends ServiceCommand {

    public PingCommandNotAuth() {
    }

    protected PingCommandNotAuth(Parcel in) {
    }

    public static final Creator<PingCommandNotAuth> CREATOR = new Creator<PingCommandNotAuth>() {
        @Override
        public PingCommandNotAuth createFromParcel(Parcel in) {
            return new PingCommandNotAuth(in);
        }

        @Override
        public PingCommandNotAuth[] newArray(int size) {
            return new PingCommandNotAuth[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.ping_not_auth(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
