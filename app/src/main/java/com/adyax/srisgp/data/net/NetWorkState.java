package com.adyax.srisgp.data.net;

import android.content.Context;

import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.db.IDataBaseHelper;

/**
 * Created by SUVOROV on 9/26/16.
 */

public class NetWorkState implements INetWorkState{

    private Context context;
    private IRepository repository;
    private IDataBaseHelper dataBaseHelper;

    public NetWorkState(Context context, IRepository repository, IDataBaseHelper dataBaseHelper) {
        this.context = context;
        this.repository = repository;
        this.dataBaseHelper = dataBaseHelper;
    }

    // TODO need to check real networking type
    @Override
    public boolean isLoggedIn() {
        if(ExecutionService.isConnected(context)) {
            RegisterUserResponse credential = repository.getCredential();
            return credential != null && !credential.isEmpty();
        }
        return false;
    }

    @Override
    public void logOut() {
        repository.reset();
        dataBaseHelper.reset();
    }
}
