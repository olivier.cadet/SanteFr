package com.adyax.srisgp.data.net.response;

import com.google.gson.annotations.SerializedName;

public class UrgenceResponse extends BaseResponse {
    public static final String URL = "url";
    public static final String TXT = "text";

    @SerializedName(URL)
    public String url;

    @SerializedName(TXT)
    public String text;
}
