package com.adyax.srisgp.data.net.response.tags;

import android.database.Cursor;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.db.IExpandArray;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by SUVOROV on 9/20/16.
 */
@DatabaseTable(tableName = "related_items")
public class RelatedItem  implements IExpandArray {

    public static final String SEARCH_TYPE = "type_item";
    @DatabaseField(generatedId = true, allowGeneratedIdInsert=true, columnName = "id_item", canBeNull = false)
    public long id;

    @DatabaseField(foreign=true, columnName="parent_id_item", uniqueCombo = true)
    public Related related;

    @SerializedName("type")
    @DatabaseField(columnName = SEARCH_TYPE, uniqueCombo = true)
    public SearchType type;

    @SerializedName("filters")
//    @ForeignCollectionField()
    public RelatedFilters filters;

    @SerializedName("title")
    @DatabaseField(columnName = "title_item", uniqueCombo = true)
    public String title;

    public RelatedItem(){
    }

    @Override
    public void collapse() {

    }

    @Override
    public void expand(Cursor cursor) {
        filters=new RelatedFilters();
        BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, filters);
    }

    public void setParendId(Related related) {
        this.related = related;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RelatedFilters getFilter() {
//        return filters.toArray(new RelatedFilters[filters.size()])[0];
        return filters;
    }

    public void setType(SearchType searchType) {
        type=searchType;
    }
}
