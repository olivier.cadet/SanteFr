package com.adyax.srisgp.data.net.core;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.executor.FailAction;
import com.adyax.srisgp.data.net.executor.SuccessAction;
import com.adyax.srisgp.data.net.response.BaseResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.google.gson.Gson;

//import org.apache.commons.io.IOUtils;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 05.10.2016.
 */
public class BaseCall<T extends BaseResponse> {

    public static final String HTTP_503_SERVICE_UNAVAILABLE = "HTTP 503 Service unavailable";

    public static boolean debugValue=false;

    final private Observable<T> observable;

    public BaseCall(Observable<T> observable) {
        this.observable = observable;
    }

    public Observable<? extends BaseResponse> getObservable() {
        return observable;
    }

    private int getResponseCode(Throwable throwable) {
        int responseCode;
        if (throwable instanceof HttpException) {
            responseCode = ((HttpException) throwable).response().code();
        } else {
            responseCode = -1;
        }

        if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED ||
                responseCode == HttpURLConnection.HTTP_FORBIDDEN) {
//            App.instance().notifyTokenExpired();
        }

        return responseCode;
    }

    public void subscribe(final SuccessAction<T> successAction, final FailAction failAction) {
        observable.subscribe(t -> {
            if (t==null|| t.isSuccess()) {
                boolean debug=false;
                if(BuildConfig.DEBUG) {
                    debug = debugValue;
                }
                if(debug){
                    failAction.onFail(new Gson().toJson(new ErrorResponse(ErrorItem.MAINTENANCE_MODE, HTTP_503_SERVICE_UNAVAILABLE)), ErrorItem.MAINTENANCE_MODE);
                }else {
                    successAction.onSuccess(t);
                }
            } else {
                failAction.onFail(t.getErrorMessage(null), -1);
            }
        }, throwable -> {
            final int responseCode = getResponseCode(throwable);
            switch(responseCode){
                case 401:
                case 403:
                case 406:
                    if(throwable instanceof HttpException){
                        try {
                            final String errorMessage = getErrorStr((HttpException) throwable);
                            if (errorMessage != null && errorMessage.length() > 1) {
                                failAction.onFail(errorMessage, responseCode);
                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                default://HTTP 503 Service unavailable
                    failAction.onFail(throwable.getMessage(), responseCode);
                    break;
                case ErrorItem.MAINTENANCE_MODE:
                    failAction.onFail(new Gson().toJson(new ErrorResponse(responseCode, throwable.getMessage())), responseCode);
                    break;
            }
        });
    }

    @NonNull
    private String getErrorStr(HttpException throwable) throws IOException {
        InputStream inputStream= throwable.response().errorBody().byteStream();
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, "UTF-8");
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        return out.toString();
    }

}
