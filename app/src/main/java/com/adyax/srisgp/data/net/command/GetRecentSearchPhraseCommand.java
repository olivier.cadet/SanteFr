package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class GetRecentSearchPhraseCommand extends ServiceCommand {

    public GetRecentSearchPhraseCommand() {
    }

    protected GetRecentSearchPhraseCommand(Parcel in) {
    }

    public static final Creator<GetRecentSearchPhraseCommand> CREATOR = new Creator<GetRecentSearchPhraseCommand>() {
        @Override
        public GetRecentSearchPhraseCommand createFromParcel(Parcel in) {
            return new GetRecentSearchPhraseCommand(in);
        }

        @Override
        public GetRecentSearchPhraseCommand[] newArray(int size) {
            return new GetRecentSearchPhraseCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getRecentSearchPhrase(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
