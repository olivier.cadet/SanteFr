package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.history.HistoryType;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class GetHistoryCommand implements Parcelable {
public class GetHistoryCommand extends ServiceCommand {

    private String type = "info";
    private int page_offset = 0;
    private int items_per_page = 20;
    private int page = 0;

    public GetHistoryCommand(int pageOffset, HistoryType type) {
        this.page_offset = pageOffset;
        this.type = type.name();
    }

    public int getPage_offset() {
        return page_offset;
    }

    public HistoryType getType() {
        return HistoryType.valueOf(type);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getHistory(this, receiver, requestCode);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.type);
        dest.writeInt(this.page_offset);
        dest.writeInt(this.items_per_page);
        dest.writeInt(this.page);
    }

    protected GetHistoryCommand(Parcel in) {
        this.type = in.readString();
        this.page_offset = in.readInt();
        this.items_per_page = in.readInt();
        this.page = in.readInt();
    }

    public static final Creator<GetHistoryCommand> CREATOR = new Creator<GetHistoryCommand>() {
        @Override
        public GetHistoryCommand createFromParcel(Parcel source) {
            return new GetHistoryCommand(source);
        }

        @Override
        public GetHistoryCommand[] newArray(int size) {
            return new GetHistoryCommand[size];
        }
    };
}
