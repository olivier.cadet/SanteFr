package com.adyax.srisgp.data.net.command;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class AddToHistoryCommand implements Parcelable {
public class AddToHistoryCommand extends ServiceCommand {

    public String type;
    public String location;
    public String text;
    public Double latitude;
    public Double longitude;

    public AddToHistoryCommand(Context context, SearchCommand searchCommand, SearchResponseHuge response) {
        super();
        location = searchCommand.getCityLocation();
        text = searchCommand.getSearchText();
        type = searchCommand.getType().toString();
        latitude = searchCommand.getLatitude();
        longitude = searchCommand.getLongitude();
//        if(location==null&& searchCommand.isGeoLocation()){
//            location=context.getString(R.string.search_around_me);
//        }
    }

    protected AddToHistoryCommand(Parcel in) {
        type = in.readString();
        location = in.readString();
        text = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(location);
        dest.writeString(text);
        dest.writeDouble(latitude==null? 0: latitude);
        dest.writeDouble(longitude==null? 0: longitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AddToHistoryCommand> CREATOR = new Creator<AddToHistoryCommand>() {
        @Override
        public AddToHistoryCommand createFromParcel(Parcel in) {
            return new AddToHistoryCommand(in);
        }

        @Override
        public AddToHistoryCommand[] newArray(int size) {
            return new AddToHistoryCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.addToHistory(this, receiver, requestCode);
    }

}
