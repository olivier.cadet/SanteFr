package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import androidx.annotation.StringDef;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class TaxonomyVocabularyTermsCommand extends ServiceCommand {

    @Retention(SOURCE)
    @StringDef({
            INTERESTS, KEYWORDS
    })
    public @interface Vocabulary {
    }

    public static final String INTERESTS = "interests";
    public static final String KEYWORDS = "mots_cles";

    private String vocabulary;

    public TaxonomyVocabularyTermsCommand(@Vocabulary String vocabulary) {
        this.vocabulary = vocabulary;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getTaxonomyVocabularyTerms(this, receiver, requestCode);
    }

    public String getVocabulary() {
        return vocabulary;
    }

    /**
     * Parcelable
     */

    public TaxonomyVocabularyTermsCommand(Parcel in) {
        vocabulary = in.readString();
    }

    public static final Creator<TaxonomyVocabularyTermsCommand> CREATOR = new Creator<TaxonomyVocabularyTermsCommand>() {
        @Override
        public TaxonomyVocabularyTermsCommand createFromParcel(Parcel in) {
            return new TaxonomyVocabularyTermsCommand(in);
        }

        @Override
        public TaxonomyVocabularyTermsCommand[] newArray(int size) {
            return new TaxonomyVocabularyTermsCommand[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(vocabulary);
    }

}
