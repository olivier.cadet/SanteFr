package com.adyax.srisgp.data.net.response.tags;

import android.database.Cursor;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.SearchItemType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.db.IExpandArray;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by SUVOROV on 9/19/16.
 */
@DatabaseTable(tableName = "cards")
public class QuickCardsAnonymousItem implements IGetUrl, IExpandArray, INewFields {

    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String UO_TYPE = "uo_type";
    public static final String FAVORITE = "favorite";
    public static final String FICHE_TYPE = "fiche_type";
    public static final String UPDATE_DATE = "update_date";
    public static final String NODE_TYPE = "node_type";

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(NID)
    @DatabaseField(columnName = NID, unique = true)
    public long nid;

    @SerializedName(UO_TYPE)
    @DatabaseField(columnName = UO_TYPE)
    public String uo_type;

    @SerializedName(URL)
    @DatabaseField(columnName = URL_DB)
    public String url;

    @SerializedName(PROPOSE)
    @DatabaseField(columnName = PROPOSE_DB)
    public String propose;

    // TODO: add to database
    @SerializedName(FICHE_TYPE)
//    @DatabaseField(columnName = FICHE_TYPE)
    public SearchItemType ficheType;

    @SerializedName(UPDATE_DATE)
    @DatabaseField(columnName = UPDATE_DATE)
    public String updateDate;

    // it field absent in json
    @SerializedName(FAVORITE)
    @DatabaseField(columnName = FAVORITE)
    public long favorite;

    @SerializedName(PHONE_NUMBER)
    @DatabaseField(columnName = PHONE_NUMBER)
    public String phoneNumber;

    @SerializedName(NODE_TYPE)
    @DatabaseField(columnName = NODE_TYPE)
    public NodeType nodeType;

    @SerializedName(APP_STORE)
    @DatabaseField(columnName = APP_STORE_DB)
    public String apple;

    @SerializedName(PLAY_STORE)
    @DatabaseField(columnName = PLAY_STORE_DB)
    public String google;

    @SerializedName(IMAGE)
    @DatabaseField(columnName = IMAGE)
    public String image;

    @SerializedName(LINK_TITLE)
    @DatabaseField(columnName = LINK_TITLE)
    public String linkTitle;


    public QuickCardsAnonymousItem() {
    }

    public String getContentUrl() {
        return url;
//        try {
//            if(url!=null)
//                return new URI(url);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    public URI getContentUri() {
        try {
            if (url != null)
                return new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isFavorite() {
        return favorite > 0;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite ? 1 : 0;
    }

    public long getId() {
        return nid;
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getCardLink() {
        return getContentUrl();
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return nid;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public NodeType getNodeType() {
        if (nodeType == null)
            return NodeType.UNKNOWN;
        return nodeType;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return propose;
    }


    public boolean isSimplePage() {
        return nodeType == NodeType.PAGE_SIMPLE || nodeType == NodeType.RECHERCHE_ENREGISTREE;
    }

    @Override
    public String getPhone() {
        return phoneNumber;
    }

    @Override
    public void collapse() {
        if (xiti != null) {
            xiti_str = new Gson().toJson(xiti);
        }
    }

    @Override
    public void expand(Cursor cursor) {
        if (xiti_str != null) {
            xiti = new Gson().fromJson(xiti_str, Xiti.class);
        }
    }

    @Override
    public IconType getIconType() {
        return IconType.getIcon(this);
    }

    @Override
    public String getApple() {
        return apple;
    }

    @Override
    public String getGoogle() {
        return google;
    }

    public String getImageUrl() {
        return image;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

}
