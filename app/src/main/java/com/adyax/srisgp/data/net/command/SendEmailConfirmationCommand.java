package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class SendEmailConfirmationCommand extends ServiceCommand {

    public SendEmailConfirmationCommand() {
    }

    protected SendEmailConfirmationCommand(Parcel in) {
    }

    public static final Creator<SendEmailConfirmationCommand> CREATOR = new Creator<SendEmailConfirmationCommand>() {
        @Override
        public SendEmailConfirmationCommand createFromParcel(Parcel in) {
            return new SendEmailConfirmationCommand(in);
        }

        @Override
        public SendEmailConfirmationCommand[] newArray(int size) {
            return new SendEmailConfirmationCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.sendEmailConfirmation(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
