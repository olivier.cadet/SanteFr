package com.adyax.srisgp.data.net.response;

import android.content.Context;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class ErrorResponse extends BaseResponse implements INewFields{

    @SerializedName(MAIL)
    public ErrorItem mail;

    @SerializedName("error")
    public ErrorItem error;

    @SerializedName(URL_OTHER)
    public ErrorItem url;

    @SerializedName("sso_client_id")
    public ErrorItem sso_client_id;

    private String errorMessage;

    private ErrorResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ErrorResponse(int error_code, String error_message) {
        error=new ErrorItem(error_code, error_message);
    }

    public boolean isEmailError() {
        return mail!=null;
    }

    public boolean isSSOError() {
        return sso_client_id!=null;
    }

    public ErrorItem getEmailItem() {
        return mail;
    }

    public static ErrorResponse create(AppReceiver.ErrorMessage errorMessage) {
        String textErrorMessage = errorMessage.getErrorMessage();
        try {
            return new Gson().fromJson(textErrorMessage, ErrorResponse.class);
        }catch (Exception e){

        }
        return new ErrorResponse(textErrorMessage);
    }

    @Override
    public String getErrorMessage(Context context) {
        if(isError()){
            return error.getLocalizeErrorMessage(context);
        }
        if(isSSOError()){
            return sso_client_id.getLocalizeErrorMessage(context);
        }
        if(isEmailError()){
            return mail.getLocalizeErrorMessage(context);
        }
        if(url!=null){
            return url.getLocalizeErrorMessage(context);
        }
        return errorMessage;
    }

    public boolean isNotLogin() {
        if(error!=null){
            return error.isNotLogin();
        }
        if(mail!=null){
            return mail.isNotLogin();
        }
        return false;
    }

    public boolean isError() {
        return error!=null;
    }

    public ErrorItem getErrorAnyItem() {
        if(isError()){
            return error;
        }
        if(isSSOError()){
            return sso_client_id;
        }
        if(isEmailError()){
            return mail;
        }
        if(url!=null){
            return url;
        }
        return error;
    }

    public boolean isAnyError() {
        return errorMessage!=null|| error!=null|| mail!=null|| url!=null|| isSSOError();
    }

    public ErrorItem getErrorItem() {
        return error;
    }

    public boolean isMaintenanceMode() {
        return error!=null&& error.getErrorCode()==ErrorItem.MAINTENANCE_MODE;
    }
}
