package com.adyax.srisgp.data.tracker;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.atinternet.tracker.Gesture;
import com.atinternet.tracker.Screen;
import com.atinternet.tracker.Tracker;

/**
 * Created by SUVOROV on 4/4/17.
 */

public class InternalSearchTrackerHelper {

    public static final String RECHERCHES_S_INFORMER = "S'informer";
    public static final String RECHERCHES_TROUVER = "Trouver";
    public static final String AUTOUR_DE_MOI = "Autour de moi";
//    public static final String MASK_KEYWORD = "(%s1%s)";
    public static final String MASK_KEYWORD = "%s+%s";

    private Tracker tracker;
    private int count;
    private SearchCommand searchCommand;
    private SearchResponseHuge response;

    public InternalSearchTrackerHelper(Tracker tracker) {
        this.tracker = tracker;
        count=0;
    }

    public void addSearchInfo(SearchCommand searchCommand, SearchResponseHuge response) {
        this.searchCommand = searchCommand;
        this.response = response;
        final int page = searchCommand.getPage();
        if(page==0){
            if(searchCommand.isBbox()){
                addScreen("click 'relancer la recherche'");
            }else {
                addScreen("first search");
            }
            count=0;
        }else {
            addScreen("Next Search (AFFICHER PLUS DE RÉSULTATS)");
        }
        count+=response.getCount();
    }

    private void addScreen(String debugInfo) {
        final int page = searchCommand.getPage()+1;
        Screen screen = tracker.Screens().add(getPageName());
        screen.InternalSearch(getKeyWord(), page);
        if(!BuildConfig.DEBUG) {
            screen.sendView();
        }
    }

    public void addGesture(TrackerAT.TrackerPageArg trackerPageArg) {
        // Click on Item
//        switch (trackerPageArg.getXiti().getAppTrackerLevel()){
//            case RESEARCH_RESULTS_FIND:
        if(searchCommand!=null) {
            Gesture gesture = tracker.Gestures().add(trackerPageArg.getXiti().getName());
            gesture.InternalSearch(getKeyWord(), searchCommand.getPage() + 1, trackerPageArg.getPosition()+1);
            gesture.sendSearch();
        }
//                break;
//        }
    }

    private String getKeyWord() {
        try {
            if (searchCommand.isBbox()) {
                return String.format(MASK_KEYWORD, searchCommand.getSearchText(), response.getWhere());
            } else {
                return String.format(MASK_KEYWORD, searchCommand.getSearchText(), searchCommand.getWhere());
            }
        }catch (Exception e){
            if(BuildConfig.DEBUG){
                throw e;
            }
        }
        return "";
    }

    private String getPageName() {
        try {
            switch (searchCommand.getType()) {
                case info:
                    return RECHERCHES_S_INFORMER;
                case find:
                    return RECHERCHES_TROUVER;
                case around:
                    return AUTOUR_DE_MOI;
                default:
                case unknown:
                    return "";
            }
        }catch (Exception e){
            if(BuildConfig.DEBUG){
                throw e;
            }
        }
        return  "";
    }

}
