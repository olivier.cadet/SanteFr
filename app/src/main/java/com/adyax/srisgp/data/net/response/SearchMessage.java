package com.adyax.srisgp.data.net.response;

import android.text.TextUtils;

import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by anton.kobylianskiy on 3/7/17.
 */
@DatabaseTable(tableName = "search_message")
public class SearchMessage {
    public static final String KEY = "key";
    public static final String DESCRIPTION = "description";
    public static final String READ = "read";
    public static final String SEARCH_TYPE = "type";
    public static final String REGION_KEY = "region";

    @DatabaseField(columnName = KEY)
    public String key;

    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @DatabaseField(columnName = READ)
    public boolean read = false;

    @DatabaseField(columnName = SEARCH_TYPE)
    public SearchType type;

    public SearchMessage() {
    }

    public SearchMessage(String key, String description, SearchType searchType) {
        this.key = key;
        this.description = description;
        this.type = searchType;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public SearchType getType() {
        return type;
    }

    public boolean isAroundMeRegionMessage() {
        return TextUtils.equals(REGION_KEY, key)&& type==SearchType.around;
    }

}
