package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Kirill on 22.11.16.
 */

public class UpdatePersonalizeCommand extends ServiceCommand {

    @SerializedName("field_manual_interests")
    private ArrayList<String> interests;

    @SerializedName("field_search_interests")
    private ArrayList<String> searchInterests;

    @SerializedName("notify")
    private NotifySettingsBody notifySettings;

    public UpdatePersonalizeCommand(ArrayList<String> interests, ArrayList<String> searchInterests, NotifySettingsBody notifySettings) {
        this.interests = interests;
        this.searchInterests = searchInterests;
        this.notifySettings = notifySettings;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.updatePersonalize(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.interests);
        dest.writeStringList(this.searchInterests);
        dest.writeParcelable(this.notifySettings, flags);
    }

    protected UpdatePersonalizeCommand(Parcel in) {
        this.interests = in.createStringArrayList();
        this.searchInterests = in.createStringArrayList();
        this.notifySettings = in.readParcelable(NotifySettingsBody.class.getClassLoader());
    }

    public static final Creator<UpdatePersonalizeCommand> CREATOR = new Creator<UpdatePersonalizeCommand>() {
        @Override
        public UpdatePersonalizeCommand createFromParcel(Parcel source) {
            return new UpdatePersonalizeCommand(source);
        }

        @Override
        public UpdatePersonalizeCommand[] newArray(int size) {
            return new UpdatePersonalizeCommand[size];
        }
    };
}
