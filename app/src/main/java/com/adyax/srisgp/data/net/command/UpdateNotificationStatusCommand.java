package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.response.tags.NotificationItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 9/28/16.
 */

public class UpdateNotificationStatusCommand extends ServiceCommand {
    public static final String ACTION_DELETE = "delete";
    public static final String ACTION_MARK_AS_READ = "mark_as_read";

    private List<Long> nids;
    private String action;

    public UpdateNotificationStatusCommand(String action, List<Long> nids) {
        this.action = action;
        this.nids = nids;
    }

    public UpdateNotificationStatusCommand(String action, long id) {
        this.action = action;
        nids = new ArrayList<Long>() {{
           add(id);
        }};
    }

    public List<Long> getNids() {
        return nids;
    }

    public String getAction() {
        return action;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.changeNotificationStatus(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.nids);
        dest.writeString(this.action);
    }

    protected UpdateNotificationStatusCommand(Parcel in) {
        this.nids = new ArrayList<Long>();
        in.readList(this.nids, Long.class.getClassLoader());
        this.action = in.readString();
    }

    public static final Creator<UpdateNotificationStatusCommand> CREATOR = new Creator<UpdateNotificationStatusCommand>() {
        @Override
        public UpdateNotificationStatusCommand createFromParcel(Parcel source) {
            return new UpdateNotificationStatusCommand(source);
        }

        @Override
        public UpdateNotificationStatusCommand[] newArray(int size) {
            return new UpdateNotificationStatusCommand[size];
        }
    };
}
