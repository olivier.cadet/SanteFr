package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.BuildConfig;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by anton.kobylianskiy on 11/18/16.
 */

public class FeedbackFormField implements Parcelable {
    @SerializedName("name")
    public String name;
    @SerializedName("label")
    public String label;
    @SerializedName("type")
    public String type;
    @SerializedName("required")
    public int required;
    @SerializedName("singular")
    public int singular;
    @SerializedName("special_field")
    public String specialField;
    @SerializedName("options")
    public List<FeedbackOption> options;

    public FieldType getType() {
        return FieldType.parse(type);
    }

    public String[] getItems() {
        if(BuildConfig.DEBUG){
            String[] s = new String[options.size()+1];
            int i = 0;
            for (FeedbackOption f : options) {
                s[i++] = f.getLabel();
            }
            s[i]="test9 test8 test7 test6 test5 test4 a b c d e f g h t i o p test2 test1 test0";
            return s;
        }else {
            String[] s = new String[options.size()];
            int i = 0;
            for (FeedbackOption f : options) {
                s[i++] = f.getLabel();
            }
            return s;
        }
    }

    public String getValue(String value) {
        for(FeedbackOption f:options){
            if(TextUtils.equals(f.getLabel(), value)){
                return f.getValue();
            }
        }
        return "";
    }

    public String getLabel() {
        return label;
    }

    public enum FieldType {
        SELECT("select"),
        RADIOS("radios"),
        CHECKBOXES("checkboxes"),
        CHECKBOX("checkbox"),
        TEXT("text"),
        TEXT_LONG("text_long"),
        DEFAULT("default");

        private String type;

        FieldType(String type) {
            this.type = type;
        }

        public static FieldType parse(String type) {
            if (type == null || type.isEmpty()) {
                return FieldType.DEFAULT;
            }

            for (FieldType fieldType : FieldType.values()) {
                if (fieldType.type.equals(type)) {
                    return fieldType;
                }
            }

            return DEFAULT;
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.label);
        dest.writeString(this.type);
        dest.writeInt(this.required);
        dest.writeInt(this.singular);
        dest.writeString(this.specialField);
    }

    public FeedbackFormField() {
    }

    protected FeedbackFormField(Parcel in) {
        this.name = in.readString();
        this.label = in.readString();
        this.type = in.readString();
        this.required = in.readInt();
        this.singular = in.readInt();
        this.specialField = in.readString();
    }

    public static final Parcelable.Creator<FeedbackFormField> CREATOR = new Parcelable.Creator<FeedbackFormField>() {
        @Override
        public FeedbackFormField createFromParcel(Parcel source) {
            return new FeedbackFormField(source);
        }

        @Override
        public FeedbackFormField[] newArray(int size) {
            return new FeedbackFormField[size];
        }
    };

    public String getKey() {
        return name;
    }
}
