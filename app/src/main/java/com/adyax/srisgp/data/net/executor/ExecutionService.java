package com.adyax.srisgp.data.net.executor;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import javax.inject.Inject;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.repository.IRepository;

public class ExecutionService extends IntentService {

    public static final int LOGIN_ACTION = 1;
    public static final int LOGOUT_ACTION = 2;
    public static final int RESET_ACTION = 3;
    public static final int CITIES_ACTION = 4;
    public static final int GET_HISTORY_COMMAND = 5;
    public static final int GET_POPULAR_ACTION = 6;
    public static final int GET_NOTIFICATION = 7;
    public static final int FAVORITES_ACTION = 8;
    public static final int COMMAND_GET_CARDS = 9;
    public static final int SEARCH_ACTION = 10;
    public static final int ALERTS_ACTION = 11;
    public static final int PING_AUTH_ACTION = 12;
    public static final int CREATE_ACCOUNT_ACTION = 13;
    public static final int TAXONOMY_VOCABULARY_TERMS_ACTION = 14;
    public static final int CHANGE_CREDENTIAL_ACTION = 15;
    public static final int ANCHORS_ACTION = 16;
    public static final int PING_AUTH_NON_ACTION = 17;
    public static final int FAVORITE_ACTION = 18;
    public static final int GET_FILTERS_REQUEST_CODE = 19;
    public static final int SEND_EMAIL_CONFIRMATION_ACTION = 20;
    public static final int GET_FORM = 21;
    public static final int SUBMIT_FORM = 22;
    public static final int ADD_CONTACT_FEEDBACK_FORM = 23;
    public static final int DELETE_ACCOUNT = 24;
    public static final int GET_SSO_INFO_REQUEST = 25;
    public static final int CREATE_GOOGLE_PLUS_ACCOUNT_ACTION = 26;
    public static final int COMMAND_GET_CITIES = 27;
    public static final int REPORT_ACTION = 28;
    public static final int SET_PUSH_CREDENTIAL = 29;
    public static final int CHANGE_EMAIL_REQUEST_CODE = 30;
    public static final int CHANGE_PASS_REQUEST_CODE = 31;
    public static final int ADD_IN_HISTORY = 32;
    public static final int GET_CONFIGURATION = 33;
    public static final int SGP_LOCATION_COMPLETE = 34;
    public static final int GET_FORM_REPORT_WITHOUT_EMAIL = 35;
    public static final int GET_FORM_REPORT_WITH_EMAIL = 36;
    public static final int ADD_TO_HISTORY = 37;
    public final static int GET_INTERESTS_REQUEST = 38;
    public final static int UPDATE_PERSONALIZE_REQUEST = 39;
    public final static int SEND_EMERGENCY = 40;
    public final static int COMMAND_FAVORITES = 41;
    public static final int DEL_PUSH_CREDENTIAL = 42;

    private static final String TAG = "ExecutionService";
    public static final String ACTION_NO_CONNECTION = "ACTION_NO_CONNECTION";

    public static final String KEY_COMMAND = "COMMAND";
    public static final String KEY_RECEIVER = "RECEIVER";
    public static final String KEY_REQUEST_CODE = "REQUEST_CODE";

    @Inject
    CommandExecutor commandExecutor;
    
    @Inject
    IRepository repository;

    public ExecutionService() {
        super(TAG);
    }

    public static void sendCommand(Context context, AppReceiver receiver, ServiceCommand command,
                                   int code, boolean retryDialog) {
        if (isConnected(context)) {
            Intent intent = new Intent(context, ExecutionService.class);
            intent.putExtra(KEY_RECEIVER, receiver);
            intent.putExtra(KEY_COMMAND, command);
            intent.putExtra(KEY_REQUEST_CODE, code);
            context.startService(intent);
        } else {
            // TODO modife Executor
            if(receiver!=null){
                receiver.sendFail(code, new Bundle());
            }
            if (retryDialog) {
//            Intent intent = new Intent(Utils.ACTION_NO_CONNECTION);
                Intent intent = new Intent(ACTION_NO_CONNECTION);
                intent.putExtra(KEY_RECEIVER, receiver);
                intent.putExtra(KEY_COMMAND, command);
                intent.putExtra(KEY_REQUEST_CODE, code);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        }
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    public static void sendCommand(Context context, AppReceiver receiver, ServiceCommand command, int code) {
        sendCommand(context, receiver, command, code, false);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        App.getApplicationComponent().inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ServiceCommand command = intent.getParcelableExtra(KEY_COMMAND);
        AppReceiver resultReceiver = intent.getParcelableExtra(KEY_RECEIVER);
        int requestCode = intent.getIntExtra(KEY_REQUEST_CODE, 0);
        if (command != null) {
            if(command instanceof LoginBaseServiceCommand) {
                ((LoginBaseServiceCommand)command).updateLocation(repository.getGeoLocation());
            }
            command.execute(commandExecutor, resultReceiver, requestCode);
        }
    }
}
