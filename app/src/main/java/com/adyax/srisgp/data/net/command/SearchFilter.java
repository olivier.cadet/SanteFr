package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by SUVOROV on 9/20/16.
 */

public class SearchFilter implements Parcelable, Serializable, Comparable {

    @SerializedName("param")
    private String param;

    @SerializedName("value")
    private ArrayList<String> value = new ArrayList<>();

//    public boolean equals(SearchFilter s) {
//        if(s!=null){
//            if(Objects.equals(s.param, param)&&
//                    Objects.equals(s.value, value)){
//                return true;
//            }
//        }
//        return false;
//    }

    public SearchFilter(String param, String[] value) {
        this.param = param;
        this.value = new ArrayList<>(Arrays.asList(value));
    }

    public SearchFilter(String param, ArrayList<String> value) {
        this.param = param;
        this.value = value;
    }

    public String getParam() {
        return param;
    }

    public ArrayList<String> getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.param);
        dest.writeStringList(this.value);
    }

    protected SearchFilter(Parcel in) {
        this.param = in.readString();
        this.value = in.createStringArrayList();
    }

    public static final Creator<SearchFilter> CREATOR = new Creator<SearchFilter>() {
        @Override
        public SearchFilter createFromParcel(Parcel source) {
            return new SearchFilter(source);
        }

        @Override
        public SearchFilter[] newArray(int size) {
            return new SearchFilter[size];
        }
    };


    @Override
    public int compareTo(@NonNull Object obj) {
        SearchFilter searchFilterHuge=(SearchFilter)obj;
        if(!TextUtils.equals(param, searchFilterHuge.param)){
            return param.compareTo(searchFilterHuge.param);
        }
        int size1 = value.size();
        int size2 = searchFilterHuge.value.size();
        if(size1 != size2){
            return size1>size2? 1: -1;
        }
        if(size1==0&& size2==0){
            return 0;
        }
        for(int i=0;i<size1;i++){
            if(!TextUtils.equals(value.get(i), searchFilterHuge.value.get(i))){
                return value.get(i).compareTo(searchFilterHuge.value.get(i));
            }
        }
        return 0;
    }
}
