package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.tags.AnchorItem;
import com.adyax.srisgp.data.net.response.tags.EmergencyItem;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.NodeType;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class AnchorResponse extends BaseResponse implements Parcelable, INewFields, IGetUrl {

    public static final String ANCHOR_RESPONSE = "AnchorResponse";
    public static final String RELATED = "En relation";

    public static final String NODE_TYPE = "node_type";
    public static final String DESCRIPTION = "description";
    public static final String ICON = "icon";

    @SerializedName("anchors")
    public AnchorItem[] anchors;

    @SerializedName("favorite")
    public long favorite;

    @SerializedName(NID)
    public long nid;

    @SerializedName("title")
    public String title;

    @SerializedName(Xiti.XITI)
    public Xiti xiti;

    @SerializedName(NODE_TYPE)
    public NodeType node_type;

    @SerializedName(PHONE_ARRAY)
    public String[] phones;

    @SerializedName(DESCRIPTION)
    public String description;

    @SerializedName(SOURCE)
    public String source;

    @SerializedName("node_url")
    public String url;

    @SerializedName(ICON)
    public IconType icon = IconType.UNKNOWN;

    @SerializedName(SearchItemHuge.CONTENT_MAINTENANCE)
    public int content_maintenance;

    @SerializedName("emergency")
    public EmergencyItem emergency;

    protected AnchorResponse(Parcel in) {
        anchors = in.createTypedArray(AnchorItem.CREATOR);
        favorite = in.readLong();
        nid = in.readLong();
        title = in.readString();
        this.xiti = in.readParcelable(Xiti.class.getClassLoader());
        final int tmpNode_type = in.readInt();
        this.node_type = tmpNode_type == -1 ? null : NodeType.values()[tmpNode_type];
        phones = in.createStringArray();
        description = in.readString();
        source = in.readString();
        url = in.readString();
        final int tmp_Icon = in.readInt();
        this.icon = tmp_Icon == -1 ? null : IconType.values()[tmp_Icon];
        emergency = in.readParcelable(EmergencyItem.class.getClassLoader());
    }

    public static final Creator<AnchorResponse> CREATOR = new Creator<AnchorResponse>() {
        @Override
        public AnchorResponse createFromParcel(Parcel in) {
            return new AnchorResponse(in);
        }

        @Override
        public AnchorResponse[] newArray(int size) {
            return new AnchorResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(anchors, flags);
        dest.writeLong(favorite);
        dest.writeLong(nid);
        dest.writeString(title);
        if (this.xiti == null) {
            this.xiti = new Xiti();
        }
        dest.writeParcelable(this.xiti, flags);
        dest.writeInt(this.node_type == null ? -1 : this.node_type.ordinal());
        dest.writeStringArray(this.phones);
        dest.writeString(this.description);
        dest.writeString(this.source);
        dest.writeString(this.url);
        dest.writeInt(this.icon == null ? -1 : this.icon.ordinal());
        dest.writeParcelable(emergency, flags);
    }

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    public String getCardLink() {
        return url;
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return nid;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public NodeType getNodeType() {
        return node_type;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return source;
    }

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    @Override
    public IconType getIconType() {
        return icon;
    }

    public boolean isFavorite() {
        return favorite != 0;
    }

    public void setFavorite(boolean bFavorite) {
        this.favorite = bFavorite ? 1 : 0;
    }

    public long getId() {
        return nid;
    }

    public long getFavorite() {
        return favorite;
    }

    public boolean containRelated() {
        return anchors != null && anchors.length >= 1 && RELATED.equals(anchors[anchors.length - 1].title);
    }

    public AnchorItem getRelatedItem() {
        return anchors[anchors.length - 1];
    }

    public void setXiti(Xiti xiti) {
        this.xiti = xiti;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean fixUrl() {
        if (url != null && !url.startsWith(UrlExtras.getBaseURL())) {
            if (url.startsWith("/")) {
                url = UrlExtras.getBaseURL() + url;
            } else {
                url = UrlExtras.getBaseURL() + "/" + url;
            }
            return true;
        }
        return false;
    }

    public String getContentUrl() {
        return url;
    }

    public String getPhone() {
        return phones != null ? phones[0] : null;
    }

    public String getXitiStr() {
        if (xiti != null) {
            return new Gson().toJson(xiti);
        }
        return null;
    }

    public boolean isContentMaintenanceMode() {
        return content_maintenance == SearchItemHuge.MAINTENANCE_MODE;
    }

    @Override
    public String getApple() {
        return null;
    }

    @Override
    public String getGoogle() {
        return null;
    }

    public Long getValueIndex(String value) {
        return emergency.getValueIndex(value);
    }

    public Long getServiceIndex(String value) {
        return emergency.getServiceIndex(value);
    }

    public int getServiceSpinnerIndex(Long sid) {
        return emergency.getServiceSpinnerIndex(sid);
    }

    public boolean isEmergencyButton(String geoLocation) {
        return emergency != null && emergency.isEmergencyButton(geoLocation);
    }

    public boolean isEmergecyVisible() {
        return emergency != null && emergency.isEmergecyVisible();
    }

    public int getValueSpinnerIndex(int vote) {
        return emergency.getValueSpinnerIndex(vote);
    }
}
