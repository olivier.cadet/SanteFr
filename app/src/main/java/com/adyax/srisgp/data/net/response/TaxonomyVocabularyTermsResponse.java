package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.TermsItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by SUVOROV on 9/19/16.
 */

//public class TaxonomyVocabularyTermsResponse extends BaseResponse {
public class TaxonomyVocabularyTermsResponse extends BaseResponse implements Parcelable {

    private Map<Long, TermsItem> data_map;

    public TaxonomyVocabularyTermsResponse(String response) {
        super();
        data_map = new Gson().fromJson(response, new TypeToken<TreeMap<Long, TermsItem>>() {
        }.getType());
    }

    public boolean isSuccess() {
        return true;
    }

    public Map<Long, TermsItem> getData_map() {
        return data_map;
    }

    public TermsItem[] getArray() {
        TermsItem[] termsItem = new TermsItem[data_map.size()];
        int i = 0;
        for (Long key : data_map.keySet()) {
            termsItem[i++] = data_map.get(key);
        }
        return termsItem;
    }

    public ArrayList<TermsItem> getList() {
        ArrayList<TermsItem> items = new ArrayList<>();
        for (Long key : data_map.keySet()) {
            items.add(data_map.get(key));
        }
        return items;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.data_map.size());
        for (Map.Entry<Long, TermsItem> entry : this.data_map.entrySet()) {
            dest.writeValue(entry.getKey());
            dest.writeParcelable(entry.getValue(), flags);
        }
    }

    protected TaxonomyVocabularyTermsResponse(Parcel in) {
        int data_mapSize = in.readInt();
        this.data_map = new HashMap<Long, TermsItem>(data_mapSize);
        for (int i = 0; i < data_mapSize; i++) {
            Long key = (Long) in.readValue(Long.class.getClassLoader());
            TermsItem value = in.readParcelable(TermsItem.class.getClassLoader());
            this.data_map.put(key, value);
        }
    }

    public static final Creator<TaxonomyVocabularyTermsResponse> CREATOR = new Creator<TaxonomyVocabularyTermsResponse>() {
        @Override
        public TaxonomyVocabularyTermsResponse createFromParcel(Parcel source) {
            return new TaxonomyVocabularyTermsResponse(source);
        }

        @Override
        public TaxonomyVocabularyTermsResponse[] newArray(int size) {
            return new TaxonomyVocabularyTermsResponse[size];
        }
    };
}
