package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.annotations.SerializedName;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class RegisterUserCommand implements Parcelable {
public class CreateAccountCommand extends LoginBaseServiceCommand implements INewFields {

    public static final String GENDER_TYPE = "register_mode";
    public static final int FAKE_REGISTER = 0;
    public static final int START_REGISTER = 1;//1

    public Sso_type sso_type;
    public String sso_client_id;
    public String token;

    @SerializedName(MAIL)
    public String mail;

    @SerializedName(PASS)
    public String pass;

    public GenderType field_gender;
    public long field_ville;
    public long[] field_manual_interests;
    public long field_age;
    public int register;
//    private String deviceToken;
    public String twitterTokenSecret;
    public String twitterScreenName;
//    private String password;

    public CreateAccountCommand() {
        super(FirebaseInstanceId.getInstance().getToken());
//        deviceToken = FirebaseInstanceId.getInstance().getToken();
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.registerUser(this, receiver, requestCode);
    }

    protected CreateAccountCommand(Parcel in) {
        super(in);
        sso_type = Sso_type.valueOf(in.readString());
        if (sso_type == Sso_type.sgp_sso_mail) {
            sso_type = null;
        }
        sso_client_id = in.readString();
        mail = in.readString();
        pass = in.readString();
        field_gender = GenderType.valueOf(in.readString());
        if (field_gender == GenderType.empty) {
            field_gender = null;
        }
        field_ville = in.readLong();
        field_manual_interests = in.createLongArray();
        field_age = in.readLong();
        register = in.readInt();
        token = in.readString();
        twitterTokenSecret = in.readString();
        twitterScreenName = in.readString();
    }

    public static final Creator<CreateAccountCommand> CREATOR = new Creator<CreateAccountCommand>() {
        @Override
        public CreateAccountCommand createFromParcel(Parcel in) {
            return new CreateAccountCommand(in);
        }

        @Override
        public CreateAccountCommand[] newArray(int size) {
            return new CreateAccountCommand[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (sso_type == null) {
            parcel.writeString(Sso_type.sgp_sso_mail.name());
        } else {
            parcel.writeString(sso_type.name());
        }
        parcel.writeString(sso_client_id);
        parcel.writeString(mail);
        parcel.writeString(pass);
        if (field_gender == null) {
            parcel.writeString(GenderType.empty.name());
        } else
            parcel.writeString(field_gender.name());
        parcel.writeLong(field_ville);
        parcel.writeLongArray(field_manual_interests);
        parcel.writeLong(field_age);
        parcel.writeInt(register);
        parcel.writeString(token);
        parcel.writeString(twitterTokenSecret);
        parcel.writeString(twitterScreenName);
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setGender(GenderType sex) {
        field_gender = sex;
    }

    public void setAge(Integer year) {
        Timestamp date = new Timestamp((year - 1900), 1, 1, 0, 0, 0, 0);
        field_age = date.getTime() / 1000;
    }

    public void setIds(Set<Long> ids) {
//        field_manual_interests = ids.toArray(new long[ids.size()]);
        field_manual_interests = new long[ids.size()];
        int i = 0;
        for (Long l : ids) {
            field_manual_interests[i++] = l;
        }
    }

    public void setPostal(Long postal) {
        field_ville = postal.longValue();
    }

    public void setRegister(int register) {
        this.register = register;
    }

    public void setSsoRegister(Sso_type ssoType, String clientId, String email, String token) {
        this.mail = email;
        this.sso_type = ssoType;
        this.sso_client_id = clientId;
        this.token = token;
    }

    public void setSsoRegisterTwitter(Sso_type ssoType, String clientId, String email,
                                      String token, String twitterTokenSecret, String twitterScreenName) {
        setSsoRegister(ssoType, clientId, email, token);
        this.twitterTokenSecret = twitterTokenSecret;
        this.twitterScreenName = twitterScreenName;
    }

    public String getMail() {
        return mail;
    }

    public String getPass() {
        return pass;
    }

    public boolean isRegister() {
        return register == START_REGISTER;
    }

    public boolean isSsoRegister() {
        return sso_type != null && sso_client_id != null;
    }

    public boolean isGooglePlus() {
        return sso_type!=null&& sso_type==Sso_type.sgp_sso_google;
    }

    public String getPassword() {
        return pass;
    }

    public GenderType getGender() {
        return field_gender;
    }
}
