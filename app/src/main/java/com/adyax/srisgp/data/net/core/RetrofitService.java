package com.adyax.srisgp.data.net.core;

import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.AddContactFormFeedbackCommand;
import com.adyax.srisgp.data.net.command.AddInHistoryCommand;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.AddToHistoryCommand;
import com.adyax.srisgp.data.net.command.AlertsCommand;
import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.command.CheckPasswordCommand;
import com.adyax.srisgp.data.net.command.CitiesCommand;
import com.adyax.srisgp.data.net.command.ClearHistoryCommand;
import com.adyax.srisgp.data.net.command.DelPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.command.GetAnchorsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchLocationsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchPhrasesCommand;
import com.adyax.srisgp.data.net.command.GetFavoritesCommand;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.GetHistoryCommand;
import com.adyax.srisgp.data.net.command.GetConfigurationCommand;
import com.adyax.srisgp.data.net.command.GetNotificationCommand;
import com.adyax.srisgp.data.net.command.GetPopularCommand;
import com.adyax.srisgp.data.net.command.GetQuickCardsCommand;
import com.adyax.srisgp.data.net.command.GetSsoInfoCommand;
import com.adyax.srisgp.data.net.command.LoginUserCommand;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.LoginUserSsoCommand;
import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.command.SetPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.ResetCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.TaxonomyVocabularyTermsCommand;
import com.adyax.srisgp.data.net.command.UpdateNotificationStatusCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalInformationCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalizeCommand;
import com.adyax.srisgp.data.net.response.AddToHistoryResponse;
import com.adyax.srisgp.data.net.response.AlertResponse;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.BaseResponse;
import com.adyax.srisgp.data.net.response.CheckCguResponse;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.FavoritesResponse;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchPhrasesResponse;
import com.adyax.srisgp.data.net.response.GetHistoryResponse;
import com.adyax.srisgp.data.net.response.GetQuickCardsAnonymousResponse;
import com.adyax.srisgp.data.net.response.GetSsoInfoResponse;
import com.adyax.srisgp.data.net.response.PingNonAuthResponse;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.data.net.response.NotificationResponse;
import com.adyax.srisgp.data.net.response.PopularResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.RecentLocationsResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.NonStandardResponse;
import com.adyax.srisgp.data.net.response.SearchResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.UrgenceResponse;
import com.adyax.srisgp.data.net.response.UserResponse;
import com.adyax.srisgp.di.modules.NetworkModule;

import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public interface RetrofitService {

    @POST(UrlExtras.User.REGISTRATION)
    Observable<RegisterUserResponse> register(@Body CreateAccountCommand createAccountCommand);

    @POST(UrlExtras.User.LOGIN)
    Observable<RegisterUserResponse> login(@Body LoginUserCommand loginUserCommand);

    @POST(UrlExtras.User.LOGIN_SSO)
    Observable<RegisterUserResponse> loginSso(@Body LoginUserSsoCommand command);

    @GET(UrlExtras.User.CHECK_CGU_VALIDATION)
    Observable<CheckCguResponse> checkCGUValidation(@Path("user_uid") String user_uid);

    @GET(UrlExtras.User.SEND_CGU_VALIDATION)
    Observable<CheckCguResponse> sendCGUValidation(@Path("user_uid") String user_uid);

    @POST(UrlExtras.RestUser.PING)
    Observable<UserResponse> ping_auth();

    @POST(UrlExtras.RestUser.LOGOUT)
    Observable<NonStandardResponse> logout();

    @POST(UrlExtras.User.RESET)
    Observable<NonStandardResponse> reset(@Body ResetCommand resetCommand);

    @POST(UrlExtras.User.CITIES)
    Observable<CitiesResponse> cities(@Body CitiesCommand citiesCommand);

    @POST(UrlExtras.RestUser.GET_HISTORY)
    Observable<GetHistoryResponse> getHistory(@Body GetHistoryCommand getHistoryCommand);

    @POST(UrlExtras.RestUser.CLEAR_HISTORY)
    Observable<NonStandardResponse> clearHistory(@Body ClearHistoryCommand clearHistoryCommand);

    @POST(UrlExtras.Search.GET_POPULAR)
    Observable<PopularResponse> getPopular(@Body GetPopularCommand getPopularCommand);

    @POST(UrlExtras.RestUser.GET_NOTIFICATION)
    Observable<NotificationResponse> getNotification(@Body GetNotificationCommand getNotificationCommand);

    @POST(UrlExtras.RestUser.GET_FAVORITES)
    Observable<FavoritesResponse> getFavorites(@Body GetFavoritesCommand getFavoritesCommand);

    @POST(UrlExtras.Search.GET_AUTOCOMPLETE_SEARCH_PHRASES)
    Observable<GetAutoCompleteSearchPhrasesResponse> getAutoCompleteSearchPhrases(@Body GetAutoCompleteSearchPhrasesCommand getAutoCompleteSearchPhrasesCommand);

    @POST(UrlExtras.Search.GET_AUTOCOMPLETE_SEARCH_LOCATIONS)
    Observable<GetAutoCompleteSearchLocationsResponse> getAutoCompleteSearchLocations(@Body GetAutoCompleteSearchLocationsCommand getAutoCompleteSearchLocationsCommand);

    @POST(UrlExtras.RestUserSearch.GET_RECENT_SEARCH_PHRASES)
    Observable<SearchResponse> getRecentSearchPhraseCommand();

    @POST(UrlExtras.RestUserSearch.GET_RECENT_SEARCH_LOCATIONS)
    Observable<RecentLocationsResponse> getRecentSearchLocationCommand();

//    @POST(UrlExtras.RestHomePage.GET_QUICK_CARDS_ANONYMOUS)
//    Observable<GetQuickCardsAnonymousResponse> getQuickCardsAnonymous();

    @POST(UrlExtras.RestUserHomePage.GET_QUICK_CARDS_AUTHENTICATED)
    Observable<GetQuickCardsAnonymousResponse> getQuickCardsAuthenticated(@Body GetQuickCardsCommand getQuickCardsCommand);

    @POST(UrlExtras.RestUserSearch.SEARCH)
    Observable<SearchResponseHuge> search(@Body SearchCommand searchCommand);

    // TODO
    // X-Alerts-Since: timestamp
    // X-Coordinates: lat,lon,accuracy
    // X-Coordinates-Changed: 1
    @POST(UrlExtras.User.ALERTS)
    Observable<AlertResponse> alerts(@Header(NetworkModule.X_ALERTS_SINCE) String change , @Body AlertsCommand alertsCommand);

    @POST(UrlExtras.RestUserSearch.CLEAR_RECENT_SEARCH_PHRASES)
    Observable<NonStandardResponse> clearRecentSearchPhrases();

    @POST(UrlExtras.RestUserSearch.CLEAR_RECENT_SEARCH_LOCATIONS)
    Observable<NonStandardResponse> clearRecentLocations();

    @POST(UrlExtras.RestUserSearch.REMOVE_RECENT_SEARCH_ITEM)
    Observable<NonStandardResponse> removeRecentSearchItem(@Body RemoveSearchItemCommand removeSearchItemCommand);

    @POST(UrlExtras.RestUser.UPDATE_NOTIFICATIONS)
    Observable<NonStandardResponse> changeNotificationStatus(@Body UpdateNotificationStatusCommand command);

    @POST(UrlExtras.RestUser.ADD_REMOVE_FAVORITE)
    Observable<NonStandardResponse> addOrRemoveFavorite(@Body AddOrRemoveFavoriteCommand command);

    @POST(UrlExtras.RestUserTaxonomyVocabulary.TERMS)
    Observable<TaxonomyVocabularyTermsResponse> getTaxonomyVocabularyTerms(@Body TaxonomyVocabularyTermsCommand command);

    @POST(UrlExtras.RestUser.UPDATE_CREDENTIALS)
    Observable<NonStandardResponse> updateCredentials(@Body ChangeCredentialsCommand changeCredentialsCommand);

    @POST(UrlExtras.RestUser.UPDATE_PERSONAL_INFORMATION)
    Observable<RegisterUserResponse> updatePersonalInformation(@Body UpdatePersonalInformationCommand command);

    @POST(UrlExtras.RestUserNode.ANCHORS)
    Observable<AnchorResponse> getAnchors(@Body GetAnchorsCommand anchorsCommand);

    @POST(UrlExtras.User.PING)
    Observable<PingNonAuthResponse> ping_not_auth();

    @POST(UrlExtras.RestUser.SEND_EMAIL_CONFIRATION)
    Observable<RegisterUserResponse> sendEmailConfirmation();

    @POST(UrlExtras.RestFeedback.GET_FORM)
    Observable<FeedbackFormResponse> getForm(@Body GetFormCommand command);

    @POST(UrlExtras.RestFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> submitFormAnonymous(@Body Map<String, Object> body);

    @POST(UrlExtras.RestUserFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> submitForm(@Body Map<String, Object> body);

    @POST(UrlExtras.RestUserFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> addContactFormFeedback(@Body AddContactFormFeedbackCommand command);

    @POST(UrlExtras.RestFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> addContactFormFeedbackAnonymous(@Body AddContactFormFeedbackCommand command);

    @POST(UrlExtras.User.SSO_INFO)
    Observable<GetSsoInfoResponse> getSsoInfo(@Body GetSsoInfoCommand command);

    @POST(UrlExtras.RestUser.UPDATE_PERSONALIZE)
    Observable<RegisterUserResponse> updatePersonalize(@Body UpdatePersonalizeCommand command);

    @POST(UrlExtras.RestUser.CANCEL_ACCOUNT)
    Observable<NonStandardResponse> cancelAccount();

    @POST(UrlExtras.RestFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> sendSubmitForm(@Body BaseFeedbackCommandCommand command);

    @POST(UrlExtras.User.SET_PUSH_CREDENTIAL)
    Observable<NonStandardResponse> setPushCredential(@Body SetPushCredentialServiceCommand loginUserCommand);

    @POST(UrlExtras.User.DEL_PUSH_CREDENTIAL)
    Observable<NonStandardResponse> delPushCredential(@Body DelPushCredentialServiceCommand loginUserCommand);

    @POST(UrlExtras.RestUserFeedback.SUBMIT_FORM)
    Observable<NonStandardResponse> sendSubmitFormA(@Body BaseFeedbackCommandCommand command);

    @POST(UrlExtras.RestUserNode.NODE_LINK_CLICK)
    Observable<NonStandardResponse> addInHistory(@Body AddInHistoryCommand command);

    @POST(UrlExtras.System.GET_CONFIGURATION)
    Observable<ConfigurationResponse> getConfiguration(@Body GetConfigurationCommand command);

    @POST(UrlExtras.Extra.SGP_LOCATION_COMPLETE)
    Observable<GetAutoCompleteSearchLocationsResponse> sgplocationCcomplete(@Query("loc") String loc, @Query("strict") Integer strict);

    @POST(UrlExtras.RestUserSearch.ADD_TO_HISTORY)
    Observable<AddToHistoryResponse> addToHistory(@Body AddToHistoryCommand addToHistoryCommand);

    @POST(UrlExtras.RestUser.CHECK_PASSWORD)
    Observable<NonStandardResponse> checkPassword(@Body CheckPasswordCommand checkPasswordCommand);

    @POST(UrlExtras.RestEmergency.EMERGENCY)
    Observable<BaseResponse> sendEmergency(@Body EmergencyCommand emergencyCommand);

    @GET(UrlExtras.RestNode.URGENCE)
    Observable<UrgenceResponse> getUrgence();

}
