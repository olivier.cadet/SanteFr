package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by SUVOROV on 9/20/16.
 */
public class SearchFilterHugeItem implements Parcelable, Serializable {

    // TODO May be it must be long
    @SerializedName("value")
    public String value;

    @SerializedName("label")
    public String label;

    @SerializedName("checked")
    public int checked;

    @SerializedName("count")
    public int count;

    public boolean isChecked() {
        return checked == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
        dest.writeString(this.label);
        dest.writeInt(this.checked);
        dest.writeInt(this.count);
    }

    public SearchFilterHugeItem() {
    }

    protected SearchFilterHugeItem(Parcel in) {
        this.value = in.readString();
        this.label = in.readString();
        this.checked = in.readInt();
        this.count = in.readInt();
    }

    public static final Parcelable.Creator<SearchFilterHugeItem> CREATOR = new Parcelable.Creator<SearchFilterHugeItem>() {
        @Override
        public SearchFilterHugeItem createFromParcel(Parcel source) {
            return new SearchFilterHugeItem(source);
        }

        @Override
        public SearchFilterHugeItem[] newArray(int size) {
            return new SearchFilterHugeItem[size];
        }
    };
}
