package com.adyax.srisgp.data.net.command.base;

import android.os.Parcelable;

import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public abstract class ServiceCommand implements Parcelable {

    public abstract void execute(CommandExecutor executor, AppReceiver receiver, int requestCode);

}
