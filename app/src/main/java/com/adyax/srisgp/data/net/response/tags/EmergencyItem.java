package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.utils.DistanceUtils;
import com.google.gson.annotations.SerializedName;

public class EmergencyItem implements INewFields, Parcelable {

//    @SerializedName("services")
//    public Map<Long, Map<String, String>> services;
//
//    @SerializedName("values")
//    public Map<Integer, String> values;

    @SerializedName("services")
    public EmergencyServiceItem[] services;

    @SerializedName("values")
    public EmergencyValueItem[] values;

    @SerializedName("latitude")
    public double latitude;

    @SerializedName("longitude")
    public double longitude;

    @SerializedName(SearchItemHuge.DISTANCE)
    public double distance;

    // save
    private transient boolean isEmergecyVisible;

    protected EmergencyItem(Parcel in) {
        services = in.createTypedArray(EmergencyServiceItem.CREATOR);
        values = in.createTypedArray(EmergencyValueItem.CREATOR);
        latitude = in.readDouble();
        longitude = in.readDouble();
        distance = in.readDouble();
    }

    public static final Creator<EmergencyItem> CREATOR = new Creator<EmergencyItem>() {
        @Override
        public EmergencyItem createFromParcel(Parcel in) {
            return new EmergencyItem(in);
        }

        @Override
        public EmergencyItem[] newArray(int size) {
            return new EmergencyItem[size];
        }
    };

    public String[] getValues() {
        String[] strings = new String[values.length];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = values[i].name;
        }
        return strings;
    }

    public String[] getServices() {
        String[] strings = new String[services.length];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = services[i].name;
        }
        return strings;
    }

    public Long getValueIndex(String value) {
        for (EmergencyValueItem e : values) {
            if (TextUtils.equals(e.name, value)) {
                return e.value;
            }
        }
        return -1L;
    }

    public Long getServiceIndex(String value) {
        for (EmergencyServiceItem e : services) {
            if (TextUtils.equals(e.name, value)) {
                return e.sid != null ? e.sid : -1L;
            }
        }
        return -1L;
    }

    public int getServiceSpinnerIndex(Long sid) {
        for (int i = 0; i < services.length; i++) {
            if (sid.equals(services[i].sid)) {
                return i;
            }
        }
        return -1;
    }

    public int getValueSpinnerIndex(long value) {
        for (int i = 0; i < values.length; i++) {
            if (values[i].value == value) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeTypedArray(services, i);
        dest.writeTypedArray(values, i);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeDouble(distance);
    }

    public boolean isEmergencyButton(String geoLocation) {
        if (geoLocation != null) {
            final String[] split = geoLocation.split(",");
            if (split != null && split.length == 3) {
                try {
                    final int realDistance = DistanceUtils.calculateDistanceInMetr(
                            Double.valueOf(split[0]), Double.valueOf(split[1]), latitude, longitude);
                    if (realDistance > distance) {
                        isEmergecyVisible = false;
                        return isEmergecyVisible;
                    }
                } catch (Exception e) {

                }
            }
        }
        isEmergecyVisible = services != null && services.length > 0;
        return isEmergecyVisible;
    }

    public boolean isEmergecyVisible() {
        return isEmergecyVisible;
    }
}
