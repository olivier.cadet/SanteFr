package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class IdValuePair {

    @SerializedName("id")
    public String id;

    @SerializedName("value")
    public String value;

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static IdValuePair fromString(String s) {
        Gson gson = new Gson();
        return gson.fromJson(s, IdValuePair.class);
    }
}
