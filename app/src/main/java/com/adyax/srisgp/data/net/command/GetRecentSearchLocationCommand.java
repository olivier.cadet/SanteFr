package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class GetRecentSearchLocationCommand extends ServiceCommand {

    public GetRecentSearchLocationCommand() {
    }

    protected GetRecentSearchLocationCommand(Parcel in) {
    }

    public static final Creator<GetRecentSearchLocationCommand> CREATOR = new Creator<GetRecentSearchLocationCommand>() {
        @Override
        public GetRecentSearchLocationCommand createFromParcel(Parcel in) {
            return new GetRecentSearchLocationCommand(in);
        }

        @Override
        public GetRecentSearchLocationCommand[] newArray(int size) {
            return new GetRecentSearchLocationCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getRecentSearchLocation(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
