package com.adyax.srisgp.data.net.response.tags;

import android.content.Context;
import androidx.annotation.NonNull;

import com.adyax.srisgp.R;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/20/16.
 */
public class ErrorItem {

    public static final int WRONG_USER_NAME = 401001;
    public static final int WRONG_PASSWORD = 401002;
    public static final int SAME_PASSWORD = 4010020;
    public static final int ACCOUNT_TEMPORARY_BLOCKED = 401004;
    public static final int EMAIL_HAS_NOT_BEEN_CONFIRMED = 403002;
    public static final int USER_HAS_NOT_BEEN_REGISTERED_VIA_SSO = 403003;
    // registered social network an login social netword is different
    public static final int OTHER_SOCIAL = 403004;
    public static final int EMAIL_ALREADY_EXISTS = 406002;
    public static final int USER_IS_NOT_LOGIN = 406008;
    public static final int SSO_CLIENT_ALREADY_REGISTERED = 406015;
    public static final int INVALID_PARAMETER_URL = 406035;
    public static final int NODE_RESOURCE_IS_NOT_FOUND = 406036;
    public static final int IT_IS_SAME_PASSWORD = 406043;
    public static final int MAINTENANCE_MODE = 503;

    @SerializedName("error_code")
    public int error_code;

    @SerializedName("error_message")
    public String error_message;

    public ErrorItem(){

    }

    public ErrorItem(int error_code, String error_message) {
        this.error_code = error_code;
        this.error_message = error_message;
    }

    public String getLocalizeErrorMessage(Context context) {
        if(context!=null) {
            switch (error_code) {
                case USER_HAS_NOT_BEEN_REGISTERED_VIA_SSO:
                    return getString(context, R.string.error_onnecting_thirdparty_service);
                case EMAIL_HAS_NOT_BEEN_CONFIRMED:
                    return getString(context, R.string.my_account_confirm_error);
                case OTHER_SOCIAL:
                    return getString(context, R.string.error_onnecting_thirdparty_service);
                case EMAIL_ALREADY_EXISTS:
                    return getString(context, R.string.account_already_exists);
                case WRONG_PASSWORD:
                    return getString(context, R.string.authentication_pass_error);
                case SAME_PASSWORD:
                    return getString(context, R.string.same_password);
                case WRONG_USER_NAME:
                    return getString(context, R.string.authentication_email_unknown_error);
                case ACCOUNT_TEMPORARY_BLOCKED:
                    return getString(context, R.string.account_temporary_blocked);
                case SSO_CLIENT_ALREADY_REGISTERED:
                    return getString(context, R.string.account_already_exists);
            }
        }
        return error_message;
    }

    @NonNull
    private String getString(Context context, int resId) {
        return context.getString(resId);
    }

    public String getLocalizeErrorMessage() {
        return error_message;
    }

    public boolean isNotLogin() {
        switch(error_code) {
            case USER_IS_NOT_LOGIN:
                return true;
        }
        return false;
    }

    public int getErrorCode() {
        return error_code;
    }
}
