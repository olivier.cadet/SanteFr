package com.adyax.srisgp.data.net.response;

import android.text.TextUtils;

/**
 * Created by SUVOROV on 9/19/16.
 */

public class NonStandardResponse extends BaseResponse {

    private String response;

    public NonStandardResponse(String response) {
        super();
        this.response = response;
    }

    public boolean isSuccess() {
        return TextUtils.equals(response, "[true]")|| TextUtils.equals(response, "[1]");
    }
}
