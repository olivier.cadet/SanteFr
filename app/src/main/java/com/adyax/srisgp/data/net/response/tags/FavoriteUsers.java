package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/19/16.
 */
public class FavoriteUsers implements Parcelable {

    @SerializedName("items")
    public FavoritesItem[] items;

    @SerializedName("has_more")
    public int has_more;

    public boolean hasMore() {
        return has_more == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(this.items, flags);
        dest.writeInt(this.has_more);
    }

    public FavoriteUsers() {
    }

    protected FavoriteUsers(Parcel in) {
        this.items = in.createTypedArray(FavoritesItem.CREATOR);
        this.has_more = in.readInt();
    }

    public static final Parcelable.Creator<FavoriteUsers> CREATOR = new Parcelable.Creator<FavoriteUsers>() {
        @Override
        public FavoriteUsers createFromParcel(Parcel source) {
            return new FavoriteUsers(source);
        }

        @Override
        public FavoriteUsers[] newArray(int size) {
            return new FavoriteUsers[size];
        }
    };
}
