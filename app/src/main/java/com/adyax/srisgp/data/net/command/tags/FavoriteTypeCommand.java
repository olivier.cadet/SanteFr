package com.adyax.srisgp.data.net.command.tags;

/**
 * Created by SUVOROV on 10/2/16.
 */

public enum FavoriteTypeCommand {
    // sgp_sso_mail need to replace on NULL
    add, sgp_sso_google, remove;
}
