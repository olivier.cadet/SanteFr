package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class UrgenceCommand extends ServiceCommand {

    public String url;
    public String text;

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getUrlUrgence(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeString(text);
    }

    public UrgenceCommand() {
    }

    protected UrgenceCommand(Parcel in) {
        url = in.readString();
        text = in.readString();
    }

    public static final Creator<UrgenceCommand> CREATOR = new Creator<UrgenceCommand>() {
        @Override
        public UrgenceCommand createFromParcel(Parcel source) {
            return new UrgenceCommand(source);
        }

        @Override
        public UrgenceCommand[] newArray(int size) {
            return new UrgenceCommand[size];
        }
    };
}
