package com.adyax.srisgp.data.net.response.tags;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class User implements INewFields {

    public static final String UID = "uid";
    public static final String BIRTH_YEAR = "birth_year";
    public static final String SEX = "sex";
    public static final String CITY = "city";
    public static final String MANUAL_INTERESTS = "manual_interests";
    public static final String SEARCH_INTERESTS = "search_interests";
    public static final String NOTIFY_BY_MOBILE = "notify_mobile";
    public static final String NOTIFY_BY_EMAIL = "notify_by_email";
    public static final String NOTIFY_BY_EMAIL_DAILY = "notify_by_email_daily";
    public static final String NOTIFY_BY_EMAIL_WEEKLY = "notify_by_email_weekly";
    public static final String DELETABLE = "deletable";
    public static final String TRACK_ACTIVITY = "track_activity";

    @SerializedName(UID)
    public String uid;

    @SerializedName(MAIL)
    public String mail;

    @SerializedName(BIRTH_YEAR)
    public String birth_year;

    @SerializedName(SEX)
    public IdValuePair sex;

    // RegisterUserResponse +
    @SerializedName(CITY)
    public IdValuePair city;
    //
//    @SerializedName("regions")
//    public Region[] regions;
//
    // RegisterUserResponse +
    @SerializedName(MANUAL_INTERESTS)
    public ArrayList<IntIdValuePair> manualInterests;

    @SerializedName(SEARCH_INTERESTS)
    public ArrayList<IntIdValuePair> searchInterests;

    @SerializedName(NOTIFY_BY_MOBILE)
    public IntIdValuePair notifyByMobile;

    // RegisterUserResponse +
    @SerializedName(NOTIFY_BY_EMAIL)
    public IntIdValuePair notifyByEmail;

    // RegisterUserResponse +
    @SerializedName(NOTIFY_BY_EMAIL_DAILY)
    public IntIdValuePair notifyByEmailDaily;

    // RegisterUserResponse +
    @SerializedName(NOTIFY_BY_EMAIL_WEEKLY)
    public IntIdValuePair notifyByEmailWeekly;


    // RegisterUserResponse -
//    @SerializedName(" search_interests")
//    public SearchInterest[] search_interests;

    @SerializedName(DELETABLE)
    public int deletable;

    @SerializedName(TRACK_ACTIVITY)
    public int track_activity;

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static User fromString(String s) {
        Gson gson = new Gson();
        return gson.fromJson(s, User.class);
    }

    public ArrayList<IntIdValuePair> getManualInterests() {
        if(manualInterests==null){
            Timber.w("manualInterests is null");
            return new ArrayList<>();
        }
        return manualInterests;
    }

    public ArrayList<IntIdValuePair> getSearchInterests() {
        if(searchInterests==null){
            Timber.w("searchInterests is null");
            return new ArrayList<>();
        }
        return searchInterests;
    }

    public boolean isNotifyByMobile() {
        return notifyByMobile != null && notifyByMobile.id == 1;
    }

    public boolean isNotifyByEmail() {
        return notifyByEmail != null && notifyByEmail.id == 1;
    }

    public boolean isNotifyByEmailDaily(){
        return notifyByEmailDaily != null && notifyByEmailDaily.id == 1;
    }

    public boolean isNotifyByEmailWeekly() {
        return notifyByEmailWeekly != null && notifyByEmailWeekly.id == 1;
    }
}
