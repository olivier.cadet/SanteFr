package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 7/17/17.
 */

public class ContentMaintenanceConfiguratonItem implements Parcelable {

    public String getTeaserActionText() {
        return teaser_action_text;
    }

    @SerializedName("teaser_action_text")
    public String teaser_action_text;

    protected ContentMaintenanceConfiguratonItem(Parcel in) {
        teaser_action_text = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(teaser_action_text);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ContentMaintenanceConfiguratonItem> CREATOR = new Creator<ContentMaintenanceConfiguratonItem>() {
        @Override
        public ContentMaintenanceConfiguratonItem createFromParcel(Parcel in) {
            return new ContentMaintenanceConfiguratonItem(in);
        }

        @Override
        public ContentMaintenanceConfiguratonItem[] newArray(int size) {
            return new ContentMaintenanceConfiguratonItem[size];
        }
    };
}
