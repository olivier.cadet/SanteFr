package com.adyax.srisgp.data.gson_extention;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by anton.kobylianskiy on 2/15/17.
 */

public class InterestNodeCountDeserializer implements JsonDeserializer<Map<Long, Integer>> {
    @Override
    public Map<Long, Integer> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Map<Long, Integer> result = new HashMap<>();

        JsonObject jsonObject = json.getAsJsonObject();
        for (Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue().getAsInt();
            result.put(Long.parseLong(key), value);
        }
        return result;
    }
}
