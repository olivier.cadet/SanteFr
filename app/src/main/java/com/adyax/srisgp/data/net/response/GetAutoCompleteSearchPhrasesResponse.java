package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.SearchResult;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class GetAutoCompleteSearchPhrasesResponse extends BaseResponse implements Parcelable {

    @SerializedName("phrase_suggestions")
    public List<String> phrase_suggestions;

    @SerializedName("search_results")
    public List<SearchResult> search_results;

    @SerializedName("history")
    public List<String> history;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.history);
        dest.writeStringList(this.phrase_suggestions);
        dest.writeList(this.search_results);
    }

    public GetAutoCompleteSearchPhrasesResponse() {
    }

    protected GetAutoCompleteSearchPhrasesResponse(Parcel in) {
        this.history = in.createStringArrayList();
        this.phrase_suggestions = in.createStringArrayList();
        this.search_results = new ArrayList<SearchResult>();
        in.readList(this.search_results, SearchResult.class.getClassLoader());
    }

    public static final Parcelable.Creator<GetAutoCompleteSearchPhrasesResponse> CREATOR = new Parcelable.Creator<GetAutoCompleteSearchPhrasesResponse>() {
        @Override
        public GetAutoCompleteSearchPhrasesResponse createFromParcel(Parcel source) {
            return new GetAutoCompleteSearchPhrasesResponse(source);
        }

        @Override
        public GetAutoCompleteSearchPhrasesResponse[] newArray(int size) {
            return new GetAutoCompleteSearchPhrasesResponse[size];
        }
    };

    public void correctBrackets(){
        correct(phrase_suggestions);
        correct(history);
    }

    private void correct(List<String> strings){
        if(strings!=null) {
            for (int i = 0; i < strings.size(); i++) {
                String s = strings.get(i);
                s = s.replaceAll("[\\p{Ps}\\p{Pe}]", "");
                strings.set(i, s);
            }
        }
    }
}
