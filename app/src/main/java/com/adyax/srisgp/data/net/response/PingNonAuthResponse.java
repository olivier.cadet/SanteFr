package com.adyax.srisgp.data.net.response;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class PingNonAuthResponse extends BaseResponse{

    @SerializedName("uid")
    public long uid;

}
