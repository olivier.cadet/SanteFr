package com.adyax.srisgp.data.gson_extention;

import com.adyax.srisgp.data.net.response.AlertResponse;
import com.adyax.srisgp.data.net.response.BaseResponse;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.NonStandardResponse;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import okhttp3.internal.http.RealResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Created by SUVOROV on 9/19/16.
 */
public final class NonStandardConverter extends Converter.Factory {

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations,
                                                            Retrofit retrofit) {
        if (type == NonStandardResponse.class) {
            return new Converter<ResponseBody, BaseResponse>() {
                @Override
                public BaseResponse convert(ResponseBody value) throws IOException {
                    return new NonStandardResponse(value.string());
                }
            };
        } else if (type == CitiesResponse.class) {
            return new Converter<ResponseBody, BaseResponse>() {
                @Override
                public BaseResponse convert(ResponseBody value) throws IOException {
                    return new CitiesResponse(value.string());
                }
            };
        } else if (type == TaxonomyVocabularyTermsResponse.class) {
            return new Converter<ResponseBody, BaseResponse>() {
                @Override
                public BaseResponse convert(ResponseBody value) throws IOException {
                    return new TaxonomyVocabularyTermsResponse(value.string());
                }
            };
        }
        return null;
    }

}

