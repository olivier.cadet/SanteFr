package com.adyax.srisgp.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings.Secure;
import androidx.core.app.ActivityCompat;
import android.telephony.TelephonyManager;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class UUIDUtils {

	private static final String HASH_STANDARD = "MD5";
	public static int counter = 0;

	@SuppressLint("HardwareIds")
	public static final String generateUniqueID(Context context) {
		String result = null;
		try {
			final TelephonyManager telephonyManager = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (telephonyManager != null) {
				result = telephonyManager.getDeviceId();
			}
		} catch (Exception e) {
//			MyLog.getMyLog().e(e);
		}
		if(result!=null && !result.equals("000000000000000"))
			return result;
		try {
			result = Secure.getString(context.getApplicationContext()
					.getContentResolver(), Secure.ANDROID_ID);
		} catch (Exception e) {
//			MyLog.getMyLog().e(e);
		}
		if (result == null) {
			result = "355" + Build.BOARD.length() % 10 + Build.BRAND.length()
					% 10 + Build.CPU_ABI.length() % 10 + Build.DEVICE.length()
					% 10 + Build.DISPLAY.length() % 10 + Build.HOST.length()
					% 10 + Build.ID.length() % 10 + Build.MANUFACTURER.length()
					% 10 + Build.MODEL.length() % 10 + Build.PRODUCT.length()
					% 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10
					+ Build.USER.length() % 10;
		}
		result = result + Build.TIME;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance(HASH_STANDARD);
			byte[] messageDigest = md.digest(result.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			result = number.toString(16);

			while (result.length() < 32) {
				result = "0" + result;
			}

		} catch (NoSuchAlgorithmException e) {
//			MyLog.getMyLog().e(e);
		}
		if("000000000000000".equals(result)){
			return "0123456789";
		}
		return result;
	}

	public static synchronized String getUUID() {
		final String uuid = UUID.randomUUID().toString().replaceAll("-", "");
//		if(CompileKey.EXTENDED_ID_MESSAGE) {
//			counter++;
//			return String.format("%04x%s", counter, uuid).toLowerCase();
//		}
		return uuid;
	}

	public static String getImageHash(byte[] avatarImage) {
		if(avatarImage!=null) {
			try {
				final MessageDigest md = MessageDigest.getInstance(HASH_STANDARD);
				final byte[] md5sum = md.digest(avatarImage);
				return String.format("%032X", new BigInteger(1, md5sum));
			} catch (NoSuchAlgorithmException e) {
//				MyLog.getMyLog().e(e);
			}
		}
		return null;
	}

}
