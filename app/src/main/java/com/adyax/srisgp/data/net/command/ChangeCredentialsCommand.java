package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import androidx.annotation.IntDef;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class ChangeCredentialsCommand extends ServiceCommand implements INewFields{

    public static final int CHANGE_EMAIL = 0;
    public static final int CHANGE_PASS = 1;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({CHANGE_EMAIL, CHANGE_PASS})
    public @interface ChangeCredentialsMode {}

    @SerializedName(MAIL)
    public String email;

    @SerializedName(PASS)
    public String newPass;

    private @ChangeCredentialsMode int mode;

    transient public String oldPass;

    public ChangeCredentialsCommand(String credential, @ChangeCredentialsMode int mode, String oldPass) {
        this.mode = mode;
//        if(BuildConfig.DEBUG) {
//            oldPass = "qwerty";
//        }
        switch (mode) {
            case CHANGE_EMAIL:
                this.email = credential;
                break;

            case CHANGE_PASS:
                this.newPass = credential;
                break;
        }
        this.oldPass = oldPass;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.updateCredentials(this, receiver, requestCode);
    }

    protected ChangeCredentialsCommand(Parcel in) {
        email = in.readString();
        newPass = in.readString();
        oldPass = in.readString();
        mode= in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(newPass);
        dest.writeString(oldPass);
        dest.writeInt(mode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChangeCredentialsCommand> CREATOR = new Creator<ChangeCredentialsCommand>() {
        @Override
        public ChangeCredentialsCommand createFromParcel(Parcel in) {
            return new ChangeCredentialsCommand(in);
        }

        @Override
        public ChangeCredentialsCommand[] newArray(int size) {
            return new ChangeCredentialsCommand[size];
        }
    };

    public String getOldPassword() {
        return oldPass;
    }

    public String getNewPassword() {
        return newPass;
    }

    public String getEmail() {
        return email;
    }

    public boolean isChangeEmail() {
        return email!=null;
    }

    public int getMode() {
        return mode;
    }
}
