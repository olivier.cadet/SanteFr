package com.adyax.srisgp.data.tracker;

import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;

/**
 * Created by SUVOROV on 7/7/17.
 */

public interface IHistoryRecentUpdateHelper {
    void add(SearchCommand searchCommand, SearchResponseHuge response);
    void tabChanged(int position);
    void reset();
    boolean isActive();
    HistoryRecentUpdateHelper.SearchData getBestSearchData();
}
