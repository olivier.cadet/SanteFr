package com.adyax.srisgp.data.net.response;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.ContentMaintenanceConfiguratonItem;
import com.adyax.srisgp.data.net.response.tags.ImageConfigurationItem;
import com.adyax.srisgp.data.net.response.tags.NotificationUsers;
import com.adyax.srisgp.data.net.response.tags.SearchConfigurationItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class ConfigurationResponse extends BaseResponse implements Parcelable {

    public static final String IMAGES = "images";
    public static final String SEARCH = "search";

    @SerializedName(IMAGES)
    public ImageConfigurationItem images;

    @SerializedName(SEARCH)
    public SearchConfigurationItem search;

    public ContentMaintenanceConfiguratonItem getContentMaintenance() {
        return content_maintenance;
    }

    @SerializedName(SearchItemHuge.CONTENT_MAINTENANCE)
    public ContentMaintenanceConfiguratonItem content_maintenance;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.images, flags);
        dest.writeParcelable(this.search, flags);
    }

    public ConfigurationResponse(Context context) {
        search=new SearchConfigurationItem(context);
    }

    protected ConfigurationResponse(Parcel in) {
        this.images = in.readParcelable(NotificationUsers.class.getClassLoader());
        this.search = in.readParcelable(NotificationUsers.class.getClassLoader());
    }

    public static final Creator<ConfigurationResponse> CREATOR = new Creator<ConfigurationResponse>() {
        @Override
        public ConfigurationResponse createFromParcel(Parcel source) {
            return new ConfigurationResponse(source);
        }

        @Override
        public ConfigurationResponse[] newArray(int size) {
            return new ConfigurationResponse[size];
        }
    };

    public ImageConfigurationItem getImages() {
        return images;
    }

    public SearchConfigurationItem getSearch() {
        return search;
    }

}
