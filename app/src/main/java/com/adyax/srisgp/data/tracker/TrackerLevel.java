package com.adyax.srisgp.data.tracker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 9/22/16.
 */

public enum TrackerLevel {

    HOMEPAGE(1),
    RESEARCH(2),
    SEARCH_RESULTS_FOR_INFORMATION(3),
    RESEARCH_RESULTS_FIND(4),
    UNIT_CONTENTS_INFORMATION(5),
    UNIT_CONTENTS_FIND(6),
    ACCOUNT_MANAGEMENT(7),
    HISTORICAL(8),
    NOTIFICATIONS(9),
    FAVORITES(10),
    TRANSVERSE_PAGES(11),
    PLUS(14),
    UNKNOWN(0);


    private int value;
    private static Map<Integer, TrackerLevel> map = new HashMap<>();

    TrackerLevel(int value) {
        this.value = value;
    }

    static {
        for (TrackerLevel pageType : TrackerLevel.values()) {
            map.put(pageType.value, pageType);
        }
    }

    public static TrackerLevel valueOf(int pageType) {
        return (TrackerLevel) map.get(pageType);
    }

    public int getValue() {
        return value;
    }

    public String getValueStr() {
        return String.valueOf(value);
    }
}