package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 11/14/16.
 */

public class FeedbackFormValue implements Parcelable {

    @SerializedName("field_helpful")
    private String helpful;

//    @SerializedName("field_helpless_reason")
    @SerializedName("field_helpless_reason")
    private List<String> reason;

    @SerializedName(BaseFeedbackCommandCommand.FIELD_URL)
    private String url;

    @SerializedName("field_other_helpless_reason")
    private String otherReason;

    private String helplessReasonFieldName;

    public FeedbackFormValue(String helplessReasonFieldName, String helpful, List<String> reason, String url, String otherReason) {
        this.helplessReasonFieldName = helplessReasonFieldName;
        this.helpful = helpful;
        this.reason = reason;
        this.url = url;
        this.otherReason = otherReason;
    }

    public String getHelpful() {
        return helpful;
    }

    public List<String> getReason() {
        return reason;
    }

    public String getUrl() {
        return url;
    }

    public String getOtherReason() {
        return otherReason;
    }

    public String getHelplessReasonFieldName() {
        return helplessReasonFieldName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.helpful);
        dest.writeStringList(this.reason);
        dest.writeString(this.url);
        dest.writeString(this.otherReason);
        dest.writeString(this.helplessReasonFieldName);
    }

    protected FeedbackFormValue(Parcel in) {
        this.helpful = in.readString();
        this.reason = in.createStringArrayList();
        this.url = in.readString();
        this.otherReason = in.readString();
        this.helplessReasonFieldName = in.readString();
    }

    public static final Creator<FeedbackFormValue> CREATOR = new Creator<FeedbackFormValue>() {
        @Override
        public FeedbackFormValue createFromParcel(Parcel source) {
            return new FeedbackFormValue(source);
        }

        @Override
        public FeedbackFormValue[] newArray(int size) {
            return new FeedbackFormValue[size];
        }
    };
}
