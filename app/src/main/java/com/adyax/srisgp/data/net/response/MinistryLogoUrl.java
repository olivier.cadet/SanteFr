package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 5/12/17.
 */

public class MinistryLogoUrl extends BaseResponse implements Parcelable {
    private String logoUrl;

    public MinistryLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getLogoUrl() {
        return logoUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.logoUrl);
    }

    protected MinistryLogoUrl(Parcel in) {
        this.logoUrl = in.readString();
    }

    public static final Parcelable.Creator<MinistryLogoUrl> CREATOR = new Parcelable.Creator<MinistryLogoUrl>() {
        @Override
        public MinistryLogoUrl createFromParcel(Parcel source) {
            return new MinistryLogoUrl(source);
        }

        @Override
        public MinistryLogoUrl[] newArray(int size) {
            return new MinistryLogoUrl[size];
        }
    };
}
