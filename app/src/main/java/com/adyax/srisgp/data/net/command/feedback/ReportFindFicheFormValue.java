package com.adyax.srisgp.data.net.command.feedback;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

/**
 * Created by SUVOROV on 2/15/17.
 */
public class ReportFindFicheFormValue implements Parcelable {

    @SerializedName(ReportBuilder.FIELD_CONCERNED_PARTY)
    private String field_concerned_party;

    @SerializedName(ReportBuilder.FIELD_EMAIL)
    private String field_email;

    @SerializedName(ReportBuilder.FIELD_ERROR_INFO)
    private String field_error_info;

    @SerializedName(ReportBuilder.FIELD_NOM)
    private String field_nom;

    @SerializedName(ReportBuilder.FIELD_PRENOM)
    private String field_prenom;

    @SerializedName(ReportBuilder.FIELD_PROFESSION_OF_REPORTER)
    private String field_profession_of_reporter;

    @SerializedName(ReportBuilder.FIELD_PROPOSED_INFO)
    private String field_proposed_info;

    @SerializedName(ReportBuilder.FIELD_TELEPHONE_FIXE)
    private String field_telephone_fixe;

    @SerializedName(ReportBuilder.FIELD_URL)
    private String field_url;

    @SerializedName(ReportBuilder.FIELD_IDENTIFIANT_RPPS)
    private String field_identifiant_rpps;

    public ReportFindFicheFormValue(Map<String, String> map){
        this.field_error_info = map.get(ReportBuilder.FIELD_ERROR_INFO);
        this.field_proposed_info = map.get(ReportBuilder.FIELD_PROPOSED_INFO);
        this.field_url = map.get(ReportBuilder.FIELD_URL);
        this.field_email = map.get(ReportBuilder.FIELD_EMAIL);
        this.field_concerned_party = map.get(ReportBuilder.FIELD_CONCERNED_PARTY);
        this.field_nom = map.get(ReportBuilder.FIELD_NOM);
        this.field_prenom = map.get(ReportBuilder.FIELD_PRENOM);
        this.field_profession_of_reporter = map.get(ReportBuilder.FIELD_PROFESSION_OF_REPORTER);
        this.field_telephone_fixe = map.get(ReportBuilder.FIELD_TELEPHONE_FIXE);
        this.field_identifiant_rpps = map.get(ReportBuilder.FIELD_IDENTIFIANT_RPPS);
    }

    protected ReportFindFicheFormValue(Parcel in) {
        field_error_info = in.readString();
        field_proposed_info = in.readString();
        field_url = in.readString();
        field_email = in.readString();
        field_concerned_party = in.readString();
        field_nom = in.readString();
        field_prenom = in.readString();
        field_profession_of_reporter = in.readString();
        field_telephone_fixe = in.readString();
        field_identifiant_rpps = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(field_error_info);
        dest.writeString(field_proposed_info);
        dest.writeString(field_url);
        dest.writeString(field_email);
        dest.writeString(field_concerned_party);
        dest.writeString(field_nom);
        dest.writeString(field_prenom);
        dest.writeString(field_profession_of_reporter);
        dest.writeString(field_telephone_fixe);
        dest.writeString(field_identifiant_rpps);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ReportFindFicheFormValue> CREATOR = new Creator<ReportFindFicheFormValue>() {
        @Override
        public ReportFindFicheFormValue createFromParcel(Parcel in) {
            return new ReportFindFicheFormValue(in);
        }

        @Override
        public ReportFindFicheFormValue[] newArray(int size) {
            return new ReportFindFicheFormValue[size];
        }
    };
}
