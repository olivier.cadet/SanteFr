package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class PingCommand implements Parcelable {
public class CancelAccountCommand extends ServiceCommand {

    public CancelAccountCommand() {
    }

    protected CancelAccountCommand(Parcel in) {
    }

    public static final Creator<CancelAccountCommand> CREATOR = new Creator<CancelAccountCommand>() {
        @Override
        public CancelAccountCommand createFromParcel(Parcel in) {
            return new CancelAccountCommand(in);
        }

        @Override
        public CancelAccountCommand[] newArray(int size) {
            return new CancelAccountCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.cancelAccount(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
