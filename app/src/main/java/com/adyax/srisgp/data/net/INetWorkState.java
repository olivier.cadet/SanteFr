package com.adyax.srisgp.data.net;

/**
 * Created by SUVOROV on 9/26/16.
 */
public interface INetWorkState {
    boolean isLoggedIn();
    void logOut();
}
