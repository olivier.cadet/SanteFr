package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.xiti.ClickType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.data.tracker.TrackerHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by anton.kobylianskiy on 10/7/16.
 */

public class AddOrRemoveFavoriteCommand extends ServiceCommand {
    public static final String ACTION_ADD = "add";
    public static final String ACTION_REMOVE = "remove";

    private List<Long> nids;

    @SerializedName("action")
//    private FavoriteTypeCommand action = FavoriteTypeCommand.add;
    private String action;

//TrackerLevel trackerLevel, NodeType nodeType
    public AddOrRemoveFavoriteCommand(TrackerLevel trackerLevel, List<Long> ids, String action, IGetUrl getUrl) {
        this.action = action;
        this.nids = ids;
        updateLevel2Click(trackerLevel, getUrl);
        addTouchGesture(action, trackerLevel, getUrl);
    }

    public AddOrRemoveFavoriteCommand(TrackerLevel trackerLevel, long id, String action, IGetUrl getUrl) {
        List<Long> nids = new LinkedList<>();
        nids.add(id);
        this.action = action;
        this.nids = nids;
        updateLevel2Click(trackerLevel, getUrl);
        addTouchGesture(action, trackerLevel, getUrl);
    }

    public AddOrRemoveFavoriteCommand(Xiti xiti, long id, String action) {
        List<Long> nids = new LinkedList<>();
        nids.add(id);
        this.action = action;
        this.nids = nids;
        new TrackerHelper().addTouchGesture(xiti,
                ACTION_ADD.equals(action) ? ClickType.clic_contenu_ajout_aux_favoris : ClickType.clic_contenu_retrait_des_favoris);
    }

    private void updateLevel2Click(TrackerLevel trackerLevel, IGetUrl getUrl) {
        if(trackerLevel!=TrackerLevel.UNKNOWN) {
            if (getUrl != null && getUrl.getXiti() != null) {
                getUrl.getXiti().setLevel2Click(trackerLevel);
            }
        }else{
            Timber.w("trackerLevel is unknown");
        }
    }

    private void addTouchGesture(String action, TrackerLevel trackerLevel, IGetUrl getUrl) {
        if(trackerLevel!=TrackerLevel.UNKNOWN&& getUrl!=null) {
            Xiti xiti = getUrl.getXiti();
            if(xiti !=null) {
                TrackerLevel clickTrackerLevel = xiti.getClickTrackerLevel();
                if(trackerLevel!=clickTrackerLevel) {
                    xiti=null;
                }
                new TrackerHelper(getUrl).addTouchGesture(trackerLevel,
                        ACTION_ADD.equals(action) ? ClickType.clic_contenu_ajout_aux_favoris : ClickType.clic_contenu_retrait_des_favoris);
            }
        }else{
            if(BuildConfig.DEBUG){
                throw new IllegalArgumentException();
            }
        }
    }

    public String getAction() {
        return action;
    }

    public List<Long> getNids() {
        return nids;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.addOrRemove(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.nids);
        dest.writeString(this.action);
    }

    protected AddOrRemoveFavoriteCommand(Parcel in) {
        this.nids = new ArrayList<Long>();
        in.readList(this.nids, Long.class.getClassLoader());
        this.action = in.readString();
    }

    public static final Creator<AddOrRemoveFavoriteCommand> CREATOR = new Creator<AddOrRemoveFavoriteCommand>() {
        @Override
        public AddOrRemoveFavoriteCommand createFromParcel(Parcel source) {
            return new AddOrRemoveFavoriteCommand(source);
        }

        @Override
        public AddOrRemoveFavoriteCommand[] newArray(int size) {
            return new AddOrRemoveFavoriteCommand[size];
        }
    };
}
