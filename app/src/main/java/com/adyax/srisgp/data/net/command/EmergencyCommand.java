package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class EmergencyCommand implements Parcelable {
public class EmergencyCommand extends ServiceCommand {

    public static final String EMERGENCY_DEFAULT = "EmergencyDefault";
    //    private final long nid;
//    private final long sid;
//    private final long value;
    private long nid;
    private long sid;
    private long value;

    public EmergencyCommand(long nid, long sid, long value){
        this.nid = nid;
        this.sid = sid;
        this.value = value;
    }

    protected EmergencyCommand(Parcel in) {
        nid = in.readLong();
        sid = in.readLong();
        value = in.readLong();
    }

    public static final Creator<EmergencyCommand> CREATOR = new Creator<EmergencyCommand>() {
        @Override
        public EmergencyCommand createFromParcel(Parcel in) {
            return new EmergencyCommand(in);
        }

        @Override
        public EmergencyCommand[] newArray(int size) {
            return new EmergencyCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.sendEmergency(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(nid);
        parcel.writeLong(sid);
        parcel.writeLong(value);
    }

    public void setDebug() {
        if(BuildConfig.DEBUG) {
            nid = 0;
            sid = 0;
            value = 0;
        }
    }

    public String getKey() {
//        return String.format("%d-%d", nid, sid);
//        return String.format("%d", nid);
        return EMERGENCY_DEFAULT;
    }

    public Long getValue() {
        return value;
    }

}
