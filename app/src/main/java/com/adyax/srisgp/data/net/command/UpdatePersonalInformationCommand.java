package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kirill on 10.11.16.
 */

public class UpdatePersonalInformationCommand extends ServiceCommand {

    @SerializedName("field_gender")
    private String gender;

    @SerializedName("field_age")
    private String yearOfBirth;

    @SerializedName("field_ville")
    private String city;

    public UpdatePersonalInformationCommand(String gender, String yearOfBirth, String city) {
        this.gender = gender;
        this.yearOfBirth = yearOfBirth;
        this.city = city;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.updatePersonalInformation(this, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.gender);
        dest.writeString(this.yearOfBirth);
        dest.writeString(this.city);
    }

    public UpdatePersonalInformationCommand() {
    }

    protected UpdatePersonalInformationCommand(Parcel in) {
        this.gender = in.readString();
        this.yearOfBirth = in.readString();
        this.city = in.readString();
    }

    public static final Creator<UpdatePersonalInformationCommand> CREATOR = new Creator<UpdatePersonalInformationCommand>() {
        @Override
        public UpdatePersonalInformationCommand createFromParcel(Parcel source) {
            return new UpdatePersonalInformationCommand(source);
        }

        @Override
        public UpdatePersonalInformationCommand[] newArray(int size) {
            return new UpdatePersonalInformationCommand[size];
        }
    };
}
