package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by SUVOROV on 9/20/16.
 */
public class SearchFilterHuge implements Parcelable, Serializable, Comparable {

    @SerializedName("label")
    public String label;

    @SerializedName("type")
    private String type;

    @SerializedName("param")
    public String param;

    @SerializedName("singular")
    public int singular;

    @SerializedName("items")
    public SearchFilterHugeItem[] items;

    @SerializedName("visibility_condition")
    public SearchFilterValue visibilityCondition;

    @SerializedName("do_not_open")
    public int doNotOpen;

    @SerializedName("do_not_close")
    public int doNotClose;

    public enum FilterType {
        CHECKBOXES("checkboxes"), SWITCHES("switch"), RADIO_BUTTONS("radios");

        private String type;

        FilterType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

        public static FilterType getDefault() {
            return CHECKBOXES;
        }

        @Nullable
        public static FilterType parse(String type) {
            if (type == null || type.isEmpty())
                return getDefault();

            for (FilterType filter : FilterType.values()) {
                if (filter.type.equals(type))
                    return filter;
            }

            return getDefault();
        }
    }

    public FilterType getFilterType() {
        return FilterType.parse(type);
    }

    public SearchFilterValue getVisibilityCondition() {
        return visibilityCondition;
    }

    public boolean isTopMargin() {
        return doNotOpen == 0 || (label != null && !label.isEmpty());
    }

    public boolean isBottomMargin() {
        return doNotClose == 0;
    }

    public ArrayList<String> getValues() {
        ArrayList<String> result = new ArrayList<>();
        for (SearchFilterHugeItem item : items)
            if (item.isChecked())
                result.add(item.value);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeString(this.type);
        dest.writeString(this.param);
        dest.writeInt(this.singular);
        dest.writeTypedArray(this.items, flags);
        dest.writeParcelable(this.visibilityCondition, flags);
        dest.writeInt(this.doNotOpen);
        dest.writeInt(this.doNotClose);
    }

    public SearchFilterHuge() {
    }

    protected SearchFilterHuge(Parcel in) {
        this.label = in.readString();
        this.type = in.readString();
        this.param = in.readString();
        this.singular = in.readInt();
        this.items = in.createTypedArray(SearchFilterHugeItem.CREATOR);
        this.visibilityCondition = in.readParcelable(SearchFilterValue.class.getClassLoader());
        this.doNotOpen = in.readInt();
        this.doNotClose = in.readInt();
    }

    public static final Creator<SearchFilterHuge> CREATOR = new Creator<SearchFilterHuge>() {
        @Override
        public SearchFilterHuge createFromParcel(Parcel source) {
            return new SearchFilterHuge(source);
        }

        @Override
        public SearchFilterHuge[] newArray(int size) {
            return new SearchFilterHuge[size];
        }
    };

    // TODO need to test
    @Override
    public int compareTo(@NonNull Object obj) {
        SearchFilterHuge searchFilterHuge=(SearchFilterHuge)obj;
        if(!TextUtils.equals(param, searchFilterHuge.param)){
            return param.compareTo(searchFilterHuge.param);
        }
//        int size1 = value.size();
//        int size2 = searchFilterHuge.value.size();
//        if(size1 != size2){
//            return false;
//        }
//        if(size1==0&& size2==0){
//            return true;
//        }
//        for(int i=0;i<size1;i++){
//            if(!TextUtils.equals(value.get(i), searchFilterHuge.value.get(i))){
//                return false;
//            }
//        }
        return 0;
    }


}
