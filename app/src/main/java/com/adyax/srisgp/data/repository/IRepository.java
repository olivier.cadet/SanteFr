package com.adyax.srisgp.data.repository;

import android.location.Location;

import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.UserResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.mvp.emergency.EmergencyArg;

import java.util.Map;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public interface IRepository {
    void saveLastUrgentAlert(AlertItem alertItem);

    AlertItem getLastUrgentAlert();

    // TODO password it is temporary solution
    void saveCredential(RegisterUserResponse user, String password);

    RegisterUserResponse getCredential();

    String getUserPassword();

    void reset();

    void addLoggedInStateListener(SharedPreferencesHelper.LoggedInStateListener loggedInStateListener);

    void removeLoggedInStateListener(SharedPreferencesHelper.LoggedInStateListener loggedInStateListener);

    // TODO it is path!!! need to replace
    void setTemporaryHeader(String header);

    String getTemporaryHeader();

    boolean appVersionHasChanged();

    void setAppVersion();

    boolean isFirstRun();

    void setNotFirstRun();

    boolean hasBeenShownGeoRequest();

    void setStatusShowGeoRequest();

    boolean checkCredential(ChangeCredentialsCommand changeCredentialsCommand);

    void updateCredentials(ChangeCredentialsCommand changeCredentialsCommand);

    void saveGeoLocation(Location location);

    String getGeoLocation();

    void setNewNotificationCount(int newNotificationCount);

    int getNewNotificationCount();

    void saveUser(UserResponse userResponse);

    String getUserUID();

    Map<Long, Integer> getInterestCount();

    void saveConfig(ConfigurationResponse configurationResponse);

    ConfigurationResponse getConfig();

    void saveLastSearchRequest(SearchCommand searchCommand);

    SearchCommand readLastSearchRequest(SearchType type);

    void saveVote(EmergencyCommand emergencyCommand);

    int getVote(EmergencyArg emergencyArg);

    boolean isVote(EmergencyArg emergencyArg);

}
