package com.adyax.srisgp.data;

import java.sql.Timestamp;

/**
 * Created by SUVOROV on 10/10/16.
 */

public class DateUtils {

    public static Timestamp getCurrentTime() {
        return new Timestamp(System.currentTimeMillis());
    }

}
