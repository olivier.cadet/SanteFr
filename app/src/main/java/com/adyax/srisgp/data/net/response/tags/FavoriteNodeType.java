package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/28/16.
 */

public enum FavoriteNodeType {
    @SerializedName("care_deals")
    care_deals,
    @SerializedName("ext_links")
    ext_links,
    @SerializedName("video")
    video,
    @SerializedName("info_sheet")
    info_sheet,
    @SerializedName("apps")
    apps,
    unknown,

////    service_de_sante(R.string.laboratory),
////    professionnel_de_sante(R.string.general_medicine);
//    care_deals(1),
//    ext_links(2),
//    info_sheet(3),
//    apps(4);
//
//    private int value;
//    private static Map map = new HashMap<>();
//
//    private FavoriteNodeType(int value) {
//        this.value = value;
//    }
//
//    static {
//        for (FavoriteNodeType pageType : FavoriteNodeType.values()) {
//            map.put(pageType.value, pageType);
//        }
//    }
//
//    public static FavoriteNodeType valueOf(int pageType) {
//        return (FavoriteNodeType) map.get(pageType);
//    }
//
//    public int getValue() {
//        return value;
//    }

}
