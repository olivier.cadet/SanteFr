package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.mvp.favorite.FavoriteType;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by SUVOROV on 9/28/16.
 */

@DatabaseTable(tableName = "favorite")
public class FavoritesItem implements Parcelable, IGetUrl, INewFields {

    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String TITLE = "title";
    public static final String TIMESTAMP = "timestamp";
    public static final String NODE_TYPE = "node_type";
    public static final String DATE_ADDED = "date_added";
    public static final String DESCRIPTION = "description";
    public static final String STATUS = "type";
    public static final String APP_STORE_LINK = "app_store_link";
    public static final String PLAY_STORE_LINK = "play_store_link";
    public static final String ICON = "icon";

    @SerializedName(NID)
    @DatabaseField(columnName = NID, uniqueCombo = true)
    public long nid;

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(TIMESTAMP)
    @DatabaseField(columnName = TIMESTAMP)
    public String timestamp;

    @SerializedName(NODE_TYPE)
    @DatabaseField(columnName = NODE_TYPE)
    public NodeType node_type;

    @SerializedName(LINK)
    @DatabaseField(columnName = LINK_DB)
    public String link;

    @SerializedName(URL)
    @DatabaseField(columnName = URL_DB)
    public String url;

    @SerializedName(DATE_ADDED)
    @DatabaseField(columnName = DATE_ADDED)
    public String date_added;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(PHONE)
    @DatabaseField(columnName = PHONE_DB)
    public String phone;

    /* 0 all 1 find 2 get informed*/
    @DatabaseField(columnName = STATUS, uniqueCombo = true)
    public int type;

    @SerializedName(PROPOSE)
    @DatabaseField(columnName = PROPOSE_DB)
    public String propose;

    @SerializedName(APP_STORE_LINK)
    @DatabaseField(columnName = APP_STORE_LINK)
    public String appStoreLink;

    @SerializedName(PLAY_STORE_LINK)
    @DatabaseField(columnName = PLAY_STORE_LINK)
    public String playStoreLink;

    @SerializedName(ICON)
    @DatabaseField(columnName = ICON)
    public IconType icon;

    @SerializedName(SearchItemHuge.ICON_URL)
    @DatabaseField(columnName = SearchItemHuge.ICON_URL)
    public String icon_url;

    public void setType(FavoriteType type) {
        this.type = type.getValue();
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return propose;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public NodeType getNodeType() {
        if (node_type == null) {
            return NodeType.UNKNOWN;
        }
        return node_type;
    }

    public String getDateAdded() {
        return date_added;
    }

    public long getId() {
        return nid;
    }

//    public String getContentUrl() {
//        return url;
//    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getCardLink() {
        switch (getNodeType()) {
            case VIDEO:
            case FICHES_INFOS:
                return url;
            case LIENS_EXTERNES:
                if (!TextUtils.isEmpty(link)) {
                    return link;
                }
            case UNKNOWN:
                break;
        }
        return url;
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return nid;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.nid);
        dest.writeString(this.title);
        dest.writeString(this.timestamp);
        dest.writeInt(this.node_type == null ? -1 : this.node_type.ordinal());
        dest.writeString(this.link);
        dest.writeString(this.url);
        dest.writeString(this.date_added);
        dest.writeString(this.description);
        dest.writeString(this.phone);
        dest.writeInt(this.type);
        dest.writeString(this.propose);
        dest.writeString(this.appStoreLink);
        dest.writeString(this.playStoreLink);
        if (this.xiti == null) {
            this.xiti = new Xiti();
        }
        dest.writeParcelable(this.xiti, flags);
        dest.writeString(this.icon.name());
        dest.writeString(this.icon_url);
    }

    public FavoritesItem() {
    }

    protected FavoritesItem(Parcel in) {
        this.nid = in.readLong();
        this.title = in.readString();
        this.timestamp = in.readString();
        int tmpNode_type = in.readInt();
        this.node_type = tmpNode_type == -1 ? null : NodeType.values()[tmpNode_type];
        this.link = in.readString();
        this.url = in.readString();
        this.date_added = in.readString();
        this.description = in.readString();
        this.phone = in.readString();
        this.type = in.readInt();
        this.propose = in.readString();
        this.appStoreLink = in.readString();
        this.playStoreLink = in.readString();
        this.xiti = in.readParcelable(Xiti.class.getClassLoader());
        this.icon = IconType.valueOf(in.readString());
        this.icon_url = in.readString();
    }

    public static final Creator<FavoritesItem> CREATOR = new Creator<FavoritesItem>() {
        @Override
        public FavoritesItem createFromParcel(Parcel source) {
            return new FavoritesItem(source);
        }

        @Override
        public FavoritesItem[] newArray(int size) {
            return new FavoritesItem[size];
        }
    };

    public IconType getIconResource() {
        if (icon == null) {
            icon = IconType.UNKNOWN;
        }
        return icon;
    }

    @Override
    public IconType getIconType() {
        return icon;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getApple() {
        return appStoreLink;
    }

    @Override
    public String getGoogle() {
        return playStoreLink;
    }

    public String getIcon_url() {
        return icon_url;
    }

}
