package com.adyax.srisgp.data.tracker;

import android.content.Context;
import android.text.TextUtils;

import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddToHistoryCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.mvp.searchresults.SearchResultsActivity;

/**
 * Created by SUVOROV on 7/7/17.
 */

public class HistoryRecentUpdateHelper implements IHistoryRecentUpdateHelper {

    private boolean bResultFound = false;
    private boolean bSearch = false;

    private final Context context;
    private final IDataBaseHelper dataBaseHelper;
    private final INetWorkState netWorkState;
    private boolean bIgnoreFirstTab;

    public class SearchData {

        public SearchCommand searchCommand;
        public SearchResponseHuge response;

        public SearchData() {

        }

        public SearchData(SearchCommand searchCommand, SearchResponseHuge response) {
            this.searchCommand = searchCommand;
            this.response = response;
        }

        public boolean isActive() {
            return searchCommand != null && response != null;
        }

        public boolean isEmpty() {
            return response.isEmpty();
        }

        @Override
        public String toString() {
            return isActive() && response.isAreaActive() ? "active area" : "non active area";
        }

        public SearchResponseHuge getResponse() {
            return response;
        }
    }

    private SearchData[] searchData = new SearchData[2];

    public HistoryRecentUpdateHelper(Context context, IDataBaseHelper dataBaseHelper, INetWorkState netWorkState) {
        this.context = context;
        this.dataBaseHelper = dataBaseHelper;
        this.netWorkState = netWorkState;
        reset();
    }


    private void addToHistory(SearchCommand searchCommand, SearchResponseHuge response, boolean isChangeTab) {
        if (!bIgnoreFirstTab && isChangeTab && response.isSuggestedType(SearchType.find)) {
            bIgnoreFirstTab = true;
        }else {
            ExecutionService.sendCommand(context, null,
                    new AddToHistoryCommand(context, searchCommand, response), ExecutionService.ADD_TO_HISTORY);
        }
        if (netWorkState.isLoggedIn()) {
//            ExecutionService.sendCommand(context, null,
//                    new AddToHistoryCommand(context, searchCommand, response), ExecutionService.ADD_TO_HISTORY);
        } else {
            if (!isChangeTab && response.isAreaActive()) {
                dataBaseHelper.insertOrUpdateHistory(new HistoryItem(context, searchCommand, response));
            }
        }
    }

    @Override
    public void add(SearchCommand searchCommand, SearchResponseHuge response) {
        bSearch = true;
        switch (searchCommand.getType()) {
            case info:
                searchData[SearchResultsActivity.INFO_PAGE] = new SearchData(searchCommand, response);
                if (searchData[SearchResultsActivity.FIND_PAGE].isActive()) {
                    addHistoryAndRecent();
                }
//                bResultFound = (response.items != null && !response.items.isEmpty());
//                if (bResultFound) {
//                    addToHistory(searchCommand, response);
//                    addRecent(searchCommand, response);
//                }
                break;
            case find:
                searchData[SearchResultsActivity.FIND_PAGE] = new SearchData(searchCommand, response);
                if (searchData[SearchResultsActivity.INFO_PAGE].isActive()) {
                    addHistoryAndRecent();
                }
//                if (!bResultFound) {
//                    addToHistory(searchCommand, response);
//                    addRecent(searchCommand, response);
//                }
                break;
            case around:
                final SearchData data = new SearchData(searchCommand, response);
                addRecent(data.searchCommand, data.response);
                break;
        }
    }

    private void addHistoryAndRecent() {
        final SearchData data = getBestSearchData();
        addToHistory(data.searchCommand, data.response, false);
        addRecent(data.searchCommand, data.response);
    }

    private void addRecent(SearchCommand searchCommand, SearchResponseHuge response) {
        if (!netWorkState.isLoggedIn()) {
            final String search = searchCommand.getSearch();
            if (!TextUtils.isEmpty(search)) {
                dataBaseHelper.addRecentWhatPhrase(SearchItem.createLocal(search));
            }
            if (response.isAreaActive()) {
                final String cityLocation = searchCommand.getCityLocation();
                if (!TextUtils.isEmpty(cityLocation)) {
                    dataBaseHelper.addRecentLocation(RecentLocationItem.createLocal(cityLocation));
                }
            }
        }
    }

    @Override
    public void tabChanged(int position) {
        if (bSearch) {
            if (position == SearchResultsActivity.FIND_PAGE || position == SearchResultsActivity.INFO_PAGE) {
                final SearchData searchData = this.searchData[position];
                if (searchData.isActive()) {
                    addToHistory(searchData.searchCommand, searchData.response, true);
                }
            }
        }
    }

    @Override
    public void reset() {
        bIgnoreFirstTab = false;
        bSearch = false;
        bResultFound = false;
        searchData[SearchResultsActivity.INFO_PAGE] = new SearchData();
        searchData[SearchResultsActivity.FIND_PAGE] = new SearchData();
    }

    @Override
    public boolean isActive() {
        return searchData[SearchResultsActivity.INFO_PAGE].isActive() &&
                searchData[SearchResultsActivity.FIND_PAGE].isActive();
    }

    @Override
    public SearchData getBestSearchData() {
        final SearchData data = searchData[SearchResultsActivity.INFO_PAGE];
        if (data.isEmpty()) {
            return searchData[SearchResultsActivity.FIND_PAGE];
        }
        return data;
    }

}
