package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class AnchorItem implements Parcelable{

    public static final String ANCHOR_ITEM = "AnchorItem";

    @SerializedName("anchor")
    public String anchor;

    @SerializedName("title")
    public String title;

    protected AnchorItem(Parcel in) {
        anchor = in.readString();
        title = in.readString();
    }

    public static final Creator<AnchorItem> CREATOR = new Creator<AnchorItem>() {
        @Override
        public AnchorItem createFromParcel(Parcel in) {
            return new AnchorItem(in);
        }

        @Override
        public AnchorItem[] newArray(int size) {
            return new AnchorItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(anchor);
        parcel.writeString(title);
    }

    public String getTitle() {
        return title;
    }

    public String getAnchor() {
        return anchor;
    }
}
