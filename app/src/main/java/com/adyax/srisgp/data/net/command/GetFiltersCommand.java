package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Kirill on 19.10.16.
 */

public class GetFiltersCommand extends ServiceCommand {
    private SearchType searchType;

    public GetFiltersCommand(SearchType searchType) {
        this.searchType = searchType;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getFilters(searchType, receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.searchType == null ? -1 : this.searchType.ordinal());
    }

    protected GetFiltersCommand(Parcel in) {
        int tmpSearchType = in.readInt();
        this.searchType = tmpSearchType == -1 ? null : SearchType.values()[tmpSearchType];
    }

    public static final Creator<GetFiltersCommand> CREATOR = new Creator<GetFiltersCommand>() {
        @Override
        public GetFiltersCommand createFromParcel(Parcel source) {
            return new GetFiltersCommand(source);
        }

        @Override
        public GetFiltersCommand[] newArray(int size) {
            return new GetFiltersCommand[size];
        }
    };
}
