package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.LoginBaseServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class LoginUserCommand implements Parcelable {
public class LoginUserCommand extends LoginBaseServiceCommand implements INewFields{

    @SerializedName(USERNAME)
    public String username;

    @SerializedName(PASSWORD)
    public String password;

    public LoginUserCommand(String deviceToken, String username, String password) {
        super(deviceToken);
        this.username = username;
        this.password = password;
    }

    public LoginUserCommand(CreateAccountCommand createAccountCommand){
        this(createAccountCommand.getPushToken(),
                    createAccountCommand.getMail(), createAccountCommand.getPass());
    }

    protected LoginUserCommand(Parcel in) {
        super(in);
        username = in.readString();
        password = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(username);
        dest.writeString(password);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginUserCommand> CREATOR = new Creator<LoginUserCommand>() {
        @Override
        public LoginUserCommand createFromParcel(Parcel in) {
            return new LoginUserCommand(in);
        }

        @Override
        public LoginUserCommand[] newArray(int size) {
            return new LoginUserCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.loginUser(this, receiver, requestCode);
    }

    public String getPassword() {
        return password;
    }
}
