package com.adyax.srisgp.data.net.response;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class GetHistoryResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public List<HistoryItem> items;

    @SerializedName("has_more")
    public int has_more;

    public boolean hasMore() {
        return has_more == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.items);
        dest.writeInt(this.has_more);
    }

    public GetHistoryResponse() {
    }

    protected GetHistoryResponse(Parcel in) {
        this.items = in.createTypedArrayList(HistoryItem.CREATOR);
        this.has_more = in.readInt();
    }

    public static final Parcelable.Creator<GetHistoryResponse> CREATOR = new Parcelable.Creator<GetHistoryResponse>() {
        @Override
        public GetHistoryResponse createFromParcel(Parcel source) {
            return new GetHistoryResponse(source);
        }

        @Override
        public GetHistoryResponse[] newArray(int size) {
            return new GetHistoryResponse[size];
        }
    };

    public void fixAroundMe(Context context) {
        for(HistoryItem item:items){
            item.fixAroundMe(context);
        }
    }
}
