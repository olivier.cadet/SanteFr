package com.adyax.srisgp.data.net.command.tags;

/**
 * Created by SUVOROV on 10/2/16.
 */

public enum Sso_type {
    // sgp_sso_mail need to replace on NULL
    sgp_sso_mail, sgp_sso_google, sgp_sso_twitter, sgp_sso_facebook;
}
