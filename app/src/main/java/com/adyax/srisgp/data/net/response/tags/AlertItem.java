package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.Timestamp;

/**
 * Created by SUVOROV on 9/20/16.
 */
@DatabaseTable(tableName = "alerts")
public class AlertItem implements Parcelable, Comparable<AlertItem>, IGetUrl, INewFields {

    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String BUNDLE_NAME = "AlertItem";

    public static final int ATTENTION = 1;
    public static final int WARNING = 2;
    public static final int URGENT_ALERTS = 3;

    public static final String REVISION = "revision";
    public static final String LEVEL = "level";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String BUTTON_TEXT = "button_text";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String CONTENT_ID = "content_id";
    public static final String CONTENT_URL = "content_url";
    public static final String CONTENT_TITLE = "content_title";
    public static final String INFO_UPDATED = "info_updated";
    public static final String PUSH_READ = "push_read";
    public static final String URGENT_READ = "urgent_read";

    @SerializedName(ID_OTHER)
    @DatabaseField(columnName = ID_DB, uniqueCombo = true)
    public long id;

    @SerializedName(REVISION)
    @DatabaseField(columnName = REVISION)
    public int revision;

    @SerializedName(LEVEL)
    @DatabaseField(columnName = LEVEL)
    public int level;

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(BUTTON_TEXT)
    @DatabaseField(columnName = BUTTON_TEXT)
    public String button_text;

    @SerializedName(START_DATE)
    @DatabaseField(columnName = START_DATE)
    public Timestamp start_date;

    @SerializedName(END_DATE)
    @DatabaseField(columnName = END_DATE)
    public Timestamp end_date;

    @SerializedName(CONTENT_ID)
    @DatabaseField(columnName = CONTENT_ID)
    public long content_id;

    @SerializedName(CONTENT_URL)
    @DatabaseField(columnName = CONTENT_URL)
    public String content_url;

    @SerializedName(CONTENT_TITLE)
    @DatabaseField(columnName = CONTENT_TITLE)
    public String content_title;

    // TODO date maybe
    @SerializedName(INFO_UPDATED)
    @DatabaseField(columnName = INFO_UPDATED)
    public Timestamp info_updated;

    @DatabaseField(columnName = PUSH_READ)
    public boolean push_read;

    @DatabaseField(columnName = URGENT_READ)
    public boolean urgent_read;

    // constructor only for push notification
    public AlertItem(String content_url) {
        this.content_url = content_url;
    }

    public AlertItem() {

    }

    protected AlertItem(Parcel in) {
        id = in.readLong();
        revision = in.readInt();
        level = in.readInt();
        title = in.readString();
        description = in.readString();
        button_text = in.readString();
        start_date = new Timestamp(in.readLong());
        end_date = new Timestamp(in.readLong());
        content_id = in.readLong();
        content_url = in.readString();
        content_title = in.readString();
        info_updated = new Timestamp(in.readLong());
        push_read = in.readInt() != 0;
        urgent_read = in.readInt() != 0;
    }

    public static final Creator<AlertItem> CREATOR = new Creator<AlertItem>() {
        @Override
        public AlertItem createFromParcel(Parcel in) {
            return new AlertItem(in);
        }

        @Override
        public AlertItem[] newArray(int size) {
            return new AlertItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeInt(revision);
        parcel.writeInt(level);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(button_text);
        if (start_date != null)
            parcel.writeLong(start_date.getTime());
        else
            parcel.writeLong(0);
        if (end_date != null)
            parcel.writeLong(end_date.getTime());
        else
            parcel.writeLong(0);
        parcel.writeLong(content_id);
        parcel.writeString(content_url);
        parcel.writeString(content_title);
        if (info_updated != null)
            parcel.writeLong(info_updated.getTime());
        else
            parcel.writeLong(0);
        parcel.writeInt(push_read ? 1 : 0);
        parcel.writeInt(urgent_read ? 1 : 0);
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return null;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public NodeType getNodeType() {
        // TODO need to find solution!!!
        return NodeType.UNKNOWN;
    }

    public String getBottonText() {
        return button_text;
    }

    public int getColor() {
        return isRedAlert() ? R.color.red : R.color.orange;
    }

    public boolean isRedAlert() {
        return level > ATTENTION;
    }

    public boolean isSoundAlert() {
//        return level>ATTENTION;
        return true;
    }

    public long getId() {
        return id;
    }

    public boolean isContent() {
        return button_text != null && content_url != null;
    }

    public String getContentUrl() {
        return content_url;
    }

    public long getDate() {
        return start_date.getTime();
    }

    public int getIdForNotify() {
        return new Long(getId() & 0xffffffff).intValue();
    }

    public void setPushRead() {
        this.push_read = true;
    }

    public boolean isPushRead() {
        return push_read;
    }

    public boolean isUrgent_read() {
        return urgent_read;
    }

    public void setUrgentRead() {
        this.urgent_read = true;
    }

    @Override
    public int compareTo(AlertItem alertItem) {
        if (level != alertItem.level) {
            return alertItem.level > level ? 1 : -1;
        }
        if (start_date != null && alertItem.start_date != null && !start_date.equals(alertItem.start_date)) {
            return alertItem.start_date.compareTo(start_date);
        }
        if (info_updated != null && alertItem.info_updated != null && !info_updated.equals(alertItem.info_updated)) {
            return alertItem.info_updated.compareTo(info_updated);
        }
        return alertItem.id > id ? 1 : -1;
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getCardLink() {
        return getContentUrl();
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return -1;
    }

    @Override
    public IconType getIconType() {
        return IconType.UNKNOWN;
    }

    @Override
    public String getPhone() {
        return null;
    }

    @Override
    public String getApple() {
        return null;
    }

    @Override
    public String getGoogle() {
        return null;
    }
}
