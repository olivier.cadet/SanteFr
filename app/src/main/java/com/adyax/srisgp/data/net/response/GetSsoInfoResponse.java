package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kirill on 15.11.16.
 */

public class GetSsoInfoResponse extends BaseResponse implements Parcelable, INewFields{

    @SerializedName(ID_OTHER)
    private String id;

    @SerializedName("displayName")
    private String displayName;

    @SerializedName("gender")
    private String gender;

    private String email;
    private String token;

    public GetSsoInfoResponse() {
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getToken() {
        return token;
    }

    public void addCredentials(String email, String token) {
        this.email = email;
        this.token = token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.displayName);
        dest.writeString(this.gender);
        dest.writeString(this.email);
        dest.writeString(this.token);
    }

    protected GetSsoInfoResponse(Parcel in) {
        this.id = in.readString();
        this.displayName = in.readString();
        this.gender = in.readString();
        this.email = in.readString();
        this.token = in.readString();
    }

    public static final Creator<GetSsoInfoResponse> CREATOR = new Creator<GetSsoInfoResponse>() {
        @Override
        public GetSsoInfoResponse createFromParcel(Parcel source) {
            return new GetSsoInfoResponse(source);
        }

        @Override
        public GetSsoInfoResponse[] newArray(int size) {
            return new GetSsoInfoResponse[size];
        }
    };
}
