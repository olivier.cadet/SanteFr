package com.adyax.srisgp.data.net.command.tags;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/28/16.
 */

public enum SearchType {
//    info, find, around, news;
    @SerializedName("info")
    info,
    @SerializedName("find")
    find,
    @SerializedName("around")
    around,
    @SerializedName("news")
    news,
    unknown;

    public boolean searchAutomatic() {
        switch (this) {
            case around: {
                return false;
            }
            default:
                return true;
        }
    }

}