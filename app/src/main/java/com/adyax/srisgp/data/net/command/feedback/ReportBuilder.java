package com.adyax.srisgp.data.net.command.feedback;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 2/23/17.
 */

public class ReportBuilder implements Parcelable {

    public static final String REPORT_FORM_VALUE_BUILDER = "ReportFormValue.Builder";


    public static final String FIELD_ERROR_INFO = "field_error_info";
    public static final String FIELD_PROPOSED_INFO = "field_proposed_info";
    public static final String FIELD_URL = "field_url";
    public static final String FIELD_EMAIL = "field_email";
    public static final String FIELD_CONCERNED_PARTY = "field_concerned_party";
    public static final String FIELD_ERROR_ATTENTION_REASON = "field_error_attention_reason";
    public static final String FIELD_NOM = "field_nom";
    public static final String FIELD_PRENOM = "field_prenom";
    public static final String FIELD_PROFESSION_OF_REPORTER = "field_profession_of_reporter";
    public static final String FIELD_TELEPHONE_FIXE = "field_telephone_fixe";
    public static final String FIELD_IDENTIFIANT_RPPS = "field_identifiant_rpps";

    private Map<String, String> map=new HashMap<>();
    private String fromName;

    public ReportBuilder(){

    }

    protected ReportBuilder(Parcel in) {
        int size = in.readInt();
        for(int i = 0; i < size; i++){
            String key = in.readString();
            String value = in.readString();
            map.put(key,value);
        }
    }

    public static final Creator<ReportBuilder> CREATOR = new Creator<ReportBuilder>() {
        @Override
        public ReportBuilder createFromParcel(Parcel in) {
            return new ReportBuilder(in);
        }

        @Override
        public ReportBuilder[] newArray(int size) {
            return new ReportBuilder[size];
        }
    };

    public Parcelable build(){
        final String fromName = getFromName();
        if(TextUtils.equals(fromName, InapLinkHelper.FIND_CONTENT_ERROR_FORM_NAME)) {
            if(map.containsKey(ReportBuilder.FIELD_ERROR_ATTENTION_REASON)){
                return new ReportFindFormValue(map);
            }else {
                return new ReportFindFicheFormValue(map);
            }
        }else{
            return new ReportInfoFormValue(map);
        }
    }

    public ReportBuilder set(String key, String value) {
        map.put(key, value);
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags){
        out.writeInt(map.size());
        for(Map.Entry<String,String> entry : map.entrySet()){
            out.writeString(entry.getKey());
            out.writeString(entry.getValue());
        }
    }

    public String getFromName() {
        return fromName;
    }

    public ReportBuilder setFromName(String fromName) {
        this.fromName = fromName;
        return this;
    }
}