package com.adyax.srisgp.data.net.response.tags.xiti;

import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 10/25/16.
 */

public enum ClickType {
    clic_contenu_consultation,
    clic_homepage_en_savoir_plus,
    clic_homepage_alerts,
    clic_contenu_ajout_aux_favoris,
    clic_contenu_retrait_des_favoris,
    clic_homepage_acces_rapides_fermer,
    clic_recherche_appliquer_les_filtres,
    clic_utile_oui,
    clic_utile_non,
    clic_contenu_itineraire,
    clic_notification_par_email_oui,
    clic_notification_par_email_non,
    clic_raporter_une_information_erronee,
    clic_signaler_une_information_erronee,
    clic_recherche,
    clic_recherche_suppression_tout_historique,
    clic_notifications_suppression_centre_interet,
    clic_notifications_pas_interesse,
    clic_signaler_temps_urgence,
    clic_premier_attente_urgences_fort,
    clic_premier_attente_urgences_modere,
    clic_attente_urgence_fort,
    clic_attente_urgence_modere,
    clic_attente_urgence_faible,
    UNKNOWN
}
