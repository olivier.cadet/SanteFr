package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.mvp.notification.StatusNotification;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class GetNotoficationCommand implements Parcelable {
public class GetNotificationCommand extends ServiceCommand {

    public String[] statuses;
    public int page_offset = 0;
    public int items_per_page = 20;
    private int page = 0;

    public GetNotificationCommand(int pageOffset, StatusNotification... statusNotification) {
        this.statuses = new String[statusNotification.length];
        int i = 0;
        for (StatusNotification s : statusNotification) {
            statuses[i++] = s.name();
        }
        this.page_offset = pageOffset;
    }

    public GetNotificationCommand(StatusNotification... statusNotification) {
        this.statuses = new String[statusNotification.length];
        int i = 0;
        for (StatusNotification s : statusNotification) {
            statuses[i++] = s.name();
        }
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getNotification(this, receiver, requestCode);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(this.statuses);
        dest.writeInt(this.page_offset);
        dest.writeInt(this.items_per_page);
        dest.writeInt(this.page);
    }

    protected GetNotificationCommand(Parcel in) {
        this.statuses = in.createStringArray();
        this.page_offset = in.readInt();
        this.items_per_page = in.readInt();
        this.page = in.readInt();
    }

    public static final Creator<GetNotificationCommand> CREATOR = new Creator<GetNotificationCommand>() {
        @Override
        public GetNotificationCommand createFromParcel(Parcel source) {
            return new GetNotificationCommand(source);
        }

        @Override
        public GetNotificationCommand[] newArray(int size) {
            return new GetNotificationCommand[size];
        }
    };
}
