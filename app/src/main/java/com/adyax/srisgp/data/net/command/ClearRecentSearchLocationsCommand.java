package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class ClearRecentSearchLocationsCommand extends ServiceCommand {

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.clearRecentLocations(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ClearRecentSearchLocationsCommand() {
    }

    protected ClearRecentSearchLocationsCommand(Parcel in) {
    }

    public static final Creator<ClearRecentSearchLocationsCommand> CREATOR = new Creator<ClearRecentSearchLocationsCommand>() {
        @Override
        public ClearRecentSearchLocationsCommand createFromParcel(Parcel source) {
            return new ClearRecentSearchLocationsCommand(source);
        }

        @Override
        public ClearRecentSearchLocationsCommand[] newArray(int size) {
            return new ClearRecentSearchLocationsCommand[size];
        }
    };
}
