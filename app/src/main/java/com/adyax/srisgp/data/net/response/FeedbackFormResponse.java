package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anton.kobylianskiy on 11/14/16.
 */

public class FeedbackFormResponse extends BaseResponse implements Parcelable {

    public static final String FIELD_CONCERNED_PARTY = "field_concerned_party";
    public static final String FIELD_ERROR_ATTENTION_REASON = "field_error_attention_reason";
    public static final String FIELD_ERROR_INFO = "field_error_info";
    public static final String FIELD_PROPOSED_INFO = "field_proposed_info";

    @SerializedName("title")
    public String title;
    @SerializedName("button_text")
    public String buttonText;
    @SerializedName(BaseFeedbackCommandCommand.SECRET_FIELD)
    public String secret;
    @SerializedName("fields")
    public List<FeedbackFormField> fields;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.buttonText);
        dest.writeString(this.secret);
        dest.writeList(this.fields);
    }

    public FeedbackFormResponse() {
    }

    protected FeedbackFormResponse(Parcel in) {
        this.title = in.readString();
        this.buttonText = in.readString();
        this.secret = in.readString();
        this.fields = new ArrayList<FeedbackFormField>();
        in.readList(this.fields, FeedbackFormField.class.getClassLoader());
    }

    public static final Creator<FeedbackFormResponse> CREATOR = new Creator<FeedbackFormResponse>() {
        @Override
        public FeedbackFormResponse createFromParcel(Parcel source) {
            return new FeedbackFormResponse(source);
        }

        @Override
        public FeedbackFormResponse[] newArray(int size) {
            return new FeedbackFormResponse[size];
        }
    };

    public Map<String, FeedbackFormField> getFields() {
        Map<String, FeedbackFormField> map=new HashMap<>();
        for(FeedbackFormField f:fields){
            map.put(f.getKey(), f);
        }
        return map;
    }

    public String getSecret() {
        return secret;
    }
}
