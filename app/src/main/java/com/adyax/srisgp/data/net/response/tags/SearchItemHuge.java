package com.adyax.srisgp.data.net.response.tags;

import android.database.Cursor;
import android.text.TextUtils;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.SearchItemType;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.db.IExpandArray;
import com.adyax.srisgp.mvp.around_me.ISearchItem;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;

/**
 * Created by SUVOROV on 9/19/16.
 */
@DatabaseTable(tableName = "advert_search")
public class SearchItemHuge implements IExpandArray, IGetUrl, INewFields, ISearchItem {

    public static final String CONTENT_MAINTENANCE = "content_maintenance";
    public static final int MAINTENANCE_MODE = 1;

    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @SerializedName(CONTENT_MAINTENANCE)
    @DatabaseField(columnName = CONTENT_MAINTENANCE)
    public int content_maintenance;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String NODE_TYPE = "node_type";
    public static final String SUBTITLE = "subtitle";
    public static final String SUBTITLES = "subtitles";
    public static final String DESCRIPTION = "description";
    public static final String ADDRESS = "address";
    public static final String OPEN = "open";
    public static final String HANDICAPPED = "handicapped";
    public static final String DISTANCE = "distance";
    public static final String DATE = "date_tri";
    public static final String DATE_DB = "date";
    public static final String LAT = "lat";
    public static final String LON = "lon";
    public static final String FAVORITE = "favorite";
    public static final String NAME = "name";
    public static final String ICON = "icon";
    public static final String TITLE = "title";
    public static final String ICON_URL = "icon_url";

    public static final String SUBTITLE_SPLIT_SYMBOL = ", ";
    public static final String SPLIT_SYMBOL = "\n";
    public static final String TYPE = "type";
    public static final String DISTANCE_M = "distance_m";
    public static final String ADDITIONAL_LABELS = "additional_labels";
    public static final String WAITING_TIME_ENABLED = "waiting_time_enabled";
    public static final String WAITING_TIME_KNOW_MORE = "waiting_time_know_more";
    public static final String WAITING_TIME_LEVEL_VALUE = "waiting_time_level_value";
    public static final String WAITING_TIME_LEVEL_LABEL = "waiting_time_level_label";

    public static final String REQUEST_TYPE = "request_type";

    public static final String IMAGE_URL = "image";
    public static final String SOURCE_URL = "source_url";

    @SerializedName(ID)
    @DatabaseField(columnName = ID_DB, uniqueCombo = true)
    public long id;

    @SerializedName(NODE_TYPE)
    @DatabaseField(columnName = NODE_TYPE)
    public NodeType node_type;

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(SUBTITLE)
    @DatabaseField(columnName = SUBTITLE)
    public String subtitle;

    @SerializedName(SUBTITLES)
    public String[] subtitles;

    @DatabaseField(columnName = SUBTITLES)
    public String subtitles_array;

    @SerializedName(URL)
    @DatabaseField(columnName = URL_DB)
    public String url;

    public String urlPost;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(ADDRESS)
    public String[] address;

    @DatabaseField(columnName = ADDRESS)
    public String address_array;

    @SerializedName(PHONE_ARRAY)
    public String[] phones;

    @DatabaseField(columnName = PHONE_DB)
    public String phone_array;

    @SerializedName(OPEN)
    @DatabaseField(columnName = OPEN)
    public int open = -1;

    @SerializedName(HANDICAPPED)
    @DatabaseField(columnName = HANDICAPPED)
    public int handicapped = -1;

    // TODO !!! Mistake in docs
    @SerializedName(DISTANCE)
    @DatabaseField(columnName = DISTANCE)
    public String distance;

    // it's distance in metres
    @DatabaseField(columnName = DISTANCE_M)
    public int distance_m;

    @SerializedName(ADDITIONAL_LABELS)
    public String[] additional_labels;

    @DatabaseField(columnName = ADDITIONAL_LABELS)
    public String additional_labels_array;

    // Waiting time enbaled
    @DatabaseField(columnName = WAITING_TIME_ENABLED)
    public int waiting_time_enabled;

    // Get waiting time datas
    @DatabaseField(columnName = WAITING_TIME_LEVEL_LABEL)
    public String waiting_time_level_label;

    // Get waiting time datas
    @DatabaseField(columnName = WAITING_TIME_LEVEL_VALUE)
    public String waiting_time_level_value;

    // Get waiting time datas
    @DatabaseField(columnName = WAITING_TIME_KNOW_MORE)
    public String waiting_time_know_more;

    @SerializedName(APPLE)
    @DatabaseField(columnName = APPLE_DB)
    public String apple;

    @SerializedName(GOOGLE)
    @DatabaseField(columnName = GOOGLE_DB)
    public String google;

    @SerializedName(SOURCE)
    @DatabaseField(columnName = SOURCE_DB)
    public String source;

    @SerializedName(SOURCE_URL)
    @DatabaseField(columnName = SOURCE_URL)
    public String sourceUrl;

    @SerializedName(IMAGE_URL)
    @DatabaseField(columnName = IMAGE_URL)
    public String image;

    @SerializedName(DATE)
    @DatabaseField(columnName = DATE_DB)
    public String date;

    @SerializedName(LAT)
    @DatabaseField(columnName = LAT)
    public double lat;

    @SerializedName(LON)
    @DatabaseField(columnName = LON)
    public double lon;

    @SerializedName(FAVORITE)
    @DatabaseField(columnName = FAVORITE)
    public int favorite;

    @SerializedName(LINK)
    @DatabaseField(columnName = LINK_DB)
    public String link;

    @SerializedName(NAME)
    @DatabaseField(columnName = NAME)
    public String name;

    @SerializedName(ICON)
    @DatabaseField(columnName = ICON)
    public IconType icon;

    @SerializedName(TYPE)
    @DatabaseField(columnName = TYPE)
    public SearchItemType type;

    @DatabaseField(columnName = REQUEST_TYPE, uniqueCombo = true)
    public SearchType requestType = SearchType.unknown;

    @SerializedName(ICON_URL)
    @DatabaseField(columnName = ICON_URL)
    public String icon_url;

    private int position;
    private int active;

    // do not delete
    public SearchItemHuge() {
    }

    public SearchItemHuge(int position) {
        this.position = position;
    }

    @Override
    public NodeType getNodeType() {
        if (node_type == null) {
            node_type = NodeType.UNKNOWN;
        }
        return node_type;
    }

    public IconType getIcon() {
        if (icon == null) {
            icon = IconType.UNKNOWN;
        }
        return icon;
    }

    public boolean isFavorite() {
        return favorite == 1;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public IGetUrl getItem() {
        return this;
    }

    @Override
    public LatLng getLatLng() {
        return new LatLng(getLat(), getLon());
    }

    @Override
    public boolean isGroup() {
        return false;
    }

    @Override
    public String getGroupCount() {
        return "";
    }

    @Override
    public boolean contains(long id) {
        return this.id == id;
    }

    @Override
    public boolean isEquals(Marker marker) {
        return false;
    }

    public long getId() {
        return id;
    }

    public String getContentUrl() {
        return url;
    }

    @Override
    public String getTitle() {
        return title != null ? title : name;
    }

    @Override
    public String getSourceOrPropose() {
        return source;
    }

//    @Override
//    public String getCategory() {
//        return category!=null? category[0]: null;
//    }
//
//    @Override
//    public String getCity() {
//        return city;
//    }

    @Override
    public void collapse() {
        if (subtitles != null) {
            subtitles_array = TextUtils.join(SUBTITLE_SPLIT_SYMBOL, subtitles);
        }
        if (additional_labels != null) {
            additional_labels_array = TextUtils.join(SPLIT_SYMBOL, additional_labels);
        }
        StringBuilder s = new StringBuilder();
        if (address != null) {
            for (String ss : address) {
                if (s.length() > 0) {
                    s.append(SPLIT_SYMBOL);
                }
                s.append(ss);
            }
            address_array = s.toString();
        }
        s = new StringBuilder();
        if (phones != null) {
            for (String ss : phones) {
                if (s.length() > 0) {
                    s.append(SPLIT_SYMBOL);
                }
                s.append(ss);
            }
            phone_array = s.toString();
        }
//        s = new StringBuilder();
//        if (category != null) {
//            for (String ss : category) {
//                if (s.length() > 0) {
//                    s.append(SPLIT_SYMBOL);
//                }
//                s.append(ss);
//            }
//            category_array = s.toString();
//        }
        if (xiti != null) {
            xiti_str = new Gson().toJson(xiti);
        }
    }

    @Override
    public void expand(Cursor cursor) {
        if (subtitles_array != null) {
            subtitles = subtitles_array.split(SUBTITLE_SPLIT_SYMBOL);
        }
        if (additional_labels_array != null && additional_labels_array.length() > 0) {
            additional_labels = additional_labels_array.split(SPLIT_SYMBOL);
        }
        if (address_array != null) {
            address = address_array.split(SPLIT_SYMBOL);
        }
        if (phone_array != null) {
            phones = phone_array.split(SPLIT_SYMBOL);
        }
//        if (category_array != null) {
//            category = category_array.split(SPLIT_SYMBOL);
//        }
        if (xiti_str != null) {
            xiti = new Gson().fromJson(xiti_str, Xiti.class);
        }
    }

    public String getAddress() {
        return address_array;
    }

    public String getSubtitles() {
        return subtitles_array;
    }

    public String[] getAdditionalLabels() {
        return additional_labels;
    }

    public String getPhones() {
        return phone_array;
    }

    @Override
    public int getPosition() {
        return position;
    }

    public void setRequestType(SearchType type) {
        this.requestType = type;
        if (distance != null) {
            try {
                if (distance.contains("km")) {
                    distance_m = Integer.parseInt(distance.substring(0, distance.length() - 3)) * 1000;
                } else if (distance.contains("m")) {
                    distance_m = Integer.parseInt(distance.substring(0, distance.length() - 2));
                }
            } catch (Exception e) {

            }
        }
    }

    public String[] getPhones2() {
        return phones;
    }

    public boolean isOpen() {
        return open == 1 || open == 0;
    }

    public boolean isTemporarilyClosed() {
        return open == 0;
    }

    public boolean isHandicapped() {
        return handicapped == 1 || handicapped == 0;
    }

    public boolean isWaitingEnabled() {
        return waiting_time_enabled == 1;
    }

    public String getWaitingTimelabel() {
        return waiting_time_level_label;
    }


    public String getWaitingTimelabelColor() {
        return waiting_time_level_value;
    }

    public String getWaitingTimeKnowMore() {
        return waiting_time_know_more;
    }

    public boolean isHandicappedCrossed() {
        return handicapped == 0;
    }

    public String getDistance() {
        return distance;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String getCardLink() {
        switch (getNodeType()) {
            default:
            case VIDEO:
            case FICHES_INFOS:
                return getContentUrl();
            case LIENS_EXTERNES:
                return getLink();
        }
    }

    @Override
    public String getPostData() {
        return this.urlPost;
    }

    @Override
    public long getNid() {
        return getId();
    }

    @Override
    public String getPhone() {
        return phones != null ? phones[0] : null;
    }

    @Override
    public String getSubtitle() {
        return (subtitles != null && subtitles.length > 0) ? subtitles[0] : null;
    }

    @Override
    public IconType getIconType() {
        if (icon == null) {
            return IconType.UNKNOWN;
        }
        return icon;
    }

    public boolean isContentMaintenanceMode() {
        return content_maintenance == MAINTENANCE_MODE;
    }

    @Override
    public String toString() {
//        return CONTENT_MAINTENANCE + "= " + String.valueOf(content_maintenance);
        return address_array;
    }

    @Override
    public String getApple() {
        return apple;
    }

    @Override
    public String getGoogle() {
        return google;
    }

    public boolean isActive() {
        return active != 0;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public boolean isEquals(SearchItemHuge item) {
        if (item != null) {
            return TextUtils.equals(name, item.name) && TextUtils.equals(title, item.title) &&
                    TextUtils.equals(address_array, item.address_array) && lat == item.lat && lon == item.lon;
        }
        return false;
    }

    public String getIcon_url() {
        return icon_url;
    }

}
