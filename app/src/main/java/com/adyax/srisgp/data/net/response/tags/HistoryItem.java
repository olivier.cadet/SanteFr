package com.adyax.srisgp.data.net.response.tags;

import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.IntDef;
import android.text.TextUtils;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.response.IconType;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.xiti.Xiti;
import com.adyax.srisgp.db.IExpandArray;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Calendar;

/**
 * Created by SUVOROV on 9/19/16.
 */
@DatabaseTable(tableName = "history")
public class HistoryItem implements Parcelable, IGetUrl, IExpandArray, INewFields {

    public static final String ANONYMOUS = "anonymous";
    public static final String SRIS_SEARCH_AROUND_ME = "SRIS_SEARCH_AROUND_ME";
    @SerializedName(Xiti.XITI)
    public Xiti xiti;
    @DatabaseField(columnName = Xiti.XITI)
    public String xiti_str;

    @Override
    public Xiti getXiti() {
        return xiti;
    }

    public static final String SID = "sid";
    public static final String LAST_VISIT = "last_visit";
    public static final String TITLE = "title";
    public static final String NODE_TYPE = "node_type";
    public static final String DESCRIPTION = "description";
    public static final String STATUS = "type";
    public static final String APP_STORE_LINK = "app_store_link";
    public static final String PLAY_STORE_LINK = "play_store_link";
    public static final String ICON = "icon";
    public static final String FAVORITE = "favorite";

    @SerializedName(NID)
    @DatabaseField(columnName = NID, uniqueCombo = true)
    public long nid;

    @SerializedName(SID)
    @DatabaseField(columnName = SID, uniqueCombo = true)
    public long sid;

    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    public String title;

    @SerializedName(DESCRIPTION)
    @DatabaseField(columnName = DESCRIPTION)
    public String description;

    @SerializedName(URL)
    @DatabaseField(columnName = URL_DB)
    public String url;

    @SerializedName(LINK)
    @DatabaseField(columnName = LINK_DB)
    public String link;

    @SerializedName(PHONE)
    @DatabaseField(columnName = PHONE_DB)
    public String phone;

    @SerializedName(NODE_TYPE)
    @DatabaseField(columnName = NODE_TYPE)
    public NodeType node_type;

    // TODO Date
    @SerializedName(LAST_VISIT)
    @DatabaseField(columnName = LAST_VISIT)
    public long last_visit;

    @SerializedName(SOURCE)
    @DatabaseField(columnName = SOURCE_DB)
    public String source;

    @SerializedName(APP_STORE_LINK)
    @DatabaseField(columnName = APP_STORE_LINK)
    public String appStoreLink;

    @SerializedName(PLAY_STORE_LINK)
    @DatabaseField(columnName = PLAY_STORE_LINK)
    public String playStoreLink;

    @SerializedName(FAVORITE)
    @DatabaseField(columnName = FAVORITE)
    public int favorite;

    public static final int TYPE_AUTH_HISTORY=0;
    public static final int TYPE_ANONIMUS_HISTORY=1;
    public static final int TYPE_SEARCH=2;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void fixAroundMe(Context context) {
        if(title!=null&& title.endsWith(SRIS_SEARCH_AROUND_ME)){
            title= title.replace(SRIS_SEARCH_AROUND_ME, context.getString(R.string.search_around_me));
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({TYPE_AUTH_HISTORY, TYPE_ANONIMUS_HISTORY, TYPE_SEARCH})
    public @interface TypeHistoryItem {};

    @DatabaseField(columnName = ANONYMOUS)
    public @TypeHistoryItem int anonymous;

    @SerializedName(ICON)
    @DatabaseField(columnName = ICON)
    public IconType icon=IconType.UNKNOWN;

    @SerializedName(SearchItemHuge.ICON_URL)
    @DatabaseField(columnName = SearchItemHuge.ICON_URL)
    public String icon_url;

    public HistoryItem(Context context, SearchCommand searchCommand, SearchResponseHuge response) {
        title=searchCommand.getSearchText();
        final String where = searchCommand.getWhere(context);
        if(where!=null&& where.length()>0){
            title+=", " +where;
        }
        setAnonymous(TYPE_SEARCH);
        updateTime();
        if (response.items != null && !response.items.isEmpty()) {
            nid = response.items.get(0).getId();
        } else {
            nid = searchCommand.hashCode();//new Random().nextLong();
        }
    }

    @Override
    public int getPosition() {
        return -1;
    }

    @Override
    public String getCardLink() {
        switch (getNodeType()) {
            case VIDEO:
            case FICHES_INFOS:
                return url;
            case LIENS_EXTERNES:
                if (!TextUtils.isEmpty(link)) {
                    return link;
                }
        }
        return url;
    }

    @Override
    public String getPostData() {
        return null;
    }

    @Override
    public long getNid() {
        return nid;
    }

    public long getSid() {
        return sid;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String getSourceOrPropose() {
        return source;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    public NodeType getNodeType() {
        if (node_type == null) {
            return NodeType.UNKNOWN;
        }
        return node_type;
    }

    public boolean isFavorite() {
        return favorite == 1;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite ? 1 : 0;
    }

    public long getLastVisit() {
        return last_visit;
    }

    public String getPropose() {
        return source;
    }

    public String getAppStoreLink() {
        return appStoreLink;
    }

    public String getPlayStoreLink() {
        return playStoreLink;
    }

    public String getContentUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.nid);
        dest.writeLong(this.sid);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.url);
        dest.writeString(this.link);
        dest.writeString(this.phone);
        dest.writeInt(this.node_type == null ? -1 : this.node_type.ordinal());
        dest.writeLong(this.last_visit);
        dest.writeString(this.source);
        dest.writeString(this.appStoreLink);
        dest.writeString(this.playStoreLink);
        if(this.xiti==null){
            this.xiti=new Xiti();
        }
        dest.writeParcelable(this.xiti, flags);
        dest.writeString(this.icon.name());
        dest.writeInt(this.favorite);
        dest.writeString(this.icon_url);
    }

    public HistoryItem() {
    }

    public HistoryItem(Parcel in) {
        this.nid = in.readLong();
        this.sid = in.readLong();
        this.title = in.readString();
        this.description = in.readString();
        this.url = in.readString();
        this.link = in.readString();
        this.phone = in.readString();
        final int tmpNode_type = in.readInt();
        this.node_type = tmpNode_type == -1 ? null : NodeType.values()[tmpNode_type];
        this.last_visit = in.readLong();
        this.source = in.readString();
        this.appStoreLink = in.readString();
        this.playStoreLink = in.readString();
        this.xiti=in.readParcelable(Xiti.class.getClassLoader());
        this.icon = IconType.valueOf(in.readString());
        this.favorite = in.readInt();
        this.icon_url = in.readString();
    }

    public static final Creator<HistoryItem> CREATOR = new Creator<HistoryItem>() {
        @Override
        public HistoryItem createFromParcel(Parcel source) {
            return new HistoryItem(source);
        }

        @Override
        public HistoryItem[] newArray(int size) {
            return new HistoryItem[size];
        }
    };

    @Override
    public void collapse() {
        if(xiti!=null){
            xiti_str=new Gson().toJson(xiti);
        }
    }

    @Override
    public void expand(Cursor cursor) {
        if(xiti_str!=null){
            xiti=new Gson().fromJson(xiti_str, Xiti.class);
        }
    }

    public IconType getIcon() {
        if (icon == null) {
            icon = IconType.UNKNOWN;
        }
        return icon;
    }

    public void setAnonymous(@TypeHistoryItem int anonymous) {
        this.anonymous = anonymous;
    }

    public void updateTime() {
//        sid = Calendar.getInstance().getTime().getTime();
//        last_visit = sid / 1000;
        last_visit= getTime();
//        sid = node_type!=null? node_type.hashCode(): last_visit;
        sid=hashCode();
    }

    public static long getTime() {
        return Calendar.getInstance().getTime().getTime()/1000;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        result = prime * result + ((link == null) ? 0 : link.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        return result;
    }

    @Override
    public IconType getIconType() {
        return icon;
    }

    @Override
    public String getApple() {
        return appStoreLink;
    }

    @Override
    public String getGoogle() {
        return playStoreLink;
    }

    public String getIcon_url() {
        return icon_url;
    }
}
