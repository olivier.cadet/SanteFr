package com.adyax.srisgp.data.net;

import com.adyax.srisgp.BuildConfig;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class UrlExtras {

//    public static final String ABOUT_PAGE = "/ios/a-propos";
//    public static final String HOW_IT_WORKS = "/ios/comment-ça-marche";
//    public static final String PROTECTION = "/ios/la-protection-de-vos-donnees";
    public static final String ABOUT_PAGE = "/android/a-propos";
    public static final String HOW_IT_WORKS = "/android/comment-ça-marche";
    public static final String PROTECTION = "/android/protection-des-donnees";
    public static final String CHARTER = "/charte-editoriale";
    public static final String INFORMATION = "/android/mentions-legales";
    public static final String INFORMATION_LEGALES = "/android/informations-legales";
    public static final String CGU_LINK = "/conditions-generales-dutilisation";
    public static final String CONFIDENTIALITY_POLICY_LINK = "/privacy-policy";


    public static final String ABOUT_HOME_PAGE = "/propos";
    public static final String PRIVACY_POLICY = "/privacy-policy";
    public static final String PAGE_URGENCES = "/restapi/node/page_urgences";
    public static final String API_WIDGET_FORM = "/restapi/widget/form";

    public static final String FACEBOOK_LINK = "https://www.facebook.com/MinSoliSante";
    public static final String TWITTER_LINK = "https://twitter.com/MinSoliSante";


    public static String API_URL = getBaseURL();

    public static String getBaseURL() {
        return BuildConfig.API_SERVER_URL;
    }

    public interface User {
        String SESSION_NAME = "/rest/user";
        String REGISTRATION = SESSION_NAME + "/registration";
        String LOGIN = SESSION_NAME + "/authenticate";
        String RESET = SESSION_NAME + "/reset_password";
        String CITIES = SESSION_NAME + "/cities";
        String ALERTS = SESSION_NAME + "/get_alerts";
        String PING = SESSION_NAME + "/ping";
        String LOGIN_SSO = SESSION_NAME + "/authenticate_sso";
        String SSO_INFO = SESSION_NAME + "/get_ssoinfo";
        String SET_PUSH_CREDENTIAL = SESSION_NAME + "/onboarding_finished";
        String DEL_PUSH_CREDENTIAL = SESSION_NAME + "/onboarding_reset";
        String CHECK_CGU_VALIDATION = "/get_user_cgu_validation_json/{user_uid}";
        String SEND_CGU_VALIDATION = "/send_user_cgu_validation/{user_uid}";
    }

    public interface RestUser {
        String SESSION_NAME = "/restuser/user";
        String CANCEL_ACCOUNT = SESSION_NAME + "/cancel_account";
        String PING = SESSION_NAME + "/ping_user";
        String LOGOUT = SESSION_NAME + "/logout";
        String GET_HISTORY = SESSION_NAME + "/get_history";
        String CLEAR_HISTORY = SESSION_NAME + "/clear_history";
        String GET_NOTIFICATION = SESSION_NAME + "/get_notifications";
        String GET_FAVORITES = SESSION_NAME + "/get_favorites";
        String UPDATE_NOTIFICATIONS = SESSION_NAME + "/update_notifications";
        String UPDATE_CREDENTIALS = SESSION_NAME + "/update_credentials";
        String UPDATE_PERSONAL_INFORMATION = SESSION_NAME + "/updateinfo";
        String ADD_REMOVE_FAVORITE = SESSION_NAME + "/favorite_interact";
        String SEND_EMAIL_CONFIRATION = SESSION_NAME + "/send_email_confirmation";
        String UPDATE_PERSONALIZE = SESSION_NAME + "/update_personalize";
        String CHECK_PASSWORD = SESSION_NAME + "/check_password";
    }

    public interface Search {
        String SESSION_NAME = "/rest/search";
        String GET_POPULAR = SESSION_NAME + "/get_popular";
        String GET_AUTOCOMPLETE_SEARCH_PHRASES = SESSION_NAME + "/get_autocompleted_search_phrases";
        String GET_AUTOCOMPLETE_SEARCH_LOCATIONS = SESSION_NAME + "/get_autocompleted_search_locations";
    }

    public interface RestUserSearch {
        String SESSION_NAME = "/restuser/search";
        String GET_RECENT_SEARCH_PHRASES = SESSION_NAME + "/get_recent_search_phrases";
        String GET_RECENT_SEARCH_LOCATIONS = SESSION_NAME + "/get_recent_search_locations";
        String SEARCH = SESSION_NAME + "/search";
        String CLEAR_RECENT_SEARCH_PHRASES = SESSION_NAME + "/clear_recent_search_phrases";
        String CLEAR_RECENT_SEARCH_LOCATIONS = SESSION_NAME + "/clear_recent_search_locations";
        String REMOVE_RECENT_SEARCH_ITEM = SESSION_NAME + "/remove_recent_search_items";
        String ADD_TO_HISTORY = SESSION_NAME + "/add_to_history";
        String SORT_RELEVANCE = "search_api_relevance";
        String SORT_DATE = "date_common_for_sort";
    }

    public interface RestHomePage {
        String SESSION_NAME = "/rest/homepage";
        String GET_QUICK_CARDS_ANONYMOUS = SESSION_NAME + "/get_quick_cards";
    }

    public interface RestUserHomePage {
        String SESSION_NAME = "/restuser/homepage";
        String GET_QUICK_CARDS_AUTHENTICATED = SESSION_NAME + "/get_quick_cards";
    }

    public interface RestUserTaxonomyVocabulary {
        String SESSION_NAME = "/rest/taxonomy_vocabulary";
        String TERMS= SESSION_NAME + "/terms";
    }

    public interface RestUserNode {
        String SESSION_NAME = "/restuser/node";
        String ANCHORS = SESSION_NAME + "/get_anchors ";
        String NODE_LINK_CLICK = SESSION_NAME + "/node_link_clicked ";
    }

    public interface RestFeedback {
        String SESSION_NAME = "/rest/feedback";
        String GET_FORM = SESSION_NAME + "/get_form";
        String SUBMIT_FORM = SESSION_NAME + "/submit_form";
    }

    public interface RestUserFeedback {
        String SESSION_NAME = "/restuser/feedback";
        String SUBMIT_FORM = SESSION_NAME + "/submit_form";
    }

    public interface System {
        String SESSION_NAME = "/rest/system";
        String GET_CONFIGURATION = SESSION_NAME + "/get_configuration";
    }

    public interface Extra {
        String SGP_LOCATION_COMPLETE = "/sgplocation-complete";
    }

    public interface RestEmergency {
        String SESSION_NAME = "/restuser/emergency";
        String EMERGENCY = SESSION_NAME + "/submit_waiting_time";
    }

    public interface ExternalSearch {
        String MEDICS = "http://base-donnees-publique.medicaments.gouv.fr/";
    }

    public interface RestNode {
        String SESSION_NAME = "/rest/node";
        String URGENCE = SESSION_NAME + "/page_urgences";
    }


}//https://dev.sris-gp.adyax-dev.com/restuser/emergency/submit_waiting_time
