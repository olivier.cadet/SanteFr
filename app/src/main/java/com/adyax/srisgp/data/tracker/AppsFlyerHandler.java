package com.adyax.srisgp.data.tracker;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;

import java.util.Map;

public class AppsFlyerHandler {
    //private static final String AF_DEV_KEY = "rtAjm3LTb5UbuqHAVHA3in";   // key for stage version
    private static final String AF_DEV_KEY = "7wGTgQuHpsrtpBzYVtu6Qj";
    private String TAG = this.getClass().getSimpleName();

    public AppsFlyerHandler(Application application) {
        // AppFlyer Init
        AppsFlyerConversionListener conversionDataListener = new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
                for (String attrName : conversionData.keySet()) {
                    Log.d(TAG, "attribute: " + attrName + " = " + conversionData.get(attrName));
                }
            }
            @Override
            public void onInstallConversionFailure(String errorMessage) {
                Log.d(TAG, "error getting conversion data: " + errorMessage);
            }
            @Override
            public void onAppOpenAttribution(Map<String, String> conversionData) {

            }
            @Override
            public void onAttributionFailure(String errorMessage) {
                Log.d(TAG, "error onAttributionFailure : " + errorMessage);
            }
        };
        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionDataListener, application.getApplicationContext());
        AppsFlyerLib.getInstance().startTracking(application);
    }
}
