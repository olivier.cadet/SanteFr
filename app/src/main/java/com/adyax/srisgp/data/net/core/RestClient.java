package com.adyax.srisgp.data.net.core;

import com.adyax.srisgp.data.net.command.AddContactFormFeedbackCommand;
import com.adyax.srisgp.data.net.command.AddInHistoryCommand;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.AddToHistoryCommand;
import com.adyax.srisgp.data.net.command.AlertsCommand;
import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.command.CheckPasswordCommand;
import com.adyax.srisgp.data.net.command.CitiesCommand;
import com.adyax.srisgp.data.net.command.ClearHistoryCommand;
import com.adyax.srisgp.data.net.command.DelPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.command.GetAnchorsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchLocationsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchPhrasesCommand;
import com.adyax.srisgp.data.net.command.GetFavoritesCommand;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.GetHistoryCommand;
import com.adyax.srisgp.data.net.command.GetConfigurationCommand;
import com.adyax.srisgp.data.net.command.GetNotificationCommand;
import com.adyax.srisgp.data.net.command.GetPopularCommand;
import com.adyax.srisgp.data.net.command.GetQuickCardsCommand;
import com.adyax.srisgp.data.net.command.GetSsoInfoCommand;
import com.adyax.srisgp.data.net.command.LoginUserCommand;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.LoginUserSsoCommand;
import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.command.SetPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.ResetCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.TaxonomyVocabularyTermsCommand;
import com.adyax.srisgp.data.net.command.UpdateNotificationStatusCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalInformationCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalizeCommand;
import com.adyax.srisgp.data.net.response.AddToHistoryResponse;
import com.adyax.srisgp.data.net.response.AlertResponse;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.BaseResponse;
import com.adyax.srisgp.data.net.response.CheckCguResponse;
import com.adyax.srisgp.data.net.response.CitiesResponse;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.FavoritesResponse;
import com.adyax.srisgp.data.net.response.FeedbackFormResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchLocationsResponse;
import com.adyax.srisgp.data.net.response.GetAutoCompleteSearchPhrasesResponse;
import com.adyax.srisgp.data.net.response.GetHistoryResponse;
import com.adyax.srisgp.data.net.response.GetQuickCardsAnonymousResponse;
import com.adyax.srisgp.data.net.response.GetSsoInfoResponse;
import com.adyax.srisgp.data.net.response.NonStandardResponse;
import com.adyax.srisgp.data.net.response.NotificationResponse;
import com.adyax.srisgp.data.net.response.PingNonAuthResponse;
import com.adyax.srisgp.data.net.response.PopularResponse;
import com.adyax.srisgp.data.net.response.RecentLocationsResponse;
import com.adyax.srisgp.data.net.response.RegisterUserResponse;
import com.adyax.srisgp.data.net.response.SearchResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.TaxonomyVocabularyTermsResponse;
import com.adyax.srisgp.data.net.response.UrgenceResponse;
import com.adyax.srisgp.data.net.response.UserResponse;

import java.util.Map;

import rx.Observable;
import timber.log.Timber;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class RestClient {

    private RetrofitService retrofitService;

    public RestClient(RetrofitService retrofitService) {
        this.retrofitService = retrofitService;
    }

    public BaseCall<RegisterUserResponse> register(CreateAccountCommand createAccountCommand) {
        return new BaseCall<>(retrofitService.register(createAccountCommand));
    }

    public BaseCall<RegisterUserResponse> login(LoginUserCommand loginUserCommand) {
        return new BaseCall<>(retrofitService.login(loginUserCommand));
    }

    public BaseCall<RegisterUserResponse> loginSso(LoginUserSsoCommand command) {
        return new BaseCall<>(retrofitService.loginSso(command));
    }

    public BaseCall<CheckCguResponse> checkCguReponseBaseCall(String user_uid) {
        return new BaseCall<>(retrofitService.checkCGUValidation(user_uid));
    }

    public BaseCall<CheckCguResponse> sendCguReponseBaseCall(String user_uid) {
        return new BaseCall<>(retrofitService.sendCGUValidation(user_uid));
    }

    public RetrofitService getRetrofitService() {
        return retrofitService;
    }

    public BaseCall<UserResponse> ping_auth() {
        return new BaseCall<>(retrofitService.ping_auth());
    }

    public BaseCall<NonStandardResponse> logout() {
        return new BaseCall<>(retrofitService.logout());
    }

    public BaseCall<NonStandardResponse> reset(ResetCommand resetCommand) {
        return new BaseCall<>(retrofitService.reset(resetCommand));
    }

    public BaseCall<CitiesResponse> cities(CitiesCommand citiesCommand) {
        return new BaseCall<>(retrofitService.cities(citiesCommand));
    }

    public BaseCall<GetHistoryResponse> getHistory(GetHistoryCommand getHistoryCommand) {
        return new BaseCall<>(retrofitService.getHistory(getHistoryCommand));
    }

    public BaseCall<NonStandardResponse> clearHistory(ClearHistoryCommand command) {
        return new BaseCall<>(retrofitService.clearHistory(command));
    }

    public BaseCall<PopularResponse> getPopular(GetPopularCommand getPopularCommand) {
        return new BaseCall<>(retrofitService.getPopular(getPopularCommand));
    }

    public BaseCall<NotificationResponse> getNotification(GetNotificationCommand getNotificationCommand) {
        return new BaseCall<>(retrofitService.getNotification(getNotificationCommand));
    }

    public BaseCall<FavoritesResponse> getFavorites(GetFavoritesCommand getFavoritesCommand) {
        return new BaseCall<>(retrofitService.getFavorites(getFavoritesCommand));
    }

    public BaseCall<GetAutoCompleteSearchPhrasesResponse> getAutoCompleteSearchPhrases(
            GetAutoCompleteSearchPhrasesCommand getFavoritesCommand) {
        Observable<GetAutoCompleteSearchPhrasesResponse> autoCompleteSearchPhrases =
                retrofitService.getAutoCompleteSearchPhrases(getFavoritesCommand);
        if(autoCompleteSearchPhrases==null){
            Timber.w("autoCompleteSearchPhrases is null");
        }
        return new BaseCall<>(autoCompleteSearchPhrases);
    }

    public BaseCall<GetAutoCompleteSearchLocationsResponse> getAutoCompleteSearchLocations(
            GetAutoCompleteSearchLocationsCommand getAutoCompleteSearchLocationsCommand) {
        return new BaseCall<>(retrofitService.getAutoCompleteSearchLocations(getAutoCompleteSearchLocationsCommand));
    }

    public BaseCall<SearchResponse> getRecentSearchPhrase() {
        return new BaseCall<>(retrofitService.getRecentSearchPhraseCommand());
    }

    public BaseCall<RecentLocationsResponse> getRecentSearchLocation() {
        return new BaseCall<>(retrofitService.getRecentSearchLocationCommand());
    }

//    public BaseCall<GetQuickCardsAnonymousResponse> getQuickCardsAnonymous() {
//        return new BaseCall<>(retrofitService.getQuickCardsAnonymous());
//    }

    public BaseCall<GetQuickCardsAnonymousResponse> getQuickCardsAuthenticated(GetQuickCardsCommand getQuickCardsCommand) {
        return new BaseCall<>(retrofitService.getQuickCardsAuthenticated(getQuickCardsCommand));
    }

    public BaseCall<SearchResponseHuge> search(SearchCommand searchCommand) {
        return new BaseCall<>(retrofitService.search(searchCommand));
    }

    public BaseCall<AlertResponse> alerts(AlertsCommand alertsCommand) {
        return new BaseCall<>(retrofitService.alerts("0", alertsCommand));
    }

    public BaseCall<NonStandardResponse> clearRecentSearchPhrases() {
        return new BaseCall<>(retrofitService.clearRecentSearchPhrases());
    }

    public BaseCall<NonStandardResponse> clearRecentLocations() {
        return new BaseCall<>(retrofitService.clearRecentLocations());
    }

    public BaseCall<NonStandardResponse> removeRecentSearchItem(RemoveSearchItemCommand removeSearchItemCommand) {
        return new BaseCall<>(retrofitService.removeRecentSearchItem(removeSearchItemCommand));
    }

    public BaseCall<NonStandardResponse> changeNotificationStatus(UpdateNotificationStatusCommand command) {
        return new BaseCall<>(retrofitService.changeNotificationStatus(command));
    }

    public BaseCall<TaxonomyVocabularyTermsResponse> getTaxonomyVocabularyTerms(TaxonomyVocabularyTermsCommand command) {
        return new BaseCall<>(retrofitService.getTaxonomyVocabularyTerms(command));
    }

    public BaseCall<NonStandardResponse> addRemoveFavorite(AddOrRemoveFavoriteCommand command) {
        return new BaseCall<>(retrofitService.addOrRemoveFavorite(command));
    }

    public BaseCall<NonStandardResponse> addInHistory(AddInHistoryCommand command) {
        return new BaseCall<>(retrofitService.addInHistory(command));
    }

    public BaseCall<NonStandardResponse> updateCredentials(ChangeCredentialsCommand changeCredentialsCommand) {
        return new BaseCall<>(retrofitService.updateCredentials(changeCredentialsCommand));
    }

    public BaseCall<AnchorResponse> getAnchors(GetAnchorsCommand anchorsCommand) {
        return new BaseCall<>(retrofitService.getAnchors(anchorsCommand));
    }

    public BaseCall<PingNonAuthResponse> ping_not_auth() {
        return new BaseCall<>(retrofitService.ping_not_auth());
    }

    public BaseCall<RegisterUserResponse> sendEmailConfirmation() {
        return new BaseCall<>(retrofitService.sendEmailConfirmation());
    }

    public BaseCall<RegisterUserResponse> updatePersonalInformation(UpdatePersonalInformationCommand command) {
        return new BaseCall<>(retrofitService.updatePersonalInformation(command));
    }

    public BaseCall<FeedbackFormResponse> getForm(GetFormCommand command) {
        return new BaseCall<>(retrofitService.getForm(command));
    }

    public BaseCall<NonStandardResponse> submitFormAnonymous(Map<String, Object> body) {
        return new BaseCall<>(retrofitService.submitFormAnonymous(body));
    }

    public BaseCall<NonStandardResponse> submitForm(Map<String, Object> body) {
        return new BaseCall<>(retrofitService.submitForm(body));
    }

    public BaseCall<GetSsoInfoResponse> getSsoInfo(GetSsoInfoCommand command) {
        return new BaseCall<>(retrofitService.getSsoInfo(command));
    }

    public BaseCall<RegisterUserResponse> updatePersonalize(UpdatePersonalizeCommand command) {
        return new BaseCall<>(retrofitService.updatePersonalize(command));
    }

    public BaseCall<NonStandardResponse> addContactFormFeedback(AddContactFormFeedbackCommand command) {
        return new BaseCall<>(retrofitService.addContactFormFeedback(command));
    }

    public BaseCall<NonStandardResponse> addContactFormFeedbackAnonymous(AddContactFormFeedbackCommand command) {
        return new BaseCall<>(retrofitService.addContactFormFeedbackAnonymous(command));
    }

    public BaseCall<NonStandardResponse> cancelAccount() {
        return new BaseCall<>(retrofitService.cancelAccount());
    }

    public BaseCall<NonStandardResponse> sendSubmitForm(BaseFeedbackCommandCommand command) {
        return new BaseCall<>(retrofitService.sendSubmitForm(command));
    }

    public BaseCall<NonStandardResponse> setPushCredential(SetPushCredentialServiceCommand serviceCommand) {
        return new BaseCall<>(retrofitService.setPushCredential(serviceCommand));
    }

    public BaseCall<NonStandardResponse> delPushCredential(DelPushCredentialServiceCommand serviceCommand) {
        return new BaseCall<>(retrofitService.delPushCredential(serviceCommand));
    }

    public BaseCall<NonStandardResponse> sendSubmitFormA(BaseFeedbackCommandCommand command) {
        return new BaseCall<>(retrofitService.sendSubmitFormA(command));
    }

    public BaseCall<ConfigurationResponse> getConfiguration(GetConfigurationCommand command) {
        return new BaseCall<ConfigurationResponse>(retrofitService.getConfiguration(command));
    }

    public BaseCall<GetAutoCompleteSearchLocationsResponse> sgplocationComplete(String loc, Integer strict){
        return new BaseCall<>(retrofitService.sgplocationCcomplete(loc, strict));
    }

    public BaseCall<AddToHistoryResponse> addToHistory(AddToHistoryCommand addToHistoryCommand) {
        return new BaseCall<>(retrofitService.addToHistory(addToHistoryCommand));
    }

    public BaseCall<NonStandardResponse> checkPassword(CheckPasswordCommand checkPasswordCommand) {
        return new BaseCall<>(retrofitService.checkPassword(checkPasswordCommand));
    }

    public BaseCall<BaseResponse> sendEmergency(EmergencyCommand emergencyCommand) {
        return new BaseCall<>(retrofitService.sendEmergency(emergencyCommand));
    }

    public BaseCall<UrgenceResponse> getUrgenceResponseBaseCall() {
        return new BaseCall<>(retrofitService.getUrgence());
    }
}
