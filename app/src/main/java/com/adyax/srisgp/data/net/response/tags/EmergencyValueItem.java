package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class EmergencyValueItem implements Parcelable {

    @SerializedName("name")
    public String name;

    @SerializedName("value")
    public Long value;

    protected EmergencyValueItem(Parcel in) {
        name = in.readString();
        if (in.readByte() == 0) {
            value = null;
        } else {
            value = in.readLong();
        }
    }

    public static final Creator<EmergencyValueItem> CREATOR = new Creator<EmergencyValueItem>() {
        @Override
        public EmergencyValueItem createFromParcel(Parcel in) {
            return new EmergencyValueItem(in);
        }

        @Override
        public EmergencyValueItem[] newArray(int size) {
            return new EmergencyValueItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        if (value == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(value);
        }
    }
}
