package com.adyax.srisgp.data.net.response;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.App;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.response.tags.GetWaitingTimeConf;
import com.adyax.srisgp.data.net.response.tags.QuestionnaireItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.SearchFilterHuge;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class SearchResponseHuge extends BaseResponse implements Parcelable {

    public static final String BUNDLE_SEARCH = "search";

    @SerializedName("items")
    public List<SearchItemHuge> items = new ArrayList<>();

    @SerializedName("has_more")
    public int has_more;

    @SerializedName("total")
    public int total;

    @SerializedName("filters")
    public ArrayList<SearchFilterHuge> filters = new ArrayList<>();

    @SerializedName("related")
    public List<Related> related = new ArrayList<>();

    @SerializedName("state_token")
    public String stateToken;

    @SerializedName("distance_sort")
    public int distance_sort;

    @SerializedName("around_lat")
    public double around_lat;

    @SerializedName("around_lon")
    public double around_lon;

    @SerializedName("area_center_lat")
    public double area_center_lat;

    @SerializedName("area_center_lon")
    public double area_center_lon;

    @SerializedName("area")
    public String area;

    @SerializedName("region")
    public String region;

    @SerializedName("suggested_type")
    public String suggestedType;

    @SerializedName("empty_result_inactive_region")
    public int empty_result_inactive_region;

    @SerializedName("messages")
    public Map<String, String> messages;

    @SerializedName("bmc")
    public String bmc;

    @SerializedName("questionnaire")
    public QuestionnaireItem questionnaire;

    @SerializedName("get_waiting_time_conf")
    public GetWaitingTimeConf get_waiting_time_conf;

//    private boolean geoLocation;

    private boolean active;

    public boolean hasMore() {
        return has_more == 1;
    }

    public int getTotal() {
//        return items!=null? items.size(): 0;
        return total;
    }

    public boolean isEmpty() {
        return items == null || items.isEmpty();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
        dest.writeInt(this.has_more);
        dest.writeInt(this.total);
        dest.writeTypedList(this.filters);
        dest.writeList(this.related);
        dest.writeString(this.stateToken);
        dest.writeInt(this.distance_sort);
        dest.writeDouble(this.around_lat);
        dest.writeDouble(this.around_lon);
        dest.writeDouble(this.area_center_lat);
        dest.writeDouble(this.area_center_lon);
        dest.writeString(this.area);
        dest.writeString(this.bmc);
        dest.writeParcelable(this.get_waiting_time_conf, flags);
        dest.writeString(this.suggestedType);
        dest.writeInt(this.active ? 1 : 0);
    }

    public SearchResponseHuge() {
    }

    protected SearchResponseHuge(Parcel in) {
        this.items = new ArrayList<SearchItemHuge>();
        in.readList(this.items, SearchItemHuge.class.getClassLoader());
        this.has_more = in.readInt();
        this.total = in.readInt();
        this.filters = in.createTypedArrayList(SearchFilterHuge.CREATOR);
        this.related = new ArrayList<Related>();
        in.readList(this.related, Related.class.getClassLoader());
        this.stateToken = in.readString();
        this.distance_sort = in.readInt();
        this.around_lat = in.readDouble();
        this.around_lon = in.readDouble();
        this.area_center_lat = in.readDouble();
        this.area_center_lon = in.readDouble();
        this.area = in.readString();
        this.bmc = in.readString();
        this.get_waiting_time_conf = in.readParcelable(GetWaitingTimeConf.class.getClassLoader());
        this.suggestedType = in.readString();
        this.active = in.readInt() == 1;
    }

    public static final Creator<SearchResponseHuge> CREATOR = new Creator<SearchResponseHuge>() {
        @Override
        public SearchResponseHuge createFromParcel(Parcel source) {
            return new SearchResponseHuge(source);
        }

        @Override
        public SearchResponseHuge[] newArray(int size) {
            return new SearchResponseHuge[size];
        }
    };

    public boolean isAroundGeoLocation() {
        return Math.abs(around_lat) > AroundMeArguments.MIN_DOUBLE && Math.abs(around_lon) > AroundMeArguments.MIN_DOUBLE;
    }

    public int getCount() {
        return items == null ? 0 : items.size();
    }

    public String getWhere() {
        if (isAroundGeoLocation()) {
            try {
                final List<Address> addresses = new Geocoder(App.getAppContext(),
                        Locale.getDefault()).getFromLocation(around_lat, around_lon, 1);
                if (addresses.size() > 0) {
                    return "viewport: " + addresses.get(0).getLocality();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public boolean isAreaCenter() {
        return Math.abs(area_center_lat) > AroundMeArguments.MIN_DOUBLE && Math.abs(area_center_lon) > AroundMeArguments.MIN_DOUBLE;
    }

    public boolean isAreaActive() {
        return !isEmpty() || (isAreaCenter() || isAroundGeoLocation());
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isSuggestedType(SearchType searchType) {
        return suggestedType != null && searchType.name().equals(suggestedType);
    }

    public Location getPreciseAddress() {
        Location location = new Location("City");
        location.setLatitude(around_lat);
        location.setLongitude(around_lon);
        return location;
    }

    public boolean isPreciseAddress() {
        return isAroundGeoLocation();
    }
}
