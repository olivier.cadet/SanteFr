package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

public class DelPushCredentialServiceCommand extends SetPushCredentialServiceCommand {

    private DelPushCredentialServiceCommand(Parcel in) {
        super(in);
    }

    public DelPushCredentialServiceCommand(String deviceToken, String uid) {
        super(deviceToken, uid);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public static final Creator<DelPushCredentialServiceCommand> CREATOR = new Creator<DelPushCredentialServiceCommand>() {
        @Override
        public DelPushCredentialServiceCommand createFromParcel(Parcel in) {
            return new DelPushCredentialServiceCommand(in);
        }

        @Override
        public DelPushCredentialServiceCommand[] newArray(int size) {
            return new DelPushCredentialServiceCommand[size];
        }
    };

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.delPushCredential(this, receiver, requestCode);
    }
}
