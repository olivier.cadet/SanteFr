package com.adyax.srisgp.data.net.response.tags;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.R;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class SearchConfigurationItem implements Parcelable {

    @SerializedName("mapbox_style")
    public String mapbox_style;

    public String getEmpty_result_inactive_region() {
        return empty_result_inactive_region;
    }

    public String getAutocomplete_inactive_region() {
        return autocomplete_inactive_region;
    }

    @SerializedName("empty_result_inactive_region")
    public String empty_result_inactive_region;

    @SerializedName("autocomplete_inactive_region")
    public String autocomplete_inactive_region;

    public SearchConfigurationItem(Context context) {
        mapbox_style = context.getString(R.string.default_mapbox_style_url);
        empty_result_inactive_region = context.getString(R.string.empty_result_inactive_region);
    }

    public SearchConfigurationItem() {

    }

    protected SearchConfigurationItem(Parcel in) {
        mapbox_style = in.readString();
        empty_result_inactive_region = in.readString();
        autocomplete_inactive_region = in.readString();
    }

    public static final Creator<SearchConfigurationItem> CREATOR = new Creator<SearchConfigurationItem>() {
        @Override
        public SearchConfigurationItem createFromParcel(Parcel in) {
            return new SearchConfigurationItem(in);
        }

        @Override
        public SearchConfigurationItem[] newArray(int size) {
            return new SearchConfigurationItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mapbox_style);
        parcel.writeString(empty_result_inactive_region);
        parcel.writeString(autocomplete_inactive_region);
    }

    public String getMapbox_style() {
        return mapbox_style;
    }

}
