package com.adyax.srisgp.data.net.command;

import android.content.Context;
import android.location.Location;
import android.os.Parcel;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class SearchCommand implements Parcelable {
public class SearchCommand extends ServiceCommand {

    public static final String MASK_KEYWORD2 = "{%s} & {%s}";
    public static final int EQUAL = 0;
    public static final int FILTER_NOT_EQUAL = 1;
    public static final int NOT_EQUAL = 2;

    // TODO Hardcode
    @SerializedName("type")
    private SearchType type;

    @SerializedName("text")
    private String text;

    @SerializedName("location")
    private String location;

    @SerializedName("latitude")
    private Double latitude;

    @SerializedName("longitude")
    private Double longitude;

    @SerializedName("accuracy")
    private Integer accuracy;

    @SerializedName("filters")
    private ArrayList<SearchFilter> filters;

    @SerializedName("page")
    private int page;

    // Attention!!! more than 100 do not works
    @SerializedName("items_per_page")
    private int itemsPerPage = 20;

    @SerializedName("state_token")
    private String stateToken;

    @SerializedName("bbox")
    private double[] bbox;

    @SerializedName("extra_response_fields")
    private List<String> extraFields;

    @SerializedName("order")
    private String order;


    public transient double around_lat;
    public transient double around_lon;
    public transient double area_center_lat;
    public transient double area_center_lon;
    public transient String area;

    public SearchCommand() {
    }

    public SearchCommand(String text) {
        this.text = text;
    }

    public int compare(SearchCommand s) {
        if (s != null) {
            if (Objects.equals(s.type, type) &&
                    Objects.equals(s.text, text) &&
                    Objects.equals(s.order, order) &&
                    Objects.equals(s.location, location) &&
                    Objects.equals(s.latitude, latitude) &&
                    Objects.equals(s.longitude, longitude) &&
                    Objects.equals(s.accuracy, accuracy) &&
//                Objects.equals(s.page, page)&&
                    Objects.equals(s.itemsPerPage, itemsPerPage) &&
                    Objects.equals(s.bbox, bbox) &&
                    Objects.equals(s.extraFields, extraFields)

            ) {
                if (Objects.equals(s.filters, filters)) {
                    return EQUAL;
                } else {
                    if (Objects.equals(s.stateToken, stateToken)) {
                        return FILTER_NOT_EQUAL;
                    }
                }
            }
        }
        return NOT_EQUAL;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.search(this, receiver, requestCode);
    }

    public void setExtraFields(List<String> extraFields) {
        this.extraFields = extraFields;
    }

    public List<String> getExtraFields() {
        return extraFields;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SearchCommand setType(SearchType type) {
        this.type = type;
        return this;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setStateToken(String stateToken) {
        this.stateToken = stateToken;
    }

    public String getCityLocation() {
        return location;
    }

    public void setGeo(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public void setFilters(ArrayList<SearchFilter> filters) {
        this.filters = filters;
    }

    //used for getting filters
    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public SearchType getType() {
        return type;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public ArrayList<SearchFilter> getFilters() {
        return filters;
    }

    public int getPage() {
        return page;
    }

    public String getStateToken() {
        return stateToken;
    }

    public void nextPage() {
        page++;
    }

    public boolean isFirstRequest() {
        return page == 0;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getSearch() {
        return text;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public boolean isGeoLocation() {
        return longitude != null && latitude != null && Math.abs(longitude) > AroundMeArguments.MIN_DOUBLE && Math.abs(latitude) > AroundMeArguments.MIN_DOUBLE;
    }

    public void update(SearchResponseHuge response) {
        stateToken = response.stateToken;
        around_lat = response.around_lat;
        around_lon = response.around_lon;
        area_center_lat = response.area_center_lat;
        area_center_lon = response.area_center_lon;
        area = response.area;
    }

//    public Location getMapCenterLocation() {
//        Location result = getMyLocation();
//        if (result != null) {
//            return result;
//        }
//        result = new Location("network");
//        if (isAreaCenterLocation()) {
//            result.setLatitude(area_center_lat);
//            result.setLongitude(area_center_lon);
//        } else {
//            result.setLatitude(AroundMeArguments.CENTRE_OF_FRANCE_LATITUDE);//48.85
//            result.setLongitude(AroundMeArguments.CENTRE_OF_FRANCE_LONGITUDE);//2.34
//        }
//        return result;
//    }
//
//    public boolean isAreaCenterLocation() {
//        return Math.abs(area_center_lon) > AroundMeArguments.MIN_DOUBLE && Math.abs(area_center_lat) > AroundMeArguments.MIN_DOUBLE;
//    }

    public Location getMyLocation() {
//        if (longitude != null && latitude != null && Math.abs(longitude) > AroundMeArguments.MIN_DOUBLE && Math.abs(latitude) > AroundMeArguments.MIN_DOUBLE) {
        if (isGeoLocation()) {
            Location result = new Location("network");
            result.setLatitude(latitude);
            result.setLongitude(longitude);
            return result;
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.text);
        dest.writeString(this.location);
        dest.writeValue(this.latitude);
        dest.writeValue(this.longitude);
        dest.writeValue(this.accuracy);
        dest.writeTypedList(this.filters);
        dest.writeInt(this.page);
        dest.writeInt(this.itemsPerPage);
        dest.writeString(this.stateToken);
        dest.writeStringList(this.extraFields);
        dest.writeDouble(this.around_lat);
        dest.writeDouble(this.around_lon);
        dest.writeDouble(this.area_center_lat);
        dest.writeDouble(this.area_center_lon);
        dest.writeString(this.area);
        dest.writeString(this.order);
        if (this.bbox != null && this.bbox.length == 4) {
            dest.writeInt(4);
            dest.writeDouble(bbox[0]);
            dest.writeDouble(bbox[1]);
            dest.writeDouble(bbox[2]);
            dest.writeDouble(bbox[3]);
        } else {
            dest.writeInt(0);
        }
    }

    protected SearchCommand(Parcel in) {
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : SearchType.values()[tmpType];
        this.text = in.readString();
        this.location = in.readString();
        this.latitude = (Double) in.readValue(Double.class.getClassLoader());
        this.longitude = (Double) in.readValue(Double.class.getClassLoader());
        this.accuracy = (Integer) in.readValue(Integer.class.getClassLoader());
        this.filters = in.createTypedArrayList(SearchFilter.CREATOR);
        this.page = in.readInt();
        this.itemsPerPage = in.readInt();
        this.stateToken = in.readString();
        this.extraFields = in.createStringArrayList();
        this.around_lat = in.readDouble();
        this.around_lon = in.readDouble();
        this.area_center_lat = in.readDouble();
        this.area_center_lon = in.readDouble();
        this.area = in.readString();
        this.order = in.readString();
        final int length = in.readInt();
        if (length > 0) {
            if (length == 4) {
                bbox = new double[4];
                bbox[0] = in.readDouble();
                bbox[1] = in.readDouble();
                bbox[2] = in.readDouble();
                bbox[3] = in.readDouble();
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public static final Creator<SearchCommand> CREATOR = new Creator<SearchCommand>() {
        @Override
        public SearchCommand createFromParcel(Parcel source) {
            return new SearchCommand(source);
        }

        @Override
        public SearchCommand[] newArray(int size) {
            return new SearchCommand[size];
        }
    };

//    public boolean isCity() {
//        return location!=null&& location.length()>0;
//    }

    public boolean isAroundGeo() {
        return location != null && location.length() > 0 && Math.abs(around_lon) > AroundMeArguments.MIN_DOUBLE && Math.abs(around_lat) > AroundMeArguments.MIN_DOUBLE;
    }

    public Location getAroundLocation() {
        if (isAroundGeo()) {
            Location result = new Location("network");
            result.setLatitude(around_lat);
            result.setLongitude(around_lon);
            return result;
        }
        return null;
    }

    public void initBbox(double minLong, double minLat, double maxLong, double maxLat) {
        bbox = new double[4];
        bbox[0] = minLong;
        bbox[1] = minLat;
        bbox[2] = maxLong;
        bbox[3] = maxLat;
    }

    public void clearBbox() {
        bbox = null;
    }

    public double[] getBbox() {
        return bbox;
    }

    public void setBbox(double[] bbox) {
        this.bbox = bbox;
    }

    public String getSearchText() {
        if (text != null) {
            return text;
        } else {
            return "";
        }
    }

    public boolean isBbox() {
        return bbox != null;
    }

    public String getWhere() {
        if (location != null) {
            return location;
        }
        if (isGeoLocation()) {
            return App.getAppContext().getString(R.string.search_around_me);
        }
        return "";
    }

    public String getWhere(Context context) {
        if (location != null) {
            return location;
        }
        if (isGeoLocation()) {
            return App.getAppContext().getString(R.string.search_around_me);
        }
        return "";
    }

    public String getLocationATformat() {
        return String.format(MASK_KEYWORD2, getSearchText(), getWhere());
    }

    public SearchCommand getClone() {
        final Gson gson = new Gson();
        final SearchCommand result = gson.fromJson(gson.toJson(this), SearchCommand.class);
        result.around_lat = around_lat;
        result.around_lon = around_lon;
        result.area_center_lat = area_center_lat;
        result.area_center_lon = area_center_lon;
        result.area = area;
        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((area == null) ? 0 : area.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return getType().name();
    }

}
