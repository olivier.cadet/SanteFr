package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.User;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class PopularResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public List<String> items;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.items);
    }

    public PopularResponse() {
    }

    protected PopularResponse(Parcel in) {
        this.items = in.createStringArrayList();
    }

    public static final Parcelable.Creator<PopularResponse> CREATOR = new Parcelable.Creator<PopularResponse>() {
        @Override
        public PopularResponse createFromParcel(Parcel source) {
            return new PopularResponse(source);
        }

        @Override
        public PopularResponse[] newArray(int size) {
            return new PopularResponse[size];
        }
    };
}
