package com.adyax.srisgp.data.net.command;

import android.app.Service;
import android.os.Parcel;
import android.widget.TabHost;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class RemoveSearchItemCommand extends ServiceCommand {
    public static final int ITEM_LOCATION = 0;
    public static final int ITEM_PHRASE = 1;

    private List<Long> sids;

    private transient int itemType;

    public RemoveSearchItemCommand(long id, int itemType) {
        this.sids = new ArrayList<Long>(1) {{
            add(id);
        }};
        this.itemType = itemType;
    }

    public List<Long> getSids() {
        return sids;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.removeRecentSearchItem(this, receiver, requestCode, itemType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.sids);
    }

    protected RemoveSearchItemCommand(Parcel in) {
        this.sids = new ArrayList<Long>();
        in.readList(this.sids, Long.class.getClassLoader());
    }

    public static final Creator<RemoveSearchItemCommand> CREATOR = new Creator<RemoveSearchItemCommand>() {
        @Override
        public RemoveSearchItemCommand createFromParcel(Parcel source) {
            return new RemoveSearchItemCommand(source);
        }

        @Override
        public RemoveSearchItemCommand[] newArray(int size) {
            return new RemoveSearchItemCommand[size];
        }
    };
}
