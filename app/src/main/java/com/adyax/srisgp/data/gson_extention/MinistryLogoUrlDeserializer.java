package com.adyax.srisgp.data.gson_extention;

import com.adyax.srisgp.data.net.response.MinistryLogoUrl;
import com.adyax.srisgp.data.net.response.tags.ImageConfigurationItem;
import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;

/**
 * Created by anton.kobylianskiy on 5/12/17.
 */

public class MinistryLogoUrlDeserializer implements JsonDeserializer<MinistryLogoUrl> {

    @Override
    public MinistryLogoUrl deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Set<Map.Entry<String, JsonElement>> entries = ((JsonObject) json).entrySet();
        if (!entries.isEmpty()) {
            Map.Entry<String, JsonElement> element = entries.iterator().next();
            JsonElement value = element.getValue();
            return new MinistryLogoUrl(((JsonObject) value).get(ImageConfigurationItem.HEADER_LOGO_ANDROID).getAsString());
        }
        return new MinistryLogoUrl("");
    }
}
