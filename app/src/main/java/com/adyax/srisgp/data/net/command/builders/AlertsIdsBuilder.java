package com.adyax.srisgp.data.net.command.builders;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by SUVOROV on 10/7/16.
 */

public class AlertsIdsBuilder {

    private Timestamp time;
    private Map<String, String> map = new HashMap<>();

    public AlertsIdsBuilder add(String header) {
        try {
            String[] pairs = header.split(":");
            if (pairs.length == 2) {
                long pair = Long.parseLong(pairs[0]);
                time = new Timestamp(pair * 1000);
                pairs = pairs[1].split(",");
                for (String e : pairs) {
                    String[] ids = e.split("-");
                    if (ids.length == 2) {
                        map.put(ids[0], ids[1]);
                    }
                }
            }
        } catch (Exception e) {

        }
        return this;
    }

    public long[] build() {
        try {
            long[] result = new long[map.size()];
            int i = 0;
            for (String s : map.keySet()) {
                result[i++] = Long.parseLong(s);
            }
            return result;
        } catch (Exception e) {

        }
        return new long[0];
    }

    public Timestamp getTime() {
        return time;
    }

    public static boolean alertsCompare(String alertNew, String alertOld) {
        if (alertNew != null) {
            if (alertOld == null) {
                return true;
            }
            final String[] splitNew = alertNew.split(":");
            final String[] splitOld = alertOld.split(":");
            if (splitNew.length == 2) {
                if (splitOld.length == 2) {
                    return !splitNew[1].equals(splitOld[1]);
                } else {
                    return true;
                }
            } else {
                return splitOld.length == 2;
            }
        }
        return false;
    }
}
