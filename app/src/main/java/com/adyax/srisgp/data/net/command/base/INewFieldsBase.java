package com.adyax.srisgp.data.net.command.base;

/**
 * Created by SUVOROV on 4/14/17.
 */

public interface INewFieldsBase {
    // 1
    public static final String MAIL = "mail";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String PASS = "pass";
    public static final String PASSWORD = "password";
    //2
    public static final String ID = "id";
    public static final String ID_OTHER = "id";
    public static final String ID_DB = "id";
    public static final String NID = "nid";
    //4
    public static final String URL = "node_url";
    public static final String URL_OTHER = "url";
    public static final String URL_DB = "url";
    public static final String LINK = "link";
    public static final String LINK_DB = "link";
    public static final String LINK_TITLE = "link_title";
    public static final String IMAGE = "image";

    // 5
    public static final String PHONE = "phone";
    public static final String PHONE_ARRAY = "phone";
    public static final String PHONE_DB = "phone";
    public static final String PHONE_NUMBER = "phone_number";
    // 6
    public static final String APP_STORE = "field_lien_app_store";
    public static final String APP_STORE_DB = "field_lien_app_store";
    public static final String PLAY_STORE = "field_lien_play_store";
    public static final String PLAY_STORE_DB = "field_lien_play_store";
    public static final String GOOGLE = "google";
    public static final String GOOGLE_DB = "google";
    public static final String APPLE = "apple";
    public static final String APPLE_DB = "apple";
    // 7
    public static final String PROPOSE = "propose";
    public static final String PROPOSE_DB = "propose";
    public static final String SOURCE = "source";
    public static final String SOURCE_DB = "source";
}
