package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class GetAutoCompleteSearchPhrasesCommand extends ServiceCommand {

    public String text;

    public GetAutoCompleteSearchPhrasesCommand(Parcel in) {
        text = in.readString();
    }

    public static final Creator<GetAutoCompleteSearchPhrasesCommand> CREATOR = new Creator<GetAutoCompleteSearchPhrasesCommand>() {
        @Override
        public GetAutoCompleteSearchPhrasesCommand createFromParcel(Parcel in) {
            return new GetAutoCompleteSearchPhrasesCommand(in);
        }

        @Override
        public GetAutoCompleteSearchPhrasesCommand[] newArray(int size) {
            return new GetAutoCompleteSearchPhrasesCommand[size];
        }
    };

    public GetAutoCompleteSearchPhrasesCommand(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getAutocompletedSearchPhrases(this, receiver, requestCode);
    }

}
