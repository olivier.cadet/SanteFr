package com.adyax.srisgp.data.net.command;

import android.os.Parcel;
import android.provider.Settings;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.command.tags.Sso_type;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Kirill on 15.11.16.
 */

public class GetSsoInfoCommand extends ServiceCommand implements INewFields {

    @SerializedName("sso_type")
    private Sso_type ssoType = Sso_type.sgp_sso_google;

    @SerializedName("device_type")
    private String deviceType = "android";

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("expires_in")
    private String expiresIn = "3600000";

    @SerializedName("created")
    private String created;

    @SerializedName(EMAIL)
    private String email;

    public GetSsoInfoCommand(String accessToken, String email) {
        this.accessToken = accessToken;
        this.created = Long.toString(System.currentTimeMillis() - 3600);
        this.email = email;
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getSsoInfo(this, receiver, requestCode);
    }

    public Sso_type getSsoType() {
        return ssoType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ssoType == null ? -1 : this.ssoType.ordinal());
        dest.writeString(this.deviceType);
        dest.writeString(this.accessToken);
        dest.writeString(this.expiresIn);
        dest.writeString(this.created);
        dest.writeString(this.email);
    }

    protected GetSsoInfoCommand(Parcel in) {
        int tmpSsoType = in.readInt();
        this.ssoType = tmpSsoType == -1 ? null : Sso_type.values()[tmpSsoType];
        this.deviceType = in.readString();
        this.accessToken = in.readString();
        this.expiresIn = in.readString();
        this.created = in.readString();
        this.email = in.readString();
    }

    public static final Creator<GetSsoInfoCommand> CREATOR = new Creator<GetSsoInfoCommand>() {
        @Override
        public GetSsoInfoCommand createFromParcel(Parcel source) {
            return new GetSsoInfoCommand(source);
        }

        @Override
        public GetSsoInfoCommand[] newArray(int size) {
            return new GetSsoInfoCommand[size];
        }
    };
}
