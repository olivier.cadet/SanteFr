package com.adyax.srisgp.data.net.executor;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.AddContactFormFeedbackCommand;
import com.adyax.srisgp.data.net.command.AddInHistoryCommand;
import com.adyax.srisgp.data.net.command.AddOrRemoveFavoriteCommand;
import com.adyax.srisgp.data.net.command.AddToHistoryCommand;
import com.adyax.srisgp.data.net.command.AlertsCommand;
import com.adyax.srisgp.data.net.command.ChangeCredentialsCommand;
import com.adyax.srisgp.data.net.command.CheckPasswordCommand;
import com.adyax.srisgp.data.net.command.CitiesCommand;
import com.adyax.srisgp.data.net.command.ClearHistoryCommand;
import com.adyax.srisgp.data.net.command.CreateAccountCommand;
import com.adyax.srisgp.data.net.command.DelPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.EmergencyCommand;
import com.adyax.srisgp.data.net.command.GetAnchorsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchLocationsCommand;
import com.adyax.srisgp.data.net.command.GetAutoCompleteSearchPhrasesCommand;
import com.adyax.srisgp.data.net.command.GetConfigurationCommand;
import com.adyax.srisgp.data.net.command.GetFavoritesCommand;
import com.adyax.srisgp.data.net.command.GetFormCommand;
import com.adyax.srisgp.data.net.command.GetHistoryCommand;
import com.adyax.srisgp.data.net.command.GetNotificationCommand;
import com.adyax.srisgp.data.net.command.GetPopularCommand;
import com.adyax.srisgp.data.net.command.GetQuickCardsCommand;
import com.adyax.srisgp.data.net.command.GetSsoInfoCommand;
import com.adyax.srisgp.data.net.command.LoginUserCommand;
import com.adyax.srisgp.data.net.command.LoginUserSsoCommand;
import com.adyax.srisgp.data.net.command.RemoveSearchItemCommand;
import com.adyax.srisgp.data.net.command.ResetCommand;
import com.adyax.srisgp.data.net.command.SearchCommand;
import com.adyax.srisgp.data.net.command.SetPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.SgpLocationCompleteCommand;
import com.adyax.srisgp.data.net.command.SubmitFeedbackFormCommand;
import com.adyax.srisgp.data.net.command.TaxonomyVocabularyTermsCommand;
import com.adyax.srisgp.data.net.command.UpdateNotificationStatusCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalInformationCommand;
import com.adyax.srisgp.data.net.command.UpdatePersonalizeCommand;
import com.adyax.srisgp.data.net.command.feedback.BaseFeedbackCommandCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.core.RestClient;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.GetHistoryResponse;
import com.adyax.srisgp.data.net.response.RecentLocationsResponse;
import com.adyax.srisgp.data.net.response.SearchMessage;
import com.adyax.srisgp.data.net.response.SearchResponse;
import com.adyax.srisgp.data.net.response.SearchResponseHuge;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.BmcItem;
import com.adyax.srisgp.data.net.response.tags.ErrorItem;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.RecentLocationItem;
import com.adyax.srisgp.data.net.response.tags.Related;
import com.adyax.srisgp.data.net.response.tags.RelatedFilters;
import com.adyax.srisgp.data.net.response.tags.RelatedItem;
import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.repository.SharedPreferencesHelper;
import com.adyax.srisgp.data.tracker.IHistoryRecentUpdateHelper;
import com.adyax.srisgp.data.tracker.ITracker;
import com.adyax.srisgp.db.BaseOrmLiteSqliteOpenHelper;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.mvp.cguHandler.CguHandler;
import com.adyax.srisgp.mvp.create_account.flow.your_information_2.GenderType;
import com.adyax.srisgp.mvp.history.HistoryType;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.INotificationHelper;
import com.adyax.srisgp.sound.ISoundPlayer;
import com.adyax.srisgp.utils.FiltersHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class CommandExecutor {

    public static final String BUNDLE_DATA = "data";

    public static final String BUNDLE_GET_QUICK_CARDS = "get_quick_cards";
    public static final String BUNDLE_GET_RECENT_PHRASES = "get_recent_phrases";
    public static final String BUNDLE_GET_RECENT_LOCATIONS = "get_recent_locations";

    public static final String BUNDLE_GET_POPULAR_PHRASES = "get_popular_phrases";

    public static final String BUNDLE_GET_AUTOCOMPLETE_SEARCH_PHRASE = "get_autocomplete_search_phrase";
    public static final String BUNDLE_GET_AUTOCOMPLETE_SEARCH_LOCATION = "get_autocomplete_search_location";

    public static final String BUNDLE_REMOVE_SEARCH_IDS = "remove_search_ids";

    public static final String BUNDLE_UPDATE_NOTIFICATION = "update_notification_ids";

    public static final String BUNDLE_IS_FIRST_PAGE = "is_first_page";
    public static final String BUNDLE_NOTIFICATIONS = "notifications";
    public static final String BUNDLE_HISTORIES = "histories";
    public static final String BUNDLE_FAVORITES = "favorites";
    public static final String BUNDLE_ADD_REMOVE_FAVORITE = "add_remove_favorites";
    public static final String BUNDLE_ADD_IN_HISTORY = "add_in_history";
    public static final String BUNDLE_CLEAR_HISTORY = "clear_history";
    public static final String BUNDLE_CONFIGURATION = "ministry_logo";

    public static final String ERROR_TITLE_CODE = "error_title";
    public static final String ERROR_MESSAGE_CODE = "error_message";
    public static final String TAXONOMY_VOCABULARY_TERMS_RESPONSE = "TaxonomyVocabularyTermsResponse";
    public static final String CITIES_RESPONSE = "CitiesResponse";
    public static final String ANCHOR_RESPONSE = "AnchorResponse";

    public static final String BUNDLE_GET_FORM = "get_form";
    public static final String BUNDLE_GET_FORM_CMD = "get_form_cmd";
    public static final String BUNDLE_SGP_LOCATION_COMPLETE = "sgpLocationComplete";
    public static final String START_TIME = "start_time";

    public static final String BUNDLE_ALERT_ITEMS = "alert_items";

    private final Context context;
    private final RestClient restClient;
    private IRepository repository;
    private IDataBaseHelper dataBaseHelper;
    private INetWorkState netWorkState;
    private FiltersHelper filtersHelper;
    private ISoundPlayer soundPlayer;
    private INotificationHelper notificationHelper;
    private boolean pingState = false;
    private ITracker tracker;
    private final CguHandler cguHandler;

    private int retryLoginCount = 0;

    public CommandExecutor(Context context, RestClient restClient, IRepository repository,
                           IDataBaseHelper dataBaseHelper, INetWorkState netWorkState,
                           FiltersHelper filtersHelper, ISoundPlayer soundPlayer,
                           INotificationHelper notificationHelper, ITracker tracker) {
        this.context = context;
        this.restClient = restClient;
        this.repository = repository;
        this.dataBaseHelper = dataBaseHelper;
        this.netWorkState = netWorkState;
        this.filtersHelper = filtersHelper;
        this.soundPlayer = soundPlayer;
        this.notificationHelper = notificationHelper;
        this.tracker = tracker;

        this.cguHandler = new CguHandler(context, restClient, this);
    }

    public boolean isPingState() {
        return pingState;
    }

    public void setPingState(boolean pingState) {
        this.pingState = pingState;
    }

    public void sendSuccess(int requestCode, AppReceiver receiver) {
        if (receiver != null)
            receiver.sendSuccess(requestCode, Bundle.EMPTY);
    }

    private void sendSuccess(int requestCode, AppReceiver receiver, Bundle bundle) {
        if (receiver != null)
            receiver.sendSuccess(requestCode, bundle);
    }

//    private void sendFail(int requestCode, AppReceiver receiver, @StringRes int errorMessage) {
//        sendFail(requestCode, receiver, context.getString(errorMessage));
//    }
//
//    private void sendFailWithTitle(int requestCode, AppReceiver receiver, @StringRes int title, @StringRes int errorMessage) {
//        Bundle bundle = new Bundle();
//        bundle.putString(ERROR_TITLE_CODE, context.getString(title));
//        bundle.putString(ERROR_MESSAGE_CODE, context.getString(errorMessage));
//        receiver.sendFail(requestCode, bundle);
//    }

    public void sendFail(int requestCode, AppReceiver receiver, String errorMessage) {
        Bundle bundle = new Bundle();
        bundle.putString(ERROR_MESSAGE_CODE, errorMessage);
        if (receiver != null) {
            receiver.sendFail(requestCode, bundle);
        }
    }

    private void sendProgress(int requestCode, AppReceiver receiver, int progress) {
        receiver.sendProgress(requestCode, progress);
    }

    public void registerUser(CreateAccountCommand createAccountCommand, final AppReceiver receiver, final int requestCode) {
        restClient.register(createAccountCommand)
                .subscribe(response -> {
                    repository.saveCredential(response, createAccountCommand.getPassword());
                    if (createAccountCommand.isRegister()) {
                        sendSuccess(requestCode, receiver);
                        // because of that autologin absent!!!! i make login
                        if (createAccountCommand.isSsoRegister()) {
                            if (createAccountCommand.isGooglePlus()) {
                                sendSuccess(/*requestCode*/ExecutionService.CREATE_GOOGLE_PLUS_ACCOUNT_ACTION, receiver);
                            } else {
                                LoginUserSsoCommand loginUserSsoCommand = new LoginUserSsoCommand(createAccountCommand);
                                loginUserSsoCommand.setFirstLogin();
                                loginUserSso(loginUserSsoCommand, receiver, ExecutionService.LOGIN_ACTION);
                            }
                        } else {
                            LoginUserCommand loginUserCommand = (LoginUserCommand) new LoginUserCommand(createAccountCommand).setFirstLogin();
                            loginUser(loginUserCommand, receiver, ExecutionService.LOGIN_ACTION);
                        }
                    } else {
                        GenderType gender = createAccountCommand.getGender();
                        gender = gender != null ? gender : GenderType.empty;
                        Bundle bundle = new Bundle();
                        bundle.putString(CreateAccountCommand.GENDER_TYPE, gender.toString());
                        sendSuccess(requestCode, receiver, bundle);
                    }
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void updateCredentials(ChangeCredentialsCommand changeCredentialsCommand, final AppReceiver receiver, final int requestCode) {
        if (TextUtils.equals(changeCredentialsCommand.getOldPassword(), changeCredentialsCommand.getNewPassword())) {
            sendFail(requestCode, receiver, String.format("{\"error\":{\"error_code\":%d,\"error_message\":\"Same password\"}}", ErrorItem.SAME_PASSWORD));
        } else {
            if ((!changeCredentialsCommand.isChangeEmail()) && BuildConfig.FEATURE2_TEST) {
                restClient.checkPassword(new CheckPasswordCommand(changeCredentialsCommand))
                        .subscribe(response -> {
                            // we've forgotten password!!!
                            sendFail(requestCode, receiver, String.format("{\"error\":{\"error_code\":%d,\"error_message\":\"Wrong password\"}}", ErrorItem.WRONG_PASSWORD));
//                            if(response.isSuccess()){
//
//                            }
//                            Bundle bundle = new Bundle();
//                            sendSuccess(requestCode, receiver, bundle);
                        }, (errorMessage, errorCode) -> {
                            final ErrorResponse errorResponse = ErrorResponse.create(new AppReceiver.ErrorMessage(errorMessage));
                            if (errorResponse.getErrorItem().getErrorCode() == ErrorItem.IT_IS_SAME_PASSWORD) {
                                // it is good. We just check out old password the similar way
                                restClient.updateCredentials(changeCredentialsCommand)
                                        .subscribe(response2 -> {
                                                    repository.updateCredentials(changeCredentialsCommand);
                                                    sendSuccess(requestCode, receiver);
                                                },
                                                (errorMessage2, errorCode2) -> sendFail(requestCode, receiver, errorMessage2));
                            } else {
                                sendFail(requestCode, receiver, errorMessage);
                            }
                        });

            } else {
                if (!repository.checkCredential(changeCredentialsCommand)) {
                    sendFail(requestCode, receiver, String.format("{\"error\":{\"error_code\":%d,\"error_message\":\"Wrong password\"}}", ErrorItem.WRONG_PASSWORD));
                } else {
                    restClient.updateCredentials(changeCredentialsCommand)
                            .subscribe(response -> {
                                        repository.updateCredentials(changeCredentialsCommand);
                                        sendSuccess(requestCode, receiver);
                                    },
                                    (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
                }
            }
        }
    }

    public void getUrlUrgence(AppReceiver receiver, int requestCode) {
        restClient.getUrgenceResponseBaseCall()
                .subscribe(response -> {
                            sendSuccess(requestCode, receiver);
                        },
                        (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void ping_auth(AppReceiver receiver, int requestCode) {
        this.setPingState(true);
        restClient.ping_auth()
                .subscribe(response -> {
//                            Bundle bundle = new Bundle();
//                            bundle.putInt(UserResponse.NEW_NOTIFICATIONS, response.getNewNotificationCount());

                            repository.setNewNotificationCount(response.getNewNotificationCount());
                            repository.saveUser(response);
                            sendSuccess(requestCode, receiver);
                            // Check CGU validation
                            cguHandler.checkCguAgreement(
                                    response.user.uid, receiver, requestCode,
                                    () -> {
                                        repository.setNewNotificationCount(response.getNewNotificationCount());
                                        repository.saveUser(response);
                                    });

                        },
                        (errorMessage, errorCode) -> {
                            if (errorCode == ErrorItem.USER_IS_NOT_LOGIN && repository.getUserPassword() != null && repository.getCredential().user != null & retryLoginCount < 1) {
                                // Si error d'authentification on essaie la reconnexion
                                retryLoginCount++;
                                LoginUserCommand loginCmd = new LoginUserCommand(null, repository.getCredential().user.mail, repository.getUserPassword());
                                restClient.login(loginCmd);
                            } else {
                                retryLoginCount = 0;
                                sendFail(requestCode, receiver, errorMessage);
                            }
                        });
    }

    public void logout(AppReceiver receiver, int requestCode) {
        this.setPingState(true);
        restClient.logout()
                .subscribe(response -> {
                    this.setPingState(false);

                    // Delete push token on the server
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (!task.isSuccessful()) {
                                        Log.w("AsipMessagingService", "getInstanceId failed", task.getException());
                                    } else {
                                        // Get new Instance ID token
                                        String token = task.getResult().getToken();
                                        Log.d("AsipMessagingService", "Removing token: " + token);

                                        // Execute service
                                        ExecutionService.sendCommand(
                                                App.getAppContext(),
                                                receiver,
                                                new DelPushCredentialServiceCommand(token, repository.getUserUID()),
                                                ExecutionService.DEL_PUSH_CREDENTIAL);
                                    }

                                    // Logout
                                    netWorkState.logOut();
                                }
                            });

                    Bundle bundle = new Bundle();
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> {
                    this.setPingState(false);
                    sendFail(requestCode, receiver, errorMessage);
                });
    }

    public void reset(ResetCommand resetCommand, AppReceiver receiver, int requestCode) {
        restClient.reset(resetCommand)
                .subscribe(response -> sendSuccess(requestCode, receiver), (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getHistory(GetHistoryCommand getHistoryCommand, AppReceiver receiver, int requestCode) {
        if (!netWorkState.isLoggedIn()) {
            Cursor cursor = dataBaseHelper.getHistory(getHistoryCommand.getType());
            List<HistoryItem> items = new ArrayList<>();
            try {
                while (cursor.moveToNext()) {
                    HistoryItem historyItem = new HistoryItem();
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, historyItem);
                    items.add(historyItem);
                }
            } finally {
                cursor.close();
            }
            GetHistoryResponse response = new GetHistoryResponse();
            response.items = items;
            Bundle bundle = getBundle(BUNDLE_HISTORIES, response);
            bundle.putBoolean(BUNDLE_IS_FIRST_PAGE, getHistoryCommand.getPage_offset() == 0);
            sendSuccess(requestCode, receiver, bundle);
        } else {
            restClient.getHistory(getHistoryCommand)
                    .subscribe(response -> {
                        if (getHistoryCommand.getType() == HistoryType.search) {
                            response.fixAroundMe(context);
                        }
                        Bundle bundle = getBundle(BUNDLE_HISTORIES, response);
                        bundle.putBoolean(BUNDLE_IS_FIRST_PAGE, getHistoryCommand.getPage_offset() == 0);
                        sendSuccess(requestCode, receiver, bundle);
//                            List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
//                            for (HistoryItem historyItem : response.items) {
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(historyItem));
//                            }
//                            databaseHelper.bulkInsert(TablesContentProvider.HISTORY_CONTENT_URI, contentValuesForUserBulk);
                    }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        }
    }

    public void clearHistory(ClearHistoryCommand clearHistoryCommand, AppReceiver receiver, int requestCode) {
        restClient.clearHistory(clearHistoryCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_CLEAR_HISTORY, clearHistoryCommand);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getPopular(GetPopularCommand getPopularCommand, AppReceiver receiver, int requestCode) {
        restClient.getPopular(getPopularCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_GET_POPULAR_PHRASES, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getNotification(GetNotificationCommand getNotificationCommand, AppReceiver receiver, int requestCode) {
        restClient.getNotification(getNotificationCommand)
                .subscribe(response -> {
                    if (response.unread != null && response.unread.items != null && !response.unread.hasMore()) {
                        repository.setNewNotificationCount(response.unread.items.length);
                    }
                    Bundle bundle = getBundle(BUNDLE_NOTIFICATIONS, response);
                    bundle.putBoolean(BUNDLE_IS_FIRST_PAGE, getNotificationCommand.page_offset == 0);
                    sendSuccess(requestCode, receiver, bundle);
//                            databaseHelper.delete(TablesContentProvider.NOTIFICATION_CONTENT_URI);
//                            List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
//                            for (NotificationItem notificationItem : response.all.items) {
//                                notificationItem.setType(StatusNotification.all);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(notificationItem));
//                            }
//                            for (NotificationItem notificationItem : response.read.items) {
//                                notificationItem.setType(StatusNotification.read);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(notificationItem));
//                            }
//                            for (NotificationItem notificationItem : response.unread.items) {
//                                notificationItem.setType(StatusNotification.unread);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(notificationItem));
//                            }
//                            databaseHelper.bulkInsert(TablesContentProvider.NOTIFICATION_CONTENT_URI, contentValuesForUserBulk);
//                            sendSuccess(requestCode, receiver);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getFavorites(GetFavoritesCommand getFavoritesCommand, AppReceiver receiver, int requestCode) {
        restClient.getFavorites(getFavoritesCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_FAVORITES, response);
                    bundle.putBoolean(BUNDLE_IS_FIRST_PAGE, getFavoritesCommand.getPage_offset() == 0);
                    sendSuccess(requestCode, receiver, bundle);
//                            List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
//                            for (FavoritesItem favoritesItem : response.all.items) {
//                                favoritesItem.setType(StatusNotification.all);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(favoritesItem));
//                            }
//                            for (FavoritesItem favoritesItem : response.read.items) {
//                                favoritesItem.setType(StatusNotification.read);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(favoritesItem));
//                            }
//                            for (FavoritesItem favoritesItem : response.unread.items) {
//                                favoritesItem.setType(StatusNotification.unread);
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(favoritesItem));
//                            }
//                            databaseHelper.bulkInsert(TablesContentProvider.FAVORITES_CONTENT_URI, contentValuesForUserBulk);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getAutocompletedSearchPhrases(GetAutoCompleteSearchPhrasesCommand getAutoCompleteSearchPhrasesCommand, AppReceiver receiver, int requestCode) {
        restClient.getAutoCompleteSearchPhrases(getAutoCompleteSearchPhrasesCommand)
                .subscribe(response -> {
                    response.correctBrackets();
                    Bundle bundle = getBundle(BUNDLE_GET_AUTOCOMPLETE_SEARCH_PHRASE, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getAutoCompleteSearchLocations(GetAutoCompleteSearchLocationsCommand getAutoCompleteSearchLocationsCommand, AppReceiver receiver, int requestCode) {
        restClient.getAutoCompleteSearchLocations(getAutoCompleteSearchLocationsCommand)
                .subscribe(response -> {
                    response.setLocation(getAutoCompleteSearchLocationsCommand.text);
                    Bundle bundle = getBundle(BUNDLE_GET_AUTOCOMPLETE_SEARCH_LOCATION, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getRecentSearchPhrase(AppReceiver receiver, int requestCode) {
        if (netWorkState.isLoggedIn()) {
            restClient.getRecentSearchPhrase()
                    .subscribe(response -> {
                        Bundle bundle = getBundle(BUNDLE_GET_RECENT_PHRASES, response);
                        sendSuccess(requestCode, receiver, bundle);
                    }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        } else {
            Cursor cursor = dataBaseHelper.getRecentPhrases();
            List<SearchItem> items = new ArrayList<>();
            try {
                while (cursor.moveToNext()) {
                    SearchItem searchItem = new SearchItem();
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, searchItem);
                    items.add(searchItem);
                }
            } finally {
                cursor.close();
            }
            SearchResponse response = new SearchResponse();
            response.items = items;
            Bundle bundle = getBundle(BUNDLE_GET_RECENT_PHRASES, response);
            sendSuccess(requestCode, receiver, bundle);
        }
    }

    public void getRecentSearchLocation(AppReceiver receiver, int requestCode) {
        if (netWorkState.isLoggedIn()) {
            restClient.getRecentSearchLocation()
                    .subscribe(response -> {
//                            databaseHelper.delete(TablesContentProvider.RECENT_LOCATION_CONTENT_URI);
//                            List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
//                            for (RecentLocationItem recentLocationItem : response.items) {
//                                contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(recentLocationItem));
//                            }
//                            databaseHelper.bulkInsert(TablesContentProvider.RECENT_LOCATION_CONTENT_URI, contentValuesForUserBulk);
                        Bundle bundle = getBundle(BUNDLE_GET_RECENT_LOCATIONS, response);
                        sendSuccess(requestCode, receiver, bundle);
                    }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        } else {
            Cursor cursor = dataBaseHelper.getRecentLocations();
            List<RecentLocationItem> items = new ArrayList<>();
            try {
                while (cursor.moveToNext()) {
                    RecentLocationItem recentLocationItem = new RecentLocationItem();
                    BaseOrmLiteSqliteOpenHelper.fromCursor(cursor, recentLocationItem);
                    items.add(recentLocationItem);
                }
            } finally {
                cursor.close();
            }
            RecentLocationsResponse response = new RecentLocationsResponse();
            response.items = items;
            Bundle bundle = getBundle(BUNDLE_GET_RECENT_LOCATIONS, response);
            sendSuccess(requestCode, receiver, bundle);
        }
    }

    public void getQuickCards(GetQuickCardsCommand getQuickCardsCommand, AppReceiver receiver, int requestCode) {
        restClient.getQuickCardsAuthenticated(getQuickCardsCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_GET_QUICK_CARDS, response);
                    sendSuccess(requestCode, receiver, bundle);
                    List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
                    for (QuickCardsAnonymousItem quickCardsAnonymousItem : response.items) {
                        contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(quickCardsAnonymousItem));
                        // TODO because of we did not receive Favorite status
                        ExecutionService.sendCommand(context, receiver,
                                new GetAnchorsCommand(quickCardsAnonymousItem.getContentUri().getPath()), ExecutionService.ANCHORS_ACTION);
                    }
                    dataBaseHelper.bulkInsert(TablesContentProvider.CARDS_CONTENT_URI, contentValuesForUserBulk);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void search(SearchCommand searchCommand, AppReceiver receiver, int requestCode) {
        restClient.search(searchCommand)
                .subscribe(response -> {

                    final SearchType searchType = searchCommand.getType();
                    Bundle bundle = getBundle(SearchResponseHuge.BUNDLE_SEARCH, response);

                    tracker.addSearchInfo(searchCommand, response);
                    final IHistoryRecentUpdateHelper historyUpdateHelper = tracker.getHistoryUpdateHelper();
                    if (historyUpdateHelper.isActive()) {
                        final boolean areaActive = historyUpdateHelper.getBestSearchData().getResponse().isAreaActive();
                        response.setActive(true);
                    }

                    sendSuccess(requestCode, receiver, bundle);

                    if (searchCommand.isFirstRequest()) {
                        filtersHelper.setFilters(response.filters, searchType);
                    }

                    // move to HistoryUpdateHelper
//                    if (!response.isEmpty()) {
//                        if (!netWorkState.isLoggedIn()) {
//                            if (!TextUtils.isEmpty(searchCommand.getCityLocation())) {
//                                dataBaseHelper.addRecentLocation(RecentLocationItem.createLocal(searchCommand.getCityLocation()));
//                            }
//
//                            if (!TextUtils.isEmpty(searchCommand.getSearch())) {
//                                dataBaseHelper.addRecentPhrase(SearchItem.createLocal(searchCommand.getSearch()));
//                            }
//                        }
//                    }

                    List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
                    List<ContentValues> bmcValuesForUserBulk = new LinkedList<>();

                    for (SearchItemHuge searchItemHuge : response.items) {
                        searchItemHuge.setRequestType(searchType);
                        contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(searchItemHuge));
                    }
                    if (response.bmc != null) {
                        BmcItem bmcItem = new BmcItem(response.bmc);
                        bmcValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(bmcItem));
                    }
                    if (searchCommand.isFirstRequest()) {
                        dataBaseHelper.deleteHistory(TablesContentProvider.ADVERT_CONTENT_URI, searchType);
                    }

                    // save in database
                    dataBaseHelper.bulkInsert(TablesContentProvider.ADVERT_CONTENT_URI, contentValuesForUserBulk);
                    dataBaseHelper.bulkInsert(TablesContentProvider.BMC_ITEM_CONTENT_URI, bmcValuesForUserBulk);

                    if (searchCommand.getPage() == 0) {
                        searchRelatedToDataBase(response, searchType);
                        saveMessages(response, searchCommand.getType());
                    }

                }, (errorMessage, errorCode) -> {

                    final SearchType searchType = searchCommand.getType();
                    sendFail(requestCode, receiver, errorMessage);
                });
    }

    private void saveMessages(SearchResponseHuge response, SearchType searchType) {
        dataBaseHelper.deleteUnreadSearchMessages(searchType);
        if (response.messages != null) {
            List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
            Iterator entries = response.messages.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) entries.next();

                if (!dataBaseHelper.containsSearchMessage(entry.getKey(), searchType)) {
                    contentValuesForUserBulk.add(BaseOrmLiteSqliteOpenHelper.getContentValues(new SearchMessage(entry.getKey(), entry.getValue(), searchType)));
                }
            }
            dataBaseHelper.bulkInsert(TablesContentProvider.SEARCH_MESSAGE_CONTENT_URI, contentValuesForUserBulk);
        }
    }

    private void searchRelatedToDataBase(SearchResponseHuge response, SearchType searchType) {
        dataBaseHelper.deleteHistory(TablesContentProvider.RELATED_CONTENT_URI, Related.SEARCH_TYPE, searchType);
        dataBaseHelper.deleteHistory(TablesContentProvider.RELATED_ITEM_CONTENT_URI, RelatedItem.SEARCH_TYPE, searchType);
        dataBaseHelper.deleteHistory(TablesContentProvider.RELATED_FILTER_ITEM_CONTENT_URI, RelatedFilters.SEARCH_TYPE, searchType);
        for (Related related : response.related) {
            related.setType(searchType);
            final Uri insertUri = dataBaseHelper.insert(TablesContentProvider.RELATED_CONTENT_URI, BaseOrmLiteSqliteOpenHelper.getContentValues(related));
            final String id = insertUri.getLastPathSegment();
            related.setId(Long.parseLong(id));
            for (RelatedItem relatedItem : related.items) {
                relatedItem.setParendId(related);
                relatedItem.setType(searchType);
                final Uri insertUri2 = dataBaseHelper.insert(TablesContentProvider.RELATED_ITEM_CONTENT_URI, BaseOrmLiteSqliteOpenHelper.getContentValues(relatedItem));
                final String id2 = insertUri2.getLastPathSegment();
                relatedItem.setId(Long.parseLong(id2));
                relatedItem.filters.setParentId(relatedItem);
                relatedItem.filters.setSearchType(searchType);
                final Uri insertUri3 = dataBaseHelper.insert(TablesContentProvider.RELATED_FILTER_ITEM_CONTENT_URI, BaseOrmLiteSqliteOpenHelper.getContentValues(relatedItem.filters));
                final String id3 = insertUri3.getLastPathSegment();
                relatedItem.filters.setId(Long.parseLong(id3));
            }
        }
        Uri tt = Uri.parse(String.format("%s#%s", TablesContentProvider.RELATED_VIRTUAL_CONTENT_URI.toString(), searchType.name()));
        context.getContentResolver().notifyChange(tt/*TablesContentProvider.RELATED_VIRTUAL_CONTENT_URI*/, null);
    }

    public void getFilters(SearchType searchType, AppReceiver receiver, int requestCode) {
        SearchCommand searchCommand = new SearchCommand();
        searchCommand.setType(searchType);
        searchCommand.setItemsPerPage(0);
        restClient.search(searchCommand).subscribe(response -> {
            if (searchCommand.getFilters().isEmpty()) {
                filtersHelper.setFilters(response.filters, searchType);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(BUNDLE_DATA, response.filters);
            sendSuccess(requestCode, receiver, bundle);
        }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }


    private static Map<Long, AlertItem> sortByComparator(Map<Long, AlertItem> unsortMap, final boolean order) {
        List<Map.Entry<Long, AlertItem>> list = new LinkedList<>(unsortMap.entrySet());
        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<Long, AlertItem>>() {
            public int compare(Map.Entry<Long, AlertItem> o1,
                               Map.Entry<Long, AlertItem> o2) {
                return order ? o1.getValue().compareTo(o2.getValue()) :
                        o2.getValue().compareTo(o1.getValue());
            }
        });
        // Maintaining insertion order with the help of LinkedList
        Map<Long, AlertItem> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<Long, AlertItem> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public void alerts(AlertsCommand alertsCommand, AppReceiver receiver, int requestCode) {
        if (alertsCommand.isAlertsForQuery()) {
            restClient.alerts(alertsCommand)
                    .subscribe(response -> {
                        //sendSuccess(requestCode, receiver);
                        Bundle bundle = getBundle(BUNDLE_ALERT_ITEMS, response);
                        sendSuccess(requestCode, receiver, bundle);
                        boolean soundAlert = false;
                        Map<Long, AlertItem> alerts = new HashMap<Long, AlertItem>();
                        for (AlertItem alertItem : response.items) {
                            alerts.put(alertItem.getId(), alertItem);
                        }

                        List<Map.Entry<Long, AlertItem>> listSortForNotification = new LinkedList<>(alerts.entrySet());
                        Collections.sort(listSortForNotification, (o1, o2) -> o1.getValue().compareTo(o2.getValue()));

                        removeExistsAlertsFromApi(alerts);

                        List<ContentValues> contentValuesForUserBulk = new LinkedList<>();
                        for (AlertItem alertItem : response.items) {
                            ContentValues contentValues = BaseOrmLiteSqliteOpenHelper.getContentValues(alertItem);
                            contentValues.put(AlertItem.PUSH_READ, "true");
                            contentValuesForUserBulk.add(contentValues);
                            if (!soundAlert && alertItem.isSoundAlert()) {
                                soundAlert = true;
                            }
                        }
                        dataBaseHelper.deleteHistory(TablesContentProvider.ALERTS_CONTENT_URI);
                        dataBaseHelper.bulkInsert(TablesContentProvider.ALERTS_CONTENT_URI, contentValuesForUserBulk);
                    }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        } else {
            dataBaseHelper.deleteHistory(TablesContentProvider.ALERTS_CONTENT_URI);
        }
    }

    private void removeExistsAlertsFromApi(Map<Long, AlertItem> alerts) {
        final Cursor cursor = dataBaseHelper.getAlerts(true);
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    final long id = cursor.getLong(cursor.getColumnIndex(AlertItem.ID_DB));
                    if (alerts.containsKey(id)) {
                        boolean bNeedRemove = false;
                        if (!TextUtils.equals(cursor.getString(cursor.getColumnIndex(AlertItem.PUSH_READ)), "false")) {
                            alerts.get(id).setPushRead();
                            bNeedRemove = true;
                        }
                        if (!TextUtils.equals(cursor.getString(cursor.getColumnIndex(AlertItem.URGENT_READ)), "false")) {
                            alerts.get(id).setUrgentRead();
                            bNeedRemove = true;
                        }
                        if (bNeedRemove) {
                            alerts.remove(id);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }
    }

    public void clearRecentLocations(AppReceiver receiver, int requestCode) {
        if (!netWorkState.isLoggedIn()) {
            dataBaseHelper.clearRecentLocations();
            sendSuccess(requestCode, receiver);
        } else {
            restClient.clearRecentLocations()
                    .subscribe(response -> sendSuccess(requestCode, receiver),
                            (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        }
    }

    public void clearRecentSearchPhrases(AppReceiver receiver, int requestCode) {
        if (!netWorkState.isLoggedIn()) {
            dataBaseHelper.clearRecentPhrases();
            sendSuccess(requestCode, receiver);
        } else {
            restClient.clearRecentSearchPhrases()
                    .subscribe(response -> sendSuccess(requestCode, receiver),
                            (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        }
    }

    public void removeRecentSearchItem(RemoveSearchItemCommand command, AppReceiver receiver, int requestCode, int itemType) {
        if (!netWorkState.isLoggedIn()) {
            if (itemType == RemoveSearchItemCommand.ITEM_LOCATION) {
                for (int i = 0; i < command.getSids().size(); i++) {
                    dataBaseHelper.removeRecentLocation(command.getSids().get(i));
                }
            } else if (itemType == RemoveSearchItemCommand.ITEM_PHRASE) {
                for (int i = 0; i < command.getSids().size(); i++) {
                    dataBaseHelper.removeRecentPhrase(command.getSids().get(i));
                }
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(BUNDLE_REMOVE_SEARCH_IDS, new ArrayList<>(command.getSids()));
            sendSuccess(requestCode, receiver, bundle);
        } else {
            restClient.removeRecentSearchItem(command)
                    .subscribe(response -> {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(BUNDLE_REMOVE_SEARCH_IDS, new ArrayList<>(command.getSids()));
                        sendSuccess(requestCode, receiver, bundle);
                    }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
        }
    }

    public void changeNotificationStatus(UpdateNotificationStatusCommand command, AppReceiver receiver, int requestCode) {
        restClient.changeNotificationStatus(command)
                .subscribe(response -> {
                    if (command.getAction().equals(UpdateNotificationStatusCommand.ACTION_DELETE)) {
                        // just one element
                        for (long id : command.getNids()) {
                            dataBaseHelper.removeNotification(id);
                        }
                    } else if (command.getAction().equals(UpdateNotificationStatusCommand.ACTION_MARK_AS_READ)) {
                        for (long id : command.getNids()) {
                            dataBaseHelper.markNotificationAsRead(id);
                        }
                    }
                    Bundle bundle = getBundle(BUNDLE_UPDATE_NOTIFICATION, command);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void addOrRemove(AddOrRemoveFavoriteCommand command, AppReceiver receiver, int requestCode) {
        restClient.addRemoveFavorite(command)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_ADD_REMOVE_FAVORITE, command);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void addInHistory(AddInHistoryCommand command, AppReceiver receiver, int requestCode) {
        restClient.addInHistory(command)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_ADD_IN_HISTORY, command);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    private Bundle getBundle(String key, Parcelable parcelable) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(key, parcelable);
        return bundle;
    }

    public void getTaxonomyVocabularyTerms(TaxonomyVocabularyTermsCommand taxonomyVocabularyTermsCommand, AppReceiver receiver, int requestCode) {
        restClient.getTaxonomyVocabularyTerms(taxonomyVocabularyTermsCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(TAXONOMY_VOCABULARY_TERMS_RESPONSE, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void cities(CitiesCommand citiesCommand, AppReceiver receiver, int requestCode) {
        restClient.cities(citiesCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(CITIES_RESPONSE, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void loginUser(LoginUserCommand loginUserCommand, final AppReceiver receiver, final int requestCode) {
        restClient.login(loginUserCommand)
                .subscribe(response -> {
                    if (loginUserCommand.isFirstLogin()) {
                        sendEmailConfirmation(receiver, ExecutionService.SEND_EMAIL_CONFIRMATION_ACTION);
                    }
                    sendSuccess(requestCode, receiver);

                    // Check CGU validation
                    cguHandler.checkCguAgreement(
                            response.user.uid, receiver, requestCode,
                            () -> repository.saveCredential(response, loginUserCommand.getPassword())
                    );
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void loginUserSso(LoginUserSsoCommand command, AppReceiver receiver, int requestCode) {
        restClient.loginSso(command).subscribe(response -> {
            repository.saveCredential(response, SharedPreferencesHelper.RESET);
            if (command.isFirstLogin()) {
                sendEmailConfirmation(receiver, ExecutionService.SEND_EMAIL_CONFIRMATION_ACTION);
            }
            sendSuccess(requestCode, receiver);
        }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getAnchors(GetAnchorsCommand anchorsCommand, AppReceiver receiver, int requestCode) {
        restClient.getAnchors(anchorsCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(ANCHOR_RESPONSE, response);
                    sendSuccess(requestCode, receiver, bundle);
                    dataBaseHelper.updateFavoriteQuickCard(response.getId(), response.getFavorite());
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void getForm(GetFormCommand formCommand, AppReceiver receiver, int requestCode) {
        restClient.getForm(formCommand)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_GET_FORM, response);
                    bundle.putLong(START_TIME, System.currentTimeMillis());
                    bundle.putParcelable(BUNDLE_GET_FORM_CMD, formCommand);
                    sendSuccess(requestCode, receiver, bundle);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void submitFormAnonymous(SubmitFeedbackFormCommand command, AppReceiver receiver, int requestCode) {
        restClient.submitFormAnonymous(getFeedbackFormBody(command))
                .subscribe(response -> {
                    sendSuccess(requestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void submitForm(SubmitFeedbackFormCommand command, AppReceiver receiver, int requestCode) {
        restClient.submitForm(getFeedbackFormBody(command))
                .subscribe(response -> {
                    sendSuccess(requestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    private Map<String, Object> getFeedbackFormBody(SubmitFeedbackFormCommand command) {
        Map<String, Object> body = new HashMap<>();
        body.put(BaseFeedbackCommandCommand.FORM_NAME_FIELD, command.getFormName());
        body.put(BaseFeedbackCommandCommand.SECRET_FIELD, command.getSecret());

        Map<String, Object> fieldValues = new HashMap<>();
        fieldValues.put(BaseFeedbackCommandCommand.FIELD_URL, command.getValue().getUrl());
        fieldValues.put("field_helpful", command.getValue().getHelpful());

        if (command.getValue().getHelplessReasonFieldName() != null) {
            fieldValues.put(command.getValue().getHelplessReasonFieldName(), command.getValue().getReason());
        }
        fieldValues.put("field_other_helpless_reason", command.getValue().getOtherReason());

        body.put(BaseFeedbackCommandCommand.FIELD_VALUES_FIELD, fieldValues);
        return body;
    }

    public void addContactFormFeedback(AddContactFormFeedbackCommand command, AppReceiver receiver, int reqiestCode) {
        restClient.addContactFormFeedback(command)
                .subscribe(response -> {
                    sendSuccess(reqiestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(reqiestCode, receiver, errorMessage)));
    }

    public void addContactFormFeedbackAnonymous(AddContactFormFeedbackCommand command, AppReceiver receiver, int reqiestCode) {
        restClient.addContactFormFeedbackAnonymous(command)
                .subscribe(response -> {
                    sendSuccess(reqiestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(reqiestCode, receiver, errorMessage)));
    }

    public void ping_not_auth(AppReceiver receiver, int requestCode) {
        restClient.ping_not_auth()
                .subscribe(response -> sendSuccess(requestCode, receiver),
                        (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void sendEmailConfirmation(AppReceiver receiver, int requestCode) {
        restClient.sendEmailConfirmation()
                .subscribe(response -> sendSuccess(requestCode, receiver),
                        (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void updatePersonalInformation(UpdatePersonalInformationCommand command,
                                          AppReceiver appReceiver, int requestCode) {
        restClient.updatePersonalInformation(command).subscribe(response -> {
            repository.saveCredential(response, SharedPreferencesHelper.NO_ACTION);
            sendSuccess(requestCode, appReceiver);
        }, (errorMessage, errorCode) -> sendFail(requestCode, appReceiver, errorMessage));
    }

    public void getSsoInfo(GetSsoInfoCommand command, AppReceiver appReceiver, int requestCode) {
        restClient.getSsoInfo(command).subscribe(response -> {
            response.addCredentials(command.getEmail(), command.getAccessToken());
            Bundle bundle = new Bundle();
            bundle.putParcelable(BUNDLE_DATA, response);
            sendSuccess(requestCode, appReceiver, bundle);
        }, (errorMessage, errorCode) -> sendFail(requestCode, appReceiver, errorMessage));
    }

    public void updatePersonalize(UpdatePersonalizeCommand command, AppReceiver receiver, int requestCode) {
        restClient.updatePersonalize(command).subscribe(response -> {
            repository.saveCredential(response, SharedPreferencesHelper.NO_ACTION);
            sendSuccess(requestCode, receiver);
        }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void cancelAccount(AppReceiver receiver, int requestCode) {
        restClient.cancelAccount()
                .subscribe(response -> {
                    netWorkState.logOut();
                    Bundle bundle = new Bundle();
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void sendSubmitForm(BaseFeedbackCommandCommand command, AppReceiver receiver, int requestCode) {
        restClient.sendSubmitForm(command)
                .subscribe(response -> {
                    sendSuccess(requestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void setPushCredential(SetPushCredentialServiceCommand serviceCommand, final AppReceiver receiver, final int requestCode) {
        restClient.setPushCredential(serviceCommand)
                .subscribe(response -> {
                    sendSuccess(requestCode, receiver);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void delPushCredential(DelPushCredentialServiceCommand serviceCommand, final AppReceiver receiver, final int requestCode) {
        restClient.delPushCredential(serviceCommand)
                .subscribe(response -> {
                    sendSuccess(requestCode, receiver);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void sendSubmitFormA(BaseFeedbackCommandCommand command, AppReceiver receiver, int requestCode) {
        restClient.sendSubmitFormA(command)
                .subscribe(response -> {

                    sendSuccess(requestCode, receiver);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void getConfiguration(GetConfigurationCommand command, AppReceiver receiver, int requestCode) {
        restClient.getConfiguration(command)
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_CONFIGURATION, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void sgpLocationComplete(SgpLocationCompleteCommand command, AppReceiver receiver, int requestCode) {
        restClient.sgplocationComplete(command.getLoc(), command.getStrict())
                .subscribe(response -> {
                    Bundle bundle = getBundle(BUNDLE_SGP_LOCATION_COMPLETE, response);
                    sendSuccess(requestCode, receiver, getBundle(BUNDLE_SGP_LOCATION_COMPLETE, response));
                }, ((errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage)));
    }

    public void addToHistory(AddToHistoryCommand addToHistoryCommand, AppReceiver receiver, int requestCode) {
        restClient.addToHistory(addToHistoryCommand)
                .subscribe(response -> {
//                    Bundle bundle = getBundle(CITIES_RESPONSE, response);
                    sendSuccess(requestCode, receiver);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void checkPassword(CheckPasswordCommand checkPasswordCommand, AppReceiver receiver, int requestCode) {
        restClient.checkPassword(checkPasswordCommand)
                .subscribe(response -> {
//                    Bundle bundle = getBundle(CITIES_RESPONSE, response);
                    sendSuccess(requestCode, receiver);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

    public void sendEmergency(EmergencyCommand emergencyCommand, AppReceiver receiver, int requestCode) {
        restClient.sendEmergency(emergencyCommand)
                .subscribe(response -> {
                    Bundle bundle = null;//getBundle(CITIES_RESPONSE, response);
                    sendSuccess(requestCode, receiver, bundle);
                }, (errorMessage, errorCode) -> sendFail(requestCode, receiver, errorMessage));
    }

}
