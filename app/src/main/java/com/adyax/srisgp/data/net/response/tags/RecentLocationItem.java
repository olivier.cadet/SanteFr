package com.adyax.srisgp.data.net.response.tags;

import android.text.format.DateUtils;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by anton.kobylianskiy on 9/27/16.
 */

@DatabaseTable(tableName = "recent_location")
public class RecentLocationItem {

    public static final String ID = "id";
    public static final String LOCATION = "location";

    public RecentLocationItem() {

    }

    public static RecentLocationItem createLocal(String location) {
        final RecentLocationItem recentLocationItem = new RecentLocationItem();
        recentLocationItem.id = System.currentTimeMillis();
        recentLocationItem.location = location;
        return recentLocationItem;
    }

    @SerializedName(ID)
    @DatabaseField(columnName = ID, unique = true)
    public long id;

    @SerializedName(LOCATION)
    @DatabaseField(columnName = LOCATION, unique = true)
    public String location;

    public RecentLocationItem(long id, String location) {
        this.id = id;
        this.location = location;
    }
}
