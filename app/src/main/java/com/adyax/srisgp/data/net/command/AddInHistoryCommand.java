package com.adyax.srisgp.data.net.command;

import android.app.Service;
import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kobylianskiy on 3/29/17.
 */

public class AddInHistoryCommand extends ServiceCommand {
    private List<Long> nids;
    private String referrer = "";

    public AddInHistoryCommand(List<Long> nids) {
        this.nids = nids;
    }

    public AddInHistoryCommand(Long id) {
        this.nids = new ArrayList<>();
        this.nids.add(id);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.addInHistory(this, receiver, requestCode);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.nids);
    }

    protected AddInHistoryCommand(Parcel in) {
        this.nids = new ArrayList<Long>();
        in.readList(this.nids, Long.class.getClassLoader());
    }

    public static final Creator<AddInHistoryCommand> CREATOR = new Creator<AddInHistoryCommand>() {
        @Override
        public AddInHistoryCommand createFromParcel(Parcel source) {
            return new AddInHistoryCommand(source);
        }

        @Override
        public AddInHistoryCommand[] newArray(int size) {
            return new AddInHistoryCommand[size];
        }
    };
}
