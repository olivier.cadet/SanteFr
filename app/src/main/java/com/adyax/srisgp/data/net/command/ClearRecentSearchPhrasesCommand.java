package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class ClearRecentSearchPhrasesCommand extends ServiceCommand {

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.clearRecentSearchPhrases(receiver, requestCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public ClearRecentSearchPhrasesCommand() {
    }

    protected ClearRecentSearchPhrasesCommand(Parcel in) {
    }

    public static final Creator<ClearRecentSearchPhrasesCommand> CREATOR = new Creator<ClearRecentSearchPhrasesCommand>() {
        @Override
        public ClearRecentSearchPhrasesCommand createFromParcel(Parcel source) {
            return new ClearRecentSearchPhrasesCommand(source);
        }

        @Override
        public ClearRecentSearchPhrasesCommand[] newArray(int size) {
            return new ClearRecentSearchPhrasesCommand[size];
        }
    };
}
