package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class GetAutoCompleteSearchLocationsCommand extends ServiceCommand {

    public String text;

    public GetAutoCompleteSearchLocationsCommand(Parcel in) {
        text = in.readString();
    }

    public static final Creator<GetAutoCompleteSearchLocationsCommand> CREATOR = new Creator<GetAutoCompleteSearchLocationsCommand>() {
        @Override
        public GetAutoCompleteSearchLocationsCommand createFromParcel(Parcel in) {
            return new GetAutoCompleteSearchLocationsCommand(in);
        }

        @Override
        public GetAutoCompleteSearchLocationsCommand[] newArray(int size) {
            return new GetAutoCompleteSearchLocationsCommand[size];
        }
    };

    public GetAutoCompleteSearchLocationsCommand(String text) {
        this.text = text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getAutoCompleteSearchLocations(this, receiver, requestCode);
    }

}
