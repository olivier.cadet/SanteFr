package com.adyax.srisgp.data.net.response.tags.xiti;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SUVOROV on 9/19/16.
 */
public class Xiti implements Parcelable {

    public static final String XITI = "xiti";

    public static final String NAME = "name";
    public static final String CHAPTER1 = "chapter1";
    public static final String CHAPTER2 = "chapter2";
    public static final String CHAPTER3 = "chapter3";
    public static final String LEVEL2_PAGE = "level2_page";
    public static final String LEVEL2_CLICK = "level2_click";
    public static final String LEVEL2 = "level2";
    public static final String TYPE = "type";
    public static final String NAME_CLICK = "name_click";
    public static final String CHAPTER3_PAGE = "chapter3_page";
    public static final String CHAPTER3_CLICK = "chapter3_click";

    public String getName() {
        return name;
    }

    @SerializedName(NAME)
    public String name;

    @SerializedName(CHAPTER1)
    public String chapter1;

    @SerializedName(CHAPTER2)
    public String chapter2;

    @SerializedName(CHAPTER3)
    public String chapter3;

    @SerializedName(LEVEL2_PAGE)
    public int level2_page;

    @SerializedName(LEVEL2_CLICK)
    public int level2_click;

    @SerializedName(LEVEL2)
    public int level2;

    @SerializedName(TYPE)
    public XitiType type;

    @SerializedName(NAME_CLICK)
    public String name_click;

    private TrackerLevel trackerLevel;

    private ClickType clickType;

    @SerializedName(CHAPTER3_PAGE)
    public String chapter3_page;

    @SerializedName(CHAPTER3_CLICK)
    public String chapter3_click;

    public Xiti() {
        this.trackerLevel = TrackerLevel.UNKNOWN;
        clickType = ClickType.UNKNOWN;
        type = XitiType.UNKNOWN;
    }

    public Xiti(TrackerLevel trackerLevel) {
        this.trackerLevel = trackerLevel;
        clickType = ClickType.UNKNOWN;
        type = XitiType.UNKNOWN;
    }

    protected Xiti(Parcel in) {
        name = in.readString();
        chapter1 = in.readString();
        chapter2 = in.readString();
        chapter3 = in.readString();
        level2_page = in.readInt();
        level2_click = in.readInt();
        type = XitiType.valueOf(in.readString());
        trackerLevel = TrackerLevel.valueOf(in.readString());
        clickType = ClickType.valueOf(in.readString());
        level2 = in.readInt();
        chapter3_page = in.readString();
        chapter3_click = in.readString();
    }

    public static final Creator<Xiti> CREATOR = new Creator<Xiti>() {
        @Override
        public Xiti createFromParcel(Parcel in) {
            return new Xiti(in);
        }

        @Override
        public Xiti[] newArray(int size) {
            return new Xiti[size];
        }
    };

    public String getChapter1() {
        return chapter1;
    }

    public String getChapter2() {
        return chapter2;
    }

    public String getChapter3() {
//        if (chapter3_click != null) {
//            return chapter3_click;
//        }
        return chapter3;
    }

    public String getChapter3Click() {
        if (chapter3_click != null) {
            return chapter3_click;
        }
        return chapter3;
    }

    public String getChapter3Page() {
        if (chapter3_page != null) {
            return chapter3_page;
        }
        return chapter3;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(chapter1);
        parcel.writeString(chapter2);
        parcel.writeString(chapter3);
        parcel.writeInt(level2_page);
        parcel.writeInt(level2_click);
        final String name = type == null ? null : type.name();
        if (name != null) {
            parcel.writeString(name);
        } else {
            parcel.writeString(XitiType.UNKNOWN.name());
        }
        parcel.writeString(trackerLevel.name());
        parcel.writeString(clickType.name());
        parcel.writeInt(level2);
        parcel.writeString(chapter3_page);
        parcel.writeString(chapter3_click);
    }

    public Xiti setChapter1(String chapter1) {
        this.chapter1 = chapter1;
        return this;
    }

    public Xiti setChapter2(String chapter2) {
        this.chapter2 = chapter2;
        return this;
    }

    public Xiti setChapter3(String chapter3) {
        this.chapter3 = chapter3;
        return this;
    }

    public Xiti setTitle(String name) {
        this.name = name;
        return this;
    }

    public String getTitle() {
        switch (clickType) {
            case UNKNOWN:
                return name;
            default:
                return clickType.name();
        }
    }

    public TrackerLevel getScreenTrackerLevel() {
        try {
            return TrackerLevel.valueOf(level2_page);
        } catch (Exception e) {

        }
        return getAppTrackerLevel();
    }

    public TrackerLevel getClickTrackerLevel() {
        try {
            return TrackerLevel.valueOf(level2_click);
        } catch (Exception e) {

        }
        return getAppTrackerLevel();
    }

    public TrackerLevel getAppTrackerLevel() {
        return trackerLevel != null ? trackerLevel : TrackerLevel.UNKNOWN;
    }

    public Xiti setTrackerLevel(TrackerLevel trackLevel) {
        this.trackerLevel = trackLevel;
        return this;
    }

    public int getLevel2() {
        // it case for anchorResponse
        if (level2_page == 0 && level2_click == 0) {
            return level2;
        }
        switch (clickType) {
            case UNKNOWN:
                return level2_page;
            default:
                return level2_click;
        }
//        return getTrackerLevel().getValue();
    }

    public Xiti setClickType(ClickType clickType) {
        this.clickType = clickType;
        return this;
    }

    public XitiType getType() {
        return type;
    }

    public Xiti setType(XitiType type) {
        this.type = type;
        return this;
    }

    public ClickType getClickType() {
        return clickType;
    }

    public Xiti getClone() {
        final Gson gson = new Gson();
        return gson.fromJson(gson.toJson(this), Xiti.class);
    }

    public Xiti setLevel2Click(TrackerLevel trackerLevel) {
        level2_click = trackerLevel.getValue();
        return this;
    }

    public Xiti updateTrackLevel2() {
        level2_click = getAppTrackerLevel().getValue();
        level2_page = level2_click;
        return this;
    }
}
