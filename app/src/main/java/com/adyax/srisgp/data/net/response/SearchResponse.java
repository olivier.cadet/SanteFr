package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.SearchItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class SearchResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public List<SearchItem> items = new ArrayList<>();

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.items);
    }

    public SearchResponse() {
    }

    protected SearchResponse(Parcel in) {
        this.items = new ArrayList<SearchItem>();
        in.readList(this.items, SearchItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<SearchResponse> CREATOR = new Parcelable.Creator<SearchResponse>() {
        @Override
        public SearchResponse createFromParcel(Parcel source) {
            return new SearchResponse(source);
        }

        @Override
        public SearchResponse[] newArray(int size) {
            return new SearchResponse[size];
        }
    };
}
