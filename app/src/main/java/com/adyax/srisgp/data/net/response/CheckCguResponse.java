package com.adyax.srisgp.data.net.response;

import com.adyax.srisgp.data.net.response.tags.User;
import com.google.gson.annotations.SerializedName;

public class CheckCguResponse extends BaseResponse {
    public static final String CGUVALUE = "cgu";

    @SerializedName(CGUVALUE)
    public Boolean toValidate;
}
