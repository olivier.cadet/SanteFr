package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class QuestionnaireItem implements Parcelable {
    @SerializedName("bgBlock")
    public String bgBlock;

    @SerializedName("txtBlock")
    public String txtBlock;

    @SerializedName("labelBlock")
    public String labelBlock;

    @SerializedName("link")
    public String link;

    protected QuestionnaireItem(Parcel in) {
        bgBlock = in.readString();
        txtBlock = in.readString();
        labelBlock = in.readString();
        link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bgBlock);
        dest.writeString(txtBlock);
        dest.writeString(labelBlock);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QuestionnaireItem> CREATOR = new Creator<QuestionnaireItem>() {
        @Override
        public QuestionnaireItem createFromParcel(Parcel in) {
            return new QuestionnaireItem(in);
        }

        @Override
        public QuestionnaireItem[] newArray(int size) {
            return new QuestionnaireItem[size];
        }
    };
}
