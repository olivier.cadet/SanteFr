package com.adyax.srisgp.data.net.response;

import com.adyax.srisgp.data.net.response.tags.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

public class RegisterUserResponse extends BaseResponse {

    public static final String SESSID = "sessid";
    public static final String SESSION_NAME = "session_name";
    public static final String TOKEN = "token";
    public static final String USER = "user";

    public String getSessid() {
        return sessid;
    }

    public String getSession_name() {
        return session_name;
    }

    public String getToken() {
        return token;
    }

    @SerializedName(SESSID)
    public String sessid;

    @SerializedName(SESSION_NAME)
    public String session_name;

    @SerializedName(TOKEN)
    public String token;

    @SerializedName(USER)
    public User user;

    public void setSessid(String sessid) {
        this.sessid = sessid;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setSession_name(String session_name) {
        this.session_name = session_name;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isEmpty() {
        return !(token!=null&& session_name!=null&& sessid!=null&& !token.isEmpty()&& !session_name.isEmpty()&& !sessid.isEmpty());
    }

    public User getUser() {
        return user;
    }
}
