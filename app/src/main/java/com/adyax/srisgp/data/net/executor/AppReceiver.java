package com.adyax.srisgp.data.net.executor;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.StackedResultReceiver;

import androidx.annotation.NonNull;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class AppReceiver extends StackedResultReceiver<AppReceiver.AppReceiverCallback> {

    private static final int CODE_SUCCESS = 0;
    private static final int CODE_FAIL = 1;
    private static final int CODE_MESSAGE = 2;

    private static final String KEY_REQUEST_CODE = "REQUEST_CODE";

    public static final String PROGRESS_INT_KEY = "progress";

    public AppReceiver() {
        super();
    }

    protected AppReceiver(Parcel parcel) {
        super(parcel);
    }

    @Override
    protected void onHandleResult(@NonNull AppReceiverCallback listener, int resultCode, @NonNull Bundle resultData) {
        int requestCode = resultData.getInt(KEY_REQUEST_CODE);
        switch (resultCode) {
            case CODE_SUCCESS:
                listener.onSuccess(requestCode, resultData);
                break;

            case CODE_FAIL:
                ErrorMessage errorMessage =
                        new ErrorMessage(resultData.getString(CommandExecutor.ERROR_MESSAGE_CODE));
                if (resultData.containsKey(CommandExecutor.ERROR_TITLE_CODE)) {
                    errorMessage.setTitle(resultData.getString(CommandExecutor.ERROR_TITLE_CODE));
                }

                listener.onFail(requestCode, errorMessage);
                break;

            case CODE_MESSAGE:
                listener.onMessage(requestCode, resultData);
                break;
        }
    }

    public void sendProgress(int requestCode, int progress) {
        Bundle bundle = new Bundle();
        bundle.putInt(PROGRESS_INT_KEY, progress);
        sendMessage(requestCode, bundle);
    }

    public void sendSuccess(int requestCode, Bundle bundle) {
        sendBundle(CODE_SUCCESS, requestCode, bundle);
    }

    public void sendFail(int requestCode, Bundle bundle) {
        sendBundle(CODE_FAIL, requestCode, bundle);
    }

    public void sendMessage(int requestCode, Bundle bundle) {
        sendBundle(CODE_MESSAGE, requestCode, bundle);
    }

    private void sendBundle(int resultCode, int requestCode, Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            bundle = new Bundle();
        }
        bundle.putInt(KEY_REQUEST_CODE, requestCode);
        send(resultCode, bundle);
    }


    public static Parcelable.Creator<AppReceiver> CREATOR = new Parcelable.Creator<AppReceiver>() {
        public AppReceiver createFromParcel(Parcel parcel) {
            return new AppReceiver(parcel);
        }

        public AppReceiver[] newArray(int size) {
            return new AppReceiver[size];
        }
    };


    public interface AppReceiverCallback {
        void onSuccess(int requestCode, Bundle data);

        void onFail(int requestCode, ErrorMessage errorMessage);

        void onMessage(int requestCode, Bundle data);
    }

    public static class ErrorMessage {
        private String title, errorMessage;

        public ErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

//        public AlertDialogFragment.Builder getAlertBuilder() {
//            return new AlertDialogFragment.Builder(errorMessage)
//                    .setTitle(title);
//        }
    }
}
