package com.adyax.srisgp.data.net;

import android.os.StrictMode;
import android.util.Base64;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.di.modules.NetworkModule;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by NW, 2019-03-12
 * This class allow to read data from URL.
 * <p>
 * Example codes from : https://www.dev2qa.com/android-httpurlconnection-example/
 * https://www.jsontest.com/
 */

public class UrlUtility {

    private String url;
    private String method = "GET";
    private String data = null;
    private String basicAuth = null;
    private Exception lastException = null;

    public UrlUtility(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    public String getData() {
        return data;
    }

    public Exception getLastException() {
        return lastException;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setBasicAuth(String basicAuth) {
        try{
            this.basicAuth = NetworkModule.BASIC + android.util.Base64.encodeToString(BuildConfig.CREDENTIAL_SERVER.getBytes(), Base64.NO_WRAP);
        }catch (Exception e){
        }
    }

    /**
     * Set data send method.
     *
     * @param method GET or POST, default is GET
     */
    public void setMethod(String method) {
        // @TODO : For other methods if necessary. //  [OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, PATCH]
        if (method == "GET" || method == "POST") {
            this.method = method;
        }
    }

    /**
     * Add data to send.
     * - Data string must like : user=jerry&pasword=666666
     *
     * @param key
     * @param value
     */
    public void addData(String key, String value) {
        this.data = data;
        if (this.data == null) {
            this.data = "";
        } else {
            this.data += "?";
        }
        this.data += "?" + key + "=" + value;
    }

    /**
     * Read/Get string from HTTP.
     */
    public String readUrlString() {

        String resultStr = null;
        try {

            // Because the DEV environment has a self signed certificate.
            if(BuildConfig.FLAVOR.equals("dev")){
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
                };

                // Install the all-trusting trust manager
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };
                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            }

            URLConnection urlConn = null;
            BufferedReader bufferedReader = null;

            // Open connection on strict mode, Otherwise "android os network on main thread exception".
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL urlObj;

            // @TODO : Unify data catch and send methods system (GET/POST ....).
            if (this.method == "GET" && this.data != null) {
                // Add data for GET
                urlObj = new URL(this.url + "?" + this.data);
            } else {
                urlObj = new URL(this.url);
            }

            urlConn = urlObj.openConnection();
            // Set basic Auth
            if(this.basicAuth != null){
                urlConn.setRequestProperty ("Authorization", this.basicAuth);
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            if (this.method == "POST") {
                httpConn.setRequestMethod("POST");
                // Add data string.
                if (this.data != null) {
                    OutputStream outputStream = httpConn.getOutputStream();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
                    BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                    bufferedWriter.write(this.data);
                }

            } else if (this.method == "GET") {
                httpConn.setRequestMethod("GET");
            }

            //bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
            bufferedReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
            resultStr = stringBuffer.toString();

        } catch (Exception ex) {
            lastException = ex;
        }
        return resultStr;
    }

    /**
     * Read/Get Json from HTTP.
     */
    public JSONObject readUrlJson() {
        JSONObject json = null;
        try {
            String resultStr = readUrlString();
            json = new JSONObject(resultStr);
        } catch (Exception ex) {
            lastException = ex;
        }
        return json;
    }
}
