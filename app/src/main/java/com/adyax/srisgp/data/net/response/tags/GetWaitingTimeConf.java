package com.adyax.srisgp.data.net.response.tags;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GetWaitingTimeConf implements Parcelable {
    @SerializedName("button_text")
    public String button_text;

    @SerializedName("text_no_feeling")
    public String text_no_feeling;

    @SerializedName("text_thank")
    public String text_thank;

    @SerializedName("distance_max")
    public int distance_max;

    @SerializedName("delay_min")
    public int delay_min;

    @SerializedName("select_vote")
    public String[] select_vote;

    protected GetWaitingTimeConf(Parcel in) {
        button_text = in.readString();
        text_no_feeling = in.readString();
        text_thank = in.readString();
        distance_max = in.readInt();
        delay_min = in.readInt();
        select_vote = in.createStringArray();
    }

    public static final Creator<GetWaitingTimeConf> CREATOR = new Creator<GetWaitingTimeConf>() {
        @Override
        public GetWaitingTimeConf createFromParcel(Parcel in) {
            return new GetWaitingTimeConf(in);
        }

        @Override
        public GetWaitingTimeConf[] newArray(int size) {
            return new GetWaitingTimeConf[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(button_text);
        dest.writeString(text_no_feeling);
        dest.writeString(text_thank);
        dest.writeInt(distance_max);
        dest.writeInt(delay_min);
        dest.writeStringArray(select_vote);
    }
}
