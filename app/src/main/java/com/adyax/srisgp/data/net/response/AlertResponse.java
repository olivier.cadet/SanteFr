package com.adyax.srisgp.data.net.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public class AlertResponse extends BaseResponse implements Parcelable {

    @SerializedName("items")
    public AlertItem[] items;

    public AlertResponse() {
    }

    protected AlertResponse(Parcel in) {
        this.items = (AlertItem[])in.readArray(AlertItem.class.getClassLoader());
    }

    public static final Creator<AlertResponse> CREATOR = new Creator<AlertResponse>() {
        @Override
        public AlertResponse createFromParcel(Parcel in) {
            return new AlertResponse(in);
        }

        @Override
        public AlertResponse[] newArray(int size) {
            return new AlertResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeArray(this.items);
    }
}
