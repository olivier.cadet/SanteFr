package com.adyax.srisgp.data.net.command;

import android.os.Parcel;

import com.adyax.srisgp.data.net.command.base.INewFields;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */

//public class ResetCommand implements Parcelable {
public class GetAnchorsCommand extends ServiceCommand implements INewFields{

    // Do not change username to email !!!!
    @SerializedName(URL_OTHER)
    public String url;

    public GetAnchorsCommand(String url) {
        this.url = url;
    }

    public GetAnchorsCommand(Parcel in) {
        url = in.readString();
    }

    public static final Creator<GetAnchorsCommand> CREATOR = new Creator<GetAnchorsCommand>() {
        @Override
        public GetAnchorsCommand createFromParcel(Parcel in) {
            return new GetAnchorsCommand(in);
        }

        @Override
        public GetAnchorsCommand[] newArray(int size) {
            return new GetAnchorsCommand[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
    }

    @Override
    public void execute(CommandExecutor executor, AppReceiver receiver, int requestCode) {
        executor.getAnchors(this, receiver, requestCode);
    }

}
