package com.adyax.srisgp.data.net.response;

import androidx.annotation.DrawableRes;

import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anton.kobylianskiy on 10/26/16.
 */

public enum IconType {
    @SerializedName("1")
    MEDECIN(R.drawable.ic_medecin_24dp),
    @SerializedName("2")
    PROFESSIONNEL_DE_SANTE(R.drawable.ic_professionnel_de_sante_24dp),
    @SerializedName("3")
    PHARMACIE(R.drawable.ic_pharmacie_24dp),
    @SerializedName("4")
    LABARATOIRE(R.drawable.ic_laboratoire1_24dp),
    @SerializedName("5")
    BATIMENT(R.drawable.ic_batiment_24dp),
    @SerializedName("6")
    HOPITAL_CLINIQUE(R.drawable.ic_clinique_24dp),
    @SerializedName("7")
    SERVICE_A_LA_PERSONNE(R.drawable.ic_service_a_la_personne_24dp),
    UNKNOWN(R.drawable.ic_fiche_b_24dp);

    @DrawableRes
    private int resourceId;

    IconType(int resourceId) {
        this.resourceId = resourceId;
    }

    @DrawableRes
    public int getResourceId() {
        return resourceId;
    }

    public static int NodeToIcon(IGetUrl iGetUrl) {
        switch(iGetUrl.getNodeType()){
            case APPLICATIONS:
            case FICHES_INFOS:
            case VIDEO:
            case NUMERO_TEL:
                return R.drawable.ic_fiche_b_24dp;
            case PROFESSIONNEL_DE_SANTE:
                return R.drawable.ic_medecin_24dp;// R.drawable.ic_professionnel_de_sante_24dp
            case ESTABLISSEMENT_DE_SANTE:// was R.drawable.ic_pharmacie_24dp; https://prj.adyax.com/issues/224352
                return R.drawable.ic_clinique_24dp;
            case SERVICE_DE_SANTE://https://prj.adyax.com/issues/224352
//                return R.drawable.ic_pharmacie_24dp;
                return R.drawable.ic_batiment_24dp;

        }
        return R.drawable.ic_fiche_b_24dp;
    }

    public static IconType getIcon(IGetUrl iGetUrl) {
        switch(iGetUrl.getNodeType()){
            case APPLICATIONS:
            case FICHES_INFOS:
            case VIDEO:
            case NUMERO_TEL:
                return UNKNOWN;
            case PROFESSIONNEL_DE_SANTE:
                return MEDECIN;// R.drawable.ic_professionnel_de_sante_24dp
            case ESTABLISSEMENT_DE_SANTE:// was R.drawable.ic_pharmacie_24dp; https://prj.adyax.com/issues/224352
                return HOPITAL_CLINIQUE;
            case SERVICE_DE_SANTE://https://prj.adyax.com/issues/224352
//                return R.drawable.ic_pharmacie_24dp;
                return BATIMENT;

        }
        return UNKNOWN;
    }
}
