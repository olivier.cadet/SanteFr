package com.adyax.srisgp.data.net.executor;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public interface SuccessAction<T> {
    void onSuccess(T response);
}