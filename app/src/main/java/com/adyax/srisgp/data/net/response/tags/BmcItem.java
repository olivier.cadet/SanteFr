package com.adyax.srisgp.data.net.response.tags;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "bmc_item")
public class BmcItem {
    public static final String HTML = "html";

    @SerializedName(HTML)
    @DatabaseField(columnName = HTML, unique = true)
    public String html;

    public BmcItem(String html) {
        this.html = html;
    }

    public BmcItem() {
    }

}
