package com.adyax.srisgp.data.tracker;

import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.data.net.response.AnchorResponse;
import com.adyax.srisgp.data.net.response.tags.HistoryItem;
import com.adyax.srisgp.data.net.response.tags.IGetUrl;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.net.response.tags.SearchItemHuge;

/**
 * Created by SUVOROV on 3/28/17.
 */

public class ItemAdapter {

    private IGetUrl item;

    public ItemAdapter(IGetUrl item) {
        this.item = item;
    }

    public HistoryItem getHistoryItem() {
        if (item instanceof HistoryItem) {
            return (HistoryItem) item;
        }
        final HistoryItem historyItem = new HistoryItem();
        historyItem.updateTime();
        historyItem.xiti = item.getXiti();
        historyItem.nid = item.getNid();
        historyItem.title = item.getTitle();
        historyItem.node_type = item.getNodeType();
        historyItem.description = item.getDescription();
        historyItem.source = item.getSourceOrPropose();
        historyItem.icon = item.getIconType();
        if (item instanceof SearchItemHuge) {
            final SearchItemHuge searchItemHuge = (SearchItemHuge) item;
            historyItem.xiti_str = searchItemHuge.xiti_str;
            historyItem.url = searchItemHuge.getContentUrl();
            historyItem.phone = ((SearchItemHuge) item).getPhone();
        } else if (item instanceof QuickCardsAnonymousItem) {
            final QuickCardsAnonymousItem quickCardsAnonymousItem = (QuickCardsAnonymousItem) item;
            historyItem.xiti_str = quickCardsAnonymousItem.xiti_str;
            historyItem.url = quickCardsAnonymousItem.getContentUrl();
            historyItem.phone = ((QuickCardsAnonymousItem) item).getPhone();
        } else if (item instanceof AnchorResponse) {
            AnchorResponse anchorResponse = (AnchorResponse) this.item;
            anchorResponse.fixUrl();
            final AnchorResponse item = anchorResponse;
            historyItem.xiti_str = item.getXitiStr();
            historyItem.url = item.getContentUrl();
            historyItem.phone = (anchorResponse).getPhone();
        }
        switch (historyItem.getNodeType()) {
            case APPLICATIONS:
            case NUMERO_TEL:
                break;
            default:
            case LIENS_EXTERNES:
                historyItem.link = historyItem.url;
                break;
        }
        return historyItem;
    }
}
