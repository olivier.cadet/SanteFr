package com.adyax.srisgp.presentation;

import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.adyax.srisgp.R;
import com.adyax.srisgp.utils.NetworkStateReceiver;

/**
 * Created by anton.kobylianskiy on 9/23/16.
 */
public class BaseActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateListener {

    private NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();
    private AlertDialog networkDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    protected void replaceFragment(int containerId, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (networkStateReceiver == null) {
            networkStateReceiver = new NetworkStateReceiver();
        }
        networkStateReceiver.addNetworkStateListener(this);
        registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        networkStateReceiver.removeNetworkStateListener(this);
        unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void onNetworkAvailable() {

    }

    @Override
    public void onNetworkUnavailable() {
        showAlertDialog(R.string.connection_interrupted);
    }

    public void showAlertDialog(@StringRes int textRes) {
        if (networkDialog == null) {
            networkDialog = new AlertDialog.Builder(this)
                    .setMessage(textRes)
                    .setPositiveButton(R.string.ok_title, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
        }

        if (!networkDialog.isShowing()) {
            networkDialog.show();
        }
    }
}
