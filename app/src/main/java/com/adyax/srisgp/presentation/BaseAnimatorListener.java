package com.adyax.srisgp.presentation;

import android.animation.Animator;
import android.view.animation.Animation;

/**
 * Created by anton.kobylianskiy on 9/22/16.
 */
public class BaseAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
