package com.adyax.srisgp.presentation;

import android.content.Context;
import com.google.android.material.tabs.TabLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by anton.kobylianskiy on 11/23/16.
 */

public class CustomTabLayout extends TabLayout {
    private static final int MIND_DISTANCE = 150;

    private float x1;
    private float x2;

    private SwipeListener swipeListener;

    public CustomTabLayout(Context context) {
        super(context);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSwipeListener(SwipeListener swipeListener) {
        this.swipeListener = swipeListener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIND_DISTANCE) {
                    if (x2 > x1) {
                        if (swipeListener != null) {
                            swipeListener.onLeft();
                        }
                    } else {
                        if (swipeListener != null) {
                            swipeListener.onRight();
                        }
                    }
                }
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {

        return super.onInterceptTouchEvent(event);
    }

    public interface SwipeListener {
        void onLeft();

        void onRight();
    }
}
