package com.adyax.srisgp.presentation;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import com.adyax.srisgp.R;
import com.google.android.material.snackbar.Snackbar;

/**
 * Created by anton.kobylianskiy on 9/29/16.
 */

public class BaseFragment extends Fragment {

    protected void showLocationNotEnabled() {
        showSnackbar(R.string.enable_location_refused);
    }

    protected void showLocationPermissionDenied() {
        showSnackbar(R.string.location_permission_denied);
    }

    protected void showLocationPermissionNotAskAgain() {
        showSnackbar(R.string.location_permission_not_ask_again);
    }

    private void showSnackbar(@StringRes int resource) {
        Snackbar snackbar = Snackbar.make(getView(), resource, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }
}
