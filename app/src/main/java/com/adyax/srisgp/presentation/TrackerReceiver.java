package com.adyax.srisgp.presentation;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.atinternet.tracker.ReferrerReceiver;


public class TrackerReceiver extends ReferrerReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        // #84283 - Implement tracker (referer)
        // Get Campaign ID:
        String campid = context.getSharedPreferences("ATPreferencesKey", 0).getString("ATMarketingCampaignSaved", "");
        Log.d("ATInternet", "Campaign id : " + campid);

        // Send to ATInternet.
//        AutoTracker tracker = ATInternet.getInstance().getAutoTracker(context.getString(R.string.at_internet_smart_token));
//        Screen s = tracker.Screens().add("Ad");
//        s.Campaign(campid);
//        s.sendView();
    }
}

