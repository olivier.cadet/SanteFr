package com.adyax.srisgp.presentation;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adyax.srisgp.App;
import com.adyax.srisgp.BuildConfig;
import com.adyax.srisgp.R;
import com.adyax.srisgp.appwidget.SNTAppWidget;
import com.adyax.srisgp.data.net.UrlExtras;
import com.adyax.srisgp.data.net.command.GetConfigurationCommand;
import com.adyax.srisgp.data.net.command.LogoutCommand;
import com.adyax.srisgp.data.net.command.PingCommandAuth;
import com.adyax.srisgp.data.net.command.SetPushCredentialServiceCommand;
import com.adyax.srisgp.data.net.command.tags.SearchType;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ConfigurationResponse;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.ImageConfigurationItem;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.repository.SharedPreferencesHelper;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.databinding.ActivityMainBinding;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.around_me.AroundMeArguments;
import com.adyax.srisgp.mvp.around_me.AroundMeFragment;
import com.adyax.srisgp.mvp.around_me.get_location.GetLocationAroundMeFragment;
import com.adyax.srisgp.mvp.ask_login.AskLoginArg;
import com.adyax.srisgp.mvp.ask_login.AskLoginPresenter;
import com.adyax.srisgp.mvp.create_account.CreateAccountActivity;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.favorite.YourFavoritesFragment;
import com.adyax.srisgp.mvp.history.YourHistoryFragment;
import com.adyax.srisgp.mvp.maintenance.MaintenanceFragment;
import com.adyax.srisgp.mvp.my_account.MyAccountFragment;
import com.adyax.srisgp.mvp.notification.YourNotificationsFragment;
import com.adyax.srisgp.mvp.onboarding.OnboardingActivity;
import com.adyax.srisgp.mvp.searchhome.SearchHomeFragment;
import com.adyax.srisgp.mvp.searchhome.alerts.notification.NotificationChannelIds;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;
import com.adyax.srisgp.mvp.web_views.WebViewStatic;
import com.adyax.srisgp.mvp.widget.WidgetViewHelper;
import com.adyax.srisgp.utils.BadgeDrawable;
import com.adyax.srisgp.utils.ViewUtils;
import com.appsflyer.AppsFlyerLib;
import com.atinternet.tracker.ATInternet;
import com.atinternet.tracker.SetConfigCallback;
import com.atinternet.tracker.Tracker;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

public class MainActivity extends PlusAlertPopupActivity
        implements NavigationView.OnNavigationItemSelectedListener, AppReceiver.AppReceiverCallback,
        GetLocationAroundMeFragment.GetLocationAroundMeListener, SharedPreferencesHelper.LoggedInStateListener,
        MyAccountFragment.MyAccountListener {

    private static final int REQUEST_ONBOARDING = 123;
    private static final int REQUEST_CHECK_SETTINGS = 12;
    private static final int REQUEST_STORAGE_PERMISSION = 2;
    private Tracker tracker;

    //    private AppReceiver appReceiver = new AppReceiver();
    private ActivityMainBinding binding;
    @Inject
    FragmentFactory fragmentFactory;
    //    @Inject
//    INetWorkState netWorkState;
    @Inject
    IRepository repository;
    @Inject
    IDataBaseHelper dataBaseHelper;
    @Inject
    IGeoHelper geoHelper;

    private Bundle bundle;

    private Intent onboardingActivityIntent = null;

    private boolean bStartAroundMe = false;

    private ImageView ivMinistryLogo;

    private void applyWidgetAction(final Intent intent) {
        String itemType = intent.getStringExtra(SNTAppWidget.EXTRA_ITEM_TYPE);
        String itemUrl = intent.getStringExtra(SNTAppWidget.EXTRA_ITEM_URL);
        Long itemNode = intent.getLongExtra(SNTAppWidget.EXTRA_ITEM_NODE, 0);

        WebViewArg webviewArg = null;
        if (itemUrl == null || itemUrl.equals("")) {
            webviewArg = new WebViewArg(itemNode, WebPageType.SERVER, TrackerLevel.UNKNOWN);
        } else {
            webviewArg = new WebViewArg(itemUrl, true);
        }
        if ("WIDGET_ALERT".equals(itemType)) {
            if (itemUrl != null && !itemUrl.equals("")) {
                fragmentFactory.startWebViewActivity(this, webviewArg, -1);
            }
        } else if ("WIDGET_ARTICLE".equals(itemType)) {
            fragmentFactory.startWebViewActivity(this, webviewArg, -1);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (SNTAppWidget.OPEN_ACTION.equals(intent.getAction())) {
            applyWidgetAction(intent);
        } else {
            bundle = intent.getExtras();
            openNotifyWebView();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);

        // Init AtInternet tracker
        initAtInternet();

        // #84283 - Implement referer tracking system. ()
        // https://developers.atinternet-solutions.com/android-fr/campagnes-android-fr/campagnes-marketing-android-fr-2-3-0/
        tracker = ATInternet.getInstance().getDefaultTracker();
        tracker.setConfig("campaignLastPersistence", false, new SetConfigCallback() {
            @Override
            public void setConfigEnd() {
            }
        });

        // On verifie si l'application n'a pas été lancée depuis le widget
        if (SNTAppWidget.OPEN_ACTION.equals(getIntent().getAction())) {
            applyWidgetAction(getIntent());
        } else {
            bundle = getIntent().getExtras();
        }
        setTheme(R.style.AppTheme);
//        requestPermissions();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.toolbar.setBackgroundColor(getResources().getColor(R.color.bg_new_blue));
//        binding.toolbar.setTitle(R.string.around_me);
        setSupportActionBar(binding.toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        binding.navView.setNavigationItemSelectedListener(this);
        ivMinistryLogo = (ImageView) findViewById(R.id.iv_ministry_logo);

        if (savedInstanceState == null) {
            setStartFragment();
        }
        initListeners();

        // Test AppFlyer
//        Map<String, Object> eventValue = new HashMap<String, Object>();
//        eventValue.put(AFInAppEventParameterName.LEVEL, 9);
//        eventValue.put(AFInAppEventParameterName.SCORE, 100);
//        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.LEVEL_ACHIEVED, eventValue);
        AppsFlyerLib.getInstance().sendDeepLinkData(this);
        // END TEST

        updateConfiguration();

    }

    private void showInit() {
        if (repository.isFirstRun() || repository.appVersionHasChanged()) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.onboardingActivityIntent = OnboardingActivity.newIntent(MainActivity.this);
            repository.setAppVersion();
            startActivityForResult(this.onboardingActivityIntent, REQUEST_ONBOARDING);
        }
    }

    private void updateConfiguration() {
        ExecutionService.sendCommand(getApplicationContext(), appReceiver,
                new GetConfigurationCommand(), ExecutionService.GET_CONFIGURATION);
    }

    private void initAtInternet() {
        Tracker tracker = ATInternet.getInstance().getDefaultTracker();
        HashMap config = new HashMap<String, Object>() {{
            put("log", getString(R.string.at_internet_log));
            put("logSSL", getString(R.string.at_internet_logssl));
            put("site", getString(R.string.at_internet_site));
//            put("secure", false);
            put("hashUserId", false);
            put("storage", "required");
            put("pixelPath", "/hit.xiti");
            put("plugins", "tvtracking");
            put("domain", "xiti.com");
            put("identifier", "androidId");//?
            put("persistIdentifiedVisitor", true);
            put("enableCrashDetection", true);
            put("tvtVisitDuration", 10);//1
            put("tvtURL", "");
            put("campaignLastPersistence", false);//true
            put("campaignLifetime", 30);//1
            put("sessionBackgroundDuration", 60);//10
        }};
        tracker.setConfig(config, () -> Log.d("TrackerAT", "Configuration is now set"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ONBOARDING:
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.w("MessagingService", "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                Log.d("MessagingService", "Sending token: " + token);

                                // Execute service
                                ExecutionService.sendCommand(
                                        getApplicationContext(),
                                        appReceiver,
                                        new SetPushCredentialServiceCommand(token, repository.getUserUID()),
                                        ExecutionService.SET_PUSH_CREDENTIAL);

                            }
                        });
                if (resultCode == RESULT_OK &&
                        data.getBooleanExtra(OnboardingActivity.INTENT_OPEN_CREATE_ACCOUNT, false)) {
                    clickCreateAccount();
                }
                break;
            case REQUEST_CHECK_SETTINGS:
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (fragment != null && fragment instanceof GetLocationAroundMeFragment) {
                    ((GetLocationAroundMeFragment) fragment).onPromptSettingsResult(resultCode == Activity.RESULT_OK);
                }
                break;
            case AskLoginPresenter.START_LOGIN_REQUEST_CODE:
                updateDrawable();
                break;
            case AskLoginPresenter.START_CREATE_ACCOUNT_REQUEST_CODE:
                updateDrawable();
                break;
            case AroundMeFragment.FILTERS_ACTIVITY_REQUEST_CODE:
                for (Fragment f : getSupportFragmentManager().getFragments()) {
                    if (f != null && f.isVisible()) {
                        f.onActivityResult(requestCode, resultCode, data);
                    }
                }
                break;
            case WidgetViewHelper.REQUEST_WIDGET:
                if (data != null && data.getBooleanExtra(WidgetViewHelper.SHOW_AROUNDME_RESPONSE, false) == true) {
                    onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_around_me));
                    binding.navView.setCheckedItem(R.id.nav_around_me);
                }
                break;
        }
    }

    private void setStartFragment() {
//        fragmentFactory.startAroundMeFragment(this);
//        binding.navView.setCheckedItem(R.id.nav_around_me);
        gotoHomePage();
        binding.navView.setCheckedItem(R.id.nav_new_search);
    }

    private void initListeners() {
        binding.navView.getHeaderView(0).findViewById(R.id.create_account).setOnClickListener(view -> clickCreateAccount());
        binding.navView.getHeaderView(0).findViewById(R.id.log_in).setOnClickListener(view -> clickLogIn(view));
        findViewById(R.id.sign_out).setOnClickListener(view -> clickSignOut(view));
        findViewById(R.id.facebook_login).setOnClickListener(view -> clickFacebookLogIn(view));
        findViewById(R.id.twitter_login).setOnClickListener(view -> clickTwitterLogIn(view));
        binding.drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                updateNavigateMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        View view = this.getCurrentFocus();
        if (view != null) {
            ViewUtils.closeKeyboard(view);
        }
        updateConfiguration();
        testAlerts();
        switch (item.getItemId()) {
            case R.id.nav_new_search:
                gotoHomePage();
                break;
            case R.id.nav_around_me:
                if (!geoHelper.isShowGeoRequest()) {
                    fragmentFactory.startGetLocationAroundMeFragment(this);
                } else {
//                    if (!requestPermissions()) {
                    fragmentFactory.startAroundMeFragment(this, new AroundMeArguments(SearchType.around));
//                    }
                }
                break;
            case R.id.nav_favorites:
                if (netWorkState.isLoggedIn()) {
                    fragmentFactory.startFavoriteFragment(this);
                } else {
                    fragmentFactory.startAskLoginFragment(this,
                            new AskLoginArg(R.drawable.ic_favoris_48dp, R.string.favorite_your_favorites, R.string.favorite_your_favorites_description));
                }
                break;
            case R.id.nav_notifications:
                if (netWorkState.isLoggedIn()) {
                    fragmentFactory.startNotificationFragment(this);
                } else {
                    fragmentFactory.startAskLoginFragment(this,
                            new AskLoginArg(R.drawable.ic_notifications_48dp, R.string.notification_your_notifications, R.string.notification_body));
                }
                break;
            case R.id.nav_historical:
//                if(netWorkState.isLoggedIn()) {
                fragmentFactory.startHistoryFragment(this);
//                }else{
//                    fragmentFactory.startAskLoginFragment(this,
//                            new AskLoginArg(R.drawable.ic_historique_24dp, R.string.history_your_history, R.string.history_login));
//                }
                break;
            case R.id.nav_my_account:
                if (netWorkState.isLoggedIn()) {
                    fragmentFactory.startMyAccountFragment(this);
                } else {
                    fragmentFactory.startAskLoginFragment(this,
                            new AskLoginArg(R.drawable.ic_mon_compte_48dp, R.string.my_account, R.string.mon_compte_body));
                }
                break;
            case R.id.nav_configure_text_size:
                fragmentFactory.startConfigureTextFragment(this);
                break;
            case R.id.nav_about:
//                if (BuildConfig.DEV_STATUS) {
//                    fragmentFactory.startDummyFragment(this, R.string.about_menu);
//                } else {
                fragmentFactory.startWebViewActivity(this, new WebViewArg(UrlExtras.ABOUT_PAGE), -1);
//                }
                break;
            case R.id.nav_how_it_works:
//                if (BuildConfig.DEV_STATUS) {
//                    fragmentFactory.startDummyFragment(this, R.string.how_it_works);
//                } else {
                fragmentFactory.startWebViewActivity(this, new WebViewArg(UrlExtras.HOW_IT_WORKS), -1);
//                }
                break;
            case R.id.nav_data_protection:
//                if (BuildConfig.DEV_STATUS) {
//                    fragmentFactory.startDummyFragment(this, R.string.data_protection);
//                } else {
                fragmentFactory.startWebViewActivity(this, new WebViewArg(UrlExtras.INFORMATION_LEGALES), -1);
//                }
                break;
            case R.id.nav_sante:
                fragmentFactory.startWebViewActivity(this, new WebViewArg(UrlExtras.CHARTER), -1);
                break;
            case R.id.nav_contact:
//                if (!BuildConfig.DEV_STATUS) {
                fragmentFactory.startContactFragment(this);
//                } else {
//                    fragmentFactory.startDummyFragment(this, R.string.contact);
//                }
                break;
        }
        closeDrawer();
        return true;
    }

    private void gotoHomePage() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, SearchHomeFragment.newInstance())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (appReceiver == null) {
            appReceiver = new AppReceiver();
        }
        appReceiver.setListener(this);
        openNotifyWebView();
        repository.addLoggedInStateListener(this);
        createNotificationChannel();
    }

    @Override
    public void onStop() {
        super.onStop();
//        Debugger.remove();
        appReceiver.setListener(null);
        repository.removeLoggedInStateListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (bStartAroundMe) {
            bStartAroundMe = false;
            fragmentFactory.startAroundMeFragment(this, new AroundMeArguments(SearchType.around));
        }
//        startTimer();
        updateNavigateMenu();
        // It for quick update badge on navigation menu
        if (netWorkState.isLoggedIn()) {
            ExecutionService.sendCommand(getApplicationContext(), appReceiver,
                    new PingCommandAuth(), ExecutionService.PING_AUTH_ACTION);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
//        stopTimer();
    }

    private void updateNavigateMenu() {
        if (netWorkState.isLoggedIn()) {
            setNotificationBadge(repository.getNewNotificationCount());
//            startTimer();
            binding.navView.getHeaderView(0).findViewById(R.id.welcome).setVisibility(View.VISIBLE);
            binding.navView.getHeaderView(0).findViewById(R.id.login_container).setVisibility(View.INVISIBLE);
            findViewById(R.id.sign_out_divider).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out).setVisibility(View.VISIBLE);
            //
            binding.navView.getMenu().findItem(R.id.nav_favorites).setEnabled(true);
            binding.navView.getMenu().findItem(R.id.nav_notifications).setEnabled(true);
            binding.navView.getMenu().findItem(R.id.nav_historical).setEnabled(true);
            binding.navView.getMenu().findItem(R.id.nav_my_account).setEnabled(true);
        } else {
            setNotificationBadge(0);
//            stopTimer();
            binding.navView.getHeaderView(0).findViewById(R.id.welcome).setVisibility(View.INVISIBLE);
            binding.navView.getHeaderView(0).findViewById(R.id.login_container).setVisibility(View.VISIBLE);
            findViewById(R.id.sign_out_divider).setVisibility(View.GONE);
            findViewById(R.id.sign_out).setVisibility(View.GONE);
            //
//            binding.navView.getMenu().findItem(R.id.nav_favorites).setEnabled(false);
//            binding.navView.getMenu().findItem(R.id.nav_notifications).setEnabled(false);
//            if (!BuildConfig.DEV_STATUS && !BuildConfig.DEBUG) {
//                binding.navView.getMenu().findItem(R.id.nav_historical).setEnabled(false);
//            }
//            binding.navView.getMenu().findItem(R.id.nav_my_account).setEnabled(false);
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            List<NotificationChannel> channels = new ArrayList<>();
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            // NEWS
            NotificationChannel channel =
                    new NotificationChannel(NotificationChannelIds.NEWS,
                            getString(R.string.news_channel_name),
                            NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(getString(R.string.news_channel_description));
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            channels.add(channel);


            // ACTU
            NotificationChannel channelActu =
                    new NotificationChannel(NotificationChannelIds.ALERT,
                            getString(R.string.alert_channel_name),
                            NotificationManager.IMPORTANCE_HIGH);
            channel.setDescription(getString(R.string.alert_channel_description));
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            channels.add(channelActu);

            // Register channels
            notificationManager.createNotificationChannels(channels);
        }
    }

    private void setNotificationBadge(int badgeCounter) {
        try {
            MenuItem itemCart = binding.navView.getMenu().findItem(R.id.nav_notifications);
            LayerDrawable icon = (LayerDrawable) itemCart.getIcon();
            BadgeDrawable.setBadgeCount(this, icon, String.valueOf(badgeCounter));
        } catch (Exception e) {

        }
    }

    @Override
    public void onSuccess(int requestCode, Bundle data) {
        if (binding.navView.getMenu().findItem(R.id.nav_new_search).isChecked()) {
            final Fragment fragment = getSupportFragmentManager().findFragmentByTag(MaintenanceFragment.class.getCanonicalName());
            if (fragment != null) {
                onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_new_search));
            }
        }
        switch (requestCode) {
            case ExecutionService.PING_AUTH_ACTION:
                if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    if (netWorkState.isLoggedIn()) {
                        setNotificationBadge(repository.getNewNotificationCount());
                    } else {
                        setNotificationBadge(0);
                    }
                }
//                data.getInt(UserResponse.NEW_NOTIFICATIONS);
//                //bundle.putInt(UserResponse.NEW_NOTIFICATIONS, response.getNewNotificationCount());
                break;
            case ExecutionService.LOGIN_ACTION:
                startTimer();
                break;
            case ExecutionService.LOGOUT_ACTION:
                updateMenuAndFragments();
                break;
            case ExecutionService.GET_CONFIGURATION://ConfigurationResponse
                this.showInit();
                final ConfigurationResponse configurationResponse = data.getParcelable(CommandExecutor.BUNDLE_CONFIGURATION);
//                MinistryLogoUrl logoUrl = data.getParcelable(CommandExecutor.BUNDLE_MINISTRY_LOGO);
                if (configurationResponse != null) {
                    if (configurationResponse != null) {
                        final ImageConfigurationItem images = configurationResponse.getImages();
                        if (images != null) {
                            Glide.with(this)
                                    .load(images.getHeader_logo_android())
                                    .into(ivMinistryLogo);
                        }
                    }
                    repository.saveConfig(configurationResponse);
                }
                break;
        }
    }

    public void updateMenuAndFragments() {
        updateNavigateMenu();

        Fragment fragment = fragmentFactory.getCurrentFragment(this);
        if (fragment != null) {
            if (fragment instanceof YourFavoritesFragment ||
                    fragment instanceof YourNotificationsFragment ||
                    fragment instanceof MyAccountFragment ||
                    fragment instanceof YourHistoryFragment) {
                setStartFragment();
            }
        }
    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        super.onFail(requestCode, errorMessage);
        final ErrorResponse errorResponse = ErrorResponse.create(errorMessage);
        if (errorResponse != null) {
            if (requestCode == ExecutionService.GET_CONFIGURATION) {
                WebViewArg webviewArg = null;
                webviewArg = new WebViewArg("/", false);
                Intent intent = new Intent(this, WebViewStatic.class);
                intent.putExtra(WebViewArg.CONTENT_URL, webviewArg);
                startActivity(intent);
            }
            if (errorResponse.isAnyError()) {
                if (BuildConfig.DEBUG) {
                    Snackbar snackbar = Snackbar.make(binding.getRoot(), errorResponse.getErrorMessage(null), Snackbar.LENGTH_SHORT);
                    View view = snackbar.getView();
                    TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
                    tv.setTextColor(Color.RED);
                    snackbar.show();
                }
                if (errorResponse.isNotLogin()) {
                    // Si nous avons encore les informations de connexion de l'utilisateur alors on essaie une reconnexion automatique

                    // Sinon on logout
                    netWorkState.logOut();
                    updateNavigateMenu();
                }
//                if(errorResponse.isMaintenanceMode()){
////                    onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_new_search));
//                    fragmentFactory.startMaintenanceFragment(this, R.string.app_name);
//                    binding.navView.setCheckedItem(R.id.nav_new_search);
//                }
            }
        }
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {

    }

    private void clickCreateAccount() {
//        Snackbar.make(view, "clickCreateAccount", Snackbar.LENGTH_LONG).show();
        Intent intent = CreateAccountActivity.newIntent(getApplicationContext(), CreateAccountActivity.START_STANDART);
        startActivity(intent);
        overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left);
        closeDrawer();
    }

    private void closeDrawer() {
        binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void clickLogIn(View view) {
//        sendLogin();
//        AuthenticationActivity.start(this);
        fragmentFactory.startLoginActivity(this);
        closeDrawer();
    }

    private void clickSignOut(View view) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.sign_out_message)
                .setPositiveButton(R.string.sign_out_positive, (dialog, which) -> {
                    ExecutionService.sendCommand(getApplicationContext(), appReceiver,
                            new LogoutCommand(), ExecutionService.LOGOUT_ACTION);
                    closeDrawer();
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void clickFacebookLogIn(View view) {
        openUrlInBrowser(UrlExtras.FACEBOOK_LINK);
    }

    private void openUrlInBrowser(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void clickTwitterLogIn(View view) {
        openUrlInBrowser(UrlExtras.TWITTER_LINK);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
                overridePendingTransition(R.anim.anim_slide_right_in, R.anim.anim_slide_right_out);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPromptLocation(Status status) {
        try {
            status.startResolutionForResult(
                    this,
                    REQUEST_CHECK_SETTINGS);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCloseGetLocationAroundMeFragment() {

    }

    private synchronized void openNotifyWebView() {
        try {
            if (bundle != null) {
                final AlertItem alertItem = bundle.getParcelable(AlertItem.BUNDLE_NAME);
                if (alertItem != null) {
                    try {
                        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(alertItem.getIdForNotify());
                    } catch (Exception e) {
                        bundle = null;
                    }
                    final long id = alertItem.getId();
                    if (alertItem.push_read) {
                        // it is push
                        if (alertItem.isContent()) {
//                            dataBaseHelper.closeUrgentAlert(alertItem);
//                            repository.saveUrgentAlert(alertItem.getId());
                            fragmentFactory.startWebViewActivity(this, new WebViewArg(alertItem, TrackerLevel.UNKNOWN).setPushAlert(alertItem), WebViewActivity.WEB_VIEW_REQUEST_CODE);
                            setPushAlert(alertItem);
                            return;
                        }
                    } else {
                        if (alertItem.isContent()) {
                            if (!dataBaseHelper.isPushReadAlert(id)) {
                                fragmentFactory.startWebViewActivity(this, new WebViewArg(alertItem, TrackerLevel.UNKNOWN).setPushAlert(alertItem), WebViewActivity.WEB_VIEW_REQUEST_CODE);
                                dataBaseHelper.setPushReadAlert(id);
                                setPushAlert(alertItem);
                                return;
                            }
                        } else {
                            dataBaseHelper.setPushReadAlert(id);
                        }
                    }
                }
                setStartFragment();
            }
        } catch (Exception e) {

        } finally {
            bundle = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            bStartAroundMe = true;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
////        List<Fragment> fragments = getSupportFragmentManager().getChildFragmentManager().getFragments();
//        List<Fragment> fragments = getSupportFragmentManager().getFragments();
//        if (fragments != null) {
//            for (Fragment fragment : fragments) {
//                if (fragment != null) {
//                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
//                }
//            }
//        }
    }

//    private boolean requestPermissions() {
//
//        List<String> permissions = new LinkedList<>();
//
////        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
////            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
////
////        }
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
//            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        }
//
//        if (permissions.size() > 0) {
//            String[] permissionArray = new String[permissions.size()];
//            for (int i = 0; i < permissionArray.length; i++) {
//                permissionArray[i] = permissions.get(i);
//            }
//            ActivityCompat.requestPermissions(this, permissionArray, REQUEST_STORAGE_PERMISSION);
//        }
//        return permissions.size() > 0;
//    }

    @Override
    public void onLoggedIn() {

    }

    @Override
    public void onLoggedOut() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof YourNotificationsFragment || currentFragment instanceof YourFavoritesFragment || currentFragment instanceof YourHistoryFragment) {
            binding.navView.setCheckedItem(R.id.nav_new_search);
        }
    }

    @Override
    public void onHistoryClicked() {
        onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_historical));
        binding.navView.setCheckedItem(R.id.nav_historical);
    }

    @Override
    public void onFavoritClicked() {
        onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_favorites));
        binding.navView.setCheckedItem(R.id.nav_favorites);
    }

    @Override
    public void onNotificationsClicked() {
        onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_notifications));
        binding.navView.setCheckedItem(R.id.nav_notifications);
    }

    @Override
    public void onMyAccountClicked() {
        onNavigationItemSelected(binding.navView.getMenu().findItem(R.id.nav_my_account));
        binding.navView.setCheckedItem(R.id.nav_my_account);
    }

    public void updateDrawable() {
        if (binding.navView.getMenu().findItem(R.id.nav_favorites).isChecked()) {
            onFavoritClicked();
        } else if (binding.navView.getMenu().findItem(R.id.nav_notifications).isChecked()) {
            onNotificationsClicked();
        } else if (binding.navView.getMenu().findItem(R.id.nav_my_account).isChecked()) {
            onMyAccountClicked();
        } else if (binding.navView.getMenu().findItem(R.id.nav_historical).isChecked()) {
            onHistoryClicked();
        }
    }

}
