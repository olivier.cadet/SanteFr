package com.adyax.srisgp.presentation;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ICreateAccount;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.ask_geo.AskGeoFragment;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.GeoHelper;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.maintenance.MaintenanceBaseActivity;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 2/13/17.
 */

public abstract class AskGeoPlusAlertPopupActivity extends MaintenanceBaseActivity implements ICreateAccount {
    @Inject
    IGeoHelper geoHelper;

    @Inject
    protected IRepository repository;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);
    }

    protected void startFragment() {
        if(!repository.hasBeenShownGeoRequest()&& !geoHelper.isShowGeoRequest()){
            Fragment fragment = AskGeoFragment.newInstance();
            ((FragmentActivity) this).getSupportFragmentManager().beginTransaction()
//                .setCustomAnimations(R.anim.anim_slide_in_right, R.anim.anim_slide_out_left)
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            repository.setStatusShowGeoRequest();
        }else {
            startSwipe(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == GeoHelper.REQUEST_LOCATION_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                searchPresenter.onRequestCurrentLocation(aroundMeClicked);
                startAskGeoPermission();
            } else {
                // TODO unfortunatly it don't work
//                this.runOnUiThread(() -> startSwipe());
                geoHelper.setFinished();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void startAskGeoPermission() {
        if(geoHelper.askPermission(this)){
//            startSwipe(true);
        }
    }

//    @Override
    public void startGetGeo() {
        if(geoHelper.isShowGeoRequest()) {
            startAskGeoPermission();
        }
    }

    @Override
    public void onDestroy(){
        geoHelper.disconnect();
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(geoHelper.isFinished()){
            startSwipe(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GeoHelper.REQUEST_CHECK_SETTINGS) {
            if(resultCode== Activity.RESULT_OK){
                geoHelper.requestLocationUpdates();
                startSwipe(true);
            }else{
                startSwipe(true);
            }
//            ((SearchFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container))
//                    .onPromptSettingsResult(resultCode == Activity.RESULT_OK);
        }
    }

    protected abstract void startSwipe(boolean bAnimaton);

    public IGeoHelper getGeoHelper() {
        return geoHelper;
    }
}
