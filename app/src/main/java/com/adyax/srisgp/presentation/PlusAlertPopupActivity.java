package com.adyax.srisgp.presentation;

import android.content.Intent;
import android.database.ContentObserver;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.INetWorkState;
import com.adyax.srisgp.data.net.command.PingCommandAuth;
import com.adyax.srisgp.data.net.command.PingCommandNotAuth;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.ErrorResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.repository.IRepository;
import com.adyax.srisgp.data.tracker.TrackerLevel;
import com.adyax.srisgp.db.IDataBaseHelper;
import com.adyax.srisgp.db.TablesContentProvider;
import com.adyax.srisgp.factory.FragmentFactory;
import com.adyax.srisgp.mvp.create_account.flow.finish_create_account.geo_helper.IGeoHelper;
import com.adyax.srisgp.mvp.searchhome.alerts.IAlerts;
import com.adyax.srisgp.mvp.searchhome.alerts.alert_popin.AlertPopinActivity;
import com.adyax.srisgp.mvp.web_views.WebPageType;
import com.adyax.srisgp.mvp.web_views.WebViewActivity;
import com.adyax.srisgp.mvp.web_views.WebViewArg;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

/**
 * Created by SUVOROV on 2/3/17.
 */

public class PlusAlertPopupActivity extends BaseActivity {

    public static final int ALERT_POPIN_REQUEST_CODE = 123;
    @Inject
    IAlerts alerts;
    @Inject
    IRepository repository;
    @Inject
    FragmentFactory fragmentFactory;
    @Inject
    INetWorkState netWorkState;
    @Inject
    IDataBaseHelper dataBaseHelper;
    //    @Inject
    IGeoHelper geoHelper;

    protected AppReceiver appReceiver = new AppReceiver();

    private AlertItem pushAlertItem;
    private Boolean isAlertShow = false;
    private boolean isReset = false;

    private Timer timer;
    private boolean pushAlertStaus;

    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        final ErrorResponse errorResponse = ErrorResponse.create(errorMessage);
        if (errorResponse != null && errorResponse.isMaintenanceMode()) {
            showDialogMaintenanceMode();
        }
    }

    private class PingTimerTask extends TimerTask {

        @Override
        public void run() {
            try {
                if (netWorkState.isLoggedIn()) {
                    ExecutionService.sendCommand(getApplicationContext(), appReceiver,
                            new PingCommandAuth(), ExecutionService.PING_AUTH_ACTION);
                } else {
//                    // TODO i use it api for ping when user not log in
//                    ExecutionService.sendCommand(getApplicationContext(), appReceiver,
//                        new GetPopularCommand(), ExecutionService.GET_POPULAR_ACTION);
                    ExecutionService.sendCommand(getApplicationContext(), appReceiver,
                            new PingCommandNotAuth(), ExecutionService.PING_AUTH_NON_ACTION);
                }
            } catch (Exception e) {

            }
        }
    }

    protected synchronized void startTimer() {
        if (timer == null) {
            timer = new Timer();
            PingTimerTask pingTimerTask = new PingTimerTask();
            timer.schedule(pingTimerTask, 10000, 10000);
        }
    }

    private synchronized void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private ContentObserver contentObserver = new ContentObserver(new Handler()) {

        @Override
        public void onChange(boolean selfChange) {
            synchronized (isAlertShow) {
                if (!isAlertShow) {
                    new AsyncTask<Void, Void, AlertItem>() {

                        @Override
                        protected AlertItem doInBackground(Void... params) {
                            return alerts.getUrgentAlert();
                        }

                        @Override
                        protected void onPostExecute(AlertItem urgentAlert) {
                            synchronized (isAlertShow) {
                                try {
                                    if (urgentAlert != null && !pushAlertStaus && !isAlertShow) {
//                                        AlertItem lastUrgentAlert = repository.getLastUrgentAlert();
//                                        if (urgentAlert.getId() != lastUrgentAlert.getId() /*&& pushAlertItem!=lastUrgentAlert*/) {
                                        dataBaseHelper.closeUrgentAlert(urgentAlert);
                                        if (pushAlertItem == null) {
                                            repository.saveLastUrgentAlert(urgentAlert);
                                            pushAlertItem = urgentAlert;
                                        }
                                        isAlertShow = true;
                                        fragmentFactory.startAlertPopinActivity(PlusAlertPopupActivity.this, urgentAlert, ALERT_POPIN_REQUEST_CODE);
//                                        } else {
////                                dataBaseHelper.closeUrgentAlert(urgentAlert);
//                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }.execute();
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case WebViewActivity.WEB_VIEW_REQUEST_CODE2:
                isAlertShow = false;
                if (pushAlertStaus) {
                    repository.saveLastUrgentAlert(alerts.getUrgentAlert());
                    pushAlertStaus = false;
                }
////                isReset = true;
//                if (resultCode != RESULT_OK) {
//                    isReset = true;
//                } else {
                pushAlertItem = null;
//                }
                break;
            case ALERT_POPIN_REQUEST_CODE:
                if (resultCode == RESULT_OK && pushAlertItem != null) {
                    fragmentFactory.startWebViewActivity(this,
                            new WebViewArg(pushAlertItem, WebPageType.SERVER, TrackerLevel.UNKNOWN)
                                    .setAlertItem(pushAlertItem)
                                    .setRedButtonFlag(), WebViewActivity.WEB_VIEW_REQUEST_CODE2);
                } else {
                    isAlertShow = false;
                    if (pushAlertStaus) {
                        repository.saveLastUrgentAlert(alerts.getUrgentAlert());
                        pushAlertStaus = false;
                    }
//                isReset = true;
                    if (resultCode != RESULT_OK) {
                        isReset = true;
                    } else {
                        pushAlertItem = null;
                    }
                }
                break;
            default: {
                pushAlertItem = null;
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.getApplicationComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
////        getContentResolver().registerContentObserver(TablesContentProvider.ALERTS_CONTENT_URI, true, contentObserver);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
////        getContentResolver().unregisterContentObserver(contentObserver);
//    }

    @Override
    public void onResume() {
        super.onResume();
        if (!isAlertPopinActivity()) {
            getContentResolver().registerContentObserver(TablesContentProvider.ALERTS_CONTENT_URI, true, contentObserver);
            testAlerts();
        }
        startTimer();
    }

    public void testAlerts() {
        if (pushAlertItem == null) {
            getContentResolver().notifyChange(TablesContentProvider.ALERTS_CONTENT_URI, null);
        }
        if (isReset) {
//            pushAlertItem = null;
            if (!isAlertPopinActivity()) {
                pushAlertItem = null;
            } else {
                getContentResolver().notifyChange(TablesContentProvider.ALERTS_CONTENT_URI, null);
            }
            isReset = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isAlertPopinActivity()) {
            getContentResolver().unregisterContentObserver(contentObserver);
        }
        stopTimer();
    }

    private boolean isAlertPopinActivity() {
        return this instanceof AlertPopinActivity;
    }

    public void setPushAlert(AlertItem alertItem) {
        // We'll show any alert from push. But we save first urgent alert
//        repository.saveLastUrgentAlert(alerts.getUrgentAlert());
        this.pushAlertItem = alertItem;
//        pushAlertStaus = alertItem != null;
    }

    private void showDialogMaintenanceMode() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.maintenance_popup_title)
                .setMessage(R.string.maintenance_popup_text)
                .setPositiveButton(R.string.to_close, (dialog, which) -> {
                    fragmentFactory.startMaintenanceActivity(this);
//                    dialog.dismiss();
                })
                .show();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (geoHelper != null) {
            geoHelper.setOnLocationListener(location -> {
                update(location);
                geoHelper.disconnect();
            });
            startAskGeo();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (geoHelper != null) {
            geoHelper.disconnect();
        }
    }

    private void startAskGeo() {
        if (!geoHelper.askGeo(this)) {
            update(null);
        }
    }

    protected void update(Location location) {

    }

}
