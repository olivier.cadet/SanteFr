package com.adyax.srisgp.appwidget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.adyax.srisgp.App;
import com.adyax.srisgp.R;
import com.adyax.srisgp.data.net.command.AlertsCommand;
import com.adyax.srisgp.data.net.command.GetQuickCardsCommand;
import com.adyax.srisgp.data.net.command.base.ServiceCommand;
import com.adyax.srisgp.data.net.executor.AppReceiver;
import com.adyax.srisgp.data.net.executor.CommandExecutor;
import com.adyax.srisgp.data.net.executor.ExecutionService;
import com.adyax.srisgp.data.net.response.AlertResponse;
import com.adyax.srisgp.data.net.response.GetQuickCardsAnonymousResponse;
import com.adyax.srisgp.data.net.response.tags.AlertItem;
import com.adyax.srisgp.data.net.response.tags.QuickCardsAnonymousItem;
import com.adyax.srisgp.data.repository.IRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class AppWidgetListRemoteViewsFactory extends BroadcastReceiver implements RemoteViewsService.RemoteViewsFactory, AppReceiver.AppReceiverCallback {

    private AppReceiver appReceiver = null;

    private static final int count = 5;
    private Context context;
    private int appWidgetId;
    private int[] appWidgetIds;
    private int[] viewTypeCount = new int[4];
    // Data
    //private List<String> items = new ArrayList<>();
    public List<QuickCardsAnonymousItem> items = new ArrayList<>();
    public List<AlertItem> alerts = new ArrayList<>();

    @Inject
    IRepository repository;

    public AppWidgetListRemoteViewsFactory() {}

    public AppWidgetListRemoteViewsFactory(Context context, Intent intent) {
        super();
        this.context = context;
        this.appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID);
    }

    @Override
    public void onCreate() {
        App.getApplicationComponent().inject(this);
        this.appReceiver = new AppReceiver();
        this.appReceiver.setListener(this);

        // On appelle le webservice pour récupérer les articles de la home
        this.launchCommand(this.context, this.appReceiver);
    }

    /**
     * Service command
     * Cards homepage
     */
    public void launchCommand(Context context, AppReceiver appReceiver) {
        final ServiceCommand command = new GetQuickCardsCommand(null);
        ExecutionService.sendCommand(context.getApplicationContext(), appReceiver, command, ExecutionService.COMMAND_GET_CARDS);
    }

    @Override
    public void onDataSetChanged() {
        // Update views
        //final ServiceCommand command = new GetQuickCardsCommand(null);
        // Log.v("WIDGET", "onDataSetChanged");
    }

    @Override
    public void onDestroy() {
        appReceiver.setListener(null);
    }

    @Override
    public int getCount() {
        return alerts.size() + Math.min(items.size(), 3);
    }

    @Override
    public RemoteViews getViewAt(int position) {
        // Si il y a une alerte alors on décale de 1 la position
        int index = position;

        // Next, set a fill-intent, which will be used to fill in the pending intent template
        // that is set on the collection view in StackWidgetProvider.
        Intent fillInIntent = new Intent();
        //fillInIntent.putExtras(extras);
        fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM, position);

        // Construct a remote views item based on the app widget item XML file,
        // and set the text based on the position.
        RemoteViews rv = null;//new RemoteViews(context.getPackageName(), R.layout.app_widget_item_search_card);
        if (!alerts.isEmpty() && position < alerts.size()) {
            final AlertItem alert = alerts.get(position);

            if (alert.level == 1) {
                rv = new RemoteViews(context.getPackageName(), R.layout.app_widget_item_search_card_alert);
                this.viewTypeCount[0] = 1;
            } else if (alert.level == 2) {
                rv = new RemoteViews(context.getPackageName(), R.layout.app_widget_item_search_card_alert_2);
                this.viewTypeCount[1] = 1;
            } else {
                rv = new RemoteViews(context.getPackageName(), R.layout.app_widget_item_search_card_alert_3);
                this.viewTypeCount[2] = 1;
            }

            // On renvoie une view pour Alerte
            rv.setTextViewText(R.id.tvTitle, "" + alert.getTitle());
            rv.setTextViewText(R.id.tvContent, "" + alert.getDescription());
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_TYPE, "WIDGET_ALERT");
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_NODE, alert.id);
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_URL, alert.content_url);
        } else {
            // index = position - alerts.size();
            rv = new RemoteViews(context.getPackageName(), R.layout.app_widget_item_search_card);
            rv.setTextViewText(R.id.tvTitle, items.get(index).title);
            rv.setTextViewText(R.id.tvContent, items.get(index).description);
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_TYPE, "WIDGET_ARTICLE");
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_NODE, items.get(index).nid);
            fillInIntent.putExtra(SNTAppWidget.EXTRA_ITEM_URL, items.get(index).getContentUrl());
            this.viewTypeCount[3] = 1;
        }

        // Make it possible to distinguish the individual on-click
        // action of a given item
        rv.setOnClickFillInIntent(R.id.rlContent, fillInIntent);

        // Return the remote views object.
        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        int count = 0;

        for (int value : this.viewTypeCount) {
            if (value == 1) {
                count++;
            }
        }

        return count;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    // AppReceiver Callback
    @Override
    public void onSuccess(int requestCode, Bundle data) {

        if (requestCode == ExecutionService.COMMAND_GET_CARDS) {
            GetQuickCardsAnonymousResponse response = data.getParcelable(CommandExecutor.BUNDLE_GET_QUICK_CARDS);
            items = response.items;

            this.updateWidgets(context);

            final String header = repository.getTemporaryHeader();
            if (header != null) {
                ExecutionService.sendCommand(context, appReceiver, new AlertsCommand(header), ExecutionService.ALERTS_ACTION);
            } else {
                alerts = new ArrayList<>();
            }
        }
        if (requestCode == ExecutionService.ALERTS_ACTION) {
            AlertResponse response = data.getParcelable(CommandExecutor.BUNDLE_ALERT_ITEMS);

            this.alerts = Arrays.asList(response.items);
            Collections.sort(this.alerts, (o1, o2) -> o1.compareTo(o2));

            this.updateWidgets(context);
        }

    }

    @Override
    public void onFail(int requestCode, AppReceiver.ErrorMessage errorMessage) {
        // Nothing to do
        // Log.v("WIDGET", "onFail");
    }

    @Override
    public void onMessage(int requestCode, Bundle data) {
        // Nothing to do
        // Log.v("WIDGET", "onMessage");

    }

    public void updateWidgets(Context context) {
        Intent intent = new Intent(context, SNTAppWidget.class);
        intent.setAction(SNTAppWidget.REFRESH_ACTION);
        // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
        // since it seems the onUpdate() is only fired on that:
        AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
        int[] ids = widgetManager.getAppWidgetIds(new ComponentName(context, SNTAppWidget.class));

        widgetManager.notifyAppWidgetViewDataChanged(ids, R.id.listView);

        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
        context.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String strAction = intent.getAction();

        if (SNTAppWidget.REFRESH_ACTION.equals(strAction)) {
            // DO UPDATE CUSTOM
            // Log.v("WIDGET", "__ ACTION_APPWIDGET_UPDATE");

            Bundle extras = intent.getExtras();
            if (extras != null) {
                appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);

                if (appWidgetIds != null && appWidgetIds.length > 0) {
                    // AppWidgetManager.getInstance(context).updateAppWidget(appWidgetIds, null);
                    this.context = context;
                    this.onCreate();
                }
            }
        }
    }

//... include adapter-like methods here. See the StackView Widget sample.

}