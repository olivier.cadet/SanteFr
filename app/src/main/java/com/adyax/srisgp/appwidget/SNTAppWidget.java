package com.adyax.srisgp.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.RemoteViews;

import com.adyax.srisgp.R;
import com.adyax.srisgp.presentation.MainActivity;

/**
 * Implementation of App Widget functionality.
 */
public class SNTAppWidget extends AppWidgetProvider {

    public static final String OPEN_ACTION = "com.adyax.srisgp.appwidget.OPEN_ACTION";
    public static final String REFRESH_ACTION  = "com.adyax.srisgp.appwidget.REFRESH_ACTION";
    public static final String EXTRA_ITEM = "com.adyax.srisgp.appwidget.EXTRA_ITEM";
    public static final String EXTRA_ITEM_TYPE = "com.adyax.srisgp.appwidget.EXTRA_ITEM_TYPE";
    public static final String EXTRA_ITEM_URL = "com.adyax.srisgp.appwidget.EXTRA_ITEM_URL";
    public static final String EXTRA_ITEM_NODE = "com.adyax.srisgp.appwidget.EXTRA_ITEM_NODE";
    public static final String EXTRA_APPRECEIVER = "com.adyax.srisgp.appwidget.EXTRA_APPRECEIVER";

    public void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId, int[] appWidgetIds) {

        Intent intent = new Intent(context, AppWidgetServices.class);

        // Add the app widget ID to the intent extras.
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        // Instantiate the RemoteViews object for the app widget layout.
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.app_widget_layout);
        // Set up the RemoteViews object to use a RemoteViews adapter.
        // This adapter connects
        // to a RemoteViewsService  through the specified intent.
        // This is how you populate the data.
        remoteViews.setRemoteAdapter(R.id.listView, intent);

        // Template Intent
        Intent templateActionIntent = new Intent(context, SNTAppWidget.class);
        // Set the action for the intent.
        // When the user touches a particular view, it will have the effect of
        // broadcasting TOAST_ACTION.
        templateActionIntent.setAction(SNTAppWidget.OPEN_ACTION);
        templateActionIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        templateActionIntent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
        PendingIntent refreshIntent = PendingIntent.getBroadcast(context, 0, templateActionIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setPendingIntentTemplate(R.id.listView, refreshIntent);

        Intent intentSync = new Intent(context, AppWidgetListRemoteViewsFactory.class);
        intentSync.setAction(SNTAppWidget.REFRESH_ACTION);
        intentSync.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
        PendingIntent pendingSync = PendingIntent.getBroadcast(context,0, intentSync, PendingIntent.FLAG_UPDATE_CURRENT);
        // Add click listener on refresh icon
        remoteViews.setOnClickPendingIntent(R.id.refreshWidget, pendingSync);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, null);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);

        // Update
        // appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId, R.id.listView);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, appWidgetIds);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        String strAction = intent.getAction();

        if (SNTAppWidget.OPEN_ACTION.equals(strAction)) {
            int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            int viewIndex = intent.getIntExtra(EXTRA_ITEM, 0);

            String itemType = intent.getStringExtra(EXTRA_ITEM_TYPE);
            String itemUrl = intent.getStringExtra(EXTRA_ITEM_URL);
            Long itemNode = intent.getLongExtra(EXTRA_ITEM_NODE, 0);

            Intent openIntent = new Intent(context, MainActivity.class);
            openIntent.setAction(OPEN_ACTION);
            openIntent.putExtra(EXTRA_ITEM_TYPE, itemType);
            openIntent.putExtra(EXTRA_ITEM_URL, itemUrl);
            openIntent.putExtra(EXTRA_ITEM_NODE, itemNode);

            if ("WIDGET_ALERT".equals(itemType)) {
                // Open WebViewActivity
                openIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(openIntent);
            } else {
                openIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(openIntent);
            }
        } else if (SNTAppWidget.REFRESH_ACTION.equals(strAction)) {
            // DO UPDATE CUSTOM
            // Log.v("WIDGET", "ACTION_APPWIDGET_UPDATE REFRESH_ACTION");

            Bundle extras = intent.getExtras();
            if (extras != null) {
                int[] appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);

                if (appWidgetIds != null && appWidgetIds.length > 0) {
                    this.onUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
                }
            }
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

