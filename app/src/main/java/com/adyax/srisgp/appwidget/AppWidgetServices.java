package com.adyax.srisgp.appwidget;

import android.content.Intent;
import android.widget.RemoteViewsService;


public class AppWidgetServices extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new AppWidgetListRemoteViewsFactory(this.getApplicationContext(), intent);
    }
}

