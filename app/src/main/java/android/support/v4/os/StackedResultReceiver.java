package android.support.v4.os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Stack;

/**
 * Created by Sergii Suvorov AKA CYBOPOB on 13.09.2016.
 */
public abstract class StackedResultReceiver<T> extends ResultReceiver {

    private static final String PARAM_RESULT_CODE = "android.support.v4.os.param_result_code";
    private final Stack<Bundle> mBundles = new Stack<>();
    private T listener;

    public StackedResultReceiver() {
        super(new Handler());
    }

    public StackedResultReceiver(@NonNull Parcel in) {
        super(in);
        int size = in.readInt();
        while (size > 0) {
            mBundles.push(in.readBundle(getClass().getClassLoader()));
            size--;
        }
    }

    public T getListener() {
        return listener;
    }

    public void setListener(T listener) {
        this.listener = listener;
        if (this.listener != null) {
            while (!mBundles.empty()) {
                onHandleResult(this.listener, mBundles.peek().getInt(PARAM_RESULT_CODE), mBundles.pop());
            }
        }
    }

    @Override
    protected void onReceiveResult(int resultCode, @Nullable Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (listener == null) {
            Bundle localResultData = resultData;
            if (localResultData == null || localResultData == Bundle.EMPTY) {
                localResultData = new Bundle();
            }
            localResultData.putInt(PARAM_RESULT_CODE, resultCode);
            mBundles.push(localResultData);
        } else {
            onHandleResult(listener, resultCode, resultData);
        }
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(mBundles.size());
        for (Bundle bundle : mBundles) {
            out.writeBundle(bundle);
        }
    }

    protected abstract void onHandleResult(@NonNull T listener, int resultCode, Bundle resultData);

}
