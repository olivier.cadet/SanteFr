fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android deploy_beta_stage
```
fastlane android deploy_beta_stage
```
Deploy Stage application

Usage "bundle exec fastlane deploy_beta_stage updateBuildVersion:true|false toFirebase:true|false"
### android deploy_beta_preprod
```
fastlane android deploy_beta_preprod
```
Deploy Pre-Production application

Usage "bundle exec fastlane deploy_beta_preprod updateBuildVersion:true|false toFirebase:true|false"
### android deploy_beta_production
```
fastlane android deploy_beta_production
```
Deploy Production application

Usage "bundle exec fastlane deploy_beta_production updateBuildVersion:true|false toFirebase:true|false"
### android deploy_beta_connect
```
fastlane android deploy_beta_connect
```
Build and deploy a new version to the Google Play

Usage "bundle exec fastlane deploy_beta_connect envIndex:0|1|2 updateBuildVersion:true|false toFirebase:true|false"
### android test
```
fastlane android test
```


----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
