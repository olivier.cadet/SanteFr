






### Automatic deployment on Firebase Distribution
Doc : https://firebase.google.com/docs/app-distribution/android/distribute-fastlane

#### Install Firebase CLI on CI serveur
Doc: https://firebase.google.com/docs/cli#install-cli-mac-linux
 ```{bash}
curl -sL firebase.tools | bash
```

#### Configure CI
Doc: https://firebase.google.com/docs/cli#cli-ci-systems
```
firebase login:ci

Token to use :
FIREBASE_TOKEN = 1//07QLfdyVmV_KHCgYIARAAGAcSNwF-L9IrqxMV0SBobWMyMD8Djg57LMMbr_yBbW5wnNUyOAPLIQrmHP2qlLy9a1NvulpB5L2fNbI

Example: firebase deploy --token "$FIREBASE_TOKEN"

```


