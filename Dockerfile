FROM jangrewe/gitlab-ci-android:29

RUN apt-get update -yq \
    && apt-get -yq install curl gnupg ca-certificates \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get update -yq \
    && apt-get install -yq \
        build-essential \
        apt-utils \
        dh-autoreconf \
        ruby \
        ruby-dev \
        nodejs \
        yarn

ENV PATH="$(yarn global bin):$PATH"

RUN yarn global add firebase-tools
RUN gem install bundler
